///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:31:00
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_int.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW34CE.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_int.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\phy_int.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _RpCb
        EXTERN _RpRfStat
        EXTERN _RpFreqBandTbl
        EXTERN _RpTc0WasteTime
        EXTERN _RpCcaVthOffsetTblDefault
        EXTERN _RpCcaVthOffsetTblNarrow
        EXTERN ?L_AND_FAST_L03
        EXTERN ?L_IOR_L03
        EXTERN ?SI_MOD_L02
        EXTERN ?UL_CMP_L03
        EXTERN _RpAddressFilterSetting
        EXTERN _RpAvailableRcvRamEnable
        EXTERN _RpCalcTotalBytes
        EXTERN _RpCalcTxInterval
        EXTERN _RpCheckLongerThanTotalTxTime
        EXTERN _RpCheckRfIRQ
        EXTERN _RpExtChkErrFrameLen
        EXTERN _RpGetRxBuf
        EXTERN _RpGetTime
        EXTERN _RpInverseTxAnt
        EXTERN _RpLog_Event
        EXTERN _RpMemcpy
        EXTERN _RpPrevSentTimeReSetting
        EXTERN _RpReadIrq
        EXTERN _RpRegAdcVgaDefault
        EXTERN _RpRegBlockRead
        EXTERN _RpRegBlockWrite
        EXTERN _RpRegCcaBandwidth225k
        EXTERN _RpRegRead
        EXTERN _RpRegRxDataRateDefault
        EXTERN _RpRegTxRxDataRateDefault
        EXTERN _RpRegWrite
        EXTERN _RpRelRxBuf
        EXTERN _RpRxOffBeforeReplyingAck
        EXTERN _RpRxOnStart
        EXTERN _RpRxOnStop
        EXTERN _RpSetCcaDurationVal
        EXTERN _RpSetCsmaBackoffPeriod
        EXTERN _RpSetFcsLengthVal
        EXTERN _RpSetMaxCsmaBackoffVal
        EXTERN _RpSetPowerMode
        EXTERN _RpSetSfdDetectionExtendWrite
        EXTERN _RpSetStateRxOnToTrxOff
        EXTERN _RpSetTxTriggerTimer
        EXTERN _RpTC0SetReg
        EXTERN _RpTrnxHdrFunc
        EXTERN _RpUpdateTxTime
        EXTERN ___get_psw
        EXTERN ___set_psw

        PUBLIC _RpAckCheckCallback
        PUBLIC _RpAntSelAssist_RecoveryProc
        PUBLIC _RpAntSelAssist_StartProc
        PUBLIC _RpAntSelAssist_StopProc
        PUBLIC _RpCcaAck
        PUBLIC _RpChangeDefaultFilter
        PUBLIC _RpChangeFilter
        PUBLIC _RpChangeNarrowBandFilter
        PUBLIC _RpIntpHdr
        PUBLIC _RpStartTransmitWithCca
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
        
          CFI Common cfiCommon3 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon3
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_int.c
//    1 /***********************************************************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
//    4  * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
//    5  * applicable laws, including copyright laws. 
//    6  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
//    7  * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
//    8  * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//    9  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
//   10  * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
//   11  * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
//   12  * DAMAGES.
//   13  * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
//   14  * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
//   15  * following link:
//   16  * http://www.renesas.com/disclaimer 
//   17  **********************************************************************************************************************/
//   18 /***********************************************************************************************************************
//   19  * file name	: phy_int.c
//   20  * description	: This is the RF driver's interrupts code.
//   21  ***********************************************************************************************************************
//   22  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
//   23  **********************************************************************************************************************/
//   24 /***************************************************************************************************************
//   25  * includes
//   26  **************************************************************************************************************/
//   27 #include <string.h>
//   28 //
//   29 #include "phy.h"
//   30 #include "phy_def.h"
//   31 //
//   32 #if defined(__arm)
//   33 	#include "cpx3.h"
//   34 	#include "phy_drv.h"
//   35 #endif
//   36 //
//   37 #ifdef R_FSB_FAN_ENABLED
//   38 	#include "r_fsb.h"
//   39 #endif // #ifdef R_FSB_FAN_ENABLED
//   40 
//   41 /***************************************************************************************************************
//   42  * extern definitions
//   43  **************************************************************************************************************/
//   44 extern RP_PHY_CB RpCb;
//   45 extern RP_PHY_ERROR RpRfStat;
//   46 extern const uint16_t RpTc0WasteTime[];
//   47 extern const uint8_t RpFreqBandTbl[][RP_MAXNUM_OFFSET];
//   48 extern const uint16_t RpCcaVthOffsetTblDefault[];
//   49 extern const uint16_t RpCcaVthOffsetTblNarrow[];
//   50 
//   51 /***************************************************************************************************************
//   52  * private function prototypes
//   53  **************************************************************************************************************/
//   54 /* "Interrupt source number 1" */
//   55 /* "Timer compare 0 interrupt handler" */
//   56 static void RpBbTim0Hdr( void );
//   57 
//   58 /* "Interrupt source number 2" */
//   59 /* "Timer compare 1 interrupt handler" */
//   60 static void RpBbTim1Hdr( void );
//   61 
//   62 /* "Interrupt source number 3" */
//   63 /* "Timer compare 2 interrupt handler" */
//   64 static void RpBbTim2Hdr( void );
//   65 
//   66 /* "Interrupt source number 4" */
//   67 /* "Frame transmit completion interrupt handler" */
//   68 static void RpTrnFinHdr( void );
//   69 
//   70 /* "Interrupt source number 5" */
//   71 /* "Bank0 transmit completion interrupt handler" */
//   72 static void RpTrn0Hdr( void );
//   73 
//   74 /* "Interrupt source number 6" */
//   75 /* "Bank1 transmit completion interrupt handler" */
//   76 static void RpTrn1Hdr( void );
//   77 
//   78 /* "Interrupt source number 8,9" */
//   79 /* "CCA/CSMA-CA completion interrupt handler" */
//   80 static void RpCcaHdr( void );
//   81 
//   82 /* "Interrupt source number 10" */
//   83 /* "Frame receive completion interrupt handler" */
//   84 static void RpRcvFinHdr( void );
//   85 
//   86 /* "Interrupt source number 11" */
//   87 /* "Bank0 receive completion interrupt handler" */
//   88 static void RpRcv0Hdr( void );
//   89 
//   90 /* "Interrupt source number 12" */
//   91 /* "Bank1 receive completion interrupt handler" */
//   92 static void RpRcv1Hdr( void );
//   93 
//   94 /* "Interrupt source number 13" */
//   95 /* "Start reception interrupt handler" */
//   96 static void RpRcvStHdr( void );
//   97 
//   98 /* "Interrupt source number 14" */
//   99 /* "Address filter interrupt handler" */
//  100 static void RpAdrsHdr( void );
//  101 
//  102 /* "Interrupt source number 15" */
//  103 /* "Receive overrun interrupt handler" */
//  104 static void RpRovrHdr( void );
//  105 
//  106 /* "Interrupt source number 16" */
//  107 /* "Mode switch receive completion interrupt handler" */
//  108 static void RpRcvModeswHdr( void );
//  109 
//  110 /* "Interrupt source number 17" */
//  111 /* "Receive level filter interrupt handler" */
//  112 static void RpRcvLvlIntHdr( void );
//  113 
//  114 /* "Interrupt source number 18" */
//  115 /* "Receive byte counts interrupt handler" */
//  116 static void RpRcvCuntHdr( void );
//  117 
//  118 /* "Interrupt source number 19" */
//  119 /* "Frame length interrupt handler" */
//  120 static void RpFlenHdr( void );
//  121 
//  122 /* "Interrupt source number 20" */
//  123 /* "Byte reception interrupt handler" */
//  124 static void RpByteRcvHdr( void );
//  125 
//  126 /* === Unused interrupt handler === */
//  127 /* "Interrupt source number 7" "Calibration completion" */
//  128 /* "Interrupt source number 21" "Automatic receive timeout" */
//  129 
//  130 //
//  131 static void RpSetAntennaDiversityModeReg( void );
//  132 static void RpSetNumberOfCrcBitSwitchBit( void );
//  133 static void RpGetAntennaDiversityRssi( void );
//  134 static void RpGetSelectedAntennaInfo( uint16_t status );
//  135 static void RpValidityCheckSFDTimeStamp( void );
//  136 static void RpPdDataIndCallbackPrc( uint8_t *pBuf, uint8_t stOptInd, uint8_t warningIndCallbackFlag );
//  137 static void RpTrnHdrFunc( void );
//  138 static void RpCancelTrn( uint16_t status );
//  139 static uint8_t RpDriverRetryTrn( uint16_t status );
//  140 static void RpAfterTrnEnd( uint16_t status );
//  141 static void RpResumeCsmaTrn( uint16_t status, uint8_t ccaTotal, uint8_t rxEna );
//  142 static void RpStateRx_SetTxTrigger( uint8_t useCca );
//  143 static void RpEndTransmitWithCca( void );
//  144 static uint8_t RpChkCrcStatus( uint8_t *pStatOpt, uint8_t txrxst0 );
//  145 static void RpRxEndSetting( uint16_t status );
//  146 static void RpRcvHdrFunc( uint8_t rcvNo );
//  147 static void RpRcvRamEnable( uint8_t rcvxNos );
//  148 static void RpReadRam( uint8_t rcvNo );
//  149 static int16_t RpChkRcvBank( uint8_t rcvBankNo );
//  150 static void RpSelectRcvDataBank( void );
//  151 static int16_t RpReadRxDataBank( void );
//  152 static uint8_t RpDetRcvdAntenna( void );
//  153 static void RpReadAddressInfo( void );
//  154 static void RpDumPrcRcvPhr( void );
//  155 static void RpReStartProc( void );
//  156 static void RpAntSelAssist_StateCheckHdr( void );
//  157 static void RpAntSelAssist_TimerProc( void );
//  158 static void RpAntSelAssist_ReStartProc( void );
//  159 
//  160 /***************************************************************************************************************
//  161  * private variables
//  162  **************************************************************************************************************/
//  163 #ifdef R_FSB_FAN_ENABLED
//  164 	typedef struct {
//  165 		uint8_t enable;
//  166 		uint8_t resData[R_SIZE_PSDU];
//  167 	} r_phy_fsb_t;
//  168 	r_phy_fsb_t RpFsb;
//  169 #endif // #ifdef R_FSB_FAN_ENABLED
//  170 
//  171 typedef struct {
//  172 	uint8_t enable;
//  173 	uint8_t result;
//  174 } r_phy_cca_ack_t;

        SECTION `.bssf`:DATA:REORDER:NOROOT(0)
//  175 r_phy_cca_ack_t RpCcaAck;
_RpCcaAck:
        DS 2
//  176 
//  177 /***************************************************************************************************************
//  178  * program
//  179  **************************************************************************************************************/
//  180 /***************************************************************************************************************
//  181  * function name  : RpIntpHdr
//  182  * description    : intp interrupt handler
//  183  * parameters     : none
//  184  * return value   : none
//  185  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _RpIntpHdr
        CODE
//  186 void RpIntpHdr( void )
//  187 {
_RpIntpHdr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 10
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+14
          CFI FunCall _RpCheckRfIRQ
        ; ------------------------------------- Block: 1 cycles
//  188 	#if defined(__arm)
//  189 	volatile uint32_t ireq, ien;
//  190 	#else
//  191 	uint32_t ireq, ien;
//  192 	#endif
//  193 
//  194 	while (RpCheckRfIRQ() == RP_TRUE)
??RpIntpHdr_0:
        CALL      F:_RpCheckRfIRQ    ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_0  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  195 	{
//  196 		#if !defined(__CCRL__) && !defined(__ICCRL78__)
//  197 		ien = *(uint32_t *)(RpCb.reg.bbIntEn);
//  198 		#else
//  199 		ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
//  200 			  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+344    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOV       A, ES:_RpCb+345    ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       X, ES:_RpCb+346    ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  201 		#endif
//  202 
//  203 		ireq = RpReadIrq();
          CFI FunCall _RpReadIrq
        CALL      F:_RpReadIrq       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
//  204 		ireq &= ien;
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        BR        S:??RpAckCheckCallback_1  ;; 3 cycles
          CFI CFA SP+14
          CFI FunCall _RpRcvStHdr
        ; ------------------------------------- Block: 33 cycles
//  205 
//  206 		while (ireq != RP_BB_NO_IREQ)
//  207 		{
//  208 			if (ireq & RP_BBRCVST_IREQ)
//  209 			{
//  210 				#if defined(__arm)
//  211 				RpLedRxOff();
//  212 				#endif
//  213 
//  214 				RpRcvStHdr();
??RpIntpHdr_1:
        CALL      F:_RpRcvStHdr      ;; 3 cycles
//  215 
//  216 				#if !defined(__CCRL__) && !defined(__ICCRL78__)
//  217 				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
//  218 				#else
//  219 				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
//  220 					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+344    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOV       A, ES:_RpCb+345    ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       X, ES:_RpCb+346    ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  221 				#endif
//  222 
//  223 				ireq &= ien;
//  224 				ireq &= (~RP_BBRCVST_IREQ);
        MOVW      AX, DE             ;; 1 cycle
        AND       A, #0xF7           ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        ; ------------------------------------- Block: 26 cycles
??RpIntpHdr_2:
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_1:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall ?L_AND_FAST_L03
        ; ------------------------------------- Block: 1 cycles
??RpIntpHdr_3:
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpIntpHdr_4:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, SP             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??RpIntpHdr_5:
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  225 			}
??RpIntpHdr_6:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpIntpHdr_0    ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        INCW      HL                 ;; 1 cycle
        BT        [HL].3, ??RpIntpHdr_1  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  226 			else if (ireq & RP_BBMODEVA_IREQ)
        INCW      HL                 ;; 1 cycle
        BF        [HL].7, ??RpAckCheckCallback_2  ;; 5 cycles
          CFI FunCall _RpAntSelAssist_StateCheckHdr
        ; ------------------------------------- Block: 6 cycles
//  227 			{
//  228 				RpAntSelAssist_StateCheckHdr();
        CALL      F:_RpAntSelAssist_StateCheckHdr  ;; 3 cycles
//  229 
//  230 				#if !defined(__CCRL__) && !defined(__ICCRL78__)
//  231 				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
//  232 				#else
//  233 				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
//  234 					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+344    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOV       A, ES:_RpCb+345    ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       X, ES:_RpCb+346    ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  235 				#endif
//  236 
//  237 				ireq |= RpReadIrq();
//  238 				ireq &= ien;
//  239 				ireq &= (~RP_BBMODEVA_IREQ);
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _RpReadIrq
        CALL      F:_RpReadIrq       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        XCH       A, C               ;; 1 cycle
        AND       A, #0x7F           ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        BR        S:??RpIntpHdr_4    ;; 3 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 46 cycles
//  240 			}
//  241 			else if (ireq & RP_BBBYTE_IREQ)
??RpAckCheckCallback_2:
        BF        [HL].6, ??RpAckCheckCallback_3  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  242 			{
//  243 				RpByteRcvHdr();
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+344.2     ;; 3 cycles
        CLR1      ES:_RpCb+346.6     ;; 3 cycles
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       C, ES:_RpCb+346    ;; 2 cycles
        MOVW      AX, #0x1D8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  244 
//  245 				#if !defined(__CCRL__) && !defined(__ICCRL78__)
//  246 				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
//  247 				#else
//  248 				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
//  249 					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+344    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOV       A, ES:_RpCb+345    ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       X, ES:_RpCb+346    ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  250 				#endif
//  251 
//  252 				ireq |= RpReadIrq();
//  253 				ireq &= ien;
//  254 				ireq &= (~(RP_BBBYTE_IREQ | RP_BBTIM2_IREQ));
        MOVW      AX, #0xFFBF        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       X, #0xFB           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
          CFI FunCall _RpReadIrq
        CALL      F:_RpReadIrq       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+22
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
        BR        R:??RpIntpHdr_3    ;; 3 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 65 cycles
//  255 			}
//  256 			else if (ireq & RP_BBTIM2_IREQ)
??RpAckCheckCallback_3:
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        BF        [HL].2, ??RpAckCheckCallback_4  ;; 5 cycles
          CFI FunCall _RpBbTim2Hdr
        ; ------------------------------------- Block: 7 cycles
//  257 			{
//  258 				RpBbTim2Hdr();
        CALL      F:_RpBbTim2Hdr     ;; 3 cycles
//  259 
//  260 				#if !defined(__CCRL__) && !defined(__ICCRL78__)
//  261 				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
//  262 				#else
//  263 				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
//  264 					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+344    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOV       A, ES:_RpCb+345    ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       X, ES:_RpCb+346    ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  265 				#endif
//  266 
//  267 				ireq |= (RpCb.reg.bbIntReq & ien);
//  268 				ireq &= (~(RP_BBBYTE_IREQ | RP_BBTIM2_IREQ));
        MOVW      AX, #0xFFBF        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       X, #0xFB           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      HL, #LWRD(_RpCb+348)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+18
        BR        R:??RpIntpHdr_3    ;; 3 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 50 cycles
//  269 			}
//  270 			else if (ireq & RP_BBROVR_IREQ)
??RpAckCheckCallback_4:
        INCW      HL                 ;; 1 cycle
        BF        [HL].5, ??RpAckCheckCallback_5  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  271 			{
//  272 				RpRovrHdr();
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??RpAckCheckCallback_6  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       X, #0x8            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
        MOVW      HL, #LWRD(_RpCb+312)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x22           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
        MOV       ES, #BYTE3(_RpRfStat)  ;; 1 cycle
        MOVW      AX, ES:_RpRfStat   ;; 2 cycles
        MOVW      BC, ES:_RpRfStat+2  ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      HL, #LWRD(_RpRfStat)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
          CFI FunCall _RpAvailableRcvRamEnable
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].0, ??RpAckCheckCallback_6  ;; 5 cycles
          CFI FunCall _RpRxOnStart
        ; ------------------------------------- Block: 46 cycles
        CALL      F:_RpRxOnStart     ;; 3 cycles
          CFI FunCall _RpAntSelAssist_ReStartProc
        CALL      F:_RpAntSelAssist_ReStartProc  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  273 				ireq &= (~(RP_BBROVR_IREQ | RP_BBADRS_IREQ | RP_BBFL_IREQ | RP_BBTIM2_IREQ));
??RpAckCheckCallback_6:
        MOVW      AX, #0xFFDF        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, #0xCFFB        ;; 1 cycle
        BR        R:??RpIntpHdr_2    ;; 3 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 6 cycles
//  274 			}
//  275 			else if (ireq & RP_BBADRS_IREQ)
??RpAckCheckCallback_5:
        BF        [HL].4, ??RpAckCheckCallback_7  ;; 5 cycles
          CFI FunCall _RpSelectRcvDataBank
        ; ------------------------------------- Block: 5 cycles
//  276 			{
//  277 				RpAdrsHdr();
        CALL      F:_RpSelectRcvDataBank  ;; 3 cycles
//  278 				ireq &= (~RP_BBADRS_IREQ);
        MOVW      AX, [SP]           ;; 1 cycle
        AND       A, #0xEF           ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  279 				if ((RpCb.status & RP_PHY_STAT_RX_TMOUT) && (RpCb.pib.phyRxTimeoutMode == RP_TRUE) &&
//  280 						(RpCb.rx.cnt == RP_PHY_RX_STAT_PHR_DETECT))
        ; ------------------------------------- Block: 7 cycles
??RpIntpHdr_7:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        BT        A.2, $+7           ;; 5 cycles
        BR        F:??RpIntpHdr_6    ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        CMP       ES:_RpCb+230, #0x1  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpIntpHdr_6    ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpIntpHdr_6    ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  281 				{
//  282 					ireq |= RpReadIrq();
//  283 					ireq &= ien;
//  284 					ireq &= (~RP_BBTIM1_IREQ);
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _RpReadIrq
        CALL      F:_RpReadIrq       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFD           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        BR        R:??RpIntpHdr_4    ;; 3 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 25 cycles
//  285 				}
//  286 			}
//  287 			else if (ireq & RP_BBFL_IREQ)
??RpAckCheckCallback_7:
        INCW      HL                 ;; 1 cycle
        BF        [HL].5, ??RpAckCheckCallback_8  ;; 5 cycles
          CFI FunCall _RpSelectRcvDataBank
        ; ------------------------------------- Block: 6 cycles
//  288 			{
//  289 				RpFlenHdr();
        CALL      F:_RpSelectRcvDataBank  ;; 3 cycles
//  290 				ireq &= (~RP_BBFL_IREQ);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0xDF           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  291 				if ((RpCb.status & RP_PHY_STAT_RX_TMOUT) && (RpCb.pib.phyRxTimeoutMode == RP_TRUE) &&
//  292 						(RpCb.rx.cnt == RP_PHY_RX_STAT_PHR_DETECT))
        BR        S:??RpIntpHdr_7    ;; 3 cycles
        ; ------------------------------------- Block: 14 cycles
//  293 				{
//  294 					ireq |= RpReadIrq();
//  295 					ireq &= ien;
//  296 					ireq &= (~RP_BBTIM1_IREQ);
//  297 				}
//  298 			}
//  299 			else if (ireq & RP_BBRCVCUNT_IREQ)
??RpAckCheckCallback_8:
        MOV1      CY, [HL].4         ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        BNC       ??RpAckCheckCallback_9  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  300 			{
//  301 				RpRcvCuntHdr();
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??RpAckCheckCallback_10  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, ES:_RpCb+106   ;; 2 cycles
        CMPW      AX, #0x1B          ;; 1 cycle
        SKC                          ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, #0x1B          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpIntpHdr_8:
        MOVW      [SP+0x08], AX      ;; 1 cycle
        CMP0      ES:_RpCb+146       ;; 2 cycles
        BZ        ??RpAckCheckCallback_11  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      AX, #0x1800        ;; 1 cycle
        BR        S:??RpAckCheckCallback_12  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_11:
        MOVW      AX, #0x1C00        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_12:
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       C, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, C               ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        ADDW      AX, HL             ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        ; ------------------------------------- Block: 28 cycles
//  302 				ireq &= (~RP_BBRCVCUNT_IREQ);
??RpAckCheckCallback_10:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0xEF           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        BR        R:??RpIntpHdr_5    ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  303 			}
//  304 			else if (ireq & RP_BBRCV0_IREQ)
??RpAckCheckCallback_9:
        INCW      HL                 ;; 1 cycle
        BF        [HL].1, ??RpAckCheckCallback_13  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  305 			{
//  306 				RpRcv0Hdr();
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??RpAckCheckCallback_14  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, #0x38          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].4, ??RpAckCheckCallback_14  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        CMPW      AX, #0xFFFE        ;; 1 cycle
        BZ        ??RpAckCheckCallback_15  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        CMP0      ES:_RpCb+143       ;; 2 cycles
        BZ        ??RpAckCheckCallback_16  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      BC, #0x80          ;; 1 cycle
          CFI FunCall ?SI_MOD_L02
        CALL      N:?SI_MOD_L02      ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      DE, ES:_RpCb+110   ;; 2 cycles
        MOVW      AX, ES:_RpCb+106   ;; 2 cycles
        SUBW      AX, DE             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xFF80        ;; 1 cycle
        CMPW      AX, #0x81          ;; 1 cycle
        BNC       ??RpAckCheckCallback_16  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, ES:_RpCb+106   ;; 2 cycles
        CMPW      AX, #0x80          ;; 1 cycle
        BZ        ??RpAckCheckCallback_14  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        ONEB      ES:_RpCb+145       ;; 2 cycles
        BR        S:??RpAckCheckCallback_14  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpAckCheckCallback_16:
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpReadRam
        CALL      F:_RpReadRam       ;; 3 cycles
        BR        S:??RpAckCheckCallback_14  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpAckCheckCallback_15:
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpRcvRamEnable
        CALL      F:_RpRcvRamEnable  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  307 				ireq &= (~RP_BBRCV0_IREQ);
??RpAckCheckCallback_14:
        MOVW      AX, [SP]           ;; 1 cycle
        AND       A, #0xFD           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpIntpHdr_9:
        MOVW      HL, SP             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        R:??RpIntpHdr_6    ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  308 			}
//  309 			else if (ireq & RP_BBRCV1_IREQ)
??RpAckCheckCallback_13:
        BF        [HL].2, ??RpAckCheckCallback_17  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  310 			{
//  311 				RpRcv1Hdr();
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??RpAckCheckCallback_18  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, #0x38          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].5, ??RpAckCheckCallback_18  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        CMPW      AX, #0xFFFE        ;; 1 cycle
        BZ        ??RpAckCheckCallback_19  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        CMP0      ES:_RpCb+143       ;; 2 cycles
        BZ        ??RpAckCheckCallback_20  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      BC, #0x80          ;; 1 cycle
          CFI FunCall ?SI_MOD_L02
        CALL      N:?SI_MOD_L02      ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      DE, ES:_RpCb+110   ;; 2 cycles
        MOVW      AX, ES:_RpCb+106   ;; 2 cycles
        SUBW      AX, DE             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xFF80        ;; 1 cycle
        CMPW      AX, #0x81          ;; 1 cycle
        BNC       ??RpAckCheckCallback_20  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, ES:_RpCb+106   ;; 2 cycles
        CMPW      AX, #0x80          ;; 1 cycle
        BZ        ??RpAckCheckCallback_18  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        ONEB      ES:_RpCb+145       ;; 2 cycles
        BR        S:??RpAckCheckCallback_18  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpAckCheckCallback_20:
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpReadRam
        CALL      F:_RpReadRam       ;; 3 cycles
        BR        S:??RpAckCheckCallback_18  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpAckCheckCallback_19:
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpRcvRamEnable
        CALL      F:_RpRcvRamEnable  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  312 				ireq &= (~RP_BBRCV1_IREQ);
??RpAckCheckCallback_18:
        MOVW      AX, [SP]           ;; 1 cycle
        AND       A, #0xFB           ;; 1 cycle
        BR        S:??RpIntpHdr_9    ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  313 			}
//  314 			else if (ireq & RP_BBRCVFIN_IREQ)
??RpAckCheckCallback_17:
        BF        [HL].0, ??RpAckCheckCallback_21  ;; 5 cycles
          CFI FunCall _RpRcvFinHdr
        ; ------------------------------------- Block: 5 cycles
//  315 			{
//  316 				#if defined(__arm)
//  317 				RpLedRxOn();
//  318 				#endif
//  319 
//  320 				RpRcvFinHdr();
        CALL      F:_RpRcvFinHdr     ;; 3 cycles
//  321 
//  322 				#if !defined(__CCRL__) && !defined(__ICCRL78__)
//  323 				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
//  324 				#else
//  325 				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
//  326 					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+344    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOV       A, ES:_RpCb+345    ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       X, ES:_RpCb+346    ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  327 				#endif
//  328 
//  329 				ireq |= (RpCb.reg.bbIntReq & ien);
//  330 				ireq &= ien;
//  331 				ireq &= (~RP_BBRCVFIN_IREQ);
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      HL, #LWRD(_RpCb+348)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        AND       A, #0xFE           ;; 1 cycle
        BR        R:??RpIntpHdr_4    ;; 3 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 52 cycles
//  332 			}
//  333 			else if (ireq & RP_BBCCA_IREQ)
??RpAckCheckCallback_21:
        DECW      HL                 ;; 1 cycle
        BF        [HL].7, ??RpAckCheckCallback_22  ;; 5 cycles
          CFI FunCall _RpCcaHdr
        ; ------------------------------------- Block: 6 cycles
//  334 			{
//  335 				RpCcaHdr();
        CALL      F:_RpCcaHdr        ;; 3 cycles
//  336 				ireq &= (~RP_BBCCA_IREQ);
        MOVW      AX, [SP]           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x7F           ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
??RpIntpHdr_10:
        XCH       A, X               ;; 1 cycle
        BR        R:??RpIntpHdr_9    ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  337 			}
//  338 			else if (ireq & RP_BBTRN0_IREQ)
??RpAckCheckCallback_22:
        BF        [HL].4, ??RpAckCheckCallback_23  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  339 			{
//  340 				#if defined(__arm)
//  341 				RpLedTxOff();
//  342 				#endif
//  343 
//  344 				RpTrn0Hdr();
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
//  345 				ireq &= (~RP_BBTRN0_IREQ);
        MOVW      AX, [SP]           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xEF           ;; 1 cycle
        BR        S:??RpIntpHdr_10   ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
//  346 			}
//  347 			else if (ireq & RP_BBTRN1_IREQ)
??RpAckCheckCallback_23:
        BF        [HL].5, ??RpAckCheckCallback_24  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  348 			{
//  349 				#if defined(__arm)
//  350 				RpLedTxOff();
//  351 				#endif
//  352 
//  353 				RpTrn1Hdr();
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
//  354 				ireq &= (~RP_BBTRN1_IREQ);
        MOVW      AX, [SP]           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xDF           ;; 1 cycle
        BR        S:??RpIntpHdr_10   ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
//  355 			}
//  356 			else if (ireq & RP_BBTRNFIN_IREQ)
??RpAckCheckCallback_24:
        BF        [HL].3, ??RpAckCheckCallback_25  ;; 5 cycles
          CFI FunCall _RpTrnHdrFunc
        ; ------------------------------------- Block: 5 cycles
//  357 			{
//  358 				#if defined(__arm)
//  359 				RpLedTxOn();
//  360 				#endif
//  361 
//  362 				RpTrnFinHdr();
        CALL      F:_RpTrnHdrFunc    ;; 3 cycles
//  363 				ireq &= (~RP_BBTRNFIN_IREQ);
        MOVW      AX, [SP]           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xF7           ;; 1 cycle
        BR        S:??RpIntpHdr_10   ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  364 			}
//  365 			else if (ireq & RP_BBTIM0_IREQ)
??RpAckCheckCallback_25:
        BF        [HL].0, ??RpAckCheckCallback_26  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  366 			{
//  367 				RpBbTim0Hdr();
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+344.0     ;; 3 cycles
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].6, ??RpAckCheckCallback_27  ;; 5 cycles
        ; ------------------------------------- Block: 22 cycles
        CLR1      ES:_RpCb.6         ;; 3 cycles
        BF        [HL].1, ??RpAckCheckCallback_28  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        SET1      ES:[HL].5          ;; 3 cycles
        BR        S:??RpAckCheckCallback_27  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpAckCheckCallback_28:
        BF        [HL].0, ??RpAckCheckCallback_27  ;; 5 cycles
          CFI FunCall _RpAvailableRcvRamEnable
        ; ------------------------------------- Block: 5 cycles
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
          CFI FunCall _RpRxOnStart
        CALL      F:_RpRxOnStart     ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  368 				ireq &= (~RP_BBTIM0_IREQ);
??RpAckCheckCallback_27:
        MOVW      AX, [SP]           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        BR        S:??RpIntpHdr_10   ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  369 			}
//  370 			else if (ireq & RP_BBTIM1_IREQ)
??RpAckCheckCallback_26:
        BF        [HL].1, ??RpAckCheckCallback_29  ;; 5 cycles
          CFI FunCall _RpBbTim1Hdr
        ; ------------------------------------- Block: 5 cycles
//  371 			{
//  372 				RpBbTim1Hdr();
        CALL      F:_RpBbTim1Hdr     ;; 3 cycles
//  373 				ireq &= (~RP_BBTIM1_IREQ);
        MOVW      AX, [SP]           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFD           ;; 1 cycle
        BR        R:??RpIntpHdr_10   ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  374 			}
//  375 			else if (ireq & RP_BBMODESW_IREQ)
??RpAckCheckCallback_29:
        INCW      HL                 ;; 1 cycle
        BF        [HL].6, ??RpAckCheckCallback_30  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  376 			{
//  377 				RpRcvModeswHdr();
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??RpAckCheckCallback_31  ;; 4 cycles
          CFI FunCall _RpAvailableRcvRamEnable
        ; ------------------------------------- Block: 13 cycles
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].0, ??RpAckCheckCallback_31  ;; 5 cycles
          CFI FunCall _RpRxOnStart
        ; ------------------------------------- Block: 12 cycles
        CALL      F:_RpRxOnStart     ;; 3 cycles
          CFI FunCall _RpAntSelAssist_ReStartProc
        CALL      F:_RpAntSelAssist_ReStartProc  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  378 				ireq &= (~(RP_BBMODESW_IREQ | RP_BBTIM2_IREQ));
??RpAckCheckCallback_31:
        MOVW      AX, #0xFFFF        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, #0xBFFB        ;; 1 cycle
        BR        R:??RpIntpHdr_2    ;; 3 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 6 cycles
//  379 			}
//  380 			else if (ireq & RP_BBRCVLVL_IREQ)
??RpAckCheckCallback_30:
        BT        [HL].7, $+7        ;; 5 cycles
        BR        F:??RpIntpHdr_0    ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  381 			{
//  382 				RpRcvLvlIntHdr();
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOV       B, ES:_RpCb+2      ;; 2 cycles
        XCH       A, B               ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??RpAckCheckCallback_32  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].1, ??RpAckCheckCallback_33  ;; 5 cycles
          CFI FunCall _RpSetStateRxOnToTrxOff
        ; ------------------------------------- Block: 8 cycles
        CALL      F:_RpSetStateRxOnToTrxOff  ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+2         ;; 2 cycles
        MOV       X, #0x5            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
        MOVW      HL, #LWRD(_RpCb+304)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall
        CALL      AX                 ;; 3 cycles
        BR        S:??RpAckCheckCallback_32  ;; 3 cycles
          CFI FunCall _RpAvailableRcvRamEnable
        ; ------------------------------------- Block: 24 cycles
??RpAckCheckCallback_33:
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
          CFI FunCall _RpAntSelAssist_ReStartProc
        CALL      F:_RpAntSelAssist_ReStartProc  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
//  383 				ireq &= (~(RP_BBRCVLVL_IREQ | RP_BBTIM2_IREQ));
??RpAckCheckCallback_32:
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, #0x7FFB        ;; 1 cycle
        BR        R:??RpIntpHdr_2    ;; 3 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 6 cycles
//  384 			}
//  385 			else
//  386 			{
//  387 				ireq = RP_BB_NO_IREQ;
//  388 			}
//  389 		}
//  390 	}
//  391 }
??RpAckCheckCallback_0:
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 990 cycles
//  392 
//  393 /* "Interrupt source number 1" */
//  394 /***************************************************************************************************************
//  395  * function name  : RpBbTim0Hdr
//  396  * description    : Timer compare 0 interrupt handler
//  397  * parameters     : none
//  398  * return value   : none
//  399  **************************************************************************************************************/
//  400 static void RpBbTim0Hdr( void )
//  401 {
//  402 	uint16_t	status;
//  403 
//  404 	RpCb.reg.bbIntEn[0] &= ~TIM0INTEN;
//  405 	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
//  406 	status = RpCb.status;
//  407 	if (status & RP_PHY_STAT_WAIT_TMRTRG)
//  408 	{
//  409 		RpCb.status &= (~RP_PHY_STAT_WAIT_TMRTRG);
//  410 		if (status & RP_PHY_STAT_TX)
//  411 		{
//  412 			RpCb.status |= RP_PHY_STAT_BUSY;
//  413 		}
//  414 		else if (status & RP_PHY_STAT_RX)
//  415 		{
//  416 			RpAvailableRcvRamEnable();
//  417 			RpRxOnStart();	// RX trigger
//  418 		}
//  419 	}
//  420 }
//  421 
//  422 /* "Interrupt source number 2" */
//  423 /***************************************************************************************************************
//  424  * function name  : RpBbTim1Hdr
//  425  * description    : Timer compare 1 interrupt handler
//  426  * parameters     : none
//  427  * return value   : none
//  428  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _RpBbTim1Hdr
        CODE
//  429 static void RpBbTim1Hdr( void )
//  430 {
_RpBbTim1Hdr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  431 	uint16_t	status;
//  432 
//  433 	RpCb.reg.bbIntEn[0] &= ~TIM1INTEN;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+344.1     ;; 3 cycles
//  434 	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  435 	status = RpCb.status;
//  436 
//  437 	if (status & RP_PHY_STAT_RX)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        AND       A, #0x4            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        CMPW      AX, #0x401         ;; 1 cycle
        BNZ       ??RpAckCheckCallback_34  ;; 4 cycles
          CFI FunCall _RpRxOffBeforeReplyingAck
        ; ------------------------------------- Block: 22 cycles
//  438 	{
//  439 		if (status & RP_PHY_STAT_RX_TMOUT)
//  440 		{
//  441 			if (RpRxOffBeforeReplyingAck() == RP_FALSE)
        CALL      F:_RpRxOffBeforeReplyingAck  ;; 3 cycles
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_35  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  442 			{
//  443 				RpCb.statusRxTimeout |= RP_STATUS_RX_TIMEOUT_TX;		// just only sign force-RxOff here
        MOVW      HL, #LWRD(_RpCb+2)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:[HL].0          ;; 3 cycles
        RET                          ;; 6 cycles
          CFI FunCall _RpSetStateRxOnToTrxOff
        ; ------------------------------------- Block: 11 cycles
//  444 			}
//  445 			else
//  446 			{
//  447 				RpSetStateRxOnToTrxOff();
??RpAckCheckCallback_35:
        CALL      F:_RpSetStateRxOnToTrxOff  ;; 3 cycles
//  448 				RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+2         ;; 2 cycles
//  449 
//  450 				/* Callback function execution Log */
//  451 				RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
        MOV       X, #0x5            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  452 				/* Callback function execution */
//  453 				INDIRECT_RpRxOffIndCallback();
        MOVW      HL, #LWRD(_RpCb+304)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall
        CALL      AX                 ;; 3 cycles
        ; ------------------------------------- Block: 21 cycles
//  454 			}
//  455 		}
//  456 	}
//  457 }
??RpAckCheckCallback_34:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 68 cycles
//  458 
//  459 /* "Interrupt source number 4" */
//  460 /***************************************************************************************************************
//  461  * function name  : RpTrnFinHdr
//  462  * description    : Frame transmit completion interrupt handler
//  463  * parameters     : none
//  464  * return value   : none
//  465  **************************************************************************************************************/
//  466 static void RpTrnFinHdr( void )
//  467 {
//  468 	RpTrnHdrFunc();
//  469 }
//  470 
//  471 /***********************************************************************
//  472  * function name  : RpChangeDefaultFilter
//  473  * parameters     : none
//  474  * return value   : none
//  475  * description    : 
//  476  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon0
          CFI Function _RpChangeDefaultFilter
        CODE
//  477 void RpChangeDefaultFilter( void )
//  478 {
_RpChangeDefaultFilter:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 14
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+18
//  479 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+342    ;; 2 cycles
//  480 	uint8_t antDivRxEna = RpCb.pib.phyAntennaDiversityRxEna;
//  481 	uint8_t fecRxEna = RpCb.pib.phyFskFecRxEna;
        MOV       A, ES:_RpCb+164    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
//  482 	uint8_t arrayChar[3];
//  483 
//  484 	// filter change case
//  485 	arrayChar[0] = (antDivRxEna == RP_TRUE)? (uint8_t)RpFreqBandTbl[index][RP_0x042D_ANDVON_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x042D_OFFSET];
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        CMP       ES:_RpCb+226, #0x1  ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BNZ       ??RpAckCheckCallback_36  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
        ADDW      AX, #0x16          ;; 1 cycle
        BR        S:??RpAckCheckCallback_37  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_36:
        ADDW      AX, #0x15          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_37:
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
//  486 	RpRegBlockWrite((0x042D << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 1);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2168        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
//  487 
//  488 	RpRegWrite((0x0430 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0430_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2180        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  489 	RpRegWrite((0x0432 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0432_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2190        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  490 	RpRegWrite((0x0436 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0436_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x21B0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  491 	RpRegWrite((0x043A << 3), (uint8_t)RpFreqBandTbl[index][RP_0x043A_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x21D0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  492 	RpRegWrite((0x0456 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0456_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x21          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x22B0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  493 
//  494 	arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0486_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0486_OFFSET];
        MOV       A, [SP+0x06]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        CMP       A, #0x1            ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        BNZ       ??RpAckCheckCallback_38  ;; 4 cycles
        ; ------------------------------------- Block: 90 cycles
        ADDW      AX, #0x2F          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        BR        S:??RpAckCheckCallback_39  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
??RpAckCheckCallback_38:
        ADDW      AX, #0x30          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x33          ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
??RpAckCheckCallback_39:
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x07], A       ;; 1 cycle
//  495 	arrayChar[1] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0487_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0487_OFFSET];
//  496 	arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x0488_OFFSET];
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x35          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
//  497 	RpRegBlockWrite((0x0486 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2430        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
//  498 
//  499 	RpRegWrite((0x04F4 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x04F4_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x27A0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  500 	RpRegWrite((0x0505 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0505_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x40          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2828        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  501 	RpRegWrite((0x0580 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0580_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x44          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2C00        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  502 }
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 62 cycles
        ; ------------------------------------- Total: 200 cycles
//  503 
//  504 /***********************************************************************
//  505  * function name  : RpChangeNarrowBandFilter
//  506  * parameters     : none
//  507  * return value   : none
//  508  * description    : 
//  509  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon0
          CFI Function _RpChangeNarrowBandFilter
        CODE
//  510 void RpChangeNarrowBandFilter( void )
//  511 {
_RpChangeNarrowBandFilter:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 8
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
//  512 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+342    ;; 2 cycles
//  513 	uint8_t arrayChar[3];
//  514 
//  515 	// filter change case
//  516 	RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_DIGICCA_OFFSET]);
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x17          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2168        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  517 	RpRegWrite((0x0430 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0430_DIGICCA_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x19          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2180        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  518 	RpRegWrite((0x0432 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0432_DIGICCA_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x1B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2190        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  519 	RpRegWrite((0x0436 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0436_DIGICCA_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x1D          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x21B0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  520 	RpRegWrite((0x043A << 3), (uint8_t)RpFreqBandTbl[index][RP_0x043A_DIGICCA_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x1F          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x21D0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  521 	RpRegWrite((0x0456 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0456_DIGICCA_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x22B0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  522 
//  523 	arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x0486_DIGICCA_OFFSET];
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x31          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
//  524 	arrayChar[1] = (uint8_t)RpFreqBandTbl[index][RP_0x0487_DIGICCA_OFFSET];
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
//  525 	arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x0488_DIGICCA_OFFSET];
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
//  526 	RpRegBlockWrite((0x0486 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2430        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
//  527 
//  528 	RpRegWrite((0x04F4 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x04F4_DIGICCA_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x3D          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x27A0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  529 	RpRegWrite((0x0505 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0505_DIGICCA_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x41          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2828        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  530 	RpRegWrite((0x0580 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0580_DIGICCA_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x45          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2C00        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  531 }
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 158 cycles
        ; ------------------------------------- Total: 158 cycles
//  532 
//  533 /***********************************************************************
//  534  * function name  : RpChangeFilter
//  535  * parameters     : none
//  536  * return value   : none
//  537  * description    : 
//  538  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon0
          CFI Function _RpChangeFilter
        CODE
//  539 void RpChangeFilter( void )
//  540 {
_RpChangeFilter:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
//  541 	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+246    ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
//  542 	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
        MOV       A, ES:_RpCb+247    ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
//  543 	uint8_t reg488h;
//  544 
//  545 	// filter change case
//  546 	if ((ccaBandwidth != RP_PHY_CCA_BANDWIDTH_INDEX_225K)
//  547 		|| (edBandwidth != RP_PHY_CCA_BANDWIDTH_INDEX_225K))
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_40  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_41  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  548 	{
//  549 		reg488h = RpRegRead(0x0488<<3);
//  550 		if (reg488h != RP_PHY_REG488H_DEFAULT)
??RpAckCheckCallback_40:
        MOVW      AX, #0x2440        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        CMP       A, #0x82           ;; 1 cycle
        BZ        ??RpAckCheckCallback_41  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
//  551 		{
//  552 			if ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW)
//  553 				|| (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW))
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BZ        ??RpAckCheckCallback_42  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_43  ;; 4 cycles
          CFI FunCall _RpChangeDefaultFilter
        ; ------------------------------------- Block: 6 cycles
//  554 			{
//  555 				RpChangeDefaultFilter();
??RpAckCheckCallback_42:
        CALL      F:_RpChangeDefaultFilter  ;; 3 cycles
        BR        S:??RpAckCheckCallback_41  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth225k
        ; ------------------------------------- Block: 6 cycles
//  556 			}
//  557 			else
//  558 			{
//  559 				RpRegCcaBandwidth225k();
??RpAckCheckCallback_43:
        CALL      F:_RpRegCcaBandwidth225k  ;; 3 cycles
//  560 				RpRegAdcVgaDefault();
          CFI FunCall _RpRegAdcVgaDefault
        CALL      F:_RpRegAdcVgaDefault  ;; 3 cycles
//  561 				RpRegRxDataRateDefault();
          CFI FunCall _RpRegRxDataRateDefault
        CALL      F:_RpRegRxDataRateDefault  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  562 			}
//  563 		}
//  564 	}
//  565 //#ifdef RP_NARROW_CCA
//  566 }
??RpAckCheckCallback_41:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 63 cycles
//  567 
//  568 /***********************************************************************
//  569  * function name  : RpSetAntennaDiversityModeReg
//  570  * parameters     : none
//  571  * return value   : none
//  572  * description    : 
//  573  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon0
          CFI Function _RpSetAntennaDiversityModeReg
        CODE
//  574 static void RpSetAntennaDiversityModeReg( void )
//  575 {
_RpSetAntennaDiversityModeReg:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  576 	uint8_t antDvrCon;
//  577 
//  578 	if (RpCb.pib.phyAntennaDiversityRxEna)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+226       ;; 2 cycles
        BZ        ??RpAckCheckCallback_44  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  579 	{
//  580 		antDvrCon = RpRegRead(BBANTDIV);
        MOVW      AX, #0x5A0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
//  581 		antDvrCon &= ~ANTSWTRNSET;
        AND       A, #0xFD           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
//  582 		if (RpCb.rx.bkupAckAnt)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+147       ;; 2 cycles
        BZ        ??RpAckCheckCallback_45  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
//  583 		{
//  584 			antDvrCon |= ANTSWTRNSET;
        OR        A, #0x2            ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  585 		}
//  586 		RpRegWrite(BBANTDIV, antDvrCon);
??RpAckCheckCallback_45:
        MOVW      AX, #0x5A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  587 	}
//  588 }
??RpAckCheckCallback_44:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 32 cycles
//  589 
//  590 /***********************************************************************
//  591  * function name  : RpSetNumberOfCrcBitSwitchBit
//  592  * parameters     : none
//  593  * return value   : none
//  594  * description    : 
//  595  **********************************************************************/
//  596 static void RpSetNumberOfCrcBitSwitchBit( void )
//  597 {
//  598 	if (RpCb.rx.fcsLength != RpCb.pib.phyFcsLength)
//  599 	{
//  600 		RpSetFcsLengthVal();
//  601 	}
//  602 }
//  603 
//  604 /***********************************************************************
//  605  * function name  : RpGetAntennaDiversityRssi
//  606  * parameters     : none
//  607  * return value   : none
//  608  * description    : 
//  609  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon0
          CFI Function _RpGetAntennaDiversityRssi
        CODE
//  610 static void RpGetAntennaDiversityRssi( void )
//  611 {
_RpGetAntennaDiversityRssi:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  612 	RpCb.rx.rssi0 = 0x0100;
        MOVW      AX, #0x100         ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+130, AX   ;; 2 cycles
//  613 	RpCb.rx.rssi1 = 0x0100;
        MOVW      ES:_RpCb+132, AX   ;; 2 cycles
//  614 
//  615 	if (RpCb.pib.phyAntennaDiversityRxEna)
        CMP0      ES:_RpCb+226       ;; 2 cycles
        BZ        ??RpAckCheckCallback_46  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  616 	{
//  617 		RpRegBlockRead(BBANT0RD, (uint8_t *)&RpCb.rx.rssi0, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpCb+130)  ;; 1 cycle
        MOV       C, #BYTE3(_RpCb)   ;; 1 cycle
        MOVW      AX, #0x640         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
//  618 		RpRegBlockRead(BBANT1RD, (uint8_t *)&RpCb.rx.rssi1, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, #LWRD(_RpCb+132)  ;; 1 cycle
        MOV       C, #BYTE3(_RpCb)   ;; 1 cycle
        MOVW      AX, #0x650         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 17 cycles
//  619 	}
//  620 }
??RpAckCheckCallback_46:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 35 cycles
//  621 
//  622 /***********************************************************************
//  623  * function name  : RpGetSelectedAntennaInfo
//  624  * parameters     : status...phy status
//  625  * return value   : none
//  626  * description    : 
//  627  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon0
          CFI Function _RpGetSelectedAntennaInfo
        CODE
//  628 static void RpGetSelectedAntennaInfo( uint16_t status )
//  629 {
_RpGetSelectedAntennaInfo:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
//  630 	if (RpCb.pib.phyAntennaDiversityRxEna)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+226       ;; 2 cycles
        BZ        ??RpAckCheckCallback_47  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  631 	{
//  632 		if (RpCb.rx.softwareAdf == RP_FALSE || ((status & RP_PHY_STAT_TX) == 0))
        CMP0      ES:_RpCb+143       ;; 2 cycles
        BZ        ??RpAckCheckCallback_48  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].1, ??RpAckCheckCallback_49  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
//  633 		{
//  634 			RpCb.rx.selectedAntenna = RpDetRcvdAntenna();
??RpAckCheckCallback_48:
        MOVW      AX, #0x870         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
//  635 		}
//  636 	}
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV1      CY, [HL].2         ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ROLC      A, 0x1             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+128, A    ;; 2 cycles
        BR        S:??RpAckCheckCallback_49  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
//  637 	else
//  638 	{
//  639 		RpCb.rx.selectedAntenna = 0;
??RpAckCheckCallback_47:
        CLRB      ES:_RpCb+128       ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
//  640 	}
//  641 }
??RpAckCheckCallback_49:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 46 cycles
//  642 
//  643 /***********************************************************************
//  644  * function name  : RpValidityCheckSFDTimeStamp
//  645  * parameters     : none
//  646  * return value   : none
//  647  * description    : 
//  648  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon0
          CFI Function _RpValidityCheckSFDTimeStamp
          CFI NoCalls
        CODE
//  649 static void RpValidityCheckSFDTimeStamp( void )
//  650 {
_RpValidityCheckSFDTimeStamp:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  651 	if (RpCb.rx.softwareAdf == RP_FALSE)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+143       ;; 2 cycles
        BNZ       ??RpAckCheckCallback_50  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  652 	{
//  653 		if ((RpCb.pib.macAddressFilter1Ena == RP_TRUE)
//  654 			||(RpCb.pib.macAddressFilter2Ena == RP_TRUE))
        CMP       ES:_RpCb+176, #0x1  ;; 2 cycles
        BZ        ??RpAckCheckCallback_51  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpCb+192, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_50  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  655 		{
//  656 			RpCb.rx.timeStamp = RP_NULL;
??RpAckCheckCallback_51:
        MOVW      HL, #LWRD(_RpCb+116)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 6 cycles
//  657 		}
//  658 	}
//  659 
//  660 	if (RpCb.pib.phyFskFecRxEna != RP_FEC_RX_MODE_DISABLE)
??RpAckCheckCallback_50:
        CMP0      ES:_RpCb+164       ;; 2 cycles
        BZ        ??RpAckCheckCallback_52  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  661 	{
//  662 		RpCb.rx.timeStamp = RP_NULL;
        MOVW      HL, #LWRD(_RpCb+116)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 6 cycles
//  663 	}
//  664 
//  665 	switch (RpCb.freqIdIndex)
??RpAckCheckCallback_52:
        MOV       A, ES:_RpCb+342    ;; 2 cycles
        SUB       A, #0x12           ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        SKC                          ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 6 cycles
//  666 	{
//  667 		case RP_PHY_200K_M03:
//  668 		case RP_PHY_400K_M03:
//  669 			RpCb.rx.timeStamp = RP_NULL;
??RpValidityCheckSFDTimeStamp_0:
        MOVW      HL, #LWRD(_RpCb+116)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  670 			break;
//  671 		default:
//  672 			break;
//  673 	}
//  674 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 60 cycles
//  675 
//  676 /***********************************************************************
//  677  * function name  : RpPdDataIndCallbackPrc
//  678  * parameters     : pBuf...
//  679  *				  : stOptInd...
//  680  *				  : warningIndCallbackFlag...
//  681  * return value   : none
//  682  * description    : 
//  683  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon1
          CFI Function _RpPdDataIndCallbackPrc
        CODE
//  684 static void RpPdDataIndCallbackPrc( uint8_t *pBuf, uint8_t stOptInd, uint8_t warningIndCallbackFlag )
//  685 {
_RpPdDataIndCallbackPrc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 8
//  686 	/* Callback function execution Log */
//  687 	RpLog_Event( RP_LOG_CB, RP_LOG_CB_CALCLQI );
        MOV       X, #0x6            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  688 	/* Callback function execution */
//  689 	RpCb.rx.lqi = INDIRECT_RpCalcLqiCallback( RpCb.rx.rssi, RP_TRUE );
        ONEB      C                  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+126   ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+316)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
          CFI FunCall
        CALL      DE                 ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+124, A    ;; 2 cycles
//  690 
//  691 #ifdef R_FSB_FAN_ENABLED
//  692 	if ( RpFsb.enable == RP_FALSE )
//  693 	{
//  694 #endif // #ifdef R_FSB_FAN_ENABLED
//  695 
//  696 		/* Callback function execution Log */
//  697 		RpLog_Event( RP_LOG_CB, RP_LOG_CB_DATAIND );
        ONEB      X                  ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  698 		/* Callback function execution */
//  699 		INDIRECT_RpPdDataIndCallback( pBuf, RpCb.rx.lenNoFcs, RpCb.rx.timeStamp, RpCb.rx.lqi,
//  700 									 RpCb.rx.rssi, RpCb.rx.selectedAntenna, RpCb.rx.rssi0, RpCb.rx.rssi1,
//  701 									 stOptInd, RpCb.rx.filteredAdress, RpCb.rx.phrRx );
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+148    ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, ES:_RpCb+137    ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, ES:_RpCb+132   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, ES:_RpCb+130   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       X, ES:_RpCb+128    ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, ES:_RpCb+126   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      HL, #LWRD(_RpCb+116)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOV       X, ES:_RpCb+124    ;; 2 cycles
        MOVW      BC, ES:_RpCb+108   ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        MOV       H, A               ;; 1 cycle
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, H               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      HL, #LWRD(_RpCb+288)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
        POP       AX                 ;; 1 cycle
          CFI CFA SP+34
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+30
          CFI FunCall
        CALL      HL                 ;; 3 cycles
//  702 #ifdef R_FSB_FAN_ENABLED
//  703 	}
//  704 	else
//  705 	{
//  706 		R_FSB_UplinkCallback( pBuf, RpCb.rx.lenNoFcs, RpCb.rx.lqi );
//  707 		RpRelRxBuf( pBuf );
//  708 		RpFsb.enable = RP_FALSE;
//  709 	}
//  710 #endif // #ifdef R_FSB_FAN_ENABLED
//  711 
//  712 	if (warningIndCallbackFlag == RP_TRUE)
        ADDW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_53  ;; 4 cycles
          CFI FunCall _RpGetRxBuf
        ; ------------------------------------- Block: 106 cycles
//  713 	{
//  714 		RpCb.rx.pTopRxBuf = RpGetRxBuf();
        CALL      F:_RpGetRxBuf      ;; 3 cycles
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  715 		if (RpCb.rx.pTopRxBuf == RP_NULL)
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_54  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpAckCheckCallback_54:
        BNZ       ??RpAckCheckCallback_53  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  716 		{
//  717 			/* Callback function execution Log */
//  718 			RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
        MOV       X, #0xB            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  719 			/* Callback function execution */
//  720 			INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );
        MOVW      HL, #LWRD(_RpCb+312)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x21           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
//  721 
//  722 			RpRfStat.failToGetRxBuf++;
        MOVW      HL, #LWRD(_RpRfStat+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRfStat)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  723 			RpCb.rx.waitRelRxBufWhenAutoRx = RP_TRUE;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        ONEB      ES:_RpCb+144       ;; 2 cycles
        ; ------------------------------------- Block: 38 cycles
//  724 		}
//  725 	}
//  726 }
??RpAckCheckCallback_53:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 176 cycles
//  727 
//  728 /******************************************************************************
//  729 Function Name:       RpTrnHdrFunc
//  730 Parameters:          none
//  731 Return value:        none
//  732 Description:         trn interrupt function.
//  733 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon0
          CFI Function _RpTrnHdrFunc
        CODE
//  734 static void RpTrnHdrFunc( void )
//  735 {
_RpTrnHdrFunc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 18
        SUBW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+22
//  736 	uint16_t	status;
//  737 	uint8_t stCfm = RP_SUCCESS, framePend = RP_NO_FRMPENBIT_ACK;
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x03], A       ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x0B], A       ;; 1 cycle
//  738 	uint8_t trnTimes = 0;
        MOV       [SP+0x0A], A       ;; 1 cycle
//  739 	uint8_t txrxst0, comstat3, ccaTotal;
//  740 	uint16_t	sduLen;
//  741 	uint8_t interruptDisable = RP_FALSE;
        MOV       [SP+0x09], A       ;; 1 cycle
//  742 
//  743 	#if defined(__RX)
//  744 	uint32_t bkupPsw2 = 0;
//  745 	#elif defined(__arm)
//  746 	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
//  747 	#else
//  748 	uint8_t bkupPsw2 = 0;
        MOV       [SP+0x06], A       ;; 1 cycle
//  749 	#endif
//  750 
//  751 	uint8_t *pBuf;
//  752 	uint8_t warningIndCallbackFlag = RP_FALSE;
        MOV       [SP+0x08], A       ;; 1 cycle
//  753 	uint8_t stOptInd;
//  754 	uint8_t statusRxTimeout;
//  755 	uint8_t useRxFcsLength = RP_TRUE;
//  756 	uint8_t applyRegulatoryModeLimit = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x05], A       ;; 1 cycle
//  757 
//  758 	RpCb.reg.bbIntEn[0] &= (uint8_t)(~(TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN));
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+344    ;; 2 cycles
        AND       A, #0x47           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpCb+344, A    ;; 2 cycles
//  759 	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  760 	status = RpCb.status;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
//  761 	statusRxTimeout = RpCb.statusRxTimeout;
        MOV       A, ES:_RpCb+2      ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
//  762 
//  763 	if (status & RP_PHY_STAT_TX)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].1, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_55  ;; 5 cycles
        ; ------------------------------------- Block: 36 cycles
//  764 	{
//  765 		RpCancelTrn(status);
        MOVW      AX, [SP]           ;; 1 cycle
          CFI FunCall _RpCancelTrn
        CALL      F:_RpCancelTrn     ;; 3 cycles
//  766 		ccaTotal = RpRegRead(BBCCATOTAL);
        MOVW      AX, #0x6B0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x07], A       ;; 1 cycle
//  767 
//  768 		if (RpCb.rx.onReplyingAck == RP_TRUE)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+142, #0x1  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_56  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
//  769 		{
//  770 			/** RpAfterTrnEnd (Start) **************************
//  771 			***************************************************/
//  772 			/* --- --- */
//  773 			RpCb.rx.onReplyingAck = RP_FALSE;
        CLRB      ES:_RpCb+142       ;; 2 cycles
//  774 
//  775 			/* enable phyAckWithCca? */
//  776 			if ( RpCcaAck.enable )
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        CMP0      ES:_RpCcaAck       ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpEndTransmitWithCca
        ; ------------------------------------- Block: 6 cycles
//  777 			{
//  778 				RpEndTransmitWithCca();
        CALL      F:_RpEndTransmitWithCca  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  779 			}
//  780 
//  781 			/* Apply the limits for Regulatory Mode to Ack */
//  782 			if (RpCb.tx.ackLenForRegulatoryMode != 0)
??RpTrnHdrFunc_0:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+102   ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_57  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  783 			{
//  784 				/* Check Tx Failure */
//  785 				if (status & RP_PHY_STAT_TX_CCA)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].7, ??RpAckCheckCallback_58  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
//  786 				{
//  787 					txrxst0 = RpRegRead(BBTXRXST0);
        MOVW      AX, #0x38          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
//  788 					/* CSMA-CA NG */
//  789 					if (txrxst0 & CSMACA)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].2, ??RpAckCheckCallback_58  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
//  790 					{
//  791 						applyRegulatoryModeLimit = RP_FALSE;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x05], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  792 					}
//  793 				}
//  794 #ifdef R_FSB_FAN_ENABLED
//  795 				if (RpCb.tx.isFsbDownlink == RP_TRUE)
//  796 				{
//  797 					useRxFcsLength = RP_FALSE;
//  798 					RpCb.tx.isFsbDownlink = RP_FALSE;
//  799 				}
//  800 #endif // #ifdef R_FSB_FAN_ENABLED
//  801 
//  802 				/* ToffMin Limit */
//  803 				if (RpCb.tx.applyAckToffMin == RP_TRUE)
??RpAckCheckCallback_58:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+105, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_59  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  804 				{
//  805 					if (applyRegulatoryModeLimit == RP_TRUE)
        MOV       A, [SP+0x05]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_60  ;; 4 cycles
          CFI FunCall _RpGetTime
        ; ------------------------------------- Block: 6 cycles
//  806 					{
//  807 						RpCb.tx.sentTime = RpGetTime();
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      HL, #LWRD(_RpCb+82)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  808 						RpCb.tx.sentElapsedTimeS = RpCb.tx.regulatoryModeElapsedTimeS;
        MOVW      HL, #LWRD(_RpCb+94)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+90)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  809 						RpCb.tx.needTxWaitRegulatoryMode = RP_TRUE;
        ONEB      ES:_RpCb+88        ;; 2 cycles
//  810 						if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        CMP       ES:_RpCb+235, #0x2  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_60  ;; 4 cycles
        ; ------------------------------------- Block: 30 cycles
        CMP       ES:_RpCb+234, #0x9  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_60  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  811 						{
//  812 							RpCb.tx.sentLen = RpCalcTotalBytes(RpCb.tx.ackLenForRegulatoryMode, &sduLen, useRxFcsLength);
        ONEB      B                  ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, ES:_RpCb+102   ;; 2 cycles
          CFI FunCall _RpCalcTotalBytes
        CALL      F:_RpCalcTotalBytes  ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+80, AX    ;; 2 cycles
        ; ------------------------------------- Block: 13 cycles
//  813 						}
//  814 					}
//  815 					RpCb.tx.applyAckToffMin = RP_FALSE;
??RpAckCheckCallback_60:
        CLRB      ES:_RpCb+105       ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
//  816 				}
//  817 
//  818 				/* Total Tx time limit */
//  819 				if (RpCb.tx.applyAckTotalTxTime == RP_TRUE)
??RpAckCheckCallback_59:
        CMP       ES:_RpCb+104, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_61  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  820 				{
//  821 					if (applyRegulatoryModeLimit == RP_TRUE)
        MOV       A, [SP+0x05]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_62  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  822 					{
//  823 						RpUpdateTxTime(1, RP_TRUE, useRxFcsLength);
        ONEB      C                  ;; 1 cycle
        ONEB      X                  ;; 1 cycle
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpUpdateTxTime
        CALL      F:_RpUpdateTxTime  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  824 					}
//  825 					RpCb.tx.applyAckTotalTxTime = RP_FALSE;
??RpAckCheckCallback_62:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+104       ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
//  826 				}
//  827 
//  828 				RpCb.tx.ackLenForRegulatoryMode = 0;
??RpAckCheckCallback_61:
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_RpCb+102, AX   ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
//  829 			}
//  830 
//  831 			if (RpCb.tx.onCsmaCa == RP_TRUE)	// backoffRx
??RpAckCheckCallback_57:
        CMP       ES:_RpCb+86, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_63  ;; 4 cycles
          CFI FunCall _RpSetAntennaDiversityModeReg
        ; ------------------------------------- Block: 6 cycles
//  832 			{
//  833 				RpSetAntennaDiversityModeReg();
        CALL      F:_RpSetAntennaDiversityModeReg  ;; 3 cycles
//  834 				RpSetNumberOfCrcBitSwitchBit();	// for TX
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+134    ;; 2 cycles
        MOV       A, ES:_RpCb+219    ;; 2 cycles
        CMP       X, A               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetFcsLengthVal
        ; ------------------------------------- Block: 10 cycles
        CALL      F:_RpSetFcsLengthVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  835 			
//  836 				RpResumeCsmaTrn(status, ccaTotal, RP_TRUE);
??RpTrnHdrFunc_1:
        ONEB      B                  ;; 1 cycle
        MOV       A, [SP+0x07]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
          CFI FunCall _RpResumeCsmaTrn
        CALL      F:_RpResumeCsmaTrn  ;; 3 cycles
        BR        S:??RpAckCheckCallback_64  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
//  837 			}
//  838 			else
//  839 			{
//  840 				if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
??RpAckCheckCallback_63:
        MOV       A, [SP+0x04]       ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_65  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  841 				{
//  842 					;
//  843 				}
//  844 				else
//  845 				{
//  846 					if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].4, ??RpAckCheckCallback_65  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
//  847 					{
//  848 						RpCb.status = RP_PHY_STAT_RX;
        ONEW      AX                 ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
//  849 						RpCb.status |= RP_PHY_STAT_TRX_TO_RX_AUTO;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
//  850 
//  851 						if (status & RP_PHY_STAT_RX_TMOUT)
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].2, ??RpAckCheckCallback_66  ;; 5 cycles
        ; ------------------------------------- Block: 15 cycles
//  852 						{
//  853 							RpCb.status |= RP_PHY_STAT_RX_TMOUT;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].2          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  854 						}
//  855 
//  856 						RpInverseTxAnt(RP_TRUE);
??RpAckCheckCallback_66:
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
//  857 
//  858 						RpAvailableRcvRamEnable();
          CFI FunCall _RpAvailableRcvRamEnable
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
//  859 						RpChangeFilter();
          CFI FunCall _RpChangeFilter
        CALL      F:_RpChangeFilter  ;; 3 cycles
//  860 						RpSetSfdDetectionExtendWrite(RpCb.status);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
          CFI FunCall _RpSetSfdDetectionExtendWrite
        CALL      F:_RpSetSfdDetectionExtendWrite  ;; 3 cycles
//  861 
//  862 						/** Interrupt disable Start **/
//  863 
//  864 						/* Disable interrupt */
//  865 						#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  866 						RP_PHY_ALL_DI(bkupPsw2);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
//  867 						#else
//  868 						RP_PHY_ALL_DI();
//  869 						#endif
//  870 						interruptDisable = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
//  871 
//  872 						/* --- --- */
//  873 						RpRxOnStart();			// rxtrg  and xxxic
          CFI FunCall _RpRxOnStart
        CALL      F:_RpRxOnStart     ;; 3 cycles
//  874 
//  875 						/* Start Antenna select assist */
//  876 						RpAntSelAssist_StartProc();
          CFI FunCall _RpAntSelAssist_StartProc
        CALL      F:_RpAntSelAssist_StartProc  ;; 3 cycles
          CFI FunCall _RpSetAntennaDiversityModeReg
        ; ------------------------------------- Block: 40 cycles
//  877 					}
//  878 				}
//  879 				RpSetAntennaDiversityModeReg();
??RpAckCheckCallback_65:
        CALL      F:_RpSetAntennaDiversityModeReg  ;; 3 cycles
//  880 				RpSetNumberOfCrcBitSwitchBit(); // for TX
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+134    ;; 2 cycles
        MOV       A, ES:_RpCb+219    ;; 2 cycles
        CMP       X, A               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetFcsLengthVal
        ; ------------------------------------- Block: 10 cycles
        CALL      F:_RpSetFcsLengthVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  881 			}
//  882 
//  883 			/** RpDataIndCall (Start) **************************
//  884 			***************************************************/
//  885 			RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
??RpAckCheckCallback_64:
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
//  886 			pBuf = RpCb.rx.pTopRxBuf;
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x10], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x0E], AX      ;; 1 cycle
//  887 
//  888 			if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].4, ??RpAckCheckCallback_67  ;; 5 cycles
        ; ------------------------------------- Block: 19 cycles
//  889 			{
//  890 				if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
        MOV       A, [SP+0x04]       ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        BZ        ??RpAckCheckCallback_68  ;; 4 cycles
          CFI FunCall _RpSetStateRxOnToTrxOff
        ; ------------------------------------- Block: 6 cycles
//  891 				{
//  892 					RpSetStateRxOnToTrxOff();
        CALL      F:_RpSetStateRxOnToTrxOff  ;; 3 cycles
//  893 					RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+2         ;; 2 cycles
        BR        S:??RpAckCheckCallback_69  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  894 				}
//  895 				else
//  896 				{
//  897 					warningIndCallbackFlag = RP_TRUE;
//  898 				}
//  899 			}
//  900 			else if (status & RP_PHY_STAT_RXON_BACKOFF)
??RpAckCheckCallback_67:
        BF        [HL].0, ??RpAckCheckCallback_70  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  901 			{
//  902 				warningIndCallbackFlag = RP_TRUE;
??RpAckCheckCallback_68:
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        BR        S:??RpAckCheckCallback_69  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  903 			}
//  904 			else	// Single RX
//  905 			{
//  906 				RpCb.status = RP_PHY_STAT_TRX_OFF;
??RpAckCheckCallback_70:
        MOVW      AX, #0x10          ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
//  907 				if (status & RP_PHY_STAT_RX_TMOUT)
        BF        [HL].2, ??RpAckCheckCallback_69  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
//  908 				{
//  909 					RpCb.reg.bbIntEn[0] &= ~TIM1INTEN;
        CLR1      ES:_RpCb+344.1     ;; 3 cycles
//  910 					RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  911 				}
//  912 			}
//  913 
//  914 			/* --- --- */
//  915 			RpRegBlockRead(BBRSSICCARSLT, (uint8_t *)&RpCb.rx.rssi, sizeof(uint16_t));
??RpAckCheckCallback_69:
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      DE, #LWRD(_RpCb+126)  ;; 1 cycle
        MOV       C, #BYTE3(_RpCb)   ;; 1 cycle
        MOV       X, #0x20           ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
//  916 			RpCb.rx.rssi -= RpCb.pib.phyRssiOutputOffset;
//  917 			RpCb.rx.rssi &= 0x01FF;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+282    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, ES:_RpCb+126   ;; 2 cycles
        SUBW      AX, HL             ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        MOVW      ES:_RpCb+126, AX   ;; 2 cycles
//  918 
//  919 			RpGetAntennaDiversityRssi();
          CFI FunCall _RpGetAntennaDiversityRssi
        CALL      F:_RpGetAntennaDiversityRssi  ;; 3 cycles
//  920 			RpGetSelectedAntennaInfo(status);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpGetSelectedAntennaInfo
        CALL      F:_RpGetSelectedAntennaInfo  ;; 3 cycles
//  921 			RpValidityCheckSFDTimeStamp();
          CFI FunCall _RpValidityCheckSFDTimeStamp
        CALL      F:_RpValidityCheckSFDTimeStamp  ;; 3 cycles
//  922 
//  923 			/** Interrupt disable End **/
//  924 			if (interruptDisable == RP_TRUE)
        POP       AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x9           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_71  ;; 5 cycles
        ; ------------------------------------- Block: 38 cycles
//  925 			{
//  926 				/* Enable interrupt */
//  927 				#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  928 				RP_PHY_ALL_EI(bkupPsw2);
        MOV       A, [SP+0x06]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  929 				#else
//  930 				RP_PHY_ALL_EI();
//  931 				#endif
//  932 			}
//  933 
//  934 			stOptInd = (uint8_t)((RpCb.tx.ackData[0] & 0x10) ? RP_FRMPENBIT_ACK : RP_NO_FRMPENBIT_ACK);
??RpAckCheckCallback_71:
        MOVW      HL, #LWRD(_RpCb+12)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        BF        ES:[HL].4, ??RpAckCheckCallback_72  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       X, #0x4            ;; 1 cycle
        BR        S:??RpAckCheckCallback_73  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_72:
        CLRB      X                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  935 
//  936 			if ( RpCcaAck.enable )
??RpAckCheckCallback_73:
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        CMP0      ES:_RpCcaAck       ;; 2 cycles
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
//  937 			{
//  938 				stOptInd = RpCcaAck.result;
        MOV       X, ES:_RpCcaAck+1  ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
//  939 			}
//  940 
//  941 			RpPdDataIndCallbackPrc(pBuf, stOptInd, warningIndCallbackFlag);
??RpTrnHdrFunc_2:
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall _RpPdDataIndCallbackPrc
        CALL      F:_RpPdDataIndCallbackPrc  ;; 3 cycles
//  942 
//  943 			if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
        MOV       A, [SP+0x04]       ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_74  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
//  944 			{
//  945 				if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].4, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_74  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
//  946 				{
//  947 					/* Callback function execution Log */
//  948 					RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
        MOV       X, #0x5            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  949 					/* Callback function execution */
//  950 					INDIRECT_RpRxOffIndCallback();
        MOVW      HL, #LWRD(_RpCb+304)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall
        CALL      AX                 ;; 3 cycles
        BR        R:??RpAckCheckCallback_74  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
//  951 				}
//  952 			}
//  953 			/** RpDataIndCall (End) ****************************
//  954 			***************************************************/
//  955 		}
//  956 		else
//  957 		{
//  958 			comstat3 = RpRegRead(BBCOMSTATE3);
??RpAckCheckCallback_56:
        MOVW      AX, #0x378         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
//  959 			txrxst0 = RpRegRead(BBTXRXST0);
        MOVW      AX, #0x38          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
//  960 			if (status & RP_PHY_STAT_TX_CCA)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].7, ??RpAckCheckCallback_75  ;; 5 cycles
        ; ------------------------------------- Block: 17 cycles
//  961 			{
//  962 				if (status & RP_PHY_STAT_CCA)
        BF        [HL].2, ??RpAckCheckCallback_76  ;; 5 cycles
          CFI FunCall _RpEndTransmitWithCca
        ; ------------------------------------- Block: 5 cycles
//  963 				{
//  964 					RpEndTransmitWithCca();
        CALL      F:_RpEndTransmitWithCca  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  965 				}
//  966 
//  967 				RpCb.tx.ccaTimes += ccaTotal;
??RpAckCheckCallback_76:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+67     ;; 2 cycles
        MOV       A, [SP+0x07]       ;; 1 cycle
        ADD       A, X               ;; 1 cycle
        MOV       ES:_RpCb+67, A     ;; 2 cycles
        ; ------------------------------------- Block: 7 cycles
//  968 			}
//  969 			if ((status & RP_PHY_STAT_TX_CCA) && (txrxst0 & CSMACA))
??RpAckCheckCallback_75:
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].7, ??RpAckCheckCallback_77  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].2, ??RpAckCheckCallback_77  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
//  970 			{
//  971 				// csmaca NG
//  972 				if ((RpCb.pib.phyProfileSpecificMode == RP_SPECIFIC_WSUN) &&
//  973 						(RpDriverRetryTrn(status) == RP_TRUE))
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+224, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_78  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      AX, [SP]           ;; 1 cycle
          CFI FunCall _RpDriverRetryTrn
        CALL      F:_RpDriverRetryTrn  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_55  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
//  974 				{
//  975 					return;
//  976 				}
//  977 				else
//  978 				{
//  979 					stCfm = RP_BUSY;	// busy channel
??RpAckCheckCallback_78:
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x03], A       ;; 1 cycle
        BR        R:??RpAckCheckCallback_79  ;; 3 cycles
          CFI FunCall _RpGetTime
        ; ------------------------------------- Block: 5 cycles
//  980 				}
//  981 			}
//  982 			else
//  983 			{
//  984 				RpCb.tx.sentTime =  RpGetTime();
??RpAckCheckCallback_77:
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      HL, #LWRD(_RpCb+82)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  985 				RpCb.tx.sentLen = RpCalcTotalBytes((uint16_t)(RpCb.tx.len), &sduLen, RP_FALSE);
        CLRB      B                  ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, ES:_RpCb+4     ;; 2 cycles
          CFI FunCall _RpCalcTotalBytes
        CALL      F:_RpCalcTotalBytes  ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+80, AX    ;; 2 cycles
//  986 
//  987 				if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
//  988 					((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
        CMP       ES:_RpCb+235, #0x1  ;; 2 cycles
        BZ        ??RpAckCheckCallback_80  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
        CMP       ES:_RpCb+235, #0x2  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_81  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpCb+234, #0x9  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_81  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  989 				{
//  990 					RpCb.tx.sentElapsedTimeS = RpCb.tx.regulatoryModeElapsedTimeS;
??RpAckCheckCallback_80:
        MOVW      HL, #LWRD(_RpCb+94)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+90)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  991 					RpCb.tx.needTxWaitRegulatoryMode = RP_TRUE;
        ONEB      ES:_RpCb+88        ;; 2 cycles
        ; ------------------------------------- Block: 14 cycles
//  992 				}
//  993 
//  994 				if (status & RP_PHY_STAT_TX_UNDERFLOW)
??RpAckCheckCallback_81:
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].7, ??RpAckCheckCallback_82  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
//  995 				{
//  996 					stCfm = RP_TX_UNDERFLOW;
        MOV       A, #0xF4           ;; 1 cycle
        BR        S:??RpAckCheckCallback_83  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  997 				}
//  998 				else
//  999 				{
// 1000 					if ((status & RP_PHY_STAT_TRX_ACK) && (txrxst0 & TRNRCVSQC))
??RpAckCheckCallback_82:
        BF        [HL].1, ??RpAckCheckCallback_84  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
        INCW      HL                 ;; 1 cycle
        BF        [HL].3, ??RpAckCheckCallback_84  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1001 					{
// 1002 						// sequence complete NG
// 1003 						if (RpDriverRetryTrn(status) == RP_TRUE)
        MOVW      AX, [SP]           ;; 1 cycle
          CFI FunCall _RpDriverRetryTrn
        CALL      F:_RpDriverRetryTrn  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_85  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 1004 						{
// 1005 							trnTimes += 1;
// 1006 							RpUpdateTxTime((uint8_t)((comstat3 & RETRNRD) + trnTimes), RP_FALSE, RP_FALSE);	// retry frames + a success frame
        CLRB      C                  ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
        INC       A                  ;; 1 cycle
          CFI FunCall _RpUpdateTxTime
        CALL      F:_RpUpdateTxTime  ;; 3 cycles
// 1007 
// 1008 							return;
        BR        S:??RpAckCheckCallback_55  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
// 1009 						}
// 1010 						else
// 1011 						{
// 1012 							stCfm = RP_NO_ACK;
??RpAckCheckCallback_85:
        MOV       A, #0xF2           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_83:
        MOV       [SP+0x03], A       ;; 1 cycle
        BR        S:??RpAckCheckCallback_86  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1013 						}
// 1014 					}
// 1015 					else
// 1016 					{
// 1017 						RpAfterTrnEnd(status);
??RpAckCheckCallback_84:
        MOVW      AX, [SP]           ;; 1 cycle
          CFI FunCall _RpAfterTrnEnd
        CALL      F:_RpAfterTrnEnd   ;; 3 cycles
// 1018 
// 1019 						if ((status & RP_PHY_STAT_TRX_ACK) && (txrxst0 & RCVPEND))
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].1, ??RpAckCheckCallback_86  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
        INCW      HL                 ;; 1 cycle
        BF        [HL].6, ??RpAckCheckCallback_86  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1020 						{
// 1021 							framePend = RP_FRMPENBIT_ACK;
        MOV       A, #0x4            ;; 1 cycle
        MOV       [SP+0x0B], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1022 						}
// 1023 					}
// 1024 				}
// 1025 				trnTimes += 1;
??RpAckCheckCallback_86:
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1026 			}
// 1027 			RpUpdateTxTime((uint8_t)((comstat3 & RETRNRD) + trnTimes), RP_FALSE, RP_FALSE);	// retry frames + a success frame
??RpAckCheckCallback_79:
        CLRB      C                  ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        ADD       A, B               ;; 1 cycle
          CFI FunCall _RpUpdateTxTime
        CALL      F:_RpUpdateTxTime  ;; 3 cycles
// 1028 
// 1029 			/* Callback function execution Log */
// 1030 			RpLog_Event( RP_LOG_CB, RP_LOG_CB_DATACFM );
        MOV       X, #0x2            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 1031 			/* Callback function execution */
// 1032 			INDIRECT_RpPdDataCfmCallback( stCfm, framePend, RpCb.tx.ccaTimes );
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       C, ES:_RpCb+67     ;; 2 cycles
        MOV       A, [SP+0x0B]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+292)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
        POP       AX                 ;; 1 cycle
          CFI CFA SP+22
          CFI FunCall
        CALL      HL                 ;; 3 cycles
        ; ------------------------------------- Block: 34 cycles
// 1033 		}
// 1034 
// 1035 		if (RpCb.status & RP_PHY_STAT_TRX_OFF)
??RpAckCheckCallback_74:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        BF        A.4, ??RpAckCheckCallback_55  ;; 5 cycles
        ; ------------------------------------- Block: 9 cycles
// 1036 		{
// 1037 			RpCb.rx.waitRelRxBufWhenAutoRx = RP_FALSE;
        CLRB      ES:_RpCb+144       ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 1038 		}
// 1039 	}
// 1040 }
??RpAckCheckCallback_55:
        ADDW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 707 cycles
// 1041 
// 1042 /******************************************************************************
// 1043 Function Name:       RpCancelTrn
// 1044 Parameters:          status:current transmit statusus
// 1045 Return value:        none
// 1046 Description:         cancel Transimit process.
// 1047 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon0
          CFI Function _RpCancelTrn
        CODE
// 1048 static void RpCancelTrn( uint16_t status )
// 1049 {
_RpCancelTrn:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
          CFI FunCall _RpRxOnStop
        ; Auto size: 2
// 1050 	RpRxOnStop();
        CALL      F:_RpRxOnStop      ;; 3 cycles
// 1051 	RpCb.reg.bbTimeCon &= ~COMP0TRG;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+352.1     ;; 3 cycles
// 1052 	RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOV       C, ES:_RpCb+352    ;; 2 cycles
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1053 	if (status & RP_PHY_STAT_RXON_BACKOFF)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_87  ;; 5 cycles
        ; ------------------------------------- Block: 22 cycles
// 1054 	{
// 1055 		RpRelRxBuf(RpCb.rx.pTopRxBuf);
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _RpRelRxBuf
        CALL      F:_RpRelRxBuf      ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
// 1056 	}
// 1057 }
??RpAckCheckCallback_87:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 41 cycles
// 1058 
// 1059 /******************************************************************************
// 1060 Function Name:       RpDriverRetryTrn
// 1061 Parameters:          status:current transmit statusus
// 1062 Return value:        none
// 1063 Description:         Driver Retry Transimit process.
// 1064 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon0
          CFI Function _RpDriverRetryTrn
        CODE
// 1065 static uint8_t RpDriverRetryTrn( uint16_t status )
// 1066 {
_RpDriverRetryTrn:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 8
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+12
// 1067 	uint8_t rtn = RP_FALSE;
        CLRB      B                  ;; 1 cycle
// 1068 
// 1069 	#if defined(__RX)
// 1070 	uint32_t bkupPsw2;
// 1071 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1072 	uint8_t  bkupPsw2;
// 1073 	#elif defined(__arm)
// 1074 	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
// 1075 	#endif
// 1076 
// 1077 	if (status & RP_PHY_STAT_TRX_ACK)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].1, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_88  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
// 1078 	{
// 1079 		if (RpCb.tx.rtrnTimes)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+66        ;; 2 cycles
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_88  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 1080 		{
// 1081 			RpCb.tx.rtrnTimes--;
        DEC       ES:_RpCb+66        ;; 3 cycles
// 1082 			RpCb.status = status;
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
// 1083 			if (status & RP_PHY_STAT_TX_CCA)
        DECW      HL                 ;; 1 cycle
        BT        [HL].7, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_89  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
// 1084 			{
// 1085 				RpInverseTxAnt(RP_TRUE);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
// 1086 
// 1087 				if (status & RP_PHY_STAT_RXON_BACKOFF)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_90  ;; 5 cycles
          CFI FunCall _RpGetRxBuf
        ; ------------------------------------- Block: 13 cycles
// 1088 				{
// 1089 					RpCb.rx.pTopRxBuf  = RpGetRxBuf();
        CALL      F:_RpGetRxBuf      ;; 3 cycles
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1090 					if (RpCb.rx.pTopRxBuf == RP_NULL)
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_91  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpAckCheckCallback_91:
        BNZ       ??RpAckCheckCallback_92  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1091 					{
// 1092 						/* Callback function execution Log */
// 1093 						RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
        MOV       X, #0xB            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 1094 						/* Callback function execution */
// 1095 						INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );
        MOVW      HL, #LWRD(_RpCb+312)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x21           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
// 1096 
// 1097 						RpRfStat.failToGetRxBuf++;
        MOVW      HL, #LWRD(_RpRfStat+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRfStat)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 1098 						return (rtn);
        CLRB      A                  ;; 1 cycle
        BR        R:??RpAckCheckCallback_93  ;; 3 cycles
        ; ------------------------------------- Block: 39 cycles
// 1099 					}
// 1100 					RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
??RpAckCheckCallback_92:
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
// 1101 					RpAvailableRcvRamEnable();
          CFI FunCall _RpAvailableRcvRamEnable
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 1102 				}
// 1103 
// 1104 				RpCb.tx.onCsmaCa = RP_TRUE;
??RpAckCheckCallback_90:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        ONEB      ES:_RpCb+86        ;; 2 cycles
// 1105 				RpSetMaxCsmaBackoffVal();
          CFI FunCall _RpSetMaxCsmaBackoffVal
        CALL      F:_RpSetMaxCsmaBackoffVal  ;; 3 cycles
// 1106 				RpRegWrite(BBCSMACON3, RpCb.pib.macMinBe);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       C, ES:_RpCb+209    ;; 2 cycles
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1107 				RpCb.tx.ccaTimesOneFrame = 0;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+87        ;; 2 cycles
        BR        S:??RpAckCheckCallback_94  ;; 3 cycles
        ; ------------------------------------- Block: 19 cycles
// 1108 			}
// 1109 			else
// 1110 			{
// 1111 				RpInverseTxAnt(RP_FALSE);
??RpAckCheckCallback_89:
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
// 1112 				RpCb.tx.onCsmaCa = RP_FALSE;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+86        ;; 2 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 7 cycles
// 1113 			}
// 1114 
// 1115 			/* Disable interrupt */
// 1116 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1117 			RP_PHY_ALL_DI(bkupPsw2);
??RpAckCheckCallback_94:
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 1118 			#else
// 1119 			RP_PHY_ALL_DI();
// 1120 			#endif
// 1121 
// 1122 			if (RpSetTxTriggerTimer(RpGetTime(), RP_TRUE, 0x00) == RP_TRUE)	// TX trigger
          CFI FunCall _RpGetTime
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       D, #0x0            ;; 1 cycle
        MOV       E, #0x1            ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpSetTxTriggerTimer
        CALL      F:_RpSetTxTriggerTimer  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_95  ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
// 1123 			{
// 1124 				RpCb.status &= ~RP_PHY_STAT_BUSY;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:[HL].5          ;; 3 cycles
// 1125 				RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;
        SET1      ES:[HL].6          ;; 3 cycles
        BR        S:??RpAckCheckCallback_96  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
// 1126 			}
// 1127 			else
// 1128 			{
// 1129 				RpCb.reg.bbCsmaCon0 |= CSMAST;
??RpAckCheckCallback_95:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb+358.0     ;; 3 cycles
// 1130 				RpRegWrite(BBCSMACON0, RpCb.reg.bbCsmaCon0);	// TX trigger
        MOV       C, ES:_RpCb+358    ;; 2 cycles
        MOVW      AX, #0x68          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1131 				RpCb.status |= RP_PHY_STAT_BUSY;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:[HL].5          ;; 3 cycles
        ; ------------------------------------- Block: 15 cycles
// 1132 			}
// 1133 			RpCb.reg.bbIntEn[0] |= (TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN);
??RpAckCheckCallback_96:
        MOV       A, ES:_RpCb+344    ;; 2 cycles
        OR        A, #0xB8           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpCb+344, A    ;; 2 cycles
// 1134 			RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1135 			RpCb.tx.cnt = 0x00;
        CLRW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+6, AX     ;; 2 cycles
// 1136 
// 1137 			if ((status & (RP_PHY_STAT_TX_CCA | RP_PHY_STAT_RXON_BACKOFF)) == (RP_PHY_STAT_TX_CCA | RP_PHY_STAT_RXON_BACKOFF) || (RpCb.tx.len > (RP_BB_TX_RAM_SIZE * 2)))
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x80           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        CMPW      AX, #0x180         ;; 1 cycle
        BZ        ??RpAckCheckCallback_97  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
        MOVW      AX, ES:_RpCb+4     ;; 2 cycles
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8101        ;; 1 cycle
        BC        ??RpAckCheckCallback_98  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 1138 			{
// 1139 #if defined (__ICCRL78__)
// 1140 				RpCb.tx.pNextData = RpCb.tx.pOrgData;
??RpAckCheckCallback_97:
        MOVW      HL, #LWRD(_RpCb+68)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpCb+72)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 1141 #else
// 1142 				RpCb.tx.pNextData = RpCb.tx.data;
// 1143 #endif
// 1144 				RpTrnxHdrFunc(RP_PHY_BANK_0);
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
// 1145 				RpTrnxHdrFunc(RP_PHY_BANK_1);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 1146 			}
// 1147 
// 1148 			/* Enable interrupt */
// 1149 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1150 			RP_PHY_ALL_EI(bkupPsw2);
??RpAckCheckCallback_98:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1151 			#else
// 1152 			RP_PHY_ALL_EI();
// 1153 			#endif
// 1154 
// 1155 			rtn = RP_TRUE;
        ONEB      B                  ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 1156 		}
// 1157 	}
// 1158 
// 1159 	return (rtn);
??RpAckCheckCallback_88:
        MOV       A, B               ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_93:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 270 cycles
// 1160 }
// 1161 
// 1162 /******************************************************************************
// 1163 Function Name:       RpAfterTrnEnd
// 1164 Parameters:          status:current transmit statusus
// 1165 Return value:        none
// 1166 Description:         process after finishing trn interrupt handler for Auto Tx->Rx.
// 1167 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon0
          CFI Function _RpAfterTrnEnd
        CODE
// 1168 static void RpAfterTrnEnd( uint16_t status )
// 1169 {
_RpAfterTrnEnd:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 2
// 1170 	if ((status & RP_PHY_STAT_TRX_TO_RX_AUTO) && (RpCb.tx.onCsmaCa == RP_FALSE))
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].4, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_99  ;; 5 cycles
        ; ------------------------------------- Block: 9 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+86        ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_99  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 1171 	{
// 1172 		if (RpCb.rx.onReplyingAck == RP_FALSE)
        CMP0      ES:_RpCb+142       ;; 2 cycles
        BNZ       ??RpAckCheckCallback_100  ;; 4 cycles
          CFI FunCall _RpGetRxBuf
        ; ------------------------------------- Block: 6 cycles
// 1173 		{
// 1174 			RpCb.rx.pTopRxBuf  = RpGetRxBuf();
        CALL      F:_RpGetRxBuf      ;; 3 cycles
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1175 			if (RpCb.rx.pTopRxBuf == RP_NULL)
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_101  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpAckCheckCallback_101:
        BNZ       ??RpAckCheckCallback_100  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1176 			{
// 1177 				/* Callback function execution Log */
// 1178 				RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
        MOV       X, #0xB            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 1179 				/* Callback function execution */
// 1180 				INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );
        MOVW      HL, #LWRD(_RpCb+312)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x21           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
// 1181 
// 1182 				RpRfStat.failToGetRxBuf++;
        MOVW      HL, #LWRD(_RpRfStat+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRfStat)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 1183 				RpCb.rx.waitRelRxBufWhenAutoRx = RP_TRUE;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        ONEB      ES:_RpCb+144       ;; 2 cycles
        ; ------------------------------------- Block: 38 cycles
// 1184 			}
// 1185 		}
// 1186 		RpCb.status = RP_PHY_STAT_RX;
??RpAckCheckCallback_100:
        ONEW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
// 1187 		if (((RpCb.pib.macAddressFilter1Ena == RP_TRUE) || (RpCb.pib.macAddressFilter2Ena == RP_TRUE))
// 1188 				&& (RpCb.rx.softwareAdf == RP_FALSE))
        CMP       ES:_RpCb+176, #0x1  ;; 2 cycles
        BZ        ??RpAckCheckCallback_102  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        CMP       ES:_RpCb+192, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_103  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_102:
        CMP0      ES:_RpCb+143       ;; 2 cycles
        BNZ       ??RpAckCheckCallback_103  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1189 		{
// 1190 			RpCb.reg.bbTxRxMode0 |= AUTOACKEN;
        SET1      ES:_RpCb+353.2     ;; 3 cycles
// 1191 			RpCb.reg.bbTxRxMode3 |= ADRSFILEN;
        SET1      ES:_RpCb+356.0     ;; 3 cycles
// 1192 			RpRegWrite(BBTXRXMODE3, (uint8_t)(RpCb.reg.bbTxRxMode3));
        MOV       C, ES:_RpCb+356    ;; 2 cycles
        MOV       X, #0x50           ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1193 			if (status & RP_PHY_STAT_SLOTTED)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].3, ??RpAckCheckCallback_103  ;; 5 cycles
        ; ------------------------------------- Block: 20 cycles
// 1194 			{
// 1195 				RpCb.status |= RP_PHY_STAT_SLOTTED;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].3          ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1196 			}
// 1197 		}
// 1198 		RpCb.status |= RP_PHY_STAT_TRX_TO_RX_AUTO;
??RpAckCheckCallback_103:
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
// 1199 
// 1200 		if (status & RP_PHY_STAT_RX_TMOUT)
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].2, ??RpAckCheckCallback_104  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
// 1201 		{
// 1202 			RpCb.status |= RP_PHY_STAT_RX_TMOUT;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].2          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1203 		}
// 1204 		RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
??RpAckCheckCallback_104:
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
// 1205 		RpCb.tx.onCsmaCa = RP_FALSE;
        CLRB      ES:_RpCb+86        ;; 2 cycles
// 1206 
// 1207 		if (RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE)
        CMP       ES:_RpCb+226, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_105  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 1208 		{
// 1209 			RpSetCcaDurationVal(RP_FALSE);
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetCcaDurationVal
        CALL      F:_RpSetCcaDurationVal  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1210 		}
// 1211 		RpInverseTxAnt(RP_TRUE);
??RpAckCheckCallback_105:
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
// 1212 
// 1213 		RpAddressFilterSetting((uint8_t)(RpCb.pib.macAddressFilter1Ena | RpCb.pib.macAddressFilter2Ena));
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+176    ;; 2 cycles
        OR        A, ES:_RpCb+192    ;; 2 cycles
          CFI FunCall _RpAddressFilterSetting
        CALL      F:_RpAddressFilterSetting  ;; 3 cycles
// 1214 		RpCb.reg.bbTxRxMode1 |= CCASEL; // RSSI
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb+354.0     ;; 3 cycles
// 1215 		RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
        MOV       C, ES:_RpCb+354    ;; 2 cycles
        MOVW      AX, #0x18          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1216 		RpAvailableRcvRamEnable();
          CFI FunCall _RpAvailableRcvRamEnable
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
// 1217 
// 1218 		RpChangeFilter();
          CFI FunCall _RpChangeFilter
        CALL      F:_RpChangeFilter  ;; 3 cycles
// 1219 		RpSetSfdDetectionExtendWrite(RpCb.status);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
          CFI FunCall _RpSetSfdDetectionExtendWrite
        CALL      F:_RpSetSfdDetectionExtendWrite  ;; 3 cycles
// 1220 
// 1221 		RpRxOnStart();			// rxtrg  and xxxic
          CFI FunCall _RpRxOnStart
        CALL      F:_RpRxOnStart     ;; 3 cycles
// 1222 
// 1223 		/* Start Antenna select assist */
// 1224 		RpAntSelAssist_StartProc();
          CFI FunCall _RpAntSelAssist_StartProc
        CALL      F:_RpAntSelAssist_StartProc  ;; 3 cycles
        ; ------------------------------------- Block: 40 cycles
// 1225 	}
// 1226 }
??RpAckCheckCallback_99:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 213 cycles
// 1227 
// 1228 /******************************************************************************
// 1229 Function Name:	 RpResumeCsmaTrn
// 1230 Parameters: 		 status : RF status
// 1231 Return value:		 none
// 1232 Description:		 Resume Transmit function.
// 1233 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon2
          CFI Function _RpResumeCsmaTrn
        CODE
// 1234 static void RpResumeCsmaTrn( uint16_t status, uint8_t ccaTotal, uint8_t rxEna )
// 1235 {
_RpResumeCsmaTrn:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 1236 	uint16_t txFlen;
// 1237 	uint8_t csmacon1, csmacon3;
// 1238 
// 1239 	RpCb.status = status;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
// 1240 	if (status & RP_PHY_STAT_TRX_ACK)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].1, ??RpAckCheckCallback_106  ;; 5 cycles
        ; ------------------------------------- Block: 15 cycles
// 1241 	{
// 1242 		RpRegWrite(BBTXRXCON, ACKRCVEN);
        MOV       C, #0x8            ;; 1 cycle
        BR        S:??RpAckCheckCallback_107  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1243 	}
// 1244 	else
// 1245 	{
// 1246 		RpRegWrite(BBTXRXCON, 0x00);
??RpAckCheckCallback_106:
        CLRB      C                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_107:
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1247 	}
// 1248 
// 1249 	RpInverseTxAnt(RP_TRUE);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
// 1250 
// 1251 	RpCb.reg.bbIntEn[0] |= (TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+344    ;; 2 cycles
        OR        A, #0xB8           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpCb+344, A    ;; 2 cycles
// 1252 	RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1253 	RpCb.tx.cnt = 0x00;
        CLRW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+6, AX     ;; 2 cycles
// 1254 #if defined (__ICCRL78__)
// 1255 	RpCb.tx.pNextData = RpCb.tx.pOrgData;
        MOVW      HL, #LWRD(_RpCb+68)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpCb+72)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 1256 #else
// 1257 	RpCb.tx.pNextData = RpCb.tx.data;
// 1258 #endif
// 1259 	RpTrnxHdrFunc(RP_PHY_BANK_0);
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
// 1260 	RpTrnxHdrFunc(RP_PHY_BANK_1);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
// 1261 	txFlen = RpCb.tx.len + RpCb.pib.phyFcsLength;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+219    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, ES:_RpCb+4     ;; 2 cycles
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1262 	RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x520         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 1263 	RpCb.tx.ccaTimes += ccaTotal;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+67     ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        ADD       A, X               ;; 1 cycle
        MOV       ES:_RpCb+67, A     ;; 2 cycles
// 1264 	RpCb.tx.ccaTimesOneFrame += ccaTotal;
        MOV       X, ES:_RpCb+87     ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        ADD       A, X               ;; 1 cycle
        MOV       ES:_RpCb+87, A     ;; 2 cycles
// 1265 	csmacon1 = RpCb.reg.bbCsmaCon1;
// 1266 	csmacon1 &= (~NB);
        MOV       A, ES:_RpCb+359    ;; 2 cycles
        AND       A, #0xF8           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
// 1267 	if (RpCb.pib.macMaxCsmaBackOff >= RpCb.tx.ccaTimesOneFrame)
        MOV       B, ES:_RpCb+208    ;; 2 cycles
        MOV       A, ES:_RpCb+87     ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        CMP       B, A               ;; 1 cycle
        BC        ??RpAckCheckCallback_108  ;; 4 cycles
        ; ------------------------------------- Block: 94 cycles
// 1268 	{
// 1269 		csmacon1 |= (RpCb.pib.macMaxCsmaBackOff - RpCb.tx.ccaTimesOneFrame);
        MOV       A, B               ;; 1 cycle
        SUB       A, ES:_RpCb+87     ;; 2 cycles
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 1270 	}
// 1271 	RpCb.reg.bbCsmaCon1 = csmacon1;
??RpAckCheckCallback_108:
        MOV       A, X               ;; 1 cycle
        MOV       ES:_RpCb+359, A    ;; 2 cycles
// 1272 	RpRegWrite(BBCSMACON1, RpCb.reg.bbCsmaCon1);
        MOV       C, ES:_RpCb+359    ;; 2 cycles
        MOVW      AX, #0x90          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1273 	csmacon3 = RpRegRead(BBCSMACON3);
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 1274 	csmacon3 &= (~BEMIN);
// 1275 	csmacon3 |= (RpCb.tx.ccaTimesOneFrame + RpCb.pib.macMinBe);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+87     ;; 2 cycles
        ADD       A, ES:_RpCb+209    ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0xF0           ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
// 1276 	if (csmacon3 > RpCb.pib.macMaxBe)
        MOV       A, ES:_RpCb+210    ;; 2 cycles
        CMP       A, C               ;; 1 cycle
        SKNC                         ;; 1 cycle
        ; ------------------------------------- Block: 28 cycles
// 1277 	{
// 1278 		csmacon3 = RpCb.pib.macMaxBe;
        MOV       C, ES:_RpCb+210    ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 1279 	}
// 1280 	RpRegWrite(BBCSMACON3, csmacon3);
??RpResumeCsmaTrn_0:
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1281 	RpCb.reg.bbCsmaCon0 |= CSMAST;
        MOVW      HL, #LWRD(_RpCb+358)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:[HL].0          ;; 3 cycles
// 1282 	if(rxEna == RP_FALSE)
        MOV       A, [SP+0x03]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_109  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
// 1283 	{
// 1284 		RpCb.reg.bbCsmaCon0 &= ~CSMARCVEN;
        CLR1      ES:[HL].2          ;; 3 cycles
// 1285 		RpCb.status &= ~RP_PHY_STAT_RXON_BACKOFF;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        CLR1      ES:[HL].0          ;; 3 cycles
// 1286 		RpRelRxBuf(RpCb.rx.pTopRxBuf);
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _RpRelRxBuf
        CALL      F:_RpRelRxBuf      ;; 3 cycles
        ; ------------------------------------- Block: 19 cycles
// 1287 	}
// 1288 	RpRegWrite(BBCSMACON0, RpCb.reg.bbCsmaCon0);	// TX trigger
??RpAckCheckCallback_109:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       C, ES:_RpCb+358    ;; 2 cycles
        MOVW      AX, #0x68          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1289 
// 1290 	#if defined(__arm)
// 1291 	RpLedTxOff();
// 1292 	#endif
// 1293 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 14 cycles
        ; ------------------------------------- Total: 197 cycles
// 1294 
// 1295 /* "Interrupt source number 5" */
// 1296 /***************************************************************************************************************
// 1297  * function name  : RpTrn0Hdr
// 1298  * description    : Bank0 transmit completion interrupt handler
// 1299  * parameters     : none
// 1300  * return value   : none
// 1301  **************************************************************************************************************/
// 1302 static void RpTrn0Hdr( void )
// 1303 {
// 1304 	RpTrnxHdrFunc(RP_PHY_BANK_0);
// 1305 }
// 1306 
// 1307 /* "Interrupt source number 6" */
// 1308 /***************************************************************************************************************
// 1309  * function name  : RpTrn1Hdr
// 1310  * description    : Bank1 transmit completion interrupt handler
// 1311  * parameters     : none
// 1312  * return value   : none
// 1313  **************************************************************************************************************/
// 1314 static void RpTrn1Hdr( void )
// 1315 {
// 1316 	RpTrnxHdrFunc(RP_PHY_BANK_1);
// 1317 }
// 1318 
// 1319 /* "Interrupt source number 11" */
// 1320 /***************************************************************************************************************
// 1321  * function name  : RpRcv0Hdr
// 1322  * description    : Bank0 receive completion interrupt handler
// 1323  * parameters     : none
// 1324  * return value   : none
// 1325  **************************************************************************************************************/
// 1326 static void RpRcv0Hdr( void )
// 1327 {
// 1328 	RpRcvHdrFunc(RP_PHY_BANK_0);
// 1329 }
// 1330 
// 1331 /* "Interrupt source number 12" */
// 1332 /***************************************************************************************************************
// 1333  * function name  : RpRcv1Hdr
// 1334  * description    : Bank1 receive completion interrupt handler
// 1335  * parameters     : none
// 1336  * return value   : none
// 1337  **************************************************************************************************************/
// 1338 static void RpRcv1Hdr( void )
// 1339 {
// 1340 	RpRcvHdrFunc(RP_PHY_BANK_1);
// 1341 }
// 1342 
// 1343 /***********************************************************************
// 1344  * function name  : RpStartTransmitWithCca
// 1345  * description    : 
// 1346  * parameters     : none
// 1347  * return value   : none
// 1348  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon0
          CFI Function _RpStartTransmitWithCca
          CFI NoCalls
        CODE
// 1349 void RpStartTransmitWithCca( void )
// 1350 {
_RpStartTransmitWithCca:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 1351 	uint8_t wk;
// 1352 	uint16_t wk16;
// 1353 
// 1354 	/* Save the current value */
// 1355 	RpCb.pib.backup_macMaxCsmaBackOff    = RpCb.pib.macMaxCsmaBackOff;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+208    ;; 2 cycles
        MOV       ES:_RpCb+275, A    ;; 2 cycles
// 1356 	RpCb.pib.backup_macMinBe             = RpCb.pib.macMinBe;
        MOV       A, ES:_RpCb+209    ;; 2 cycles
        MOV       ES:_RpCb+276, A    ;; 2 cycles
// 1357 	RpCb.pib.backup_phyCsmaBackoffPeriod = RpCb.pib.phyCsmaBackoffPeriod;
        MOVW      AX, ES:_RpCb+240   ;; 2 cycles
        MOVW      ES:_RpCb+278, AX   ;; 2 cycles
// 1358 	RpCb.pib.backup_phyCcaDuration       = RpCb.pib.phyCcaDuration;
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        MOVW      ES:_RpCb+280, AX   ;; 2 cycles
// 1359 
// 1360 	/* Set the value for ACK transmission */
// 1361 	RpCb.pib.macMaxCsmaBackOff    = 0x00;
        CLRB      ES:_RpCb+208       ;; 2 cycles
// 1362 	RpCb.pib.macMinBe             = 0x00;
        CLRB      ES:_RpCb+209       ;; 2 cycles
// 1363 
// 1364 	switch( RpCb.pib.phyDataRate )
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        SUBW      AX, #0x28          ;; 1 cycle
        BZ        ??RpAckCheckCallback_110  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
        SUBW      AX, #0xA           ;; 1 cycle
        BZ        ??RpAckCheckCallback_111  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0x32          ;; 1 cycle
        BZ        ??RpAckCheckCallback_112  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0x32          ;; 1 cycle
        BZ        ??RpAckCheckCallback_113  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0x32          ;; 1 cycle
        BZ        ??RpAckCheckCallback_114  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0x64          ;; 1 cycle
        BZ        ??RpAckCheckCallback_115  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0x64          ;; 1 cycle
        BZ        ??RpAckCheckCallback_116  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??RpAckCheckCallback_117  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1365 	{
// 1366 		case 10:
// 1367 		case 20:
// 1368 			wk = 0x00;
// 1369 			break;
// 1370 		case 40:
// 1371 			if (RpCb.pib.phyCcaDuration < 0x12) {
??RpAckCheckCallback_110:
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        CMPW      AX, #0x12          ;; 1 cycle
        BNC       ??RpAckCheckCallback_117  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 1372 				wk = 0x12;
        MOV       A, #0x12           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1373 			} else {
// 1374 				wk = 0x00; }
// 1375 			break;
// 1376 		case 50:
// 1377 			if (RpCb.pib.phyCcaDuration < 0x16) {
// 1378 				wk = 0x16; }
// 1379 			else {
// 1380 				wk = 0x00; }
// 1381 			break;
// 1382 		case 100:
// 1383 			if (RpCb.pib.phyCcaDuration < 0x28) {
// 1384 				wk = 0x28; }
// 1385 			else {
// 1386 				wk = 0x00; }
// 1387 			break;
// 1388 		case 150:
// 1389 			if (RpCb.pib.phyCcaDuration < 0x3A) {
// 1390 				wk = 0x3A; }
// 1391 			else {
// 1392 				wk = 0x00; }
// 1393 			break;
// 1394 		case 200:
// 1395 			if (RpCb.pib.phyCcaDuration < 0x4D) {
// 1396 				wk = 0x4D; }
// 1397 			else {
// 1398 				wk = 0x00; }
// 1399 			break;
// 1400 		case 300:
// 1401 			if (RpCb.pib.phyCcaDuration < 0x6E) {
// 1402 				wk = 0x6E; }
// 1403 			else {
// 1404 				wk = 0x00; }
// 1405 			break;
// 1406 		case 400:
// 1407 			if (RpCb.pib.phyCcaDuration < 0x96) {
// 1408 				wk = 0x96; }
// 1409 			else {
// 1410 				wk = 0x00; }
// 1411 			break;
// 1412 		default:
// 1413 			wk = 0x00;
// 1414 			break;
// 1415 	}
// 1416 
// 1417 	if ( wk != 0x00 )
// 1418 	{
// 1419 		wk16 = (RpCb.pib.phyCcaDuration+1) / 2;
// 1420 		RpCb.pib.phyCsmaBackoffPeriod = wk16 + (wk / 2);
??RpStartTransmitWithCca_0:
        SHR       A, 0x1             ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        INCW      AX                 ;; 1 cycle
        SHRW      AX, 0x1            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
??RpStartTransmitWithCca_1:
        MOVW      ES:_RpCb+240, AX   ;; 2 cycles
// 1421 	}
// 1422 	else
// 1423 	{
// 1424 		RpCb.pib.phyCsmaBackoffPeriod = 0x10;
// 1425 	}
// 1426 
// 1427 	/* Set the value in the register */
// 1428 	/* macMaxCsmaBackOff */
// 1429 	/* === BBCSMACON1(NB) === */
// 1430 	RpSetMaxCsmaBackoffVal();
        BR        F:?Subroutine0     ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpAckCheckCallback_111:
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        CMPW      AX, #0x16          ;; 1 cycle
        BNC       ??RpAckCheckCallback_117  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x16           ;; 1 cycle
        BR        S:??RpStartTransmitWithCca_0  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_112:
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        CMPW      AX, #0x28          ;; 1 cycle
        BNC       ??RpAckCheckCallback_117  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x28           ;; 1 cycle
        BR        S:??RpStartTransmitWithCca_0  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_113:
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        CMPW      AX, #0x3A          ;; 1 cycle
        BNC       ??RpAckCheckCallback_117  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x3A           ;; 1 cycle
        BR        S:??RpStartTransmitWithCca_0  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_114:
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        CMPW      AX, #0x4D          ;; 1 cycle
        BNC       ??RpAckCheckCallback_117  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x4D           ;; 1 cycle
        BR        S:??RpStartTransmitWithCca_0  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_115:
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        CMPW      AX, #0x6E          ;; 1 cycle
        BNC       ??RpAckCheckCallback_117  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x6E           ;; 1 cycle
        BR        S:??RpStartTransmitWithCca_0  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_116:
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        CMPW      AX, #0x96          ;; 1 cycle
        BNC       ??RpAckCheckCallback_117  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x96           ;; 1 cycle
        BR        S:??RpStartTransmitWithCca_0  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_117:
        MOVW      AX, #0x10          ;; 1 cycle
        BR        S:??RpStartTransmitWithCca_1  ;; 3 cycles
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 4 cycles
        ; ------------------------------------- Total: 152 cycles
// 1431 
// 1432 	/* macMinBe */
// 1433 	/* === BBCSMACON3 === */
// 1434 	RpCb.pib.macMinBe &= BEMIN;		// b0-b3: BEMIN bit (Sets the macMinBE value.)
// 1435 	RpRegWrite( BBCSMACON3, RpCb.pib.macMinBe );
// 1436 
// 1437 	/* phyCsmaBackoffPeriod */
// 1438 	/* === BBBOFFPERIOD === */
// 1439 	RpSetCsmaBackoffPeriod();
// 1440 
// 1441 	/* phyCcaDuration */
// 1442 	RpSetCcaDurationVal(RP_TRUE);
// 1443 }
// 1444 
// 1445 /***********************************************************************
// 1446  * function name  : RpEndTransmitWithCca
// 1447  * description    : 
// 1448  * parameters     : none
// 1449  * return value   : none
// 1450  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon0
          CFI Function _RpEndTransmitWithCca
          CFI NoCalls
        CODE
// 1451 static void RpEndTransmitWithCca( void )
// 1452 {
_RpEndTransmitWithCca:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 1453 	RpCb.pib.macMaxCsmaBackOff    = RpCb.pib.backup_macMaxCsmaBackOff;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+275    ;; 2 cycles
        MOV       ES:_RpCb+208, A    ;; 2 cycles
// 1454 	RpCb.pib.macMinBe             = RpCb.pib.backup_macMinBe;
        MOV       A, ES:_RpCb+276    ;; 2 cycles
        MOV       ES:_RpCb+209, A    ;; 2 cycles
// 1455 	RpCb.pib.phyCsmaBackoffPeriod = RpCb.pib.backup_phyCsmaBackoffPeriod;
        MOVW      AX, ES:_RpCb+278   ;; 2 cycles
        MOVW      ES:_RpCb+240, AX   ;; 2 cycles
// 1456 	RpCb.pib.phyCcaDuration       = RpCb.pib.backup_phyCcaDuration;
        MOVW      AX, ES:_RpCb+280   ;; 2 cycles
        MOVW      ES:_RpCb+212, AX   ;; 2 cycles
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 17 cycles
        ; ------------------------------------- Total: 17 cycles
// 1457 
// 1458 	/* Set the value in the register */
// 1459 	/* macMaxCsmaBackOff */
// 1460 	RpSetMaxCsmaBackoffVal();
        REQUIRE ?Subroutine0
        ; // Fall through to label ?Subroutine0
// 1461 
// 1462 	/* macMinBe */
// 1463 	/* === BBCSMACON3 === */
// 1464 	RpCb.pib.macMinBe &= BEMIN;		// b0-b3: BEMIN bit (Sets the macMinBE value.)
// 1465 	RpRegWrite( BBCSMACON3, RpCb.pib.macMinBe );
// 1466 
// 1467 	/* phyCsmaBackoffPeriod */
// 1468 	RpSetCsmaBackoffPeriod();
// 1469 
// 1470 	/* phyCcaDuration */
// 1471 	RpSetCcaDurationVal(RP_TRUE);
// 1472 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon0
          CFI NoFunction
          CFI FunCall _RpStartTransmitWithCca _RpSetMaxCsmaBackoffVal
          CFI FunCall _RpEndTransmitWithCca _RpSetMaxCsmaBackoffVal
        CODE
?Subroutine0:
        CALL      F:_RpSetMaxCsmaBackoffVal  ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+209    ;; 2 cycles
        AND       A, #0xF            ;; 1 cycle
        MOV       ES:_RpCb+209, A    ;; 2 cycles
        MOV       C, ES:_RpCb+209    ;; 2 cycles
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpStartTransmitWithCca _RpRegWrite
          CFI FunCall _RpEndTransmitWithCca _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
          CFI FunCall _RpStartTransmitWithCca _RpSetCsmaBackoffPeriod
          CFI FunCall _RpEndTransmitWithCca _RpSetCsmaBackoffPeriod
        CALL      F:_RpSetCsmaBackoffPeriod  ;; 3 cycles
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpStartTransmitWithCca _RpSetCcaDurationVal
          CFI FunCall _RpEndTransmitWithCca _RpSetCcaDurationVal
        CALL      F:_RpSetCcaDurationVal  ;; 3 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 28 cycles
        ; ------------------------------------- Total: 28 cycles
// 1473 
// 1474 /***************************************************************************************************************
// 1475  * function name  : RpStateRx_SetTxTrigger
// 1476  * description    : 
// 1477  * parameters     : useCca...
// 1478  * return value   : none
// 1479  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock18 Using cfiCommon0
          CFI Function _RpStateRx_SetTxTrigger
        CODE
// 1480 static void RpStateRx_SetTxTrigger( uint8_t useCca )
// 1481 {
_RpStateRx_SetTxTrigger:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 1482 	if ( useCca == RP_FALSE )
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_118  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1483 	{
// 1484 		RpRegWrite(BBTXRXCON, TRNTRG); // TX trigger without CCA
        MOV       C, #0x2            ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1485 		RpCb.status |= RP_PHY_STAT_BUSY;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:[HL].5          ;; 3 cycles
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 16 cycles
// 1486 	}
// 1487 	else
// 1488 	{
// 1489 		/* === BBTXRXMODE4 === */
// 1490 		RpCb.reg.bbTxRxMode4 = 0x00;	// init
// 1491 		RpCb.reg.bbTxRxMode4 |= CCAINTSEL;		// b0: Upon completion of the CSMA-CA sequence
??RpAckCheckCallback_118:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        ONEB      ES:_RpCb+357       ;; 2 cycles
// 1492 	//	RpCb.reg.bbTxRxMode4 &= ~INTOUTSEL;		// b4: When with the interrupt request = INTOUT pin is at high level
// 1493 	//	RpCb.reg.bbTxRxMode4 &= ~TIMEOUTRCV;	// b6: Automatic normal reception mode
// 1494 	//	RpCb.reg.bbTxRxMode4 &= ~TIMEOUTINTSEL;	// b7: Byte receive interrupt
// 1495 		RpRegWrite( BBTXRXMODE4, RpCb.reg.bbTxRxMode4 );
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x88          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1496 
// 1497 		/* === BBCSMACON0 === */
// 1498 		RpCb.reg.bbCsmaCon0 = 0x00;		// init
// 1499 	//	RpCb.reg.bbCsmaCon0 &= ~UNICASTFRM;		// b4: Prohibition of automatic ack reception
// 1500 	//	RpCb.reg.bbCsmaCon0 &= ~CSMARCVEN;		// b2: Reception during CSMA-CA function disable
// 1501 		RpCb.reg.bbCsmaCon0 |= CSMATRNST;		// b1: Transmit after CSMA-CA process
// 1502 		RpCb.reg.bbCsmaCon0 |= CSMAST;			// b0: Automatic CSMA-CA start
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+358, #0x3  ;; 2 cycles
// 1503 		RpRegWrite( BBCSMACON0, RpCb.reg.bbCsmaCon0 );	// TX trigger with CCA (if the timer trigger is off)
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0x68          ;; 1 cycle
          CFI EndBlock cfiBlock18
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 34 cycles
        REQUIRE ?Subroutine2
        ; // Fall through to label ?Subroutine2
// 1504 	}
// 1505 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock19 Using cfiCommon0
          CFI NoFunction
          CFI FunCall _RpStateRx_SetTxTrigger _RpRegWrite
          CFI FunCall _RpDumPrcRcvPhr _RpRegWrite
        CODE
?Subroutine2:
        CALL      F:_RpRegWrite      ;; 3 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock19
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
// 1506 
// 1507 /* "Interrupt source number 10" */
// 1508 /***************************************************************************************************************
// 1509  * function name  : RpRcvFinHdr
// 1510  * description    : Frame receive completion interrupt handler
// 1511  * parameters     : none
// 1512  * return value   : none
// 1513  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock20 Using cfiCommon0
          CFI Function _RpRcvFinHdr
        CODE
// 1514 static void RpRcvFinHdr( void )
// 1515 {
_RpRcvFinHdr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 30
        SUBW      SP, #0x1E          ;; 1 cycle
          CFI CFA SP+34
// 1516 	uint16_t	status;
// 1517 	uint8_t		statusRxTimeout;
// 1518 	uint8_t	stOptInd;
// 1519 	uint8_t	rcvRamSt;
// 1520 	uint16_t txFlen;
// 1521 	uint16_t txFlenBk;
// 1522 	uint8_t errCd, errCdAnd;
// 1523 	uint8_t rcvRamSt2;
// 1524 	uint32_t nowTime, difTime;
// 1525 	uint32_t willTxTime;
// 1526 	uint32_t toffMinTime;
// 1527 	uint8_t antTxSel, antDvrCon;
// 1528 	uint8_t txrxst0;
// 1529 	uint32_t ackTxTrigerLimitTimeBit;
// 1530 	uint8_t ackTxTrigerFlag = RP_TRUE;
// 1531 	uint16_t dummy;
// 1532 	uint8_t interruptDisable = RP_FALSE;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
// 1533 	uint8_t useRxFcsLength = RP_TRUE;
// 1534 
// 1535 	#if defined(__RX)
// 1536 	uint32_t bkupPsw2;
// 1537 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1538 	uint8_t  bkupPsw2;
// 1539 	#elif defined(__arm)
// 1540 	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
// 1541 	#endif
// 1542 
// 1543 	uint8_t *pBuf;
// 1544 	uint8_t warningIndCallbackFlag = RP_FALSE;
        MOV       [SP+0x0B], A       ;; 1 cycle
// 1545 #ifdef R_FSB_FAN_ENABLED
// 1546 	uint8_t res;
// 1547 #endif // #ifdef R_FSB_FAN_ENABLED
// 1548 
// 1549 	RpCb.reg.bbIntReq = 0;
        MOVW      HL, #LWRD(_RpCb+348)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 1550 	status = RpCb.status;
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 1551 	statusRxTimeout = RpCb.statusRxTimeout;
        MOV       A, ES:_RpCb+2      ;; 2 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 1552 
// 1553 	// receive mode
// 1554 	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_119  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
// 1555 	{
// 1556 		if (RpCb.rx.cnt != RP_PHY_RX_STAT_INIT)
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        CMPW      AX, #0xFFFE        ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_120  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 1557 		{
// 1558 			txrxst0 = RpRegRead(BBTXRXST0);
        MOVW      AX, #0x38          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 1559 			if (RpChkCrcStatus(&stOptInd, txrxst0) == RP_ERROR)
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _RpChkCrcStatus
        CALL      F:_RpChkCrcStatus  ;; 3 cycles
        INC       A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_120  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
// 1560 			{
// 1561 				RpRxEndSetting(status);
// 1562 			}
// 1563 			else
// 1564 			{
// 1565 #ifdef R_FSB_FAN_ENABLED
// 1566 				RpFsb.enable = RP_FALSE;	// init
// 1567 #endif // #ifdef R_FSB_FAN_ENABLED
// 1568 				RpCcaAck.enable = RP_FALSE;	// init
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        CLRB      ES:_RpCcaAck       ;; 2 cycles
// 1569 
// 1570 				if ( RpCb.rx.softwareAdf )
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+143       ;; 2 cycles
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_121  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 1571 				{
// 1572 					RpCb.rx.rcvRamSt = (uint8_t)((txrxst0 & RCVRAMST) ? 0x00 : RCVRAMST);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].7, ??RpAckCheckCallback_122  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        CLRB      A                  ;; 1 cycle
        BR        S:??RpAckCheckCallback_123  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_122:
        MOV       A, #0x80           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_123:
        MOV       [SP+0x09], A       ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        MOV       ES:_RpCb+146, A    ;; 2 cycles
// 1573 					RpReadAddressInfo();
          CFI FunCall _RpReadAddressInfo
        CALL      F:_RpReadAddressInfo  ;; 3 cycles
// 1574 
// 1575 #ifdef R_FSB_FAN_ENABLED
// 1576 					res = R_FSB_DownlinkCallback( RpCb.rx.pTopRxBuf, (uint16_t)RpCb.rx.lenNoFcs, &(RpFsb.resData[0]), (uint16_t *)&txFlen );
// 1577 					switch ( res )
// 1578 					{
// 1579 						case R_FSB_INVALID_FRAME:
// 1580 							errCd = RP_INVALID_FRAME;
// 1581 							break;
// 1582 						case R_FSB_VALID_FRAME:
// 1583 							errCd = RP_VALID_FRAME;
// 1584 							RpFsb.enable = RP_TRUE;
// 1585 							break;
// 1586 						case R_FSB_NEED_RESPONSE:
// 1587 							errCd = (RP_VALID_FRAME | RP_NEED_ACK);
// 1588 							RpFsb.enable = RP_TRUE;
// 1589 							useRxFcsLength = RP_FALSE;
// 1590 							break;
// 1591 						default:
// 1592 							/* Callback function execution Log */
// 1593 							RpLog_Event( RP_LOG_CB, RP_LOG_CB_ACKCHECK );
// 1594 							/* Callback function execution */
// 1595 							errCd = INDIRECT_RpAckCheckCallback( RpCb.rx.pTopRxBuf, (uint16_t)RpCb.rx.lenNoFcs, RpCb.tx.ackData, (uint16_t *)&txFlen );
// 1596 							break;
// 1597 					}
// 1598 #else  // #ifdef R_FSB_FAN_ENABLED
// 1599 					/* Callback function execution Log */
// 1600 					RpLog_Event( RP_LOG_CB, RP_LOG_CB_ACKCHECK );
        MOV       X, #0x7            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 1601 					/* Callback function execution */
// 1602 					errCd = INDIRECT_RpAckCheckCallback( RpCb.rx.pTopRxBuf, (uint16_t)RpCb.rx.lenNoFcs, RpCb.tx.ackData, (uint16_t *)&txFlen );
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x1C], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x1A], AX      ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+38
        MOV       X, #BYTE3(_RpCb)   ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, #LWRD(_RpCb+12)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+108   ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+320)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+44
        POP       AX                 ;; 1 cycle
          CFI CFA SP+42
          CFI FunCall
        CALL      HL                 ;; 3 cycles
        MOV       B, A               ;; 1 cycle
// 1603 #endif // #ifdef R_FSB_FAN_ENABLED
// 1604 
// 1605 #ifdef R_FSB_FAN_ENABLED
// 1606 					if (( RpCb.pib.phyAckWithCca )||( RpFsb.enable == RP_TRUE ))
// 1607 #else
// 1608 					if ( RpCb.pib.phyAckWithCca )
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+34
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+274       ;; 2 cycles
        BZ        ??RpAckCheckCallback_124  ;; 4 cycles
        ; ------------------------------------- Block: 61 cycles
// 1609 #endif // #ifdef R_FSB_FAN_ENABLED
// 1610 					{
// 1611 						RpCcaAck.enable = RP_TRUE;
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        ONEB      ES:_RpCcaAck       ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
// 1612 					}
// 1613 
// 1614 					if (errCd & (RP_VALID_FRAME | RP_VALID_FRAME2))
??RpAckCheckCallback_124:
        AND       A, #0x11           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_125  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1615 					{
// 1616 						errCdAnd = (uint8_t)(errCd & (RP_VALID_FRAME | RP_VALID_FRAME2));
// 1617 						if (errCdAnd == RP_VALID_FRAME)
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_126  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1618 						{
// 1619 							RpCb.rx.filteredAdress = 0x01;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        ONEB      ES:_RpCb+137       ;; 2 cycles
        BR        S:??RpAckCheckCallback_127  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1620 						}
// 1621 						else if (errCdAnd == RP_VALID_FRAME2)
??RpAckCheckCallback_126:
        CMP       A, #0x10           ;; 1 cycle
        BNZ       ??RpAckCheckCallback_128  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1622 						{
// 1623 							RpCb.rx.filteredAdress = 0x02;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+137, #0x2  ;; 2 cycles
        BR        S:??RpAckCheckCallback_127  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1624 						}
// 1625 						else if (errCdAnd == (RP_VALID_FRAME | RP_VALID_FRAME2))
??RpAckCheckCallback_128:
        CMP       A, #0x11           ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_129  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1626 						{
// 1627 							RpCb.rx.filteredAdress = 0x03;
        MOV       ES:_RpCb+137, #0x3  ;; 2 cycles
        BR        S:??RpAckCheckCallback_127  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1628 						}
// 1629 						else
// 1630 						{
// 1631 							RpCb.rx.filteredAdress = 0x00;
??RpAckCheckCallback_129:
        CLRB      ES:_RpCb+137       ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 1632 						}
// 1633 						if ((stOptInd & RP_CRC_BAD) == 0)
??RpAckCheckCallback_127:
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].7, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_130  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 1634 						{
// 1635 							if ((errCd & (RP_NEED_ACK | RP_NEED_ACK2)) && (((RpCb.reg.bbIntReq = (uint32_t)RpRegRead(BBINTREQ0)) & RP_BBCCA_IREQ) == 0))
        MOV       A, B               ;; 1 cycle
        AND       A, #0x22           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_130  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, #0x1B0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+348)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        BF        ES:[HL].7, $+8     ;; 5 cycles
        BR        F:??RpAckCheckCallback_130  ;; 5 cycles
        ; ------------------------------------- Block: 18 cycles
// 1636 							{
// 1637 								if (RpCb.tx.onCsmaCa == RP_TRUE)
        CMP       ES:_RpCb+86, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_131  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1638 								{
// 1639 									RpRegWrite(BBTXRXRST, RFSTOP);		// RF Stop
        ONEB      C                  ;; 1 cycle
        MOV       X, #0x8            ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 5 cycles
// 1640 								}
// 1641 
// 1642 								/* Disable interrupt */
// 1643 								#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1644 								RP_PHY_ALL_DI(bkupPsw2);
??RpAckCheckCallback_131:
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 1645 								#else
// 1646 								RP_PHY_ALL_DI();
// 1647 								#endif
// 1648 
// 1649 								/* Begin ACK Transmit */
// 1650 								nowTime = RpGetTime();
          CFI FunCall _RpGetTime
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      [SP+0x16], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
// 1651 								if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+235, #0x2  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_132  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
        CMP       ES:_RpCb+234, #0x9  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_133  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1652 								{
// 1653 									if ( RpCcaAck.enable ) // Ack with CCA or R_FSB_DownlinkCallback
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        CMP0      ES:_RpCcaAck       ;; 2 cycles
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_134  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 1654 									{
// 1655 										if (((RpCb.pib.phyCurrentChannel >= RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE) && (RpCheckLongerThanTotalTxTime(txFlen, 0x00, useRxFcsLength) == RP_TRUE)) || // Total Tx Time
// 1656 											(RpExtChkErrFrameLen(txFlen, useRxFcsLength) == RP_TRUE)) // TonMax
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+152    ;; 2 cycles
        CMP       A, #0x9            ;; 1 cycle
        BC        ??RpAckCheckCallback_135  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        ONEB      B                  ;; 1 cycle
        CLRB      C                  ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpCheckLongerThanTotalTxTime
        CALL      F:_RpCheckLongerThanTotalTxTime  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_136  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
??RpAckCheckCallback_135:
        ONEB      C                  ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpExtChkErrFrameLen
        CALL      F:_RpExtChkErrFrameLen  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_136  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 1657 										{
// 1658 											ackTxTrigerFlag = RP_FALSE;
// 1659 										}
// 1660 										else
// 1661 										{
// 1662 											RpCb.tx.ackLenForRegulatoryMode = txFlen;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+102, AX   ;; 2 cycles
// 1663 											RpCb.tx.applyAckToffMin = RP_TRUE;
        ONEB      ES:_RpCb+105       ;; 2 cycles
// 1664 											toffMinTime = (RpCb.tx.sentTime + RpCalcTxInterval(RpCb.tx.sentLen, RP_FALSE) - RP_PHY_TX_WARM_UP_TIME) & RP_TIME_MASK;
        CLRB      C                  ;; 1 cycle
        MOVW      AX, ES:_RpCb+80    ;; 2 cycles
          CFI FunCall _RpCalcTxInterval
        CALL      F:_RpCalcTxInterval  ;; 3 cycles
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        MOVW      BC, #0x15E         ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+82)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+38
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x12], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x14], AX      ;; 1 cycle
// 1665 											if (RpCb.pib.phyCurrentChannel >= RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)
        MOV       A, ES:_RpCb+152    ;; 2 cycles
        CMP       A, #0x9            ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_133  ;; 4 cycles
        ; ------------------------------------- Block: 86 cycles
        BR        R:??RpAckCheckCallback_137  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1666 											{
// 1667 												RpCb.tx.applyAckTotalTxTime = RP_TRUE;
// 1668 #ifdef R_FSB_FAN_ENABLED
// 1669 												if ( RpFsb.enable == RP_TRUE ) // R_FSB_DownlinkCallback
// 1670 												{
// 1671 													RpCb.tx.isFsbDownlink = RP_TRUE;
// 1672 												}
// 1673 #endif // #ifdef R_FSB_FAN_ENABLED
// 1674 											}
// 1675 										}
// 1676 									}
// 1677 									else // Ack without CCA
// 1678 									{
// 1679 										/* Check the conditions of response to skip carrier sense in ARIB. */
// 1680 										ackTxTrigerLimitTimeBit = RpCb.tx.ackCompLimitTimeBit - RP_ACKTX_WARMUP_TIME - (RpCalcTotalBytes(txFlen, &dummy, useRxFcsLength) << 3);
??RpAckCheckCallback_134:
        ONEB      B                  ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpCalcTotalBytes
        CALL      F:_RpCalcTotalBytes  ;; 3 cycles
        SHLW      AX, 0x3            ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      DE, #0x3E8         ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        MOVW      BC, #0x15E         ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+98)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+38
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
// 1681 										if (((ackTxTrigerLimitTimeBit - RpCb.rx.timeAcKRtn) > RP_TIME_LIMIT) ||
// 1682 											((ackTxTrigerLimitTimeBit - nowTime) > RP_TIME_LIMIT))
        MOVW      HL, #LWRD(_RpCb+120)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 92 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpRcvFinHdr_0:
        BNC       ??RpAckCheckCallback_136  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 18 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpRcvFinHdr_1:
        BC        ??RpAckCheckCallback_133  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1683 										{
// 1684 											ackTxTrigerFlag = RP_FALSE;
// 1685 										}
// 1686 									}
// 1687 								}
// 1688 								else if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
// 1689 								{
// 1690 									if ((RpCheckLongerThanTotalTxTime(txFlen, 0x00, useRxFcsLength) == RP_TRUE) ||	// Total Tx Time
// 1691 										(RpExtChkErrFrameLen(txFlen, useRxFcsLength) == RP_TRUE))					// TonMax
// 1692 									{
// 1693 										ackTxTrigerFlag = RP_FALSE;
// 1694 									}
// 1695 									else
// 1696 									{
// 1697 										RpCb.tx.ackLenForRegulatoryMode = txFlen;
// 1698 										RpCb.tx.applyAckTotalTxTime = RP_TRUE;
// 1699 #ifdef R_FSB_FAN_ENABLED
// 1700 										if ( RpFsb.enable == RP_TRUE ) // R_FSB_DownlinkCallback
// 1701 										{
// 1702 											RpCb.tx.applyAckToffMin = RP_TRUE;
// 1703 											toffMinTime = (RpCb.tx.sentTime + ((uint32_t)(RpCb.pib.phyRmodeToffMin) * (uint32_t)(RpCb.pib.phyDataRate)) - RP_PHY_TX_WARM_UP_TIME) & RP_TIME_MASK;
// 1704 											RpCb.tx.isFsbDownlink = RP_TRUE;
// 1705 										}
// 1706 #endif // #ifdef R_FSB_FAN_ENABLED
// 1707 									}
// 1708 								}
// 1709 
// 1710 								if ( ackTxTrigerFlag == RP_TRUE )
// 1711 								{
// 1712 									/* update status */
// 1713 									RpCb.status |= RP_PHY_STAT_TX;	// TX
// 1714 
// 1715 									/* enable phyAckWithCca? */
// 1716 									RpCcaAck.result = RP_NO_FRMPENBIT_ACK; //RP_SUCCESS;
// 1717 
// 1718 									/* CCA-ACK enabled? */
// 1719 									if ( RpCcaAck.enable )
// 1720 									{
// 1721 										/* update status */
// 1722 										RpCb.status |= RP_PHY_STAT_TX_CCA;	// TX with CCA
// 1723 
// 1724 										RpStartTransmitWithCca();
// 1725 
// 1726 										/* Change Narrow Band Filter */
// 1727 										if( RpCb.pib.phyCcaBandwidth != RP_PHY_CCA_BANDWIDTH_INDEX_225K )
// 1728 										{
// 1729 											RpChangeNarrowBandFilter();
// 1730 										}
// 1731 									}
// 1732 
// 1733 									/* Select a more future time from timeAcKRtn and toffminTime for the Tx time */
// 1734 									willTxTime = RpCb.rx.timeAcKRtn;
// 1735 									if (RpCb.tx.applyAckToffMin == RP_TRUE)
// 1736 									{
// 1737 										difTime = (toffMinTime - RpCb.rx.timeAcKRtn) & RP_TIME_MASK;
// 1738 										if (difTime <= RP_TIME_LIMIT)
// 1739 										{
// 1740 											willTxTime = toffMinTime;
// 1741 										}
// 1742 									}
// 1743 
// 1744 									difTime = (willTxTime - nowTime) & RP_TIME_MASK;
// 1745 									if ((difTime <= RP_TIME_LIMIT) && (difTime >= (uint32_t)RpTc0WasteTime[RpCb.freqIdIndex])) // (willTxTime >= nowTime) and (willTxTime - nowTime >= RpTc0WasteTime)
// 1746 									{
// 1747 										/************/
// 1748 										RpTC0SetReg( RpCcaAck.enable, willTxTime ); // TX timer trigger with/without CCA
// 1749 										/************/
// 1750 										RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;          // TX waiting timer trigger
// 1751 
// 1752 										nowTime = RpGetTime();
// 1753 										difTime = (willTxTime - nowTime) & RP_TIME_MASK;
// 1754 										if ((difTime > RP_TIME_LIMIT) || (difTime == 0)) // willTxTime < nowTime
// 1755 										{
// 1756 											if (RpCheckRfIRQ() == RP_FALSE)
// 1757 											{
// 1758 												/* Cancel timer trigger */
// 1759 												RpCb.reg.bbIntEn[0] &= ~TIM0INTEN;
// 1760 												RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
// 1761 												RpCb.reg.bbTimeCon &= ~(COMP0TRG | COMP0TRGSEL);
// 1762 												RpRegWrite(BBTIMECON, RpCb.reg.bbTimeCon);
// 1763 												RpCb.status &= ~RP_PHY_STAT_WAIT_TMRTRG; // remove TX waiting timer trigger
// 1764 
// 1765 												/* Set TX trigger with/without CCA */
// 1766 												RpStateRx_SetTxTrigger( RpCcaAck.enable );
// 1767 
// 1768 												#if defined(__arm)
// 1769 												RpLedTxOff();
// 1770 												#endif
// 1771 											}
// 1772 										}
// 1773 									}
// 1774 									else
// 1775 									{
// 1776 										if (difTime <= RP_TIME_LIMIT) // (willTxTime >= nowTime) and (willTxTime - nowTime < RpTc0WasteTime)
// 1777 										{
// 1778 											while (((willTxTime - RpGetTime()) & RP_TIME_MASK) <= RP_TIME_LIMIT); // wait for RpTc0WasteTime
// 1779 										}
// 1780 
// 1781 										/* Set TX trigger with/without CCA */
// 1782 										RpStateRx_SetTxTrigger( RpCcaAck.enable );
// 1783 									}
// 1784 
// 1785 									#if defined(__arm)
// 1786 									RpLedTxOff();
// 1787 									#endif
// 1788 
// 1789 /******************************************************************************************************
// 1790 ******************************************************************************************************/
// 1791 
// 1792 									RpSetSfdDetectionExtendWrite(RpCb.status);
// 1793 
// 1794 									txFlen = (uint16_t)(txFlen + RpCb.rx.fcsLength);
// 1795 
// 1796 									if (RpCb.rx.fcsLength != RpCb.pib.phyFcsLength)
// 1797 									{
// 1798 										if (RpCb.rx.fcsLength == RP_FCS_LENGTH_16BIT)
// 1799 										{
// 1800 											RpCb.reg.bbSubCon |= CRCBIT;
// 1801 										}
// 1802 										else if (RpCb.rx.fcsLength == RP_FCS_LENGTH_32BIT)
// 1803 										{
// 1804 											RpCb.reg.bbSubCon &= ~CRCBIT;
// 1805 										}
// 1806 										RpRegWrite(BBSUBGCON, (uint8_t)(RpCb.reg.bbSubCon));
// 1807 									}
// 1808 									RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));
// 1809 
// 1810 									if (RpCb.pib.phyAntennaDiversityRxEna)
// 1811 									{
// 1812 										RpCb.rx.selectedAntenna = RpDetRcvdAntenna();
// 1813 										switch (RpCb.pib.phyAntennaSelectAckTx)
// 1814 										{
// 1815 											case 0x00:
// 1816 												antTxSel = RP_PHY_ANTENNA_0;
// 1817 												break;
// 1818 											case 0x01:
// 1819 												antTxSel = RP_PHY_ANTENNA_1;
// 1820 												break;
// 1821 											case 0x02:
// 1822 												antTxSel = RpCb.rx.selectedAntenna;
// 1823 												break;
// 1824 											case 0x03:
// 1825 												if (RpCb.rx.selectedAntenna == RP_PHY_ANTENNA_0)
// 1826 												{
// 1827 													antTxSel = RP_PHY_ANTENNA_1;
// 1828 												}
// 1829 												else
// 1830 												{
// 1831 													antTxSel = RP_PHY_ANTENNA_0;
// 1832 												}
// 1833 												break;
// 1834 										}
// 1835 										antDvrCon = RpRegRead(BBANTDIV);
// 1836 										RpCb.rx.bkupAckAnt = (uint8_t)(antDvrCon & ANTSWTRNSET);
// 1837 										antDvrCon &= (~ANTSWTRNSET);
// 1838 										if (antTxSel)
// 1839 										{
// 1840 											antDvrCon |= ANTSWTRNSET;
// 1841 										}
// 1842 										RpRegWrite(BBANTDIV, antDvrCon);
// 1843 									}
// 1844 									else
// 1845 									{
// 1846 										RpInverseTxAnt(RP_FALSE);
// 1847 									}
// 1848 
// 1849 									RpCb.rx.onReplyingAck = RP_TRUE;
// 1850 									RpCb.reg.bbIntEn[0] &= ~CCAINTEN;
// 1851 
// 1852 									if ( RpCcaAck.enable )
// 1853 									{
// 1854 										RpCb.reg.bbIntEn[0] |= CCAINTEN;
// 1855 									}
// 1856 
// 1857 									RpCb.reg.bbIntEn[0] |= (TRNFININTEN | TRN0INTEN | TRN1INTEN);
// 1858 									RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
// 1859 									RpCb.status |= (RP_PHY_STAT_TX | RP_PHY_STAT_BUSY);
// 1860 									RpCb.tx.cnt = 0x00;
// 1861 #ifdef R_FSB_FAN_ENABLED
// 1862 									if ( RpFsb.enable == RP_TRUE )
// 1863 									{
// 1864 										RpCb.tx.pNextData = &(RpFsb.resData[0]);
// 1865 									}
// 1866 									else
// 1867 									{
// 1868 										RpCb.tx.pNextData = RpCb.tx.ackData;
// 1869 									}
// 1870 #else
// 1871 									RpCb.tx.pNextData = RpCb.tx.ackData;
// 1872 #endif // #ifdef R_FSB_FAN_ENABLED
// 1873 									txFlenBk = RpCb.tx.len;
// 1874 									RpCb.tx.len = txFlen - RpCb.rx.fcsLength;
// 1875 									RpTrnxHdrFunc(RP_PHY_BANK_0);
// 1876 									RpTrnxHdrFunc(RP_PHY_BANK_1);
// 1877 									if (statusRxTimeout & RP_STATUS_RX_TIMEOUT_EXTENTION)
// 1878 									{
// 1879 										RpCb.reg.bbIntEn[0] &= ~TIM1INTEN;
// 1880 										RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
// 1881 									}
// 1882 
// 1883 									/* Disable interrupt */
// 1884 									#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1885 									RP_PHY_ALL_EI(bkupPsw2);
// 1886 									#else
// 1887 									RP_PHY_ALL_EI();
// 1888 									#endif
// 1889 
// 1890 									RpCb.tx.len = txFlenBk;
// 1891 									rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);
// 1892 
// 1893 									if (RpCb.rx.unreadRamPrev == RP_TRUE)
// 1894 									{
// 1895 										RpCb.rx.unreadRamPrev = RP_FALSE;
// 1896 										if (rcvRamSt)
// 1897 										{
// 1898 											rcvRamSt2 = 0x00;
// 1899 										}
// 1900 										else
// 1901 										{
// 1902 											rcvRamSt2 = 0x80;
// 1903 										}
// 1904 										RpReadRam(rcvRamSt2);
// 1905 									}
// 1906 									RpReadRam(rcvRamSt);
// 1907 
// 1908 									return;
// 1909 								}
// 1910 								else
// 1911 								{
// 1912 									/* Enable interrupt */
// 1913 									#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1914 									RP_PHY_ALL_EI(bkupPsw2);
??RpAckCheckCallback_136:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1915 									#else
// 1916 									RP_PHY_ALL_EI();
// 1917 									#endif
// 1918 
// 1919 									RpRxEndSetting(status);
        MOVW      AX, [SP+0x06]      ;; 1 cycle
          CFI FunCall _RpRxEndSetting
        CALL      F:_RpRxEndSetting  ;; 3 cycles
// 1920 
// 1921 									/* Callback function execution Log */
// 1922 									RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_TXACKSTOP );
        MOV       X, #0xA            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 1923 									/* Callback function execution */
// 1924 									INDIRECT_WarningIndCallback( RP_WARN_TX_ACK_STOP );
        MOVW      HL, #LWRD(_RpCb+312)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x28           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
// 1925 #ifdef R_FSB_FAN_ENABLED
// 1926 									RpFsb.enable = RP_FALSE;
// 1927 #endif // #ifdef R_FSB_FAN_ENABLED
// 1928 									RpCcaAck.enable = RP_FALSE;
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        CLRB      ES:_RpCcaAck       ;; 2 cycles
// 1929 									return;
        BR        R:??RpAckCheckCallback_119  ;; 3 cycles
        ; ------------------------------------- Block: 31 cycles
// 1930 								}
??RpAckCheckCallback_132:
        CMP       ES:_RpCb+235, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_133  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        ONEB      B                  ;; 1 cycle
        CLRB      C                  ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpCheckLongerThanTotalTxTime
        CALL      F:_RpCheckLongerThanTotalTxTime  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpAckCheckCallback_136  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        ONEB      C                  ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpExtChkErrFrameLen
        CALL      F:_RpExtChkErrFrameLen  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpAckCheckCallback_136  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+102, AX   ;; 2 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_137:
        ONEB      ES:_RpCb+104       ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
??RpAckCheckCallback_133:
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        SET1      ES:[HL].1          ;; 3 cycles
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        CLRB      ES:_RpCcaAck+1     ;; 2 cycles
        CMP0      ES:_RpCcaAck       ;; 2 cycles
        BZ        ??RpAckCheckCallback_138  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:[HL].7          ;; 3 cycles
          CFI FunCall _RpStartTransmitWithCca
        CALL      F:_RpStartTransmitWithCca  ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+246       ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpChangeNarrowBandFilter
        ; ------------------------------------- Block: 11 cycles
        CALL      F:_RpChangeNarrowBandFilter  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_138:
        MOVW      HL, #LWRD(_RpCb+120)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x10], AX      ;; 1 cycle
        CMP       ES:_RpCb+105, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_139  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 20 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpRcvFinHdr_2:
        BNC       ??RpAckCheckCallback_139  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_139:
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x12], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x14], AX      ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 24 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpRcvFinHdr_3:
        SKC                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_140  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, ES:_RpCb+342    ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpTc0WasteTime)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpTc0WasteTime)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CLRW      BC                 ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+34
        SKNC                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_141  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        MOV       A, ES:_RpCcaAck    ;; 2 cycles
          CFI FunCall _RpTC0SetReg
        CALL      F:_RpTC0SetReg     ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb.6         ;; 3 cycles
          CFI FunCall _RpGetTime
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      DE, AX             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 39 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpRcvFinHdr_4:
        BNC       ??RpAckCheckCallback_142  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        OR        A, E               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        OR        A, H               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_143  ;; 4 cycles
          CFI FunCall _RpCheckRfIRQ
        ; ------------------------------------- Block: 7 cycles
??RpAckCheckCallback_142:
        CALL      F:_RpCheckRfIRQ    ;; 3 cycles
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_143  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+344.0     ;; 3 cycles
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+352    ;; 2 cycles
        AND       A, #0xF5           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpCb+352, A    ;; 2 cycles
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb.6         ;; 3 cycles
        BR        R:??RpAckCheckCallback_140  ;; 3 cycles
        ; ------------------------------------- Block: 28 cycles
??RpRcvFinHdr_5:
        CMP       ES:_RpCb+134, #0x4  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_144  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      HL, #LWRD(_RpCb+364)  ;; 1 cycle
        CLR1      ES:[HL].2          ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_144:
        MOV       C, ES:_RpCb+364    ;; 2 cycles
        MOVW      AX, #0x580         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpRcvFinHdr_6:
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x520         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+34
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+226       ;; 2 cycles
        BZ        ??RpAckCheckCallback_145  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, #0x870         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV1      CY, [HL].2         ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ROLC      A, 0x1             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+128, A    ;; 2 cycles
        MOV       A, ES:_RpCb+228    ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_146  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_147  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_148  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_149  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??RpAckCheckCallback_150  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_148:
        MOV       A, ES:_RpCb+128    ;; 2 cycles
        BR        S:??RpAckCheckCallback_151  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpAckCheckCallback_149:
        CMP0      ES:_RpCb+128       ;; 2 cycles
        ONEB      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_151  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        BR        S:??RpAckCheckCallback_146  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_147:
        ONEB      A                  ;; 1 cycle
        BR        S:??RpAckCheckCallback_151  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_146:
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_151:
        MOV       [SP+0x08], A       ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_150:
        MOVW      AX, #0x5A0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        AND       A, #0x2            ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+147, A    ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        AND       A, #0xFD           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_152  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        MOV       A, C               ;; 1 cycle
        OR        A, #0x2            ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_152:
        MOVW      AX, #0x5A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        BR        S:??RpAckCheckCallback_153  ;; 3 cycles
          CFI FunCall _RpInverseTxAnt
        ; ------------------------------------- Block: 7 cycles
??RpAckCheckCallback_145:
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_153:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        ONEB      ES:_RpCb+142       ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+344)  ;; 1 cycle
        CLR1      ES:[HL].7          ;; 3 cycles
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        CMP0      ES:_RpCcaAck       ;; 2 cycles
        BZ        ??RpAckCheckCallback_154  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:[HL].7          ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_154:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+344    ;; 2 cycles
        OR        A, #0x38           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        XCH       A, X               ;; 1 cycle
        OR        A, #0x22           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_RpCb+6, AX     ;; 2 cycles
        MOV       ES:_RpCb+74, #BYTE3(_RpCb)  ;; 2 cycles
        MOVW      AX, #LWRD(_RpCb+12)  ;; 1 cycle
        MOVW      ES:_RpCb+72, AX    ;; 2 cycles
        MOVW      AX, ES:_RpCb+4     ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       X, ES:_RpCb+134    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        MOVW      ES:_RpCb+4, AX     ;; 2 cycles
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].1, ??RpAckCheckCallback_155  ;; 5 cycles
        ; ------------------------------------- Block: 54 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+344.1     ;; 3 cycles
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
??RpAckCheckCallback_155:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+4, AX     ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        AND       A, #0x80           ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        CMP       ES:_RpCb+145, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_156  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        CLRB      ES:_RpCb+145       ;; 2 cycles
        MOV       A, [SP+0x09]       ;; 1 cycle
          CFI FunCall _RpReadRam
        CALL      F:_RpReadRam       ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_156:
        MOV       A, [SP+0x04]       ;; 1 cycle
          CFI FunCall _RpReadRam
        CALL      F:_RpReadRam       ;; 3 cycles
        BR        R:??RpAckCheckCallback_119  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 1931 							}
// 1932 						}
// 1933 
// 1934 #ifdef R_FSB_FAN_ENABLED
// 1935 //						g_SnBridge = RP_FALSE;
// 1936 #endif // #ifdef R_FSB_FAN_ENABLED
// 1937 
// 1938 						rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);
??RpAckCheckCallback_130:
        MOV       A, [SP+0x04]       ;; 1 cycle
        AND       A, #0x80           ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
// 1939 						if (RpCb.rx.unreadRamPrev == RP_TRUE)
        CMP       ES:_RpCb+145, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_157  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 1940 						{
// 1941 							RpCb.rx.unreadRamPrev = RP_FALSE;
        CLRB      ES:_RpCb+145       ;; 2 cycles
// 1942 							if (rcvRamSt)
// 1943 							{
// 1944 								rcvRamSt2 = 0x00;
// 1945 							}
// 1946 							else
// 1947 							{
// 1948 								rcvRamSt2 = 0x80;
// 1949 							}
// 1950 							RpReadRam(rcvRamSt2);
        MOV       A, [SP+0x09]       ;; 1 cycle
          CFI FunCall _RpReadRam
        CALL      F:_RpReadRam       ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1951 						}
// 1952 						RpReadRam(rcvRamSt);
??RpAckCheckCallback_157:
        MOV       A, [SP+0x04]       ;; 1 cycle
          CFI FunCall _RpReadRam
        CALL      F:_RpReadRam       ;; 3 cycles
// 1953 
// 1954 						if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
        MOV       A, [SP+0x05]       ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_158  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 1955 						{
// 1956 							;
// 1957 						}
// 1958 						else
// 1959 						{
// 1960 							if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].4, ??RpAckCheckCallback_158  ;; 5 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 9 cycles
// 1961 							{
// 1962 								/* Disable interrupt */
// 1963 								#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1964 								RP_PHY_ALL_DI(bkupPsw2);
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 1965 								#else
// 1966 								RP_PHY_ALL_DI();
// 1967 								#endif
// 1968 								interruptDisable = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
// 1969 
// 1970 								RpRxOnStart();		// RX trigger
          CFI FunCall _RpRxOnStart
        CALL      F:_RpRxOnStart     ;; 3 cycles
// 1971 
// 1972 								/* Start Antenna select assist */
// 1973 								RpAntSelAssist_ReStartProc();
          CFI FunCall _RpAntSelAssist_ReStartProc
        CALL      F:_RpAntSelAssist_ReStartProc  ;; 3 cycles
        BR        S:??RpAckCheckCallback_158  ;; 3 cycles
        ; ------------------------------------- Block: 27 cycles
// 1974 							}
// 1975 						}
// 1976 					}
// 1977 					else
// 1978 					{
// 1979 #ifdef R_FSB_FAN_ENABLED
// 1980 						RpFsb.enable = RP_FALSE;
// 1981 #endif // #ifdef R_FSB_FAN_ENABLED
// 1982 						RpCcaAck.enable = RP_FALSE;
??RpAckCheckCallback_125:
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        CLRB      ES:_RpCcaAck       ;; 2 cycles
// 1983 						RpRxEndSetting(status);
        BR        R:??RpAckCheckCallback_120  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1984 
// 1985 						return;
// 1986 					}
// 1987 				}
// 1988 				else
// 1989 				{
// 1990 					if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
??RpAckCheckCallback_121:
        MOV       A, [SP+0x05]       ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_159  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1991 					{
// 1992 						;
// 1993 					}
// 1994 					else
// 1995 					{
// 1996 						if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].4, ??RpAckCheckCallback_159  ;; 5 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 9 cycles
// 1997 						{
// 1998 							/* Disable interrupt */
// 1999 							#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2000 							RP_PHY_ALL_DI(bkupPsw2);
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 2001 							#else
// 2002 							RP_PHY_ALL_DI();
// 2003 							#endif
// 2004 							interruptDisable = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
// 2005 
// 2006 							RpRxOnStart();		// RX trigger
          CFI FunCall _RpRxOnStart
        CALL      F:_RpRxOnStart     ;; 3 cycles
// 2007 
// 2008 							/* Start Antenna select assist */
// 2009 							RpAntSelAssist_ReStartProc();
          CFI FunCall _RpAntSelAssist_ReStartProc
        CALL      F:_RpAntSelAssist_ReStartProc  ;; 3 cycles
        ; ------------------------------------- Block: 24 cycles
// 2010 						}
// 2011 					}
// 2012 
// 2013 					rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);
// 2014 					RpReadRam(rcvRamSt);
??RpAckCheckCallback_159:
        MOV       A, [SP+0x04]       ;; 1 cycle
        AND       A, #0x80           ;; 1 cycle
          CFI FunCall _RpReadRam
        CALL      F:_RpReadRam       ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2015 				}
// 2016 
// 2017 				/** RpDataIndCall (Start) **************************
// 2018 				***************************************************/
// 2019 				RpCcaAck.enable = RP_FALSE;
??RpAckCheckCallback_158:
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        CLRB      ES:_RpCcaAck       ;; 2 cycles
// 2020 
// 2021 				RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
// 2022 				pBuf = RpCb.rx.pTopRxBuf;
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x10], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x0E], AX      ;; 1 cycle
// 2023 
// 2024 				if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].4, ??RpAckCheckCallback_160  ;; 5 cycles
        ; ------------------------------------- Block: 23 cycles
// 2025 				{
// 2026 					if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
        MOV       A, [SP+0x05]       ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        BZ        ??RpAckCheckCallback_161  ;; 4 cycles
          CFI FunCall _RpSetStateRxOnToTrxOff
        ; ------------------------------------- Block: 6 cycles
// 2027 					{
// 2028 						RpSetStateRxOnToTrxOff();
        CALL      F:_RpSetStateRxOnToTrxOff  ;; 3 cycles
// 2029 						RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+2         ;; 2 cycles
        BR        S:??RpAckCheckCallback_162  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 2030 					}
// 2031 					else
// 2032 					{
// 2033 						warningIndCallbackFlag = RP_TRUE;
// 2034 					}
// 2035 				}
// 2036 				else if (status & RP_PHY_STAT_RXON_BACKOFF)
??RpAckCheckCallback_160:
        BF        [HL].0, ??RpAckCheckCallback_163  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 2037 				{
// 2038 					warningIndCallbackFlag = RP_TRUE;
??RpAckCheckCallback_161:
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x0B], A       ;; 1 cycle
        BR        S:??RpAckCheckCallback_162  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2039 				}
// 2040 				else	// Single RX
// 2041 				{
// 2042 					RpCb.status = RP_PHY_STAT_TRX_OFF;
??RpAckCheckCallback_163:
        MOVW      AX, #0x10          ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
// 2043 					if (status & RP_PHY_STAT_RX_TMOUT)
        BF        [HL].2, ??RpAckCheckCallback_162  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 2044 					{
// 2045 						RpCb.reg.bbIntEn[0] &= ~TIM1INTEN;
        CLR1      ES:_RpCb+344.1     ;; 3 cycles
// 2046 						RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 2047 					}
// 2048 				}
// 2049 
// 2050 				RpRegBlockRead(BBRSSICCARSLT, (uint8_t *)&RpCb.rx.rssi, sizeof(uint16_t));
??RpAckCheckCallback_162:
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      DE, #LWRD(_RpCb+126)  ;; 1 cycle
        MOV       C, #BYTE3(_RpCb)   ;; 1 cycle
        MOV       X, #0x20           ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 2051 				RpCb.rx.rssi -= RpCb.pib.phyRssiOutputOffset;
// 2052 				RpCb.rx.rssi &= 0x01FF;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+282    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, ES:_RpCb+126   ;; 2 cycles
        SUBW      AX, HL             ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        MOVW      ES:_RpCb+126, AX   ;; 2 cycles
// 2053 
// 2054 				RpGetAntennaDiversityRssi();
          CFI FunCall _RpGetAntennaDiversityRssi
        CALL      F:_RpGetAntennaDiversityRssi  ;; 3 cycles
// 2055 				RpGetSelectedAntennaInfo(status);
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall _RpGetSelectedAntennaInfo
        CALL      F:_RpGetSelectedAntennaInfo  ;; 3 cycles
// 2056 				RpValidityCheckSFDTimeStamp();
          CFI FunCall _RpValidityCheckSFDTimeStamp
        CALL      F:_RpValidityCheckSFDTimeStamp  ;; 3 cycles
// 2057 
// 2058 				if (interruptDisable == RP_TRUE)
        POP       AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_164  ;; 5 cycles
        ; ------------------------------------- Block: 38 cycles
// 2059 				{
// 2060 					/* Enable interrupt */
// 2061 					#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2062 					RP_PHY_ALL_EI(bkupPsw2);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2063 					#else
// 2064 					RP_PHY_ALL_EI();
// 2065 					#endif
// 2066 				}
// 2067 
// 2068 				RpPdDataIndCallbackPrc(pBuf, stOptInd, warningIndCallbackFlag);
??RpAckCheckCallback_164:
        MOV       A, [SP+0x0B]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall _RpPdDataIndCallbackPrc
        CALL      F:_RpPdDataIndCallbackPrc  ;; 3 cycles
// 2069 
// 2070 				if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
        MOV       A, [SP+0x05]       ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        BZ        ??RpAckCheckCallback_119  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
// 2071 				{
// 2072 					if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].4, ??RpAckCheckCallback_119  ;; 5 cycles
        ; ------------------------------------- Block: 9 cycles
// 2073 					{
// 2074 						/* Callback function execution Log */
// 2075 						RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
        MOV       X, #0x5            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2076 						/* Callback function execution */
// 2077 						INDIRECT_RpRxOffIndCallback();
        MOVW      HL, #LWRD(_RpCb+304)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall
        CALL      AX                 ;; 3 cycles
        BR        S:??RpAckCheckCallback_119  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
// 2078 					}
// 2079 				}
// 2080 				/** RpDataIndCall (End) ****************************
// 2081 				***************************************************/
// 2082 			}
// 2083 		}
// 2084 		else
// 2085 		{
// 2086 			RpRxEndSetting(status);
??RpAckCheckCallback_120:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
          CFI FunCall _RpRxEndSetting
        CALL      F:_RpRxEndSetting  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2087 		}
// 2088 	}
// 2089 }
??RpAckCheckCallback_119:
        ADDW      SP, #0x1E          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+34
          CFI FunCall _RpGetTime
        ; ------------------------------------- Block: 7 cycles
??RpAckCheckCallback_141:
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 20 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpRcvFinHdr_7:
        BC        ??RpAckCheckCallback_141  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_140:
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        MOV       A, ES:_RpCcaAck    ;; 2 cycles
          CFI FunCall _RpStateRx_SetTxTrigger
        CALL      F:_RpStateRx_SetTxTrigger  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_143:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
          CFI FunCall _RpSetSfdDetectionExtendWrite
        CALL      F:_RpSetSfdDetectionExtendWrite  ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+134    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       X, ES:_RpCb+134    ;; 2 cycles
        MOV       A, ES:_RpCb+219    ;; 2 cycles
        CMP       X, A               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpRcvFinHdr_6  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        CMP       ES:_RpCb+134, #0x2  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpRcvFinHdr_5  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      HL, #LWRD(_RpCb+364)  ;; 1 cycle
        SET1      ES:[HL].2          ;; 3 cycles
        BR        R:??RpAckCheckCallback_144  ;; 3 cycles
          CFI EndBlock cfiBlock20
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 1355 cycles
// 2090 
// 2091 /******************************************************************************
// 2092 Function Name:       RpChkCrcStatus
// 2093 Parameters:          pData:The pointer of dest copy
// 2094 					 len:The pointer of received length
// 2095 					 tmStmp:The pointer of timestamp
// 2096 					 lqi:The pointer of LQI
// 2097 					 pStatOpt:The pointer of status
// 2098 Return value:        0:normal received
// 2099                      1:crc error ocuur
// 2100 Description:         Recieved frame analize.
// 2101 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock21 Using cfiCommon3
          CFI Function _RpChkCrcStatus
        CODE
// 2102 static uint8_t RpChkCrcStatus(uint8_t *pStatOpt, uint8_t txrxst0)
// 2103 {
_RpChkCrcStatus:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       X, A               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 2104 	uint8_t rtn = RP_SUCCESS;
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2105 	uint8_t	crcRslt = (uint8_t)(txrxst0 & CRC);
        MOV       A, X               ;; 1 cycle
        AND       A, #0x2            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
// 2106 
// 2107 	if (pStatOpt != RP_NULL)
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_165  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpAckCheckCallback_165:
        BZ        ??RpAckCheckCallback_166  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2108 	{
// 2109 		*pStatOpt = RP_NO_FRMPENBIT_ACK;
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 2110 		if ((crcRslt == 0) && (RpCb.rx.pTopRxBuf != RP_NULL))
        CMP0      B                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_167  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_168  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpAckCheckCallback_168:
        BZ        ??RpAckCheckCallback_169  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2111 		{
// 2112 			if (((RpCb.pib.macAddressFilter1Ena == RP_TRUE) || (RpCb.pib.macAddressFilter2Ena == RP_TRUE))
// 2113 					&& (RpCb.rx.softwareAdf == RP_FALSE))
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+176, #0x1  ;; 2 cycles
        BZ        ??RpAckCheckCallback_170  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_RpCb+192, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_166  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_170:
        CMP0      ES:_RpCb+143       ;; 2 cycles
        BNZ       ??RpAckCheckCallback_166  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2114 			{
// 2115 				if (RpRegRead(BBTXRXMODE2) & FLMPENDST)
        MOVW      AX, #0x48          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].2, ??RpAckCheckCallback_166  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
// 2116 				{
// 2117 					*pStatOpt = RP_FRMPENBIT_ACK;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0x4            ;; 1 cycle
        BR        S:??RpAckCheckCallback_171  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 2118 				}
// 2119 			}
// 2120 		}
// 2121 		else
// 2122 		{
// 2123 			if ((crcRslt) && (RpCb.pib.phyCrcErrorUpMsg == RP_RX_CRCERROR_UPMSG) && (RpCb.rx.pTopRxBuf != RP_NULL))
??RpAckCheckCallback_167:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+172, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_169  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_172  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpAckCheckCallback_172:
        BZ        ??RpAckCheckCallback_169  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2124 			{
// 2125 				*pStatOpt = RP_CRC_BAD;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0x80           ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RpAckCheckCallback_171:
        MOV       ES:[HL], A         ;; 2 cycles
        BR        S:??RpAckCheckCallback_166  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2126 			}
// 2127 			else
// 2128 			{
// 2129 				rtn = RP_ERROR;
??RpAckCheckCallback_169:
        MOV       A, #0xFF           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2130 			}
// 2131 		}
// 2132 	}
// 2133 
// 2134 	return (rtn);
??RpAckCheckCallback_166:
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock21
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 140 cycles
// 2135 }
// 2136 
// 2137 /******************************************************************************
// 2138 Function Name:       RpRdxEndSetting
// 2139 Parameters:          current status
// 2140 Return value:        none
// 2141 Description:         Rx End Setting.
// 2142 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock22 Using cfiCommon0
          CFI Function _RpRxEndSetting
        CODE
// 2143 static void RpRxEndSetting( uint16_t status )
// 2144 {
_RpRxEndSetting:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 4
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
// 2145 	uint8_t statusRxTimeout;
// 2146 	statusRxTimeout = RpCb.statusRxTimeout;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+2      ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 2147 
// 2148 	RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
// 2149 
// 2150 	RpAvailableRcvRamEnable();
          CFI FunCall _RpAvailableRcvRamEnable
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
// 2151 
// 2152 	if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
        MOV       A, [SP]            ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        BZ        ??RpAckCheckCallback_173  ;; 4 cycles
          CFI FunCall _RpSetStateRxOnToTrxOff
        ; ------------------------------------- Block: 18 cycles
// 2153 	{
// 2154 		RpSetStateRxOnToTrxOff();
        CALL      F:_RpSetStateRxOnToTrxOff  ;; 3 cycles
// 2155 		RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+2         ;; 2 cycles
// 2156 
// 2157 		/* Callback function execution Log */
// 2158 		RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
        MOV       X, #0x5            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2159 		/* Callback function execution */
// 2160 		INDIRECT_RpRxOffIndCallback();
        MOVW      HL, #LWRD(_RpCb+304)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall
        CALL      AX                 ;; 3 cycles
        BR        S:??RpAckCheckCallback_174  ;; 3 cycles
        ; ------------------------------------- Block: 24 cycles
// 2161 	}
// 2162 	else
// 2163 	{
// 2164 		if ((status & RP_PHY_STAT_RXON_BACKOFF) == 0)
??RpAckCheckCallback_173:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].0, ??RpAckCheckCallback_174  ;; 5 cycles
          CFI FunCall _RpRxOnStart
        ; ------------------------------------- Block: 9 cycles
// 2165 		{
// 2166 			RpRxOnStart();		// RX trigger
        CALL      F:_RpRxOnStart     ;; 3 cycles
// 2167 
// 2168 			/* Start Antenna select assist */
// 2169 			RpAntSelAssist_ReStartProc();
          CFI FunCall _RpAntSelAssist_ReStartProc
        CALL      F:_RpAntSelAssist_ReStartProc  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2170 		}
// 2171 	}
// 2172 }
??RpAckCheckCallback_174:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock22
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 64 cycles
// 2173 
// 2174 /******************************************************************************
// 2175 Function Name:       RpRcvHdrFunc
// 2176 Parameters:          rcvNo:received bank number: 0 or 1
// 2177 Return value:        none
// 2178 Description:         this function is called rcv0/1 interrupt handler.
// 2179 ******************************************************************************/
// 2180 static void RpRcvHdrFunc( uint8_t rcvNo )
// 2181 {
// 2182 	uint16_t	status;
// 2183 	uint16_t nowReadLen, readLen;
// 2184 
// 2185 	status = RpCb.status;
// 2186 	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
// 2187 	{
// 2188 		if (RpChkRcvBank(rcvNo) == RP_SUCCESS)
// 2189 		{
// 2190 			if (RpCb.rx.cnt != RP_PHY_RX_STAT_INIT)
// 2191 			{
// 2192 				if (RpCb.rx.softwareAdf ==  RP_FALSE)
// 2193 				{
// 2194 					RpReadRam(rcvNo);
// 2195 				}
// 2196 				else
// 2197 				{
// 2198 					nowReadLen = (uint16_t)(RpCb.rx.cnt % RP_BB_RX_RAM_SIZE);
// 2199 					nowReadLen = (uint16_t)(RP_BB_RX_RAM_SIZE - nowReadLen);
// 2200 					readLen = (uint16_t)(RpCb.rx.len - (RpCb.rx.cnt + nowReadLen));
// 2201 					if (readLen > RP_BB_RX_RAM_SIZE)
// 2202 					{
// 2203 						RpReadRam(rcvNo);
// 2204 					}
// 2205 					else
// 2206 					{
// 2207 						if (RpCb.rx.len != RP_BB_RX_RAM_SIZE)	// In case that
// 2208 						{
// 2209 							RpCb.rx.unreadRamPrev = RP_TRUE;
// 2210 						}
// 2211 					}
// 2212 				}
// 2213 			}
// 2214 			else
// 2215 			{
// 2216 				RpRcvRamEnable(rcvNo);
// 2217 			}
// 2218 		}
// 2219 	}
// 2220 }
// 2221 
// 2222 /******************************************************************************
// 2223 Function Name:       RpRcvRamEnable
// 2224 Parameters:          rcvxNos:received bank number: 0 or 1
// 2225 Return value:        none
// 2226 Description:         change recieve bank.
// 2227 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock23 Using cfiCommon0
          CFI Function _RpRcvRamEnable
        CODE
// 2228 static void RpRcvRamEnable(uint8_t rcvxNos)
// 2229 {
_RpRcvRamEnable:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 2
// 2230 	uint8_t tmpRegBuf = RpRegRead(BBTXRXST0);
        MOVW      AX, #0x38          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 2231 
// 2232 	if (rcvxNos == 0)
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        BNZ       ??RpAckCheckCallback_175  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
// 2233 	{
// 2234 		tmpRegBuf &= ~0x10;		// rcvbank0_tmp_regbuf(bit4) = 0
// 2235 		tmpRegBuf |= 0x20;		// rcvbank1_tmp_regbuf(bit5) = 1
        AND       A, #0xEF           ;; 1 cycle
        OR        A, #0x20           ;; 1 cycle
        BR        S:??RpAckCheckCallback_176  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2236 	}
// 2237 	else
// 2238 	{
// 2239 		tmpRegBuf &= ~0x20;		// rcvbank1_tmp_regbuf(bit5) = 0
// 2240 		tmpRegBuf |= 0x10;		// rcvbank0_tmp_regbuf(bit4) = 1
??RpAckCheckCallback_175:
        AND       A, #0xDF           ;; 1 cycle
        OR        A, #0x10           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpAckCheckCallback_176:
        MOV       C, A               ;; 1 cycle
// 2241 	}
// 2242 	RpRegWrite(BBTXRXST0, tmpRegBuf);
        MOVW      AX, #0x38          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2243 }
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock23
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 32 cycles
// 2244 
// 2245 /******************************************************************************
// 2246 Function Name:       RpReadRam
// 2247 Parameters:          rcvNo
// 2248 Return value:        none
// 2249 Description:         read receive RAMx.
// 2250 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock24 Using cfiCommon0
          CFI Function _RpReadRam
        CODE
// 2251 static void RpReadRam( uint8_t rcvNo )
// 2252 {
_RpReadRam:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 8
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
        MOV       D, A               ;; 1 cycle
// 2253 	uint16_t readLen, readAddrOffset;
// 2254 
// 2255 	readAddrOffset = (uint16_t)(RpCb.rx.cnt % RP_BB_RX_RAM_SIZE);
        MOVW      BC, #0x80          ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
          CFI FunCall ?SI_MOD_L02
        CALL      N:?SI_MOD_L02      ;; 3 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2256 	readLen = (uint16_t)(((RpCb.rx.cnt + (RP_BB_RX_RAM_SIZE - readAddrOffset)) < RpCb.rx.len) ?
// 2257 						 (RP_BB_RX_RAM_SIZE - readAddrOffset) : (RpCb.rx.len - RpCb.rx.cnt));
        MOVW      BC, ES:_RpCb+106   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        ADDW      AX, #0x80          ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        BNC       ??RpAckCheckCallback_177  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
        MOVW      AX, #0x80          ;; 1 cycle
        BR        S:??RpAckCheckCallback_178  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_177:
        MOVW      HL, ES:_RpCb+110   ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_178:
        SUBW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2258 	RpRegBlockRead((uint16_t)((rcvNo ? BBRXRAM1 : BBRXRAM0) + (readAddrOffset << 3)), (uint8_t *)&RpCb.rx.pTopRxBuf[RpCb.rx.cnt], readLen);	// Data Read from received RAMx
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_179  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOVW      AX, #0x1C00        ;; 1 cycle
        BR        S:??RpAckCheckCallback_180  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_179:
        MOVW      AX, #0x1800        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_180:
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, ES:_RpCb+110   ;; 2 cycles
        MOVW      DE, #LWRD(_RpCb+112)  ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 2259 	RpRcvRamEnable(rcvNo);
        MOV       A, [SP+0x02]       ;; 1 cycle
          CFI FunCall _RpRcvRamEnable
        CALL      F:_RpRcvRamEnable  ;; 3 cycles
// 2260 	RpCb.rx.cnt += readLen;
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        ADDW      AX, HL             ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
// 2261 }
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock24
        ; ------------------------------------- Block: 45 cycles
        ; ------------------------------------- Total: 88 cycles
// 2262 
// 2263 /******************************************************************************
// 2264 Function Name:       RpChkRcvBank
// 2265 Parameters:          rcvBankNo:
// 2266 Return value:        RP_SUCCESS:success RP_ERROR:no data on bankx
// 2267 Description:         process check recieving frame bank.
// 2268 ******************************************************************************/
// 2269 static int16_t RpChkRcvBank( uint8_t rcvBankNo )
// 2270 {
// 2271 	uint8_t rtn = RP_SUCCESS;
// 2272 	uint8_t txrxst0;
// 2273 
// 2274 	txrxst0 = RpRegRead(BBTXRXST0);
// 2275 	if (((rcvBankNo == RP_PHY_BANK_0) && ((txrxst0 & RCVRAMBANK0) == 0)) ||
// 2276 			((rcvBankNo == RP_PHY_BANK_1) && ((txrxst0 & RCVRAMBANK1) == 0)))
// 2277 	{
// 2278 		// bank no data
// 2279 		rtn = RP_ERROR;
// 2280 	}
// 2281 
// 2282 	return ((int16_t)rtn);
// 2283 }
// 2284 
// 2285 /* "Interrupt source number 14" */
// 2286 /***************************************************************************************************************
// 2287  * function name  : RpAdrsHdr
// 2288  * description    : Address filter interrupt handler
// 2289  * parameters     : none
// 2290  * return value   : none
// 2291  **************************************************************************************************************/
// 2292 static void RpAdrsHdr( void )
// 2293 {
// 2294 	RpSelectRcvDataBank();
// 2295 }
// 2296 
// 2297 /******************************************************************************
// 2298 Function Name:       RpSelectRcvDataBank
// 2299 Parameters:          none
// 2300 Return value:        none
// 2301 Description:         Select Rcvdatabank function.
// 2302 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock25 Using cfiCommon0
          CFI Function _RpSelectRcvDataBank
        CODE
// 2303 static void RpSelectRcvDataBank( void )
// 2304 {
_RpSelectRcvDataBank:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 12
        SUBW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+16
// 2305 	uint16_t status;
// 2306 	uint8_t	txrxst0;
// 2307 	uint8_t	txrxst2;
// 2308 	uint8_t curStaBank;
// 2309 	uint8_t prvSelBank;
// 2310 	uint8_t	rcvRamSt;
// 2311 	uint16_t sduLen;
// 2312 	uint32_t difTime;
// 2313 	uint32_t newRxOffTime;
// 2314 
// 2315 	status = RpCb.status;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2316 	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_181  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
// 2317 	{
// 2318 		RpCb.rx.cnt = RP_PHY_RX_STAT_PHR_DETECT;
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
// 2319 		txrxst0 = RpRegRead(BBTXRXST0);
        MOV       X, #0x38           ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 2320 		txrxst2 = RpRegRead(BBTXRXST2);
        MOVW      AX, #0x80          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
// 2321 		RpCb.rx.rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);
        MOV       A, [SP]            ;; 1 cycle
        AND       A, #0x80           ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+146, A    ;; 2 cycles
// 2322 		curStaBank = (uint8_t)((txrxst2 & RCVSTOREFLG) ? RP_PHY_BANK_1 : RP_PHY_BANK_0);
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV1      CY, [HL].1         ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ROLC      A, 0x1             ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 2323 		prvSelBank = (uint8_t)((RpCb.reg.bbTxRxMode3 & RCVSTOREBANKSEL) ? RP_PHY_BANK_1 : RP_PHY_BANK_0);
// 2324 		
// 2325 		if (curStaBank == prvSelBank)
        MOV       X, A               ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+356)  ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV1      CY, ES:[HL].4      ;; 2 cycles
        ROLC      A, 0x1             ;; 1 cycle
        CMP       X, A               ;; 1 cycle
        BNZ       ??RpAckCheckCallback_182  ;; 4 cycles
        ; ------------------------------------- Block: 36 cycles
// 2326 		{
// 2327 			if ((txrxst0 & RCVRAMBANK0) || (txrxst0 & RCVRAMBANK1))
        MOV       A, [SP]            ;; 1 cycle
        AND       A, #0x30           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BZ        ??RpAckCheckCallback_182  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2328 			{
// 2329 				/* Callback function execution Log */
// 2330 				RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_RXDELAY );
        MOV       X, #0x9            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2331 				/* Callback function execution */
// 2332 				INDIRECT_WarningIndCallback( RP_WARN_RX_PROCESS_DELAY );
        MOVW      HL, #LWRD(_RpCb+312)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x24           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
// 2333 
// 2334 				rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
// 2335 
// 2336 				if ((txrxst0 & RCVRAMBANK0) && (txrxst0 & RCVRAMBANK1))
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x30           ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        BNZ       ??RpAckCheckCallback_183  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
// 2337 				{
// 2338 					RpRcvRamEnable((~rcvRamSt) & RCVRAMST);
        XOR       A, #0xFF           ;; 1 cycle
        AND       A, #0x80           ;; 1 cycle
          CFI FunCall _RpRcvRamEnable
        ; ------------------------------------- Block: 2 cycles
// 2339 				}
// 2340 				else
// 2341 				{
// 2342 					RpRcvRamEnable(rcvRamSt);
??RpAckCheckCallback_183:
        CALL      F:_RpRcvRamEnable  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2343 				}
// 2344 			}
// 2345 		}
// 2346 
// 2347 		// Select Current RcvDataBank
// 2348 		if (curStaBank == RP_PHY_BANK_1)
??RpAckCheckCallback_182:
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_184  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
// 2349 		{
// 2350 			RpCb.reg.bbTxRxMode3 |= RCVSTOREBANKSEL;
        MOVW      HL, #LWRD(_RpCb+356)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
        BR        S:??RpAckCheckCallback_185  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 2351 		}
// 2352 		else
// 2353 		{
// 2354 			RpCb.reg.bbTxRxMode3 &= (~RCVSTOREBANKSEL);
??RpAckCheckCallback_184:
        MOVW      HL, #LWRD(_RpCb+356)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:[HL].4          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2355 		}
// 2356 		RpRegWrite(BBTXRXMODE3, RpCb.reg.bbTxRxMode3);
??RpAckCheckCallback_185:
        MOV       C, ES:_RpCb+356    ;; 2 cycles
        MOVW      AX, #0x50          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2357 
// 2358 		if (RpReadRxDataBank())
          CFI FunCall _RpReadRxDataBank
        CALL      F:_RpReadRxDataBank  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_186  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
// 2359 		{
// 2360 			if ((status & RP_PHY_STAT_RX_TMOUT) && (RpCb.pib.phyRxTimeoutMode == RP_TRUE))
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].2, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_181  ;; 5 cycles
        ; ------------------------------------- Block: 9 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+230, #0x1  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_181  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2361 			{			
// 2362 				RpCalcTotalBytes(RpCb.rx.lenNoFcs, &sduLen, RP_FALSE);
        CLRB      B                  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, ES:_RpCb+108   ;; 2 cycles
          CFI FunCall _RpCalcTotalBytes
        CALL      F:_RpCalcTotalBytes  ;; 3 cycles
// 2363 				newRxOffTime = (RpGetTime() + (sduLen << 3) + RP_RXOFF_MERGIN_TIME);
          CFI FunCall _RpGetTime
        CALL      F:_RpGetTime       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
// 2364 				newRxOffTime += (RpCb.pib.phyAckReplyTime + RP_PHY_TX_WARM_UP_TIME + (256 << 3));
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        MOVW      BC, #0x15E         ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, ES:_RpCb+220   ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, #0x806         ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
// 2365 				difTime = (RpCb.rx.timeout - newRxOffTime) & RP_TIME_MASK;
// 2366 
// 2367 				if ((difTime > RP_TIME_LIMIT) || (difTime < RP_RXTMOUT_TIMER_WASTE_TIME))
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+138)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, #0x3           ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x7FFF        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 119 cycles
        CMPW      AX, #0xFFFE        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSelectRcvDataBank_0:
        BC        ??RpAckCheckCallback_181  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2368 				{
// 2369 					RpCb.statusRxTimeout |= RP_STATUS_RX_TIMEOUT_EXTENTION;
        SET1      ES:_RpCb+2.1       ;; 3 cycles
// 2370 						
// 2371 					// RpCb.rx.timeout already unavailable and set TC1 for new timeout
// 2372 					RpRegBlockWrite(BBTCOMP1REG0, (uint8_t RP_FAR *)&newRxOffTime, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x140         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2373 					RpCb.rx.timeout = newRxOffTime;
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+138)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        BR        S:??RpAckCheckCallback_181  ;; 3 cycles
        ; ------------------------------------- Block: 27 cycles
// 2374 				}
// 2375 			}
// 2376 		}
// 2377 		else
// 2378 		{
// 2379 			RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
??RpAckCheckCallback_186:
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
// 2380 
// 2381 			RpRxOnStop();
          CFI FunCall _RpRxOnStop
        CALL      F:_RpRxOnStop      ;; 3 cycles
// 2382 			RpAvailableRcvRamEnable();
          CFI FunCall _RpAvailableRcvRamEnable
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
// 2383 			if ((status & RP_PHY_STAT_RXON_BACKOFF) == 0)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].0, ??RpAckCheckCallback_187  ;; 5 cycles
          CFI FunCall _RpRxOnStart
        ; ------------------------------------- Block: 19 cycles
// 2384 			{
// 2385 				RpRxOnStart();	// RX trigger
        CALL      F:_RpRxOnStart     ;; 3 cycles
// 2386 				RpCb.status = status;
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
// 2387 
// 2388 				/* Start Antenna select assist */
// 2389 				RpAntSelAssist_ReStartProc();
          CFI FunCall _RpAntSelAssist_ReStartProc
        CALL      F:_RpAntSelAssist_ReStartProc  ;; 3 cycles
        BR        S:??RpAckCheckCallback_181  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
// 2390 			}
// 2391 			else
// 2392 			{
// 2393 				RpResumeCsmaTrn(status, RpRegRead(BBCCATOTAL), RP_FALSE);
??RpAckCheckCallback_187:
        MOVW      AX, #0x6B0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       C, A               ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _RpResumeCsmaTrn
        CALL      F:_RpResumeCsmaTrn  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
// 2394 			}
// 2395 		}
// 2396 	}
// 2397 }
??RpAckCheckCallback_181:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock25
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 338 cycles
// 2398 
// 2399 /******************************************************************************
// 2400 Function Name:       RpReadRxDataBank
// 2401 Parameters:          none
// 2402 Return value:        RP_FALSE:illegal frame length
// 2403 Description:         read out Rx data bank.
// 2404 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock26 Using cfiCommon0
          CFI Function _RpReadRxDataBank
        CODE
// 2405 static int16_t RpReadRxDataBank( void )
// 2406 {
_RpReadRxDataBank:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 12
        SUBW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+16
// 2407 	int16_t rtn, len;
// 2408 	uint8_t adrfDet1 = 0, adrfDet2 = 0, adrfTmp, adrfRd;
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 2409 	uint16_t sduLen;
// 2410 	uint32_t flameLenIntTimeStamp;
// 2411 	uint8_t ackResponsTimeMs;
// 2412 	uint16_t ackResponsTimeBit;
// 2413 
// 2414 	RpRegBlockRead(BBRXFLEN, (uint8_t *)&len, sizeof(uint16_t));
        MOV       X, #0x2            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x500         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 2415 	RpCb.rx.len = len;
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+106, AX   ;; 2 cycles
// 2416 	RpCb.rx.phrRx = (uint8_t)RpRegRead(BBPHRRX);
        MOVW      AX, #0x5F0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+148, A    ;; 2 cycles
// 2417 	if ((((RpCb.pib.macAddressFilter1Ena) || (RpCb.pib.macAddressFilter2Ena)) && (len < RP_aMinPHYPacketSize)) ||
// 2418 			(len > RP_aMaxPHYPacketSize) || (RpCb.rx.waitRelRxBufWhenAutoRx) || (RpCb.rx.phrRx & 0x07))
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        CMP0      ES:_RpCb+176       ;; 2 cycles
        BNZ       ??RpAckCheckCallback_188  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
        CMP0      ES:_RpCb+192       ;; 2 cycles
        BZ        ??RpAckCheckCallback_189  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_188:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xFFFB        ;; 1 cycle
        CMPW      AX, #0x63C         ;; 1 cycle
        BC        ??RpAckCheckCallback_190  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        BR        R:??RpAckCheckCallback_191  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_189:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8641        ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_191  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
??RpAckCheckCallback_190:
        CMP0      ES:_RpCb+144       ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_191  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, ES:_RpCb+148    ;; 2 cycles
        AND       A, #0x7            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_191  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2419 	{
// 2420 		rtn = RP_FALSE;
// 2421 	}
// 2422 	else
// 2423 	{
// 2424 		RpCb.rx.fcsLength = (uint8_t)((RpCb.rx.phrRx & FCSTYPE) ? RP_FCS_LENGTH_16BIT : RP_FCS_LENGTH_32BIT);
        MOVW      HL, #LWRD(_RpCb+148)  ;; 1 cycle
        BF        ES:[HL].3, ??RpAckCheckCallback_192  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, #0x2            ;; 1 cycle
        BR        S:??RpAckCheckCallback_193  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_192:
        MOV       A, #0x4            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_193:
        MOV       ES:_RpCb+134, A    ;; 2 cycles
// 2425 		RpCb.rx.lenNoFcs = (uint16_t)(RpCb.rx.len - RpCb.rx.fcsLength);
        MOV       X, ES:_RpCb+134    ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, ES:_RpCb+106   ;; 2 cycles
        SUBW      AX, HL             ;; 1 cycle
        MOVW      ES:_RpCb+108, AX   ;; 2 cycles
// 2426 
// 2427 		if (RpCb.rx.softwareAdf == RP_FALSE)
        CMP0      ES:_RpCb+143       ;; 2 cycles
        BNZ       ??RpAckCheckCallback_194  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
// 2428 		{
// 2429 			adrfRd = RpRegRead(BBADFCON);
        MOVW      AX, #0x868         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
// 2430 			if (adrfRd & ADFMONI1)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].2, ??RpAckCheckCallback_195  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
// 2431 			{
// 2432 				adrfDet1 = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2433 			}
// 2434 			if (adrfRd & ADFMONI2)
??RpAckCheckCallback_195:
        BF        [HL].3, ??RpAckCheckCallback_196  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 2435 			{
// 2436 				adrfDet2 = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2437 			}
// 2438 			if ((RpCb.pib.macAddressFilter1Ena == RP_FALSE) && (RpCb.pib.macAddressFilter2Ena == RP_TRUE))
??RpAckCheckCallback_196:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+176       ;; 2 cycles
        BNZ       ??RpAckCheckCallback_197  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_RpCb+192, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_198  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2439 			{
// 2440 				adrfTmp = adrfDet2;
// 2441 				adrfDet2 = adrfDet1;
        MOV       A, [SP]            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 2442 				adrfDet1 = adrfTmp;
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpAckCheckCallback_199  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 2443 			}
// 2444 			if (RpCb.pib.macAddressFilter1Ena == RP_FALSE)
// 2445 			{
// 2446 				adrfDet1 = RP_FALSE;
??RpAckCheckCallback_198:
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2447 			}
// 2448 			if (RpCb.pib.macAddressFilter2Ena == RP_FALSE)
??RpAckCheckCallback_197:
        CMP0      ES:_RpCb+192       ;; 2 cycles
        BNZ       ??RpAckCheckCallback_199  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2449 			{
// 2450 				adrfDet2 = RP_FALSE;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2451 			}
// 2452 			adrfDet2 <<= 1;
// 2453 			RpCb.rx.filteredAdress = (uint8_t)(adrfDet1 | adrfDet2);
??RpAckCheckCallback_199:
        MOV       A, [SP+0x01]       ;; 1 cycle
        SHL       A, 0x1             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       ES:_RpCb+137, A    ;; 2 cycles
        ; ------------------------------------- Block: 7 cycles
// 2454 		}
// 2455 		RpRegBlockRead(BBTSTAMP0, (uint8_t *)&flameLenIntTimeStamp, sizeof(uint32_t));
??RpAckCheckCallback_194:
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x180         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 2456 		RpCalcTotalBytes(RpCb.rx.lenNoFcs, &sduLen, RP_FALSE);
        CLRB      B                  ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+108   ;; 2 cycles
          CFI FunCall _RpCalcTotalBytes
        CALL      F:_RpCalcTotalBytes  ;; 3 cycles
// 2457 		RpCb.rx.timeStamp = flameLenIntTimeStamp - RP_PHY_FLINT_DELAY_TIME;	
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        SUBW      AX, #0x28          ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        MOVW      HL, #LWRD(_RpCb+116)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2458 		RpCb.rx.timeAcKRtn = RpCb.rx.timeStamp + (sduLen << 3) + RpCb.pib.phyAckReplyTime - RP_PHY_TX_WARM_UP_TIME;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      [SP], AX           ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      DE, #0x3E8         ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        MOVW      BC, #0x15E         ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, ES:_RpCb+220   ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+116)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+120)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2459 
// 2460 		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        CMP       ES:_RpCb+235, #0x2  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_200  ;; 4 cycles
        ; ------------------------------------- Block: 123 cycles
        CMP       ES:_RpCb+234, #0x9  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_200  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2461 		{
// 2462 			if ((RpCb.pib.phyFskOpeMode == RP_PHY_FSK_OPEMODE_1) || (RpCb.pib.phyFskOpeMode == RP_PHY_FSK_OPEMODE_6))
        CMP       ES:_RpCb+218, #0x1  ;; 2 cycles
        BZ        ??RpAckCheckCallback_201  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpCb+218, #0x6  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_202  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2463 			{
// 2464 				ackResponsTimeMs = RP_ACK_RESPONSE_SINGLE_CH_LIMIT;
??RpAckCheckCallback_201:
        MOV       X, #0x32           ;; 1 cycle
        BR        S:??RpAckCheckCallback_203  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2465 			}
// 2466 			else
// 2467 			{
// 2468 				ackResponsTimeMs = RP_ACK_RESPONSE_MULTIPLE_CH_LIMIT;
??RpAckCheckCallback_202:
        MOV       X, #0x5            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2469 			}
// 2470 			ackResponsTimeBit = (uint16_t)ackResponsTimeMs * RpCb.pib.phyDataRate;
// 2471 			RpCb.tx.ackCompLimitTimeBit = flameLenIntTimeStamp + (sduLen << 3) - RP_PHY_FLINT_DELAY_TIME + ackResponsTimeBit;
??RpAckCheckCallback_203:
        MOVW      BC, ES:_RpCb+232   ;; 2 cycles
        CLRB      A                  ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        SUBW      AX, #0x28          ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        MOVW      HL, #LWRD(_RpCb+98)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 40 cycles
// 2472 		}
// 2473 
// 2474 		rtn = RP_TRUE;
??RpAckCheckCallback_200:
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2475 	}
// 2476 
// 2477 	return (rtn);
??RpAckCheckCallback_191:
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock26
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 351 cycles
// 2478 }
// 2479 
// 2480 /******************************************************************************
// 2481 Function Name:       RpDetRcvdAntenna
// 2482 Parameters:          none
// 2483 Return value:        received anntenna
// 2484 Description:         determine received anntenna.
// 2485 ******************************************************************************/
// 2486 static uint8_t RpDetRcvdAntenna( void )
// 2487 {
// 2488 	return ((uint8_t)((RpRegRead(BBANTDIV2) & ANTMONI) ? RP_PHY_ANTENNA_1 : RP_PHY_ANTENNA_0));
// 2489 }
// 2490 
// 2491 /* "Interrupt source number 8" */
// 2492 /***************************************************************************************************************
// 2493  * function name  : RpRcv0Hdr
// 2494  * description    : CCA completion interrupt handler
// 2495  * parameters     : none
// 2496  * return value   : none
// 2497  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock27 Using cfiCommon0
          CFI Function _RpCcaHdr
        CODE
// 2498 static void RpCcaHdr( void )
// 2499 {
_RpCcaHdr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 8
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
// 2500 	uint16_t	status;
// 2501 	uint8_t	stCfm, val;
// 2502 	uint16_t	rssi;
// 2503 	uint16_t wk16;
// 2504 	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+247    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 2505 	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
        MOV       A, ES:_RpCb+246    ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
// 2506 	uint8_t index = RpCb.freqIdIndex;
        MOV       A, ES:_RpCb+342    ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 2507 
// 2508 	status = RpCb.status;
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 2509 
// 2510 	if ((status & (RP_PHY_STAT_TX | RP_PHY_STAT_TX_CCA)) == (RP_PHY_STAT_TX | RP_PHY_STAT_TX_CCA))
        MOV       A, X               ;; 1 cycle
        AND       A, #0x82           ;; 1 cycle
        CMP       A, #0x82           ;; 1 cycle
        BNZ       ??RpAckCheckCallback_204  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
// 2511 	{
// 2512 		RpInverseTxAnt(RP_FALSE);
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
// 2513 		RpCb.tx.onCsmaCa = RP_FALSE;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+86        ;; 2 cycles
        ; ------------------------------------- Block: 7 cycles
// 2514 	}
// 2515 
// 2516 	if ((status & (RP_PHY_STAT_TX | RP_PHY_STAT_TX_CCA | RP_PHY_STAT_TRX_ACK)) == (RP_PHY_STAT_TX | RP_PHY_STAT_TX_CCA))
??RpAckCheckCallback_204:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        AND       A, #0x2            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x82           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        CMPW      AX, #0x82          ;; 1 cycle
        BNZ       ??RpAckCheckCallback_205  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 2517 	{
// 2518 		if ((RpRegRead(BBTXRXST0)) & CSMACA)
        MOVW      AX, #0x38          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].2, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_206  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
// 2519 		{
// 2520 			RpCcaAck.result = RP_CHANNEL_BUSY;
        MOV       ES, #BYTE3(_RpCcaAck)  ;; 1 cycle
        MOV       ES:_RpCcaAck+1, #0x5  ;; 2 cycles
// 2521 			RpTrnHdrFunc();
          CFI FunCall _RpTrnHdrFunc
        CALL      F:_RpTrnHdrFunc    ;; 3 cycles
        BR        R:??RpAckCheckCallback_206  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 2522 		}
// 2523 	}
// 2524 	else
// 2525 	{
// 2526 		RpCb.reg.bbIntEn[0] &= ~CCAINTEN;
??RpAckCheckCallback_205:
        CLR1      ES:_RpCb+344.7     ;; 3 cycles
// 2527 		RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2528 
// 2529 		if ((index != RP_PHY_100K_M10_JP) && (index != RP_PHY_100K_M10_IN)) // except for 100kbps m=1
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x9            ;; 1 cycle
        BZ        ??RpAckCheckCallback_207  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        CMP       A, #0xA            ;; 1 cycle
        BZ        ??RpAckCheckCallback_207  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2530 		{
// 2531 			if (((status & RP_PHY_STAT_CCA) && ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K))) ||
// 2532 				((status & RP_PHY_STAT_ED) && ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K))))
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].2, ??RpAckCheckCallback_208  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpAckCheckCallback_209  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0x2            ;; 1 cycle
        BZ        ??RpAckCheckCallback_209  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpAckCheckCallback_208:
        BF        [HL].3, ??RpAckCheckCallback_207  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpAckCheckCallback_209  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_207  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2533 			{
// 2534 				if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
// 2535 					((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
??RpAckCheckCallback_209:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+235, #0x1  ;; 2 cycles
        BZ        ??RpAckCheckCallback_210  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_RpCb+235, #0x2  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_211  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpCb+234, #0x9  ;; 2 cycles
        SKNZ                         ;; 1 cycle
          CFI FunCall _RpPrevSentTimeReSetting
        ; ------------------------------------- Block: 3 cycles
// 2536 				{
// 2537 					RpPrevSentTimeReSetting();
??RpAckCheckCallback_210:
        CALL      F:_RpPrevSentTimeReSetting  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2538 				}
// 2539 
// 2540 				RpCb.reg.bbTimeCon &= ~(TIMEEN);// Timer Count Disable, Comp0 transmit Disable
??RpAckCheckCallback_211:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+352.0     ;; 3 cycles
// 2541 				RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOV       C, ES:_RpCb+352    ;; 2 cycles
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2542 
// 2543 				RpRegTxRxDataRateDefault();
          CFI FunCall _RpRegTxRxDataRateDefault
        CALL      F:_RpRegTxRxDataRateDefault  ;; 3 cycles
// 2544 
// 2545 				RpCb.reg.bbTimeCon |= TIMEEN;	// Timer Count Start, Comp0 transmit Disable Stay
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb+352.0     ;; 3 cycles
// 2546 				RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOV       C, ES:_RpCb+352    ;; 2 cycles
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 23 cycles
// 2547 			}
// 2548 		}
// 2549 
// 2550 		if (status & RP_PHY_STAT_CCA)
??RpAckCheckCallback_207:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].2, ??RpAckCheckCallback_212  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 2551 		{
// 2552 			// CCA mode
// 2553 			RpCb.status = RP_PHY_STAT_TRX_OFF;
        MOVW      AX, #0x10          ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
// 2554 			if ((RpRegRead(BBTXRXST0) & CCA) == 0)
        MOV       X, #0x38           ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        BT        [HL].0, ??RpAckCheckCallback_213  ;; 5 cycles
        ; ------------------------------------- Block: 15 cycles
// 2555 			{
// 2556 				stCfm = RP_IDLE;							// idle channel
        MOV       A, #0x4            ;; 1 cycle
        BR        S:??RpAckCheckCallback_214  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2557 			}
// 2558 			else
// 2559 			{
// 2560 				// busy channel
// 2561 				stCfm = RP_BUSY;							// busy channel
??RpAckCheckCallback_213:
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_214:
        MOV       [SP], A            ;; 1 cycle
// 2562 			}
// 2563 
// 2564 			/* Callback function execution Log */
// 2565 			RpLog_Event( RP_LOG_CB, RP_LOG_CB_CCACFM );
        MOV       X, #0x3            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2566 			/* Callback function execution */
// 2567 			INDIRECT_RpPlmeCcaCfmCallback( stCfm );
        MOV       A, [SP]            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+296)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
        BR        R:??RpAckCheckCallback_206  ;; 3 cycles
        ; ------------------------------------- Block: 23 cycles
// 2568 		}
// 2569 		else if (status & RP_PHY_STAT_ED)
??RpAckCheckCallback_212:
        BT        [HL].3, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_206  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 2570 		{
// 2571 			// ED mode
// 2572 			RpCb.status = RP_PHY_STAT_TRX_OFF;
        MOVW      AX, #0x10          ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
// 2573 			
// 2574 			RpRegBlockRead(BBRSSICCARSLT, (uint8_t *)&rssi, sizeof(uint16_t));
        MOV       X, #0x2            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x20          ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 2575 
// 2576 			switch (RpCb.pib.phyEdBandwidth)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+247    ;; 2 cycles
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_215  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpAckCheckCallback_216  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BR        S:??RpAckCheckCallback_217  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2577 			{
// 2578 				case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
// 2579 					wk16 = RpCcaVthOffsetTblDefault[index];
??RpAckCheckCallback_215:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpCcaVthOffsetTblDefault)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCcaVthOffsetTblDefault)  ;; 1 cycle
        BR        S:??RpAckCheckCallback_218  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
// 2580 					break;
// 2581 				case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
// 2582 				case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
// 2583 					wk16 = 0x0000;
??RpAckCheckCallback_216:
        CLRW      AX                 ;; 1 cycle
// 2584 					break;
        BR        S:??RpAckCheckCallback_219  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2585 				case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
// 2586 				default:
// 2587 					wk16 = RpCcaVthOffsetTblNarrow[index];
??RpAckCheckCallback_217:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpCcaVthOffsetTblNarrow)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCcaVthOffsetTblNarrow)  ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
??RpAckCheckCallback_218:
        MOVW      AX, ES:[HL]        ;; 2 cycles
// 2588 					break;
        ; ------------------------------------- Block: 2 cycles
// 2589 			}
// 2590 			rssi -= wk16;
// 2591 			rssi -= RpCb.pib.phyRssiOutputOffset;
// 2592 			rssi &= 0x01FF;
??RpAckCheckCallback_219:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, ES:_RpCb+282    ;; 2 cycles
        XCH       A, L               ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 2593 
// 2594 			/* Callback function execution Log */
// 2595 			RpLog_Event( RP_LOG_CB, RP_LOG_CB_CALCLQI );
        MOV       X, #0x6            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2596 			/* Callback function execution */
// 2597 			val = INDIRECT_RpCalcLqiCallback( rssi, RP_FALSE );
        CLRB      C                  ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+316)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
          CFI FunCall
        CALL      DE                 ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 2598 
// 2599 			stCfm = RP_SUCCESS;
// 2600 			/* Callback function execution Log */
// 2601 			RpLog_Event( RP_LOG_CB, RP_LOG_CB_EDCFM );
        MOV       X, #0x4            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2602 			/* Callback function execution */
// 2603 			INDIRECT_RpPlmeEdCfmCallback( stCfm , val, rssi );
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0x7            ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+300)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
          CFI FunCall
        CALL      HL                 ;; 3 cycles
        ; ------------------------------------- Block: 57 cycles
// 2604 		}
// 2605 	}
// 2606 }
??RpAckCheckCallback_206:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock27
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 331 cycles
// 2607 
// 2608 /* "Interrupt source number 15" */
// 2609 /***************************************************************************************************************
// 2610  * function name  : RpRovrHdr
// 2611  * description    : Receive overrun interrupt handler
// 2612  * parameters     : none
// 2613  * return value   : none
// 2614  **************************************************************************************************************/
// 2615 static void RpRovrHdr( void )
// 2616 {
// 2617 	uint16_t	status;
// 2618 
// 2619 	status = RpCb.status;
// 2620 	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
// 2621 	{
// 2622 		/* Callback function execution Log */
// 2623 		RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_RXOVERRUN );
// 2624 		/* Callback function execution */
// 2625 		INDIRECT_WarningIndCallback( RP_WARN_RXRAMOVERRUN );
// 2626 
// 2627 		RpRfStat.rxRamOverrun++;
// 2628 		RpAvailableRcvRamEnable();
// 2629 		if ((status & RP_PHY_STAT_RXON_BACKOFF) == 0)
// 2630 		{
// 2631 			RpRxOnStart();		// RX trigger
// 2632 
// 2633 			/* Start Antenna select assist */
// 2634 			RpAntSelAssist_ReStartProc();
// 2635 		}
// 2636 	}
// 2637 }
// 2638 
// 2639 /* "Interrupt source number 16" */
// 2640 /***************************************************************************************************************
// 2641  * function name  : RpRcvModeswHdr
// 2642  * description    : Mode switch receive completion interrupt handler
// 2643  * parameters     : none
// 2644  * return value   : none
// 2645  **************************************************************************************************************/
// 2646 static void RpRcvModeswHdr( void )
// 2647 {
// 2648 	uint16_t	status;
// 2649 
// 2650 	status = RpCb.status;
// 2651 	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
// 2652 	{
// 2653 		RpAvailableRcvRamEnable();
// 2654 		if ((status & RP_PHY_STAT_RXON_BACKOFF) == 0)
// 2655 		{
// 2656 			RpRxOnStart();		// RX trigger
// 2657 
// 2658 			/* Start Antenna select assist */
// 2659 			RpAntSelAssist_ReStartProc();
// 2660 		}
// 2661 	}
// 2662 }
// 2663 
// 2664 /* "Interrupt source number 17" */
// 2665 /***************************************************************************************************************
// 2666  * function name  : RpRcvLvlIntHdr
// 2667  * description    : Receive level filter interrupt handler
// 2668  * parameters     : none
// 2669  * return value   : none
// 2670  **************************************************************************************************************/
// 2671 static void RpRcvLvlIntHdr( void )
// 2672 {
// 2673 	uint16_t	status;
// 2674 	uint8_t		statusRxTimeout;
// 2675 
// 2676 	status = RpCb.status;
// 2677 	statusRxTimeout = RpCb.statusRxTimeout;
// 2678 
// 2679 	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
// 2680 	{
// 2681 		if (statusRxTimeout & RP_STATUS_RX_TIMEOUT_EXTENTION)
// 2682 		{
// 2683 			RpSetStateRxOnToTrxOff();
// 2684 			RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;
// 2685 
// 2686 			/* Callback function execution Log */
// 2687 			RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
// 2688 			/* Callback function execution */
// 2689 			INDIRECT_RpRxOffIndCallback();
// 2690 		}
// 2691 		else
// 2692 		{
// 2693 			RpAvailableRcvRamEnable();
// 2694 			RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
// 2695 
// 2696 			/* Start Antenna select assist */
// 2697 			RpAntSelAssist_ReStartProc();
// 2698 		}
// 2699 	}
// 2700 }
// 2701 
// 2702 /* "Interrupt source number 19" */
// 2703 /***************************************************************************************************************
// 2704  * function name  : RpFlenHdr
// 2705  * description    : Frame length interrupt handler
// 2706  * parameters     : none
// 2707  * return value   : none
// 2708  **************************************************************************************************************/
// 2709 static void RpFlenHdr( void )
// 2710 {
// 2711 	RpSelectRcvDataBank();
// 2712 }
// 2713 
// 2714 /* "Interrupt source number 18" */
// 2715 /***************************************************************************************************************
// 2716  * function name  : RpRcvCuntHdr
// 2717  * description    : Receive byte counts interrupt handler
// 2718  * parameters     : none
// 2719  * return value   : none
// 2720  **************************************************************************************************************/
// 2721 static void RpRcvCuntHdr( void )
// 2722 {
// 2723 	RpReadAddressInfo();
// 2724 }
// 2725 
// 2726 /******************************************************************************
// 2727 Function Name:       RpReadAddressInfo
// 2728 Parameters:          none
// 2729 Return value:        none
// 2730 Description:         Read Address Information.
// 2731 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock28 Using cfiCommon0
          CFI Function _RpReadAddressInfo
        CODE
// 2732 static void RpReadAddressInfo( void )
// 2733 {
_RpReadAddressInfo:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 2734 	uint16_t readLen;
// 2735 
// 2736 	if (RpCb.rx.cnt == RP_PHY_RX_STAT_PHR_DETECT)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??RpAckCheckCallback_220  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 2737 	{
// 2738 		if (RpCb.rx.len < RP_PHY_RX_ADDRESS_DECODE_LEN)
        MOVW      AX, ES:_RpCb+106   ;; 2 cycles
        CMPW      AX, #0x1B          ;; 1 cycle
        SKC                          ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
// 2739 		{
// 2740 			readLen = RpCb.rx.len;
// 2741 		}
// 2742 		else
// 2743 		{
// 2744 			readLen = RP_PHY_RX_ADDRESS_DECODE_LEN;
        MOVW      AX, #0x1B          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpReadAddressInfo_0:
        MOVW      [SP], AX           ;; 1 cycle
// 2745 		}
// 2746 		RpRegBlockRead((uint16_t)(RpCb.rx.rcvRamSt ? BBRXRAM0 : BBRXRAM1),	// Inverse here
// 2747 					   (uint8_t *)&RpCb.rx.pTopRxBuf[RP_PHY_RX_STAT_PHR_DETECT], readLen);
        CMP0      ES:_RpCb+146       ;; 2 cycles
        BZ        ??RpAckCheckCallback_221  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      AX, #0x1800        ;; 1 cycle
        BR        S:??RpAckCheckCallback_222  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_221:
        MOVW      AX, #0x1C00        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_222:
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      HL, #LWRD(_RpCb+112)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+8
        MOV       C, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, C               ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 2748 		RpCb.rx.cnt += readLen;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+110   ;; 2 cycles
        ADDW      AX, HL             ;; 1 cycle
        MOVW      ES:_RpCb+110, AX   ;; 2 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+6
        ; ------------------------------------- Block: 28 cycles
// 2749 	}
// 2750 }
??RpAckCheckCallback_220:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock28
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 61 cycles
// 2751 
// 2752 /******************************************************************************
// 2753  * Notice About Reception
// 2754  *****************************************************************************/
// 2755 /* "Interrupt source number 13" */
// 2756 /***************************************************************************************************************
// 2757  * function name  : RpRcvStHdr
// 2758  * description    : Start reception interrupt handler
// 2759  * parameters     : none
// 2760  * return value   : none
// 2761  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock29 Using cfiCommon0
          CFI Function _RpRcvStHdr
        CODE
// 2762 static void RpRcvStHdr( void )
// 2763 {
_RpRcvStHdr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 2764 	uint32_t time;
// 2765 	uint8_t evacon0 = RpRegRead(BBCOMSTATE);
        MOVW      AX, #0x330         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 2766 	uint16_t status;
// 2767 
// 2768 	/* get current status */
// 2769 	status = RpCb.status;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
// 2770 	/* Stop Antenna selection assistance */
// 2771 	RpAntSelAssist_StopProc( status );
          CFI FunCall _RpAntSelAssist_StopProc
        CALL      F:_RpAntSelAssist_StopProc  ;; 3 cycles
// 2772 
// 2773 	if (evacon0 & FRCVSTATE)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].3, ??RpAckCheckCallback_223  ;; 5 cycles
          CFI FunCall _RpGetTime
        ; ------------------------------------- Block: 19 cycles
// 2774 	{
// 2775 		time = RpGetTime() + RP_PHY_BYTE_INT_LIMIT_DELAY;
        CALL      F:_RpGetTime       ;; 3 cycles
        ADDW      AX, #0x50          ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2776 		RpRegBlockWrite(BBTCOMP2REG0, (uint8_t RP_FAR *)&time, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x160         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2777 		RpCb.reg.bbIntEn[0] |= TIM2INTEN;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb+344.2     ;; 3 cycles
// 2778 		RpCb.reg.bbIntEn[2] |= BYTERCVINTEN;
        SET1      ES:_RpCb+346.6     ;; 3 cycles
// 2779 		RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2780 		RpRegWrite(BBINTEN2, (uint8_t)(RpCb.reg.bbIntEn[2]));
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       C, ES:_RpCb+346    ;; 2 cycles
        MOVW      AX, #0x1D8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        ; ------------------------------------- Block: 45 cycles
// 2781 	}
// 2782 }
??RpAckCheckCallback_223:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock29
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 71 cycles
// 2783 
// 2784 /* "Interrupt source number 20" */
// 2785 /***************************************************************************************************************
// 2786  * function name  : RpByteRcvHdr
// 2787  * description    : Byte reception interrupt handler
// 2788  * parameters     : none
// 2789  * return value   : none
// 2790  **************************************************************************************************************/
// 2791 static void RpByteRcvHdr( void )
// 2792 {
// 2793 	RpCb.reg.bbIntEn[0] &= ~TIM2INTEN;
// 2794 	RpCb.reg.bbIntEn[2] &= ~BYTERCVINTEN;
// 2795 	RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
// 2796 	RpRegWrite(BBINTEN2, (uint8_t)(RpCb.reg.bbIntEn[2]));
// 2797 }
// 2798 
// 2799 /* "Interrupt source number 3" */
// 2800 /***************************************************************************************************************
// 2801  * function name  : RpBbTim2Hdr
// 2802  * description    : Timer compare 2 interrupt handler
// 2803  * parameters     : none
// 2804  * return value   : none
// 2805  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock30 Using cfiCommon0
          CFI Function _RpBbTim2Hdr
        CODE
// 2806 static void RpBbTim2Hdr( void )
// 2807 {
_RpBbTim2Hdr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 2808 	uint8_t reg0x04E3;
// 2809 	uint16_t status;
// 2810 
// 2811 	/* bbtimer2 interrupt disable */
// 2812 	RpCb.reg.bbIntEn[0] &= ~TIM2INTEN;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+344.2     ;; 3 cycles
// 2813 	RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2814 
// 2815  	if ((RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE) &&
// 2816 		(RpCb.pib.phyDataRate == RP_PHY_ANTDV_DATARATE)) // AntennaDiver and 100kbps only
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+226, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_224  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        CMPW      AX, #0x64          ;; 1 cycle
        BNZ       ??RpAckCheckCallback_224  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2817  	{
// 2818 		status = RpCb.status;
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 2819 
// 2820 		/* get regvalue */
// 2821 		reg0x04E3 = RpRegRead(0x04E3 << 3);
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       B, A               ;; 1 cycle
// 2822 
// 2823 		if ( status & RP_PHY_STAT_ANTENNA_SELECT )
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        BF        [HL].6, ??RpAckCheckCallback_225  ;; 5 cycles
        ; ------------------------------------- Block: 17 cycles
// 2824 		{
// 2825 			/* before reception start interrupt occurs */
// 2826 			if ( reg0x04E3 == 0x22 )
        CMP       A, #0x22           ;; 1 cycle
        BNZ       ??RpAckCheckCallback_226  ;; 4 cycles
          CFI FunCall _RpAntSelAssist_TimerProc
        ; ------------------------------------- Block: 5 cycles
// 2827 			{
// 2828 				/* Antenna selection assistance */
// 2829 				RpAntSelAssist_TimerProc();
        CALL      F:_RpAntSelAssist_TimerProc  ;; 3 cycles
        BR        S:??RpAckCheckCallback_227  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2830 			}
// 2831 			else
// 2832 			{
// 2833 				INDIRECT_WarningIndCallback( reg0x04E3 );	// Warning
??RpAckCheckCallback_226:
        MOVW      HL, #LWRD(_RpCb+312)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
// 2834 
// 2835 				// Safty proc
// 2836 				RpRegWrite(BBTXRXRST, RFSTOP);	// RF Stop
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x8           ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2837 				RpAntSelAssist_StopProc( status );
        MOVW      AX, [SP]           ;; 1 cycle
          CFI FunCall _RpAntSelAssist_StopProc
        CALL      F:_RpAntSelAssist_StopProc  ;; 3 cycles
// 2838 				RpAntSelAssist_StartProc();
          CFI FunCall _RpAntSelAssist_StartProc
        CALL      F:_RpAntSelAssist_StartProc  ;; 3 cycles
// 2839 				RpRegWrite(BBTXRXCON, RCVTRG);	// RX Start
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        BR        S:??RpAckCheckCallback_227  ;; 3 cycles
        ; ------------------------------------- Block: 32 cycles
// 2840 			}
// 2841 		}
// 2842 		else
// 2843 		{
// 2844 			switch ( reg0x04E3 )
??RpAckCheckCallback_225:
        SUB       A, #0x74           ;; 1 cycle
        BZ        ??RpAckCheckCallback_224  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x14           ;; 1 cycle
        BZ        ??RpAckCheckCallback_224  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2845 			{
// 2846 				case 0x88:	// antsel on
// 2847 				case 0x74:	// antsel off
// 2848 					break;
// 2849 				default:
// 2850 					INDIRECT_WarningIndCallback( reg0x04E3 );	// Warning
        MOVW      HL, #LWRD(_RpCb+312)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
// 2851 					break;
          CFI FunCall _RpDumPrcRcvPhr
        ; ------------------------------------- Block: 12 cycles
// 2852 			}
// 2853 
// 2854 			RpDumPrcRcvPhr();
// 2855 		}
// 2856  	}
// 2857 	/* after the reception start interrupt occurs */
// 2858 	else
// 2859 	{
// 2860 		RpDumPrcRcvPhr();
??RpAckCheckCallback_224:
        CALL      F:_RpDumPrcRcvPhr  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2861 	}
// 2862 }
??RpAckCheckCallback_227:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock30
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 117 cycles
// 2863 
// 2864 /***********************************************************************
// 2865  * function name  : RpDumPrcRcvPhr
// 2866  * description    : Dummy process receiving PHR.
// 2867  * parameters     : none
// 2868  * return value   : none
// 2869  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock31 Using cfiCommon0
          CFI Function _RpDumPrcRcvPhr
        CODE
// 2870 static void RpDumPrcRcvPhr( void )
// 2871 {
_RpDumPrcRcvPhr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 2872 	RpCb.reg.bbIntReq = 0;
        MOVW      HL, #LWRD(_RpCb+348)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2873 	RpRegWrite(0x0077 << 3,	0x05);
        MOV       C, #0x5            ;; 1 cycle
        MOVW      AX, #0x3B8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2874 	RpCb.reg.bbIntReq = RpReadIrq();
          CFI FunCall _RpReadIrq
        CALL      F:_RpReadIrq       ;; 3 cycles
        MOVW      HL, #LWRD(_RpCb+348)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2875 	if (RpCb.reg.bbIntReq & RP_BBBYTE_IREQ)
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        ES:[HL].6, ??RpAckCheckCallback_228  ;; 5 cycles
          CFI FunCall _RpReStartProc
        ; ------------------------------------- Block: 29 cycles
// 2876 	{
// 2877 		RpReStartProc();
        CALL      F:_RpReStartProc   ;; 3 cycles
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 9 cycles
// 2878 	}
// 2879 	else
// 2880 	{
// 2881 		RpRegWrite(0x0077 << 3, 0x06);
??RpAckCheckCallback_228:
        MOV       C, #0x6            ;; 1 cycle
        MOVW      AX, #0x3B8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2882 		RpRegWrite(0x0060 << 3,	0x0D);
        MOV       C, #0xD            ;; 1 cycle
        MOVW      AX, #0x300         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2883 		RpRegWrite(0x0063 << 3,	0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x318         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2884 		RpRegWrite(0x0063 << 3,	0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x318         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2885 		RpRegWrite(0x0060 << 3,	0x01);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x300         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2886 		RpRegWrite(0x0077 << 3,	0x07);
        MOV       C, #0x7            ;; 1 cycle
        MOVW      AX, #0x3B8         ;; 1 cycle
        BR        F:?Subroutine2     ;; 3 cycles
          CFI EndBlock cfiBlock31
        ; ------------------------------------- Block: 30 cycles
        ; ------------------------------------- Total: 68 cycles
// 2887 	}
// 2888 }
// 2889 
// 2890 /***********************************************************************
// 2891  * function name  : RpReStartProc
// 2892  * description    : restart process function.
// 2893  * parameters     : none
// 2894  * return value   : none
// 2895  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock32 Using cfiCommon0
          CFI Function _RpReStartProc
        CODE
// 2896 static void RpReStartProc( void )
// 2897 {
_RpReStartProc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 2898 	uint16_t status;
// 2899 	uint8_t ccaTotal, csmacon1, csmacon3;
// 2900 
// 2901 	status = RpCb.status;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 2902 	ccaTotal = RpRegRead(BBCCATOTAL);
        MOVW      AX, #0x6B0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 2903 	RpRxOnStop();
          CFI FunCall _RpRxOnStop
        CALL      F:_RpRxOnStop      ;; 3 cycles
// 2904 	RpRegWrite(0x0077 << 3, 0x07);
        MOV       C, #0x7            ;; 1 cycle
        MOVW      AX, #0x3B8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2905 	if (status & RP_PHY_STAT_RX)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_229  ;; 5 cycles
          CFI FunCall _RpRxOnStart
        ; ------------------------------------- Block: 26 cycles
// 2906 	{
// 2907 		RpRxOnStart();			// RX trigger
        CALL      F:_RpRxOnStart     ;; 3 cycles
// 2908 		RpCb.status = status;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
// 2909 
// 2910 		/* Start Antenna select assist */
// 2911 		RpAntSelAssist_ReStartProc();
          CFI FunCall _RpAntSelAssist_ReStartProc
        CALL      F:_RpAntSelAssist_ReStartProc  ;; 3 cycles
        BR        R:??RpAckCheckCallback_230  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
// 2912 	}
// 2913 	else if (status & RP_PHY_STAT_TX)
??RpAckCheckCallback_229:
        BT        [HL].1, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_230  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 2914 	{
// 2915 		if (RpCb.tx.onCsmaCa) // Rx on backoffperiod
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+86        ;; 2 cycles
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_231  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2916 		{
// 2917 			RpCb.tx.ccaTimes += ccaTotal;
        MOV       X, ES:_RpCb+67     ;; 2 cycles
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, X               ;; 1 cycle
        MOV       ES:_RpCb+67, A     ;; 2 cycles
// 2918 			RpCb.tx.ccaTimesOneFrame += ccaTotal;
        MOV       X, ES:_RpCb+87     ;; 2 cycles
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, X               ;; 1 cycle
        MOV       ES:_RpCb+87, A     ;; 2 cycles
// 2919 			csmacon1 = RpCb.reg.bbCsmaCon1;
// 2920 			csmacon1 &= (~NB);
        MOV       A, ES:_RpCb+359    ;; 2 cycles
        AND       A, #0xF8           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
// 2921 			if (RpCb.pib.macMaxCsmaBackOff >= RpCb.tx.ccaTimesOneFrame)
        MOV       B, ES:_RpCb+208    ;; 2 cycles
        MOV       A, ES:_RpCb+87     ;; 2 cycles
        CMP       B, A               ;; 1 cycle
        BC        ??RpAckCheckCallback_232  ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
// 2922 			{
// 2923 				csmacon1 |= (RpCb.pib.macMaxCsmaBackOff - RpCb.tx.ccaTimesOneFrame);
        MOV       A, B               ;; 1 cycle
        SUB       A, ES:_RpCb+87     ;; 2 cycles
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 2924 			}
// 2925 			RpCb.reg.bbCsmaCon1 = csmacon1;
??RpAckCheckCallback_232:
        MOV       A, X               ;; 1 cycle
        MOV       ES:_RpCb+359, A    ;; 2 cycles
// 2926 			RpRegWrite(BBCSMACON1, RpCb.reg.bbCsmaCon1);
        MOV       C, ES:_RpCb+359    ;; 2 cycles
        MOVW      AX, #0x90          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2927 			csmacon3 = RpRegRead(BBCSMACON3);
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 2928 			csmacon3 &= (~BEMIN);
// 2929 			csmacon3 |= (RpCb.tx.ccaTimesOneFrame + RpCb.pib.macMinBe);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+87     ;; 2 cycles
        ADD       A, ES:_RpCb+209    ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0xF0           ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
// 2930 			if (csmacon3 > RpCb.pib.macMaxBe)
        MOV       A, ES:_RpCb+210    ;; 2 cycles
        CMP       A, C               ;; 1 cycle
        SKNC                         ;; 1 cycle
        ; ------------------------------------- Block: 28 cycles
// 2931 			{
// 2932 				csmacon3 = RpCb.pib.macMaxBe;
        MOV       C, ES:_RpCb+210    ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 2933 			}
// 2934 			RpRegWrite(BBCSMACON3, csmacon3);
??RpReStartProc_0:
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2935 			RpCb.reg.bbCsmaCon0 |= CSMAST;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb+358.0     ;; 3 cycles
// 2936 			RpRegWrite(BBCSMACON0, RpCb.reg.bbCsmaCon0);	// TX trigger
        MOV       C, ES:_RpCb+358    ;; 2 cycles
        MOVW      AX, #0x68          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2937 			RpCb.status = status;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb, AX       ;; 2 cycles
        BR        S:??RpAckCheckCallback_230  ;; 3 cycles
        ; ------------------------------------- Block: 21 cycles
// 2938 		}
// 2939 		else // Rx on Ack Receiving
// 2940 		{
// 2941 			// sequence complete NG
// 2942 			RpUpdateTxTime(1, RP_FALSE, RP_FALSE);
??RpAckCheckCallback_231:
        CLRB      C                  ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpUpdateTxTime
        CALL      F:_RpUpdateTxTime  ;; 3 cycles
// 2943 			RpCancelTrn(status);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpCancelTrn
        CALL      F:_RpCancelTrn     ;; 3 cycles
// 2944 			if (RpDriverRetryTrn(status) == RP_FALSE)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpDriverRetryTrn
        CALL      F:_RpDriverRetryTrn  ;; 3 cycles
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_230  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 2945 			{
// 2946 				if (status & RP_PHY_STAT_TX_CCA)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].7, ??RpAckCheckCallback_233  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 2947 				{
// 2948 					RpCb.tx.ccaTimes += ccaTotal;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+67     ;; 2 cycles
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, X               ;; 1 cycle
        MOV       ES:_RpCb+67, A     ;; 2 cycles
        ; ------------------------------------- Block: 7 cycles
// 2949 				}
// 2950 
// 2951 				/* Callback function execution Log */
// 2952 				RpLog_Event( RP_LOG_CB, RP_LOG_CB_DATACFM );
??RpAckCheckCallback_233:
        MOV       X, #0x2            ;; 1 cycle
        MOV       A, #0xF0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2953 				/* Callback function execution */
// 2954 				INDIRECT_RpPdDataCfmCallback( RP_NO_ACK, RP_NO_FRMPENBIT_ACK, RpCb.tx.ccaTimes );
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       C, ES:_RpCb+67     ;; 2 cycles
        CLRB      X                  ;; 1 cycle
        MOV       A, #0xF2           ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+292)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        POP       AX                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall
        CALL      HL                 ;; 3 cycles
        ; ------------------------------------- Block: 23 cycles
// 2955 			}
// 2956 		}
// 2957 	}
// 2958 }
??RpAckCheckCallback_230:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock32
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 196 cycles
// 2959 
// 2960 /******************************************************************************
// 2961  * Antenna selection assistance function
// 2962  *****************************************************************************/
// 2963 /***********************************************************************
// 2964  * function name  : RpAntSelAssist_StartProc
// 2965  * description    : Start of antenna selection assistance process
// 2966  * parameters     : none
// 2967  * return value   : none
// 2968  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock33 Using cfiCommon0
          CFI Function _RpAntSelAssist_StartProc
        CODE
// 2969 void RpAntSelAssist_StartProc( void )
// 2970 {
_RpAntSelAssist_StartProc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 2971 	uint16_t wk16;
// 2972 
// 2973  	if ((RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE) &&
// 2974 		( RpCb.pib.phyDataRate == RP_PHY_ANTDV_DATARATE )) // AntennaDiver and 100kbps only
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP       ES:_RpCb+226, #0x1  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_234  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        CMPW      AX, #0x64          ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_234  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2975 	{
// 2976 		RpCb.status |= RP_PHY_STAT_ANTENNA_SELECT;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].6          ;; 3 cycles
// 2977 
// 2978 		wk16 = 0x0198;
        MOVW      AX, #0x198         ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 2979 		RpRegBlockWrite(BBCCAVTH, (uint8_t RP_FAR *)&(wk16), 2);
        MOVW      HL, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       C, #0xF            ;; 1 cycle
        MOV       X, #0x70           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2980 		wk16 = 0x0007;
        MOVW      AX, #0x7           ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 2981 		RpRegBlockWrite(BBCCATIME, (uint8_t RP_FAR *)&(wk16), 2);
        MOV       X, #0x2            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x590         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2982 		RpRegBlockWrite(BBANTDIVTIM, (uint8_t RP_FAR *)&(wk16), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x670         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2983 		wk16 = 0xFFFF;
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 2984 		RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(wk16), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x680         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2985 		RpRegWrite(0x042D << 3, 0x06);
        MOV       C, #0x6            ;; 1 cycle
        MOVW      AX, #0x2168        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2986 		RpRegWrite(0x04E2 << 3, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x2710        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2987 		RpRegRead(0x0408 << 3); 		// Dummy Read
        MOVW      AX, #0x2040        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
// 2988 		RpRegWrite(0x04E3 << 3, 0x88);
        MOV       C, #0x88           ;; 1 cycle
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2989 		RpRegWrite(0x04E4 << 3, 0x88);
        MOV       C, #0x88           ;; 1 cycle
        MOVW      AX, #0x2720        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2990 		wk16 = 0x0198;
        MOVW      AX, #0x198         ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2991 		RpRegBlockWrite(0x04E7 << 3, (uint8_t RP_FAR *)&(wk16), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2738        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2992 		RpRegWrite(0x04F4 << 3, 0x06);
        MOV       C, #0x6            ;; 1 cycle
        MOVW      AX, #0x27A0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2993 		RpRegWrite(0x04F9 << 3, 0x03);
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0x27C8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2994 		RpRegWrite(0x04FC << 3, 0x01);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x27E0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2995 		RpRegWrite(0x0513 << 3, 0x0E);
        MOV       C, #0xE            ;; 1 cycle
        MOVW      AX, #0x2898        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2996 		RpReadIrq();
          CFI FunCall _RpReadIrq
        CALL      F:_RpReadIrq       ;; 3 cycles
// 2997 
// 2998 		RpCb.reg.bbIntEn[2] |= 0x80;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb+346.7     ;; 3 cycles
// 2999 		RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);
        MOV       C, ES:_RpCb+346    ;; 2 cycles
        MOVW      AX, #0x1D8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+10
        ; ------------------------------------- Block: 121 cycles
// 3000 	}
// 3001 }
??RpAckCheckCallback_234:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock33
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 143 cycles
// 3002 
// 3003 /***********************************************************************
// 3004  * function name  : RpAntSelAssist_StopProc
// 3005  * description    : Stop processing for antenna selection assistance
// 3006  * parameters     : none
// 3007  * return value   : none
// 3008  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock34 Using cfiCommon0
          CFI Function _RpAntSelAssist_StopProc
        CODE
// 3009 void RpAntSelAssist_StopProc( uint16_t status )
// 3010 {
_RpAntSelAssist_StopProc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 2
// 3011 	if ( status & RP_PHY_STAT_ANTENNA_SELECT )
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].6, ??RpAckCheckCallback_235  ;; 5 cycles
        ; ------------------------------------- Block: 9 cycles
// 3012 	{
// 3013 		RpCb.status &= ~RP_PHY_STAT_ANTENNA_SELECT;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        CLR1      ES:[HL].6          ;; 3 cycles
// 3014 
// 3015 		/* bbtimer2 interrupt disable */
// 3016 		RpCb.reg.bbIntEn[0] &= ~TIM2INTEN;
        CLR1      ES:_RpCb+344.2     ;; 3 cycles
// 3017 		RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3018 
// 3019 		RpCb.reg.bbIntEn[2] &= ~(0x80);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+346.7     ;; 3 cycles
// 3020 		RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);
        MOV       C, ES:_RpCb+346    ;; 2 cycles
        MOVW      AX, #0x1D8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3021 
// 3022 		RpRegWrite(0x04E3 << 3, 0x88);
        MOV       C, #0x88           ;; 1 cycle
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 30 cycles
// 3023 	}
// 3024 }
??RpAckCheckCallback_235:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock34
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 46 cycles
// 3025 
// 3026 /***********************************************************************
// 3027  * function name  : RpAntSelAssist_RecoveryProc
// 3028  * description    : Recovery processing for antenna selection assistance
// 3029  * parameters     : none
// 3030  * return value   : none
// 3031  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock35 Using cfiCommon0
          CFI Function _RpAntSelAssist_RecoveryProc
        CODE
// 3032 void RpAntSelAssist_RecoveryProc( void )
// 3033 {
_RpAntSelAssist_RecoveryProc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3034 	uint8_t wk;
// 3035 
// 3036 	wk = RpRegRead(0x04E3 << 3);
// 3037 
// 3038 	if ( wk != 0x74 )
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        CMP       A, #0x74           ;; 1 cycle
        BZ        ??RpAckCheckCallback_236  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 3039 	{
// 3040 		RpSetPowerMode(RP_RF_LOWPOWER);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetPowerMode
        CALL      F:_RpSetPowerMode  ;; 3 cycles
// 3041 		RpSetPowerMode(RP_RF_IDLE);
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetPowerMode
        CALL      F:_RpSetPowerMode  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 3042 	}
// 3043 }
??RpAckCheckCallback_236:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock35
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 23 cycles
// 3044 
// 3045 /***********************************************************************
// 3046  * function name  : RpAntSelAssist_StateCheckHdr
// 3047  * description    : State processing for antenna selection assistance
// 3048  * parameters     : none
// 3049  * return value   : none
// 3050  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock36 Using cfiCommon0
          CFI Function _RpAntSelAssist_StateCheckHdr
        CODE
// 3051 static void RpAntSelAssist_StateCheckHdr( void )
// 3052 {
_RpAntSelAssist_StateCheckHdr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 3053 	uint8_t reg0x04E3;
// 3054 	uint32_t futureTime;
// 3055 
// 3056 	#if defined(__RX)
// 3057 	uint32_t bkupPsw2;
// 3058 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 3059 	uint8_t  bkupPsw2;
// 3060 	#elif defined(__arm)
// 3061 	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
// 3062 	#endif
// 3063 
// 3064 	/* Disable interrupt */
// 3065 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 3066 	RP_PHY_ALL_DI(bkupPsw2);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 3067 	#else
// 3068 	RP_PHY_ALL_DI();
// 3069 	#endif
// 3070 
// 3071 	reg0x04E3 = RpRegRead(0x04E3 << 3);
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 3072 
// 3073 	if ( reg0x04E3 == 0x88 )
        CMP       A, #0x88           ;; 1 cycle
        BNZ       ??RpAckCheckCallback_237  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
// 3074 	{
// 3075 		RpCb.reg.bbIntEn[2] &= ~(0x80);
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+346.7     ;; 3 cycles
// 3076 		RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);
        MOV       C, ES:_RpCb+346    ;; 2 cycles
        MOVW      AX, #0x1D8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3077 
// 3078 		RpRegRead(0x0408 << 3); 		// Dummy Read					
        MOVW      AX, #0x2040        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
// 3079 		RpRegWrite(0x04E3 << 3, 0x22);
        MOV       C, #0x22           ;; 1 cycle
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3080 		RpRegRead(0x0408 << 3); 		// Dummy Read
        MOVW      AX, #0x2040        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
// 3081 
// 3082 		// timer set
// 3083 		futureTime = RpGetTime() + (RpCb.rx.antdvTimerValue/10);
          CFI FunCall _RpGetTime
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      DE, #0xA           ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+150   ;; 2 cycles
        DIVHU                        ;; 9 cycles
        NOP                          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 3084 		RpRegBlockWrite(BBTCOMP2REG0, (uint8_t RP_FAR *)&futureTime, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x160         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3085 		
// 3086 		// timer start
// 3087 		RpCb.reg.bbIntEn[0] |= TIM2INTEN;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb+344.2     ;; 3 cycles
// 3088 		RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3089 
// 3090 		RpRegWrite(0x04FC << 3, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x27E0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        BR        S:??RpAckCheckCallback_238  ;; 3 cycles
        ; ------------------------------------- Block: 85 cycles
// 3091 	}
// 3092 	else // if ( reg0x04E3 == 0x94 )
// 3093 	{
// 3094 		RpRegWrite(BBTXRXRST, RFSTOP);	// RF Stop
??RpAckCheckCallback_237:
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x8           ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3095 		RpRegWrite(0x04E2 << 3, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x2710        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3096 		RpRegWrite(0x04E3 << 3, 0x88);
        MOV       C, #0x88           ;; 1 cycle
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3097 		RpRegRead(0x0408 << 3); 		// Dummy Read
        MOVW      AX, #0x2040        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
// 3098 		RpRegWrite(0x04FC << 3, 0x01);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x27E0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3099 		RpRegWrite(BBTXRXCON, RCVTRG);	// RX Start
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 29 cycles
// 3100 	}
// 3101 
// 3102 	/* set execution log */
// 3103 	RpLog_Event(RP_DEBUG_IRQ_MODEM_HDR, reg0x04E3);
??RpAckCheckCallback_238:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xD0           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
          CFI EndBlock cfiBlock36
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 147 cycles
// 3104 
// 3105 	/* Enable interrupt */
// 3106 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 3107 	RP_PHY_ALL_EI(bkupPsw2);
        REQUIRE ?Subroutine1
        ; // Fall through to label ?Subroutine1
// 3108 	#else
// 3109 	RP_PHY_ALL_EI();
// 3110 	#endif
// 3111 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock37 Using cfiCommon0
          CFI NoFunction
          CFI CFA SP+10
        CODE
?Subroutine1:
        MOV       A, [SP+0x01]       ;; 1 cycle
          CFI FunCall _RpAntSelAssist_StateCheckHdr ___set_psw
          CFI FunCall _RpAntSelAssist_TimerProc ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock37
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 11 cycles
// 3112 
// 3113 /***********************************************************************
// 3114  * function name  : RpAntSelAssist_TimerProc
// 3115  * description    : Timer processing for antenna selection assistance
// 3116  * parameters     : none
// 3117  * return value   : none
// 3118  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock38 Using cfiCommon0
          CFI Function _RpAntSelAssist_TimerProc
        CODE
// 3119 static void RpAntSelAssist_TimerProc( void )
// 3120 {
_RpAntSelAssist_TimerProc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 3121 	uint8_t reg0x04FA;
// 3122 
// 3123 	#if defined(__RX)
// 3124 	uint32_t bkupPsw2;
// 3125 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 3126 	uint8_t  bkupPsw2;
// 3127 	#elif defined(__arm)
// 3128 	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
// 3129 	#endif
// 3130 
// 3131 	/* Disable interrupt */
// 3132 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 3133 	RP_PHY_ALL_DI(bkupPsw2);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 3134 	#else
// 3135 	RP_PHY_ALL_DI();
// 3136 	#endif
// 3137 
// 3138 	RpCb.reg.bbIntReq = 0;
        MOVW      HL, #LWRD(_RpCb+348)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3139 
// 3140 	reg0x04FA = RpRegRead(0x04FA << 3);
        MOVW      AX, #0x27D0        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 3141 
// 3142 	if ( 3 <= reg0x04FA )
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpAckCheckCallback_239  ;; 4 cycles
        ; ------------------------------------- Block: 34 cycles
// 3143 	{
// 3144 		RpLog_Event(RP_DEBUG_IRQ_MODEM_TIMER, reg0x04FA);
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xD1           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 3145 		RpRegWrite(0x04E2 << 3, 0x06);
        MOV       C, #0x6            ;; 1 cycle
        MOVW      AX, #0x2710        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3146 		RpRegWrite(0x04E3 << 3, 0x94);
        MOV       C, #0x94           ;; 1 cycle
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3147 		RpCb.reg.bbIntReq |= RpReadIrq();
          CFI FunCall _RpReadIrq
        CALL      F:_RpReadIrq       ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 3148 		RpCb.reg.bbIntReq &= ~(RP_BBMODEVA_IREQ);
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, #LWRD(_RpCb+348)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        XCH       A, C               ;; 1 cycle
        AND       A, #0x7F           ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3149 
// 3150 		if ( reg0x04FA < 5 )
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x5            ;; 1 cycle
        BNC       ??RpAckCheckCallback_240  ;; 4 cycles
        ; ------------------------------------- Block: 48 cycles
// 3151 		{
// 3152 			reg0x04FA = RpRegRead(0x04FA << 3);
        MOVW      AX, #0x27D0        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 3153 		}
// 3154 	}
// 3155 
// 3156 	if ( reg0x04FA < 3 )
??RpAckCheckCallback_239:
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BNC       ??RpAckCheckCallback_240  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3157 	{
// 3158 		RpRegWrite(BBTXRXRST, RFSTOP);	// RF Stop
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x8           ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3159 		RpRegWrite(0x04E2 << 3, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x2710        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3160 		RpRegWrite(0x04E3 << 3, 0x88);
        MOV       C, #0x88           ;; 1 cycle
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3161 		RpCb.reg.bbIntReq |= RpReadIrq();
          CFI FunCall _RpReadIrq
        CALL      F:_RpReadIrq       ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 3162 		RpCb.reg.bbIntReq &= ~(RP_BBMODEVA_IREQ);
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, #LWRD(_RpCb+348)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        XCH       A, C               ;; 1 cycle
        AND       A, #0x7F           ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3163 		RpRegRead(0x0408 << 3); 		// Dummy Read
        MOVW      AX, #0x2040        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
// 3164 		RpRegWrite(0x04FC << 3, 0x01);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x27E0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3165 		RpRegWrite(BBTXRXCON, RCVTRG);	// RX Start
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3166 		RpLog_Event(RP_DEBUG_IRQ_MODEM_TIMER, reg0x04FA);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xD1           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        ; ------------------------------------- Block: 62 cycles
// 3167 	}
// 3168 
// 3169 	RpCb.reg.bbIntEn[2] |= 0x80;
??RpAckCheckCallback_240:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb+346.7     ;; 3 cycles
// 3170 	RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);
        MOV       C, ES:_RpCb+346    ;; 2 cycles
        MOVW      AX, #0x1D8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3171 
// 3172 	/* Enable interrupt */
// 3173 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 3174 	RP_PHY_ALL_EI(bkupPsw2);
        BR        F:?Subroutine1     ;; 3 cycles
          CFI EndBlock cfiBlock38
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 168 cycles
// 3175 	#else
// 3176 	RP_PHY_ALL_EI();
// 3177 	#endif
// 3178 }
// 3179 
// 3180 /***********************************************************************
// 3181  * function name  : RpAntSelAssist_ReStartProc
// 3182  * description    : Start of antenna selection assistance process
// 3183  * parameters     : none
// 3184  * return value   : none
// 3185  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock39 Using cfiCommon0
          CFI Function _RpAntSelAssist_ReStartProc
        CODE
// 3186 static void RpAntSelAssist_ReStartProc( void )
// 3187 {
_RpAntSelAssist_ReStartProc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3188 	uint16_t status;
// 3189 
// 3190 	/* get current status */
// 3191 	status = RpCb.status;
// 3192 
// 3193 	if ( status & RP_PHY_STAT_RX )
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        CMPW      AX, #0x1           ;; 1 cycle
        BNZ       ??RpAckCheckCallback_241  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 3194 	{
// 3195 		if (( status & RP_PHY_STAT_RXON_BACKOFF ) == 0 )
// 3196 		{
// 3197 			if (( RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE )
// 3198 				&&( RpCb.pib.phyDataRate == RP_PHY_ANTDV_DATARATE ))
        CMP       ES:_RpCb+226, #0x1  ;; 2 cycles
        BNZ       ??RpAckCheckCallback_241  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        CMPW      AX, #0x64          ;; 1 cycle
        BNZ       ??RpAckCheckCallback_241  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 3199 			{
// 3200 				RpRegWrite(BBTXRXRST, RFSTOP);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x8           ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3201 
// 3202 				/* bbtimer2 interrupt disable */
// 3203 				RpCb.reg.bbIntEn[0] &= ~TIM2INTEN;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLR1      ES:_RpCb+344.2     ;; 3 cycles
// 3204 				RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpCb+344    ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3205 
// 3206 				RpRegWrite(0x04E2 << 3, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x2710        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3207 				RpRegWrite(0x04E3 << 3, 0x88);
        MOV       C, #0x88           ;; 1 cycle
        MOVW      AX, #0x2718        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3208 
// 3209 				RpCb.status |= RP_PHY_STAT_ANTENNA_SELECT;
        MOVW      HL, #LWRD(_RpCb)   ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].6          ;; 3 cycles
// 3210 
// 3211 				RpRegWrite(0x04FC << 3, 0x01);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x27E0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3212 				RpRegWrite(BBTXRXCON, RCVTRG);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3213 
// 3214 				RpCb.reg.bbIntEn[2] |= 0x80;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        SET1      ES:_RpCb+346.7     ;; 3 cycles
// 3215 				RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);
        MOV       C, ES:_RpCb+346    ;; 2 cycles
        MOVW      AX, #0x1D8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 51 cycles
// 3216 			}
// 3217 		}
// 3218 	}
// 3219 }
??RpAckCheckCallback_241:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock39
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 82 cycles
// 3220 
// 3221 /******************************************************************************
// 3222  * H/W address filter assistance function
// 3223  *****************************************************************************/
// 3224 /***************************************************************************************************************
// 3225  * function name  : RpAckCheckCallback
// 3226  * description    : The frame header structure and ACK advisability check callback function
// 3227  * parameters     : pData...The pointer to receive buffer.
// 3228  *                : dataLen...Size of the received data.
// 3229  *                : pAck...The pointer to ACK frame to be sent back
// 3230  *                : pAckLen...The pointer to the area of the ACK frame length
// 3231  * return value   : RP_INVALID_FRAME...Not to be received
// 3232  *                : RP_VALID_FRAME...Valid frame but not need reply ACK.
// 3233  *                : RP_NEED_ACK | RP_VALID_FRAME...Valid frame and need reply ACK
// 3234  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock40 Using cfiCommon1
          CFI Function _RpAckCheckCallback
        CODE
// 3235 uint8_t RpAckCheckCallback(uint8_t *pData, uint16_t dataLen, uint8_t *pAck, uint16_t *pAckLen)
// 3236 {
_RpAckCheckCallback:
        ; * Stack frame (at entry) *
        ; Param size: 8
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 50
        SUBW      SP, #0x2C          ;; 1 cycle
          CFI CFA SP+54
// 3237     uint8_t *srcAddInfoPtr;
// 3238     RpMacFrameControlT frameControl;
// 3239     uint8_t dstAddrMode;
// 3240     uint8_t srcAddrMode;
// 3241     uint16_t panId;
// 3242     uint8_t retVal = RP_INVALID_FRAME;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
// 3243     uint8_t noUpDataScndAckLen = RP_FALSE;
// 3244     uint8_t frmVersion;
// 3245     uint16_t frmType;
// 3246     uint16_t ackDataCnt;
// 3247     uint8_t dstPanExist = RP_FALSE, srcPanExist = RP_FALSE;
        MOV       [SP+0x03], A       ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
// 3248     uint16_t panCompression;
// 3249     uint8_t ackReq;
// 3250     uint8_t sqnNoSuppress;
// 3251     uint16_t refPanId, chkPanId;
// 3252     uint32_t refExtAddr[2];
// 3253     uint8_t refPanCoord;
// 3254     uint8_t refFramePend;
// 3255     uint8_t *bkupPtrData = pData;
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+56
        POP       HL                 ;; 1 cycle
          CFI CFA SP+54
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x16], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x14], AX      ;; 1 cycle
// 3256     uint8_t *bkupPtrAck = pAck;
// 3257     uint32_t extAddr[2];
// 3258 
// 3259     pData = bkupPtrData;
// 3260     pAck = bkupPtrAck;
// 3261     ackDataCnt = 2;
        MOVW      AX, #0x2           ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 3262 
// 3263     if (RpCb.pib.macAddressFilter1Ena == RP_FALSE)
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+176       ;; 2 cycles
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
// 3264     {
// 3265         return retVal;
// 3266     }
// 3267     refPanId = RpCb.pib.macPanId1;
        MOVW      AX, ES:_RpCb+180   ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 3268     refExtAddr[0] = RpCb.pib.macExtendedAddress1[0];
        MOVW      HL, #LWRD(_RpCb+182)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x20], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x22], AX      ;; 1 cycle
// 3269     refExtAddr[1] = RpCb.pib.macExtendedAddress1[1];
        MOVW      HL, #LWRD(_RpCb+186)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x1C], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x1E], AX      ;; 1 cycle
// 3270     refPanCoord = RpCb.pib.macPanCoord1;
        MOV       A, ES:_RpCb+190    ;; 2 cycles
        MOV       [SP+0x12], A       ;; 1 cycle
// 3271     refFramePend = RpCb.pib.macPendBit1;
        MOV       A, ES:_RpCb+191    ;; 2 cycles
        MOV       [SP+0x0B], A       ;; 1 cycle
// 3272 
// 3273     if (noUpDataScndAckLen == RP_FALSE)
// 3274     {
// 3275         *pAckLen = 0x03;
        MOV       A, [SP+0x3C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x3A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, #0x3           ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 3276         RP_VAL_UINT16_TO_ARRAY(RP_FRM_FrameType_Acknowledgment, pAck);
        MOV       A, [SP+0x38]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0x2            ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        INCW      HL                 ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3277     }
// 3278 
// 3279     // frame controle decode
// 3280     *((uint16_t *)(&frameControl)) = RP_VAL_ARRAY_TO_UINT16(pData);
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+56
        POP       HL                 ;; 1 cycle
          CFI CFA SP+54
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 3281     frmVersion = frameControl.frameVersion;
        SHRW      AX, 0xC            ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 3282     frmType = frameControl.frameType;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        AND       A, #0x7            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
// 3283     dstAddrMode = frameControl.dstAddrMode;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        SHRW      AX, 0xA            ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
// 3284     srcAddrMode = frameControl.srcAddrMode;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        SHRW      AX, 0xE            ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 3285     panCompression = frameControl.panIdCompression;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        SHR       A, 0x6             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
// 3286     ackReq = frameControl.ackRequest;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        SHR       A, 0x5             ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
// 3287     if (frmVersion == RP_MAC2012_FRAME_VERSION)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_243  ;; 4 cycles
        ; ------------------------------------- Block: 91 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV1      CY, [HL].0         ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ROLC      A, 0x1             ;; 1 cycle
        MOV       C, A               ;; 1 cycle
// 3288     {
// 3289         sqnNoSuppress = frameControl.sequenceNumberSuppress;
// 3290     }
// 3291     else
// 3292     {
// 3293         sqnNoSuppress = RP_FALSE;
// 3294     }
// 3295 
// 3296     if (frmVersion == RP_MAC2012_FRAME_VERSION)
// 3297     {
// 3298         if (panCompression)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, ??RpAckCheckCallback_244  ;; 5 cycles
        ; ------------------------------------- Block: 16 cycles
// 3299         {
// 3300             if (dstAddrMode == 0 && srcAddrMode == 0)
// 3301             {
// 3302                 dstPanExist = RP_TRUE;
// 3303             }
// 3304         }
// 3305         else
// 3306         {
// 3307             if (dstAddrMode != 0)
        CMP0      B                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_245  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??RpAckCheckCallback_246  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3308             {
// 3309                 dstPanExist = RP_TRUE;
// 3310             }
// 3311             else
// 3312             {
// 3313                 if (srcAddrMode != 0)
// 3314                 {
// 3315                     srcPanExist = RP_TRUE;
// 3316                 }
// 3317             }
// 3318         }
??RpAckCheckCallback_243:
        CLRB      C                  ;; 1 cycle
// 3319     }
// 3320     else
// 3321     {
// 3322         if ((panCompression && dstAddrMode != 0) && (srcAddrMode != 0))
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, ??RpAckCheckCallback_247  ;; 5 cycles
        ; ------------------------------------- Block: 9 cycles
// 3323         {
// 3324             dstPanExist = RP_TRUE;
// 3325         }
// 3326         else
// 3327         {
// 3328             if (dstAddrMode != 0)
        CMP0      B                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_246  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3329             {
// 3330                 dstPanExist = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x03], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 3331             }
// 3332             if (srcAddrMode != 0 && panCompression == 0)
??RpAckCheckCallback_246:
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_248  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3333             {
// 3334                 srcPanExist = RP_TRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 3335             }
// 3336         }
// 3337     }
// 3338     pData += sizeof(uint16_t);
??RpAckCheckCallback_248:
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+56
        POP       HL                 ;; 1 cycle
          CFI CFA SP+54
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x30], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x2E], AX      ;; 1 cycle
// 3339     // invalid  FrameType check
// 3340     if (frmType > RP_FRM_FrameType_MACCommand)
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        CMPW      AX, #0x4           ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 3341     {
// 3342         return retVal;
// 3343     }
// 3344     if (frmVersion > RP_MAC2012_FRAME_VERSION)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3345     {
// 3346         return retVal;
// 3347     }
// 3348     else if ((frmVersion == RP_MAC2003_FRAME_VERSION) || (frmVersion == RP_MAC2011_FRAME_VERSION))
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_249  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_250  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3349     {
// 3350         // invalid  PANID check
// 3351         if ((dstPanExist == 0) && (srcPanExist == 0))
??RpAckCheckCallback_249:
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOV       A, [SP+0x03]       ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 3352         {
// 3353             return retVal;
// 3354         }
// 3355         // invalid  Address Mode check
// 3356         if ((dstAddrMode == RP_AddressingMode_No_Address) && (srcAddrMode == RP_AddressingMode_No_Address))
        CMP0      B                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_251  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_252  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BR        R:??RpAckCheckCallback_242  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_244:
        CMP0      B                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_248  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_248  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BR        S:??RpAckCheckCallback_245  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_247:
        CMP0      B                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_248  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpAckCheckCallback_245:
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x03], A       ;; 1 cycle
        BR        S:??RpAckCheckCallback_248  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3357         {
// 3358             return retVal;
// 3359         }
// 3360         // invalid  Address Mode check
// 3361         if ((dstAddrMode == 0x0001) || (srcAddrMode == 0x0001))
??RpAckCheckCallback_251:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_252:
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_253  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BR        R:??RpAckCheckCallback_242  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3362         {
// 3363             return retVal;
// 3364         }
// 3365     }
// 3366     else if (frmVersion == RP_MAC2012_FRAME_VERSION)
??RpAckCheckCallback_250:
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_253  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3367     {
// 3368         // invalid  Address Mode check
// 3369         if ((dstAddrMode == RP_AddressingMode_No_Address) && (srcAddrMode == RP_AddressingMode_No_Address))
        CMP0      B                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_253  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3370         {
// 3371             return retVal;
// 3372         }
// 3373     }
// 3374     // beacon frame or ACK frame ?
// 3375     if (!(frmType & 0x01))
??RpAckCheckCallback_253:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, ??RpAckCheckCallback_254  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 3376     {
// 3377         if (frmType == RP_FRM_FrameType_Beacon)
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        CMP0      X                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3378         {
// 3379             if (frmVersion != RP_MAC2012_FRAME_VERSION)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BZ        ??RpAckCheckCallback_254  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3380               {
// 3381                 pData ++;
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+56
        POP       HL                 ;; 1 cycle
          CFI CFA SP+54
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x2E], AX      ;; 1 cycle
// 3382                 panId = RP_VAL_ARRAY_TO_UINT16(pData);
// 3383                 if ((dstAddrMode == RP_AddressingMode_No_Address) && ((panId == refPanId) || (refPanId == 0xFFFFU)))
        CMP0      B                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_255  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+56
        POP       HL                 ;; 1 cycle
          CFI CFA SP+54
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        BZ        ??RpAckCheckCallback_256  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOVW      AX, BC             ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_255  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3384                 {
// 3385                     retVal = RP_VALID_FRAME;
??RpAckCheckCallback_256:
        ONEB      A                  ;; 1 cycle
        BR        R:??RpAckCheckCallback_257  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3386                 }
// 3387                 return retVal;
// 3388               }
// 3389         }
// 3390         else
// 3391         {
// 3392             return retVal;
// 3393         }
// 3394     }
// 3395     // sequence numberdecode
// 3396     // Build Ack
// 3397     retVal = (uint8_t)(ackReq ? (RP_NEED_ACK | RP_VALID_FRAME) : RP_VALID_FRAME);
??RpAckCheckCallback_254:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_258  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x3            ;; 1 cycle
        BR        S:??RpAckCheckCallback_259  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpAckCheckCallback_258:
        ONEB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_259:
        MOV       [SP+0x02], A       ;; 1 cycle
// 3398     if (sqnNoSuppress)
        CMP0      C                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_260  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3399     {
// 3400         *(pAck + 1) |= 0x01;
        MOV       A, [SP+0x38]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        BR        S:??RpAckCheckCallback_261  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
// 3401     }
// 3402     else
// 3403     {
// 3404         pAck[ackDataCnt] = *pData;
??RpAckCheckCallback_260:
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOV       A, [SP+0x38]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3405         pData++;
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+56
        POP       HL                 ;; 1 cycle
          CFI CFA SP+54
        MOV       A, ES              ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x2E], AX      ;; 1 cycle
// 3406         ackDataCnt++;
        MOVW      AX, #0x3           ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 30 cycles
// 3407     }
// 3408     // dest Address
// 3409     if (dstAddrMode == RP_AddressingMode_No_Address)
??RpAckCheckCallback_261:
        CMP0      B                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_262  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3410     {
// 3411         if (frmType == RP_FRM_FrameType_Beacon)
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        CMP0      X                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_263  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
// 3412         {
// 3413             chkPanId = RP_VAL_ARRAY_TO_UINT16(pData);
// 3414 
// 3415             if ((srcPanExist == RP_TRUE) && (refPanId != 0xFFFF) &&
// 3416                 ((refPanId != chkPanId) || (chkPanId == 0xFFFF)))
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_264  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_265  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMPW      AX, DE             ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3417             {
// 3418                 return RP_INVALID_FRAME;
// 3419             }
// 3420             if ((refPanId == 0xFFFF) && (!panCompression))
// 3421             {
// 3422                 retVal &= ~RP_NEED_ACK;
// 3423             }
// 3424         }
// 3425         else // if(frmType =! RP_FRM_FrameType_Beacon)
// 3426         {
// 3427             chkPanId = RP_VAL_ARRAY_TO_UINT16(pData);
// 3428             if (!refPanCoord)
// 3429             {
// 3430                 return RP_INVALID_FRAME;
// 3431             }
// 3432             else if ((srcPanExist == RP_TRUE) &&
// 3433 #if !defined(RP_BROAD_CAST_ACK_DISABLE)
// 3434                      (refPanId != 0xFFFF) &&
// 3435 #endif
// 3436                      (chkPanId == 0xFFFF))
// 3437             {
// 3438                 retVal &= ~RP_NEED_ACK;
// 3439             }
// 3440             else if ((!panCompression && (refPanId == 0xFFFF) && (chkPanId != 0xFFFF)))
// 3441             {
// 3442                 return RP_INVALID_FRAME;
// 3443             }
// 3444         }
// 3445     }
// 3446     else
// 3447     {
// 3448         if (dstPanExist == RP_TRUE)
// 3449         {
// 3450             panId = RP_VAL_ARRAY_TO_UINT16(pData);
// 3451             pData += sizeof(uint16_t);
// 3452 
// 3453             if ((frmVersion == RP_MAC2012_FRAME_VERSION) && (refPanId == 0xFFFF) && (panId != 0xFFFF) &&
// 3454                 ((dstAddrMode != RP_AddressingMode_Extended_Address) || (srcAddrMode != RP_AddressingMode_Extended_Address)))
// 3455             {
// 3456                 return RP_INVALID_FRAME;
// 3457             }
// 3458             if ((frmType != RP_FRM_FrameType_Beacon) && (refPanId != panId) && (panId != 0xFFFF))
// 3459             {
// 3460                 return RP_INVALID_FRAME;
// 3461             }
// 3462             if ((frmVersion != RP_MAC2012_FRAME_VERSION) && (srcAddrMode == RP_AddressingMode_No_Address) && panCompression)
// 3463             {
// 3464                 return RP_INVALID_FRAME;
// 3465             }
// 3466             if (dstAddrMode != RP_AddressingMode_Extended_Address)
// 3467             {
// 3468                 if (((refPanId == 0xFFFF) || (panId == 0xFFFF))
// 3469 #if !defined(RP_BROAD_CAST_ACK_DISABLE)
// 3470                     && !((refPanId == 0xFFFF) && (panId == 0xFFFF))
// 3471 #endif
// 3472                     )
// 3473                 {
// 3474                     retVal &= ~RP_NEED_ACK;
// 3475                 }
// 3476             }
// 3477             if (srcAddrMode != RP_AddressingMode_No_Address)
// 3478             {
// 3479                 RP_VAL_UINT16_TO_ARRAY(panId, &pAck[ackDataCnt]);
// 3480                 ackDataCnt += sizeof(uint16_t);
// 3481             }
// 3482         }
// 3483         if (dstAddrMode == RP_AddressingMode_Extended_Address)
// 3484         {
// 3485             RpMemcpy(extAddr, pData, (sizeof(uint32_t) * 2));
// 3486             pData += (sizeof(uint32_t) * 2);
// 3487 
// 3488             if ((extAddr[0] != refExtAddr[0]) || (extAddr[1] != refExtAddr[1]))
// 3489             {
// 3490                 return RP_INVALID_FRAME;
// 3491             }
// 3492         }
// 3493     }
// 3494 
// 3495     // src Address
// 3496     srcAddInfoPtr = pData;
??RpAckCheckCallback_266:
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x1A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
// 3497     if ((frmVersion == RP_MAC2012_FRAME_VERSION) && (ackReq))
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_267  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_267  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 3498     {
// 3499         if (srcPanExist == RP_TRUE)
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_268  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
// 3500         {
// 3501             pAck[ackDataCnt]	 = *(pData);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x38]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3502             pAck[ackDataCnt + 1] = *(pData + 1);
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3503             pData += sizeof(uint16_t);
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      [SP+0x2E], AX      ;; 1 cycle
// 3504             ackDataCnt += sizeof(uint16_t);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 49 cycles
// 3505         }
// 3506         switch (srcAddrMode)
??RpAckCheckCallback_268:
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_267  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3507         {
// 3508             case RP_AddressingMode_Extended_Address:
// 3509                 pAck[ackDataCnt] = *pData;
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x38]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3510                 pAck[ackDataCnt + 1] = *(pData + 1);
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3511                 pAck[ackDataCnt + 2] = *(pData + 2);
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3512                 pAck[ackDataCnt + 3] = *(pData + 3);
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        ADDW      AX, #0x3           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x3           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3513                 pAck[ackDataCnt + 4] = *(pData + 4);
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3514                 pAck[ackDataCnt + 5] = *(pData + 5);
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3515                 pAck[ackDataCnt + 6] = *(pData + 6);
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3516                 pAck[ackDataCnt + 7] = *(pData + 7);
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        ADDW      AX, #0x7           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x7           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3517                 pData += (sizeof(uint32_t) * 2);
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      [SP+0x2E], AX      ;; 1 cycle
// 3518                 ackDataCnt += (sizeof(uint32_t) * 2);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 3519                 *(pAck + 1) |= 0x0C;
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        OR        A, #0xC            ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 3520                 break;
        ; ------------------------------------- Block: 153 cycles
??RpAckCheckCallback_267:
        MOVW      AX, [SP+0x2C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
// 3521 
// 3522             case RP_AddressingMode_No_Address:
// 3523             default:
// 3524                 break;
// 3525         }
// 3526     }
// 3527     if ((srcAddInfoPtr - bkupPtrData) <= dataLen)
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        SUBW      AX, BC             ;; 1 cycle
        CMPW      AX, DE             ;; 1 cycle
        SKNH                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_269  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 3528     {
// 3529         if (ackReq)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_255  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 3530         {
// 3531             if (frmVersion == RP_MAC2012_FRAME_VERSION)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_270  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3532             {
// 3533                 *(pAck + 1) |= 0x20;
        MOV       A, [SP+0x38]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].5          ;; 3 cycles
// 3534                 if ((panCompression) && (srcAddrMode != RP_AddressingMode_No_Address))
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_271  ;; 5 cycles
        ; ------------------------------------- Block: 16 cycles
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_271  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3535                 {
// 3536                     *(pAck) |= 0x40;
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        SET1      ES:[HL].6          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3537                 }
// 3538                 *pAckLen = (uint16_t)ackDataCnt;
??RpAckCheckCallback_271:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x3C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x3A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        BR        R:??RpAckCheckCallback_272  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
// 3539             }
??RpAckCheckCallback_264:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_266  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_265:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_266  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        BR        S:??RpAckCheckCallback_273  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_263:
        MOV       A, [SP+0x12]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, ??RpAckCheckCallback_274  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BZ        ??RpAckCheckCallback_274  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_266  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_273:
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        BR        R:??RpAckCheckCallback_266  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpAckCheckCallback_274:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_266  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_266  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BR        R:??RpAckCheckCallback_266  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_262:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x3           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_275  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       E, A               ;; 1 cycle
        MOV       D, #0x0            ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+56
        POP       HL                 ;; 1 cycle
          CFI CFA SP+54
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x2E], AX      ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_276  ;; 4 cycles
        ; ------------------------------------- Block: 31 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BNZ       ??RpAckCheckCallback_276  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BZ        ??RpAckCheckCallback_277  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_276:
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        CMP0      X                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_278  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        BZ        ??RpAckCheckCallback_278  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpAckCheckCallback_278:
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BZ        ??RpAckCheckCallback_277  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpAckCheckCallback_277  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, $+7        ;; 5 cycles
        BR        F:??RpAckCheckCallback_242  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
??RpAckCheckCallback_277:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BZ        ??RpAckCheckCallback_279  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        BZ        ??RpAckCheckCallback_280  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BNZ       ??RpAckCheckCallback_279  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??RpAckCheckCallback_281  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpAckCheckCallback_280:
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BZ        ??RpAckCheckCallback_279  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpAckCheckCallback_281:
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpAckCheckCallback_279:
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_275  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [SP+0x38]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+56
        POP       AX                 ;; 1 cycle
          CFI CFA SP+54
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x1A], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+56
        POP       HL                 ;; 1 cycle
          CFI CFA SP+54
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 29 cycles
??RpAckCheckCallback_275:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpAckCheckCallback_266  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        MOV       A, [SP+0x32]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x30]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x26          ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _RpMemcpy
        CALL      F:_RpMemcpy        ;; 3 cycles
        MOVW      AX, [SP+0x30]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+54
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x2E], AX      ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        MOVW      AX, [SP+0x2A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x28]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+54
        BNZ       ??RpAckCheckCallback_242  ;; 4 cycles
        ; ------------------------------------- Block: 38 cycles
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        MOVW      AX, [SP+0x2E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x2C]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+54
        SKNZ                         ;; 4 cycles
        BR        R:??RpAckCheckCallback_266  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
??RpAckCheckCallback_242:
        CLRB      A                  ;; 1 cycle
        BR        S:??RpAckCheckCallback_282  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3540             else if (frmVersion == RP_MAC2011_FRAME_VERSION)
??RpAckCheckCallback_270:
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpAckCheckCallback_272  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3541             {
// 3542                 *(pAck + 1) |= 0x10;
        MOV       A, [SP+0x38]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 3543             }
// 3544             if ((retVal & RP_NEED_ACK) && (refFramePend))
??RpAckCheckCallback_272:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].1, ??RpAckCheckCallback_255  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, [SP+0x0B]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpAckCheckCallback_255  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3545             {
// 3546                 *(pAck) |= 0x10;
        MOV       A, [SP+0x38]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
        BR        S:??RpAckCheckCallback_255  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
// 3547             }
// 3548         }
// 3549     }
// 3550     else
// 3551     {
// 3552         retVal = RP_INVALID_FRAME;
??RpAckCheckCallback_269:
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_257:
        MOV       [SP+0x02], A       ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 3553     }
// 3554     if (retVal & RP_NEED_ACK)
// 3555     {
// 3556         noUpDataScndAckLen = RP_TRUE;
// 3557     }
// 3558 
// 3559     return retVal;
??RpAckCheckCallback_255:
        MOV       A, [SP+0x02]       ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpAckCheckCallback_282:
        ADDW      SP, #0x32          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock40
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 1104 cycles
// 3560 }

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
// 3561 
// 3562 #ifndef RP_WISUN_FAN_STACK
// 3563 /******************************************************************************
// 3564  * callback program
// 3565  *****************************************************************************/
// 3566 #define RP_EDFLOORLVL		(-75)
// 3567 #define RP_EDSATURLVL		(RP_EDFLOORLVL + (256/4))
// 3568 #define RP_RSSIFLOORLVL		(-99)
// 3569 #define RP_RSSISATURLVL		(-35)
// 3570 
// 3571 /******************************************************************************
// 3572  *	function Name  : RpCalcLqiCallback
// 3573  *	parameters     : rssiEdValue : read value of baseband RSSI
// 3574  *				   : rssiEdSelect : RP_TRUE: rssi select
// 3575  *				   :                RP_FALSE:ED select
// 3576  *	return value   : IEEE802.15.4 LQI or ED format
// 3577  *	description    : convert rssirslt to the value which format is specifiled
// 3578  *				   : in IEEE802.15.4.
// 3579  *****************************************************************************/
// 3580 uint8_t RpCalcLqiCallback( uint16_t rssiEdValue, uint8_t rssiEdSelect )
// 3581 {
// 3582 	int16_t schg;
// 3583 	uint8_t val;
// 3584 
// 3585 	schg = (int16_t)rssiEdValue;
// 3586 	schg = (int16_t)((schg < 0x0100) ? schg : (schg | 0xfe00));
// 3587 	if (rssiEdSelect == RP_FALSE)
// 3588 	{
// 3589 		if (schg <= RP_EDFLOORLVL)
// 3590 		{
// 3591 			val = 0x00;
// 3592 		}
// 3593 		else if (schg >= RP_EDSATURLVL)
// 3594 		{
// 3595 			val = 0xff;
// 3596 		}
// 3597 		else // if(schg > EDFLOORLVL && schg < EDSATURLVL)
// 3598 		{
// 3599 			schg = (int16_t)(schg - RP_EDFLOORLVL);
// 3600 			val = (uint8_t)(schg << 2);
// 3601 		}
// 3602 	}
// 3603 	else // if (rssiSelect == RP_TRUE)
// 3604 	{
// 3605 		if	(schg <= RP_RSSIFLOORLVL)
// 3606 		{
// 3607 			// floor level 		= RSSIFLOORLVL(dBm)
// 3608 			val = 0x00;
// 3609 		}
// 3610 		else if (schg >= RP_RSSISATURLVL)
// 3611 		{
// 3612 			// saturation level = RSSISATURLVL(dBm)
// 3613 			val = 0xff;
// 3614 		}
// 3615 		else // if(schg > RSSIFLOORLVL && schg < RSSISATURLVL)
// 3616 		{
// 3617 			schg = (int16_t)(schg - RP_RSSIFLOORLVL);
// 3618 			val = (uint8_t)(schg << 2);
// 3619 		}
// 3620 	}
// 3621 
// 3622 	return (val);	// min(-99dBm) = 0, max(-35dBm) = 255 for (rssiSelect == RP_TRUE)
// 3623 }
// 3624 #endif // #ifndef RP_WISUN_FAN_STACK
// 3625 
// 3626 /***********************************************************************************************************************
// 3627  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
// 3628  **********************************************************************************************************************/
// 3629 
// 
//      2 bytes in section .bssf
// 11 487 bytes in section .textf
// 
//      2 bytes of DATA    memory
// 11 487 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
