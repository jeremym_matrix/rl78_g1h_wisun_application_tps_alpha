///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:58
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\src\r_modem_demo_msg.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3AB5.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\src\r_modem_demo_msg.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\r_modem_demo_msg.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_file_descriptor", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__iar_require __Printf", "unknown"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _AppCmdBuf
        EXTERN ?FAR_NEAR_MOVE_LONG_L06
        EXTERN ?XLOAD_AX_SP_L06
        EXTERN ?XLOAD_A_SP_L06
        EXTERN _AppCmd_ProcessCmd
        EXTERN _R_LOG_GetLog
        EXTERN _R_memset
        EXTERN _RdrvUART_GetChar
        EXTERN _RdrvUART_PutChar
        EXTERN _UInt32ToArr
        EXTERN _r_modem_msg_tx
        EXTERN _r_modem_rx
        EXTERN _r_modem_rx_start
        EXTERN _r_modem_tx
        EXTERN _vsnprintf

        PUBLIC _R_Modem_Demo_Init
        PUBLIC _R_Modem_Demo_ReadCommand
        PUBLIC _R_Modem_Demo_sendLogData
        PUBLIC _R_Modem_print
        PUBLIC _R_Modem_sendLogIndication
        PUBLIC _R_Modem_vprint
        PUBLIC _print_hdlc_footer
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\src\r_modem_demo_msg.c
//    1 /**********************************************************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
//    4  * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
//    5  * applicable laws, including copyright laws.
//    6  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
//    7  * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
//    8  * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
//    9  * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
//   10  * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
//   11  * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   12  * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
//   13  * this software. By using this software, you agree to the additional terms and conditions found by accessing the
//   14  * following link:
//   15  * http://www.renesas.com/disclaimer
//   16  *
//   17  * Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
//   18  *********************************************************************************************************************/
//   19 
//   20 /**
//   21  * @file r_modem_demo_msg.c
//   22  * @brief HDLC/Modem module for Demonstrator messages
//   23  */
//   24 
//   25 #include <stdio.h>
//   26 
//   27 #include "r_modem.h"
//   28 #include "hardware.h"
//   29 #include "r_app_main.h"
//   30 #include "r_mem_tools.h"
//   31 #include "r_byte_swap.h"
//   32 #include "r_impl_utils.h"
//   33 
//   34 #include "r_log.h"
//   35 #include "r_log_flags.h"
//   36 
//   37 /** The maximum number of characters that may be printed by a single call to any R_Modem_print function variant */
//   38 #define R_MODEM_PRINT_MAX_BYTES 256
//   39 
//   40 typedef struct
//   41 {
//   42     uint8_t  hdlcHeaderWritten; //!< Flag to indicate whether the HDLC has already been written or not
//   43     uint32_t crc;               //!< The CRC of the current HDLC message
//   44 } r_modem_demo_msg_state_t;
//   45 

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   46 static r_modem_ctx ctx;
_ctx:
        DS 24
        DS 6
//   47 static r_modem_demo_msg_state_t state;
//   48 
//   49 void print_hdlc_footer();
//   50 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _resume_rx
          CFI NoCalls
        CODE
//   51 static void resume_rx(uint8_t* buf, size_t size)
//   52 {
_resume_rx:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      BC                 ;; 1 cycle
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 4 cycles
        ; ------------------------------------- Total: 4 cycles
        ; Auto size: 6
//   53     R_memset(buf, 0xff, size);
        REQUIRE ?Subroutine0
        ; // Fall through to label ?Subroutine0

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI NoFunction
          CFI CFA SP+10
        CODE
?Subroutine0:
        MOVW      AX, BC             ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, #0xFF           ;; 1 cycle
          CFI FunCall _resume_rx _R_memset
          CFI FunCall _R_Modem_Demo_Init _R_memset
        CALL      F:_R_memset        ;; 3 cycles
//   54     r_modem_rx_start(buf, size, &ctx);
        MOV       X, #BYTE3(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, #LWRD(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _resume_rx _r_modem_rx_start
          CFI FunCall _R_Modem_Demo_Init _r_modem_rx_start
        CALL      F:_r_modem_rx_start  ;; 3 cycles
//   55 }
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 32 cycles
        ; ------------------------------------- Total: 32 cycles
//   56 
//   57 /** Transmit the byte */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _r_modem_cb_tx
          CFI FunCall _RdrvUART_PutChar
        CODE
//   58 static void r_modem_cb_tx(uint8_t byte)
//   59 {
_r_modem_cb_tx:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//   60     RdrvUART_PutChar(byte);
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
//   61 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//   62 
//   63 /** Process received HDLC message */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon1
          CFI Function _r_modem_cb_rx
        CODE
//   64 static void r_modem_cb_rx(uint16_t size)
//   65 {
_r_modem_cb_rx:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//   66     const r_modem_header_t* hdr = (const r_modem_header_t*)AppCmdBuf;
//   67 
//   68 #if defined(__ICCRL78__)
//   69 
//   70 /* Mask Compiler Warning[Pa089]: enumerated type mixed with another enumerated type. */
//   71 #pragma diag_suppress=Pa089
//   72 /* Reason: The hdr->id contains different bit fields represented by individual enum declarations.
//   73    It is the desired behavior to combine these for simplicity and effectiveness. */
//   74 #endif
//   75 
//   76     if (hdr->type != R_MODEM_TYPE_WISUN_FAN || hdr->id != (R_MODEM_IDC_CHANNEL_0 | R_MODEM_IDA_PRINTF))
        MOV       ES, #BYTE3(_AppCmdBuf)  ;; 1 cycle
        CMP       ES:_AppCmdBuf+1, #0x6  ;; 2 cycles
        BNZ       ??print_hdlc_footer_0  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_AppCmdBuf+2, #0x40  ;; 2 cycles
        BNZ       ??print_hdlc_footer_0  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//   77     {
//   78         resume_rx(ctx.rx_buf, ctx.rx_buf_capacity);  // Discard frame
//   79         return;
//   80     }
//   81 
//   82     /* HDLC message is valid -> Pass payload to application */
//   83     AppCmd_ProcessCmd(&ctx.rx_buf[sizeof(*hdr)], size - sizeof(*hdr));
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOVW      HL, ES:_ctx        ;; 2 cycles
        MOV       A, ES:_ctx+2       ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+6
        POP       DE                 ;; 1 cycle
          CFI CFA SP+4
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppCmd_ProcessCmd
        CALL      F:_AppCmd_ProcessCmd  ;; 3 cycles
//   84 
//   85     resume_rx(ctx.rx_buf, ctx.rx_buf_capacity);  // Clear RX buffer and wait for next message
        ; ------------------------------------- Block: 18 cycles
??print_hdlc_footer_0:
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOVW      BC, ES:_ctx+6      ;; 2 cycles
        MOVW      HL, ES:_ctx        ;; 2 cycles
        MOV       A, ES:_ctx+2       ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+6
        POP       DE                 ;; 1 cycle
          CFI CFA SP+4
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _resume_rx
        CALL      F:_resume_rx       ;; 3 cycles
//   86 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 20 cycles
        ; ------------------------------------- Total: 51 cycles
//   87 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon0
          CFI Function _R_Modem_Demo_Init
          CFI NoCalls
        CODE
//   88 void R_Modem_Demo_Init(uint8_t* buf, size_t size)
//   89 {
_R_Modem_Demo_Init:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 6
//   90     // Init callbacks
//   91     ctx.rx_cb = r_modem_cb_rx;
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOV       ES:_ctx+18, #BYTE3(_r_modem_cb_rx)  ;; 2 cycles
        MOVW      AX, #LWRD(_r_modem_cb_rx)  ;; 1 cycle
        MOVW      ES:_ctx+16, AX     ;; 2 cycles
//   92     ctx.tx_cb = r_modem_cb_tx;
        MOV       ES:_ctx+22, #BYTE3(_r_modem_cb_tx)  ;; 2 cycles
        MOVW      AX, #LWRD(_r_modem_cb_tx)  ;; 1 cycle
        MOVW      ES:_ctx+20, AX     ;; 2 cycles
//   93 
//   94     resume_rx(buf, size);
        BR        F:?Subroutine0     ;; 3 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 18 cycles
//   95 }
//   96 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon1
          CFI Function _R_Modem_Demo_ReadCommand
        CODE
//   97 void R_Modem_Demo_ReadCommand(void)
//   98 {
_R_Modem_Demo_ReadCommand:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//   99     for (short ch = RdrvUART_GetChar(); ch != EOF; ch = RdrvUART_GetChar())
        BR        S:??print_hdlc_footer_1  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  100     {
//  101         r_modem_rx((uint8_t)ch, &ctx);
??R_Modem_Demo_ReadCommand_0:
        MOVW      DE, #LWRD(_ctx)    ;; 1 cycle
        MOV       X, #BYTE3(_ctx)    ;; 1 cycle
        MOV       A, L               ;; 1 cycle
          CFI FunCall _r_modem_rx
        CALL      F:_r_modem_rx      ;; 3 cycles
//  102     }
          CFI FunCall _RdrvUART_GetChar
        ; ------------------------------------- Block: 6 cycles
??print_hdlc_footer_1:
        CALL      F:_RdrvUART_GetChar  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BNZ       ??R_Modem_Demo_ReadCommand_0  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
//  103 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 25 cycles
//  104 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon2
          CFI Function _R_Modem_print
        CODE
//  105 int R_Modem_print(const char* format, ...)
//  106 {
_R_Modem_print:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
//  107     /* Init argument list */
//  108     va_list argumentList;
//  109     va_start(argumentList, format);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  110 
//  111     int totalBytesWritten = R_Modem_vprint(format, argumentList);
//  112 
//  113     va_end(argumentList);  // Perform any cleanup necessary so that the function can return
//  114     return totalBytesWritten;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_Modem_vprint
        CALL      F:_R_Modem_vprint  ;; 3 cycles
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 27 cycles
        ; ------------------------------------- Total: 27 cycles
//  115 }
//  116 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon2
          CFI Function _R_Modem_vprint
        CODE
//  117 int R_Modem_vprint(const char* format, va_list argumentList)
//  118 {
_R_Modem_vprint:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 268
        SUBW      SP, #0xFE          ;; 1 cycle
          CFI CFA SP+262
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+272
//  119     /* Prepend HDLC header if not already done for the current message */
//  120     if (!state.hdlcHeaderWritten)
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        CMP0      ES:_ctx+24         ;; 2 cycles
        BNZ       ??print_hdlc_footer_2  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  121     {
//  122         ctx.tx_cb(RECORD_DELIMITER);
        MOVW      HL, #LWRD(_ctx+20)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x7E           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
//  123         state.crc = 0;
        MOVW      HL, #LWRD(_ctx+26)  ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  124         r_modem_header_t header = { 0, R_MODEM_TYPE_WISUN_FAN, R_MODEM_IDA_PRINTF, 0};
        MOVW      AX, SP             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_0)     ;; 1 cycle
        MOV       ES, #BYTE3(?_0)    ;; 1 cycle
        MOVW      BC, #0x4           ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
//  125         state.crc = r_modem_tx(&header, sizeof(header), state.crc, &ctx);
        MOV       X, #BYTE3(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+274
        MOVW      AX, #LWRD(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+276
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+278
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+280
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_modem_tx
        CALL      F:_r_modem_tx      ;; 3 cycles
        MOVW      HL, #LWRD(_ctx+26)  ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  126         state.hdlcHeaderWritten = 1;
        ONEB      ES:_ctx+24         ;; 2 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+272
        ; ------------------------------------- Block: 47 cycles
//  127     }
//  128 
//  129     /* Write input string to buffer */
//  130     char stringBuf[R_MODEM_PRINT_MAX_BYTES];
//  131     int charsWritten = vsnprintf(stringBuf, sizeof(stringBuf), format, argumentList);
??print_hdlc_footer_2:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
          CFI FunCall ?XLOAD_AX_SP_L06
        CALL      N:?XLOAD_AX_SP_L06
        DW        0x112              ;; 3 cycles
        MOVW      BC, AX             ;; 1 cycle
          CFI FunCall ?XLOAD_AX_SP_L06
        CALL      N:?XLOAD_AX_SP_L06
        DW        0x110              ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+274
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+276
          CFI FunCall ?XLOAD_AX_SP_L06
        CALL      N:?XLOAD_AX_SP_L06
        DW        0x10C              ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
          CFI FunCall ?XLOAD_A_SP_L06
        CALL      N:?XLOAD_A_SP_L06
        DW        0x10E              ;; 3 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+278
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+280
        MOVW      BC, #0x100         ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _vsnprintf
        CALL      F:_vsnprintf       ;; 3 cycles
//  132 
//  133     int success = charsWritten >= 0 && (unsigned)charsWritten <= sizeof(stringBuf);
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+272
        MOVW      [SP], AX           ;; 1 cycle
        BT        A.7, ??print_hdlc_footer_3  ;; 5 cycles
        ; ------------------------------------- Block: 42 cycles
        CMPW      AX, #0x101         ;; 1 cycle
        BC        ??print_hdlc_footer_4  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, #0x100         ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        S:??print_hdlc_footer_5  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??print_hdlc_footer_4:
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  134     if (charsWritten >= 0)
//  135     {
//  136         /* Adapt number of written characters if input string was truncated because output buffer is too small */
//  137         charsWritten = MIN((unsigned)charsWritten, sizeof(stringBuf));
//  138         state.crc = r_modem_tx(stringBuf, charsWritten, state.crc, &ctx);  // Send input string buffer via modem interface
??print_hdlc_footer_5:
        MOV       X, #BYTE3(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+274
        MOVW      AX, #LWRD(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+276
        MOVW      HL, #LWRD(_ctx+26)  ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+278
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+280
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_modem_tx
        CALL      F:_r_modem_tx      ;; 3 cycles
        MOVW      HL, #LWRD(_ctx+26)  ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  139     }
//  140 
//  141     /* Append HDLC footer if this is the end of the message (or any error occurred during write) */
//  142     if (!success || (charsWritten > 0 && stringBuf[charsWritten - 1] == '\n'))
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+272
        MOV       A, [SP+0x02]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??print_hdlc_footer_3  ;; 4 cycles
        ; ------------------------------------- Block: 35 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8001        ;; 1 cycle
        BC        ??print_hdlc_footer_6  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOV       A, [HL]            ;; 1 cycle
        CMP       A, #0xA            ;; 1 cycle
        SKNZ                         ;; 1 cycle
          CFI FunCall _print_hdlc_footer
        ; ------------------------------------- Block: 10 cycles
//  143     {
//  144         print_hdlc_footer();
??print_hdlc_footer_3:
        CALL      F:_print_hdlc_footer  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  145     }
//  146 
//  147     return charsWritten;
??print_hdlc_footer_6:
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0xFE          ;; 1 cycle
          CFI CFA SP+18
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 179 cycles
//  148 }
//  149 
//  150 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon1
          CFI Function _R_Modem_sendLogIndication
        CODE
//  151 void R_Modem_sendLogIndication()
//  152 {
_R_Modem_sendLogIndication:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 270
        SUBW      SP, #0xFE          ;; 1 cycle
          CFI CFA SP+258
        SUBW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+274
//  153 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
//  154     uint8_t buf[R_MODEM_PRINT_MAX_BYTES];  // buffer to store log records
//  155     size_t numBytes = 0;
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        BR        R:??print_hdlc_footer_7  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
//  156 
//  157     while ((numBytes = R_LOG_GetLog(buf, ARRAY_SIZE(buf))) > 0)
//  158     {
//  159         /* Prepend HDLC header if not already done for the current message */
//  160         if (!state.hdlcHeaderWritten)
??R_Modem_sendLogIndication_0:
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        CMP0      ES:_ctx+24         ;; 2 cycles
        BNZ       ??print_hdlc_footer_8  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  161         {
//  162             ctx.tx_cb(RECORD_DELIMITER);
        MOVW      HL, #LWRD(_ctx+20)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x7E           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
//  163             state.crc = 0;
        MOVW      HL, #LWRD(_ctx+26)  ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  164             r_modem_header_t header = { 0, R_MODEM_TYPE_WISUN_FAN, R_MODEM_IDA_INDICATION, 0};
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_1)     ;; 1 cycle
        MOV       ES, #BYTE3(?_1)    ;; 1 cycle
        MOVW      BC, #0x4           ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
//  165             state.crc = r_modem_tx(&header, sizeof(header), state.crc, &ctx);
        MOV       X, #BYTE3(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+276
        MOVW      AX, #LWRD(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+278
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+280
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+282
        MOV       A, [SP+0x14]       ;; 1 cycle
          CFI FunCall _r_modem_tx
        CALL      F:_r_modem_tx      ;; 3 cycles
        MOVW      HL, #LWRD(_ctx+26)  ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        XCHW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
//  166             state.hdlcHeaderWritten = 1;
        ONEB      ES:_ctx+24         ;; 2 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+274
        ; ------------------------------------- Block: 49 cycles
//  167         }
//  168 
//  169         state.crc = r_modem_tx(buf, numBytes, state.crc, &ctx);  // Send input string buffer via modem interface
??print_hdlc_footer_8:
        MOV       X, #BYTE3(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+276
        MOVW      AX, #LWRD(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+278
        MOVW      HL, #LWRD(_ctx+26)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+280
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+282
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_modem_tx
        CALL      F:_r_modem_tx      ;; 3 cycles
        MOVW      HL, #LWRD(_ctx+26)  ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+274
        ; ------------------------------------- Block: 28 cycles
//  170     }
??print_hdlc_footer_7:
        MOVW      BC, #0x100         ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_LOG_GetLog
        CALL      F:_R_LOG_GetLog    ;; 3 cycles
        MOVW      [SP], AX           ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_Modem_sendLogIndication_0  ;; 4 cycles
          CFI FunCall _print_hdlc_footer
        ; ------------------------------------- Block: 13 cycles
//  171     print_hdlc_footer();
        CALL      F:_print_hdlc_footer  ;; 3 cycles
//  172 #endif /* R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF */
//  173 }
        ADDW      SP, #0xFE          ;; 1 cycle
          CFI CFA SP+20
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 126 cycles
//  174 
//  175 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon0
          CFI Function _R_Modem_Demo_sendLogData
        CODE
//  176 void R_Modem_Demo_sendLogData(const uint8_t* buf, size_t bufSize)
//  177 {
_R_Modem_Demo_sendLogData:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  178     r_modem_msg_tx(R_MODEM_TYPE_WISUN_FAN, R_MODEM_IDA_INDICATION, 0, buf, bufSize, &ctx);
        MOV       X, #BYTE3(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      AX, #LWRD(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        CLRB      C                  ;; 1 cycle
        MOV       X, #0x20           ;; 1 cycle
        MOV       A, #0x6            ;; 1 cycle
          CFI FunCall _r_modem_msg_tx
        CALL      F:_r_modem_msg_tx  ;; 3 cycles
//  179 }
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 26 cycles
        ; ------------------------------------- Total: 26 cycles
//  180 
//  181 #endif /* R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF */
//  182 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon1
          CFI Function _print_hdlc_footer
        CODE
//  183 void print_hdlc_footer()
//  184 {
_print_hdlc_footer:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 8
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
//  185     uint8_t crcbuf[CRC_SIZE];
//  186     UInt32ToArr(state.crc, crcbuf);
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      HL, #LWRD(_ctx+26)  ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall _UInt32ToArr
        CALL      F:_UInt32ToArr     ;; 3 cycles
//  187     r_modem_tx(crcbuf, sizeof(crcbuf), 0, &ctx);
        MOV       X, #BYTE3(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, #LWRD(_ctx)    ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      BC, #0x4           ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_modem_tx
        CALL      F:_r_modem_tx      ;; 3 cycles
//  188     ctx.tx_cb(RECORD_DELIMITER);
        MOVW      HL, #LWRD(_ctx+20)  ;; 1 cycle
        MOV       ES, #BYTE3(_ctx)   ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x7E           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
//  189     MEMZERO_S(&state);  // Reset struct to write HDLC header on next function call and reset CRC
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       X, #0x6            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        CLRB      X                  ;; 1 cycle
        MOVW      DE, #LWRD(_ctx+24)  ;; 1 cycle
        MOV       A, #BYTE3(_ctx)    ;; 1 cycle
          CFI FunCall _R_memset
        CALL      F:_R_memset        ;; 3 cycles
//  190 }
        ADDW      SP, #0x18          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 67 cycles
        ; ------------------------------------- Total: 67 cycles

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_0:
        DATA8
        DB 0, 6, 64, 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_1:
        DATA8
        DB 0, 6, 32, 0

        END
// 
//  30 bytes in section .bssf
//   8 bytes in section .constf
// 782 bytes in section .textf
// 
//  30 bytes of DATA    memory
// 790 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
