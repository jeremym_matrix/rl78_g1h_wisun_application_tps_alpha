///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:31:04
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\tasks.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW400C.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\tasks.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\tasks.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_full_locale_support", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _usCriticalNesting
        EXTERN ?L_AND_FAST_L03
        EXTERN ?L_IOR_L03
        EXTERN ?L_NOT_L03
        EXTERN ?UL_CMP_L03
        EXTERN _memset
        EXTERN _pxPortInitialiseStack
        EXTERN _uxListRemove
        EXTERN _vApplicationGetIdleTaskMemory
        EXTERN _vApplicationIdleHook
        EXTERN _vApplicationStackOverflowHook
        EXTERN _vApplicationTickHook
        EXTERN _vListInitialise
        EXTERN _vListInitialiseItem
        EXTERN _vListInsert
        EXTERN _vListInsertEnd
        EXTERN _vPortEndScheduler
        EXTERN _xPortStartScheduler
        EXTERN _xTimerCreateTimerTask

        PUBLIC _eTaskGetState
        PUBLIC _pcTaskGetName
        PUBLIC _pvTaskIncrementMutexHeldCount
        PUBLIC _pxCurrentTCB
        PUBLIC _ulTaskNotifyTake
        PUBLIC _uxTaskGetNumberOfTasks
        PUBLIC _uxTaskGetStackHighWaterMark
        PUBLIC _uxTaskGetSystemState
        PUBLIC _uxTaskGetTaskNumber
        PUBLIC _uxTaskPriorityGet
        PUBLIC _uxTaskPriorityGetFromISR
        PUBLIC _uxTaskResetEventItemValue
        PUBLIC _vTaskDelay
        PUBLIC _vTaskDelayUntil
        PUBLIC _vTaskDelete
        PUBLIC _vTaskEndScheduler
        PUBLIC _vTaskGetInfo
        PUBLIC _vTaskMissedYield
        PUBLIC _vTaskNotifyGiveFromISR
        PUBLIC _vTaskPlaceOnEventList
        PUBLIC _vTaskPlaceOnEventListRestricted
        PUBLIC _vTaskPlaceOnUnorderedEventList
        PUBLIC _vTaskPriorityInherit
        PUBLIC _vTaskPrioritySet
        PUBLIC _vTaskResume
        PUBLIC _vTaskSetTaskNumber
        PUBLIC _vTaskSetTimeOutState
        PUBLIC _vTaskStartScheduler
        PUBLIC _vTaskSuspend
        PUBLIC _vTaskSuspendAll
        PUBLIC _vTaskSwitchContext
        PUBLIC _xTaskCheckForTimeOut
        PUBLIC _xTaskCreateStatic
        PUBLIC _xTaskGenericNotify
        PUBLIC _xTaskGenericNotifyFromISR
        PUBLIC _xTaskGetCurrentTaskHandle
        PUBLIC _xTaskGetSchedulerState
        PUBLIC _xTaskGetTickCount
        PUBLIC _xTaskGetTickCountFromISR
        PUBLIC _xTaskIncrementTick
        PUBLIC _xTaskNotifyStateClear
        PUBLIC _xTaskNotifyWait
        PUBLIC _xTaskPriorityDisinherit
        PUBLIC _xTaskRemoveFromEventList
        PUBLIC _xTaskRemoveFromUnorderedEventList
        PUBLIC _xTaskResumeAll
        PUBLIC _xTaskResumeFromISR
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
        
          CFI Common cfiCommon3 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon3
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\tasks.c
//    1 /*
//    2     FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
//    3     All rights reserved
//    4 
//    5     VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
//    6 
//    7     This file is part of the FreeRTOS distribution.
//    8 
//    9     FreeRTOS is free software; you can redistribute it and/or modify it under
//   10     the terms of the GNU General Public License (version 2) as published by the
//   11     Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.
//   12 
//   13     ***************************************************************************
//   14     >>!   NOTE: The modification to the GPL is included to allow you to     !<<
//   15     >>!   distribute a combined work that includes FreeRTOS without being   !<<
//   16     >>!   obliged to provide the source code for proprietary components     !<<
//   17     >>!   outside of the FreeRTOS kernel.                                   !<<
//   18     ***************************************************************************
//   19 
//   20     FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
//   21     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//   22     FOR A PARTICULAR PURPOSE.  Full license text is available on the following
//   23     link: http://www.freertos.org/a00114.html
//   24 
//   25     ***************************************************************************
//   26      *                                                                       *
//   27      *    FreeRTOS provides completely free yet professionally developed,    *
//   28      *    robust, strictly quality controlled, supported, and cross          *
//   29      *    platform software that is more than just the market leader, it     *
//   30      *    is the industry's de facto standard.                               *
//   31      *                                                                       *
//   32      *    Help yourself get started quickly while simultaneously helping     *
//   33      *    to support the FreeRTOS project by purchasing a FreeRTOS           *
//   34      *    tutorial book, reference manual, or both:                          *
//   35      *    http://www.FreeRTOS.org/Documentation                              *
//   36      *                                                                       *
//   37     ***************************************************************************
//   38 
//   39     http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
//   40     the FAQ page "My application does not run, what could be wrong?".  Have you
//   41     defined configASSERT()?
//   42 
//   43     http://www.FreeRTOS.org/support - In return for receiving this top quality
//   44     embedded software for free we request you assist our global community by
//   45     participating in the support forum.
//   46 
//   47     http://www.FreeRTOS.org/training - Investing in training allows your team to
//   48     be as productive as possible as early as possible.  Now you can receive
//   49     FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
//   50     Ltd, and the world's leading authority on the world's leading RTOS.
//   51 
//   52     http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
//   53     including FreeRTOS+Trace - an indispensable productivity tool, a DOS
//   54     compatible FAT file system, and our tiny thread aware UDP/IP stack.
//   55 
//   56     http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
//   57     Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.
//   58 
//   59     http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
//   60     Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
//   61     licenses offer ticketed support, indemnification and commercial middleware.
//   62 
//   63     http://www.SafeRTOS.com - High Integrity Systems also provide a safety
//   64     engineered and independently SIL3 certified version for use in safety and
//   65     mission critical applications that require provable dependability.
//   66 
//   67     1 tab == 4 spaces!
//   68 */
//   69 
//   70 /* Standard includes. */
//   71 #include <stdlib.h>
//   72 #include <string.h>
//   73 
//   74 /* Defining MPU_WRAPPERS_INCLUDED_FROM_API_FILE prevents task.h from redefining
//   75 all the API functions to use the MPU wrappers.  That should only be done when
//   76 task.h is included from an application file. */
//   77 #define MPU_WRAPPERS_INCLUDED_FROM_API_FILE
//   78 
//   79 /* FreeRTOS includes. */
//   80 #include "FreeRTOS.h"
//   81 #include "task.h"
//   82 #include "timers.h"
//   83 #include "StackMacros.h"
//   84 
//   85 /* Lint e961 and e750 are suppressed as a MISRA exception justified because the
//   86 MPU ports require MPU_WRAPPERS_INCLUDED_FROM_API_FILE to be defined for the
//   87 header files above, but not in this file, in order to generate the correct
//   88 privileged Vs unprivileged linkage and placement. */
//   89 #undef MPU_WRAPPERS_INCLUDED_FROM_API_FILE /*lint !e961 !e750. */
//   90 
//   91 /* Set configUSE_STATS_FORMATTING_FUNCTIONS to 2 to include the stats formatting
//   92 functions but without including stdio.h here. */
//   93 #if ( configUSE_STATS_FORMATTING_FUNCTIONS == 1 )
//   94 	/* At the bottom of this file are two optional functions that can be used
//   95 	to generate human readable text from the raw data generated by the
//   96 	uxTaskGetSystemState() function.  Note the formatting functions are provided
//   97 	for convenience only, and are NOT considered part of the kernel. */
//   98 	#include <stdio.h>
//   99 #endif /* configUSE_STATS_FORMATTING_FUNCTIONS == 1 ) */
//  100 
//  101 #if( configUSE_PREEMPTION == 0 )
//  102 	/* If the cooperative scheduler is being used then a yield should not be
//  103 	performed just because a higher priority task has been woken. */
//  104 	#define taskYIELD_IF_USING_PREEMPTION()
//  105 #else
//  106 	#define taskYIELD_IF_USING_PREEMPTION() portYIELD_WITHIN_API()
//  107 #endif
//  108 
//  109 /* Values that can be assigned to the ucNotifyState member of the TCB. */
//  110 #define taskNOT_WAITING_NOTIFICATION	( ( uint8_t ) 0 )
//  111 #define taskWAITING_NOTIFICATION		( ( uint8_t ) 1 )
//  112 #define taskNOTIFICATION_RECEIVED		( ( uint8_t ) 2 )
//  113 
//  114 /*
//  115  * The value used to fill the stack of a task when the task is created.  This
//  116  * is used purely for checking the high water mark for tasks.
//  117  */
//  118 #define tskSTACK_FILL_BYTE	( 0xa5U )
//  119 
//  120 /* Sometimes the FreeRTOSConfig.h settings only allow a task to be created using
//  121 dynamically allocated RAM, in which case when any task is deleted it is known
//  122 that both the task's stack and TCB need to be freed.  Sometimes the
//  123 FreeRTOSConfig.h settings only allow a task to be created using statically
//  124 allocated RAM, in which case when any task is deleted it is known that neither
//  125 the task's stack or TCB should be freed.  Sometimes the FreeRTOSConfig.h
//  126 settings allow a task to be created using either statically or dynamically
//  127 allocated RAM, in which case a member of the TCB is used to record whether the
//  128 stack and/or TCB were allocated statically or dynamically, so when a task is
//  129 deleted the RAM that was allocated dynamically is freed again and no attempt is
//  130 made to free the RAM that was allocated statically.
//  131 tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE is only true if it is possible for a
//  132 task to be created using either statically or dynamically allocated RAM.  Note
//  133 that if portUSING_MPU_WRAPPERS is 1 then a protected task can be created with
//  134 a statically allocated stack and a dynamically allocated TCB. */
//  135 #define tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE ( ( ( configSUPPORT_STATIC_ALLOCATION == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) ) || ( portUSING_MPU_WRAPPERS == 1 ) )
//  136 #define tskDYNAMICALLY_ALLOCATED_STACK_AND_TCB 		( ( uint8_t ) 0 )
//  137 #define tskSTATICALLY_ALLOCATED_STACK_ONLY 			( ( uint8_t ) 1 )
//  138 #define tskSTATICALLY_ALLOCATED_STACK_AND_TCB		( ( uint8_t ) 2 )
//  139 
//  140 /*
//  141  * Macros used by vListTask to indicate which state a task is in.
//  142  */
//  143 #define tskBLOCKED_CHAR		( 'B' )
//  144 #define tskREADY_CHAR		( 'R' )
//  145 #define tskDELETED_CHAR		( 'D' )
//  146 #define tskSUSPENDED_CHAR	( 'S' )
//  147 
//  148 /*
//  149  * Some kernel aware debuggers require the data the debugger needs access to be
//  150  * global, rather than file scope.
//  151  */
//  152 #ifdef portREMOVE_STATIC_QUALIFIER
//  153 	#define static
//  154 #endif
//  155 
//  156 #if ( configUSE_PORT_OPTIMISED_TASK_SELECTION == 0 )
//  157 
//  158 	/* If configUSE_PORT_OPTIMISED_TASK_SELECTION is 0 then task selection is
//  159 	performed in a generic way that is not optimised to any particular
//  160 	microcontroller architecture. */
//  161 
//  162 	/* uxTopReadyPriority holds the priority of the highest priority ready
//  163 	state task. */
//  164 	#define taskRECORD_READY_PRIORITY( uxPriority )														\ 
//  165 	{																									\ 
//  166 		if( ( uxPriority ) > uxTopReadyPriority )														\ 
//  167 		{																								\ 
//  168 			uxTopReadyPriority = ( uxPriority );														\ 
//  169 		}																								\ 
//  170 	} /* taskRECORD_READY_PRIORITY */
//  171 
//  172 	/*-----------------------------------------------------------*/
//  173 
//  174 	#define taskSELECT_HIGHEST_PRIORITY_TASK()															\ 
//  175 	{																									\ 
//  176 	UBaseType_t uxTopPriority = uxTopReadyPriority;														\ 
//  177 																										\ 
//  178 		/* Find the highest priority queue that contains ready tasks. */								\ 
//  179 		while( listLIST_IS_EMPTY( &( pxReadyTasksLists[ uxTopPriority ] ) ) )							\ 
//  180 		{																								\ 
//  181 			configASSERT( uxTopPriority );																\ 
//  182 			--uxTopPriority;																			\ 
//  183 		}																								\ 
//  184 																										\ 
//  185 		/* listGET_OWNER_OF_NEXT_ENTRY indexes through the list, so the tasks of						\ 
//  186 		the	same priority get an equal share of the processor time. */									\ 
//  187 		listGET_OWNER_OF_NEXT_ENTRY( pxCurrentTCB, &( pxReadyTasksLists[ uxTopPriority ] ) );			\ 
//  188 		uxTopReadyPriority = uxTopPriority;																\ 
//  189 	} /* taskSELECT_HIGHEST_PRIORITY_TASK */
//  190 
//  191 	/*-----------------------------------------------------------*/
//  192 
//  193 	/* Define away taskRESET_READY_PRIORITY() and portRESET_READY_PRIORITY() as
//  194 	they are only required when a port optimised method of task selection is
//  195 	being used. */
//  196 	#define taskRESET_READY_PRIORITY( uxPriority )
//  197 	#define portRESET_READY_PRIORITY( uxPriority, uxTopReadyPriority )
//  198 
//  199 #else /* configUSE_PORT_OPTIMISED_TASK_SELECTION */
//  200 
//  201 	/* If configUSE_PORT_OPTIMISED_TASK_SELECTION is 1 then task selection is
//  202 	performed in a way that is tailored to the particular microcontroller
//  203 	architecture being used. */
//  204 
//  205 	/* A port optimised version is provided.  Call the port defined macros. */
//  206 	#define taskRECORD_READY_PRIORITY( uxPriority )	portRECORD_READY_PRIORITY( uxPriority, uxTopReadyPriority )
//  207 
//  208 	/*-----------------------------------------------------------*/
//  209 
//  210 	#define taskSELECT_HIGHEST_PRIORITY_TASK()														\ 
//  211 	{																								\ 
//  212 	UBaseType_t uxTopPriority;																		\ 
//  213 																									\ 
//  214 		/* Find the highest priority list that contains ready tasks. */								\ 
//  215 		portGET_HIGHEST_PRIORITY( uxTopPriority, uxTopReadyPriority );								\ 
//  216 		configASSERT( listCURRENT_LIST_LENGTH( &( pxReadyTasksLists[ uxTopPriority ] ) ) > 0 );		\ 
//  217 		listGET_OWNER_OF_NEXT_ENTRY( pxCurrentTCB, &( pxReadyTasksLists[ uxTopPriority ] ) );		\ 
//  218 	} /* taskSELECT_HIGHEST_PRIORITY_TASK() */
//  219 
//  220 	/*-----------------------------------------------------------*/
//  221 
//  222 	/* A port optimised version is provided, call it only if the TCB being reset
//  223 	is being referenced from a ready list.  If it is referenced from a delayed
//  224 	or suspended list then it won't be in a ready list. */
//  225 	#define taskRESET_READY_PRIORITY( uxPriority )														\ 
//  226 	{																									\ 
//  227 		if( listCURRENT_LIST_LENGTH( &( pxReadyTasksLists[ ( uxPriority ) ] ) ) == ( UBaseType_t ) 0 )	\ 
//  228 		{																								\ 
//  229 			portRESET_READY_PRIORITY( ( uxPriority ), ( uxTopReadyPriority ) );							\ 
//  230 		}																								\ 
//  231 	}
//  232 
//  233 #endif /* configUSE_PORT_OPTIMISED_TASK_SELECTION */
//  234 
//  235 /*-----------------------------------------------------------*/
//  236 
//  237 /* pxDelayedTaskList and pxOverflowDelayedTaskList are switched when the tick
//  238 count overflows. */
//  239 #define taskSWITCH_DELAYED_LISTS()																	\ 
//  240 {																									\ 
//  241 	List_t *pxTemp;																					\ 
//  242 																									\ 
//  243 	/* The delayed tasks list should be empty when the lists are switched. */						\ 
//  244 	configASSERT( ( listLIST_IS_EMPTY( pxDelayedTaskList ) ) );										\ 
//  245 																									\ 
//  246 	pxTemp = pxDelayedTaskList;																		\ 
//  247 	pxDelayedTaskList = pxOverflowDelayedTaskList;													\ 
//  248 	pxOverflowDelayedTaskList = pxTemp;																\ 
//  249 	xNumOfOverflows++;																				\ 
//  250 	prvResetNextTaskUnblockTime();																	\ 
//  251 }
//  252 
//  253 /*-----------------------------------------------------------*/
//  254 
//  255 /*
//  256  * Place the task represented by pxTCB into the appropriate ready list for
//  257  * the task.  It is inserted at the end of the list.
//  258  */
//  259 #define prvAddTaskToReadyList( pxTCB )																\ 
//  260 	traceMOVED_TASK_TO_READY_STATE( pxTCB );														\ 
//  261 	taskRECORD_READY_PRIORITY( ( pxTCB )->uxPriority );												\ 
//  262 	vListInsertEnd( &( pxReadyTasksLists[ ( pxTCB )->uxPriority ] ), &( ( pxTCB )->xStateListItem ) ); \ 
//  263 	tracePOST_MOVED_TASK_TO_READY_STATE( pxTCB )
//  264 /*-----------------------------------------------------------*/
//  265 
//  266 /*
//  267  * Several functions take an TaskHandle_t parameter that can optionally be NULL,
//  268  * where NULL is used to indicate that the handle of the currently executing
//  269  * task should be used in place of the parameter.  This macro simply checks to
//  270  * see if the parameter is NULL and returns a pointer to the appropriate TCB.
//  271  */
//  272 #define prvGetTCBFromHandle( pxHandle ) ( ( ( pxHandle ) == NULL ) ? ( TCB_t * ) pxCurrentTCB : ( TCB_t * ) ( pxHandle ) )
//  273 
//  274 /* The item value of the event list item is normally used to hold the priority
//  275 of the task to which it belongs (coded to allow it to be held in reverse
//  276 priority order).  However, it is occasionally borrowed for other purposes.  It
//  277 is important its value is not updated due to a task priority change while it is
//  278 being used for another purpose.  The following bit definition is used to inform
//  279 the scheduler that the value should not be changed - in which case it is the
//  280 responsibility of whichever module is using the value to ensure it gets set back
//  281 to its original value when it is released. */
//  282 #if( configUSE_16_BIT_TICKS == 1 )
//  283 	#define taskEVENT_LIST_ITEM_VALUE_IN_USE	0x8000U
//  284 #else
//  285 	#define taskEVENT_LIST_ITEM_VALUE_IN_USE	0x80000000UL
//  286 #endif
//  287 
//  288 /*
//  289  * Task control block.  A task control block (TCB) is allocated for each task,
//  290  * and stores task state information, including a pointer to the task's context
//  291  * (the task's run time environment, including register values)
//  292  */
//  293 typedef struct tskTaskControlBlock
//  294 {
//  295 	volatile StackType_t	*pxTopOfStack;	/*< Points to the location of the last item placed on the tasks stack.  THIS MUST BE THE FIRST MEMBER OF THE TCB STRUCT. */
//  296 
//  297 	#if ( portUSING_MPU_WRAPPERS == 1 )
//  298 		xMPU_SETTINGS	xMPUSettings;		/*< The MPU settings are defined as part of the port layer.  THIS MUST BE THE SECOND MEMBER OF THE TCB STRUCT. */
//  299 	#endif
//  300 
//  301 	ListItem_t			xStateListItem;	/*< The list that the state list item of a task is reference from denotes the state of that task (Ready, Blocked, Suspended ). */
//  302 	ListItem_t			xEventListItem;		/*< Used to reference a task from an event list. */
//  303 	UBaseType_t			uxPriority;			/*< The priority of the task.  0 is the lowest priority. */
//  304 	StackType_t			*pxStack;			/*< Points to the start of the stack. */
//  305 	char				pcTaskName[ configMAX_TASK_NAME_LEN ];/*< Descriptive name given to the task when created.  Facilitates debugging only. */ /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  306 
//  307 	#if ( portSTACK_GROWTH > 0 )
//  308 		StackType_t		*pxEndOfStack;		/*< Points to the end of the stack on architectures where the stack grows up from low memory. */
//  309 	#endif
//  310 
//  311 	#if ( portCRITICAL_NESTING_IN_TCB == 1 )
//  312 		UBaseType_t		uxCriticalNesting;	/*< Holds the critical section nesting depth for ports that do not maintain their own count in the port layer. */
//  313 	#endif
//  314 
//  315 	#if ( configUSE_TRACE_FACILITY == 1 )
//  316 		UBaseType_t		uxTCBNumber;		/*< Stores a number that increments each time a TCB is created.  It allows debuggers to determine when a task has been deleted and then recreated. */
//  317 		UBaseType_t		uxTaskNumber;		/*< Stores a number specifically for use by third party trace code. */
//  318 	#endif
//  319 
//  320 	#if ( configUSE_MUTEXES == 1 )
//  321 		UBaseType_t		uxBasePriority;		/*< The priority last assigned to the task - used by the priority inheritance mechanism. */
//  322 		UBaseType_t		uxMutexesHeld;
//  323 	#endif
//  324 
//  325 	#if ( configUSE_APPLICATION_TASK_TAG == 1 )
//  326 		TaskHookFunction_t pxTaskTag;
//  327 	#endif
//  328 
//  329 	#if( configNUM_THREAD_LOCAL_STORAGE_POINTERS > 0 )
//  330 		void *pvThreadLocalStoragePointers[ configNUM_THREAD_LOCAL_STORAGE_POINTERS ];
//  331 	#endif
//  332 
//  333 	#if( configGENERATE_RUN_TIME_STATS == 1 )
//  334 		uint32_t		ulRunTimeCounter;	/*< Stores the amount of time the task has spent in the Running state. */
//  335 	#endif
//  336 
//  337 	#if ( configUSE_NEWLIB_REENTRANT == 1 )
//  338 		/* Allocate a Newlib reent structure that is specific to this task.
//  339 		Note Newlib support has been included by popular demand, but is not
//  340 		used by the FreeRTOS maintainers themselves.  FreeRTOS is not
//  341 		responsible for resulting newlib operation.  User must be familiar with
//  342 		newlib and must provide system-wide implementations of the necessary
//  343 		stubs. Be warned that (at the time of writing) the current newlib design
//  344 		implements a system-wide malloc() that must be provided with locks. */
//  345 		struct	_reent xNewLib_reent;
//  346 	#endif
//  347 
//  348 	#if( configUSE_TASK_NOTIFICATIONS == 1 )
//  349 		volatile uint32_t ulNotifiedValue;
//  350 		volatile uint8_t ucNotifyState;
//  351 	#endif
//  352 
//  353 	/* See the comments above the definition of
//  354 	tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE. */
//  355 	#if( tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE != 0 )
//  356 		uint8_t	ucStaticallyAllocated; 		/*< Set to pdTRUE if the task is a statically allocated to ensure no attempt is made to free the memory. */
//  357 	#endif
//  358 
//  359 	#if( INCLUDE_xTaskAbortDelay == 1 )
//  360 		uint8_t ucDelayAborted;
//  361 	#endif
//  362 
//  363 } tskTCB;
//  364 
//  365 /* The old tskTCB name is maintained above then typedefed to the new TCB_t name
//  366 below to enable the use of older kernel aware debuggers. */
//  367 typedef tskTCB TCB_t;
//  368 
//  369 /*lint -e956 A manual analysis and inspection has been used to determine which
//  370 static variables must be declared volatile. */
//  371 

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//  372 PRIVILEGED_DATA TCB_t * volatile pxCurrentTCB = NULL;
_pxCurrentTCB:
        DS 4
        DS 2
        DS 2
        DS 4
        DS 2
        DS 2
        DS 2
        DS 2
        DS 2
        DS 2
        DS 4
        DS 4
        DS 2
//  373 
//  374 /* Lists for ready and blocked tasks. --------------------*/

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//  375 PRIVILEGED_DATA static List_t pxReadyTasksLists[ configMAX_PRIORITIES ];/*< Prioritised ready tasks. */
_pxReadyTasksLists:
        DS 126

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//  376 PRIVILEGED_DATA static List_t xDelayedTaskList1;						/*< Delayed tasks. */
//  377 PRIVILEGED_DATA static List_t xDelayedTaskList2;						/*< Delayed tasks (two lists are used - one for delays that have overflowed the current tick count. */
//  378 PRIVILEGED_DATA static List_t * volatile pxDelayedTaskList;				/*< Points to the delayed task list currently being used. */
//  379 PRIVILEGED_DATA static List_t * volatile pxOverflowDelayedTaskList;		/*< Points to the delayed task list currently being used to hold tasks that have overflowed the current tick count. */
//  380 PRIVILEGED_DATA static List_t xPendingReadyList;						/*< Tasks that have been readied while the scheduler was suspended.  They will be moved to the ready list when the scheduler is resumed. */
_xPendingReadyList:
        DS 18
        DS 18
        DS 4
        DS 4
        DS 18
        DS 18
        DS 18
//  381 
//  382 #if( INCLUDE_vTaskDelete == 1 )
//  383 
//  384 	PRIVILEGED_DATA static List_t xTasksWaitingTermination;				/*< Tasks that have been deleted - but their memory not yet freed. */
//  385 	PRIVILEGED_DATA static volatile UBaseType_t uxDeletedTasksWaitingCleanUp = ( UBaseType_t ) 0U;
//  386 
//  387 #endif
//  388 
//  389 #if ( INCLUDE_vTaskSuspend == 1 )
//  390 
//  391 	PRIVILEGED_DATA static List_t xSuspendedTaskList;					/*< Tasks that are currently suspended. */
//  392 
//  393 #endif
//  394 
//  395 /* Other file private variables. --------------------------------*/
//  396 PRIVILEGED_DATA static volatile UBaseType_t uxCurrentNumberOfTasks 	= ( UBaseType_t ) 0U;
//  397 PRIVILEGED_DATA static volatile TickType_t xTickCount 				= ( TickType_t ) 0U;
//  398 PRIVILEGED_DATA static volatile UBaseType_t uxTopReadyPriority 		= tskIDLE_PRIORITY;
//  399 PRIVILEGED_DATA static volatile BaseType_t xSchedulerRunning 		= pdFALSE;
//  400 PRIVILEGED_DATA static volatile UBaseType_t uxPendedTicks 			= ( UBaseType_t ) 0U;
//  401 PRIVILEGED_DATA static volatile BaseType_t xYieldPending 			= pdFALSE;
//  402 PRIVILEGED_DATA static volatile BaseType_t xNumOfOverflows 			= ( BaseType_t ) 0;
//  403 PRIVILEGED_DATA static UBaseType_t uxTaskNumber 					= ( UBaseType_t ) 0U;
//  404 PRIVILEGED_DATA static volatile TickType_t xNextTaskUnblockTime		= ( TickType_t ) 0U; /* Initialised to portMAX_DELAY before the scheduler starts. */
//  405 PRIVILEGED_DATA static TaskHandle_t xIdleTaskHandle					= NULL;			/*< Holds the handle of the idle task.  The idle task is created automatically when the scheduler is started. */
//  406 
//  407 /* Context switches are held pending while the scheduler is suspended.  Also,
//  408 interrupts must not manipulate the xStateListItem of a TCB, or any of the
//  409 lists the xStateListItem can be referenced from, if the scheduler is suspended.
//  410 If an interrupt needs to unblock a task while the scheduler is suspended then it
//  411 moves the task's event list item into the xPendingReadyList, ready for the
//  412 kernel to move the task from the pending ready list into the real ready list
//  413 when the scheduler is unsuspended.  The pending ready list itself can only be
//  414 accessed from a critical section. */
//  415 PRIVILEGED_DATA static volatile UBaseType_t uxSchedulerSuspended	= ( UBaseType_t ) pdFALSE;
//  416 
//  417 #if ( configGENERATE_RUN_TIME_STATS == 1 )
//  418 
//  419 	PRIVILEGED_DATA static uint32_t ulTaskSwitchedInTime = 0UL;	/*< Holds the value of a timer/counter the last time a task was switched in. */
//  420 	PRIVILEGED_DATA static uint32_t ulTotalRunTime = 0UL;		/*< Holds the total amount of execution time as defined by the run time counter clock. */
//  421 
//  422 #endif
//  423 
//  424 /*lint +e956 */
//  425 
//  426 /*-----------------------------------------------------------*/
//  427 
//  428 /* Callback function prototypes. --------------------------*/
//  429 #if(  configCHECK_FOR_STACK_OVERFLOW > 0 )
//  430 	extern void vApplicationStackOverflowHook( TaskHandle_t xTask, char *pcTaskName );
//  431 #endif
//  432 
//  433 #if( configUSE_TICK_HOOK > 0 )
//  434 	extern void vApplicationTickHook( void );
//  435 #endif
//  436 
//  437 #if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  438 	extern void vApplicationGetIdleTaskMemory( StaticTask_t **ppxIdleTaskTCBBuffer, StackType_t **ppxIdleTaskStackBuffer, uint32_t *pulIdleTaskStackSize );
//  439 #endif
//  440 
//  441 /* File private functions. --------------------------------*/
//  442 
//  443 /**
//  444  * Utility task that simply returns pdTRUE if the task referenced by xTask is
//  445  * currently in the Suspended state, or pdFALSE if the task referenced by xTask
//  446  * is in any other state.
//  447  */
//  448 #if ( INCLUDE_vTaskSuspend == 1 )
//  449 	static BaseType_t prvTaskIsTaskSuspended( const TaskHandle_t xTask ) PRIVILEGED_FUNCTION;
//  450 #endif /* INCLUDE_vTaskSuspend */
//  451 
//  452 /*
//  453  * Utility to ready all the lists used by the scheduler.  This is called
//  454  * automatically upon the creation of the first task.
//  455  */
//  456 static void prvInitialiseTaskLists( void ) PRIVILEGED_FUNCTION;
//  457 
//  458 /*
//  459  * The idle task, which as all tasks is implemented as a never ending loop.
//  460  * The idle task is automatically created and added to the ready lists upon
//  461  * creation of the first user task.
//  462  *
//  463  * The portTASK_FUNCTION_PROTO() macro is used to allow port/compiler specific
//  464  * language extensions.  The equivalent prototype for this function is:
//  465  *
//  466  * void prvIdleTask( void *pvParameters );
//  467  *
//  468  */
//  469 static portTASK_FUNCTION_PROTO( prvIdleTask, pvParameters );
//  470 
//  471 /*
//  472  * Utility to free all memory allocated by the scheduler to hold a TCB,
//  473  * including the stack pointed to by the TCB.
//  474  *
//  475  * This does not free memory allocated by the task itself (i.e. memory
//  476  * allocated by calls to pvPortMalloc from within the tasks application code).
//  477  */
//  478 #if ( INCLUDE_vTaskDelete == 1 )
//  479 
//  480 	static void prvDeleteTCB( TCB_t *pxTCB ) PRIVILEGED_FUNCTION;
//  481 
//  482 #endif
//  483 
//  484 /*
//  485  * Used only by the idle task.  This checks to see if anything has been placed
//  486  * in the list of tasks waiting to be deleted.  If so the task is cleaned up
//  487  * and its TCB deleted.
//  488  */
//  489 static void prvCheckTasksWaitingTermination( void ) PRIVILEGED_FUNCTION;
//  490 
//  491 /*
//  492  * The currently executing task is entering the Blocked state.  Add the task to
//  493  * either the current or the overflow delayed task list.
//  494  */
//  495 static void prvAddCurrentTaskToDelayedList( TickType_t xTicksToWait, const BaseType_t xCanBlockIndefinitely ) PRIVILEGED_FUNCTION;
//  496 
//  497 /*
//  498  * Fills an TaskStatus_t structure with information on each task that is
//  499  * referenced from the pxList list (which may be a ready list, a delayed list,
//  500  * a suspended list, etc.).
//  501  *
//  502  * THIS FUNCTION IS INTENDED FOR DEBUGGING ONLY, AND SHOULD NOT BE CALLED FROM
//  503  * NORMAL APPLICATION CODE.
//  504  */
//  505 #if ( configUSE_TRACE_FACILITY == 1 )
//  506 
//  507 	static UBaseType_t prvListTasksWithinSingleList( TaskStatus_t *pxTaskStatusArray, List_t *pxList, eTaskState eState ) PRIVILEGED_FUNCTION;
//  508 
//  509 #endif
//  510 
//  511 /*
//  512  * Searches pxList for a task with name pcNameToQuery - returning a handle to
//  513  * the task if it is found, or NULL if the task is not found.
//  514  */
//  515 #if ( INCLUDE_xTaskGetHandle == 1 )
//  516 
//  517 	static TCB_t *prvSearchForNameWithinSingleList( List_t *pxList, const char pcNameToQuery[] ) PRIVILEGED_FUNCTION;
//  518 
//  519 #endif
//  520 
//  521 /*
//  522  * When a task is created, the stack of the task is filled with a known value.
//  523  * This function determines the 'high water mark' of the task stack by
//  524  * determining how much of the stack remains at the original preset value.
//  525  */
//  526 #if ( ( configUSE_TRACE_FACILITY == 1 ) || ( INCLUDE_uxTaskGetStackHighWaterMark == 1 ) )
//  527 
//  528 	static uint16_t prvTaskCheckFreeStackSpace( const uint8_t * pucStackByte ) PRIVILEGED_FUNCTION;
//  529 
//  530 #endif
//  531 
//  532 /*
//  533  * Return the amount of time, in ticks, that will pass before the kernel will
//  534  * next move a task from the Blocked state to the Running state.
//  535  *
//  536  * This conditional compilation should use inequality to 0, not equality to 1.
//  537  * This is to ensure portSUPPRESS_TICKS_AND_SLEEP() can be called when user
//  538  * defined low power mode implementations require configUSE_TICKLESS_IDLE to be
//  539  * set to a value other than 1.
//  540  */
//  541 #if ( configUSE_TICKLESS_IDLE != 0 )
//  542 
//  543 	static TickType_t prvGetExpectedIdleTime( void ) PRIVILEGED_FUNCTION;
//  544 
//  545 #endif
//  546 
//  547 /*
//  548  * Set xNextTaskUnblockTime to the time at which the next Blocked state task
//  549  * will exit the Blocked state.
//  550  */
//  551 static void prvResetNextTaskUnblockTime( void );
//  552 
//  553 #if ( ( configUSE_TRACE_FACILITY == 1 ) && ( configUSE_STATS_FORMATTING_FUNCTIONS > 0 ) )
//  554 
//  555 	/*
//  556 	 * Helper function used to pad task names with spaces when printing out
//  557 	 * human readable tables of task information.
//  558 	 */
//  559 	static char *prvWriteNameToBuffer( char *pcBuffer, const char *pcTaskName ) PRIVILEGED_FUNCTION;
//  560 
//  561 #endif
//  562 
//  563 /*
//  564  * Called after a Task_t structure has been allocated either statically or
//  565  * dynamically to fill in the structure's members.
//  566  */
//  567 static void prvInitialiseNewTask( 	TaskFunction_t pxTaskCode,
//  568 									const char * const pcName,
//  569 									const uint32_t ulStackDepth,
//  570 									void * const pvParameters,
//  571 									UBaseType_t uxPriority,
//  572 									TaskHandle_t * const pxCreatedTask,
//  573 									TCB_t *pxNewTCB,
//  574 									const MemoryRegion_t * const xRegions ) PRIVILEGED_FUNCTION; /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  575 
//  576 /*
//  577  * Called after a new task has been created and initialised to place the task
//  578  * under the control of the scheduler.
//  579  */
//  580 static void prvAddNewTaskToReadyList( TCB_t *pxNewTCB ) PRIVILEGED_FUNCTION;
//  581 
//  582 /*-----------------------------------------------------------*/
//  583 
//  584 #if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  585 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _xTaskCreateStatic
        CODE
//  586 	TaskHandle_t xTaskCreateStatic(	TaskFunction_t pxTaskCode,
//  587 									const char * const pcName,
//  588 									const uint32_t ulStackDepth,
//  589 									void * const pvParameters,
//  590 									UBaseType_t uxPriority,
//  591 									StackType_t * const puxStackBuffer,
//  592 									StaticTask_t * const pxTaskBuffer ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  593 	{
_xTaskCreateStatic:
        ; * Stack frame (at entry) *
        ; Param size: 18
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 12
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
//  594 	TCB_t *pxNewTCB;
//  595 	TaskHandle_t xReturn;
//  596 
//  597 		configASSERT( puxStackBuffer != NULL );
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_0  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_0:
        BNZ       ??prvAddCurrentTaskToDelayedList_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_2  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_2:
        BR        S:??prvAddCurrentTaskToDelayedList_2  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  598 		configASSERT( pxTaskBuffer != NULL );
??prvAddCurrentTaskToDelayedList_1:
        MOV       A, [SP+0x20]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_3  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_3:
        BNZ       ??prvAddCurrentTaskToDelayedList_4  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_5  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_5:
        BR        S:??prvAddCurrentTaskToDelayedList_5  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_4:
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
//  599 
//  600 		if( ( pxTaskBuffer != NULL ) && ( puxStackBuffer != NULL ) )
//  601 		{
//  602 			/* The memory used for the task's TCB and stack are passed into this
//  603 			function - use them. */
//  604 			pxNewTCB = ( TCB_t * ) pxTaskBuffer; /*lint !e740 Unusual cast is ok as the structures are designed to have the same alignment, and the size is checked by an assert. */
//  605 			pxNewTCB->pxStack = ( StackType_t * ) puxStackBuffer;
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x24]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+16
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        XCHW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
//  606 
//  607 			#if( tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE != 0 )
//  608 			{
//  609 				/* Tasks can be created statically or dynamically, so note this
//  610 				task was created statically in case the task is later deleted. */
//  611 				pxNewTCB->ucStaticallyAllocated = tskSTATICALLY_ALLOCATED_STACK_AND_TCB;
//  612 			}
//  613 			#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
//  614 
//  615 			prvInitialiseNewTask( pxTaskCode, pcName, ulStackDepth, pvParameters, uxPriority, &xReturn, pxNewTCB, NULL );
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x24]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, [SP+0x24]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+38
          CFI FunCall _prvInitialiseNewTask
        CALL      F:_prvInitialiseNewTask  ;; 3 cycles
//  616 			prvAddNewTaskToReadyList( pxNewTCB );
        MOV       A, [SP+0x36]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x34]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvAddNewTaskToReadyList
        CALL      F:_prvAddNewTaskToReadyList  ;; 3 cycles
//  617 		}
//  618 		else
//  619 		{
//  620 			xReturn = NULL;
//  621 		}
//  622 
//  623 		return xReturn;
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x22          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 95 cycles
        ; ------------------------------------- Total: 176 cycles
//  624 	}
//  625 
//  626 #endif /* SUPPORT_STATIC_ALLOCATION */
//  627 /*-----------------------------------------------------------*/
//  628 
//  629 #if( portUSING_MPU_WRAPPERS == 1 )
//  630 
//  631 	BaseType_t xTaskCreateRestricted( const TaskParameters_t * const pxTaskDefinition, TaskHandle_t *pxCreatedTask )
//  632 	{
//  633 	TCB_t *pxNewTCB;
//  634 	BaseType_t xReturn = errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY;
//  635 
//  636 		configASSERT( pxTaskDefinition->puxStackBuffer );
//  637 
//  638 		if( pxTaskDefinition->puxStackBuffer != NULL )
//  639 		{
//  640 			/* Allocate space for the TCB.  Where the memory comes from depends
//  641 			on the implementation of the port malloc function and whether or
//  642 			not static allocation is being used. */
//  643 			pxNewTCB = ( TCB_t * ) pvPortMalloc( sizeof( TCB_t ) );
//  644 
//  645 			if( pxNewTCB != NULL )
//  646 			{
//  647 				/* Store the stack location in the TCB. */
//  648 				pxNewTCB->pxStack = pxTaskDefinition->puxStackBuffer;
//  649 
//  650 				/* Tasks can be created statically or dynamically, so note
//  651 				this task had a statically allocated stack in case it is
//  652 				later deleted.  The TCB was allocated dynamically. */
//  653 				pxNewTCB->ucStaticallyAllocated = tskSTATICALLY_ALLOCATED_STACK_ONLY;
//  654 
//  655 				prvInitialiseNewTask(	pxTaskDefinition->pvTaskCode,
//  656 										pxTaskDefinition->pcName,
//  657 										( uint32_t ) pxTaskDefinition->usStackDepth,
//  658 										pxTaskDefinition->pvParameters,
//  659 										pxTaskDefinition->uxPriority,
//  660 										pxCreatedTask, pxNewTCB,
//  661 										pxTaskDefinition->xRegions );
//  662 
//  663 				prvAddNewTaskToReadyList( pxNewTCB );
//  664 				xReturn = pdPASS;
//  665 			}
//  666 		}
//  667 
//  668 		return xReturn;
//  669 	}
//  670 
//  671 #endif /* portUSING_MPU_WRAPPERS */
//  672 /*-----------------------------------------------------------*/
//  673 
//  674 #if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
//  675 
//  676 	BaseType_t xTaskCreate(	TaskFunction_t pxTaskCode,
//  677 							const char * const pcName,
//  678 							const uint16_t usStackDepth,
//  679 							void * const pvParameters,
//  680 							UBaseType_t uxPriority,
//  681 							TaskHandle_t * const pxCreatedTask ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  682 	{
//  683 	TCB_t *pxNewTCB;
//  684 	BaseType_t xReturn;
//  685 
//  686 		/* If the stack grows down then allocate the stack then the TCB so the stack
//  687 		does not grow into the TCB.  Likewise if the stack grows up then allocate
//  688 		the TCB then the stack. */
//  689 		#if( portSTACK_GROWTH > 0 )
//  690 		{
//  691 			/* Allocate space for the TCB.  Where the memory comes from depends on
//  692 			the implementation of the port malloc function and whether or not static
//  693 			allocation is being used. */
//  694 			pxNewTCB = ( TCB_t * ) pvPortMalloc( sizeof( TCB_t ) );
//  695 
//  696 			if( pxNewTCB != NULL )
//  697 			{
//  698 				/* Allocate space for the stack used by the task being created.
//  699 				The base of the stack memory stored in the TCB so the task can
//  700 				be deleted later if required. */
//  701 				pxNewTCB->pxStack = ( StackType_t * ) pvPortMalloc( ( ( ( size_t ) usStackDepth ) * sizeof( StackType_t ) ) ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
//  702 
//  703 				if( pxNewTCB->pxStack == NULL )
//  704 				{
//  705 					/* Could not allocate the stack.  Delete the allocated TCB. */
//  706 					vPortFree( pxNewTCB );
//  707 					pxNewTCB = NULL;
//  708 				}
//  709 			}
//  710 		}
//  711 		#else /* portSTACK_GROWTH */
//  712 		{
//  713 		StackType_t *pxStack;
//  714 
//  715 			/* Allocate space for the stack used by the task being created. */
//  716 			pxStack = ( StackType_t * ) pvPortMalloc( ( ( ( size_t ) usStackDepth ) * sizeof( StackType_t ) ) ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
//  717 
//  718 			if( pxStack != NULL )
//  719 			{
//  720 				/* Allocate space for the TCB. */
//  721 				pxNewTCB = ( TCB_t * ) pvPortMalloc( sizeof( TCB_t ) ); /*lint !e961 MISRA exception as the casts are only redundant for some paths. */
//  722 
//  723 				if( pxNewTCB != NULL )
//  724 				{
//  725 					/* Store the stack location in the TCB. */
//  726 					pxNewTCB->pxStack = pxStack;
//  727 				}
//  728 				else
//  729 				{
//  730 					/* The stack cannot be used as the TCB was not created.  Free
//  731 					it again. */
//  732 					vPortFree( pxStack );
//  733 				}
//  734 			}
//  735 			else
//  736 			{
//  737 				pxNewTCB = NULL;
//  738 			}
//  739 		}
//  740 		#endif /* portSTACK_GROWTH */
//  741 
//  742 		if( pxNewTCB != NULL )
//  743 		{
//  744 			#if( tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE != 0 )
//  745 			{
//  746 				/* Tasks can be created statically or dynamically, so note this
//  747 				task was created dynamically in case it is later deleted. */
//  748 				pxNewTCB->ucStaticallyAllocated = tskDYNAMICALLY_ALLOCATED_STACK_AND_TCB;
//  749 			}
//  750 			#endif /* configSUPPORT_STATIC_ALLOCATION */
//  751 
//  752 			prvInitialiseNewTask( pxTaskCode, pcName, ( uint32_t ) usStackDepth, pvParameters, uxPriority, pxCreatedTask, pxNewTCB, NULL );
//  753 			prvAddNewTaskToReadyList( pxNewTCB );
//  754 			xReturn = pdPASS;
//  755 		}
//  756 		else
//  757 		{
//  758 			xReturn = errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY;
//  759 		}
//  760 
//  761 		return xReturn;
//  762 	}
//  763 
//  764 #endif /* configSUPPORT_DYNAMIC_ALLOCATION */
//  765 /*-----------------------------------------------------------*/
//  766 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _prvInitialiseNewTask
        CODE
//  767 static void prvInitialiseNewTask( 	TaskFunction_t pxTaskCode,
//  768 									const char * const pcName,
//  769 									const uint32_t ulStackDepth,
//  770 									void * const pvParameters,
//  771 									UBaseType_t uxPriority,
//  772 									TaskHandle_t * const pxCreatedTask,
//  773 									TCB_t *pxNewTCB,
//  774 									const MemoryRegion_t * const xRegions ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  775 {
_prvInitialiseNewTask:
        ; * Stack frame (at entry) *
        ; Param size: 22
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 18
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+22
//  776 StackType_t *pxTopOfStack;
//  777 UBaseType_t x;
//  778 
//  779 	#if( portUSING_MPU_WRAPPERS == 1 )
//  780 		/* Should the task be created in privileged mode? */
//  781 		BaseType_t xRunPrivileged;
//  782 		if( ( uxPriority & portPRIVILEGE_BIT ) != 0U )
//  783 		{
//  784 			xRunPrivileged = pdTRUE;
//  785 		}
//  786 		else
//  787 		{
//  788 			xRunPrivileged = pdFALSE;
//  789 		}
//  790 		uxPriority &= ~portPRIVILEGE_BIT;
//  791 	#endif /* portUSING_MPU_WRAPPERS == 1 */
//  792 
//  793 	/* Avoid dependency on memset() if it is not required. */
//  794 	#if( ( configCHECK_FOR_STACK_OVERFLOW > 1 ) || ( configUSE_TRACE_FACILITY == 1 ) || ( INCLUDE_uxTaskGetStackHighWaterMark == 1 ) )
//  795 	{
//  796 		/* Fill the stack with a known value to assist debugging. */
//  797 		( void ) memset( pxNewTCB->pxStack, ( int ) tskSTACK_FILL_BYTE, ( size_t ) ulStackDepth * sizeof( StackType_t ) );
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      BC, #0xA5          ;; 1 cycle
        MOV       A, [SP+0x28]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
//  798 	}
//  799 	#endif /* ( ( configCHECK_FOR_STACK_OVERFLOW > 1 ) || ( ( configUSE_TRACE_FACILITY == 1 ) || ( INCLUDE_uxTaskGetStackHighWaterMark == 1 ) ) ) */
//  800 
//  801 	/* Calculate the top of stack address.  This depends on whether the stack
//  802 	grows from high memory to low (as per the 80x86) or vice versa.
//  803 	portSTACK_GROWTH is used to make the result positive or negative as required
//  804 	by the port. */
//  805 	#if( portSTACK_GROWTH < 0 )
//  806 	{
//  807 		pxTopOfStack = pxNewTCB->pxStack + ( ulStackDepth - ( uint32_t ) 1 );
//  808 		pxTopOfStack = ( StackType_t * ) ( ( ( portPOINTER_SIZE_TYPE ) pxTopOfStack ) & ( ~( ( portPOINTER_SIZE_TYPE ) portBYTE_ALIGNMENT_MASK ) ) ); /*lint !e923 MISRA exception.  Avoiding casts between pointers and integers is not practical.  Size differences accounted for using portPOINTER_SIZE_TYPE type. */
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        SHLW      BC, 0x1            ;; 1 cycle
        MOV       A, [SP+0x28]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xFFFE        ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  809 
//  810 		/* Check the alignment of the calculated top of stack is correct. */
//  811 		configASSERT( ( ( ( portPOINTER_SIZE_TYPE ) pxTopOfStack & ( portPOINTER_SIZE_TYPE ) portBYTE_ALIGNMENT_MASK ) == 0UL ) );
//  812 	}
//  813 	#else /* portSTACK_GROWTH */
//  814 	{
//  815 		pxTopOfStack = pxNewTCB->pxStack;
//  816 
//  817 		/* Check the alignment of the stack buffer is correct. */
//  818 		configASSERT( ( ( ( portPOINTER_SIZE_TYPE ) pxNewTCB->pxStack & ( portPOINTER_SIZE_TYPE ) portBYTE_ALIGNMENT_MASK ) == 0UL ) );
//  819 
//  820 		/* The other extreme of the stack space is required if stack checking is
//  821 		performed. */
//  822 		pxNewTCB->pxEndOfStack = pxNewTCB->pxStack + ( ulStackDepth - ( uint32_t ) 1 );
//  823 	}
//  824 	#endif /* portSTACK_GROWTH */
//  825 
//  826 	/* Store the task name in the TCB. */
//  827 	for( x = ( UBaseType_t ) 0; x < ( UBaseType_t ) configMAX_TASK_NAME_LEN; x++ )
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 55 cycles
//  828 	{
//  829 		pxNewTCB->pcTaskName[ x ] = pcName[ x ];
??prvInitialiseNewTask_0:
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [SP+0x26]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL+0x32], A    ;; 2 cycles
//  830 
//  831 		/* Don't copy all configMAX_TASK_NAME_LEN if the string is shorter than
//  832 		configMAX_TASK_NAME_LEN characters just in case the memory after the
//  833 		string is not accessible (extremely unlikely). */
//  834 		if( pcName[ x ] == 0x00 )
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_6  ;; 4 cycles
        ; ------------------------------------- Block: 36 cycles
//  835 		{
//  836 			break;
//  837 		}
//  838 		else
//  839 		{
//  840 			mtCOVERAGE_TEST_MARKER();
//  841 		}
//  842 	}
        MOVW      AX, DE             ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        CMPW      AX, #0xC           ;; 1 cycle
        BC        ??prvInitialiseNewTask_0  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  843 
//  844 	/* Ensure the name string is terminated in the case that the string length
//  845 	was greater or equal to configMAX_TASK_NAME_LEN. */
//  846 	pxNewTCB->pcTaskName[ configMAX_TASK_NAME_LEN - 1 ] = '\0';
??prvAddCurrentTaskToDelayedList_6:
        MOV       A, [SP+0x26]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        ADDW      AX, #0x3D          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  847 
//  848 	/* This is used as an array index so must ensure it's not too large.  First
//  849 	remove the privilege bit if one is present. */
//  850 	if( uxPriority >= ( UBaseType_t ) configMAX_PRIORITIES )
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        CMPW      AX, #0x7           ;; 1 cycle
        BC        ??prvAddCurrentTaskToDelayedList_7  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
//  851 	{
//  852 		uxPriority = ( UBaseType_t ) configMAX_PRIORITIES - ( UBaseType_t ) 1U;
        MOVW      AX, #0x6           ;; 1 cycle
        MOVW      [SP+0x1E], AX      ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  853 	}
//  854 	else
//  855 	{
//  856 		mtCOVERAGE_TEST_MARKER();
//  857 	}
//  858 
//  859 	pxNewTCB->uxPriority = uxPriority;
??prvAddCurrentTaskToDelayedList_7:
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  860 	#if ( configUSE_MUTEXES == 1 )
//  861 	{
//  862 		pxNewTCB->uxBasePriority = uxPriority;
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  863 		pxNewTCB->uxMutexesHeld = 0;
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        ADDW      AX, #0x44          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  864 	}
//  865 	#endif /* configUSE_MUTEXES */
//  866 
//  867 	vListInitialiseItem( &( pxNewTCB->xStateListItem ) );
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInitialiseItem
        CALL      F:_vListInitialiseItem  ;; 3 cycles
//  868 	vListInitialiseItem( &( pxNewTCB->xEventListItem ) );
        MOV       A, [SP+0x26]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInitialiseItem
        CALL      F:_vListInitialiseItem  ;; 3 cycles
//  869 
//  870 	/* Set the pxNewTCB as a link back from the ListItem_t.  This is so we can get
//  871 	back to	the containing TCB from a generic item in a list. */
//  872 	listSET_LIST_ITEM_OWNER( &( pxNewTCB->xStateListItem ), pxNewTCB );
        MOV       A, [SP+0x26]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  873 
//  874 	/* Event lists are always in priority order. */
//  875 	listSET_LIST_ITEM_VALUE( &( pxNewTCB->xEventListItem ), ( TickType_t ) configMAX_PRIORITIES - ( TickType_t ) uxPriority ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, #0x7           ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x28]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  876 	listSET_LIST_ITEM_OWNER( &( pxNewTCB->xEventListItem ), pxNewTCB );
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  877 
//  878 	#if ( portCRITICAL_NESTING_IN_TCB == 1 )
//  879 	{
//  880 		pxNewTCB->uxCriticalNesting = ( UBaseType_t ) 0U;
//  881 	}
//  882 	#endif /* portCRITICAL_NESTING_IN_TCB */
//  883 
//  884 	#if ( configUSE_APPLICATION_TASK_TAG == 1 )
//  885 	{
//  886 		pxNewTCB->pxTaskTag = NULL;
//  887 	}
//  888 	#endif /* configUSE_APPLICATION_TASK_TAG */
//  889 
//  890 	#if ( configGENERATE_RUN_TIME_STATS == 1 )
//  891 	{
//  892 		pxNewTCB->ulRunTimeCounter = 0UL;
//  893 	}
//  894 	#endif /* configGENERATE_RUN_TIME_STATS */
//  895 
//  896 	#if ( portUSING_MPU_WRAPPERS == 1 )
//  897 	{
//  898 		vPortStoreTaskMPUSettings( &( pxNewTCB->xMPUSettings ), xRegions, pxNewTCB->pxStack, ulStackDepth );
//  899 	}
//  900 	#else
//  901 	{
//  902 		/* Avoid compiler warning about unreferenced parameter. */
//  903 		( void ) xRegions;
//  904 	}
//  905 	#endif
//  906 
//  907 	#if( configNUM_THREAD_LOCAL_STORAGE_POINTERS != 0 )
//  908 	{
//  909 		for( x = 0; x < ( UBaseType_t ) configNUM_THREAD_LOCAL_STORAGE_POINTERS; x++ )
//  910 		{
//  911 			pxNewTCB->pvThreadLocalStoragePointers[ x ] = NULL;
//  912 		}
//  913 	}
//  914 	#endif
//  915 
//  916 	#if ( configUSE_TASK_NOTIFICATIONS == 1 )
//  917 	{
//  918 		pxNewTCB->ulNotifiedValue = 0;
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  919 		pxNewTCB->ucNotifyState = taskNOT_WAITING_NOTIFICATION;
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  920 	}
//  921 	#endif
//  922 
//  923 	#if ( configUSE_NEWLIB_REENTRANT == 1 )
//  924 	{
//  925 		/* Initialise this task's Newlib reent structure. */
//  926 		_REENT_INIT_PTR( ( &( pxNewTCB->xNewLib_reent ) ) );
//  927 	}
//  928 	#endif
//  929 
//  930 	#if( INCLUDE_xTaskAbortDelay == 1 )
//  931 	{
//  932 		pxNewTCB->ucDelayAborted = pdFALSE;
//  933 	}
//  934 	#endif
//  935 
//  936 	/* Initialize the TCB stack to look as if the task was already running,
//  937 	but had been interrupted by the scheduler.  The return address is set
//  938 	to the start of the task function. Once the stack has been initialised
//  939 	the	top of stack variable is updated. */
//  940 	#if( portUSING_MPU_WRAPPERS == 1 )
//  941 	{
//  942 		pxNewTCB->pxTopOfStack = pxPortInitialiseStack( pxTopOfStack, pxTaskCode, pvParameters, xRunPrivileged );
//  943 	}
//  944 	#else /* portUSING_MPU_WRAPPERS */
//  945 	{
//  946 		pxNewTCB->pxTopOfStack = pxPortInitialiseStack( pxTopOfStack, pxTaskCode, pvParameters );
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _pxPortInitialiseStack
        CALL      F:_pxPortInitialiseStack  ;; 3 cycles
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x2E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  947 	}
//  948 	#endif /* portUSING_MPU_WRAPPERS */
//  949 
//  950 	if( ( void * ) pxCreatedTask != NULL )
        MOV       A, [SP+0x26]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_8  ;; 4 cycles
        ; ------------------------------------- Block: 163 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_8:
        BZ        ??prvAddCurrentTaskToDelayedList_9  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  951 	{
//  952 		/* Pass the handle out in an anonymous way.  The handle can be used to
//  953 		change the created task's priority, delete the created task, etc.*/
//  954 		*pxCreatedTask = ( TaskHandle_t ) pxNewTCB;
        MOV       A, [SP+0x26]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x26]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 20 cycles
//  955 	}
//  956 	else
//  957 	{
//  958 		mtCOVERAGE_TEST_MARKER();
//  959 	}
//  960 }
??prvAddCurrentTaskToDelayedList_9:
        ADDW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 311 cycles
//  961 /*-----------------------------------------------------------*/
//  962 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon0
          CFI Function _prvAddNewTaskToReadyList
        CODE
//  963 static void prvAddNewTaskToReadyList( TCB_t *pxNewTCB )
//  964 {
_prvAddNewTaskToReadyList:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  965 	/* Ensure interrupts don't access the task lists while the lists are being
//  966 	updated. */
//  967 	taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_10  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_10:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
//  968 	{
//  969 		uxCurrentNumberOfTasks++;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+6  ;; 3 cycles
//  970 		if( pxCurrentTCB == NULL )
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_11  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_11:
        BNZ       ??prvAddCurrentTaskToDelayedList_12  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  971 		{
//  972 			/* There are no other tasks, or all the other tasks are in
//  973 			the suspended state - make this the current task. */
//  974 			pxCurrentTCB = pxNewTCB;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_pxCurrentTCB)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  975 
//  976 			if( uxCurrentNumberOfTasks == ( UBaseType_t ) 1 )
        MOVW      AX, ES:_pxCurrentTCB+6  ;; 2 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_13  ;; 4 cycles
          CFI FunCall _prvInitialiseTaskLists
        ; ------------------------------------- Block: 19 cycles
//  977 			{
//  978 				/* This is the first task to be created so do the preliminary
//  979 				initialisation required.  We will not recover if this call
//  980 				fails, but we will report the failure. */
//  981 				prvInitialiseTaskLists();
        CALL      F:_prvInitialiseTaskLists  ;; 3 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_13  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  982 			}
//  983 			else
//  984 			{
//  985 				mtCOVERAGE_TEST_MARKER();
//  986 			}
//  987 		}
//  988 		else
//  989 		{
//  990 			/* If the scheduler is not already running, make this task the
//  991 			current task if it is the highest priority task to be created
//  992 			so far. */
//  993 			if( xSchedulerRunning == pdFALSE )
??prvAddCurrentTaskToDelayedList_12:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+14  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_13  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  994 			{
//  995 				if( pxCurrentTCB->uxPriority <= pxNewTCB->uxPriority )
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BC        ??prvAddCurrentTaskToDelayedList_13  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
//  996 				{
//  997 					pxCurrentTCB = pxNewTCB;
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_pxCurrentTCB)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        ; ------------------------------------- Block: 10 cycles
//  998 				}
//  999 				else
// 1000 				{
// 1001 					mtCOVERAGE_TEST_MARKER();
// 1002 				}
// 1003 			}
// 1004 			else
// 1005 			{
// 1006 				mtCOVERAGE_TEST_MARKER();
// 1007 			}
// 1008 		}
// 1009 
// 1010 		uxTaskNumber++;
??prvAddCurrentTaskToDelayedList_13:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+22  ;; 3 cycles
// 1011 
// 1012 		#if ( configUSE_TRACE_FACILITY == 1 )
// 1013 		{
// 1014 			/* Add a counter into the TCB for tracing only. */
// 1015 			pxNewTCB->uxTCBNumber = uxTaskNumber;
        MOVW      HL, ES:_pxCurrentTCB+22  ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x3E          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 1016 		}
// 1017 		#endif /* configUSE_TRACE_FACILITY */
// 1018 		traceTASK_CREATE( pxNewTCB );
// 1019 
// 1020 		prvAddTaskToReadyList( pxNewTCB );
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_14  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_14:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 1021 
// 1022 		portSETUP_TCB( pxNewTCB );
// 1023 	}
// 1024 	taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_15  ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_15  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1025 
// 1026 	if( xSchedulerRunning != pdFALSE )
??prvAddCurrentTaskToDelayedList_15:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+14  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_16  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 1027 	{
// 1028 		/* If the created task is of a higher priority than the current task
// 1029 		then it should run now. */
// 1030 		if( pxCurrentTCB->uxPriority < pxNewTCB->uxPriority )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        CMPW      AX, DE             ;; 1 cycle
        SKNC                         ;; 1 cycle
        ; ------------------------------------- Block: 18 cycles
// 1031 		{
// 1032 			taskYIELD_IF_USING_PREEMPTION();
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 1033 		}
// 1034 		else
// 1035 		{
// 1036 			mtCOVERAGE_TEST_MARKER();
// 1037 		}
// 1038 	}
// 1039 	else
// 1040 	{
// 1041 		mtCOVERAGE_TEST_MARKER();
// 1042 	}
// 1043 }
??prvAddCurrentTaskToDelayedList_16:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 222 cycles
// 1044 /*-----------------------------------------------------------*/
// 1045 
// 1046 #if ( INCLUDE_vTaskDelete == 1 )
// 1047 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon0
          CFI Function _vTaskDelete
        CODE
// 1048 	void vTaskDelete( TaskHandle_t xTaskToDelete )
// 1049 	{
_vTaskDelete:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 1050 	TCB_t *pxTCB;
// 1051 
// 1052 		taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_17  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_17:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1053 		{
// 1054 			/* If null is passed in here then it is the calling task that is
// 1055 			being deleted. */
// 1056 			pxTCB = prvGetTCBFromHandle( xTaskToDelete );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_18  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_18:
        BNZ       ??prvAddCurrentTaskToDelayedList_19  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_20  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_19:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_20:
        MOVW      [SP], AX           ;; 1 cycle
// 1057 
// 1058 			/* Remove task from the ready list. */
// 1059 			if( uxListRemove( &( pxTCB->xStateListItem ) ) == ( UBaseType_t ) 0 )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 1060 			{
// 1061 				taskRESET_READY_PRIORITY( pxTCB->uxPriority );
// 1062 			}
// 1063 			else
// 1064 			{
// 1065 				mtCOVERAGE_TEST_MARKER();
// 1066 			}
// 1067 
// 1068 			/* Is the task waiting on an event also? */
// 1069 			if( listLIST_ITEM_CONTAINER( &( pxTCB->xEventListItem ) ) != NULL )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_21  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_21:
        BZ        ??prvAddCurrentTaskToDelayedList_22  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1070 			{
// 1071 				( void ) uxListRemove( &( pxTCB->xEventListItem ) );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 1072 			}
// 1073 			else
// 1074 			{
// 1075 				mtCOVERAGE_TEST_MARKER();
// 1076 			}
// 1077 
// 1078 			/* Increment the uxTaskNumber also so kernel aware debuggers can
// 1079 			detect that the task lists need re-generating.  This is done before
// 1080 			portPRE_TASK_DELETE_HOOK() as in the Windows port that macro will
// 1081 			not return. */
// 1082 			uxTaskNumber++;
??prvAddCurrentTaskToDelayedList_22:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+22  ;; 3 cycles
// 1083 
// 1084 			if( pxTCB == pxCurrentTCB )
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_23  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_23:
        BNZ       ??prvAddCurrentTaskToDelayedList_24  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1085 			{
// 1086 				/* A task is deleting itself.  This cannot complete within the
// 1087 				task itself, as a context switch to another task is required.
// 1088 				Place the task in the termination list.  The idle task will
// 1089 				check the termination list and free up any memory allocated by
// 1090 				the scheduler for the TCB and stack of the deleted task. */
// 1091 				vListInsertEnd( &xTasksWaitingTermination, &( pxTCB->xStateListItem ) );
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_xPendingReadyList+18)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 1092 
// 1093 				/* Increment the ucTasksDeleted variable so the idle task knows
// 1094 				there is a task that has been deleted and that it should therefore
// 1095 				check the xTasksWaitingTermination list. */
// 1096 				++uxDeletedTasksWaitingCleanUp;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+4  ;; 3 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_25  ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
// 1097 
// 1098 				/* The pre-delete hook is primarily for the Windows simulator,
// 1099 				in which Windows specific clean up operations are performed,
// 1100 				after which it is not possible to yield away from this task -
// 1101 				hence xYieldPending is used to latch that a context switch is
// 1102 				required. */
// 1103 				portPRE_TASK_DELETE_HOOK( pxTCB, &xYieldPending );
// 1104 			}
// 1105 			else
// 1106 			{
// 1107 				--uxCurrentNumberOfTasks;
??prvAddCurrentTaskToDelayedList_24:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        DECW      ES:_pxCurrentTCB+6  ;; 3 cycles
// 1108 				prvDeleteTCB( pxTCB );
// 1109 
// 1110 				/* Reset the next expected unblock time in case it referred to
// 1111 				the task that has just been deleted. */
// 1112 				prvResetNextTaskUnblockTime();
          CFI FunCall _prvResetNextTaskUnblockTime
        CALL      F:_prvResetNextTaskUnblockTime  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 1113 			}
// 1114 
// 1115 			traceTASK_DELETE( pxTCB );
// 1116 		}
// 1117 		taskEXIT_CRITICAL();
??prvAddCurrentTaskToDelayedList_25:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_26  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_26  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1118 
// 1119 		/* Force a reschedule if it is the currently running task that has just
// 1120 		been deleted. */
// 1121 		if( xSchedulerRunning != pdFALSE )
??prvAddCurrentTaskToDelayedList_26:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+14  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_27  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 1122 		{
// 1123 			if( pxTCB == pxCurrentTCB )
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_28  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_28:
        BNZ       ??prvAddCurrentTaskToDelayedList_27  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1124 			{
// 1125 				configASSERT( uxSchedulerSuspended == 0 );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_29  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_30  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_30:
        BR        S:??prvAddCurrentTaskToDelayedList_30  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1126 				portYIELD_WITHIN_API();
??prvAddCurrentTaskToDelayedList_29:
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 1127 			}
// 1128 			else
// 1129 			{
// 1130 				mtCOVERAGE_TEST_MARKER();
// 1131 			}
// 1132 		}
// 1133 	}
??prvAddCurrentTaskToDelayedList_27:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 253 cycles
// 1134 
// 1135 #endif /* INCLUDE_vTaskDelete */
// 1136 /*-----------------------------------------------------------*/
// 1137 
// 1138 #if ( INCLUDE_vTaskDelayUntil == 1 )
// 1139 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon0
          CFI Function _vTaskDelayUntil
        CODE
// 1140 	void vTaskDelayUntil( TickType_t * const pxPreviousWakeTime, const TickType_t xTimeIncrement )
// 1141 	{
_vTaskDelayUntil:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
// 1142 	TickType_t xTimeToWake;
// 1143 	BaseType_t xAlreadyYielded, xShouldDelay = pdFALSE;
// 1144 
// 1145 		configASSERT( pxPreviousWakeTime );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_31  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_31:
        BNZ       ??prvAddCurrentTaskToDelayedList_32  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_33  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_33:
        BR        S:??prvAddCurrentTaskToDelayedList_33  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1146 		configASSERT( ( xTimeIncrement > 0U ) );
??prvAddCurrentTaskToDelayedList_32:
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_34  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_35  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_35:
        BR        S:??prvAddCurrentTaskToDelayedList_35  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1147 		configASSERT( uxSchedulerSuspended == 0 );
??prvAddCurrentTaskToDelayedList_34:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_36  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_37  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_37:
        BR        S:??prvAddCurrentTaskToDelayedList_37  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1148 
// 1149 		vTaskSuspendAll();
??prvAddCurrentTaskToDelayedList_36:
        INCW      ES:_pxCurrentTCB+32  ;; 3 cycles
// 1150 		{
// 1151 			/* Minor optimisation.  The tick count cannot change in this
// 1152 			block. */
// 1153 			const TickType_t xConstTickCount = xTickCount;
        MOVW      HL, #LWRD(_pxCurrentTCB+8)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 1154 
// 1155 			/* Generate the tick time at which the task wants to wake. */
// 1156 			xTimeToWake = *pxPreviousWakeTime + xTimeIncrement;
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1157 
// 1158 			if( xConstTickCount < *pxPreviousWakeTime )
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        BNC       ??prvAddCurrentTaskToDelayedList_38  ;; 4 cycles
        ; ------------------------------------- Block: 61 cycles
// 1159 			{
// 1160 				/* The tick count has overflowed since this function was
// 1161 				lasted called.  In this case the only time we should ever
// 1162 				actually delay is if the wake time has also	overflowed,
// 1163 				and the wake time is greater than the tick time.  When this
// 1164 				is the case it is as if neither time had overflowed. */
// 1165 				if( ( xTimeToWake < *pxPreviousWakeTime ) && ( xTimeToWake > xConstTickCount ) )
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        BC        ??prvAddCurrentTaskToDelayedList_39  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
??vTaskDelayUntil_0:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_40  ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
// 1166 				{
// 1167 					xShouldDelay = pdTRUE;
// 1168 				}
// 1169 				else
// 1170 				{
// 1171 					mtCOVERAGE_TEST_MARKER();
// 1172 				}
// 1173 			}
// 1174 			else
// 1175 			{
// 1176 				/* The tick time has not overflowed.  In this case we will
// 1177 				delay if either the wake time has overflowed, and/or the
// 1178 				tick time is less than the wake time. */
// 1179 				if( ( xTimeToWake < *pxPreviousWakeTime ) || ( xTimeToWake > xConstTickCount ) )
??prvAddCurrentTaskToDelayedList_38:
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        BC        ??prvAddCurrentTaskToDelayedList_41  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
??prvAddCurrentTaskToDelayedList_39:
        MOVW      HL, SP             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_42  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_42  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_42  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_42:
        BNC       ??vTaskDelayUntil_0  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1180 				{
// 1181 					xShouldDelay = pdTRUE;
??prvAddCurrentTaskToDelayedList_41:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 1182 				}
// 1183 				else
// 1184 				{
// 1185 					mtCOVERAGE_TEST_MARKER();
// 1186 				}
// 1187 			}
// 1188 
// 1189 			/* Update the wake time ready for the next call. */
// 1190 			*pxPreviousWakeTime = xTimeToWake;
// 1191 
// 1192 			if( xShouldDelay != pdFALSE )
// 1193 			{
// 1194 				traceTASK_DELAY_UNTIL( xTimeToWake );
// 1195 
// 1196 				/* prvAddCurrentTaskToDelayedList() needs the block time, not
// 1197 				the time to wake, so subtract the current tick count. */
// 1198 				prvAddCurrentTaskToDelayedList( xTimeToWake - xConstTickCount, pdFALSE );
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+16
          CFI FunCall _prvAddCurrentTaskToDelayedList
        CALL      F:_prvAddCurrentTaskToDelayedList  ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 34 cycles
// 1199 			}
// 1200 			else
// 1201 			{
// 1202 				mtCOVERAGE_TEST_MARKER();
// 1203 			}
// 1204 		}
// 1205 		xAlreadyYielded = xTaskResumeAll();
// 1206 
// 1207 		/* Force a reschedule if xTaskResumeAll has not already done so, we may
// 1208 		have put ourselves to sleep. */
// 1209 		if( xAlreadyYielded == pdFALSE )
??prvAddCurrentTaskToDelayedList_40:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 1210 		{
// 1211 			portYIELD_WITHIN_API();
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 1212 		}
// 1213 		else
// 1214 		{
// 1215 			mtCOVERAGE_TEST_MARKER();
// 1216 		}
// 1217 	}
??vTaskDelayUntil_1:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 284 cycles
// 1218 
// 1219 #endif /* INCLUDE_vTaskDelayUntil */
// 1220 /*-----------------------------------------------------------*/
// 1221 
// 1222 #if ( INCLUDE_vTaskDelay == 1 )
// 1223 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon1
          CFI Function _vTaskDelay
        CODE
// 1224 	void vTaskDelay( const TickType_t xTicksToDelay )
// 1225 	{
_vTaskDelay:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1226 	BaseType_t xAlreadyYielded = pdFALSE;
// 1227 
// 1228 		/* A delay time of zero just forces a reschedule. */
// 1229 		if( xTicksToDelay > ( TickType_t ) 0U )
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_43  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 1230 		{
// 1231 			configASSERT( uxSchedulerSuspended == 0 );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_44  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_45  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_45:
        BR        S:??prvAddCurrentTaskToDelayedList_45  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1232 			vTaskSuspendAll();
??prvAddCurrentTaskToDelayedList_44:
        INCW      ES:_pxCurrentTCB+32  ;; 3 cycles
// 1233 			{
// 1234 				traceTASK_DELAY();
// 1235 
// 1236 				/* A task that is removed from the event list while the
// 1237 				scheduler is suspended will not get placed in the ready
// 1238 				list or removed from the blocked list until the scheduler
// 1239 				is resumed.
// 1240 
// 1241 				This task cannot be in an event list as it is the currently
// 1242 				executing task. */
// 1243 				prvAddCurrentTaskToDelayedList( xTicksToDelay, pdFALSE );
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
          CFI FunCall _prvAddCurrentTaskToDelayedList
        CALL      F:_prvAddCurrentTaskToDelayedList  ;; 3 cycles
// 1244 			}
// 1245 			xAlreadyYielded = xTaskResumeAll();
// 1246 		}
// 1247 		else
// 1248 		{
// 1249 			mtCOVERAGE_TEST_MARKER();
// 1250 		}
// 1251 
// 1252 		/* Force a reschedule if xTaskResumeAll has not already done so, we may
// 1253 		have put ourselves to sleep. */
// 1254 		if( xAlreadyYielded == pdFALSE )
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
// 1255 		{
// 1256 			portYIELD_WITHIN_API();
??prvAddCurrentTaskToDelayedList_43:
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 1257 		}
// 1258 		else
// 1259 		{
// 1260 			mtCOVERAGE_TEST_MARKER();
// 1261 		}
// 1262 	}
??vTaskDelay_0:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 64 cycles
// 1263 
// 1264 #endif /* INCLUDE_vTaskDelay */
// 1265 /*-----------------------------------------------------------*/
// 1266 
// 1267 #if( ( INCLUDE_eTaskGetState == 1 ) || ( configUSE_TRACE_FACILITY == 1 ) )
// 1268 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon0
          CFI Function _eTaskGetState
          CFI NoCalls
        CODE
// 1269 	eTaskState eTaskGetState( TaskHandle_t xTask )
// 1270 	{
_eTaskGetState:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 1271 	eTaskState eReturn;
// 1272 	List_t *pxStateList;
// 1273 	const TCB_t * const pxTCB = ( TCB_t * ) xTask;
// 1274 
// 1275 		configASSERT( pxTCB );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_46  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_46:
        BNZ       ??prvAddCurrentTaskToDelayedList_47  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_48  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_48:
        BR        S:??prvAddCurrentTaskToDelayedList_48  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1276 
// 1277 		if( pxTCB == pxCurrentTCB )
??prvAddCurrentTaskToDelayedList_47:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_49  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_49:
        BNZ       ??prvAddCurrentTaskToDelayedList_50  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1278 		{
// 1279 			/* The task calling this function is querying its own state. */
// 1280 			eReturn = eRunning;
        CLRB      A                  ;; 1 cycle
        BR        R:??prvAddCurrentTaskToDelayedList_51  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1281 		}
// 1282 		else
// 1283 		{
// 1284 			taskENTER_CRITICAL();
??prvAddCurrentTaskToDelayedList_50:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_52  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_52:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1285 			{
// 1286 				pxStateList = ( List_t * ) listLIST_ITEM_CONTAINER( &( pxTCB->xStateListItem ) );
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 1287 			}
// 1288 			taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_53  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_53  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1289 
// 1290 			if( ( pxStateList == pxDelayedTaskList ) || ( pxStateList == pxOverflowDelayedTaskList ) )
??prvAddCurrentTaskToDelayedList_53:
        MOVW      HL, #LWRD(_xPendingReadyList+36)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_54  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_54:
        BZ        ??prvAddCurrentTaskToDelayedList_55  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      HL, #LWRD(_xPendingReadyList+40)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_56  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_56:
        BZ        ??prvAddCurrentTaskToDelayedList_55  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1291 			{
// 1292 				/* The task being queried is referenced from one of the Blocked
// 1293 				lists. */
// 1294 				eReturn = eBlocked;
// 1295 			}
// 1296 
// 1297 			#if ( INCLUDE_vTaskSuspend == 1 )
// 1298 				else if( pxStateList == &xSuspendedTaskList )
        MOV       A, ES              ;; 1 cycle
        CMP       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_57  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #LWRD(_xPendingReadyList+44)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_57:
        BNZ       ??prvAddCurrentTaskToDelayedList_58  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1299 				{
// 1300 					/* The task being queried is referenced from the suspended
// 1301 					list.  Is it genuinely suspended or is it block
// 1302 					indefinitely? */
// 1303 					if( listLIST_ITEM_CONTAINER( &( pxTCB->xEventListItem ) ) == NULL )
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_59  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_59:
        BNZ       ??prvAddCurrentTaskToDelayedList_55  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1304 					{
// 1305 						eReturn = eSuspended;
        MOV       A, #0x3            ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_51  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1306 					}
// 1307 					else
// 1308 					{
// 1309 						eReturn = eBlocked;
??prvAddCurrentTaskToDelayedList_55:
        MOV       A, #0x2            ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_51  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1310 					}
// 1311 				}
// 1312 			#endif
// 1313 
// 1314 			#if ( INCLUDE_vTaskDelete == 1 )
// 1315 				else if( ( pxStateList == &xTasksWaitingTermination ) || ( pxStateList == NULL ) )
??prvAddCurrentTaskToDelayedList_58:
        MOV       A, ES              ;; 1 cycle
        CMP       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_60  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #LWRD(_xPendingReadyList+18)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_60:
        BZ        ??prvAddCurrentTaskToDelayedList_61  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_62  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_62:
        BNZ       ??prvAddCurrentTaskToDelayedList_63  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1316 				{
// 1317 					/* The task being queried is referenced from the deleted
// 1318 					tasks list, or it is not referenced from any lists at
// 1319 					all. */
// 1320 					eReturn = eDeleted;
??prvAddCurrentTaskToDelayedList_61:
        MOV       A, #0x4            ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_51  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1321 				}
// 1322 			#endif
// 1323 
// 1324 			else /*lint !e525 Negative indentation is intended to make use of pre-processor clearer. */
// 1325 			{
// 1326 				/* If the task is not in any other state, it must be in the
// 1327 				Ready (including pending ready) state. */
// 1328 				eReturn = eReady;
??prvAddCurrentTaskToDelayedList_63:
        ONEB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1329 			}
// 1330 		}
// 1331 
// 1332 		return eReturn;
??prvAddCurrentTaskToDelayedList_51:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 266 cycles
// 1333 	} /*lint !e818 xTask cannot be a pointer to const because it is a typedef. */
// 1334 
// 1335 #endif /* INCLUDE_eTaskGetState */
// 1336 /*-----------------------------------------------------------*/
// 1337 
// 1338 #if ( INCLUDE_uxTaskPriorityGet == 1 )
// 1339 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon0
          CFI Function _uxTaskPriorityGet
          CFI NoCalls
        CODE
// 1340 	UBaseType_t uxTaskPriorityGet( TaskHandle_t xTask )
// 1341 	{
_uxTaskPriorityGet:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 1342 	TCB_t *pxTCB;
// 1343 	UBaseType_t uxReturn;
// 1344 
// 1345 		taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_64  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_64:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1346 		{
// 1347 			/* If null is passed in here then it is the priority of the that
// 1348 			called uxTaskPriorityGet() that is being queried. */
// 1349 			pxTCB = prvGetTCBFromHandle( xTask );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_65  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_65:
        BNZ       ??prvAddCurrentTaskToDelayedList_66  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_67  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_66:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_67:
        MOVW      [SP], AX           ;; 1 cycle
// 1350 			uxReturn = pxTCB->uxPriority;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
// 1351 		}
// 1352 		taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_68  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_68  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1353 
// 1354 		return uxReturn;
??prvAddCurrentTaskToDelayedList_68:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 93 cycles
// 1355 	}
// 1356 
// 1357 #endif /* INCLUDE_uxTaskPriorityGet */
// 1358 /*-----------------------------------------------------------*/
// 1359 
// 1360 #if ( INCLUDE_uxTaskPriorityGet == 1 )
// 1361 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon0
          CFI Function _uxTaskPriorityGetFromISR
          CFI NoCalls
        CODE
// 1362 	UBaseType_t uxTaskPriorityGetFromISR( TaskHandle_t xTask )
// 1363 	{
_uxTaskPriorityGetFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 1364 	TCB_t *pxTCB;
// 1365 	UBaseType_t uxReturn, uxSavedInterruptState;
// 1366 
// 1367 		/* RTOS ports that support interrupt nesting have the concept of a
// 1368 		maximum	system call (or maximum API call) interrupt priority.
// 1369 		Interrupts that are	above the maximum system call priority are keep
// 1370 		permanently enabled, even when the RTOS kernel is in a critical section,
// 1371 		but cannot make any calls to FreeRTOS API functions.  If configASSERT()
// 1372 		is defined in FreeRTOSConfig.h then
// 1373 		portASSERT_IF_INTERRUPT_PRIORITY_INVALID() will result in an assertion
// 1374 		failure if a FreeRTOS API function is called from an interrupt that has
// 1375 		been assigned a priority above the configured maximum system call
// 1376 		priority.  Only FreeRTOS functions that end in FromISR can be called
// 1377 		from interrupts	that have been assigned a priority at or (logically)
// 1378 		below the maximum system call interrupt priority.  FreeRTOS maintains a
// 1379 		separate interrupt safe API to ensure interrupt entry is as fast and as
// 1380 		simple as possible.  More information (albeit Cortex-M specific) is
// 1381 		provided on the following link:
// 1382 		http://www.freertos.org/RTOS-Cortex-M3-M4.html */
// 1383 		portASSERT_IF_INTERRUPT_PRIORITY_INVALID();
// 1384 
// 1385 		uxSavedInterruptState = portSET_INTERRUPT_MASK_FROM_ISR();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 1386 		{
// 1387 			/* If null is passed in here then it is the priority of the calling
// 1388 			task that is being queried. */
// 1389 			pxTCB = prvGetTCBFromHandle( xTask );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_69  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_69:
        BNZ       ??prvAddCurrentTaskToDelayedList_70  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_71  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_70:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_71:
        MOVW      [SP], AX           ;; 1 cycle
// 1390 			uxReturn = pxTCB->uxPriority;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
// 1391 		}
// 1392 		portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptState );
        MOV       A, C               ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        ROL       A, 0x1             ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, C               ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 1393 
// 1394 		return uxReturn;
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 27 cycles
        ; ------------------------------------- Total: 71 cycles
// 1395 	}
// 1396 
// 1397 #endif /* INCLUDE_uxTaskPriorityGet */
// 1398 /*-----------------------------------------------------------*/
// 1399 
// 1400 #if ( INCLUDE_vTaskPrioritySet == 1 )
// 1401 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon2
          CFI Function _vTaskPrioritySet
        CODE
// 1402 	void vTaskPrioritySet( TaskHandle_t xTask, UBaseType_t uxNewPriority )
// 1403 	{
_vTaskPrioritySet:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 16
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+20
// 1404 	TCB_t *pxTCB;
// 1405 	UBaseType_t uxCurrentBasePriority, uxPriorityUsedOnEntry;
// 1406 	BaseType_t xYieldRequired = pdFALSE;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
// 1407 
// 1408 		configASSERT( ( uxNewPriority < configMAX_PRIORITIES ) );
        MOVW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x7           ;; 1 cycle
        BC        ??prvAddCurrentTaskToDelayedList_72  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_73  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_73:
        BR        S:??prvAddCurrentTaskToDelayedList_73  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1409 
// 1410 		/* Ensure the new priority is valid. */
// 1411 		if( uxNewPriority >= ( UBaseType_t ) configMAX_PRIORITIES )
// 1412 		{
// 1413 			uxNewPriority = ( UBaseType_t ) configMAX_PRIORITIES - ( UBaseType_t ) 1U;
// 1414 		}
// 1415 		else
// 1416 		{
// 1417 			mtCOVERAGE_TEST_MARKER();
// 1418 		}
// 1419 
// 1420 		taskENTER_CRITICAL();
??prvAddCurrentTaskToDelayedList_72:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_74  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_74:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1421 		{
// 1422 			/* If null is passed in here then it is the priority of the calling
// 1423 			task that is being changed. */
// 1424 			pxTCB = prvGetTCBFromHandle( xTask );
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_75  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_75:
        BNZ       ??prvAddCurrentTaskToDelayedList_76  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_77  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_76:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_77:
        MOVW      [SP], AX           ;; 1 cycle
// 1425 
// 1426 			traceTASK_PRIORITY_SET( pxTCB, uxNewPriority );
// 1427 
// 1428 			#if ( configUSE_MUTEXES == 1 )
// 1429 			{
// 1430 				uxCurrentBasePriority = pxTCB->uxBasePriority;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
// 1431 			}
// 1432 			#else
// 1433 			{
// 1434 				uxCurrentBasePriority = pxTCB->uxPriority;
// 1435 			}
// 1436 			#endif
// 1437 
// 1438 			if( uxCurrentBasePriority != uxNewPriority )
        MOVW      AX, BC             ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_78  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 1439 			{
// 1440 				/* The priority change may have readied a task of higher
// 1441 				priority than the calling task. */
// 1442 				if( uxNewPriority > uxCurrentBasePriority )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        BNH       ??prvAddCurrentTaskToDelayedList_79  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 1443 				{
// 1444 					if( pxTCB != pxCurrentTCB )
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_80  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_80:
        BZ        ??prvAddCurrentTaskToDelayedList_81  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1445 					{
// 1446 						/* The priority of a task other than the currently
// 1447 						running task is being raised.  Is the priority being
// 1448 						raised above that of the running task? */
// 1449 						if( uxNewPriority >= pxCurrentTCB->uxPriority )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        BC        ??prvAddCurrentTaskToDelayedList_81  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_82  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1450 						{
// 1451 							xYieldRequired = pdTRUE;
// 1452 						}
// 1453 						else
// 1454 						{
// 1455 							mtCOVERAGE_TEST_MARKER();
// 1456 						}
// 1457 					}
// 1458 					else
// 1459 					{
// 1460 						/* The priority of the running task is being raised,
// 1461 						but the running task must already be the highest
// 1462 						priority task able to run so no yield is required. */
// 1463 					}
// 1464 				}
// 1465 				else if( pxTCB == pxCurrentTCB )
??prvAddCurrentTaskToDelayedList_79:
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_83  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_83:
        BNZ       ??prvAddCurrentTaskToDelayedList_81  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1466 				{
// 1467 					/* Setting the priority of the running task down means
// 1468 					there may now be another task of higher priority that
// 1469 					is ready to execute. */
// 1470 					xYieldRequired = pdTRUE;
??prvAddCurrentTaskToDelayedList_82:
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1471 				}
// 1472 				else
// 1473 				{
// 1474 					/* Setting the priority of any other task down does not
// 1475 					require a yield as the running task must be above the
// 1476 					new priority of the task being modified. */
// 1477 				}
// 1478 
// 1479 				/* Remember the ready list the task might be referenced from
// 1480 				before its uxPriority member is changed so the
// 1481 				taskRESET_READY_PRIORITY() macro can function correctly. */
// 1482 				uxPriorityUsedOnEntry = pxTCB->uxPriority;
??prvAddCurrentTaskToDelayedList_81:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 1483 
// 1484 				#if ( configUSE_MUTEXES == 1 )
// 1485 				{
// 1486 					/* Only change the priority being used if the task is not
// 1487 					currently using an inherited priority. */
// 1488 					if( pxTCB->uxBasePriority == pxTCB->uxPriority )
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_84  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
// 1489 					{
// 1490 						pxTCB->uxPriority = uxNewPriority;
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        ; ------------------------------------- Block: 8 cycles
// 1491 					}
// 1492 					else
// 1493 					{
// 1494 						mtCOVERAGE_TEST_MARKER();
// 1495 					}
// 1496 
// 1497 					/* The base priority gets set whatever. */
// 1498 					pxTCB->uxBasePriority = uxNewPriority;
??prvAddCurrentTaskToDelayedList_84:
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 1499 				}
// 1500 				#else
// 1501 				{
// 1502 					pxTCB->uxPriority = uxNewPriority;
// 1503 				}
// 1504 				#endif
// 1505 
// 1506 				/* Only reset the event list item value if the value is not
// 1507 				being used for anything else. */
// 1508 				if( ( listGET_LIST_ITEM_VALUE( &( pxTCB->xEventListItem ) ) & taskEVENT_LIST_ITEM_VALUE_IN_USE ) == 0UL )
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        ES:[HL].7, ??prvAddCurrentTaskToDelayedList_85  ;; 5 cycles
        ; ------------------------------------- Block: 19 cycles
// 1509 				{
// 1510 					listSET_LIST_ITEM_VALUE( &( pxTCB->xEventListItem ), ( ( TickType_t ) configMAX_PRIORITIES - ( TickType_t ) uxNewPriority ) ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, #0x7           ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 22 cycles
// 1511 				}
// 1512 				else
// 1513 				{
// 1514 					mtCOVERAGE_TEST_MARKER();
// 1515 				}
// 1516 
// 1517 				/* If the task is in the blocked or suspended list we need do
// 1518 				nothing more than change it's priority variable. However, if
// 1519 				the task is in a ready list it needs to be removed and placed
// 1520 				in the list appropriate to its new priority. */
// 1521 				if( listIS_CONTAINED_WITHIN( &( pxReadyTasksLists[ uxPriorityUsedOnEntry ] ), &( pxTCB->xStateListItem ) ) != pdFALSE )
??prvAddCurrentTaskToDelayedList_85:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_86  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_86:
        BNZ       ??prvAddCurrentTaskToDelayedList_87  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1522 				{
// 1523 					/* The task is currently in its ready list - remove before adding
// 1524 					it to it's new ready list.  As we are in a critical section we
// 1525 					can do this even if the scheduler is suspended. */
// 1526 					if( uxListRemove( &( pxTCB->xStateListItem ) ) == ( UBaseType_t ) 0 )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 1527 					{
// 1528 						/* It is known that the task is in its ready list so
// 1529 						there is no need to check again and the port level
// 1530 						reset macro can be called directly. */
// 1531 						portRESET_READY_PRIORITY( uxPriorityUsedOnEntry, uxTopReadyPriority );
// 1532 					}
// 1533 					else
// 1534 					{
// 1535 						mtCOVERAGE_TEST_MARKER();
// 1536 					}
// 1537 					prvAddTaskToReadyList( pxTCB );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_88  ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_88:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 1538 				}
// 1539 				else
// 1540 				{
// 1541 					mtCOVERAGE_TEST_MARKER();
// 1542 				}
// 1543 
// 1544 				if( xYieldRequired != pdFALSE )
??prvAddCurrentTaskToDelayedList_87:
        MOV       A, [SP+0x04]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 1545 				{
// 1546 					taskYIELD_IF_USING_PREEMPTION();
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 1547 				}
// 1548 				else
// 1549 				{
// 1550 					mtCOVERAGE_TEST_MARKER();
// 1551 				}
// 1552 
// 1553 				/* Remove compiler warning about unused variables when the port
// 1554 				optimised task selection is not being used. */
// 1555 				( void ) uxPriorityUsedOnEntry;
// 1556 			}
// 1557 		}
// 1558 		taskEXIT_CRITICAL();
??prvAddCurrentTaskToDelayedList_78:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_89  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_89  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1559 	}
??prvAddCurrentTaskToDelayedList_89:
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 368 cycles
// 1560 
// 1561 #endif /* INCLUDE_vTaskPrioritySet */
// 1562 /*-----------------------------------------------------------*/
// 1563 
// 1564 #if ( INCLUDE_vTaskSuspend == 1 )
// 1565 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon0
          CFI Function _vTaskSuspend
        CODE
// 1566 	void vTaskSuspend( TaskHandle_t xTaskToSuspend )
// 1567 	{
_vTaskSuspend:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 1568 	TCB_t *pxTCB;
// 1569 
// 1570 		taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_90  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_90:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1571 		{
// 1572 			/* If null is passed in here then it is the running task that is
// 1573 			being suspended. */
// 1574 			pxTCB = prvGetTCBFromHandle( xTaskToSuspend );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_91  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_91:
        BNZ       ??prvAddCurrentTaskToDelayedList_92  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_93  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_92:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_93:
        MOVW      [SP], AX           ;; 1 cycle
// 1575 
// 1576 			traceTASK_SUSPEND( pxTCB );
// 1577 
// 1578 			/* Remove task from the ready/delayed list and place in the
// 1579 			suspended list. */
// 1580 			if( uxListRemove( &( pxTCB->xStateListItem ) ) == ( UBaseType_t ) 0 )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 1581 			{
// 1582 				taskRESET_READY_PRIORITY( pxTCB->uxPriority );
// 1583 			}
// 1584 			else
// 1585 			{
// 1586 				mtCOVERAGE_TEST_MARKER();
// 1587 			}
// 1588 
// 1589 			/* Is the task waiting on an event also? */
// 1590 			if( listLIST_ITEM_CONTAINER( &( pxTCB->xEventListItem ) ) != NULL )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_94  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_94:
        BZ        ??prvAddCurrentTaskToDelayedList_95  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1591 			{
// 1592 				( void ) uxListRemove( &( pxTCB->xEventListItem ) );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 1593 			}
// 1594 			else
// 1595 			{
// 1596 				mtCOVERAGE_TEST_MARKER();
// 1597 			}
// 1598 
// 1599 			vListInsertEnd( &xSuspendedTaskList, &( pxTCB->xStateListItem ) );
??prvAddCurrentTaskToDelayedList_95:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_xPendingReadyList+44)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 1600 		}
// 1601 		taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_96  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_96  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1602 
// 1603 		if( xSchedulerRunning != pdFALSE )
??prvAddCurrentTaskToDelayedList_96:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+14  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_97  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 1604 		{
// 1605 			/* Reset the next expected unblock time in case it referred to the
// 1606 			task that is now in the Suspended state. */
// 1607 			taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_98  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_98:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1608 			{
// 1609 				prvResetNextTaskUnblockTime();
          CFI FunCall _prvResetNextTaskUnblockTime
        CALL      F:_prvResetNextTaskUnblockTime  ;; 3 cycles
// 1610 			}
// 1611 			taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_97  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_97  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1612 		}
// 1613 		else
// 1614 		{
// 1615 			mtCOVERAGE_TEST_MARKER();
// 1616 		}
// 1617 
// 1618 		if( pxTCB == pxCurrentTCB )
??prvAddCurrentTaskToDelayedList_97:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_99  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_99:
        BNZ       ??prvAddCurrentTaskToDelayedList_100  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1619 		{
// 1620 			if( xSchedulerRunning != pdFALSE )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+14  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_101  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 1621 			{
// 1622 				/* The current task has just been suspended. */
// 1623 				configASSERT( uxSchedulerSuspended == 0 );
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_102  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_103  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_103:
        BR        S:??prvAddCurrentTaskToDelayedList_103  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1624 				portYIELD_WITHIN_API();
??prvAddCurrentTaskToDelayedList_102:
        BRK                          ;; 5 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_100  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 1625 			}
// 1626 			else
// 1627 			{
// 1628 				/* The scheduler is not running, but the task that was pointed
// 1629 				to by pxCurrentTCB has just been suspended and pxCurrentTCB
// 1630 				must be adjusted to point to a different task. */
// 1631 				if( listCURRENT_LIST_LENGTH( &xSuspendedTaskList ) == uxCurrentNumberOfTasks )
??prvAddCurrentTaskToDelayedList_101:
        MOVW      HL, ES:_pxCurrentTCB+6  ;; 2 cycles
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:_xPendingReadyList+44  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_104  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 1632 				{
// 1633 					/* No other tasks are ready, so set pxCurrentTCB back to
// 1634 					NULL so when the next task is created pxCurrentTCB will
// 1635 					be set to point to it no matter what its relative priority
// 1636 					is. */
// 1637 					pxCurrentTCB = NULL;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        CLRB      ES:_pxCurrentTCB+2  ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_pxCurrentTCB, AX  ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_100  ;; 3 cycles
          CFI FunCall _vTaskSwitchContext
        ; ------------------------------------- Block: 9 cycles
// 1638 				}
// 1639 				else
// 1640 				{
// 1641 					vTaskSwitchContext();
??prvAddCurrentTaskToDelayedList_104:
        CALL      F:_vTaskSwitchContext  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1642 				}
// 1643 			}
// 1644 		}
// 1645 		else
// 1646 		{
// 1647 			mtCOVERAGE_TEST_MARKER();
// 1648 		}
// 1649 	}
??prvAddCurrentTaskToDelayedList_100:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 285 cycles
// 1650 
// 1651 #endif /* INCLUDE_vTaskSuspend */
// 1652 /*-----------------------------------------------------------*/
// 1653 
// 1654 #if ( INCLUDE_vTaskSuspend == 1 )
// 1655 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon0
          CFI Function _prvTaskIsTaskSuspended
          CFI NoCalls
        CODE
// 1656 	static BaseType_t prvTaskIsTaskSuspended( const TaskHandle_t xTask )
// 1657 	{
_prvTaskIsTaskSuspended:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 1658 	BaseType_t xReturn = pdFALSE;
        CLRW      BC                 ;; 1 cycle
// 1659 	const TCB_t * const pxTCB = ( TCB_t * ) xTask;
// 1660 
// 1661 		/* Accesses xPendingReadyList so must be called from a critical
// 1662 		section. */
// 1663 
// 1664 		/* It does not make sense to check if the calling task is suspended. */
// 1665 		configASSERT( xTask );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_105  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_105:
        BNZ       ??prvAddCurrentTaskToDelayedList_106  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_107  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_107:
        BR        S:??prvAddCurrentTaskToDelayedList_107  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1666 
// 1667 		/* Is the task being resumed actually in the suspended list? */
// 1668 		if( listIS_CONTAINED_WITHIN( &xSuspendedTaskList, &( pxTCB->xStateListItem ) ) != pdFALSE )
??prvAddCurrentTaskToDelayedList_106:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_108  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #LWRD(_xPendingReadyList+44)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_108:
        BNZ       ??prvAddCurrentTaskToDelayedList_109  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1669 		{
// 1670 			/* Has the task already been resumed from within an ISR? */
// 1671 			if( listIS_CONTAINED_WITHIN( &xPendingReadyList, &( pxTCB->xEventListItem ) ) == pdFALSE )
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_110  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #LWRD(_xPendingReadyList)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_110:
        BZ        ??prvAddCurrentTaskToDelayedList_109  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1672 			{
// 1673 				/* Is it in the suspended list because it is in the	Suspended
// 1674 				state, or because is is blocked with no timeout? */
// 1675 				if( listIS_CONTAINED_WITHIN( NULL, &( pxTCB->xEventListItem ) ) != pdFALSE )
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_111  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_111:
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1676 				{
// 1677 					xReturn = pdTRUE;
        ONEW      BC                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1678 				}
// 1679 				else
// 1680 				{
// 1681 					mtCOVERAGE_TEST_MARKER();
// 1682 				}
// 1683 			}
// 1684 			else
// 1685 			{
// 1686 				mtCOVERAGE_TEST_MARKER();
// 1687 			}
// 1688 		}
// 1689 		else
// 1690 		{
// 1691 			mtCOVERAGE_TEST_MARKER();
// 1692 		}
// 1693 
// 1694 		return xReturn;
??prvAddCurrentTaskToDelayedList_109:
        MOVW      AX, BC             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 108 cycles
// 1695 	} /*lint !e818 xTask cannot be a pointer to const because it is a typedef. */
// 1696 
// 1697 #endif /* INCLUDE_vTaskSuspend */
// 1698 /*-----------------------------------------------------------*/
// 1699 
// 1700 #if ( INCLUDE_vTaskSuspend == 1 )
// 1701 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon0
          CFI Function _vTaskResume
        CODE
// 1702 	void vTaskResume( TaskHandle_t xTaskToResume )
// 1703 	{
_vTaskResume:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1704 	TCB_t * const pxTCB = ( TCB_t * ) xTaskToResume;
// 1705 
// 1706 		/* It does not make sense to resume the calling task. */
// 1707 		configASSERT( xTaskToResume );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_112  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_112:
        BNZ       ??prvAddCurrentTaskToDelayedList_113  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_114  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_114:
        BR        S:??prvAddCurrentTaskToDelayedList_114  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1708 
// 1709 		/* The parameter cannot be NULL as it is impossible to resume the
// 1710 		currently executing task. */
// 1711 		if( ( pxTCB != NULL ) && ( pxTCB != pxCurrentTCB ) )
??prvAddCurrentTaskToDelayedList_113:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        POP       DE                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_115  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_115:
        SKNZ                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_116  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1712 		{
// 1713 			taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_117  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_117:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1714 			{
// 1715 				if( prvTaskIsTaskSuspended( pxTCB ) != pdFALSE )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        POP       DE                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvTaskIsTaskSuspended
        CALL      F:_prvTaskIsTaskSuspended  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_118  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
// 1716 				{
// 1717 					traceTASK_RESUME( pxTCB );
// 1718 
// 1719 					/* As we are in a critical section we can access the ready
// 1720 					lists even if the scheduler is suspended. */
// 1721 					( void ) uxListRemove(  &( pxTCB->xStateListItem ) );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 1722 					prvAddTaskToReadyList( pxTCB );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_119  ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_119:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 1723 
// 1724 					/* We may have just resumed a higher priority task. */
// 1725 					if( pxTCB->uxPriority >= pxCurrentTCB->uxPriority )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        SKC                          ;; 1 cycle
        ; ------------------------------------- Block: 43 cycles
// 1726 					{
// 1727 						/* This yield may not cause the task just resumed to run,
// 1728 						but will leave the lists in the correct state for the
// 1729 						next yield. */
// 1730 						taskYIELD_IF_USING_PREEMPTION();
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 1731 					}
// 1732 					else
// 1733 					{
// 1734 						mtCOVERAGE_TEST_MARKER();
// 1735 					}
// 1736 				}
// 1737 				else
// 1738 				{
// 1739 					mtCOVERAGE_TEST_MARKER();
// 1740 				}
// 1741 			}
// 1742 			taskEXIT_CRITICAL();
??prvAddCurrentTaskToDelayedList_118:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_116  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_116  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1743 		}
// 1744 		else
// 1745 		{
// 1746 			mtCOVERAGE_TEST_MARKER();
// 1747 		}
// 1748 	}
??prvAddCurrentTaskToDelayedList_116:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 203 cycles
// 1749 
// 1750 #endif /* INCLUDE_vTaskSuspend */
// 1751 
// 1752 /*-----------------------------------------------------------*/
// 1753 
// 1754 #if ( ( INCLUDE_xTaskResumeFromISR == 1 ) && ( INCLUDE_vTaskSuspend == 1 ) )
// 1755 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon0
          CFI Function _xTaskResumeFromISR
        CODE
// 1756 	BaseType_t xTaskResumeFromISR( TaskHandle_t xTaskToResume )
// 1757 	{
_xTaskResumeFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 1758 	BaseType_t xYieldRequired = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1759 	TCB_t * const pxTCB = ( TCB_t * ) xTaskToResume;
// 1760 	UBaseType_t uxSavedInterruptStatus;
// 1761 
// 1762 		configASSERT( xTaskToResume );
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_120  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_120:
        BNZ       ??prvAddCurrentTaskToDelayedList_121  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_122  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_122:
        BR        S:??prvAddCurrentTaskToDelayedList_122  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1763 
// 1764 		/* RTOS ports that support interrupt nesting have the concept of a
// 1765 		maximum	system call (or maximum API call) interrupt priority.
// 1766 		Interrupts that are	above the maximum system call priority are keep
// 1767 		permanently enabled, even when the RTOS kernel is in a critical section,
// 1768 		but cannot make any calls to FreeRTOS API functions.  If configASSERT()
// 1769 		is defined in FreeRTOSConfig.h then
// 1770 		portASSERT_IF_INTERRUPT_PRIORITY_INVALID() will result in an assertion
// 1771 		failure if a FreeRTOS API function is called from an interrupt that has
// 1772 		been assigned a priority above the configured maximum system call
// 1773 		priority.  Only FreeRTOS functions that end in FromISR can be called
// 1774 		from interrupts	that have been assigned a priority at or (logically)
// 1775 		below the maximum system call interrupt priority.  FreeRTOS maintains a
// 1776 		separate interrupt safe API to ensure interrupt entry is as fast and as
// 1777 		simple as possible.  More information (albeit Cortex-M specific) is
// 1778 		provided on the following link:
// 1779 		http://www.freertos.org/RTOS-Cortex-M3-M4.html */
// 1780 		portASSERT_IF_INTERRUPT_PRIORITY_INVALID();
// 1781 
// 1782 		uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
??prvAddCurrentTaskToDelayedList_121:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 1783 		{
// 1784 			if( prvTaskIsTaskSuspended( pxTCB ) != pdFALSE )
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvTaskIsTaskSuspended
        CALL      F:_prvTaskIsTaskSuspended  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_123  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 1785 			{
// 1786 				traceTASK_RESUME_FROM_ISR( pxTCB );
// 1787 
// 1788 				/* Check the ready lists can be accessed. */
// 1789 				if( uxSchedulerSuspended == ( UBaseType_t ) pdFALSE )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_124  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 1790 				{
// 1791 					/* Ready lists can be accessed so move the task from the
// 1792 					suspended list to the ready list directly. */
// 1793 					if( pxTCB->uxPriority >= pxCurrentTCB->uxPriority )
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BC        ??prvAddCurrentTaskToDelayedList_125  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 1794 					{
// 1795 						xYieldRequired = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1796 					}
// 1797 					else
// 1798 					{
// 1799 						mtCOVERAGE_TEST_MARKER();
// 1800 					}
// 1801 
// 1802 					( void ) uxListRemove( &( pxTCB->xStateListItem ) );
??prvAddCurrentTaskToDelayedList_125:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 1803 					prvAddTaskToReadyList( pxTCB );
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_126  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_126:
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        BR        S:??prvAddCurrentTaskToDelayedList_127  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 1804 				}
// 1805 				else
// 1806 				{
// 1807 					/* The delayed or ready lists cannot be accessed so the task
// 1808 					is held in the pending ready list until the scheduler is
// 1809 					unsuspended. */
// 1810 					vListInsertEnd( &( xPendingReadyList ), &( pxTCB->xEventListItem ) );
??prvAddCurrentTaskToDelayedList_124:
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_xPendingReadyList)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInsertEnd
        ; ------------------------------------- Block: 9 cycles
??prvAddCurrentTaskToDelayedList_127:
        CALL      F:_vListInsertEnd  ;; 3 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 3 cycles
// 1811 				}
// 1812 			}
// 1813 			else
// 1814 			{
// 1815 				mtCOVERAGE_TEST_MARKER();
// 1816 			}
// 1817 		}
// 1818 		portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
??prvAddCurrentTaskToDelayedList_123:
        REQUIRE ?Subroutine0
        ; // Fall through to label ?Subroutine0
// 1819 
// 1820 		return xYieldRequired;
// 1821 	}

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon0
          CFI NoFunction
          CFI CFA SP+12
        CODE
?Subroutine0:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        ROL       A, 0x1             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 19 cycles
        ; ------------------------------------- Total: 19 cycles
// 1822 
// 1823 #endif /* ( ( INCLUDE_xTaskResumeFromISR == 1 ) && ( INCLUDE_vTaskSuspend == 1 ) ) */
// 1824 /*-----------------------------------------------------------*/
// 1825 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon3
          CFI Function _vTaskStartScheduler
        CODE
// 1826 void vTaskStartScheduler( void )
// 1827 {
_vTaskStartScheduler:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 12
        SUBW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+16
// 1828 BaseType_t xReturn;
// 1829 
// 1830 	/* Add the idle task at the lowest priority. */
// 1831 	#if( configSUPPORT_STATIC_ALLOCATION == 1 )
// 1832 	{
// 1833 		StaticTask_t *pxIdleTaskTCBBuffer = NULL;
        MOV       [SP+0x0A], #0x0    ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 1834 		StackType_t *pxIdleTaskStackBuffer = NULL;
        MOV       [SP+0x06], #0x0    ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 1835 		uint32_t ulIdleTaskStackSize;
// 1836 
// 1837 		/* The Idle task is created using user provided RAM - obtain the
// 1838 		address of the RAM then create the idle task. */
// 1839 		vApplicationGetIdleTaskMemory( &pxIdleTaskTCBBuffer, &pxIdleTaskStackBuffer, &ulIdleTaskStackSize );
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _vApplicationGetIdleTaskMemory
        CALL      F:_vApplicationGetIdleTaskMemory  ;; 3 cycles
// 1840 		xIdleTaskHandle = xTaskCreateStatic(	prvIdleTask,
// 1841 												"IDLE",
// 1842 												ulIdleTaskStackSize,
// 1843 												( void * ) NULL,
// 1844 												( tskIDLE_PRIORITY | portPRIVILEGE_BIT ),
// 1845 												pxIdleTaskStackBuffer,
// 1846 												pxIdleTaskTCBBuffer ); /*lint !e961 MISRA exception, justified as it is not a redundant explicit cast to all supported compilers. */
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      BC, #LWRD(?_0)     ;; 1 cycle
        MOV       X, #BYTE3(?_0)     ;; 1 cycle
        MOVW      DE, #LWRD(_prvIdleTask)  ;; 1 cycle
        MOV       A, #BYTE3(_prvIdleTask)  ;; 1 cycle
          CFI FunCall _xTaskCreateStatic
        CALL      F:_xTaskCreateStatic  ;; 3 cycles
        MOVW      HL, #LWRD(_pxCurrentTCB+28)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1847 
// 1848 		if( xIdleTaskHandle != NULL )
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        ADDW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_128  ;; 4 cycles
        ; ------------------------------------- Block: 71 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_128:
        BZ        ??prvAddCurrentTaskToDelayedList_129  ;; 4 cycles
          CFI FunCall _xTimerCreateTimerTask
        ; ------------------------------------- Block: 4 cycles
// 1849 		{
// 1850 			xReturn = pdPASS;
// 1851 		}
// 1852 		else
// 1853 		{
// 1854 			xReturn = pdFAIL;
// 1855 		}
// 1856 	}
// 1857 	#else
// 1858 	{
// 1859 		/* The Idle task is being created using dynamically allocated RAM. */
// 1860 		xReturn = xTaskCreate(	prvIdleTask,
// 1861 								"IDLE", configMINIMAL_STACK_SIZE,
// 1862 								( void * ) NULL,
// 1863 								( tskIDLE_PRIORITY | portPRIVILEGE_BIT ),
// 1864 								&xIdleTaskHandle ); /*lint !e961 MISRA exception, justified as it is not a redundant explicit cast to all supported compilers. */
// 1865 	}
// 1866 	#endif /* configSUPPORT_STATIC_ALLOCATION */
// 1867 
// 1868 	#if ( configUSE_TIMERS == 1 )
// 1869 	{
// 1870 		if( xReturn == pdPASS )
// 1871 		{
// 1872 			xReturn = xTimerCreateTimerTask();
        CALL      F:_xTimerCreateTimerTask  ;; 3 cycles
// 1873 		}
// 1874 		else
// 1875 		{
// 1876 			mtCOVERAGE_TEST_MARKER();
// 1877 		}
// 1878 	}
// 1879 	#endif /* configUSE_TIMERS */
// 1880 
// 1881 	if( xReturn == pdPASS )
        CMPW      AX, #0x1           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_130  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 1882 	{
// 1883 		/* Interrupts are turned off here, to ensure a tick does not occur
// 1884 		before or during the call to xPortStartScheduler().  The stacks of
// 1885 		the created tasks contain a status word with interrupts switched on
// 1886 		so interrupts will automatically get re-enabled when the first task
// 1887 		starts to run. */
// 1888 		portDISABLE_INTERRUPTS();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_131  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1889 
// 1890 		#if ( configUSE_NEWLIB_REENTRANT == 1 )
// 1891 		{
// 1892 			/* Switch Newlib's _impure_ptr variable to point to the _reent
// 1893 			structure specific to the task that will run first. */
// 1894 			_impure_ptr = &( pxCurrentTCB->xNewLib_reent );
// 1895 		}
// 1896 		#endif /* configUSE_NEWLIB_REENTRANT */
// 1897 
// 1898 		xNextTaskUnblockTime = portMAX_DELAY;
??prvAddCurrentTaskToDelayedList_131:
        MOVW      HL, #LWRD(_pxCurrentTCB+24)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 1899 		xSchedulerRunning = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+14, AX  ;; 2 cycles
// 1900 		xTickCount = ( TickType_t ) 0U;
        MOVW      HL, #LWRD(_pxCurrentTCB+8)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 1901 
// 1902 		/* If configGENERATE_RUN_TIME_STATS is defined then the following
// 1903 		macro must be defined to configure the timer/counter used to generate
// 1904 		the run time counter time base. */
// 1905 		portCONFIGURE_TIMER_FOR_RUN_TIME_STATS();
// 1906 
// 1907 		/* Setting up the timer tick is hardware specific and thus in the
// 1908 		portable interface. */
// 1909 		if( xPortStartScheduler() != pdFALSE )
          CFI FunCall _xPortStartScheduler
        CALL      F:_xPortStartScheduler  ;; 3 cycles
        ; ------------------------------------- Block: 19 cycles
// 1910 		{
// 1911 			/* Should not reach here as if the scheduler is running the
// 1912 			function will not return. */
// 1913 		}
// 1914 		else
// 1915 		{
// 1916 			/* Should only reach here if a task calls xTaskEndScheduler(). */
// 1917 		}
// 1918 	}
// 1919 	else
// 1920 	{
// 1921 		/* This line will only be reached if the kernel could not be started,
// 1922 		because there was not enough FreeRTOS heap to create the idle task
// 1923 		or the timer task. */
// 1924 		configASSERT( xReturn != errCOULD_NOT_ALLOCATE_REQUIRED_MEMORY );
// 1925 	}
// 1926 
// 1927 	/* Prevent compiler warnings if INCLUDE_xTaskGetIdleTaskHandle is set to 0,
// 1928 	meaning xIdleTaskHandle is not used anywhere else. */
// 1929 	( void ) xIdleTaskHandle;
// 1930 }
??prvAddCurrentTaskToDelayedList_129:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+16
        ; ------------------------------------- Block: 7 cycles
??prvAddCurrentTaskToDelayedList_130:
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_129  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_132  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_132:
        BR        S:??prvAddCurrentTaskToDelayedList_132  ;; 3 cycles
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 150 cycles
// 1931 /*-----------------------------------------------------------*/
// 1932 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon3
          CFI Function _vTaskEndScheduler
        CODE
// 1933 void vTaskEndScheduler( void )
// 1934 {
_vTaskEndScheduler:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 1935 	/* Stop the scheduler interrupts and call the portable scheduler end
// 1936 	routine so the original ISRs can be restored if necessary.  The port
// 1937 	layer must ensure interrupts enable	bit is left in the correct state. */
// 1938 	portDISABLE_INTERRUPTS();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_133  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1939 	xSchedulerRunning = pdFALSE;
??prvAddCurrentTaskToDelayedList_133:
        CLRW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+14, AX  ;; 2 cycles
// 1940 	vPortEndScheduler();
          CFI FunCall _vPortEndScheduler
        CALL      F:_vPortEndScheduler  ;; 3 cycles
// 1941 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 26 cycles
// 1942 /*----------------------------------------------------------*/
// 1943 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon3
          CFI Function _vTaskSuspendAll
          CFI NoCalls
        CODE
// 1944 void vTaskSuspendAll( void )
// 1945 {
_vTaskSuspendAll:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 1946 	/* A critical section is not required as the variable is of type
// 1947 	BaseType_t.  Please read Richard Barry's reply in the following link to a
// 1948 	post in the FreeRTOS support forum before reporting this as a bug! -
// 1949 	http://goo.gl/wu4acr */
// 1950 	++uxSchedulerSuspended;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+32  ;; 3 cycles
// 1951 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 1952 /*----------------------------------------------------------*/
// 1953 
// 1954 #if ( configUSE_TICKLESS_IDLE != 0 )
// 1955 
// 1956 	static TickType_t prvGetExpectedIdleTime( void )
// 1957 	{
// 1958 	TickType_t xReturn;
// 1959 	UBaseType_t uxHigherPriorityReadyTasks = pdFALSE;
// 1960 
// 1961 		/* uxHigherPriorityReadyTasks takes care of the case where
// 1962 		configUSE_PREEMPTION is 0, so there may be tasks above the idle priority
// 1963 		task that are in the Ready state, even though the idle task is
// 1964 		running. */
// 1965 		#if( configUSE_PORT_OPTIMISED_TASK_SELECTION == 0 )
// 1966 		{
// 1967 			if( uxTopReadyPriority > tskIDLE_PRIORITY )
// 1968 			{
// 1969 				uxHigherPriorityReadyTasks = pdTRUE;
// 1970 			}
// 1971 		}
// 1972 		#else
// 1973 		{
// 1974 			const UBaseType_t uxLeastSignificantBit = ( UBaseType_t ) 0x01;
// 1975 
// 1976 			/* When port optimised task selection is used the uxTopReadyPriority
// 1977 			variable is used as a bit map.  If bits other than the least
// 1978 			significant bit are set then there are tasks that have a priority
// 1979 			above the idle priority that are in the Ready state.  This takes
// 1980 			care of the case where the co-operative scheduler is in use. */
// 1981 			if( uxTopReadyPriority > uxLeastSignificantBit )
// 1982 			{
// 1983 				uxHigherPriorityReadyTasks = pdTRUE;
// 1984 			}
// 1985 		}
// 1986 		#endif
// 1987 
// 1988 		if( pxCurrentTCB->uxPriority > tskIDLE_PRIORITY )
// 1989 		{
// 1990 			xReturn = 0;
// 1991 		}
// 1992 		else if( listCURRENT_LIST_LENGTH( &( pxReadyTasksLists[ tskIDLE_PRIORITY ] ) ) > 1 )
// 1993 		{
// 1994 			/* There are other idle priority tasks in the ready state.  If
// 1995 			time slicing is used then the very next tick interrupt must be
// 1996 			processed. */
// 1997 			xReturn = 0;
// 1998 		}
// 1999 		else if( uxHigherPriorityReadyTasks != pdFALSE )
// 2000 		{
// 2001 			/* There are tasks in the Ready state that have a priority above the
// 2002 			idle priority.  This path can only be reached if
// 2003 			configUSE_PREEMPTION is 0. */
// 2004 			xReturn = 0;
// 2005 		}
// 2006 		else
// 2007 		{
// 2008 			xReturn = xNextTaskUnblockTime - xTickCount;
// 2009 		}
// 2010 
// 2011 		return xReturn;
// 2012 	}
// 2013 
// 2014 #endif /* configUSE_TICKLESS_IDLE */
// 2015 /*----------------------------------------------------------*/
// 2016 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock18 Using cfiCommon3
          CFI Function _xTaskResumeAll
        CODE
// 2017 BaseType_t xTaskResumeAll( void )
// 2018 {
_xTaskResumeAll:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 2019 TCB_t *pxTCB = NULL;
        MOV       [SP+0x02], #0x0    ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 2020 BaseType_t xAlreadyYielded = pdFALSE;
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2021 
// 2022 	/* If uxSchedulerSuspended is zero then this function does not match a
// 2023 	previous call to vTaskSuspendAll(). */
// 2024 	configASSERT( uxSchedulerSuspended );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_134  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_135  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_135:
        BR        S:??prvAddCurrentTaskToDelayedList_135  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2025 
// 2026 	/* It is possible that an ISR caused a task to be removed from an event
// 2027 	list while the scheduler was suspended.  If this was the case then the
// 2028 	removed task will have been added to the xPendingReadyList.  Once the
// 2029 	scheduler has been resumed it is safe to move all the pending ready
// 2030 	tasks from this list into their appropriate ready list. */
// 2031 	taskENTER_CRITICAL();
??prvAddCurrentTaskToDelayedList_134:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_136  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_136:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 2032 	{
// 2033 		--uxSchedulerSuspended;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        DECW      ES:_pxCurrentTCB+32  ;; 3 cycles
// 2034 
// 2035 		if( uxSchedulerSuspended == ( UBaseType_t ) pdFALSE )
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_137  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
// 2036 		{
// 2037 			if( uxCurrentNumberOfTasks > ( UBaseType_t ) 0U )
        MOVW      AX, ES:_pxCurrentTCB+6  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_138  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_137  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2038 			{
// 2039 				/* Move any readied tasks from the pending list into the
// 2040 				appropriate ready list. */
// 2041 				while( listLIST_IS_EMPTY( &xPendingReadyList ) == pdFALSE )
// 2042 				{
// 2043 					pxTCB = ( TCB_t * ) listGET_OWNER_OF_HEAD_ENTRY( ( &xPendingReadyList ) );
??xTaskResumeAll_0:
        MOVW      HL, #LWRD(_xPendingReadyList+10)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 2044 					( void ) uxListRemove( &( pxTCB->xEventListItem ) );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 2045 					( void ) uxListRemove( &( pxTCB->xStateListItem ) );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 2046 					prvAddTaskToReadyList( pxTCB );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_139  ;; 4 cycles
        ; ------------------------------------- Block: 47 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_139:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       E, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        MOV       A, E               ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 2047 
// 2048 					/* If the moved task has a priority higher than the current
// 2049 					task then a yield must be performed. */
// 2050 					if( pxTCB->uxPriority >= pxCurrentTCB->uxPriority )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BC        ??prvAddCurrentTaskToDelayedList_138  ;; 4 cycles
        ; ------------------------------------- Block: 49 cycles
// 2051 					{
// 2052 						xYieldPending = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+18, AX  ;; 2 cycles
        ; ------------------------------------- Block: 4 cycles
// 2053 					}
// 2054 					else
// 2055 					{
// 2056 						mtCOVERAGE_TEST_MARKER();
// 2057 					}
// 2058 				}
??prvAddCurrentTaskToDelayedList_138:
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:_xPendingReadyList  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??xTaskResumeAll_0  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 2059 
// 2060 				if( pxTCB != NULL )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_140  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_140:
        SKZ                          ;; 1 cycle
          CFI FunCall _prvResetNextTaskUnblockTime
        ; ------------------------------------- Block: 1 cycles
// 2061 				{
// 2062 					/* A task was unblocked while the scheduler was suspended,
// 2063 					which may have prevented the next unblock time from being
// 2064 					re-calculated, in which case re-calculate it now.  Mainly
// 2065 					important for low power tickless implementations, where
// 2066 					this can prevent an unnecessary exit from low power
// 2067 					state. */
// 2068 					prvResetNextTaskUnblockTime();
        CALL      F:_prvResetNextTaskUnblockTime  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2069 				}
// 2070 
// 2071 				/* If any ticks occurred while the scheduler was suspended then
// 2072 				they should be processed now.  This ensures the tick count does
// 2073 				not	slip, and that any delayed tasks are resumed at the correct
// 2074 				time. */
// 2075 				{
// 2076 					UBaseType_t uxPendedCounts = uxPendedTicks; /* Non-volatile copy. */
??xTaskResumeAll_1:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+16  ;; 2 cycles
// 2077 
// 2078 					if( uxPendedCounts > ( UBaseType_t ) 0U )
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_141  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      [SP], AX           ;; 1 cycle
          CFI FunCall _xTaskIncrementTick
        ; ------------------------------------- Block: 1 cycles
// 2079 					{
// 2080 						do
// 2081 						{
// 2082 							if( xTaskIncrementTick() != pdFALSE )
??xTaskResumeAll_2:
        CALL      F:_xTaskIncrementTick  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_142  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 2083 							{
// 2084 								xYieldPending = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+18, AX  ;; 2 cycles
        ; ------------------------------------- Block: 4 cycles
// 2085 							}
// 2086 							else
// 2087 							{
// 2088 								mtCOVERAGE_TEST_MARKER();
// 2089 							}
// 2090 							--uxPendedCounts;
??prvAddCurrentTaskToDelayedList_142:
        MOVW      AX, [SP]           ;; 1 cycle
        DECW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 2091 						} while( uxPendedCounts > ( UBaseType_t ) 0U );
        OR        A, X               ;; 1 cycle
        BNZ       ??xTaskResumeAll_2  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 2092 
// 2093 						uxPendedTicks = 0;
        CLRW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+16, AX  ;; 2 cycles
        ; ------------------------------------- Block: 4 cycles
// 2094 					}
// 2095 					else
// 2096 					{
// 2097 						mtCOVERAGE_TEST_MARKER();
// 2098 					}
// 2099 				}
// 2100 
// 2101 				if( xYieldPending != pdFALSE )
??prvAddCurrentTaskToDelayedList_141:
        MOVW      AX, ES:_pxCurrentTCB+18  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_137  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2102 				{
// 2103 					#if( configUSE_PREEMPTION != 0 )
// 2104 					{
// 2105 						xAlreadyYielded = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2106 					}
// 2107 					#endif
// 2108 					taskYIELD_IF_USING_PREEMPTION();
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
// 2109 				}
// 2110 				else
// 2111 				{
// 2112 					mtCOVERAGE_TEST_MARKER();
// 2113 				}
// 2114 			}
// 2115 		}
// 2116 		else
// 2117 		{
// 2118 			mtCOVERAGE_TEST_MARKER();
// 2119 		}
// 2120 	}
// 2121 	taskEXIT_CRITICAL();
??prvAddCurrentTaskToDelayedList_137:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_143  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_143  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2122 
// 2123 	return xAlreadyYielded;
??prvAddCurrentTaskToDelayedList_143:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock18
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 278 cycles
// 2124 }
// 2125 /*-----------------------------------------------------------*/
// 2126 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock19 Using cfiCommon1
          CFI Function _xTaskGetTickCount
          CFI NoCalls
        CODE
// 2127 TickType_t xTaskGetTickCount( void )
// 2128 {
_xTaskGetTickCount:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 2129 TickType_t xTicks;
// 2130 
// 2131 	/* Critical section required if running on a 16 bit processor. */
// 2132 	portTICK_TYPE_ENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_144  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_144:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 2133 	{
// 2134 		xTicks = xTickCount;
        MOVW      HL, #LWRD(_pxCurrentTCB+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
// 2135 	}
// 2136 	portTICK_TYPE_EXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_145  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_145  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2137 
// 2138 	return xTicks;
??prvAddCurrentTaskToDelayedList_145:
        MOVW      AX, DE             ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock19
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 56 cycles
// 2139 }
// 2140 /*-----------------------------------------------------------*/
// 2141 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock20 Using cfiCommon1
          CFI Function _xTaskGetTickCountFromISR
          CFI NoCalls
        CODE
// 2142 TickType_t xTaskGetTickCountFromISR( void )
// 2143 {
_xTaskGetTickCountFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 2144 TickType_t xReturn;
// 2145 UBaseType_t uxSavedInterruptStatus;
// 2146 
// 2147 	/* RTOS ports that support interrupt nesting have the concept of a maximum
// 2148 	system call (or maximum API call) interrupt priority.  Interrupts that are
// 2149 	above the maximum system call priority are kept permanently enabled, even
// 2150 	when the RTOS kernel is in a critical section, but cannot make any calls to
// 2151 	FreeRTOS API functions.  If configASSERT() is defined in FreeRTOSConfig.h
// 2152 	then portASSERT_IF_INTERRUPT_PRIORITY_INVALID() will result in an assertion
// 2153 	failure if a FreeRTOS API function is called from an interrupt that has been
// 2154 	assigned a priority above the configured maximum system call priority.
// 2155 	Only FreeRTOS functions that end in FromISR can be called from interrupts
// 2156 	that have been assigned a priority at or (logically) below the maximum
// 2157 	system call	interrupt priority.  FreeRTOS maintains a separate interrupt
// 2158 	safe API to ensure interrupt entry is as fast and as simple as possible.
// 2159 	More information (albeit Cortex-M specific) is provided on the following
// 2160 	link: http://www.freertos.org/RTOS-Cortex-M3-M4.html */
// 2161 	portASSERT_IF_INTERRUPT_PRIORITY_INVALID();
// 2162 
// 2163 	uxSavedInterruptStatus = portTICK_TYPE_SET_INTERRUPT_MASK_FROM_ISR();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 2164 	{
// 2165 		xReturn = xTickCount;
        MOVW      HL, #LWRD(_pxCurrentTCB+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 2166 	}
// 2167 	portTICK_TYPE_CLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
        MOV       A, B               ;; 1 cycle
        ROL       A, 0x1             ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 2168 
// 2169 	return xReturn;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock20
        ; ------------------------------------- Block: 37 cycles
        ; ------------------------------------- Total: 37 cycles
// 2170 }
// 2171 /*-----------------------------------------------------------*/
// 2172 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock21 Using cfiCommon3
          CFI Function _uxTaskGetNumberOfTasks
          CFI NoCalls
        CODE
// 2173 UBaseType_t uxTaskGetNumberOfTasks( void )
// 2174 {
_uxTaskGetNumberOfTasks:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 2175 	/* A critical section is not required because the variables are of type
// 2176 	BaseType_t. */
// 2177 	return uxCurrentNumberOfTasks;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+6  ;; 2 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock21
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
// 2178 }
// 2179 /*-----------------------------------------------------------*/
// 2180 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock22 Using cfiCommon0
          CFI Function _pcTaskGetName
          CFI NoCalls
        CODE
// 2181 char *pcTaskGetName( TaskHandle_t xTaskToQuery ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
// 2182 {
_pcTaskGetName:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 2183 TCB_t *pxTCB;
// 2184 
// 2185 	/* If null is passed in here then the name of the calling task is being
// 2186 	queried. */
// 2187 	pxTCB = prvGetTCBFromHandle( xTaskToQuery );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_146  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_146:
        BNZ       ??prvAddCurrentTaskToDelayedList_147  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_148  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_147:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_148:
        MOVW      [SP], AX           ;; 1 cycle
// 2188 	configASSERT( pxTCB );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_149  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_149:
        BNZ       ??prvAddCurrentTaskToDelayedList_150  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_151  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_151:
        BR        S:??prvAddCurrentTaskToDelayedList_151  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2189 	return &( pxTCB->pcTaskName[ 0 ] );
??prvAddCurrentTaskToDelayedList_150:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock22
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 84 cycles
// 2190 }
// 2191 /*-----------------------------------------------------------*/
// 2192 
// 2193 #if ( INCLUDE_xTaskGetHandle == 1 )
// 2194 
// 2195 	static TCB_t *prvSearchForNameWithinSingleList( List_t *pxList, const char pcNameToQuery[] )
// 2196 	{
// 2197 	TCB_t *pxNextTCB, *pxFirstTCB, *pxReturn = NULL;
// 2198 	UBaseType_t x;
// 2199 	char cNextChar;
// 2200 
// 2201 		/* This function is called with the scheduler suspended. */
// 2202 
// 2203 		if( listCURRENT_LIST_LENGTH( pxList ) > ( UBaseType_t ) 0 )
// 2204 		{
// 2205 			listGET_OWNER_OF_NEXT_ENTRY( pxFirstTCB, pxList );
// 2206 
// 2207 			do
// 2208 			{
// 2209 				listGET_OWNER_OF_NEXT_ENTRY( pxNextTCB, pxList );
// 2210 
// 2211 				/* Check each character in the name looking for a match or
// 2212 				mismatch. */
// 2213 				for( x = ( UBaseType_t ) 0; x < ( UBaseType_t ) configMAX_TASK_NAME_LEN; x++ )
// 2214 				{
// 2215 					cNextChar = pxNextTCB->pcTaskName[ x ];
// 2216 
// 2217 					if( cNextChar != pcNameToQuery[ x ] )
// 2218 					{
// 2219 						/* Characters didn't match. */
// 2220 						break;
// 2221 					}
// 2222 					else if( cNextChar == 0x00 )
// 2223 					{
// 2224 						/* Both strings terminated, a match must have been
// 2225 						found. */
// 2226 						pxReturn = pxNextTCB;
// 2227 						break;
// 2228 					}
// 2229 					else
// 2230 					{
// 2231 						mtCOVERAGE_TEST_MARKER();
// 2232 					}
// 2233 				}
// 2234 
// 2235 				if( pxReturn != NULL )
// 2236 				{
// 2237 					/* The handle has been found. */
// 2238 					break;
// 2239 				}
// 2240 
// 2241 			} while( pxNextTCB != pxFirstTCB );
// 2242 		}
// 2243 		else
// 2244 		{
// 2245 			mtCOVERAGE_TEST_MARKER();
// 2246 		}
// 2247 
// 2248 		return pxReturn;
// 2249 	}
// 2250 
// 2251 #endif /* INCLUDE_xTaskGetHandle */
// 2252 /*-----------------------------------------------------------*/
// 2253 
// 2254 #if ( INCLUDE_xTaskGetHandle == 1 )
// 2255 
// 2256 	TaskHandle_t xTaskGetHandle( const char *pcNameToQuery ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
// 2257 	{
// 2258 	UBaseType_t uxQueue = configMAX_PRIORITIES;
// 2259 	TCB_t* pxTCB;
// 2260 
// 2261 		/* Task names will be truncated to configMAX_TASK_NAME_LEN - 1 bytes. */
// 2262 		configASSERT( strlen( pcNameToQuery ) < configMAX_TASK_NAME_LEN );
// 2263 
// 2264 		vTaskSuspendAll();
// 2265 		{
// 2266 			/* Search the ready lists. */
// 2267 			do
// 2268 			{
// 2269 				uxQueue--;
// 2270 				pxTCB = prvSearchForNameWithinSingleList( ( List_t * ) &( pxReadyTasksLists[ uxQueue ] ), pcNameToQuery );
// 2271 
// 2272 				if( pxTCB != NULL )
// 2273 				{
// 2274 					/* Found the handle. */
// 2275 					break;
// 2276 				}
// 2277 
// 2278 			} while( uxQueue > ( UBaseType_t ) tskIDLE_PRIORITY ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
// 2279 
// 2280 			/* Search the delayed lists. */
// 2281 			if( pxTCB == NULL )
// 2282 			{
// 2283 				pxTCB = prvSearchForNameWithinSingleList( ( List_t * ) pxDelayedTaskList, pcNameToQuery );
// 2284 			}
// 2285 
// 2286 			if( pxTCB == NULL )
// 2287 			{
// 2288 				pxTCB = prvSearchForNameWithinSingleList( ( List_t * ) pxOverflowDelayedTaskList, pcNameToQuery );
// 2289 			}
// 2290 
// 2291 			#if ( INCLUDE_vTaskSuspend == 1 )
// 2292 			{
// 2293 				if( pxTCB == NULL )
// 2294 				{
// 2295 					/* Search the suspended list. */
// 2296 					pxTCB = prvSearchForNameWithinSingleList( &xSuspendedTaskList, pcNameToQuery );
// 2297 				}
// 2298 			}
// 2299 			#endif
// 2300 
// 2301 			#if( INCLUDE_vTaskDelete == 1 )
// 2302 			{
// 2303 				if( pxTCB == NULL )
// 2304 				{
// 2305 					/* Search the deleted list. */
// 2306 					pxTCB = prvSearchForNameWithinSingleList( &xTasksWaitingTermination, pcNameToQuery );
// 2307 				}
// 2308 			}
// 2309 			#endif
// 2310 		}
// 2311 		( void ) xTaskResumeAll();
// 2312 
// 2313 		return ( TaskHandle_t ) pxTCB;
// 2314 	}
// 2315 
// 2316 #endif /* INCLUDE_xTaskGetHandle */
// 2317 /*-----------------------------------------------------------*/
// 2318 
// 2319 #if ( configUSE_TRACE_FACILITY == 1 )
// 2320 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock23 Using cfiCommon2
          CFI Function _uxTaskGetSystemState
        CODE
// 2321 	UBaseType_t uxTaskGetSystemState( TaskStatus_t * const pxTaskStatusArray, const UBaseType_t uxArraySize, uint32_t * const pulTotalRunTime )
// 2322 	{
_uxTaskGetSystemState:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 36
        SUBW      SP, #0x20          ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, BC             ;; 1 cycle
// 2323 	UBaseType_t uxTask = 0, uxQueue = configMAX_PRIORITIES;
        MOVW      HL, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      HL, #0x7           ;; 1 cycle
// 2324 
// 2325 		vTaskSuspendAll();
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+32  ;; 3 cycles
// 2326 		{
// 2327 			/* Is there a space in the array for each task in the system? */
// 2328 			if( uxArraySize >= uxCurrentNumberOfTasks )
        MOVW      DE, ES:_pxCurrentTCB+6  ;; 2 cycles
        CMPW      AX, DE             ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_152  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2329 			{
// 2330 				/* Fill in an TaskStatus_t structure with information on each
// 2331 				task in the Ready state. */
// 2332 				do
// 2333 				{
// 2334 					uxQueue--;
??uxTaskGetSystemState_0:
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        DECW      AX                 ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
// 2335 					uxTask += prvListTasksWithinSingleList( &( pxTaskStatusArray[ uxTask ] ), &( pxReadyTasksLists[ uxQueue ] ), eReady );
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      BC, #0x1A          ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x1A], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_153  ;; 4 cycles
        ; ------------------------------------- Block: 39 cycles
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+44
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_154  ;; 4 cycles
        ; ------------------------------------- Block: 53 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_154:
        BNZ       ??prvAddCurrentTaskToDelayedList_155  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, E               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 32 cycles
??prvAddCurrentTaskToDelayedList_155:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       [SP+0x16], A       ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      [SP+0x14], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x12], AX      ;; 1 cycle
        ; ------------------------------------- Block: 19 cycles
??uxTaskGetSystemState_1:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+44
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_156  ;; 4 cycles
        ; ------------------------------------- Block: 56 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_156:
        BNZ       ??prvAddCurrentTaskToDelayedList_157  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, E               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 32 cycles
??prvAddCurrentTaskToDelayedList_157:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       [SP+0x1E], A       ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      [SP+0x1C], AX      ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       A, [SP+0x1E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_158  ;; 4 cycles
        ; ------------------------------------- Block: 38 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_158:
        BNZ       ??prvAddCurrentTaskToDelayedList_159  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_160  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_159:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_160:
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x3E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_161  ;; 4 cycles
        ; ------------------------------------- Block: 113 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+32  ;; 3 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_162  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_162:
        BZ        ??prvAddCurrentTaskToDelayedList_163  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0x2            ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 8 cycles
??prvAddCurrentTaskToDelayedList_163:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_161:
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvTaskCheckFreeStackSpace
        CALL      F:_prvTaskCheckFreeStackSpace  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      [SP+0x12], AX      ;; 1 cycle
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_164  ;; 4 cycles
        ; ------------------------------------- Block: 82 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_164:
        SKZ                          ;; 4 cycles
        BR        R:??uxTaskGetSystemState_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2336 
// 2337 				} while( uxQueue > ( UBaseType_t ) tskIDLE_PRIORITY ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
??prvAddCurrentTaskToDelayedList_153:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??uxTaskGetSystemState_0  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 2338 
// 2339 				/* Fill in an TaskStatus_t structure with information on each
// 2340 				task in the Blocked state. */
// 2341 				uxTask += prvListTasksWithinSingleList( &( pxTaskStatusArray[ uxTask ] ), ( List_t * ) pxDelayedTaskList, eBlocked );
        MOV       X, #0x2            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      HL, #LWRD(_xPendingReadyList+36)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, #0x1A          ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [SP+0x28]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+44
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
          CFI FunCall _prvListTasksWithinSingleList
        CALL      F:_prvListTasksWithinSingleList  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 2342 				uxTask += prvListTasksWithinSingleList( &( pxTaskStatusArray[ uxTask ] ), ( List_t * ) pxOverflowDelayedTaskList, eBlocked );
        MOV       X, #0x2            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      HL, #LWRD(_xPendingReadyList+40)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, #0x1A          ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [SP+0x2A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x28]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+46
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+44
          CFI FunCall _prvListTasksWithinSingleList
        CALL      F:_prvListTasksWithinSingleList  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2343 
// 2344 				#if( INCLUDE_vTaskDelete == 1 )
// 2345 				{
// 2346 					/* Fill in an TaskStatus_t structure with information on
// 2347 					each task that has been deleted but not yet cleaned up. */
// 2348 					uxTask += prvListTasksWithinSingleList( &( pxTaskStatusArray[ uxTask ] ), &xTasksWaitingTermination, eDeleted );
        MOV       X, #0x4            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      BC, #LWRD(_xPendingReadyList+18)  ;; 1 cycle
        MOV       X, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, #0x1A          ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [SP+0x2C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2A]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+48
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+46
          CFI FunCall _prvListTasksWithinSingleList
        CALL      F:_prvListTasksWithinSingleList  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 2349 				}
// 2350 				#endif
// 2351 
// 2352 				#if ( INCLUDE_vTaskSuspend == 1 )
// 2353 				{
// 2354 					/* Fill in an TaskStatus_t structure with information on
// 2355 					each task in the Suspended state. */
// 2356 					uxTask += prvListTasksWithinSingleList( &( pxTaskStatusArray[ uxTask ] ), &xSuspendedTaskList, eSuspended );
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      BC, #LWRD(_xPendingReadyList+44)  ;; 1 cycle
        MOV       X, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, #0x1A          ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [SP+0x2E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2C]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+50
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+48
          CFI FunCall _prvListTasksWithinSingleList
        CALL      F:_prvListTasksWithinSingleList  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2357 				}
// 2358 				#endif
// 2359 
// 2360 				#if ( configGENERATE_RUN_TIME_STATS == 1)
// 2361 				{
// 2362 					if( pulTotalRunTime != NULL )
// 2363 					{
// 2364 						#ifdef portALT_GET_RUN_TIME_COUNTER_VALUE
// 2365 							portALT_GET_RUN_TIME_COUNTER_VALUE( ( *pulTotalRunTime ) );
// 2366 						#else
// 2367 							*pulTotalRunTime = portGET_RUN_TIME_COUNTER_VALUE();
// 2368 						#endif
// 2369 					}
// 2370 				}
// 2371 				#else
// 2372 				{
// 2373 					if( pulTotalRunTime != NULL )
        MOV       A, [SP+0x32]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x30]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_165  ;; 4 cycles
        ; ------------------------------------- Block: 145 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_165:
        BZ        ??prvAddCurrentTaskToDelayedList_152  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2374 					{
// 2375 						*pulTotalRunTime = 0;
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 5 cycles
// 2376 					}
// 2377 				}
// 2378 				#endif
// 2379 			}
// 2380 			else
// 2381 			{
// 2382 				mtCOVERAGE_TEST_MARKER();
// 2383 			}
// 2384 		}
// 2385 		( void ) xTaskResumeAll();
??prvAddCurrentTaskToDelayedList_152:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
// 2386 
// 2387 		return uxTask;
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0x24          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock23
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 742 cycles
// 2388 	}
// 2389 
// 2390 #endif /* configUSE_TRACE_FACILITY */
// 2391 /*----------------------------------------------------------*/
// 2392 
// 2393 #if ( INCLUDE_xTaskGetIdleTaskHandle == 1 )
// 2394 
// 2395 	TaskHandle_t xTaskGetIdleTaskHandle( void )
// 2396 	{
// 2397 		/* If xTaskGetIdleTaskHandle() is called before the scheduler has been
// 2398 		started, then xIdleTaskHandle will be NULL. */
// 2399 		configASSERT( ( xIdleTaskHandle != NULL ) );
// 2400 		return xIdleTaskHandle;
// 2401 	}
// 2402 
// 2403 #endif /* INCLUDE_xTaskGetIdleTaskHandle */
// 2404 /*----------------------------------------------------------*/
// 2405 
// 2406 /* This conditional compilation should use inequality to 0, not equality to 1.
// 2407 This is to ensure vTaskStepTick() is available when user defined low power mode
// 2408 implementations require configUSE_TICKLESS_IDLE to be set to a value other than
// 2409 1. */
// 2410 #if ( configUSE_TICKLESS_IDLE != 0 )
// 2411 
// 2412 	void vTaskStepTick( const TickType_t xTicksToJump )
// 2413 	{
// 2414 		/* Correct the tick count value after a period during which the tick
// 2415 		was suppressed.  Note this does *not* call the tick hook function for
// 2416 		each stepped tick. */
// 2417 		configASSERT( ( xTickCount + xTicksToJump ) <= xNextTaskUnblockTime );
// 2418 		xTickCount += xTicksToJump;
// 2419 		traceINCREASE_TICK_COUNT( xTicksToJump );
// 2420 	}
// 2421 
// 2422 #endif /* configUSE_TICKLESS_IDLE */
// 2423 /*----------------------------------------------------------*/
// 2424 
// 2425 #if ( INCLUDE_xTaskAbortDelay == 1 )
// 2426 
// 2427 	BaseType_t xTaskAbortDelay( TaskHandle_t xTask )
// 2428 	{
// 2429 	TCB_t *pxTCB = ( TCB_t * ) xTask;
// 2430 	BaseType_t xReturn = pdFALSE;
// 2431 
// 2432 		configASSERT( pxTCB );
// 2433 
// 2434 		vTaskSuspendAll();
// 2435 		{
// 2436 			/* A task can only be prematurely removed from the Blocked state if
// 2437 			it is actually in the Blocked state. */
// 2438 			if( eTaskGetState( xTask ) == eBlocked )
// 2439 			{
// 2440 				/* Remove the reference to the task from the blocked list.  An
// 2441 				interrupt won't touch the xStateListItem because the
// 2442 				scheduler is suspended. */
// 2443 				( void ) uxListRemove( &( pxTCB->xStateListItem ) );
// 2444 
// 2445 				/* Is the task waiting on an event also?  If so remove it from
// 2446 				the event list too.  Interrupts can touch the event list item,
// 2447 				even though the scheduler is suspended, so a critical section
// 2448 				is used. */
// 2449 				taskENTER_CRITICAL();
// 2450 				{
// 2451 					if( listLIST_ITEM_CONTAINER( &( pxTCB->xEventListItem ) ) != NULL )
// 2452 					{
// 2453 						( void ) uxListRemove( &( pxTCB->xEventListItem ) );
// 2454 						pxTCB->ucDelayAborted = pdTRUE;
// 2455 					}
// 2456 					else
// 2457 					{
// 2458 						mtCOVERAGE_TEST_MARKER();
// 2459 					}
// 2460 				}
// 2461 				taskEXIT_CRITICAL();
// 2462 
// 2463 				/* Place the unblocked task into the appropriate ready list. */
// 2464 				prvAddTaskToReadyList( pxTCB );
// 2465 
// 2466 				/* A task being unblocked cannot cause an immediate context
// 2467 				switch if preemption is turned off. */
// 2468 				#if (  configUSE_PREEMPTION == 1 )
// 2469 				{
// 2470 					/* Preemption is on, but a context switch should only be
// 2471 					performed if the unblocked task has a priority that is
// 2472 					equal to or higher than the currently executing task. */
// 2473 					if( pxTCB->uxPriority > pxCurrentTCB->uxPriority )
// 2474 					{
// 2475 						/* Pend the yield to be performed when the scheduler
// 2476 						is unsuspended. */
// 2477 						xYieldPending = pdTRUE;
// 2478 					}
// 2479 					else
// 2480 					{
// 2481 						mtCOVERAGE_TEST_MARKER();
// 2482 					}
// 2483 				}
// 2484 				#endif /* configUSE_PREEMPTION */
// 2485 			}
// 2486 			else
// 2487 			{
// 2488 				mtCOVERAGE_TEST_MARKER();
// 2489 			}
// 2490 		}
// 2491 		xTaskResumeAll();
// 2492 
// 2493 		return xReturn;
// 2494 	}
// 2495 
// 2496 #endif /* INCLUDE_xTaskAbortDelay */
// 2497 /*----------------------------------------------------------*/
// 2498 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock24 Using cfiCommon3
          CFI Function _xTaskIncrementTick
        CODE
// 2499 BaseType_t xTaskIncrementTick( void )
// 2500 {
_xTaskIncrementTick:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 10
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+14
// 2501 TCB_t * pxTCB;
// 2502 TickType_t xItemValue;
// 2503 BaseType_t xSwitchRequired = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 2504 
// 2505 	/* Called by the portable layer each time a tick interrupt occurs.
// 2506 	Increments the tick then checks to see if the new tick value will cause any
// 2507 	tasks to be unblocked. */
// 2508 	traceTASK_INCREMENT_TICK( xTickCount );
// 2509 	if( uxSchedulerSuspended == ( UBaseType_t ) pdFALSE )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_166  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 2510 	{
// 2511 		/* Minor optimisation.  The tick count cannot change in this
// 2512 		block. */
// 2513 		const TickType_t xConstTickCount = xTickCount + 1;
        MOVW      HL, #LWRD(_pxCurrentTCB+8)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2514 
// 2515 		/* Increment the RTOS tick, switching the delayed and overflowed
// 2516 		delayed lists if it wraps to 0. */
// 2517 		xTickCount = xConstTickCount;
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2518 
// 2519 		if( xConstTickCount == ( TickType_t ) 0U )
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_167  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
// 2520 		{
// 2521 			taskSWITCH_DELAYED_LISTS();
        MOVW      HL, #LWRD(_xPendingReadyList+36)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_168  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_169  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_169:
        BR        S:??prvAddCurrentTaskToDelayedList_169  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_168:
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      HL, #LWRD(_xPendingReadyList+40)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_xPendingReadyList+36)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_xPendingReadyList+40)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+20  ;; 3 cycles
          CFI FunCall _prvResetNextTaskUnblockTime
        CALL      F:_prvResetNextTaskUnblockTime  ;; 3 cycles
        ; ------------------------------------- Block: 43 cycles
// 2522 		}
// 2523 		else
// 2524 		{
// 2525 			mtCOVERAGE_TEST_MARKER();
// 2526 		}
// 2527 
// 2528 		/* See if this tick has made a timeout expire.  Tasks are stored in
// 2529 		the	queue in the order of their wake time - meaning once one task
// 2530 		has been found whose block time has not expired there is no need to
// 2531 		look any further down the list. */
// 2532 		if( xConstTickCount >= xNextTaskUnblockTime )
??prvAddCurrentTaskToDelayedList_167:
        MOVW      HL, #LWRD(_pxCurrentTCB+24)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        SKNC                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_170  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_171  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2533 		{
// 2534 			for( ;; )
// 2535 			{
// 2536 				if( listLIST_IS_EMPTY( pxDelayedTaskList ) != pdFALSE )
// 2537 				{
// 2538 					/* The delayed list is empty.  Set xNextTaskUnblockTime
// 2539 					to the maximum possible value so it is extremely
// 2540 					unlikely that the
// 2541 					if( xTickCount >= xNextTaskUnblockTime ) test will pass
// 2542 					next time through. */
// 2543 					xNextTaskUnblockTime = portMAX_DELAY; /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
// 2544 					break;
// 2545 				}
// 2546 				else
// 2547 				{
// 2548 					/* The delayed list is not empty, get the value of the
// 2549 					item at the head of the delayed list.  This is the time
// 2550 					at which the task at the head of the delayed list must
// 2551 					be removed from the Blocked state. */
// 2552 					pxTCB = ( TCB_t * ) listGET_OWNER_OF_HEAD_ENTRY( pxDelayedTaskList );
// 2553 					xItemValue = listGET_LIST_ITEM_VALUE( &( pxTCB->xStateListItem ) );
// 2554 
// 2555 					if( xConstTickCount < xItemValue )
// 2556 					{
// 2557 						/* It is not time to unblock this item yet, but the
// 2558 						item value is the time at which the task at the head
// 2559 						of the blocked list must be removed from the Blocked
// 2560 						state -	so record the item value in
// 2561 						xNextTaskUnblockTime. */
// 2562 						xNextTaskUnblockTime = xItemValue;
// 2563 						break;
// 2564 					}
// 2565 					else
// 2566 					{
// 2567 						mtCOVERAGE_TEST_MARKER();
// 2568 					}
// 2569 
// 2570 					/* It is time to remove the item from the Blocked state. */
// 2571 					( void ) uxListRemove( &( pxTCB->xStateListItem ) );
??xTaskIncrementTick_0:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 2572 
// 2573 					/* Is the task waiting on an event also?  If so remove
// 2574 					it from the event list. */
// 2575 					if( listLIST_ITEM_CONTAINER( &( pxTCB->xEventListItem ) ) != NULL )
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_172  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_172:
        BZ        ??prvAddCurrentTaskToDelayedList_173  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2576 					{
// 2577 						( void ) uxListRemove( &( pxTCB->xEventListItem ) );
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 2578 					}
// 2579 					else
// 2580 					{
// 2581 						mtCOVERAGE_TEST_MARKER();
// 2582 					}
// 2583 
// 2584 					/* Place the unblocked task into the appropriate ready
// 2585 					list. */
// 2586 					prvAddTaskToReadyList( pxTCB );
??prvAddCurrentTaskToDelayedList_173:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_174  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_174:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       E, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        MOV       A, E               ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 2587 
// 2588 					/* A task being unblocked cannot cause an immediate
// 2589 					context switch if preemption is turned off. */
// 2590 					#if (  configUSE_PREEMPTION == 1 )
// 2591 					{
// 2592 						/* Preemption is on, but a context switch should
// 2593 						only be performed if the unblocked task has a
// 2594 						priority that is equal to or higher than the
// 2595 						currently executing task. */
// 2596 						if( pxTCB->uxPriority >= pxCurrentTCB->uxPriority )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BC        ??prvAddCurrentTaskToDelayedList_171  ;; 4 cycles
        ; ------------------------------------- Block: 49 cycles
// 2597 						{
// 2598 							xSwitchRequired = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2599 						}
// 2600 						else
// 2601 						{
// 2602 							mtCOVERAGE_TEST_MARKER();
// 2603 						}
// 2604 					}
??prvAddCurrentTaskToDelayedList_171:
        MOVW      HL, #LWRD(_xPendingReadyList+36)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_175  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x0A]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES:[DE+0x0C]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        SKC                          ;; 4 cycles
        BR        R:??xTaskIncrementTick_0  ;; 4 cycles
        ; ------------------------------------- Block: 46 cycles
        MOVW      AX, DE             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, #LWRD(_pxCurrentTCB+24)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
??xTaskIncrementTick_1:
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 2605 					#endif /* configUSE_PREEMPTION */
// 2606 				}
// 2607 			}
// 2608 		}
// 2609 
// 2610 		/* Tasks of equal priority to the currently running task will share
// 2611 		processing time (time slice) if preemption is on, and the application
// 2612 		writer has not explicitly turned time slicing off. */
// 2613 		#if ( ( configUSE_PREEMPTION == 1 ) && ( configUSE_TIME_SLICING == 1 ) )
// 2614 		{
// 2615 			if( listCURRENT_LIST_LENGTH( &( pxReadyTasksLists[ pxCurrentTCB->uxPriority ] ) ) > ( UBaseType_t ) 1 )
??prvAddCurrentTaskToDelayedList_170:
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x2           ;; 1 cycle
        BC        ??prvAddCurrentTaskToDelayedList_176  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 2616 			{
// 2617 				xSwitchRequired = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2618 			}
// 2619 			else
// 2620 			{
// 2621 				mtCOVERAGE_TEST_MARKER();
// 2622 			}
// 2623 		}
// 2624 		#endif /* ( ( configUSE_PREEMPTION == 1 ) && ( configUSE_TIME_SLICING == 1 ) ) */
// 2625 
// 2626 		#if ( configUSE_TICK_HOOK == 1 )
// 2627 		{
// 2628 			/* Guard against the tick hook being called when the pended tick
// 2629 			count is being unwound (when the scheduler is being unlocked). */
// 2630 			if( uxPendedTicks == ( UBaseType_t ) 0U )
??prvAddCurrentTaskToDelayedList_176:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+16  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_177  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 2631 			{
// 2632 				vApplicationTickHook();
        BR        S:??prvAddCurrentTaskToDelayedList_178  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2633 			}
// 2634 			else
// 2635 			{
// 2636 				mtCOVERAGE_TEST_MARKER();
// 2637 			}
// 2638 		}
??prvAddCurrentTaskToDelayedList_175:
        MOVW      HL, #LWRD(_pxCurrentTCB+24)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        BR        S:??xTaskIncrementTick_1  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 2639 		#endif /* configUSE_TICK_HOOK */
// 2640 	}
// 2641 	else
// 2642 	{
// 2643 		++uxPendedTicks;
??prvAddCurrentTaskToDelayedList_166:
        INCW      ES:_pxCurrentTCB+16  ;; 3 cycles
// 2644 
// 2645 		/* The tick hook gets called at regular intervals, even if the
// 2646 		scheduler is locked. */
// 2647 		#if ( configUSE_TICK_HOOK == 1 )
// 2648 		{
// 2649 			vApplicationTickHook();
          CFI FunCall _vApplicationTickHook
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_178:
        CALL      F:_vApplicationTickHook  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2650 		}
// 2651 		#endif
// 2652 	}
// 2653 
// 2654 	#if ( configUSE_PREEMPTION == 1 )
// 2655 	{
// 2656 		if( xYieldPending != pdFALSE )
??prvAddCurrentTaskToDelayedList_177:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+18  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_179  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 2657 		{
// 2658 			xSwitchRequired = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
          CFI EndBlock cfiBlock24
        ; ------------------------------------- Block: 2 cycles
// 2659 		}
// 2660 		else
// 2661 		{
// 2662 			mtCOVERAGE_TEST_MARKER();
// 2663 		}
// 2664 	}
// 2665 	#endif /* configUSE_PREEMPTION */
// 2666 
// 2667 	return xSwitchRequired;
??prvAddCurrentTaskToDelayedList_179:
        REQUIRE ?Subroutine2
        ; // Fall through to label ?Subroutine2
// 2668 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock25 Using cfiCommon3
          CFI NoFunction
          CFI CFA SP+14
        CODE
?Subroutine2:
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock25
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
// 2669 /*-----------------------------------------------------------*/
// 2670 
// 2671 #if ( configUSE_APPLICATION_TASK_TAG == 1 )
// 2672 
// 2673 	void vTaskSetApplicationTaskTag( TaskHandle_t xTask, TaskHookFunction_t pxHookFunction )
// 2674 	{
// 2675 	TCB_t *xTCB;
// 2676 
// 2677 		/* If xTask is NULL then it is the task hook of the calling task that is
// 2678 		getting set. */
// 2679 		if( xTask == NULL )
// 2680 		{
// 2681 			xTCB = ( TCB_t * ) pxCurrentTCB;
// 2682 		}
// 2683 		else
// 2684 		{
// 2685 			xTCB = ( TCB_t * ) xTask;
// 2686 		}
// 2687 
// 2688 		/* Save the hook function in the TCB.  A critical section is required as
// 2689 		the value can be accessed from an interrupt. */
// 2690 		taskENTER_CRITICAL();
// 2691 			xTCB->pxTaskTag = pxHookFunction;
// 2692 		taskEXIT_CRITICAL();
// 2693 	}
// 2694 
// 2695 #endif /* configUSE_APPLICATION_TASK_TAG */
// 2696 /*-----------------------------------------------------------*/
// 2697 
// 2698 #if ( configUSE_APPLICATION_TASK_TAG == 1 )
// 2699 
// 2700 	TaskHookFunction_t xTaskGetApplicationTaskTag( TaskHandle_t xTask )
// 2701 	{
// 2702 	TCB_t *xTCB;
// 2703 	TaskHookFunction_t xReturn;
// 2704 
// 2705 		/* If xTask is NULL then we are setting our own task hook. */
// 2706 		if( xTask == NULL )
// 2707 		{
// 2708 			xTCB = ( TCB_t * ) pxCurrentTCB;
// 2709 		}
// 2710 		else
// 2711 		{
// 2712 			xTCB = ( TCB_t * ) xTask;
// 2713 		}
// 2714 
// 2715 		/* Save the hook function in the TCB.  A critical section is required as
// 2716 		the value can be accessed from an interrupt. */
// 2717 		taskENTER_CRITICAL();
// 2718 		{
// 2719 			xReturn = xTCB->pxTaskTag;
// 2720 		}
// 2721 		taskEXIT_CRITICAL();
// 2722 
// 2723 		return xReturn;
// 2724 	}
// 2725 
// 2726 #endif /* configUSE_APPLICATION_TASK_TAG */
// 2727 /*-----------------------------------------------------------*/
// 2728 
// 2729 #if ( configUSE_APPLICATION_TASK_TAG == 1 )
// 2730 
// 2731 	BaseType_t xTaskCallApplicationTaskHook( TaskHandle_t xTask, void *pvParameter )
// 2732 	{
// 2733 	TCB_t *xTCB;
// 2734 	BaseType_t xReturn;
// 2735 
// 2736 		/* If xTask is NULL then we are calling our own task hook. */
// 2737 		if( xTask == NULL )
// 2738 		{
// 2739 			xTCB = ( TCB_t * ) pxCurrentTCB;
// 2740 		}
// 2741 		else
// 2742 		{
// 2743 			xTCB = ( TCB_t * ) xTask;
// 2744 		}
// 2745 
// 2746 		if( xTCB->pxTaskTag != NULL )
// 2747 		{
// 2748 			xReturn = xTCB->pxTaskTag( pvParameter );
// 2749 		}
// 2750 		else
// 2751 		{
// 2752 			xReturn = pdFAIL;
// 2753 		}
// 2754 
// 2755 		return xReturn;
// 2756 	}
// 2757 
// 2758 #endif /* configUSE_APPLICATION_TASK_TAG */
// 2759 /*-----------------------------------------------------------*/
// 2760 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock26 Using cfiCommon3
          CFI Function _vTaskSwitchContext
        CODE
// 2761 void vTaskSwitchContext( void )
// 2762 {
_vTaskSwitchContext:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 2763 	if( uxSchedulerSuspended != ( UBaseType_t ) pdFALSE )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_180  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 2764 	{
// 2765 		/* The scheduler is currently suspended - do not allow a context
// 2766 		switch. */
// 2767 		xYieldPending = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+18, AX  ;; 2 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_181  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2768 	}
// 2769 	else
// 2770 	{
// 2771 		xYieldPending = pdFALSE;
??prvAddCurrentTaskToDelayedList_180:
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+18, AX  ;; 2 cycles
// 2772 		traceTASK_SWITCHED_OUT();
// 2773 
// 2774 		#if ( configGENERATE_RUN_TIME_STATS == 1 )
// 2775 		{
// 2776 				#ifdef portALT_GET_RUN_TIME_COUNTER_VALUE
// 2777 					portALT_GET_RUN_TIME_COUNTER_VALUE( ulTotalRunTime );
// 2778 				#else
// 2779 					ulTotalRunTime = portGET_RUN_TIME_COUNTER_VALUE();
// 2780 				#endif
// 2781 
// 2782 				/* Add the amount of time the task has been running to the
// 2783 				accumulated time so far.  The time the task started running was
// 2784 				stored in ulTaskSwitchedInTime.  Note that there is no overflow
// 2785 				protection here so count values are only valid until the timer
// 2786 				overflows.  The guard against negative values is to protect
// 2787 				against suspect run time stat counter implementations - which
// 2788 				are provided by the application, not the kernel. */
// 2789 				if( ulTotalRunTime > ulTaskSwitchedInTime )
// 2790 				{
// 2791 					pxCurrentTCB->ulRunTimeCounter += ( ulTotalRunTime - ulTaskSwitchedInTime );
// 2792 				}
// 2793 				else
// 2794 				{
// 2795 					mtCOVERAGE_TEST_MARKER();
// 2796 				}
// 2797 				ulTaskSwitchedInTime = ulTotalRunTime;
// 2798 		}
// 2799 		#endif /* configGENERATE_RUN_TIME_STATS */
// 2800 
// 2801 		/* Check for stack overflow, if configured. */
// 2802 		taskCHECK_FOR_STACK_OVERFLOW();
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[HL+0x30]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL+0x2E]   ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0xA5A5        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 27 cycles
        CMPW      AX, #0xA5A5        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??vTaskSwitchContext_0:
        BNZ       ??prvAddCurrentTaskToDelayedList_182  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0xA5A5        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
        CMPW      AX, #0xA5A5        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??vTaskSwitchContext_1:
        BNZ       ??prvAddCurrentTaskToDelayedList_182  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0xA5A5        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
        CMPW      AX, #0xA5A5        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??vTaskSwitchContext_2:
        BNZ       ??prvAddCurrentTaskToDelayedList_182  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0xA5A5        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
        CMPW      AX, #0xA5A5        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??vTaskSwitchContext_3:
        BZ        ??prvAddCurrentTaskToDelayedList_183  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
??prvAddCurrentTaskToDelayedList_182:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vApplicationStackOverflowHook
        CALL      F:_vApplicationStackOverflowHook  ;; 3 cycles
        ; ------------------------------------- Block: 26 cycles
// 2803 
// 2804 		/* Select a new task to run using either the generic C or port
// 2805 		optimised asm code. */
// 2806 		taskSELECT_HIGHEST_PRIORITY_TASK();
??prvAddCurrentTaskToDelayedList_183:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB+12  ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_184  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
??vTaskSwitchContext_4:
        DECW      HL                 ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        ADDW      AX, #0xFFEE        ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_184:
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_185  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??vTaskSwitchContext_4  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_186  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_186:
        BR        S:??prvAddCurrentTaskToDelayedList_186  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_185:
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_187  ;; 4 cycles
        ; ------------------------------------- Block: 63 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_187:
        BNZ       ??prvAddCurrentTaskToDelayedList_188  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 32 cycles
??prvAddCurrentTaskToDelayedList_188:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_pxCurrentTCB)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 28 cycles
// 2807 		traceTASK_SWITCHED_IN();
// 2808 
// 2809 		#if ( configUSE_NEWLIB_REENTRANT == 1 )
// 2810 		{
// 2811 			/* Switch Newlib's _impure_ptr variable to point to the _reent
// 2812 			structure specific to this task. */
// 2813 			_impure_ptr = &( pxCurrentTCB->xNewLib_reent );
// 2814 		}
// 2815 		#endif /* configUSE_NEWLIB_REENTRANT */
// 2816 	}
// 2817 }
??prvAddCurrentTaskToDelayedList_181:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock26
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 312 cycles
// 2818 /*-----------------------------------------------------------*/
// 2819 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock27 Using cfiCommon0
          CFI Function _vTaskPlaceOnEventList
        CODE
// 2820 void vTaskPlaceOnEventList( List_t * const pxEventList, const TickType_t xTicksToWait )
// 2821 {
_vTaskPlaceOnEventList:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 2822 	configASSERT( pxEventList );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_189  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_189:
        BNZ       ??prvAddCurrentTaskToDelayedList_190  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_191  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_191:
        BR        S:??prvAddCurrentTaskToDelayedList_191  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2823 
// 2824 	/* THIS FUNCTION MUST BE CALLED WITH EITHER INTERRUPTS DISABLED OR THE
// 2825 	SCHEDULER SUSPENDED AND THE QUEUE BEING ACCESSED LOCKED. */
// 2826 
// 2827 	/* Place the event list item of the TCB in the appropriate event list.
// 2828 	This is placed in the list in priority order so the highest priority task
// 2829 	is the first to be woken by the event.  The queue that contains the event
// 2830 	list is locked, preventing simultaneous access from interrupts. */
// 2831 	vListInsert( pxEventList, &( pxCurrentTCB->xEventListItem ) );
??prvAddCurrentTaskToDelayedList_190:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _vListInsert
        CALL      F:_vListInsert     ;; 3 cycles
// 2832 
// 2833 	prvAddCurrentTaskToDelayedList( xTicksToWait, pdTRUE );
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI EndBlock cfiBlock27
        ; ------------------------------------- Block: 32 cycles
        ; ------------------------------------- Total: 72 cycles
        REQUIRE ?Subroutine1
        ; // Fall through to label ?Subroutine1
// 2834 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock28 Using cfiCommon0
          CFI NoFunction
          CFI CFA SP+8
          CFI FunCall _vTaskPlaceOnEventList _prvAddCurrentTaskToDelayedList
          CFI FunCall _vTaskPlaceOnUnorderedEventList _prvAddCurrentTaskToDelayedList
        CODE
?Subroutine1:
        CALL      F:_prvAddCurrentTaskToDelayedList  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock28
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 2835 /*-----------------------------------------------------------*/
// 2836 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock29 Using cfiCommon0
          CFI Function _vTaskPlaceOnUnorderedEventList
        CODE
// 2837 void vTaskPlaceOnUnorderedEventList( List_t * pxEventList, const TickType_t xItemValue, const TickType_t xTicksToWait )
// 2838 {
_vTaskPlaceOnUnorderedEventList:
        ; * Stack frame (at entry) *
        ; Param size: 8
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 2839 	configASSERT( pxEventList );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_192  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_192:
        BNZ       ??prvAddCurrentTaskToDelayedList_193  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_194  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_194:
        BR        S:??prvAddCurrentTaskToDelayedList_194  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2840 
// 2841 	/* THIS FUNCTION MUST BE CALLED WITH THE SCHEDULER SUSPENDED.  It is used by
// 2842 	the event groups implementation. */
// 2843 	configASSERT( uxSchedulerSuspended != 0 );
??prvAddCurrentTaskToDelayedList_193:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_195  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_196  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_196:
        BR        S:??prvAddCurrentTaskToDelayedList_196  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_195:
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
// 2844 
// 2845 	/* Store the item value in the event list item.  It is safe to access the
// 2846 	event list item here as interrupts won't access the event list item of a
// 2847 	task that is not in the Blocked state. */
// 2848 	listSET_LIST_ITEM_VALUE( &( pxCurrentTCB->xEventListItem ), xItemValue | taskEVENT_LIST_ITEM_VALUE_IN_USE );
        XCH       A, B               ;; 1 cycle
        OR        A, #0x80           ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        POP       BC                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      ES:[HL+0x18], AX   ;; 2 cycles
        XCHW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x1A], AX   ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
// 2849 
// 2850 	/* Place the event list item of the TCB at the end of the appropriate event
// 2851 	list.  It is safe to access the event list here because it is part of an
// 2852 	event group implementation - and interrupts don't access event groups
// 2853 	directly (instead they access them indirectly by pending function calls to
// 2854 	the task level). */
// 2855 	vListInsertEnd( pxEventList, &( pxCurrentTCB->xEventListItem ) );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 2856 
// 2857 	prvAddCurrentTaskToDelayedList( xTicksToWait, pdTRUE );
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        BR        F:?Subroutine1     ;; 3 cycles
          CFI EndBlock cfiBlock29
        ; ------------------------------------- Block: 56 cycles
        ; ------------------------------------- Total: 125 cycles
// 2858 }
// 2859 /*-----------------------------------------------------------*/
// 2860 
// 2861 #if( configUSE_TIMERS == 1 )
// 2862 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock30 Using cfiCommon2
          CFI Function _vTaskPlaceOnEventListRestricted
        CODE
// 2863 	void vTaskPlaceOnEventListRestricted( List_t * const pxEventList, TickType_t xTicksToWait, const BaseType_t xWaitIndefinitely )
// 2864 	{
_vTaskPlaceOnEventListRestricted:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 2865 		configASSERT( pxEventList );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_197  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        XCHW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_197:
        BNZ       ??prvAddCurrentTaskToDelayedList_198  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_199  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_199:
        BR        S:??prvAddCurrentTaskToDelayedList_199  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_198:
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
// 2866 
// 2867 		/* This function should not be called by application code hence the
// 2868 		'Restricted' in its name.  It is not part of the public API.  It is
// 2869 		designed for use by kernel code, and has special calling requirements -
// 2870 		it should be called with the scheduler suspended. */
// 2871 
// 2872 
// 2873 		/* Place the event list item of the TCB in the appropriate event list.
// 2874 		In this case it is assume that this is the only task that is going to
// 2875 		be waiting on this event list, so the faster vListInsertEnd() function
// 2876 		can be used in place of vListInsert. */
// 2877 		vListInsertEnd( pxEventList, &( pxCurrentTCB->xEventListItem ) );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
// 2878 
// 2879 		/* If the task should block indefinitely then set the block time to a
// 2880 		value that will be recognised as an indefinite delay inside the
// 2881 		prvAddCurrentTaskToDelayedList() function. */
// 2882 		if( xWaitIndefinitely != pdFALSE )
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_200  ;; 4 cycles
        ; ------------------------------------- Block: 41 cycles
// 2883 		{
// 2884 			xTicksToWait = portMAX_DELAY;
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2885 		}
// 2886 
// 2887 		traceTASK_DELAY_UNTIL( ( xTickCount + xTicksToWait ) );
// 2888 		prvAddCurrentTaskToDelayedList( xTicksToWait, xWaitIndefinitely );
??prvAddCurrentTaskToDelayedList_200:
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _prvAddCurrentTaskToDelayedList
        CALL      F:_prvAddCurrentTaskToDelayedList  ;; 3 cycles
// 2889 	}
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock30
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 98 cycles
// 2890 
// 2891 #endif /* configUSE_TIMERS */
// 2892 /*-----------------------------------------------------------*/
// 2893 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock31 Using cfiCommon0
          CFI Function _xTaskRemoveFromEventList
        CODE
// 2894 BaseType_t xTaskRemoveFromEventList( const List_t * const pxEventList )
// 2895 {
_xTaskRemoveFromEventList:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 2896 TCB_t *pxUnblockedTCB;
// 2897 BaseType_t xReturn;
// 2898 
// 2899 	/* THIS FUNCTION MUST BE CALLED FROM A CRITICAL SECTION.  It can also be
// 2900 	called from a critical section within an ISR. */
// 2901 
// 2902 	/* The event list is sorted in priority order, so the first in the list can
// 2903 	be removed as it is known to be the highest priority.  Remove the TCB from
// 2904 	the delayed list, and add it to the ready list.
// 2905 
// 2906 	If an event is for a queue that is locked then this function will never
// 2907 	get called - the lock count on the queue will get modified instead.  This
// 2908 	means exclusive access to the event list is guaranteed here.
// 2909 
// 2910 	This function assumes that a check has already been made to ensure that
// 2911 	pxEventList is not empty. */
// 2912 	pxUnblockedTCB = ( TCB_t * ) listGET_OWNER_OF_HEAD_ENTRY( pxEventList );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 2913 	configASSERT( pxUnblockedTCB );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_201  ;; 4 cycles
        ; ------------------------------------- Block: 31 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_201:
        BNZ       ??prvAddCurrentTaskToDelayedList_202  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_203  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_203:
        BR        S:??prvAddCurrentTaskToDelayedList_203  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2914 	( void ) uxListRemove( &( pxUnblockedTCB->xEventListItem ) );
??prvAddCurrentTaskToDelayedList_202:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 2915 
// 2916 	if( uxSchedulerSuspended == ( UBaseType_t ) pdFALSE )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_204  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
// 2917 	{
// 2918 		( void ) uxListRemove( &( pxUnblockedTCB->xStateListItem ) );
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 2919 		prvAddTaskToReadyList( pxUnblockedTCB );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_205  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_205:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        BR        S:??prvAddCurrentTaskToDelayedList_206  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 2920 	}
// 2921 	else
// 2922 	{
// 2923 		/* The delayed and ready lists cannot be accessed, so hold this task
// 2924 		pending until the scheduler is resumed. */
// 2925 		vListInsertEnd( &( xPendingReadyList ), &( pxUnblockedTCB->xEventListItem ) );
??prvAddCurrentTaskToDelayedList_204:
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_xPendingReadyList)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInsertEnd
        ; ------------------------------------- Block: 6 cycles
??prvAddCurrentTaskToDelayedList_206:
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 2926 	}
// 2927 
// 2928 	if( pxUnblockedTCB->uxPriority > pxCurrentTCB->uxPriority )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        CMPW      AX, DE             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_207  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
// 2929 	{
// 2930 		/* Return true if the task removed from the event list has a higher
// 2931 		priority than the calling task.  This allows the calling task to know if
// 2932 		it should force a context switch now. */
// 2933 		xReturn = pdTRUE;
        ONEW      AX                 ;; 1 cycle
// 2934 
// 2935 		/* Mark that a yield is pending in case the user is not using the
// 2936 		"xHigherPriorityTaskWoken" parameter to an ISR safe FreeRTOS function. */
// 2937 		xYieldPending = pdTRUE;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+18, AX  ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_208  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 2938 	}
// 2939 	else
// 2940 	{
// 2941 		xReturn = pdFALSE;
??prvAddCurrentTaskToDelayedList_207:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2942 	}
// 2943 
// 2944 	#if( configUSE_TICKLESS_IDLE != 0 )
// 2945 	{
// 2946 		/* If a task is blocked on a kernel object then xNextTaskUnblockTime
// 2947 		might be set to the blocked task's time out time.  If the task is
// 2948 		unblocked for a reason other than a timeout xNextTaskUnblockTime is
// 2949 		normally left unchanged, because it is automatically reset to a new
// 2950 		value when the tick count equals xNextTaskUnblockTime.  However if
// 2951 		tickless idling is used it might be more important to enter sleep mode
// 2952 		at the earliest possible time - so reset xNextTaskUnblockTime here to
// 2953 		ensure it is updated at the earliest possible time. */
// 2954 		prvResetNextTaskUnblockTime();
// 2955 	}
// 2956 	#endif
// 2957 
// 2958 	return xReturn;
??prvAddCurrentTaskToDelayedList_208:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock31
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 171 cycles
// 2959 }
// 2960 /*-----------------------------------------------------------*/
// 2961 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock32 Using cfiCommon0
          CFI Function _xTaskRemoveFromUnorderedEventList
        CODE
// 2962 BaseType_t xTaskRemoveFromUnorderedEventList( ListItem_t * pxEventListItem, const TickType_t xItemValue )
// 2963 {
_xTaskRemoveFromUnorderedEventList:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 2964 TCB_t *pxUnblockedTCB;
// 2965 BaseType_t xReturn;
// 2966 
// 2967 	/* THIS FUNCTION MUST BE CALLED WITH THE SCHEDULER SUSPENDED.  It is used by
// 2968 	the event flags implementation. */
// 2969 	configASSERT( uxSchedulerSuspended != pdFALSE );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_209  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_210  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_210:
        BR        S:??prvAddCurrentTaskToDelayedList_210  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_209:
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
// 2970 
// 2971 	/* Store the new item value in the event list. */
// 2972 	listSET_LIST_ITEM_VALUE( pxEventListItem, xItemValue | taskEVENT_LIST_ITEM_VALUE_IN_USE );
        XCH       A, B               ;; 1 cycle
        OR        A, #0x80           ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2973 
// 2974 	/* Remove the event list form the event flag.  Interrupts do not access
// 2975 	event flags. */
// 2976 	pxUnblockedTCB = ( TCB_t * ) listGET_LIST_ITEM_OWNER( pxEventListItem );
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 2977 	configASSERT( pxUnblockedTCB );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_211  ;; 4 cycles
        ; ------------------------------------- Block: 38 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_211:
        BNZ       ??prvAddCurrentTaskToDelayedList_212  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_213  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_213:
        BR        S:??prvAddCurrentTaskToDelayedList_213  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2978 	( void ) uxListRemove( pxEventListItem );
??prvAddCurrentTaskToDelayedList_212:
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 2979 
// 2980 	/* Remove the task from the delayed list and add it to the ready list.  The
// 2981 	scheduler is suspended so interrupts will not be accessing the ready
// 2982 	lists. */
// 2983 	( void ) uxListRemove( &( pxUnblockedTCB->xStateListItem ) );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 2984 	prvAddTaskToReadyList( pxUnblockedTCB );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_214  ;; 4 cycles
        ; ------------------------------------- Block: 31 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_214:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 2985 
// 2986 	if( pxUnblockedTCB->uxPriority > pxCurrentTCB->uxPriority )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        CMPW      AX, DE             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_215  ;; 4 cycles
        ; ------------------------------------- Block: 46 cycles
// 2987 	{
// 2988 		/* Return true if the task removed from the event list has
// 2989 		a higher priority than the calling task.  This allows
// 2990 		the calling task to know if it should force a context
// 2991 		switch now. */
// 2992 		xReturn = pdTRUE;
        ONEW      AX                 ;; 1 cycle
// 2993 
// 2994 		/* Mark that a yield is pending in case the user is not using the
// 2995 		"xHigherPriorityTaskWoken" parameter to an ISR safe FreeRTOS function. */
// 2996 		xYieldPending = pdTRUE;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+18, AX  ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_216  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 2997 	}
// 2998 	else
// 2999 	{
// 3000 		xReturn = pdFALSE;
??prvAddCurrentTaskToDelayedList_215:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 3001 	}
// 3002 
// 3003 	return xReturn;
??prvAddCurrentTaskToDelayedList_216:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock32
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 193 cycles
// 3004 }
// 3005 /*-----------------------------------------------------------*/
// 3006 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock33 Using cfiCommon0
          CFI Function _vTaskSetTimeOutState
          CFI NoCalls
        CODE
// 3007 void vTaskSetTimeOutState( TimeOut_t * const pxTimeOut )
// 3008 {
_vTaskSetTimeOutState:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3009 	configASSERT( pxTimeOut );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_217  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_217:
        BNZ       ??prvAddCurrentTaskToDelayedList_218  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_219  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_219:
        BR        S:??prvAddCurrentTaskToDelayedList_219  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3010 	pxTimeOut->xOverflowCount = xNumOfOverflows;
??prvAddCurrentTaskToDelayedList_218:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB+20  ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 3011 	pxTimeOut->xTimeOnEntering = xTickCount;
        MOVW      HL, #LWRD(_pxCurrentTCB+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        POP       BC                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3012 }
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock33
        ; ------------------------------------- Block: 36 cycles
        ; ------------------------------------- Total: 76 cycles
// 3013 /*-----------------------------------------------------------*/
// 3014 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock34 Using cfiCommon0
          CFI Function _xTaskCheckForTimeOut
        CODE
// 3015 BaseType_t xTaskCheckForTimeOut( TimeOut_t * const pxTimeOut, TickType_t * const pxTicksToWait )
// 3016 {
_xTaskCheckForTimeOut:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 16
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+20
// 3017 BaseType_t xReturn;
// 3018 
// 3019 	configASSERT( pxTimeOut );
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_220  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_220:
        BNZ       ??prvAddCurrentTaskToDelayedList_221  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_222  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_222:
        BR        S:??prvAddCurrentTaskToDelayedList_222  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3020 	configASSERT( pxTicksToWait );
??prvAddCurrentTaskToDelayedList_221:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_223  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_223:
        BNZ       ??prvAddCurrentTaskToDelayedList_224  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_225  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_225:
        BR        S:??prvAddCurrentTaskToDelayedList_225  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3021 
// 3022 	taskENTER_CRITICAL();
??prvAddCurrentTaskToDelayedList_224:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_226  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_226:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 3023 	{
// 3024 		/* Minor optimisation.  The tick count cannot change in this block. */
// 3025 		const TickType_t xConstTickCount = xTickCount;
        MOVW      HL, #LWRD(_pxCurrentTCB+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 3026 
// 3027 		#if( INCLUDE_xTaskAbortDelay == 1 )
// 3028 			if( pxCurrentTCB->ucDelayAborted != pdFALSE )
// 3029 			{
// 3030 				/* The delay was aborted, which is not the same as a time out,
// 3031 				but has the same result. */
// 3032 				pxCurrentTCB->ucDelayAborted = pdFALSE;
// 3033 				xReturn = pdTRUE;
// 3034 			}
// 3035 			else
// 3036 		#endif
// 3037 
// 3038 		#if ( INCLUDE_vTaskSuspend == 1 )
// 3039 			if( *pxTicksToWait == portMAX_DELAY )
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 25 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??xTaskCheckForTimeOut_0:
        BNZ       ??prvAddCurrentTaskToDelayedList_227  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3040 			{
// 3041 				/* If INCLUDE_vTaskSuspend is set to 1 and the block time
// 3042 				specified is the maximum block time then the task should block
// 3043 				indefinitely, and therefore never time out. */
// 3044 				xReturn = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        BR        R:??prvAddCurrentTaskToDelayedList_228  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3045 			}
// 3046 			else
// 3047 		#endif
// 3048 
// 3049 		if( ( xNumOfOverflows != pxTimeOut->xOverflowCount ) && ( xConstTickCount >= pxTimeOut->xTimeOnEntering ) ) /*lint !e525 Indentation preferred as is to make code within pre-processor directives clearer. */
??prvAddCurrentTaskToDelayedList_227:
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB+20  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_229  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        MOVW      HL, SP             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_230  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_230  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_230  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_230:
        BNC       ??prvAddCurrentTaskToDelayedList_231  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3050 		{
// 3051 			/* The tick count is greater than the time at which
// 3052 			vTaskSetTimeout() was called, but has also overflowed since
// 3053 			vTaskSetTimeOut() was called.  It must have wrapped all the way
// 3054 			around and gone past again. This passed since vTaskSetTimeout()
// 3055 			was called. */
// 3056 			xReturn = pdTRUE;
// 3057 		}
// 3058 		else if( ( ( TickType_t ) ( xConstTickCount - pxTimeOut->xTimeOnEntering ) ) < *pxTicksToWait ) /*lint !e961 Explicit casting is only redundant with some compilers, whereas others require it to prevent integer conversion errors. */
??prvAddCurrentTaskToDelayedList_229:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
        BNC       ??prvAddCurrentTaskToDelayedList_231  ;; 4 cycles
        ; ------------------------------------- Block: 35 cycles
// 3059 		{
// 3060 			/* Not a genuine timeout. Adjust parameters for time remaining. */
// 3061 			*pxTicksToWait -= ( xConstTickCount - pxTimeOut->xTimeOnEntering );
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+26
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3062 			vTaskSetTimeOutState( pxTimeOut );
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vTaskSetTimeOutState
        CALL      F:_vTaskSetTimeOutState  ;; 3 cycles
// 3063 			xReturn = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_228  ;; 3 cycles
        ; ------------------------------------- Block: 59 cycles
// 3064 		}
// 3065 		else
// 3066 		{
// 3067 			xReturn = pdTRUE;
??prvAddCurrentTaskToDelayedList_231:
        ONEW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvAddCurrentTaskToDelayedList_228:
        MOVW      HL, AX             ;; 1 cycle
// 3068 		}
// 3069 	}
// 3070 	taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_232  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_232  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3071 
// 3072 	return xReturn;
??prvAddCurrentTaskToDelayedList_232:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock34
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 306 cycles
// 3073 }
// 3074 /*-----------------------------------------------------------*/
// 3075 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock35 Using cfiCommon3
          CFI Function _vTaskMissedYield
          CFI NoCalls
        CODE
// 3076 void vTaskMissedYield( void )
// 3077 {
_vTaskMissedYield:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3078 	xYieldPending = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+18, AX  ;; 2 cycles
// 3079 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock35
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 3080 /*-----------------------------------------------------------*/
// 3081 
// 3082 #if ( configUSE_TRACE_FACILITY == 1 )
// 3083 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock36 Using cfiCommon0
          CFI Function _uxTaskGetTaskNumber
          CFI NoCalls
        CODE
// 3084 	UBaseType_t uxTaskGetTaskNumber( TaskHandle_t xTask )
// 3085 	{
_uxTaskGetTaskNumber:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3086 	UBaseType_t uxReturn;
// 3087 	TCB_t *pxTCB;
// 3088 
// 3089 		if( xTask != NULL )
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_233  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_233:
        BZ        ??prvAddCurrentTaskToDelayedList_234  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3090 		{
// 3091 			pxTCB = ( TCB_t * ) xTask;
// 3092 			uxReturn = pxTCB->uxTaskNumber;
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x40          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_235  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 3093 		}
// 3094 		else
// 3095 		{
// 3096 			uxReturn = 0U;
??prvAddCurrentTaskToDelayedList_234:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 3097 		}
// 3098 
// 3099 		return uxReturn;
??prvAddCurrentTaskToDelayedList_235:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock36
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 35 cycles
// 3100 	}
// 3101 
// 3102 #endif /* configUSE_TRACE_FACILITY */
// 3103 /*-----------------------------------------------------------*/
// 3104 
// 3105 #if ( configUSE_TRACE_FACILITY == 1 )
// 3106 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock37 Using cfiCommon2
          CFI Function _vTaskSetTaskNumber
          CFI NoCalls
        CODE
// 3107 	void vTaskSetTaskNumber( TaskHandle_t xTask, const UBaseType_t uxHandle )
// 3108 	{
_vTaskSetTaskNumber:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3109 	TCB_t *pxTCB;
// 3110 
// 3111 		if( xTask != NULL )
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_236  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_236:
        BZ        ??prvAddCurrentTaskToDelayedList_237  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3112 		{
// 3113 			pxTCB = ( TCB_t * ) xTask;
// 3114 			pxTCB->uxTaskNumber = uxHandle;
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x40          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 6 cycles
// 3115 		}
// 3116 	}
??prvAddCurrentTaskToDelayedList_237:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock37
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 32 cycles
// 3117 
// 3118 #endif /* configUSE_TRACE_FACILITY */
// 3119 
// 3120 /*
// 3121  * -----------------------------------------------------------
// 3122  * The Idle task.
// 3123  * ----------------------------------------------------------
// 3124  *
// 3125  * The portTASK_FUNCTION() macro is used to allow port/compiler specific
// 3126  * language extensions.  The equivalent prototype for this function is:
// 3127  *
// 3128  * void prvIdleTask( void *pvParameters );
// 3129  *
// 3130  */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock38 Using cfiCommon0
          CFI Function _prvIdleTask
          CFI FunCall _prvCheckTasksWaitingTermination
        CODE
// 3131 static portTASK_FUNCTION( prvIdleTask, pvParameters )
// 3132 {
_prvIdleTask:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3133 	/* Stop warnings. */
// 3134 	( void ) pvParameters;
// 3135 
// 3136 	/** THIS IS THE RTOS IDLE TASK - WHICH IS CREATED AUTOMATICALLY WHEN THE
// 3137 	SCHEDULER IS STARTED. **/
// 3138 
// 3139 	for( ;; )
// 3140 	{
// 3141 		/* See if any tasks have deleted themselves - if so then the idle task
// 3142 		is responsible for freeing the deleted task's TCB and stack. */
// 3143 		prvCheckTasksWaitingTermination();
??prvIdleTask_0:
        CALL      F:_prvCheckTasksWaitingTermination  ;; 3 cycles
// 3144 
// 3145 		#if ( configUSE_PREEMPTION == 0 )
// 3146 		{
// 3147 			/* If we are not using preemption we keep forcing a task switch to
// 3148 			see if any other task has become available.  If we are using
// 3149 			preemption we don't need to do this as any task becoming available
// 3150 			will automatically get the processor anyway. */
// 3151 			taskYIELD();
// 3152 		}
// 3153 		#endif /* configUSE_PREEMPTION */
// 3154 
// 3155 		#if ( ( configUSE_PREEMPTION == 1 ) && ( configIDLE_SHOULD_YIELD == 1 ) )
// 3156 		{
// 3157 			/* When using preemption tasks of equal priority will be
// 3158 			timesliced.  If a task that is sharing the idle priority is ready
// 3159 			to run then the idle task should yield before the end of the
// 3160 			timeslice.
// 3161 
// 3162 			A critical region is not required here as we are just reading from
// 3163 			the list, and an occasional incorrect value will not matter.  If
// 3164 			the ready list at the idle priority contains more than one task
// 3165 			then a task other than the idle task is ready to execute. */
// 3166 			if( listCURRENT_LIST_LENGTH( &( pxReadyTasksLists[ tskIDLE_PRIORITY ] ) ) > ( UBaseType_t ) 1 )
        MOV       ES, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      AX, ES:_pxReadyTasksLists  ;; 2 cycles
        CMPW      AX, #0x2           ;; 1 cycle
        SKC                          ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
// 3167 			{
// 3168 				taskYIELD();
        BRK                          ;; 5 cycles
          CFI FunCall _vApplicationIdleHook
        ; ------------------------------------- Block: 5 cycles
// 3169 			}
// 3170 			else
// 3171 			{
// 3172 				mtCOVERAGE_TEST_MARKER();
// 3173 			}
// 3174 		}
// 3175 		#endif /* ( ( configUSE_PREEMPTION == 1 ) && ( configIDLE_SHOULD_YIELD == 1 ) ) */
// 3176 
// 3177 		#if ( configUSE_IDLE_HOOK == 1 )
// 3178 		{
// 3179 			extern void vApplicationIdleHook( void );
// 3180 
// 3181 			/* Call the user defined function from within the idle task.  This
// 3182 			allows the application designer to add background functionality
// 3183 			without the overhead of a separate task.
// 3184 			NOTE: vApplicationIdleHook() MUST NOT, UNDER ANY CIRCUMSTANCES,
// 3185 			CALL A FUNCTION THAT MIGHT BLOCK. */
// 3186 			vApplicationIdleHook();
??prvIdleTask_1:
        CALL      F:_vApplicationIdleHook  ;; 3 cycles
        BR        S:??prvIdleTask_0  ;; 3 cycles
          CFI EndBlock cfiBlock38
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 19 cycles
// 3187 		}
// 3188 		#endif /* configUSE_IDLE_HOOK */
// 3189 
// 3190 		/* This conditional compilation should use inequality to 0, not equality
// 3191 		to 1.  This is to ensure portSUPPRESS_TICKS_AND_SLEEP() is called when
// 3192 		user defined low power mode	implementations require
// 3193 		configUSE_TICKLESS_IDLE to be set to a value other than 1. */
// 3194 		#if ( configUSE_TICKLESS_IDLE != 0 )
// 3195 		{
// 3196 		TickType_t xExpectedIdleTime;
// 3197 
// 3198 			/* It is not desirable to suspend then resume the scheduler on
// 3199 			each iteration of the idle task.  Therefore, a preliminary
// 3200 			test of the expected idle time is performed without the
// 3201 			scheduler suspended.  The result here is not necessarily
// 3202 			valid. */
// 3203 			xExpectedIdleTime = prvGetExpectedIdleTime();
// 3204 
// 3205 			if( xExpectedIdleTime >= configEXPECTED_IDLE_TIME_BEFORE_SLEEP )
// 3206 			{
// 3207 				vTaskSuspendAll();
// 3208 				{
// 3209 					/* Now the scheduler is suspended, the expected idle
// 3210 					time can be sampled again, and this time its value can
// 3211 					be used. */
// 3212 					configASSERT( xNextTaskUnblockTime >= xTickCount );
// 3213 					xExpectedIdleTime = prvGetExpectedIdleTime();
// 3214 
// 3215 					if( xExpectedIdleTime >= configEXPECTED_IDLE_TIME_BEFORE_SLEEP )
// 3216 					{
// 3217 						traceLOW_POWER_IDLE_BEGIN();
// 3218 						portSUPPRESS_TICKS_AND_SLEEP( xExpectedIdleTime );
// 3219 						traceLOW_POWER_IDLE_END();
// 3220 					}
// 3221 					else
// 3222 					{
// 3223 						mtCOVERAGE_TEST_MARKER();
// 3224 					}
// 3225 				}
// 3226 				( void ) xTaskResumeAll();
// 3227 			}
// 3228 			else
// 3229 			{
// 3230 				mtCOVERAGE_TEST_MARKER();
// 3231 			}
// 3232 		}
// 3233 		#endif /* configUSE_TICKLESS_IDLE */
// 3234 	}
// 3235 }
// 3236 /*-----------------------------------------------------------*/
// 3237 
// 3238 #if( configUSE_TICKLESS_IDLE != 0 )
// 3239 
// 3240 	eSleepModeStatus eTaskConfirmSleepModeStatus( void )
// 3241 	{
// 3242 	/* The idle task exists in addition to the application tasks. */
// 3243 	const UBaseType_t uxNonApplicationTasks = 1;
// 3244 	eSleepModeStatus eReturn = eStandardSleep;
// 3245 
// 3246 		if( listCURRENT_LIST_LENGTH( &xPendingReadyList ) != 0 )
// 3247 		{
// 3248 			/* A task was made ready while the scheduler was suspended. */
// 3249 			eReturn = eAbortSleep;
// 3250 		}
// 3251 		else if( xYieldPending != pdFALSE )
// 3252 		{
// 3253 			/* A yield was pended while the scheduler was suspended. */
// 3254 			eReturn = eAbortSleep;
// 3255 		}
// 3256 		else
// 3257 		{
// 3258 			/* If all the tasks are in the suspended list (which might mean they
// 3259 			have an infinite block time rather than actually being suspended)
// 3260 			then it is safe to turn all clocks off and just wait for external
// 3261 			interrupts. */
// 3262 			if( listCURRENT_LIST_LENGTH( &xSuspendedTaskList ) == ( uxCurrentNumberOfTasks - uxNonApplicationTasks ) )
// 3263 			{
// 3264 				eReturn = eNoTasksWaitingTimeout;
// 3265 			}
// 3266 			else
// 3267 			{
// 3268 				mtCOVERAGE_TEST_MARKER();
// 3269 			}
// 3270 		}
// 3271 
// 3272 		return eReturn;
// 3273 	}
// 3274 
// 3275 #endif /* configUSE_TICKLESS_IDLE */
// 3276 /*-----------------------------------------------------------*/
// 3277 
// 3278 #if ( configNUM_THREAD_LOCAL_STORAGE_POINTERS != 0 )
// 3279 
// 3280 	void vTaskSetThreadLocalStoragePointer( TaskHandle_t xTaskToSet, BaseType_t xIndex, void *pvValue )
// 3281 	{
// 3282 	TCB_t *pxTCB;
// 3283 
// 3284 		if( xIndex < configNUM_THREAD_LOCAL_STORAGE_POINTERS )
// 3285 		{
// 3286 			pxTCB = prvGetTCBFromHandle( xTaskToSet );
// 3287 			pxTCB->pvThreadLocalStoragePointers[ xIndex ] = pvValue;
// 3288 		}
// 3289 	}
// 3290 
// 3291 #endif /* configNUM_THREAD_LOCAL_STORAGE_POINTERS */
// 3292 /*-----------------------------------------------------------*/
// 3293 
// 3294 #if ( configNUM_THREAD_LOCAL_STORAGE_POINTERS != 0 )
// 3295 
// 3296 	void *pvTaskGetThreadLocalStoragePointer( TaskHandle_t xTaskToQuery, BaseType_t xIndex )
// 3297 	{
// 3298 	void *pvReturn = NULL;
// 3299 	TCB_t *pxTCB;
// 3300 
// 3301 		if( xIndex < configNUM_THREAD_LOCAL_STORAGE_POINTERS )
// 3302 		{
// 3303 			pxTCB = prvGetTCBFromHandle( xTaskToQuery );
// 3304 			pvReturn = pxTCB->pvThreadLocalStoragePointers[ xIndex ];
// 3305 		}
// 3306 		else
// 3307 		{
// 3308 			pvReturn = NULL;
// 3309 		}
// 3310 
// 3311 		return pvReturn;
// 3312 	}
// 3313 
// 3314 #endif /* configNUM_THREAD_LOCAL_STORAGE_POINTERS */
// 3315 /*-----------------------------------------------------------*/
// 3316 
// 3317 #if ( portUSING_MPU_WRAPPERS == 1 )
// 3318 
// 3319 	void vTaskAllocateMPURegions( TaskHandle_t xTaskToModify, const MemoryRegion_t * const xRegions )
// 3320 	{
// 3321 	TCB_t *pxTCB;
// 3322 
// 3323 		/* If null is passed in here then we are modifying the MPU settings of
// 3324 		the calling task. */
// 3325 		pxTCB = prvGetTCBFromHandle( xTaskToModify );
// 3326 
// 3327 		vPortStoreTaskMPUSettings( &( pxTCB->xMPUSettings ), xRegions, NULL, 0 );
// 3328 	}
// 3329 
// 3330 #endif /* portUSING_MPU_WRAPPERS */
// 3331 /*-----------------------------------------------------------*/
// 3332 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock39 Using cfiCommon3
          CFI Function _prvInitialiseTaskLists
        CODE
// 3333 static void prvInitialiseTaskLists( void )
// 3334 {
_prvInitialiseTaskLists:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 3335 UBaseType_t uxPriority;
// 3336 
// 3337 	for( uxPriority = ( UBaseType_t ) 0U; uxPriority < ( UBaseType_t ) configMAX_PRIORITIES; uxPriority++ )
        MOVW      HL, #0x0           ;; 1 cycle
        MOV       A, #0x7            ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
// 3338 	{
// 3339 		vListInitialise( &( pxReadyTasksLists[ uxPriority ] ) );
??prvInitialiseTaskLists_0:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       E, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        MOV       A, E               ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        POP       DE                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
// 3340 	}
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??prvInitialiseTaskLists_0  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 3341 
// 3342 	vListInitialise( &xDelayedTaskList1 );
        MOVW      DE, #LWRD(_xPendingReadyList+62)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
// 3343 	vListInitialise( &xDelayedTaskList2 );
        MOVW      DE, #LWRD(_xPendingReadyList+80)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
// 3344 	vListInitialise( &xPendingReadyList );
        MOVW      DE, #LWRD(_xPendingReadyList)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
// 3345 
// 3346 	#if ( INCLUDE_vTaskDelete == 1 )
// 3347 	{
// 3348 		vListInitialise( &xTasksWaitingTermination );
        MOVW      DE, #LWRD(_xPendingReadyList+18)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
// 3349 	}
// 3350 	#endif /* INCLUDE_vTaskDelete */
// 3351 
// 3352 	#if ( INCLUDE_vTaskSuspend == 1 )
// 3353 	{
// 3354 		vListInitialise( &xSuspendedTaskList );
        MOVW      DE, #LWRD(_xPendingReadyList+44)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
// 3355 	}
// 3356 	#endif /* INCLUDE_vTaskSuspend */
// 3357 
// 3358 	/* Start with pxDelayedTaskList using list1 and the pxOverflowDelayedTaskList
// 3359 	using list2. */
// 3360 	pxDelayedTaskList = &xDelayedTaskList1;
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOV       ES:_xPendingReadyList+38, #BYTE3(_xPendingReadyList)  ;; 2 cycles
        MOVW      AX, #LWRD(_xPendingReadyList+62)  ;; 1 cycle
        MOVW      ES:_xPendingReadyList+36, AX  ;; 2 cycles
// 3361 	pxOverflowDelayedTaskList = &xDelayedTaskList2;
        MOV       ES:_xPendingReadyList+42, #BYTE3(_xPendingReadyList)  ;; 2 cycles
        MOVW      AX, #LWRD(_xPendingReadyList+80)  ;; 1 cycle
        MOVW      ES:_xPendingReadyList+40, AX  ;; 2 cycles
// 3362 }
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock39
        ; ------------------------------------- Block: 43 cycles
        ; ------------------------------------- Total: 70 cycles
// 3363 /*-----------------------------------------------------------*/
// 3364 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock40 Using cfiCommon3
          CFI Function _prvCheckTasksWaitingTermination
        CODE
// 3365 static void prvCheckTasksWaitingTermination( void )
// 3366 {
_prvCheckTasksWaitingTermination:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        BR        S:??prvAddCurrentTaskToDelayedList_238  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3367 
// 3368 	/** THIS FUNCTION IS CALLED FROM THE RTOS IDLE TASK **/
// 3369 
// 3370 	#if ( INCLUDE_vTaskDelete == 1 )
// 3371 	{
// 3372 		BaseType_t xListIsEmpty;
// 3373 
// 3374 		/* ucTasksDeleted is used to prevent vTaskSuspendAll() being called
// 3375 		too often in the idle task. */
// 3376 		while( uxDeletedTasksWaitingCleanUp > ( UBaseType_t ) 0U )
// 3377 		{
// 3378 			vTaskSuspendAll();
??prvCheckTasksWaitingTermination_0:
        INCW      ES:_pxCurrentTCB+32  ;; 3 cycles
// 3379 			{
// 3380 				xListIsEmpty = listLIST_IS_EMPTY( &xTasksWaitingTermination );
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:_xPendingReadyList+18  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvCheckTasksWaitingTermination_1:
        MOV       [SP], A            ;; 1 cycle
// 3381 			}
// 3382 			( void ) xTaskResumeAll();
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
// 3383 
// 3384 			if( xListIsEmpty == pdFALSE )
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_238  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 3385 			{
// 3386 				TCB_t *pxTCB;
// 3387 
// 3388 				taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_239  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_239:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 3389 				{
// 3390 					pxTCB = ( TCB_t * ) listGET_OWNER_OF_HEAD_ENTRY( ( &xTasksWaitingTermination ) );
// 3391 					( void ) uxListRemove( &( pxTCB->xStateListItem ) );
        MOVW      HL, #LWRD(_xPendingReadyList+28)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        POP       DE                 ;; 1 cycle
          CFI CFA SP+6
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 3392 					--uxCurrentNumberOfTasks;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        DECW      ES:_pxCurrentTCB+6  ;; 3 cycles
// 3393 					--uxDeletedTasksWaitingCleanUp;
        DECW      ES:_pxCurrentTCB+4  ;; 3 cycles
// 3394 				}
// 3395 				taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_238  ;; 4 cycles
        ; ------------------------------------- Block: 43 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_238  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3396 
// 3397 				prvDeleteTCB( pxTCB );
// 3398 			}
// 3399 			else
// 3400 			{
// 3401 				mtCOVERAGE_TEST_MARKER();
// 3402 			}
// 3403 		}
??prvAddCurrentTaskToDelayedList_238:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+4  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvCheckTasksWaitingTermination_0  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 3404 	}
// 3405 	#endif /* INCLUDE_vTaskDelete */
// 3406 }
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock40
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 111 cycles
// 3407 /*-----------------------------------------------------------*/
// 3408 
// 3409 #if( configUSE_TRACE_FACILITY == 1 )
// 3410 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock41 Using cfiCommon0
          CFI Function _vTaskGetInfo
        CODE
// 3411 	void vTaskGetInfo( TaskHandle_t xTask, TaskStatus_t *pxTaskStatus, BaseType_t xGetFreeStackSpace, eTaskState eState )
// 3412 	{
_vTaskGetInfo:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 12
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
// 3413 	TCB_t *pxTCB;
// 3414 
// 3415 		/* xTask is NULL then get the state of the calling task. */
// 3416 		pxTCB = prvGetTCBFromHandle( xTask );
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_240  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_240:
        BNZ       ??prvAddCurrentTaskToDelayedList_241  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_242  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_241:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_242:
        MOVW      [SP], AX           ;; 1 cycle
// 3417 
// 3418 		pxTaskStatus->xHandle = ( TaskHandle_t ) pxTCB;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 3419 		pxTaskStatus->pcTaskName = ( const char * ) &( pxTCB->pcTaskName [ 0 ] );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 3420 		pxTaskStatus->uxCurrentPriority = pxTCB->uxPriority;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 3421 		pxTaskStatus->pxStackBase = pxTCB->pxStack;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 3422 		pxTaskStatus->xTaskNumber = pxTCB->uxTCBNumber;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x3E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 3423 
// 3424 		#if ( INCLUDE_vTaskSuspend == 1 )
// 3425 		{
// 3426 			/* If the task is in the suspended list then there is a chance it is
// 3427 			actually just blocked indefinitely - so really it should be reported as
// 3428 			being in the Blocked state. */
// 3429 			if( pxTaskStatus->eCurrentState == eSuspended )
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_243  ;; 4 cycles
        ; ------------------------------------- Block: 113 cycles
// 3430 			{
// 3431 				vTaskSuspendAll();
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+32  ;; 3 cycles
// 3432 				{
// 3433 					if( listLIST_ITEM_CONTAINER( &( pxTCB->xEventListItem ) ) != NULL )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_244  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_244:
        BZ        ??prvAddCurrentTaskToDelayedList_245  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3434 					{
// 3435 						pxTaskStatus->eCurrentState = eBlocked;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0x2            ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 8 cycles
// 3436 					}
// 3437 				}
// 3438 				xTaskResumeAll();
??prvAddCurrentTaskToDelayedList_245:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_243:
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
// 3439 			}
// 3440 		}
// 3441 		#endif /* INCLUDE_vTaskSuspend */
// 3442 
// 3443 		#if ( configUSE_MUTEXES == 1 )
// 3444 		{
// 3445 			pxTaskStatus->uxBasePriority = pxTCB->uxBasePriority;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 3446 		}
// 3447 		#else
// 3448 		{
// 3449 			pxTaskStatus->uxBasePriority = 0;
// 3450 		}
// 3451 		#endif
// 3452 
// 3453 		#if ( configGENERATE_RUN_TIME_STATS == 1 )
// 3454 		{
// 3455 			pxTaskStatus->ulRunTimeCounter = pxTCB->ulRunTimeCounter;
// 3456 		}
// 3457 		#else
// 3458 		{
// 3459 			pxTaskStatus->ulRunTimeCounter = 0;
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3460 		}
// 3461 		#endif
// 3462 
// 3463 		/* Obtaining the task state is a little fiddly, so is only done if the value
// 3464 		of eState passed into this function is eInvalid - otherwise the state is
// 3465 		just set to whatever is passed in. */
// 3466 		if( eState != eInvalid )
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x5            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_246  ;; 4 cycles
        ; ------------------------------------- Block: 31 cycles
// 3467 		{
// 3468 			pxTaskStatus->eCurrentState = eState;
// 3469 		}
// 3470 		else
// 3471 		{
// 3472 			pxTaskStatus->eCurrentState = eTaskGetState( xTask );
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _eTaskGetState
        CALL      F:_eTaskGetState   ;; 3 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
??prvAddCurrentTaskToDelayedList_246:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      AX, [SP+0x10]      ;; 1 cycle
// 3473 		}
// 3474 
// 3475 		/* Obtaining the stack space takes some time, so the xGetFreeStackSpace
// 3476 		parameter is provided to allow it to be skipped. */
// 3477 		if( xGetFreeStackSpace != pdFALSE )
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_247  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 3478 		{
// 3479 			#if ( portSTACK_GROWTH > 0 )
// 3480 			{
// 3481 				pxTaskStatus->usStackHighWaterMark = prvTaskCheckFreeStackSpace( ( uint8_t * ) pxTCB->pxEndOfStack );
// 3482 			}
// 3483 			#else
// 3484 			{
// 3485 				pxTaskStatus->usStackHighWaterMark = prvTaskCheckFreeStackSpace( ( uint8_t * ) pxTCB->pxStack );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvTaskCheckFreeStackSpace
        CALL      F:_prvTaskCheckFreeStackSpace  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_248  ;; 3 cycles
        ; ------------------------------------- Block: 27 cycles
// 3486 			}
// 3487 			#endif
// 3488 		}
// 3489 		else
// 3490 		{
// 3491 			pxTaskStatus->usStackHighWaterMark = 0;
??prvAddCurrentTaskToDelayedList_247:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 6 cycles
// 3492 		}
// 3493 	}
??prvAddCurrentTaskToDelayedList_248:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock41
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 282 cycles
// 3494 
// 3495 #endif /* configUSE_TRACE_FACILITY */
// 3496 /*-----------------------------------------------------------*/
// 3497 
// 3498 #if ( configUSE_TRACE_FACILITY == 1 )
// 3499 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock42 Using cfiCommon0
          CFI Function _prvListTasksWithinSingleList
        CODE
// 3500 	static UBaseType_t prvListTasksWithinSingleList( TaskStatus_t *pxTaskStatusArray, List_t *pxList, eTaskState eState )
// 3501 	{
_prvListTasksWithinSingleList:
        ; * Stack frame (at entry) *
        ; Param size: 2
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 28
        SUBW      SP, #0x14          ;; 1 cycle
          CFI CFA SP+32
// 3502 	volatile TCB_t *pxNextTCB, *pxFirstTCB;
// 3503 	UBaseType_t uxTask = 0;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 3504 
// 3505 		if( listCURRENT_LIST_LENGTH( pxList ) > ( UBaseType_t ) 0 )
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+34
        POP       HL                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_249  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 3506 		{
// 3507 			listGET_OWNER_OF_NEXT_ENTRY( pxFirstTCB, pxList );
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
        POP       BC                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_250  ;; 4 cycles
        ; ------------------------------------- Block: 53 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_250:
        BNZ       ??prvAddCurrentTaskToDelayedList_251  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 32 cycles
??prvAddCurrentTaskToDelayedList_251:
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       [SP+0x12], A       ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      [SP+0x10], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        ; ------------------------------------- Block: 19 cycles
// 3508 
// 3509 			/* Populate an TaskStatus_t structure within the
// 3510 			pxTaskStatusArray array for each task that is referenced from
// 3511 			pxList.  See the definition of TaskStatus_t in task.h for the
// 3512 			meaning of each TaskStatus_t structure member. */
// 3513 			do
// 3514 			{
// 3515 				listGET_OWNER_OF_NEXT_ENTRY( pxNextTCB, pxList );
??prvListTasksWithinSingleList_0:
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
        POP       BC                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_252  ;; 4 cycles
        ; ------------------------------------- Block: 56 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_252:
        BNZ       ??prvAddCurrentTaskToDelayedList_253  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x04]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, ES:[DE+0x06]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 32 cycles
??prvAddCurrentTaskToDelayedList_253:
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      [SP+0x0C], AX      ;; 1 cycle
// 3516 				vTaskGetInfo( ( TaskHandle_t ) pxNextTCB, &( pxTaskStatusArray[ uxTask ] ), pdTRUE, eState );
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_254  ;; 4 cycles
        ; ------------------------------------- Block: 38 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_254:
        BNZ       ??prvAddCurrentTaskToDelayedList_255  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_256  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_255:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_256:
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x3E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_257  ;; 4 cycles
        ; ------------------------------------- Block: 113 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        INCW      ES:_pxCurrentTCB+32  ;; 3 cycles
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_258  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_258:
        BZ        ??prvAddCurrentTaskToDelayedList_259  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0x2            ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 8 cycles
??prvAddCurrentTaskToDelayedList_259:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_257:
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       A, [SP+0x20]       ;; 1 cycle
        CMP       A, #0x5            ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_260  ;; 4 cycles
        ; ------------------------------------- Block: 30 cycles
        MOV       B, A               ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_261  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??prvAddCurrentTaskToDelayedList_260:
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _eTaskGetState
        CALL      F:_eTaskGetState   ;; 3 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
??prvAddCurrentTaskToDelayedList_261:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvTaskCheckFreeStackSpace
        CALL      F:_prvTaskCheckFreeStackSpace  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 3517 				uxTask++;
        MOVW      AX, [SP]           ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 3518 			} while( pxNextTCB != pxFirstTCB );
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_262  ;; 4 cycles
        ; ------------------------------------- Block: 58 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_262:
        SKZ                          ;; 4 cycles
        BR        R:??prvListTasksWithinSingleList_0  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3519 		}
// 3520 		else
// 3521 		{
// 3522 			mtCOVERAGE_TEST_MARKER();
// 3523 		}
// 3524 
// 3525 		return uxTask;
??prvAddCurrentTaskToDelayedList_249:
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0x1C          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock42
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 551 cycles
// 3526 	}
// 3527 
// 3528 #endif /* configUSE_TRACE_FACILITY */
// 3529 /*-----------------------------------------------------------*/
// 3530 
// 3531 #if ( ( configUSE_TRACE_FACILITY == 1 ) || ( INCLUDE_uxTaskGetStackHighWaterMark == 1 ) )
// 3532 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock43 Using cfiCommon0
          CFI Function _prvTaskCheckFreeStackSpace
          CFI NoCalls
        CODE
// 3533 	static uint16_t prvTaskCheckFreeStackSpace( const uint8_t * pucStackByte )
// 3534 	{
_prvTaskCheckFreeStackSpace:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 3535 	uint32_t ulCount = 0U;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_263  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 3536 
// 3537 		while( *pucStackByte == ( uint8_t ) tskSTACK_FILL_BYTE )
// 3538 		{
// 3539 			pucStackByte -= portSTACK_GROWTH;
??prvTaskCheckFreeStackSpace_0:
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 3540 			ulCount++;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 16 cycles
??prvAddCurrentTaskToDelayedList_263:
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 3541 		}
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0xA5           ;; 1 cycle
        BZ        ??prvTaskCheckFreeStackSpace_0  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
// 3542 
// 3543 		ulCount /= ( uint32_t ) sizeof( StackType_t ); /*lint !e961 Casting is not redundant on smaller architectures. */
// 3544 
// 3545 		return ( uint16_t ) ulCount;
        SHRW      AX, 0x1            ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        SHRW      AX, 0x1            ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV1      A.7, CY            ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock43
        ; ------------------------------------- Block: 15 cycles
        ; ------------------------------------- Total: 52 cycles
// 3546 	}
// 3547 
// 3548 #endif /* ( ( configUSE_TRACE_FACILITY == 1 ) || ( INCLUDE_uxTaskGetStackHighWaterMark == 1 ) ) */
// 3549 /*-----------------------------------------------------------*/
// 3550 
// 3551 #if ( INCLUDE_uxTaskGetStackHighWaterMark == 1 )
// 3552 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock44 Using cfiCommon0
          CFI Function _uxTaskGetStackHighWaterMark
          CFI NoCalls
        CODE
// 3553 	UBaseType_t uxTaskGetStackHighWaterMark( TaskHandle_t xTask )
// 3554 	{
_uxTaskGetStackHighWaterMark:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
// 3555 	TCB_t *pxTCB;
// 3556 	uint8_t *pucEndOfStack;
// 3557 	UBaseType_t uxReturn;
// 3558 
// 3559 		pxTCB = prvGetTCBFromHandle( xTask );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_264  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_264:
        BNZ       ??prvAddCurrentTaskToDelayedList_265  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_266  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_265:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_266:
        MOVW      [SP], AX           ;; 1 cycle
// 3560 
// 3561 		#if portSTACK_GROWTH < 0
// 3562 		{
// 3563 			pucEndOfStack = ( uint8_t * ) pxTCB->pxStack;
// 3564 		}
// 3565 		#else
// 3566 		{
// 3567 			pucEndOfStack = ( uint8_t * ) pxTCB->pxEndOfStack;
// 3568 		}
// 3569 		#endif
// 3570 
// 3571 		uxReturn = ( UBaseType_t ) prvTaskCheckFreeStackSpace( pucEndOfStack );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_267  ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
??uxTaskGetStackHighWaterMark_0:
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 16 cycles
??prvAddCurrentTaskToDelayedList_267:
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0xA5           ;; 1 cycle
        BZ        ??uxTaskGetStackHighWaterMark_0  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
// 3572 
// 3573 		return uxReturn;
        SHRW      AX, 0x1            ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        SHRW      AX, 0x1            ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV1      A.7, CY            ;; 1 cycle
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock44
        ; ------------------------------------- Block: 15 cycles
        ; ------------------------------------- Total: 95 cycles
// 3574 	}
// 3575 
// 3576 #endif /* INCLUDE_uxTaskGetStackHighWaterMark */
// 3577 /*-----------------------------------------------------------*/
// 3578 
// 3579 #if ( INCLUDE_vTaskDelete == 1 )
// 3580 
// 3581 	static void prvDeleteTCB( TCB_t *pxTCB )
// 3582 	{
// 3583 		/* This call is required specifically for the TriCore port.  It must be
// 3584 		above the vPortFree() calls.  The call is also used by ports/demos that
// 3585 		want to allocate and clean RAM statically. */
// 3586 		portCLEAN_UP_TCB( pxTCB );
// 3587 
// 3588 		/* Free up the memory allocated by the scheduler for the task.  It is up
// 3589 		to the task to free any memory allocated at the application level. */
// 3590 		#if ( configUSE_NEWLIB_REENTRANT == 1 )
// 3591 		{
// 3592 			_reclaim_reent( &( pxTCB->xNewLib_reent ) );
// 3593 		}
// 3594 		#endif /* configUSE_NEWLIB_REENTRANT */
// 3595 
// 3596 		#if( ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 0 ) && ( portUSING_MPU_WRAPPERS == 0 ) )
// 3597 		{
// 3598 			/* The task can only have been allocated dynamically - free both
// 3599 			the stack and TCB. */
// 3600 			vPortFree( pxTCB->pxStack );
// 3601 			vPortFree( pxTCB );
// 3602 		}
// 3603 		#elif( tskSTATIC_AND_DYNAMIC_ALLOCATION_POSSIBLE == 1 )
// 3604 		{
// 3605 			/* The task could have been allocated statically or dynamically, so
// 3606 			check what was statically allocated before trying to free the
// 3607 			memory. */
// 3608 			if( pxTCB->ucStaticallyAllocated == tskDYNAMICALLY_ALLOCATED_STACK_AND_TCB )
// 3609 			{
// 3610 				/* Both the stack and TCB were allocated dynamically, so both
// 3611 				must be freed. */
// 3612 				vPortFree( pxTCB->pxStack );
// 3613 				vPortFree( pxTCB );
// 3614 			}
// 3615 			else if( pxTCB->ucStaticallyAllocated == tskSTATICALLY_ALLOCATED_STACK_ONLY )
// 3616 			{
// 3617 				/* Only the stack was statically allocated, so the TCB is the
// 3618 				only memory that must be freed. */
// 3619 				vPortFree( pxTCB );
// 3620 			}
// 3621 			else
// 3622 			{
// 3623 				/* Neither the stack nor the TCB were allocated dynamically, so
// 3624 				nothing needs to be freed. */
// 3625 				configASSERT( pxTCB->ucStaticallyAllocated == tskSTATICALLY_ALLOCATED_STACK_AND_TCB	)
// 3626 				mtCOVERAGE_TEST_MARKER();
// 3627 			}
// 3628 		}
// 3629 		#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
// 3630 	}
// 3631 
// 3632 #endif /* INCLUDE_vTaskDelete */
// 3633 /*-----------------------------------------------------------*/
// 3634 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock45 Using cfiCommon3
          CFI Function _prvResetNextTaskUnblockTime
          CFI NoCalls
        CODE
// 3635 static void prvResetNextTaskUnblockTime( void )
// 3636 {
_prvResetNextTaskUnblockTime:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3637 TCB_t *pxTCB;
// 3638 
// 3639 	if( listLIST_IS_EMPTY( pxDelayedTaskList ) != pdFALSE )
        MOVW      HL, #LWRD(_xPendingReadyList+36)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_268  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
// 3640 	{
// 3641 		/* The new current delayed list is empty.  Set xNextTaskUnblockTime to
// 3642 		the maximum possible value so it is	extremely unlikely that the
// 3643 		if( xTickCount >= xNextTaskUnblockTime ) test will pass until
// 3644 		there is an item in the delayed list. */
// 3645 		xNextTaskUnblockTime = portMAX_DELAY;
        MOVW      HL, #LWRD(_pxCurrentTCB+24)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 13 cycles
// 3646 	}
// 3647 	else
// 3648 	{
// 3649 		/* The new current delayed list is not empty, get the value of
// 3650 		the item at the head of the delayed list.  This is the time at
// 3651 		which the task at the head of the delayed list should be removed
// 3652 		from the Blocked state. */
// 3653 		( pxTCB ) = ( TCB_t * ) listGET_OWNER_OF_HEAD_ENTRY( pxDelayedTaskList );
// 3654 		xNextTaskUnblockTime = listGET_LIST_ITEM_VALUE( &( ( pxTCB )->xStateListItem ) );
??prvAddCurrentTaskToDelayedList_268:
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x0A]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, ES:[DE+0x0C]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+4
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x06]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x04]   ;; 2 cycles
        MOVW      HL, #LWRD(_pxCurrentTCB+24)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3655 	}
// 3656 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock45
        ; ------------------------------------- Block: 38 cycles
        ; ------------------------------------- Total: 66 cycles
// 3657 /*-----------------------------------------------------------*/
// 3658 
// 3659 #if ( ( INCLUDE_xTaskGetCurrentTaskHandle == 1 ) || ( configUSE_MUTEXES == 1 ) )
// 3660 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock46 Using cfiCommon3
          CFI Function _xTaskGetCurrentTaskHandle
          CFI NoCalls
        CODE
// 3661 	TaskHandle_t xTaskGetCurrentTaskHandle( void )
// 3662 	{
_xTaskGetCurrentTaskHandle:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3663 	TaskHandle_t xReturn;
// 3664 
// 3665 		/* A critical section is not required as this is not called from
// 3666 		an interrupt and the current TCB will always be the same for any
// 3667 		individual execution thread. */
// 3668 		xReturn = pxCurrentTCB;
// 3669 
// 3670 		return xReturn;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+6
        POP       DE                 ;; 1 cycle
          CFI CFA SP+4
        MOV       A, ES              ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock46
        ; ------------------------------------- Block: 15 cycles
        ; ------------------------------------- Total: 15 cycles
// 3671 	}
// 3672 
// 3673 #endif /* ( ( INCLUDE_xTaskGetCurrentTaskHandle == 1 ) || ( configUSE_MUTEXES == 1 ) ) */
// 3674 /*-----------------------------------------------------------*/
// 3675 
// 3676 #if ( ( INCLUDE_xTaskGetSchedulerState == 1 ) || ( configUSE_TIMERS == 1 ) )
// 3677 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock47 Using cfiCommon3
          CFI Function _xTaskGetSchedulerState
          CFI NoCalls
        CODE
// 3678 	BaseType_t xTaskGetSchedulerState( void )
// 3679 	{
_xTaskGetSchedulerState:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3680 	BaseType_t xReturn;
// 3681 
// 3682 		if( xSchedulerRunning == pdFALSE )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+14  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_269  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 3683 		{
// 3684 			xReturn = taskSCHEDULER_NOT_STARTED;
        ONEW      AX                 ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 7 cycles
// 3685 		}
// 3686 		else
// 3687 		{
// 3688 			if( uxSchedulerSuspended == ( UBaseType_t ) pdFALSE )
??prvAddCurrentTaskToDelayedList_269:
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_270  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 3689 			{
// 3690 				xReturn = taskSCHEDULER_RUNNING;
        MOVW      AX, #0x2           ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 7 cycles
// 3691 			}
// 3692 			else
// 3693 			{
// 3694 				xReturn = taskSCHEDULER_SUSPENDED;
??prvAddCurrentTaskToDelayedList_270:
        CLRW      AX                 ;; 1 cycle
// 3695 			}
// 3696 		}
// 3697 
// 3698 		return xReturn;
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock47
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 36 cycles
// 3699 	}
// 3700 
// 3701 #endif /* ( ( INCLUDE_xTaskGetSchedulerState == 1 ) || ( configUSE_TIMERS == 1 ) ) */
// 3702 /*-----------------------------------------------------------*/
// 3703 
// 3704 #if ( configUSE_MUTEXES == 1 )
// 3705 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock48 Using cfiCommon0
          CFI Function _vTaskPriorityInherit
        CODE
// 3706 	void vTaskPriorityInherit( TaskHandle_t const pxMutexHolder )
// 3707 	{
_vTaskPriorityInherit:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3708 	TCB_t * const pxTCB = ( TCB_t * ) pxMutexHolder;
// 3709 
// 3710 		/* If the mutex was given back by an interrupt while the queue was
// 3711 		locked then the mutex holder might now be NULL. */
// 3712 		if( pxMutexHolder != NULL )
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_271  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_271:
        SKNZ                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_272  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3713 		{
// 3714 			/* If the holder of the mutex has a priority below the priority of
// 3715 			the task attempting to obtain the mutex then it will temporarily
// 3716 			inherit the priority of the task attempting to obtain the mutex. */
// 3717 			if( pxTCB->uxPriority < pxCurrentTCB->uxPriority )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_272  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
// 3718 			{
// 3719 				/* Adjust the mutex holder state to account for its new
// 3720 				priority.  Only reset the event list item value if the value is
// 3721 				not	being used for anything else. */
// 3722 				if( ( listGET_LIST_ITEM_VALUE( &( pxTCB->xEventListItem ) ) & taskEVENT_LIST_ITEM_VALUE_IN_USE ) == 0UL )
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        ES:[HL].7, ??prvAddCurrentTaskToDelayedList_273  ;; 5 cycles
        ; ------------------------------------- Block: 11 cycles
// 3723 				{
// 3724 					listSET_LIST_ITEM_VALUE( &( pxTCB->xEventListItem ), ( TickType_t ) configMAX_PRIORITIES - ( TickType_t ) pxCurrentTCB->uxPriority ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, #0x7           ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        POP       BC                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 31 cycles
// 3725 				}
// 3726 				else
// 3727 				{
// 3728 					mtCOVERAGE_TEST_MARKER();
// 3729 				}
// 3730 
// 3731 				/* If the task being modified is in the ready state it will need
// 3732 				to be moved into a new list. */
// 3733 				if( listIS_CONTAINED_WITHIN( &( pxReadyTasksLists[ pxTCB->uxPriority ] ), &( pxTCB->xStateListItem ) ) != pdFALSE )
??prvAddCurrentTaskToDelayedList_273:
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_274  ;; 4 cycles
        ; ------------------------------------- Block: 36 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_274:
        BNZ       ??prvAddCurrentTaskToDelayedList_275  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3734 				{
// 3735 					if( uxListRemove( &( pxTCB->xStateListItem ) ) == ( UBaseType_t ) 0 )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 3736 					{
// 3737 						taskRESET_READY_PRIORITY( pxTCB->uxPriority );
// 3738 					}
// 3739 					else
// 3740 					{
// 3741 						mtCOVERAGE_TEST_MARKER();
// 3742 					}
// 3743 
// 3744 					/* Inherit the priority before being moved into the new list. */
// 3745 					pxTCB->uxPriority = pxCurrentTCB->uxPriority;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 3746 					prvAddTaskToReadyList( pxTCB );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNH       ??prvAddCurrentTaskToDelayedList_276  ;; 4 cycles
        ; ------------------------------------- Block: 34 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 10 cycles
??prvAddCurrentTaskToDelayedList_276:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_272  ;; 3 cycles
        ; ------------------------------------- Block: 32 cycles
// 3747 				}
// 3748 				else
// 3749 				{
// 3750 					/* Just inherit the priority. */
// 3751 					pxTCB->uxPriority = pxCurrentTCB->uxPriority;
??prvAddCurrentTaskToDelayedList_275:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        ; ------------------------------------- Block: 17 cycles
// 3752 				}
// 3753 
// 3754 				traceTASK_PRIORITY_INHERIT( pxTCB, pxCurrentTCB->uxPriority );
// 3755 			}
// 3756 			else
// 3757 			{
// 3758 				mtCOVERAGE_TEST_MARKER();
// 3759 			}
// 3760 		}
// 3761 		else
// 3762 		{
// 3763 			mtCOVERAGE_TEST_MARKER();
// 3764 		}
// 3765 	}
??prvAddCurrentTaskToDelayedList_272:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock48
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 224 cycles
// 3766 
// 3767 #endif /* configUSE_MUTEXES */
// 3768 /*-----------------------------------------------------------*/
// 3769 
// 3770 #if ( configUSE_MUTEXES == 1 )
// 3771 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock49 Using cfiCommon0
          CFI Function _xTaskPriorityDisinherit
        CODE
// 3772 	BaseType_t xTaskPriorityDisinherit( TaskHandle_t const pxMutexHolder )
// 3773 	{
_xTaskPriorityDisinherit:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 3774 	TCB_t * const pxTCB = ( TCB_t * ) pxMutexHolder;
// 3775 	BaseType_t xReturn = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 3776 
// 3777 		if( pxMutexHolder != NULL )
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_277  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_277:
        SKNZ                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_278  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3778 		{
// 3779 			/* A task can only have an inherited priority if it holds the mutex.
// 3780 			If the mutex is held by a task then it cannot be given from an
// 3781 			interrupt, and if a mutex is given by the holding task then it must
// 3782 			be the running state task. */
// 3783 			configASSERT( pxTCB == pxCurrentTCB );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_279  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_279:
        BZ        ??prvAddCurrentTaskToDelayedList_280  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_281  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_281:
        BR        S:??prvAddCurrentTaskToDelayedList_281  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3784 
// 3785 			configASSERT( pxTCB->uxMutexesHeld );
??prvAddCurrentTaskToDelayedList_280:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x44          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_282  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_283  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_283:
        BR        S:??prvAddCurrentTaskToDelayedList_283  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3786 			( pxTCB->uxMutexesHeld )--;
??prvAddCurrentTaskToDelayedList_282:
        MOVW      HL, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x44          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 3787 
// 3788 			/* Has the holder of the mutex inherited the priority of another
// 3789 			task? */
// 3790 			if( pxTCB->uxPriority != pxTCB->uxBasePriority )
        DECW      DE                 ;; 1 cycle
        DECW      DE                 ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_278  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
// 3791 			{
// 3792 				/* Only disinherit if no other mutexes are held. */
// 3793 				if( pxTCB->uxMutexesHeld == ( UBaseType_t ) 0 )
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_278  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3794 				{
// 3795 					/* A task can only have an inherited priority if it holds
// 3796 					the mutex.  If the mutex is held by a task then it cannot be
// 3797 					given from an interrupt, and if a mutex is given by the
// 3798 					holding	task then it must be the running state task.  Remove
// 3799 					the	holding task from the ready	list. */
// 3800 					if( uxListRemove( &( pxTCB->xStateListItem ) ) == ( UBaseType_t ) 0 )
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 3801 					{
// 3802 						taskRESET_READY_PRIORITY( pxTCB->uxPriority );
// 3803 					}
// 3804 					else
// 3805 					{
// 3806 						mtCOVERAGE_TEST_MARKER();
// 3807 					}
// 3808 
// 3809 					/* Disinherit the priority before adding the task into the
// 3810 					new	ready list. */
// 3811 					traceTASK_PRIORITY_DISINHERIT( pxTCB, pxTCB->uxBasePriority );
// 3812 					pxTCB->uxPriority = pxTCB->uxBasePriority;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 3813 
// 3814 					/* Reset the event list item value.  It cannot be in use for
// 3815 					any other purpose if this task is running, and it must be
// 3816 					running to give back the mutex. */
// 3817 					listSET_LIST_ITEM_VALUE( &( pxTCB->xEventListItem ), ( TickType_t ) configMAX_PRIORITIES - ( TickType_t ) pxTCB->uxPriority ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
        MOVW      AX, #0x7           ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3818 					prvAddTaskToReadyList( pxTCB );
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_284  ;; 4 cycles
        ; ------------------------------------- Block: 55 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_284:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 3819 
// 3820 					/* Return true to indicate that a context switch is required.
// 3821 					This is only actually required in the corner case whereby
// 3822 					multiple mutexes were held and the mutexes were given back
// 3823 					in an order different to that in which they were taken.
// 3824 					If a context switch did not occur when the first mutex was
// 3825 					returned, even if a task was waiting on it, then a context
// 3826 					switch should occur when the last mutex is returned whether
// 3827 					a task is waiting on it or not. */
// 3828 					xReturn = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
          CFI EndBlock cfiBlock49
        ; ------------------------------------- Block: 27 cycles
// 3829 				}
// 3830 				else
// 3831 				{
// 3832 					mtCOVERAGE_TEST_MARKER();
// 3833 				}
// 3834 			}
// 3835 			else
// 3836 			{
// 3837 				mtCOVERAGE_TEST_MARKER();
// 3838 			}
// 3839 		}
// 3840 		else
// 3841 		{
// 3842 			mtCOVERAGE_TEST_MARKER();
// 3843 		}
// 3844 
// 3845 		return xReturn;
??prvAddCurrentTaskToDelayedList_278:
        REQUIRE ?Subroutine3
        ; // Fall through to label ?Subroutine3
// 3846 	}

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock50 Using cfiCommon0
          CFI NoFunction
          CFI CFA SP+10
        CODE
?Subroutine3:
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock50
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
// 3847 
// 3848 #endif /* configUSE_MUTEXES */
// 3849 /*-----------------------------------------------------------*/
// 3850 
// 3851 #if ( portCRITICAL_NESTING_IN_TCB == 1 )
// 3852 
// 3853 	void vTaskEnterCritical( void )
// 3854 	{
// 3855 		portDISABLE_INTERRUPTS();
// 3856 
// 3857 		if( xSchedulerRunning != pdFALSE )
// 3858 		{
// 3859 			( pxCurrentTCB->uxCriticalNesting )++;
// 3860 
// 3861 			/* This is not the interrupt safe version of the enter critical
// 3862 			function so	assert() if it is being called from an interrupt
// 3863 			context.  Only API functions that end in "FromISR" can be used in an
// 3864 			interrupt.  Only assert if the critical nesting count is 1 to
// 3865 			protect against recursive calls if the assert function also uses a
// 3866 			critical section. */
// 3867 			if( pxCurrentTCB->uxCriticalNesting == 1 )
// 3868 			{
// 3869 				portASSERT_IF_IN_ISR();
// 3870 			}
// 3871 		}
// 3872 		else
// 3873 		{
// 3874 			mtCOVERAGE_TEST_MARKER();
// 3875 		}
// 3876 	}
// 3877 
// 3878 #endif /* portCRITICAL_NESTING_IN_TCB */
// 3879 /*-----------------------------------------------------------*/
// 3880 
// 3881 #if ( portCRITICAL_NESTING_IN_TCB == 1 )
// 3882 
// 3883 	void vTaskExitCritical( void )
// 3884 	{
// 3885 		if( xSchedulerRunning != pdFALSE )
// 3886 		{
// 3887 			if( pxCurrentTCB->uxCriticalNesting > 0U )
// 3888 			{
// 3889 				( pxCurrentTCB->uxCriticalNesting )--;
// 3890 
// 3891 				if( pxCurrentTCB->uxCriticalNesting == 0U )
// 3892 				{
// 3893 					portENABLE_INTERRUPTS();
// 3894 				}
// 3895 				else
// 3896 				{
// 3897 					mtCOVERAGE_TEST_MARKER();
// 3898 				}
// 3899 			}
// 3900 			else
// 3901 			{
// 3902 				mtCOVERAGE_TEST_MARKER();
// 3903 			}
// 3904 		}
// 3905 		else
// 3906 		{
// 3907 			mtCOVERAGE_TEST_MARKER();
// 3908 		}
// 3909 	}
// 3910 
// 3911 #endif /* portCRITICAL_NESTING_IN_TCB */
// 3912 /*-----------------------------------------------------------*/
// 3913 
// 3914 #if ( ( configUSE_TRACE_FACILITY == 1 ) && ( configUSE_STATS_FORMATTING_FUNCTIONS > 0 ) )
// 3915 
// 3916 	static char *prvWriteNameToBuffer( char *pcBuffer, const char *pcTaskName )
// 3917 	{
// 3918 	size_t x;
// 3919 
// 3920 		/* Start by copying the entire string. */
// 3921 		strcpy( pcBuffer, pcTaskName );
// 3922 
// 3923 		/* Pad the end of the string with spaces to ensure columns line up when
// 3924 		printed out. */
// 3925 		for( x = strlen( pcBuffer ); x < ( size_t ) ( configMAX_TASK_NAME_LEN - 1 ); x++ )
// 3926 		{
// 3927 			pcBuffer[ x ] = ' ';
// 3928 		}
// 3929 
// 3930 		/* Terminate. */
// 3931 		pcBuffer[ x ] = 0x00;
// 3932 
// 3933 		/* Return the new end of string. */
// 3934 		return &( pcBuffer[ x ] );
// 3935 	}
// 3936 
// 3937 #endif /* ( configUSE_TRACE_FACILITY == 1 ) && ( configUSE_STATS_FORMATTING_FUNCTIONS > 0 ) */
// 3938 /*-----------------------------------------------------------*/
// 3939 
// 3940 #if ( ( configUSE_TRACE_FACILITY == 1 ) && ( configUSE_STATS_FORMATTING_FUNCTIONS > 0 ) )
// 3941 
// 3942 	void vTaskList( char * pcWriteBuffer )
// 3943 	{
// 3944 	TaskStatus_t *pxTaskStatusArray;
// 3945 	volatile UBaseType_t uxArraySize, x;
// 3946 	char cStatus;
// 3947 
// 3948 		/*
// 3949 		 * PLEASE NOTE:
// 3950 		 *
// 3951 		 * This function is provided for convenience only, and is used by many
// 3952 		 * of the demo applications.  Do not consider it to be part of the
// 3953 		 * scheduler.
// 3954 		 *
// 3955 		 * vTaskList() calls uxTaskGetSystemState(), then formats part of the
// 3956 		 * uxTaskGetSystemState() output into a human readable table that
// 3957 		 * displays task names, states and stack usage.
// 3958 		 *
// 3959 		 * vTaskList() has a dependency on the sprintf() C library function that
// 3960 		 * might bloat the code size, use a lot of stack, and provide different
// 3961 		 * results on different platforms.  An alternative, tiny, third party,
// 3962 		 * and limited functionality implementation of sprintf() is provided in
// 3963 		 * many of the FreeRTOS/Demo sub-directories in a file called
// 3964 		 * printf-stdarg.c (note printf-stdarg.c does not provide a full
// 3965 		 * snprintf() implementation!).
// 3966 		 *
// 3967 		 * It is recommended that production systems call uxTaskGetSystemState()
// 3968 		 * directly to get access to raw stats data, rather than indirectly
// 3969 		 * through a call to vTaskList().
// 3970 		 */
// 3971 
// 3972 
// 3973 		/* Make sure the write buffer does not contain a string. */
// 3974 		*pcWriteBuffer = 0x00;
// 3975 
// 3976 		/* Take a snapshot of the number of tasks in case it changes while this
// 3977 		function is executing. */
// 3978 		uxArraySize = uxCurrentNumberOfTasks;
// 3979 
// 3980 		/* Allocate an array index for each task.  NOTE!  if
// 3981 		configSUPPORT_DYNAMIC_ALLOCATION is set to 0 then pvPortMalloc() will
// 3982 		equate to NULL. */
// 3983 		pxTaskStatusArray = pvPortMalloc( uxCurrentNumberOfTasks * sizeof( TaskStatus_t ) );
// 3984 
// 3985 		if( pxTaskStatusArray != NULL )
// 3986 		{
// 3987 			/* Generate the (binary) data. */
// 3988 			uxArraySize = uxTaskGetSystemState( pxTaskStatusArray, uxArraySize, NULL );
// 3989 
// 3990 			/* Create a human readable table from the binary data. */
// 3991 			for( x = 0; x < uxArraySize; x++ )
// 3992 			{
// 3993 				switch( pxTaskStatusArray[ x ].eCurrentState )
// 3994 				{
// 3995 					case eReady:		cStatus = tskREADY_CHAR;
// 3996 										break;
// 3997 
// 3998 					case eBlocked:		cStatus = tskBLOCKED_CHAR;
// 3999 										break;
// 4000 
// 4001 					case eSuspended:	cStatus = tskSUSPENDED_CHAR;
// 4002 										break;
// 4003 
// 4004 					case eDeleted:		cStatus = tskDELETED_CHAR;
// 4005 										break;
// 4006 
// 4007 					default:			/* Should not get here, but it is included
// 4008 										to prevent static checking errors. */
// 4009 										cStatus = 0x00;
// 4010 										break;
// 4011 				}
// 4012 
// 4013 				/* Write the task name to the string, padding with spaces so it
// 4014 				can be printed in tabular form more easily. */
// 4015 				pcWriteBuffer = prvWriteNameToBuffer( pcWriteBuffer, pxTaskStatusArray[ x ].pcTaskName );
// 4016 
// 4017 				/* Write the rest of the string. */
// 4018 				sprintf( pcWriteBuffer, "\t%c\t%u\t%u\t%u\r\n", cStatus, ( unsigned int ) pxTaskStatusArray[ x ].uxCurrentPriority, ( unsigned int ) pxTaskStatusArray[ x ].usStackHighWaterMark, ( unsigned int ) pxTaskStatusArray[ x ].xTaskNumber );
// 4019 				pcWriteBuffer += strlen( pcWriteBuffer );
// 4020 			}
// 4021 
// 4022 			/* Free the array again.  NOTE!  If configSUPPORT_DYNAMIC_ALLOCATION
// 4023 			is 0 then vPortFree() will be #defined to nothing. */
// 4024 			vPortFree( pxTaskStatusArray );
// 4025 		}
// 4026 		else
// 4027 		{
// 4028 			mtCOVERAGE_TEST_MARKER();
// 4029 		}
// 4030 	}
// 4031 
// 4032 #endif /* ( ( configUSE_TRACE_FACILITY == 1 ) && ( configUSE_STATS_FORMATTING_FUNCTIONS > 0 ) ) */
// 4033 /*----------------------------------------------------------*/
// 4034 
// 4035 #if ( ( configGENERATE_RUN_TIME_STATS == 1 ) && ( configUSE_STATS_FORMATTING_FUNCTIONS > 0 ) )
// 4036 
// 4037 	void vTaskGetRunTimeStats( char *pcWriteBuffer )
// 4038 	{
// 4039 	TaskStatus_t *pxTaskStatusArray;
// 4040 	volatile UBaseType_t uxArraySize, x;
// 4041 	uint32_t ulTotalTime, ulStatsAsPercentage;
// 4042 
// 4043 		#if( configUSE_TRACE_FACILITY != 1 )
// 4044 		{
// 4045 			#error configUSE_TRACE_FACILITY must also be set to 1 in FreeRTOSConfig.h to use vTaskGetRunTimeStats().
// 4046 		}
// 4047 		#endif
// 4048 
// 4049 		/*
// 4050 		 * PLEASE NOTE:
// 4051 		 *
// 4052 		 * This function is provided for convenience only, and is used by many
// 4053 		 * of the demo applications.  Do not consider it to be part of the
// 4054 		 * scheduler.
// 4055 		 *
// 4056 		 * vTaskGetRunTimeStats() calls uxTaskGetSystemState(), then formats part
// 4057 		 * of the uxTaskGetSystemState() output into a human readable table that
// 4058 		 * displays the amount of time each task has spent in the Running state
// 4059 		 * in both absolute and percentage terms.
// 4060 		 *
// 4061 		 * vTaskGetRunTimeStats() has a dependency on the sprintf() C library
// 4062 		 * function that might bloat the code size, use a lot of stack, and
// 4063 		 * provide different results on different platforms.  An alternative,
// 4064 		 * tiny, third party, and limited functionality implementation of
// 4065 		 * sprintf() is provided in many of the FreeRTOS/Demo sub-directories in
// 4066 		 * a file called printf-stdarg.c (note printf-stdarg.c does not provide
// 4067 		 * a full snprintf() implementation!).
// 4068 		 *
// 4069 		 * It is recommended that production systems call uxTaskGetSystemState()
// 4070 		 * directly to get access to raw stats data, rather than indirectly
// 4071 		 * through a call to vTaskGetRunTimeStats().
// 4072 		 */
// 4073 
// 4074 		/* Make sure the write buffer does not contain a string. */
// 4075 		*pcWriteBuffer = 0x00;
// 4076 
// 4077 		/* Take a snapshot of the number of tasks in case it changes while this
// 4078 		function is executing. */
// 4079 		uxArraySize = uxCurrentNumberOfTasks;
// 4080 
// 4081 		/* Allocate an array index for each task.  NOTE!  If
// 4082 		configSUPPORT_DYNAMIC_ALLOCATION is set to 0 then pvPortMalloc() will
// 4083 		equate to NULL. */
// 4084 		pxTaskStatusArray = pvPortMalloc( uxCurrentNumberOfTasks * sizeof( TaskStatus_t ) );
// 4085 
// 4086 		if( pxTaskStatusArray != NULL )
// 4087 		{
// 4088 			/* Generate the (binary) data. */
// 4089 			uxArraySize = uxTaskGetSystemState( pxTaskStatusArray, uxArraySize, &ulTotalTime );
// 4090 
// 4091 			/* For percentage calculations. */
// 4092 			ulTotalTime /= 100UL;
// 4093 
// 4094 			/* Avoid divide by zero errors. */
// 4095 			if( ulTotalTime > 0 )
// 4096 			{
// 4097 				/* Create a human readable table from the binary data. */
// 4098 				for( x = 0; x < uxArraySize; x++ )
// 4099 				{
// 4100 					/* What percentage of the total run time has the task used?
// 4101 					This will always be rounded down to the nearest integer.
// 4102 					ulTotalRunTimeDiv100 has already been divided by 100. */
// 4103 					ulStatsAsPercentage = pxTaskStatusArray[ x ].ulRunTimeCounter / ulTotalTime;
// 4104 
// 4105 					/* Write the task name to the string, padding with
// 4106 					spaces so it can be printed in tabular form more
// 4107 					easily. */
// 4108 					pcWriteBuffer = prvWriteNameToBuffer( pcWriteBuffer, pxTaskStatusArray[ x ].pcTaskName );
// 4109 
// 4110 					if( ulStatsAsPercentage > 0UL )
// 4111 					{
// 4112 						#ifdef portLU_PRINTF_SPECIFIER_REQUIRED
// 4113 						{
// 4114 							sprintf( pcWriteBuffer, "\t%lu\t\t%lu%%\r\n", pxTaskStatusArray[ x ].ulRunTimeCounter, ulStatsAsPercentage );
// 4115 						}
// 4116 						#else
// 4117 						{
// 4118 							/* sizeof( int ) == sizeof( long ) so a smaller
// 4119 							printf() library can be used. */
// 4120 							sprintf( pcWriteBuffer, "\t%u\t\t%u%%\r\n", ( unsigned int ) pxTaskStatusArray[ x ].ulRunTimeCounter, ( unsigned int ) ulStatsAsPercentage );
// 4121 						}
// 4122 						#endif
// 4123 					}
// 4124 					else
// 4125 					{
// 4126 						/* If the percentage is zero here then the task has
// 4127 						consumed less than 1% of the total run time. */
// 4128 						#ifdef portLU_PRINTF_SPECIFIER_REQUIRED
// 4129 						{
// 4130 							sprintf( pcWriteBuffer, "\t%lu\t\t<1%%\r\n", pxTaskStatusArray[ x ].ulRunTimeCounter );
// 4131 						}
// 4132 						#else
// 4133 						{
// 4134 							/* sizeof( int ) == sizeof( long ) so a smaller
// 4135 							printf() library can be used. */
// 4136 							sprintf( pcWriteBuffer, "\t%u\t\t<1%%\r\n", ( unsigned int ) pxTaskStatusArray[ x ].ulRunTimeCounter );
// 4137 						}
// 4138 						#endif
// 4139 					}
// 4140 
// 4141 					pcWriteBuffer += strlen( pcWriteBuffer );
// 4142 				}
// 4143 			}
// 4144 			else
// 4145 			{
// 4146 				mtCOVERAGE_TEST_MARKER();
// 4147 			}
// 4148 
// 4149 			/* Free the array again.  NOTE!  If configSUPPORT_DYNAMIC_ALLOCATION
// 4150 			is 0 then vPortFree() will be #defined to nothing. */
// 4151 			vPortFree( pxTaskStatusArray );
// 4152 		}
// 4153 		else
// 4154 		{
// 4155 			mtCOVERAGE_TEST_MARKER();
// 4156 		}
// 4157 	}
// 4158 
// 4159 #endif /* ( ( configGENERATE_RUN_TIME_STATS == 1 ) && ( configUSE_STATS_FORMATTING_FUNCTIONS > 0 ) ) */
// 4160 /*-----------------------------------------------------------*/
// 4161 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock51 Using cfiCommon1
          CFI Function _uxTaskResetEventItemValue
          CFI NoCalls
        CODE
// 4162 TickType_t uxTaskResetEventItemValue( void )
// 4163 {
_uxTaskResetEventItemValue:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 8
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
// 4164 TickType_t uxReturn;
// 4165 
// 4166 	uxReturn = listGET_LIST_ITEM_VALUE( &( pxCurrentTCB->xEventListItem ) );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x18]   ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x1A]   ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 4167 
// 4168 	/* Reset the event list item to its normal value - so it can be used with
// 4169 	queues and semaphores. */
// 4170 	listSET_LIST_ITEM_VALUE( &( pxCurrentTCB->xEventListItem ), ( ( TickType_t ) configMAX_PRIORITIES - ( TickType_t ) pxCurrentTCB->uxPriority ) ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, #0x7           ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 4171 
// 4172 	return uxReturn;
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock51
        ; ------------------------------------- Block: 64 cycles
        ; ------------------------------------- Total: 64 cycles
// 4173 }
// 4174 /*-----------------------------------------------------------*/
// 4175 
// 4176 #if ( configUSE_MUTEXES == 1 )
// 4177 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock52 Using cfiCommon3
          CFI Function _pvTaskIncrementMutexHeldCount
          CFI NoCalls
        CODE
// 4178 	void *pvTaskIncrementMutexHeldCount( void )
// 4179 	{
_pvTaskIncrementMutexHeldCount:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 4180 		/* If xSemaphoreCreateMutex() is called before any tasks have been created
// 4181 		then pxCurrentTCB will be NULL. */
// 4182 		if( pxCurrentTCB != NULL )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_285  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_285:
        BZ        ??prvAddCurrentTaskToDelayedList_286  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 4183 		{
// 4184 			( pxCurrentTCB->uxMutexesHeld )++;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x44          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x44          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        ; ------------------------------------- Block: 22 cycles
// 4185 		}
// 4186 
// 4187 		return pxCurrentTCB;
??prvAddCurrentTaskToDelayedList_286:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        POP       DE                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock52
        ; ------------------------------------- Block: 16 cycles
        ; ------------------------------------- Total: 57 cycles
// 4188 	}
// 4189 
// 4190 #endif /* configUSE_MUTEXES */
// 4191 /*-----------------------------------------------------------*/
// 4192 
// 4193 #if( configUSE_TASK_NOTIFICATIONS == 1 )
// 4194 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock53 Using cfiCommon1
          CFI Function _ulTaskNotifyTake
        CODE
// 4195 	uint32_t ulTaskNotifyTake( BaseType_t xClearCountOnExit, TickType_t xTicksToWait )
// 4196 	{
_ulTaskNotifyTake:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
// 4197 	uint32_t ulReturn;
// 4198 
// 4199 		taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_287  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_287:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 4200 		{
// 4201 			/* Only block if the notification count is not already non-zero. */
// 4202 			if( pxCurrentTCB->ulNotifiedValue == 0UL )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_288  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
// 4203 			{
// 4204 				/* Mark this task as waiting for a notification. */
// 4205 				pxCurrentTCB->ucNotifyState = taskWAITING_NOTIFICATION;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES:[HL+0x4A], A    ;; 2 cycles
// 4206 
// 4207 				if( xTicksToWait > ( TickType_t ) 0 )
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_288  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 4208 				{
// 4209 					prvAddCurrentTaskToDelayedList( xTicksToWait, pdTRUE );
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _prvAddCurrentTaskToDelayedList
        CALL      F:_prvAddCurrentTaskToDelayedList  ;; 3 cycles
// 4210 					traceTASK_NOTIFY_TAKE_BLOCK();
// 4211 
// 4212 					/* All ports are written to allow a yield in a critical
// 4213 					section (some will yield immediately, others wait until the
// 4214 					critical section exits) - but it is not something that
// 4215 					application code should ever do. */
// 4216 					portYIELD_WITHIN_API();
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 11 cycles
// 4217 				}
// 4218 				else
// 4219 				{
// 4220 					mtCOVERAGE_TEST_MARKER();
// 4221 				}
// 4222 			}
// 4223 			else
// 4224 			{
// 4225 				mtCOVERAGE_TEST_MARKER();
// 4226 			}
// 4227 		}
// 4228 		taskEXIT_CRITICAL();
??prvAddCurrentTaskToDelayedList_288:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_289  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_289  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 4229 
// 4230 		taskENTER_CRITICAL();
??prvAddCurrentTaskToDelayedList_289:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_290  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_290:
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 4231 		{
// 4232 			traceTASK_NOTIFY_TAKE();
// 4233 			ulReturn = pxCurrentTCB->ulNotifiedValue;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 4234 
// 4235 			if( ulReturn != 0UL )
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_291  ;; 4 cycles
        ; ------------------------------------- Block: 30 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
// 4236 			{
// 4237 				if( xClearCountOnExit != pdFALSE )
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_292  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 4238 				{
// 4239 					pxCurrentTCB->ulNotifiedValue = 0UL;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL+0x46], AX   ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_293  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
// 4240 				}
// 4241 				else
// 4242 				{
// 4243 					pxCurrentTCB->ulNotifiedValue = ulReturn - 1;
??prvAddCurrentTaskToDelayedList_292:
        MOVW      AX, [SP]           ;; 1 cycle
        SUBW      AX, #0x1           ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      ES:[HL+0x46], AX   ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 20 cycles
??prvAddCurrentTaskToDelayedList_293:
        MOVW      ES:[HL+0x48], AX   ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 4244 				}
// 4245 			}
// 4246 			else
// 4247 			{
// 4248 				mtCOVERAGE_TEST_MARKER();
// 4249 			}
// 4250 
// 4251 			pxCurrentTCB->ucNotifyState = taskNOT_WAITING_NOTIFICATION;
??prvAddCurrentTaskToDelayedList_291:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x4A], A    ;; 2 cycles
// 4252 		}
// 4253 		taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_294  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_294  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 4254 
// 4255 		return ulReturn;
??prvAddCurrentTaskToDelayedList_294:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        BR        F:?Subroutine2     ;; 3 cycles
          CFI EndBlock cfiBlock53
        ; ------------------------------------- Block: 5 cycles
        ; ------------------------------------- Total: 220 cycles
// 4256 	}
// 4257 
// 4258 #endif /* configUSE_TASK_NOTIFICATIONS */
// 4259 /*-----------------------------------------------------------*/
// 4260 
// 4261 #if( configUSE_TASK_NOTIFICATIONS == 1 )
// 4262 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock54 Using cfiCommon1
          CFI Function _xTaskNotifyWait
        CODE
// 4263 	BaseType_t xTaskNotifyWait( uint32_t ulBitsToClearOnEntry, uint32_t ulBitsToClearOnExit, uint32_t *pulNotificationValue, TickType_t xTicksToWait )
// 4264 	{
_xTaskNotifyWait:
        ; * Stack frame (at entry) *
        ; Param size: 12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 4265 	BaseType_t xReturn;
// 4266 
// 4267 		taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_295  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_295:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 4268 		{
// 4269 			/* Only block if a notification is not already pending. */
// 4270 			if( pxCurrentTCB->ucNotifyState != taskNOTIFICATION_RECEIVED )
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0x2            ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_296  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
// 4271 			{
// 4272 				/* Clear bits in the task's notification value as bits may get
// 4273 				set	by the notifying task or interrupt.  This can be used to
// 4274 				clear the value to zero. */
// 4275 				pxCurrentTCB->ulNotifiedValue &= ~ulBitsToClearOnEntry;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall ?L_NOT_L03
        CALL      N:?L_NOT_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 4276 
// 4277 				/* Mark this task as waiting for a notification. */
// 4278 				pxCurrentTCB->ucNotifyState = taskWAITING_NOTIFICATION;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES:[HL+0x4A], A    ;; 2 cycles
// 4279 
// 4280 				if( xTicksToWait > ( TickType_t ) 0 )
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_296  ;; 4 cycles
        ; ------------------------------------- Block: 52 cycles
// 4281 				{
// 4282 					prvAddCurrentTaskToDelayedList( xTicksToWait, pdTRUE );
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
          CFI FunCall _prvAddCurrentTaskToDelayedList
        CALL      F:_prvAddCurrentTaskToDelayedList  ;; 3 cycles
// 4283 					traceTASK_NOTIFY_WAIT_BLOCK();
// 4284 
// 4285 					/* All ports are written to allow a yield in a critical
// 4286 					section (some will yield immediately, others wait until the
// 4287 					critical section exits) - but it is not something that
// 4288 					application code should ever do. */
// 4289 					portYIELD_WITHIN_API();
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 11 cycles
// 4290 				}
// 4291 				else
// 4292 				{
// 4293 					mtCOVERAGE_TEST_MARKER();
// 4294 				}
// 4295 			}
// 4296 			else
// 4297 			{
// 4298 				mtCOVERAGE_TEST_MARKER();
// 4299 			}
// 4300 		}
// 4301 		taskEXIT_CRITICAL();
??prvAddCurrentTaskToDelayedList_296:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_297  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_297  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 4302 
// 4303 		taskENTER_CRITICAL();
??prvAddCurrentTaskToDelayedList_297:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_298  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_298:
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 4304 		{
// 4305 			traceTASK_NOTIFY_WAIT();
// 4306 
// 4307 			if( pulNotificationValue != NULL )
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_299  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_299:
        BZ        ??prvAddCurrentTaskToDelayedList_300  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 4308 			{
// 4309 				/* Output the current notification value, which may or may not
// 4310 				have changed. */
// 4311 				*pulNotificationValue = pxCurrentTCB->ulNotifiedValue;
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 30 cycles
// 4312 			}
// 4313 
// 4314 			/* If ucNotifyValue is set then either the task never entered the
// 4315 			blocked state (because a notification was already pending) or the
// 4316 			task unblocked because of a notification.  Otherwise the task
// 4317 			unblocked because of a timeout. */
// 4318 			if( pxCurrentTCB->ucNotifyState == taskWAITING_NOTIFICATION )
??prvAddCurrentTaskToDelayedList_300:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_301  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 4319 			{
// 4320 				/* A notification was not received. */
// 4321 				xReturn = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_302  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 4322 			}
// 4323 			else
// 4324 			{
// 4325 				/* A notification was already pending or a notification was
// 4326 				received while the task was waiting. */
// 4327 				pxCurrentTCB->ulNotifiedValue &= ~ulBitsToClearOnExit;
??prvAddCurrentTaskToDelayedList_301:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
          CFI FunCall ?L_NOT_L03
        CALL      N:?L_NOT_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 4328 				xReturn = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        ; ------------------------------------- Block: 37 cycles
// 4329 			}
// 4330 
// 4331 			pxCurrentTCB->ucNotifyState = taskNOT_WAITING_NOTIFICATION;
??prvAddCurrentTaskToDelayedList_302:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x4A], A    ;; 2 cycles
// 4332 		}
// 4333 		taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_303  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_303  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 4334 
// 4335 		return xReturn;
??prvAddCurrentTaskToDelayedList_303:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock54
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 290 cycles
// 4336 	}
// 4337 
// 4338 #endif /* configUSE_TASK_NOTIFICATIONS */
// 4339 /*-----------------------------------------------------------*/
// 4340 
// 4341 #if( configUSE_TASK_NOTIFICATIONS == 1 )
// 4342 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock55 Using cfiCommon0
          CFI Function _xTaskGenericNotify
        CODE
// 4343 	BaseType_t xTaskGenericNotify( TaskHandle_t xTaskToNotify, uint32_t ulValue, eNotifyAction eAction, uint32_t *pulPreviousNotificationValue )
// 4344 	{
_xTaskGenericNotify:
        ; * Stack frame (at entry) *
        ; Param size: 8
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, X               ;; 1 cycle
        MOV       D, A               ;; 1 cycle
// 4345 	TCB_t * pxTCB;
// 4346 	BaseType_t xReturn = pdPASS;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 4347 	uint8_t ucOriginalNotifyState;
// 4348 
// 4349 		configASSERT( xTaskToNotify );
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_304  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_304:
        BNZ       ??prvAddCurrentTaskToDelayedList_305  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_306  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_306:
        BR        S:??prvAddCurrentTaskToDelayedList_306  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4350 		pxTCB = ( TCB_t * ) xTaskToNotify;
// 4351 
// 4352 		taskENTER_CRITICAL();
??prvAddCurrentTaskToDelayedList_305:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_307  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_307:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 4353 		{
// 4354 			if( pulPreviousNotificationValue != NULL )
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_308  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_308:
        BZ        ??prvAddCurrentTaskToDelayedList_309  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 4355 			{
// 4356 				*pulPreviousNotificationValue = pxTCB->ulNotifiedValue;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 23 cycles
??prvAddCurrentTaskToDelayedList_309:
        MOV       A, D               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
// 4357 			}
// 4358 
// 4359 			ucOriginalNotifyState = pxTCB->ucNotifyState;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       D, A               ;; 1 cycle
// 4360 
// 4361 			pxTCB->ucNotifyState = taskNOTIFICATION_RECEIVED;
        MOV       A, #0x2            ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 4362 
// 4363 			switch( eAction )
        MOV       A, B               ;; 1 cycle
        DEC       A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_310  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_311  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_312  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_313  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_314  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4364 			{
// 4365 				case eSetBits	:
// 4366 					pxTCB->ulNotifiedValue |= ulValue;
??prvAddCurrentTaskToDelayedList_310:
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 4367 					break;
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        BR        S:??prvAddCurrentTaskToDelayedList_314  ;; 3 cycles
        ; ------------------------------------- Block: 24 cycles
// 4368 
// 4369 				case eIncrement	:
// 4370 					( pxTCB->ulNotifiedValue )++;
??prvAddCurrentTaskToDelayedList_311:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_315  ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
// 4371 					break;
// 4372 
// 4373 				case eSetValueWithOverwrite	:
// 4374 					pxTCB->ulNotifiedValue = ulValue;
// 4375 					break;
// 4376 
// 4377 				case eSetValueWithoutOverwrite :
// 4378 					if( ucOriginalNotifyState != taskNOTIFICATION_RECEIVED )
??prvAddCurrentTaskToDelayedList_313:
        MOV       A, D               ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_312  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        R:??prvAddCurrentTaskToDelayedList_316  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 4379 					{
// 4380 						pxTCB->ulNotifiedValue = ulValue;
??prvAddCurrentTaskToDelayedList_312:
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        ; ------------------------------------- Block: 10 cycles
??prvAddCurrentTaskToDelayedList_315:
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 5 cycles
// 4381 					}
// 4382 					else
// 4383 					{
// 4384 						/* The value could not be written to the task. */
// 4385 						xReturn = pdFAIL;
// 4386 					}
// 4387 					break;
// 4388 
// 4389 				case eNoAction:
// 4390 					/* The task is being notified without its notify value being
// 4391 					updated. */
// 4392 					break;
// 4393 			}
// 4394 
// 4395 			traceTASK_NOTIFY();
// 4396 
// 4397 			/* If the task is in the blocked state specifically to wait for a
// 4398 			notification then unblock it now. */
// 4399 			if( ucOriginalNotifyState == taskWAITING_NOTIFICATION )
??prvAddCurrentTaskToDelayedList_314:
        MOV       A, D               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_316  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 4400 			{
// 4401 				( void ) uxListRemove( &( pxTCB->xStateListItem ) );
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 4402 				prvAddTaskToReadyList( pxTCB );
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_317  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_317:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 4403 
// 4404 				/* The task should not have been on an event list. */
// 4405 				configASSERT( listLIST_ITEM_CONTAINER( &( pxTCB->xEventListItem ) ) == NULL );
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_318  ;; 4 cycles
        ; ------------------------------------- Block: 42 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_318:
        BZ        ??prvAddCurrentTaskToDelayedList_319  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_320  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_320:
        BR        S:??prvAddCurrentTaskToDelayedList_320  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4406 
// 4407 				#if( configUSE_TICKLESS_IDLE != 0 )
// 4408 				{
// 4409 					/* If a task is blocked waiting for a notification then
// 4410 					xNextTaskUnblockTime might be set to the blocked task's time
// 4411 					out time.  If the task is unblocked for a reason other than
// 4412 					a timeout xNextTaskUnblockTime is normally left unchanged,
// 4413 					because it will automatically get reset to a new value when
// 4414 					the tick count equals xNextTaskUnblockTime.  However if
// 4415 					tickless idling is used it might be more important to enter
// 4416 					sleep mode at the earliest possible time - so reset
// 4417 					xNextTaskUnblockTime here to ensure it is updated at the
// 4418 					earliest possible time. */
// 4419 					prvResetNextTaskUnblockTime();
// 4420 				}
// 4421 				#endif
// 4422 
// 4423 				if( pxTCB->uxPriority > pxCurrentTCB->uxPriority )
??prvAddCurrentTaskToDelayedList_319:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        CMPW      AX, DE             ;; 1 cycle
        SKNC                         ;; 1 cycle
        ; ------------------------------------- Block: 18 cycles
// 4424 				{
// 4425 					/* The notified task has a priority above the currently
// 4426 					executing task so a yield is required. */
// 4427 					taskYIELD_IF_USING_PREEMPTION();
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 4428 				}
// 4429 				else
// 4430 				{
// 4431 					mtCOVERAGE_TEST_MARKER();
// 4432 				}
// 4433 			}
// 4434 			else
// 4435 			{
// 4436 				mtCOVERAGE_TEST_MARKER();
// 4437 			}
// 4438 		}
// 4439 		taskEXIT_CRITICAL();
??prvAddCurrentTaskToDelayedList_316:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_321  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_321  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 4440 
// 4441 		return xReturn;
??prvAddCurrentTaskToDelayedList_321:
        BR        F:?Subroutine3     ;; 3 cycles
          CFI EndBlock cfiBlock55
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 357 cycles
// 4442 	}
// 4443 
// 4444 #endif /* configUSE_TASK_NOTIFICATIONS */
// 4445 /*-----------------------------------------------------------*/
// 4446 
// 4447 #if( configUSE_TASK_NOTIFICATIONS == 1 )
// 4448 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock56 Using cfiCommon0
          CFI Function _xTaskGenericNotifyFromISR
        CODE
// 4449 	BaseType_t xTaskGenericNotifyFromISR( TaskHandle_t xTaskToNotify, uint32_t ulValue, eNotifyAction eAction, uint32_t *pulPreviousNotificationValue, BaseType_t *pxHigherPriorityTaskWoken )
// 4450 	{
_xTaskGenericNotifyFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 12
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        MOV       D, A               ;; 1 cycle
// 4451 	TCB_t * pxTCB;
// 4452 	uint8_t ucOriginalNotifyState;
// 4453 	BaseType_t xReturn = pdPASS;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 4454 	UBaseType_t uxSavedInterruptStatus;
// 4455 
// 4456 		configASSERT( xTaskToNotify );
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_322  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_322:
        BNZ       ??prvAddCurrentTaskToDelayedList_323  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_324  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_324:
        BR        S:??prvAddCurrentTaskToDelayedList_324  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4457 
// 4458 		/* RTOS ports that support interrupt nesting have the concept of a
// 4459 		maximum	system call (or maximum API call) interrupt priority.
// 4460 		Interrupts that are	above the maximum system call priority are keep
// 4461 		permanently enabled, even when the RTOS kernel is in a critical section,
// 4462 		but cannot make any calls to FreeRTOS API functions.  If configASSERT()
// 4463 		is defined in FreeRTOSConfig.h then
// 4464 		portASSERT_IF_INTERRUPT_PRIORITY_INVALID() will result in an assertion
// 4465 		failure if a FreeRTOS API function is called from an interrupt that has
// 4466 		been assigned a priority above the configured maximum system call
// 4467 		priority.  Only FreeRTOS functions that end in FromISR can be called
// 4468 		from interrupts	that have been assigned a priority at or (logically)
// 4469 		below the maximum system call interrupt priority.  FreeRTOS maintains a
// 4470 		separate interrupt safe API to ensure interrupt entry is as fast and as
// 4471 		simple as possible.  More information (albeit Cortex-M specific) is
// 4472 		provided on the following link:
// 4473 		http://www.freertos.org/RTOS-Cortex-M3-M4.html */
// 4474 		portASSERT_IF_INTERRUPT_PRIORITY_INVALID();
// 4475 
// 4476 		pxTCB = ( TCB_t * ) xTaskToNotify;
// 4477 
// 4478 		uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
??prvAddCurrentTaskToDelayedList_323:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 4479 		{
// 4480 			if( pulPreviousNotificationValue != NULL )
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_325  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_325:
        BZ        ??prvAddCurrentTaskToDelayedList_326  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 4481 			{
// 4482 				*pulPreviousNotificationValue = pxTCB->ulNotifiedValue;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 23 cycles
??prvAddCurrentTaskToDelayedList_326:
        MOV       A, D               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
// 4483 			}
// 4484 
// 4485 			ucOriginalNotifyState = pxTCB->ucNotifyState;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       D, A               ;; 1 cycle
// 4486 			pxTCB->ucNotifyState = taskNOTIFICATION_RECEIVED;
        MOV       A, #0x2            ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 4487 
// 4488 			switch( eAction )
        MOV       A, B               ;; 1 cycle
        DEC       A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_327  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_328  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_329  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_330  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_331  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4489 			{
// 4490 				case eSetBits	:
// 4491 					pxTCB->ulNotifiedValue |= ulValue;
??prvAddCurrentTaskToDelayedList_327:
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 4492 					break;
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        BR        S:??prvAddCurrentTaskToDelayedList_331  ;; 3 cycles
        ; ------------------------------------- Block: 24 cycles
// 4493 
// 4494 				case eIncrement	:
// 4495 					( pxTCB->ulNotifiedValue )++;
??prvAddCurrentTaskToDelayedList_328:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_332  ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
// 4496 					break;
// 4497 
// 4498 				case eSetValueWithOverwrite	:
// 4499 					pxTCB->ulNotifiedValue = ulValue;
// 4500 					break;
// 4501 
// 4502 				case eSetValueWithoutOverwrite :
// 4503 					if( ucOriginalNotifyState != taskNOTIFICATION_RECEIVED )
??prvAddCurrentTaskToDelayedList_330:
        MOV       A, D               ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_329  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        R:??prvAddCurrentTaskToDelayedList_333  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 4504 					{
// 4505 						pxTCB->ulNotifiedValue = ulValue;
??prvAddCurrentTaskToDelayedList_329:
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        ; ------------------------------------- Block: 10 cycles
??prvAddCurrentTaskToDelayedList_332:
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 5 cycles
// 4506 					}
// 4507 					else
// 4508 					{
// 4509 						/* The value could not be written to the task. */
// 4510 						xReturn = pdFAIL;
// 4511 					}
// 4512 					break;
// 4513 
// 4514 				case eNoAction :
// 4515 					/* The task is being notified without its notify value being
// 4516 					updated. */
// 4517 					break;
// 4518 			}
// 4519 
// 4520 			traceTASK_NOTIFY_FROM_ISR();
// 4521 
// 4522 			/* If the task is in the blocked state specifically to wait for a
// 4523 			notification then unblock it now. */
// 4524 			if( ucOriginalNotifyState == taskWAITING_NOTIFICATION )
??prvAddCurrentTaskToDelayedList_331:
        MOV       A, D               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_333  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 4525 			{
// 4526 				/* The task should not have been on an event list. */
// 4527 				configASSERT( listLIST_ITEM_CONTAINER( &( pxTCB->xEventListItem ) ) == NULL );
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_334  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_334:
        BZ        ??prvAddCurrentTaskToDelayedList_335  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_336  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_336:
        BR        S:??prvAddCurrentTaskToDelayedList_336  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4528 
// 4529 				if( uxSchedulerSuspended == ( UBaseType_t ) pdFALSE )
??prvAddCurrentTaskToDelayedList_335:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_337  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 4530 				{
// 4531 					( void ) uxListRemove( &( pxTCB->xStateListItem ) );
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 4532 					prvAddTaskToReadyList( pxTCB );
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_338  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_338:
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        BR        S:??prvAddCurrentTaskToDelayedList_339  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 4533 				}
// 4534 				else
// 4535 				{
// 4536 					/* The delayed and ready lists cannot be accessed, so hold
// 4537 					this task pending until the scheduler is resumed. */
// 4538 					vListInsertEnd( &( xPendingReadyList ), &( pxTCB->xEventListItem ) );
??prvAddCurrentTaskToDelayedList_337:
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_xPendingReadyList)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInsertEnd
        ; ------------------------------------- Block: 6 cycles
??prvAddCurrentTaskToDelayedList_339:
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 4539 				}
// 4540 
// 4541 				if( pxTCB->uxPriority > pxCurrentTCB->uxPriority )
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        CMPW      AX, DE             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_333  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
// 4542 				{
// 4543 					/* The notified task has a priority above the currently
// 4544 					executing task so a yield is required. */
// 4545 					if( pxHigherPriorityTaskWoken != NULL )
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_340  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_340:
        ONEW      AX                 ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_341  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 4546 					{
// 4547 						*pxHigherPriorityTaskWoken = pdTRUE;
        MOVW      ES:[HL], AX        ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_333  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 4548 					}
// 4549 					else
// 4550 					{
// 4551 						/* Mark that a yield is pending in case the user is not
// 4552 						using the "xHigherPriorityTaskWoken" parameter to an ISR
// 4553 						safe FreeRTOS function. */
// 4554 						xYieldPending = pdTRUE;
??prvAddCurrentTaskToDelayedList_341:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+18, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
// 4555 					}
// 4556 				}
// 4557 				else
// 4558 				{
// 4559 					mtCOVERAGE_TEST_MARKER();
// 4560 				}
// 4561 			}
// 4562 		}
// 4563 		portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
??prvAddCurrentTaskToDelayedList_333:
        BR        F:?Subroutine0     ;; 3 cycles
          CFI EndBlock cfiBlock56
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 365 cycles
// 4564 
// 4565 		return xReturn;
// 4566 	}
// 4567 
// 4568 #endif /* configUSE_TASK_NOTIFICATIONS */
// 4569 /*-----------------------------------------------------------*/
// 4570 
// 4571 #if( configUSE_TASK_NOTIFICATIONS == 1 )
// 4572 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock57 Using cfiCommon0
          CFI Function _vTaskNotifyGiveFromISR
        CODE
// 4573 	void vTaskNotifyGiveFromISR( TaskHandle_t xTaskToNotify, BaseType_t *pxHigherPriorityTaskWoken )
// 4574 	{
_vTaskNotifyGiveFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
// 4575 	TCB_t * pxTCB;
// 4576 	uint8_t ucOriginalNotifyState;
// 4577 	UBaseType_t uxSavedInterruptStatus;
// 4578 
// 4579 		configASSERT( xTaskToNotify );
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_342  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_342:
        BNZ       ??prvAddCurrentTaskToDelayedList_343  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_344  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_344:
        BR        S:??prvAddCurrentTaskToDelayedList_344  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4580 
// 4581 		/* RTOS ports that support interrupt nesting have the concept of a
// 4582 		maximum	system call (or maximum API call) interrupt priority.
// 4583 		Interrupts that are	above the maximum system call priority are keep
// 4584 		permanently enabled, even when the RTOS kernel is in a critical section,
// 4585 		but cannot make any calls to FreeRTOS API functions.  If configASSERT()
// 4586 		is defined in FreeRTOSConfig.h then
// 4587 		portASSERT_IF_INTERRUPT_PRIORITY_INVALID() will result in an assertion
// 4588 		failure if a FreeRTOS API function is called from an interrupt that has
// 4589 		been assigned a priority above the configured maximum system call
// 4590 		priority.  Only FreeRTOS functions that end in FromISR can be called
// 4591 		from interrupts	that have been assigned a priority at or (logically)
// 4592 		below the maximum system call interrupt priority.  FreeRTOS maintains a
// 4593 		separate interrupt safe API to ensure interrupt entry is as fast and as
// 4594 		simple as possible.  More information (albeit Cortex-M specific) is
// 4595 		provided on the following link:
// 4596 		http://www.freertos.org/RTOS-Cortex-M3-M4.html */
// 4597 		portASSERT_IF_INTERRUPT_PRIORITY_INVALID();
// 4598 
// 4599 		pxTCB = ( TCB_t * ) xTaskToNotify;
// 4600 
// 4601 		uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
??prvAddCurrentTaskToDelayedList_343:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 4602 		{
// 4603 			ucOriginalNotifyState = pxTCB->ucNotifyState;
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       D, A               ;; 1 cycle
// 4604 			pxTCB->ucNotifyState = taskNOTIFICATION_RECEIVED;
        MOV       A, #0x2            ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 4605 
// 4606 			/* 'Giving' is equivalent to incrementing a count in a counting
// 4607 			semaphore. */
// 4608 			( pxTCB->ulNotifiedValue )++;
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 4609 
// 4610 			traceTASK_NOTIFY_GIVE_FROM_ISR();
// 4611 
// 4612 			/* If the task is in the blocked state specifically to wait for a
// 4613 			notification then unblock it now. */
// 4614 			if( ucOriginalNotifyState == taskWAITING_NOTIFICATION )
        MOV       A, D               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_345  ;; 4 cycles
        ; ------------------------------------- Block: 45 cycles
// 4615 			{
// 4616 				/* The task should not have been on an event list. */
// 4617 				configASSERT( listLIST_ITEM_CONTAINER( &( pxTCB->xEventListItem ) ) == NULL );
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_346  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_346:
        BZ        ??prvAddCurrentTaskToDelayedList_347  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_348  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_348:
        BR        S:??prvAddCurrentTaskToDelayedList_348  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4618 
// 4619 				if( uxSchedulerSuspended == ( UBaseType_t ) pdFALSE )
??prvAddCurrentTaskToDelayedList_347:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+32  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_349  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 4620 				{
// 4621 					( void ) uxListRemove( &( pxTCB->xStateListItem ) );
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 4622 					prvAddTaskToReadyList( pxTCB );
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:_pxCurrentTCB+12  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_350  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+12, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_350:
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, HL             ;; 1 cycle
        MOVW      BC, #0x12          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_pxReadyTasksLists)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_pxReadyTasksLists)  ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        BR        S:??prvAddCurrentTaskToDelayedList_351  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 4623 				}
// 4624 				else
// 4625 				{
// 4626 					/* The delayed and ready lists cannot be accessed, so hold
// 4627 					this task pending until the scheduler is resumed. */
// 4628 					vListInsertEnd( &( xPendingReadyList ), &( pxTCB->xEventListItem ) );
??prvAddCurrentTaskToDelayedList_349:
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_xPendingReadyList)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInsertEnd
        ; ------------------------------------- Block: 6 cycles
??prvAddCurrentTaskToDelayedList_351:
        CALL      F:_vListInsertEnd  ;; 3 cycles
// 4629 				}
// 4630 
// 4631 				if( pxTCB->uxPriority > pxCurrentTCB->uxPriority )
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x2C]   ;; 2 cycles
        CMPW      AX, DE             ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_345  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
// 4632 				{
// 4633 					/* The notified task has a priority above the currently
// 4634 					executing task so a yield is required. */
// 4635 					if( pxHigherPriorityTaskWoken != NULL )
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_352  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_352:
        ONEW      AX                 ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_353  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 4636 					{
// 4637 						*pxHigherPriorityTaskWoken = pdTRUE;
        MOVW      ES:[HL], AX        ;; 2 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_345  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 4638 					}
// 4639 					else
// 4640 					{
// 4641 						/* Mark that a yield is pending in case the user is not
// 4642 						using the "xHigherPriorityTaskWoken" parameter in an ISR
// 4643 						safe FreeRTOS function. */
// 4644 						xYieldPending = pdTRUE;
??prvAddCurrentTaskToDelayedList_353:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      ES:_pxCurrentTCB+18, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
// 4645 					}
// 4646 				}
// 4647 				else
// 4648 				{
// 4649 					mtCOVERAGE_TEST_MARKER();
// 4650 				}
// 4651 			}
// 4652 		}
// 4653 		portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
??prvAddCurrentTaskToDelayedList_345:
        MOVW      AX, [SP]           ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        ROL       A, 0x1             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 4654 	}
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock57
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 264 cycles
// 4655 
// 4656 #endif /* configUSE_TASK_NOTIFICATIONS */
// 4657 
// 4658 /*-----------------------------------------------------------*/
// 4659 
// 4660 #if( configUSE_TASK_NOTIFICATIONS == 1 )
// 4661 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock58 Using cfiCommon0
          CFI Function _xTaskNotifyStateClear
          CFI NoCalls
        CODE
// 4662 	BaseType_t xTaskNotifyStateClear( TaskHandle_t xTask )
// 4663 	{
_xTaskNotifyStateClear:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 4664 	TCB_t *pxTCB;
// 4665 	BaseType_t xReturn;
// 4666 
// 4667 		/* If null is passed in here then it is the calling task that is having
// 4668 		its notification state cleared. */
// 4669 		pxTCB = prvGetTCBFromHandle( xTask );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_354  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_354:
        BNZ       ??prvAddCurrentTaskToDelayedList_355  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_356  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??prvAddCurrentTaskToDelayedList_355:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvAddCurrentTaskToDelayedList_356:
        MOVW      [SP], AX           ;; 1 cycle
// 4670 
// 4671 		taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_357  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvAddCurrentTaskToDelayedList_357:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 4672 		{
// 4673 			if( pxTCB->ucNotifyState == taskNOTIFICATION_RECEIVED )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_358  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 4674 			{
// 4675 				pxTCB->ucNotifyState = taskNOT_WAITING_NOTIFICATION;
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 4676 				xReturn = pdPASS;
        ONEW      AX                 ;; 1 cycle
        BR        S:??prvAddCurrentTaskToDelayedList_359  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 4677 			}
// 4678 			else
// 4679 			{
// 4680 				xReturn = pdFAIL;
??prvAddCurrentTaskToDelayedList_358:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvAddCurrentTaskToDelayedList_359:
        MOVW      HL, AX             ;; 1 cycle
// 4681 			}
// 4682 		}
// 4683 		taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_360  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_360  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 4684 
// 4685 		return xReturn;
??prvAddCurrentTaskToDelayedList_360:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock58
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 106 cycles
// 4686 	}
// 4687 
// 4688 #endif /* configUSE_TASK_NOTIFICATIONS */
// 4689 /*-----------------------------------------------------------*/
// 4690 
// 4691 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock59 Using cfiCommon2
          CFI Function _prvAddCurrentTaskToDelayedList
        CODE
// 4692 static void prvAddCurrentTaskToDelayedList( TickType_t xTicksToWait, const BaseType_t xCanBlockIndefinitely )
// 4693 {
_prvAddCurrentTaskToDelayedList:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 14
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+18
// 4694 TickType_t xTimeToWake;
// 4695 const TickType_t xConstTickCount = xTickCount;
        MOVW      HL, #LWRD(_pxCurrentTCB+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 4696 
// 4697 	#if( INCLUDE_xTaskAbortDelay == 1 )
// 4698 	{
// 4699 		/* About to enter a delayed list, so ensure the ucDelayAborted flag is
// 4700 		reset to pdFALSE so it can be detected as having been set to pdTRUE
// 4701 		when the task leaves the Blocked state. */
// 4702 		pxCurrentTCB->ucDelayAborted = pdFALSE;
// 4703 	}
// 4704 	#endif
// 4705 
// 4706 	/* Remove the task from the ready list before adding it to the blocked list
// 4707 	as the same list item is used for both lists. */
// 4708 	if( uxListRemove( &( pxCurrentTCB->xStateListItem ) ) == ( UBaseType_t ) 0 )
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
// 4709 	{
// 4710 		/* The current task must be in a ready list, so there is no need to
// 4711 		check, and the port reset macro can be called directly. */
// 4712 		portRESET_READY_PRIORITY( pxCurrentTCB->uxPriority, uxTopReadyPriority );
// 4713 	}
// 4714 	else
// 4715 	{
// 4716 		mtCOVERAGE_TEST_MARKER();
// 4717 	}
// 4718 
// 4719 	#if ( INCLUDE_vTaskSuspend == 1 )
// 4720 	{
// 4721 		if( ( xTicksToWait == portMAX_DELAY ) && ( xCanBlockIndefinitely != pdFALSE ) )
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 34 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvAddCurrentTaskToDelayedList_361:
        BNZ       ??prvAddCurrentTaskToDelayedList_362  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??prvAddCurrentTaskToDelayedList_362  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 4722 		{
// 4723 			/* Add the task to the suspended task list instead of a delayed task
// 4724 			list to ensure it is not woken by a timing event.  It will block
// 4725 			indefinitely. */
// 4726 			vListInsertEnd( &xSuspendedTaskList, &( pxCurrentTCB->xStateListItem ) );
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        INCW      BC                 ;; 1 cycle
        INCW      BC                 ;; 1 cycle
        INCW      BC                 ;; 1 cycle
        INCW      BC                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_xPendingReadyList+44)  ;; 1 cycle
        MOV       A, #BYTE3(_xPendingReadyList)  ;; 1 cycle
          CFI FunCall _vListInsertEnd
        CALL      F:_vListInsertEnd  ;; 3 cycles
        BR        R:??prvAddCurrentTaskToDelayedList_363  ;; 3 cycles
        ; ------------------------------------- Block: 22 cycles
// 4727 		}
// 4728 		else
// 4729 		{
// 4730 			/* Calculate the time at which the task should be woken if the event
// 4731 			does not occur.  This may overflow but this doesn't matter, the
// 4732 			kernel will manage it correctly. */
// 4733 			xTimeToWake = xConstTickCount + xTicksToWait;
??prvAddCurrentTaskToDelayedList_362:
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 4734 
// 4735 			/* The list item will be inserted in wake time order. */
// 4736 			listSET_LIST_ITEM_VALUE( &( pxCurrentTCB->xStateListItem ), xTimeToWake );
        MOVW      AX, [SP]           ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      ES:[HL+0x04], AX   ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x06], AX   ;; 2 cycles
// 4737 
// 4738 			if( xTimeToWake < xConstTickCount )
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_364  ;; 4 cycles
        ; ------------------------------------- Block: 45 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_364  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??prvAddCurrentTaskToDelayedList_364  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvAddCurrentTaskToDelayedList_364:
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      DE, ES:_pxCurrentTCB  ;; 2 cycles
        MOV       A, ES:_pxCurrentTCB+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        BNC       ??prvAddCurrentTaskToDelayedList_365  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 4739 			{
// 4740 				/* Wake time has overflowed.  Place this item in the overflow
// 4741 				list. */
// 4742 				vListInsert( pxOverflowDelayedTaskList, &( pxCurrentTCB->xStateListItem ) );
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, #LWRD(_xPendingReadyList+40)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInsert
        CALL      F:_vListInsert     ;; 3 cycles
        BR        S:??prvAddCurrentTaskToDelayedList_363  ;; 3 cycles
        ; ------------------------------------- Block: 28 cycles
// 4743 			}
// 4744 			else
// 4745 			{
// 4746 				/* The wake time has not overflowed, so the current block list
// 4747 				is used. */
// 4748 				vListInsert( pxDelayedTaskList, &( pxCurrentTCB->xStateListItem ) );
??prvAddCurrentTaskToDelayedList_365:
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, #LWRD(_xPendingReadyList+36)  ;; 1 cycle
        MOV       ES, #BYTE3(_xPendingReadyList)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInsert
        CALL      F:_vListInsert     ;; 3 cycles
// 4749 
// 4750 				/* If the task entering the blocked state was placed at the
// 4751 				head of the list of blocked tasks then xNextTaskUnblockTime
// 4752 				needs to be updated too. */
// 4753 				if( xTimeToWake < xNextTaskUnblockTime )
        MOVW      HL, #LWRD(_pxCurrentTCB+24)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTCB)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
        BNC       ??prvAddCurrentTaskToDelayedList_363  ;; 4 cycles
        ; ------------------------------------- Block: 45 cycles
// 4754 				{
// 4755 					xNextTaskUnblockTime = xTimeToWake;
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 5 cycles
// 4756 				}
// 4757 				else
// 4758 				{
// 4759 					mtCOVERAGE_TEST_MARKER();
// 4760 				}
// 4761 			}
// 4762 		}
// 4763 	}
// 4764 	#else /* INCLUDE_vTaskSuspend */
// 4765 	{
// 4766 		/* Calculate the time at which the task should be woken if the event
// 4767 		does not occur.  This may overflow but this doesn't matter, the kernel
// 4768 		will manage it correctly. */
// 4769 		xTimeToWake = xConstTickCount + xTicksToWait;
// 4770 
// 4771 		/* The list item will be inserted in wake time order. */
// 4772 		listSET_LIST_ITEM_VALUE( &( pxCurrentTCB->xStateListItem ), xTimeToWake );
// 4773 
// 4774 		if( xTimeToWake < xConstTickCount )
// 4775 		{
// 4776 			/* Wake time has overflowed.  Place this item in the overflow list. */
// 4777 			vListInsert( pxOverflowDelayedTaskList, &( pxCurrentTCB->xStateListItem ) );
// 4778 		}
// 4779 		else
// 4780 		{
// 4781 			/* The wake time has not overflowed, so the current block list is used. */
// 4782 			vListInsert( pxDelayedTaskList, &( pxCurrentTCB->xStateListItem ) );
// 4783 
// 4784 			/* If the task entering the blocked state was placed at the head of the
// 4785 			list of blocked tasks then xNextTaskUnblockTime needs to be updated
// 4786 			too. */
// 4787 			if( xTimeToWake < xNextTaskUnblockTime )
// 4788 			{
// 4789 				xNextTaskUnblockTime = xTimeToWake;
// 4790 			}
// 4791 			else
// 4792 			{
// 4793 				mtCOVERAGE_TEST_MARKER();
// 4794 			}
// 4795 		}
// 4796 
// 4797 		/* Avoid compiler warning when INCLUDE_vTaskSuspend is not 1. */
// 4798 		( void ) xCanBlockIndefinitely;
// 4799 	}
// 4800 	#endif /* INCLUDE_vTaskSuspend */
// 4801 }
??prvAddCurrentTaskToDelayedList_363:
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock59
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 222 cycles

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_0:
        DB "IDLE"
        DATA8
        DB 0

        END
// 4802 
// 4803 
// 4804 #ifdef FREERTOS_MODULE_TEST
// 4805 	#include "tasks_test_access_functions.h"
// 4806 #endif
// 4807 
// 
//    258 bytes in section .bssf
//      6 bytes in section .constf
// 12 712 bytes in section .textf
// 
//    258 bytes of DATA    memory
// 12 718 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
