///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:59
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\src\RTOS_api_Zm.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3EB3.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\src\RTOS_api_Zm.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\RTOS_api_Zm.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_file_descriptor", "0"
        RTMODEL "__dlib_full_locale_support", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _RoaResources
        EXTERN _RmResources
        EXTERN _RmConfigParams
        EXTERN _R_NWKDual_GetNwkConfigParams
        EXTERN _R_OS_SendMsg
        EXTERN _RmMain
        EXTERN _RoaInit

        PUBLIC _mac_tsk
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\src\RTOS_api_Zm.c
//    1 /******************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corp. and is only
//    4  * intended for use with Renesas products. No other uses are authorized.
//    5  * This software is owned by Renesas Electronics Corp. and is protected under
//    6  * all applicable laws, including copyright laws.
//    7  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
//    8  * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
//    9  * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//   10  * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY
//   11  * DISCLAIMED.
//   12  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   13  * ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   14  * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
//   15  * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
//   16  * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   17  * Renesas reserves the right, without notice, to make changes to this
//   18  * software and to discontinue the availability of this software.
//   19  * By using this software, you agree to the additional terms and
//   20  * conditions found by accessing the following link:
//   21  * http://www.renesas.com/disclaimer
//   22  *****************************************************************************/
//   23 
//   24 /******************************************************************************
//   25  * File Name   : RTOS_api_Zm.c
//   26  * Description :
//   27  ******************************************************************************
//   28  * Copyright (C) 2014-2016 Renesas Electronics Corporation.
//   29  *****************************************************************************/
//   30 
//   31 /***********************************************************************
//   32  * Check macro definition
//   33  **********************************************************************/
//   34 
//   35 
//   36 /***********************************************************************
//   37  * includes
//   38  **********************************************************************/
//   39 #if defined(__RX)
//   40 #include <iodefine.h>
//   41 #endif /* #if defined(__RX) */
//   42 
//   43 #include "mac_api.h"
//   44 #include "r_impl_utils.h"
//   45 #ifdef R_HYBRID_PLC_RF
//   46 #include "ZmConfig.h"
//   47 #include "mac_intr.h"
//   48 #else
//   49 #include "r_nwk.h"
//   50 #endif
//   51 
//   52 #undef R_ASSERT_FAIL_RETURN_VALUE
//   53 #define R_ASSERT_FAIL_RETURN_VALUE /* void */
//   54 
//   55 /***********************************************************************
//   56  * extern
//   57  **********************************************************************/
//   58 extern RmConfigParamsT RmConfigParams;
//   59 extern RmResourcesT RmResources;
//   60 extern RoaResourcesT RoaResources;
//   61 
//   62 #ifdef R_HYBRID_PLC_RF
//   63 
//   64 /* Allocate memory for RF MAC layer. */
//   65 static uint8_t g_memory[sizeof(RmMacCbT) + RM_NUM_IND_TX * sizeof(RmDataXferElemT) + R_MAC_NEIGHBOR_TABLE_SIZE * sizeof(r_mac_neighbor_t) + R_MAX_FRAME_SIZE];
//   66 #endif
//   67 
//   68 /***********************************************************************
//   69  *  Function Name  : rmUpMsgCallback_mac_task
//   70  *  -------------------------------------------------------------------
//   71  *  Parameters     : RmMsgT
//   72  *  -------------------------------------------------------------------
//   73  *  Return value   : None
//   74  *  -------------------------------------------------------------------
//   75  *  Description    : call back function (for IPv6)
//   76  *                 :
//   77  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _rmUpMsgCallback_mac_tsk
        CODE
//   78 static void rmUpMsgCallback_mac_tsk(void* pMsg)
//   79 {
_rmUpMsgCallback_mac_tsk:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
//   80 #ifdef R_HYBRID_PLC_RF
//   81     R_OS_SendMsg(pMsg, ID_nwk_mbx);
//   82 #else
//   83     r_nwk_config_params_t* p_gNwkConfigParams = R_NWKDual_GetNwkConfigParams();
          CFI FunCall _R_NWKDual_GetNwkConfigParams
        CALL      F:_R_NWKDual_GetNwkConfigParams  ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//   84     R_ASSERT(p_gNwkConfigParams != NULL);
//   85 
//   86     R_OS_SendMsg(pMsg, p_gNwkConfigParams->p_nwkTableResources->p_sysConfigTBL->nwk_mbxId);
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE+0x08]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES:[DE+0x0A]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES:[DE+0x05]    ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall _R_OS_SendMsg
        CALL      F:_R_OS_SendMsg    ;; 3 cycles
//   87 #endif
//   88 }
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 49 cycles
        ; ------------------------------------- Total: 49 cycles
//   89 
//   90 
//   91 /***********************************************************************
//   92  * Name         : mac_tsk
//   93  * Parameters   : VP_INT
//   94  * Returns      : nothing
//   95  * Description  : mac task
//   96  * Note         : !!DO NOT CHANGE!!
//   97  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _mac_tsk
        CODE
//   98 void mac_tsk(void* UNUSEDP(stacd))
//   99 {
_mac_tsk:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 26
        SUBW      SP, #0x1A          ;; 1 cycle
          CFI CFA SP+30
//  100     static const RmCallbaksT rmcbacks = {rmUpMsgCallback_mac_tsk};
//  101 
//  102     RmInitParamT data;
//  103 #ifdef R_HYBRID_PLC_RF
//  104     RoaInit(&RoaResources);
//  105 
//  106     /* Initialize memory for RF MAC layer. */
//  107     r_rfmac_memsetup_info_t p_mem_info;
//  108     p_mem_info.alloc_byte_size = sizeof(g_memory);
//  109     p_mem_info.p_start = (void*)g_memory;
//  110     p_mem_info.size_info.posTableSize = R_MAC_NEIGHBOR_TABLE_SIZE;
//  111     p_mem_info.size_info.indTxQueueSize = RM_NUM_IND_TX;
//  112 
//  113     void* p_end;
//  114 
//  115     if (rmInitMemory(p_mem_info, &p_end) != RM_MAC_SUCCESS)
//  116     {
//  117         return;
//  118     }
//  119 
//  120     /* --- --- */
//  121     data.pCbacks = &rmcbacks;
//  122     data.pResources = &RmResources;
//  123     data.pConfig = &RmConfigParams;
//  124 
//  125     /* --- --- */
//  126     data.flgId = ID_mac_flg;
//  127     data.rxMplId = R_HEAP_ID_MAC_IND;
//  128     data.warningMplId = R_HEAP_ID_WARN_IND;
//  129     data.fatalMplId = R_HEAP_ID_ERR_IND;
//  130     data.semid = ID_rfdrv_sem;
//  131 #else  /* ifdef R_HYBRID_PLC_RF */
//  132     r_nwk_config_params_t* p_gNwkConfigParams = NULL;
//  133     const r_sys_config_t* p_gSysConfig;
//  134 
//  135     RoaInit(&RoaResources);
        MOVW      DE, #LWRD(_RoaResources)  ;; 1 cycle
        MOV       A, #BYTE3(_RoaResources)  ;; 1 cycle
          CFI FunCall _RoaInit
        CALL      F:_RoaInit         ;; 3 cycles
//  136 
//  137     p_gNwkConfigParams =  R_NWKDual_GetNwkConfigParams();
          CFI FunCall _R_NWKDual_GetNwkConfigParams
        CALL      F:_R_NWKDual_GetNwkConfigParams  ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  138     R_ASSERT(p_gNwkConfigParams != NULL);
//  139     p_gSysConfig = p_gNwkConfigParams->p_nwkTableResources->p_sysConfigTBL;
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0x0A]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[DE+0x08]   ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
//  140 
//  141     /* --- --- */
//  142     data.pCbacks = &rmcbacks;
        MOV       [SP+0x06], #BYTE3(`mac_tsk::rmcbacks`)  ;; 1 cycle
        MOVW      AX, #LWRD(`mac_tsk::rmcbacks`)  ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOV       A, [SP+0x07]       ;; 1 cycle
        MOV       [SP+0x0B], A       ;; 1 cycle
//  143     data.pResources = &RmResources;
        MOV       [SP+0x06], #BYTE3(_RmResources)  ;; 1 cycle
        MOVW      AX, #LWRD(_RmResources)  ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       [SP+0x0D], A       ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOV       A, [SP+0x07]       ;; 1 cycle
        MOV       [SP+0x0F], A       ;; 1 cycle
//  144     data.pConfig = &RmConfigParams;
        MOV       [SP+0x06], #BYTE3(_RmConfigParams)  ;; 1 cycle
        MOVW      AX, #LWRD(_RmConfigParams)  ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x10], A       ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       [SP+0x11], A       ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       [SP+0x12], A       ;; 1 cycle
        MOV       A, [SP+0x07]       ;; 1 cycle
        MOV       [SP+0x13], A       ;; 1 cycle
//  145 
//  146     /* --- --- */
//  147     data.flgId = p_gSysConfig->mac_flgId;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x14], A       ;; 1 cycle
//  148     data.rxMplId = p_gSysConfig->mac_rx_mplId;
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x15], A       ;; 1 cycle
//  149     data.warningMplId = p_gSysConfig->mac_warning_ind_mplId;
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x16], A       ;; 1 cycle
//  150     data.fatalMplId = p_gSysConfig->mac_fatal_ind_mplId;
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x17], A       ;; 1 cycle
//  151     data.semid = p_gSysConfig->mac_rfdrv_semId;
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x18], A       ;; 1 cycle
//  152 #endif /* ifdef R_HYBRID_PLC_RF */
//  153 
//  154     #if defined(__RX__)
//  155     vrst_mpf(data.rxMplId);
//  156     #endif
//  157 
//  158     RmMain(&data);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _RmMain
        CALL      F:_RmMain          ;; 3 cycles
//  159 }
        ADDW      SP, #0x1A          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 101 cycles
        ; ------------------------------------- Total: 101 cycles

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
`mac_tsk::rmcbacks`:
        DATA32
        DD _rmUpMsgCallback_mac_tsk

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//  160 
//  161 /******************************************************************************
//  162  * Copyright (C) 2014-2016 Renesas Electronics Corporation.
//  163  *****************************************************************************/
// 
//   4 bytes in section .constf
// 231 bytes in section .textf
// 
// 235 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
