///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:56
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\event_groups.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW2D50.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\event_groups.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\event_groups.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_full_locale_support", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _usCriticalNesting
        EXTERN ?L_AND_FAST_L03
        EXTERN ?L_IOR_L03
        EXTERN ?L_NOT_L03
        EXTERN ?UL_CMP_L03
        EXTERN _uxTaskResetEventItemValue
        EXTERN _vListInitialise
        EXTERN _vTaskPlaceOnUnorderedEventList
        EXTERN _vTaskSuspendAll
        EXTERN _xTaskGetSchedulerState
        EXTERN _xTaskRemoveFromUnorderedEventList
        EXTERN _xTaskResumeAll
        EXTERN _xTimerPendFunctionCallFromISR

        PUBLIC _uxEventGroupGetNumber
        PUBLIC _vEventGroupClearBitsCallback
        PUBLIC _vEventGroupDelete
        PUBLIC _vEventGroupSetBitsCallback
        PUBLIC _xEventGroupClearBits
        PUBLIC _xEventGroupClearBitsFromISR
        PUBLIC _xEventGroupCreateStatic
        PUBLIC _xEventGroupGetBitsFromISR
        PUBLIC _xEventGroupSetBits
        PUBLIC _xEventGroupSetBitsFromISR
        PUBLIC _xEventGroupSync
        PUBLIC _xEventGroupWaitBits
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\event_groups.c
//    1 /*
//    2     FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
//    3     All rights reserved
//    4 
//    5     VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
//    6 
//    7     This file is part of the FreeRTOS distribution.
//    8 
//    9     FreeRTOS is free software; you can redistribute it and/or modify it under
//   10     the terms of the GNU General Public License (version 2) as published by the
//   11     Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.
//   12 
//   13     ***************************************************************************
//   14     >>!   NOTE: The modification to the GPL is included to allow you to     !<<
//   15     >>!   distribute a combined work that includes FreeRTOS without being   !<<
//   16     >>!   obliged to provide the source code for proprietary components     !<<
//   17     >>!   outside of the FreeRTOS kernel.                                   !<<
//   18     ***************************************************************************
//   19 
//   20     FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
//   21     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//   22     FOR A PARTICULAR PURPOSE.  Full license text is available on the following
//   23     link: http://www.freertos.org/a00114.html
//   24 
//   25     ***************************************************************************
//   26      *                                                                       *
//   27      *    FreeRTOS provides completely free yet professionally developed,    *
//   28      *    robust, strictly quality controlled, supported, and cross          *
//   29      *    platform software that is more than just the market leader, it     *
//   30      *    is the industry's de facto standard.                               *
//   31      *                                                                       *
//   32      *    Help yourself get started quickly while simultaneously helping     *
//   33      *    to support the FreeRTOS project by purchasing a FreeRTOS           *
//   34      *    tutorial book, reference manual, or both:                          *
//   35      *    http://www.FreeRTOS.org/Documentation                              *
//   36      *                                                                       *
//   37     ***************************************************************************
//   38 
//   39     http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
//   40     the FAQ page "My application does not run, what could be wrong?".  Have you
//   41     defined configASSERT()?
//   42 
//   43     http://www.FreeRTOS.org/support - In return for receiving this top quality
//   44     embedded software for free we request you assist our global community by
//   45     participating in the support forum.
//   46 
//   47     http://www.FreeRTOS.org/training - Investing in training allows your team to
//   48     be as productive as possible as early as possible.  Now you can receive
//   49     FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
//   50     Ltd, and the world's leading authority on the world's leading RTOS.
//   51 
//   52     http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
//   53     including FreeRTOS+Trace - an indispensable productivity tool, a DOS
//   54     compatible FAT file system, and our tiny thread aware UDP/IP stack.
//   55 
//   56     http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
//   57     Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.
//   58 
//   59     http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
//   60     Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
//   61     licenses offer ticketed support, indemnification and commercial middleware.
//   62 
//   63     http://www.SafeRTOS.com - High Integrity Systems also provide a safety
//   64     engineered and independently SIL3 certified version for use in safety and
//   65     mission critical applications that require provable dependability.
//   66 
//   67     1 tab == 4 spaces!
//   68 */
//   69 
//   70 /* Standard includes. */
//   71 #include <stdlib.h>
//   72 
//   73 /* Defining MPU_WRAPPERS_INCLUDED_FROM_API_FILE prevents task.h from redefining
//   74 all the API functions to use the MPU wrappers.  That should only be done when
//   75 task.h is included from an application file. */
//   76 #define MPU_WRAPPERS_INCLUDED_FROM_API_FILE
//   77 
//   78 /* FreeRTOS includes. */
//   79 #include "FreeRTOS.h"
//   80 #include "task.h"
//   81 #include "timers.h"
//   82 #include "event_groups.h"
//   83 
//   84 /* Lint e961 and e750 are suppressed as a MISRA exception justified because the
//   85 MPU ports require MPU_WRAPPERS_INCLUDED_FROM_API_FILE to be defined for the
//   86 header files above, but not in this file, in order to generate the correct
//   87 privileged Vs unprivileged linkage and placement. */
//   88 #undef MPU_WRAPPERS_INCLUDED_FROM_API_FILE /*lint !e961 !e750. */
//   89 
//   90 /* The following bit fields convey control information in a task's event list
//   91 item value.  It is important they don't clash with the
//   92 taskEVENT_LIST_ITEM_VALUE_IN_USE definition. */
//   93 #if configUSE_16_BIT_TICKS == 1
//   94 	#define eventCLEAR_EVENTS_ON_EXIT_BIT	0x0100U
//   95 	#define eventUNBLOCKED_DUE_TO_BIT_SET	0x0200U
//   96 	#define eventWAIT_FOR_ALL_BITS			0x0400U
//   97 	#define eventEVENT_BITS_CONTROL_BYTES	0xff00U
//   98 #else
//   99 	#define eventCLEAR_EVENTS_ON_EXIT_BIT	0x01000000UL
//  100 	#define eventUNBLOCKED_DUE_TO_BIT_SET	0x02000000UL
//  101 	#define eventWAIT_FOR_ALL_BITS			0x04000000UL
//  102 	#define eventEVENT_BITS_CONTROL_BYTES	0xff000000UL
//  103 #endif
//  104 
//  105 typedef struct xEventGroupDefinition
//  106 {
//  107 	EventBits_t uxEventBits;
//  108 	List_t xTasksWaitingForBits;		/*< List of tasks waiting for a bit to be set. */
//  109 
//  110 	#if( configUSE_TRACE_FACILITY == 1 )
//  111 		UBaseType_t uxEventGroupNumber;
//  112 	#endif
//  113 
//  114 	#if( ( configSUPPORT_STATIC_ALLOCATION == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) )
//  115 		uint8_t ucStaticallyAllocated; /*< Set to pdTRUE if the event group is statically allocated to ensure no attempt is made to free the memory. */
//  116 	#endif
//  117 } EventGroup_t;
//  118 
//  119 /*-----------------------------------------------------------*/
//  120 
//  121 /*
//  122  * Test the bits set in uxCurrentEventBits to see if the wait condition is met.
//  123  * The wait condition is defined by xWaitForAllBits.  If xWaitForAllBits is
//  124  * pdTRUE then the wait condition is met if all the bits set in uxBitsToWaitFor
//  125  * are also set in uxCurrentEventBits.  If xWaitForAllBits is pdFALSE then the
//  126  * wait condition is met if any of the bits set in uxBitsToWait for are also set
//  127  * in uxCurrentEventBits.
//  128  */
//  129 static BaseType_t prvTestWaitCondition( const EventBits_t uxCurrentEventBits, const EventBits_t uxBitsToWaitFor, const BaseType_t xWaitForAllBits ) PRIVILEGED_FUNCTION;
//  130 
//  131 /*-----------------------------------------------------------*/
//  132 
//  133 #if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  134 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _xEventGroupCreateStatic
        CODE
//  135 	EventGroupHandle_t xEventGroupCreateStatic( StaticEventGroup_t *pxEventGroupBuffer )
//  136 	{
_xEventGroupCreateStatic:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  137 	EventGroup_t *pxEventBits;
//  138 
//  139 		/* A StaticEventGroup_t object must be provided. */
//  140 		configASSERT( pxEventGroupBuffer );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_0  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??uxEventGroupGetNumber_0:
        BNZ       ??uxEventGroupGetNumber_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_2  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_2:
        BR        S:??uxEventGroupGetNumber_2  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  141 
//  142 		/* The user has provided a statically allocated event group - use it. */
//  143 		pxEventBits = ( EventGroup_t * ) pxEventGroupBuffer; /*lint !e740 EventGroup_t and StaticEventGroup_t are guaranteed to have the same size and alignment requirement - checked by configASSERT(). */
//  144 
//  145 		if( pxEventBits != NULL )
//  146 		{
//  147 			pxEventBits->uxEventBits = 0;
??uxEventGroupGetNumber_1:
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  148 			vListInitialise( &( pxEventBits->xTasksWaitingForBits ) );
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
//  149 
//  150 			#if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
//  151 			{
//  152 				/* Both static and dynamic allocation can be used, so note that
//  153 				this event group was created statically in case the event group
//  154 				is later deleted. */
//  155 				pxEventBits->ucStaticallyAllocated = pdTRUE;
//  156 			}
//  157 			#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
//  158 
//  159 			traceEVENT_GROUP_CREATE( pxEventBits );
//  160 		}
//  161 		else
//  162 		{
//  163 			traceEVENT_GROUP_CREATE_FAILED();
//  164 		}
//  165 
//  166 		return ( EventGroupHandle_t ) pxEventBits;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 24 cycles
        ; ------------------------------------- Total: 64 cycles
//  167 	}
//  168 
//  169 #endif /* configSUPPORT_STATIC_ALLOCATION */
//  170 /*-----------------------------------------------------------*/
//  171 
//  172 #if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
//  173 
//  174 	EventGroupHandle_t xEventGroupCreate( void )
//  175 	{
//  176 	EventGroup_t *pxEventBits;
//  177 
//  178 		/* Allocate the event group. */
//  179 		pxEventBits = ( EventGroup_t * ) pvPortMalloc( sizeof( EventGroup_t ) );
//  180 
//  181 		if( pxEventBits != NULL )
//  182 		{
//  183 			pxEventBits->uxEventBits = 0;
//  184 			vListInitialise( &( pxEventBits->xTasksWaitingForBits ) );
//  185 
//  186 			#if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  187 			{
//  188 				/* Both static and dynamic allocation can be used, so note this
//  189 				event group was allocated statically in case the event group is
//  190 				later deleted. */
//  191 				pxEventBits->ucStaticallyAllocated = pdFALSE;
//  192 			}
//  193 			#endif /* configSUPPORT_STATIC_ALLOCATION */
//  194 
//  195 			traceEVENT_GROUP_CREATE( pxEventBits );
//  196 		}
//  197 		else
//  198 		{
//  199 			traceEVENT_GROUP_CREATE_FAILED();
//  200 		}
//  201 
//  202 		return ( EventGroupHandle_t ) pxEventBits;
//  203 	}
//  204 
//  205 #endif /* configSUPPORT_DYNAMIC_ALLOCATION */
//  206 /*-----------------------------------------------------------*/
//  207 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon1
          CFI Function _xEventGroupSync
        CODE
//  208 EventBits_t xEventGroupSync( EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToSet, const EventBits_t uxBitsToWaitFor, TickType_t xTicksToWait )
//  209 {
_xEventGroupSync:
        ; * Stack frame (at entry) *
        ; Param size: 12
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
//  210 EventBits_t uxOriginalBitValue, uxReturn;
//  211 EventGroup_t *pxEventBits = ( EventGroup_t * ) xEventGroup;
//  212 BaseType_t xAlreadyYielded;
//  213 BaseType_t xTimeoutOccurred = pdFALSE;
//  214 
//  215 	configASSERT( ( uxBitsToWaitFor & eventEVENT_BITS_CONTROL_BYTES ) == 0 );
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_3  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_4  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_4:
        BR        S:??uxEventGroupGetNumber_4  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  216 	configASSERT( uxBitsToWaitFor != 0 );
??uxEventGroupGetNumber_3:
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_5  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_6  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_6:
        BR        S:??uxEventGroupGetNumber_6  ;; 3 cycles
          CFI FunCall _xTaskGetSchedulerState
        ; ------------------------------------- Block: 3 cycles
//  217 	#if ( ( INCLUDE_xTaskGetSchedulerState == 1 ) || ( configUSE_TIMERS == 1 ) )
//  218 	{
//  219 		configASSERT( !( ( xTaskGetSchedulerState() == taskSCHEDULER_SUSPENDED ) && ( xTicksToWait != 0 ) ) );
??uxEventGroupGetNumber_5:
        CALL      F:_xTaskGetSchedulerState  ;; 3 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_7  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??uxEventGroupGetNumber_8  ;; 4 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 10 cycles
//  220 	}
//  221 	#endif
//  222 
//  223 	vTaskSuspendAll();
??uxEventGroupGetNumber_7:
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
//  224 	{
//  225 		uxOriginalBitValue = pxEventBits->uxEventBits;
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  226 
//  227 		( void ) xEventGroupSetBits( xEventGroup, uxBitsToSet );
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xEventGroupSetBits
        CALL      F:_xEventGroupSetBits  ;; 3 cycles
//  228 
//  229 		if( ( ( uxOriginalBitValue | uxBitsToSet ) & uxBitsToWaitFor ) == uxBitsToWaitFor )
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        BNZ       ??uxEventGroupGetNumber_9  ;; 4 cycles
        ; ------------------------------------- Block: 60 cycles
//  230 		{
//  231 			/* All the rendezvous bits are now set - no need to block. */
//  232 			uxReturn = ( uxOriginalBitValue | uxBitsToSet );
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  233 
//  234 			/* Rendezvous always clear the bits.  They will have been cleared
//  235 			already unless this is the only task in the rendezvous. */
//  236 			pxEventBits->uxEventBits &= ~uxBitsToWaitFor;
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
          CFI FunCall ?L_NOT_L03
        CALL      N:?L_NOT_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  237 
//  238 			xTicksToWait = 0;
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        MOVW      [SP+0x1A], AX      ;; 1 cycle
        BR        S:??uxEventGroupGetNumber_10  ;; 3 cycles
        ; ------------------------------------- Block: 36 cycles
//  239 		}
??uxEventGroupGetNumber_8:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_11  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_11:
        BR        S:??uxEventGroupGetNumber_11  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  240 		else
//  241 		{
//  242 			if( xTicksToWait != ( TickType_t ) 0 )
??uxEventGroupGetNumber_9:
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_12  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
//  243 			{
//  244 				traceEVENT_GROUP_SYNC_BLOCK( xEventGroup, uxBitsToSet, uxBitsToWaitFor );
//  245 
//  246 				/* Store the bits that the calling task is waiting for in the
//  247 				task's event list item so the kernel knows when a match is
//  248 				found.  Then enter the blocked state. */
//  249 				vTaskPlaceOnUnorderedEventList( &( pxEventBits->xTasksWaitingForBits ), ( uxBitsToWaitFor | eventCLEAR_EVENTS_ON_EXIT_BIT | eventWAIT_FOR_ALL_BITS ), xTicksToWait );
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        OR        A, #0x5            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vTaskPlaceOnUnorderedEventList
        CALL      F:_vTaskPlaceOnUnorderedEventList  ;; 3 cycles
//  250 
//  251 				/* This assignment is obsolete as uxReturn will get set after
//  252 				the task unblocks, but some compilers mistakenly generate a
//  253 				warning about uxReturn being returned without being set if the
//  254 				assignment is omitted. */
//  255 				uxReturn = 0;
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        S:??uxEventGroupGetNumber_13  ;; 3 cycles
        ; ------------------------------------- Block: 26 cycles
//  256 			}
//  257 			else
//  258 			{
//  259 				/* The rendezvous bits were not set, but no block time was
//  260 				specified - just return the current event bit value. */
//  261 				uxReturn = pxEventBits->uxEventBits;
??uxEventGroupGetNumber_12:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        ; ------------------------------------- Block: 9 cycles
??uxEventGroupGetNumber_13:
        MOVW      [SP+0x02], AX      ;; 1 cycle
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 1 cycles
//  262 			}
//  263 		}
//  264 	}
//  265 	xAlreadyYielded = xTaskResumeAll();
??uxEventGroupGetNumber_10:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
//  266 
//  267 	if( xTicksToWait != ( TickType_t ) 0 )
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??uxEventGroupGetNumber_14  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
//  268 	{
//  269 		if( xAlreadyYielded == pdFALSE )
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  270 		{
//  271 			portYIELD_WITHIN_API();
        BRK                          ;; 5 cycles
          CFI FunCall _uxTaskResetEventItemValue
        ; ------------------------------------- Block: 5 cycles
//  272 		}
//  273 		else
//  274 		{
//  275 			mtCOVERAGE_TEST_MARKER();
//  276 		}
//  277 
//  278 		/* The task blocked to wait for its required bits to be set - at this
//  279 		point either the required bits were set or the block time expired.  If
//  280 		the required bits were set they will have been stored in the task's
//  281 		event list item, and they should now be retrieved then cleared. */
//  282 		uxReturn = uxTaskResetEventItemValue();
??xEventGroupSync_0:
        CALL      F:_uxTaskResetEventItemValue  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
//  283 
//  284 		if( ( uxReturn & eventUNBLOCKED_DUE_TO_BIT_SET ) == ( EventBits_t ) 0 )
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV1      CY, [HL].1         ;; 1 cycle
        BC        ??uxEventGroupGetNumber_15  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
//  285 		{
//  286 			/* The task timed out, just return the current event bit value. */
//  287 			taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_16  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_16:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
//  288 			{
//  289 				uxReturn = pxEventBits->uxEventBits;
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  290 
//  291 				/* Although the task got here because it timed out before the
//  292 				bits it was waiting for were set, it is possible that since it
//  293 				unblocked another task has set the bits.  If this is the case
//  294 				then it needs to clear the bits before exiting. */
//  295 				if( ( uxReturn & uxBitsToWaitFor ) == uxBitsToWaitFor )
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        BNZ       ??uxEventGroupGetNumber_17  ;; 4 cycles
        ; ------------------------------------- Block: 37 cycles
//  296 				{
//  297 					pxEventBits->uxEventBits &= ~uxBitsToWaitFor;
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
          CFI FunCall ?L_NOT_L03
        CALL      N:?L_NOT_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        ; ------------------------------------- Block: 22 cycles
//  298 				}
//  299 				else
//  300 				{
//  301 					mtCOVERAGE_TEST_MARKER();
//  302 				}
//  303 			}
//  304 			taskEXIT_CRITICAL();
??uxEventGroupGetNumber_17:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_15  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_15  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  305 
//  306 			xTimeoutOccurred = pdTRUE;
//  307 		}
//  308 		else
//  309 		{
//  310 			/* The task unblocked because the bits were set. */
//  311 		}
//  312 
//  313 		/* Control bits might be set as the task had blocked should not be
//  314 		returned. */
//  315 		uxReturn &= ~eventEVENT_BITS_CONTROL_BYTES;
??uxEventGroupGetNumber_15:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
//  316 	}
//  317 
//  318 	traceEVENT_GROUP_SYNC_END( xEventGroup, uxBitsToSet, uxBitsToWaitFor, xTimeoutOccurred );
//  319 
//  320 	return uxReturn;
??uxEventGroupGetNumber_14:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 392 cycles
//  321 }
//  322 /*-----------------------------------------------------------*/
//  323 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _xEventGroupWaitBits
        CODE
//  324 EventBits_t xEventGroupWaitBits( EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToWaitFor, const BaseType_t xClearOnExit, const BaseType_t xWaitForAllBits, TickType_t xTicksToWait )
//  325 {
_xEventGroupWaitBits:
        ; * Stack frame (at entry) *
        ; Param size: 10
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 14
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+18
//  326 EventGroup_t *pxEventBits = ( EventGroup_t * ) xEventGroup;
//  327 EventBits_t uxReturn, uxControlBits = 0;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  328 BaseType_t xWaitConditionMet, xAlreadyYielded;
//  329 BaseType_t xTimeoutOccurred = pdFALSE;
//  330 
//  331 	/* Check the user is not attempting to wait on the bits used by the kernel
//  332 	itself, and that at least one bit is being requested. */
//  333 	configASSERT( xEventGroup );
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_18  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??uxEventGroupGetNumber_18:
        BNZ       ??uxEventGroupGetNumber_19  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_20  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_20:
        BR        S:??uxEventGroupGetNumber_20  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  334 	configASSERT( ( uxBitsToWaitFor & eventEVENT_BITS_CONTROL_BYTES ) == 0 );
??uxEventGroupGetNumber_19:
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_21  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_22  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_22:
        BR        S:??uxEventGroupGetNumber_22  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  335 	configASSERT( uxBitsToWaitFor != 0 );
??uxEventGroupGetNumber_21:
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_23  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_24  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_24:
        BR        S:??uxEventGroupGetNumber_24  ;; 3 cycles
          CFI FunCall _xTaskGetSchedulerState
        ; ------------------------------------- Block: 3 cycles
//  336 	#if ( ( INCLUDE_xTaskGetSchedulerState == 1 ) || ( configUSE_TIMERS == 1 ) )
//  337 	{
//  338 		configASSERT( !( ( xTaskGetSchedulerState() == taskSCHEDULER_SUSPENDED ) && ( xTicksToWait != 0 ) ) );
??uxEventGroupGetNumber_23:
        CALL      F:_xTaskGetSchedulerState  ;; 3 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_25  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_26  ;; 4 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 10 cycles
//  339 	}
//  340 	#endif
//  341 
//  342 	vTaskSuspendAll();
??uxEventGroupGetNumber_25:
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
//  343 	{
//  344 		const EventBits_t uxCurrentEventBits = pxEventBits->uxEventBits;
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  345 
//  346 		/* Check to see if the wait condition is already met or not. */
//  347 		xWaitConditionMet = prvTestWaitCondition( uxCurrentEventBits, uxBitsToWaitFor, xWaitForAllBits );
//  348 
//  349 		if( xWaitConditionMet != pdFALSE )
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall _prvTestWaitCondition
        CALL      F:_prvTestWaitCondition  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
        OR        A, X               ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_27  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
//  350 		{
//  351 			/* The wait condition has already been met so there is no need to
//  352 			block. */
//  353 			uxReturn = uxCurrentEventBits;
//  354 			xTicksToWait = ( TickType_t ) 0;
//  355 
//  356 			/* Clear the wait bits if requested to do so. */
//  357 			if( xClearOnExit != pdFALSE )
//  358 			{
//  359 				pxEventBits->uxEventBits &= ~uxBitsToWaitFor;
//  360 			}
//  361 			else
//  362 			{
//  363 				mtCOVERAGE_TEST_MARKER();
//  364 			}
//  365 		}
//  366 		else if( xTicksToWait == ( TickType_t ) 0 )
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_28  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
//  367 		{
//  368 			/* The wait condition has not been met, but no block time was
//  369 			specified, so just return the current value. */
//  370 			uxReturn = uxCurrentEventBits;
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        BR        R:??uxEventGroupGetNumber_29  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  371 		}
??uxEventGroupGetNumber_26:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_30  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_30:
        BR        S:??uxEventGroupGetNumber_30  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??uxEventGroupGetNumber_27:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        MOVW      [SP+0x1A], AX      ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_31  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
          CFI FunCall ?L_NOT_L03
        CALL      N:?L_NOT_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
        BR        S:??uxEventGroupGetNumber_31  ;; 3 cycles
        ; ------------------------------------- Block: 29 cycles
//  372 		else
//  373 		{
//  374 			/* The task is going to block to wait for its required bits to be
//  375 			set.  uxControlBits are used to remember the specified behaviour of
//  376 			this call to xEventGroupWaitBits() - for use when the event bits
//  377 			unblock the task. */
//  378 			if( xClearOnExit != pdFALSE )
??uxEventGroupGetNumber_28:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_32  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  379 			{
//  380 				uxControlBits |= eventCLEAR_EVENTS_ON_EXIT_BIT;
        MOVW      AX, #0x100         ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  381 			}
//  382 			else
//  383 			{
//  384 				mtCOVERAGE_TEST_MARKER();
//  385 			}
//  386 
//  387 			if( xWaitForAllBits != pdFALSE )
??uxEventGroupGetNumber_32:
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_33  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  388 			{
//  389 				uxControlBits |= eventWAIT_FOR_ALL_BITS;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        OR        A, #0x4            ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  390 			}
//  391 			else
//  392 			{
//  393 				mtCOVERAGE_TEST_MARKER();
//  394 			}
//  395 
//  396 			/* Store the bits that the calling task is waiting for in the
//  397 			task's event list item so the kernel knows when a match is
//  398 			found.  Then enter the blocked state. */
//  399 			vTaskPlaceOnUnorderedEventList( &( pxEventBits->xTasksWaitingForBits ), ( uxBitsToWaitFor | uxControlBits ), xTicksToWait );
??uxEventGroupGetNumber_33:
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+22
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vTaskPlaceOnUnorderedEventList
        CALL      F:_vTaskPlaceOnUnorderedEventList  ;; 3 cycles
//  400 
//  401 			/* This is obsolete as it will get set after the task unblocks, but
//  402 			some compilers mistakenly generate a warning about the variable
//  403 			being returned without being set if it is not done. */
//  404 			uxReturn = 0;
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+18
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 27 cycles
??uxEventGroupGetNumber_29:
        MOVW      [SP+0x02], AX      ;; 1 cycle
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 1 cycles
//  405 
//  406 			traceEVENT_GROUP_WAIT_BITS_BLOCK( xEventGroup, uxBitsToWaitFor );
//  407 		}
//  408 	}
//  409 	xAlreadyYielded = xTaskResumeAll();
??uxEventGroupGetNumber_31:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
//  410 
//  411 	if( xTicksToWait != ( TickType_t ) 0 )
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??uxEventGroupGetNumber_34  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
//  412 	{
//  413 		if( xAlreadyYielded == pdFALSE )
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  414 		{
//  415 			portYIELD_WITHIN_API();
        BRK                          ;; 5 cycles
          CFI FunCall _uxTaskResetEventItemValue
        ; ------------------------------------- Block: 5 cycles
//  416 		}
//  417 		else
//  418 		{
//  419 			mtCOVERAGE_TEST_MARKER();
//  420 		}
//  421 
//  422 		/* The task blocked to wait for its required bits to be set - at this
//  423 		point either the required bits were set or the block time expired.  If
//  424 		the required bits were set they will have been stored in the task's
//  425 		event list item, and they should now be retrieved then cleared. */
//  426 		uxReturn = uxTaskResetEventItemValue();
??xEventGroupWaitBits_0:
        CALL      F:_uxTaskResetEventItemValue  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
//  427 
//  428 		if( ( uxReturn & eventUNBLOCKED_DUE_TO_BIT_SET ) == ( EventBits_t ) 0 )
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV1      CY, [HL].1         ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??uxEventGroupGetNumber_35  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
//  429 		{
//  430 			taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_36  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_36:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
//  431 			{
//  432 				/* The task timed out, just return the current event bit value. */
//  433 				uxReturn = pxEventBits->uxEventBits;
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  434 
//  435 				/* It is possible that the event bits were updated between this
//  436 				task leaving the Blocked state and running again. */
//  437 				if( prvTestWaitCondition( uxReturn, uxBitsToWaitFor, xWaitForAllBits ) != pdFALSE )
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _prvTestWaitCondition
        CALL      F:_prvTestWaitCondition  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
        OR        A, X               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_37  ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
//  438 				{
//  439 					if( xClearOnExit != pdFALSE )
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_37  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  440 					{
//  441 						pxEventBits->uxEventBits &= ~uxBitsToWaitFor;
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
          CFI FunCall ?L_NOT_L03
        CALL      N:?L_NOT_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
        ; ------------------------------------- Block: 26 cycles
//  442 					}
//  443 					else
//  444 					{
//  445 						mtCOVERAGE_TEST_MARKER();
//  446 					}
//  447 				}
//  448 				else
//  449 				{
//  450 					mtCOVERAGE_TEST_MARKER();
//  451 				}
//  452 			}
//  453 			taskEXIT_CRITICAL();
??uxEventGroupGetNumber_37:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_35  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_35  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  454 
//  455 			/* Prevent compiler warnings when trace macros are not used. */
//  456 			xTimeoutOccurred = pdFALSE;
//  457 		}
//  458 		else
//  459 		{
//  460 			/* The task unblocked because the bits were set. */
//  461 		}
//  462 
//  463 		/* The task blocked so control bits may have been set. */
//  464 		uxReturn &= ~eventEVENT_BITS_CONTROL_BYTES;
??uxEventGroupGetNumber_35:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
//  465 	}
//  466 	traceEVENT_GROUP_WAIT_BITS_END( xEventGroup, uxBitsToWaitFor, xTimeoutOccurred );
//  467 
//  468 	return uxReturn;
??uxEventGroupGetNumber_34:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 432 cycles
//  469 }
//  470 /*-----------------------------------------------------------*/
//  471 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon1
          CFI Function _xEventGroupClearBits
        CODE
//  472 EventBits_t xEventGroupClearBits( EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToClear )
//  473 {
_xEventGroupClearBits:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
//  474 EventGroup_t *pxEventBits = ( EventGroup_t * ) xEventGroup;
//  475 EventBits_t uxReturn;
//  476 
//  477 	/* Check the user is not attempting to clear the bits used by the kernel
//  478 	itself. */
//  479 	configASSERT( xEventGroup );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_38  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??uxEventGroupGetNumber_38:
        BNZ       ??uxEventGroupGetNumber_39  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_40  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_40:
        BR        S:??uxEventGroupGetNumber_40  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  480 	configASSERT( ( uxBitsToClear & eventEVENT_BITS_CONTROL_BYTES ) == 0 );
??uxEventGroupGetNumber_39:
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_41  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_42  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_42:
        BR        S:??uxEventGroupGetNumber_42  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  481 
//  482 	taskENTER_CRITICAL();
??uxEventGroupGetNumber_41:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_43  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_43:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
//  483 	{
//  484 		traceEVENT_GROUP_CLEAR_BITS( xEventGroup, uxBitsToClear );
//  485 
//  486 		/* The value returned is the event group value prior to the bits being
//  487 		cleared. */
//  488 		uxReturn = pxEventBits->uxEventBits;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  489 
//  490 		/* Clear the bits. */
//  491 		pxEventBits->uxEventBits &= ~uxBitsToClear;
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
          CFI FunCall ?L_NOT_L03
        CALL      N:?L_NOT_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  492 	}
//  493 	taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        OR        A, X               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_44  ;; 4 cycles
        ; ------------------------------------- Block: 40 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_44  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 6 cycles
//  494 
//  495 	return uxReturn;
??uxEventGroupGetNumber_44:
        REQUIRE ?Subroutine0
        ; // Fall through to label ?Subroutine0
//  496 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+12
        CODE
?Subroutine0:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
//  497 /*-----------------------------------------------------------*/
//  498 
//  499 #if ( ( configUSE_TRACE_FACILITY == 1 ) && ( INCLUDE_xTimerPendFunctionCall == 1 ) && ( configUSE_TIMERS == 1 ) )
//  500 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon0
          CFI Function _xEventGroupClearBitsFromISR
        CODE
//  501 	BaseType_t xEventGroupClearBitsFromISR( EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToClear )
//  502 	{
_xEventGroupClearBitsFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
//  503 		BaseType_t xReturn;
//  504 
//  505 		traceEVENT_GROUP_CLEAR_BITS_FROM_ISR( xEventGroup, uxBitsToClear );
//  506 		xReturn = xTimerPendFunctionCallFromISR( vEventGroupClearBitsCallback, ( void * ) xEventGroup, ( uint32_t ) uxBitsToClear, NULL );
//  507 
//  508 		return xReturn;
        MOV       L, #0x0            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        MOV       H, #0x0            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_vEventGroupClearBitsCallback)  ;; 1 cycle
        MOV       A, #BYTE3(_vEventGroupClearBitsCallback)  ;; 1 cycle
          CFI FunCall _xTimerPendFunctionCallFromISR
        CALL      F:_xTimerPendFunctionCallFromISR  ;; 3 cycles
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 30 cycles
        ; ------------------------------------- Total: 30 cycles
//  509 	}
//  510 
//  511 #endif
//  512 /*-----------------------------------------------------------*/
//  513 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon1
          CFI Function _xEventGroupGetBitsFromISR
          CFI NoCalls
        CODE
//  514 EventBits_t xEventGroupGetBitsFromISR( EventGroupHandle_t xEventGroup )
//  515 {
_xEventGroupGetBitsFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
//  516 UBaseType_t uxSavedInterruptStatus;
//  517 EventGroup_t *pxEventBits = ( EventGroup_t * ) xEventGroup;
//  518 EventBits_t uxReturn;
//  519 
//  520 	uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
//  521 	{
//  522 		uxReturn = pxEventBits->uxEventBits;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  523 	}
//  524 	portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
        MOV       A, B               ;; 1 cycle
        ROL       A, 0x1             ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
//  525 
//  526 	return uxReturn;
        BR        F:?Subroutine0     ;; 3 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 35 cycles
        ; ------------------------------------- Total: 35 cycles
//  527 }
//  528 /*-----------------------------------------------------------*/
//  529 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon1
          CFI Function _xEventGroupSetBits
        CODE
//  530 EventBits_t xEventGroupSetBits( EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToSet )
//  531 {
_xEventGroupSetBits:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 32
        SUBW      SP, #0x1C          ;; 1 cycle
          CFI CFA SP+36
//  532 ListItem_t *pxListItem, *pxNext;
//  533 ListItem_t const *pxListEnd;
//  534 List_t *pxList;
//  535 EventBits_t uxBitsToClear = 0, uxBitsWaitedFor, uxControlBits;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  536 EventGroup_t *pxEventBits = ( EventGroup_t * ) xEventGroup;
//  537 BaseType_t xMatchFound = pdFALSE;
//  538 
//  539 	/* Check the user is not attempting to set the bits used by the kernel
//  540 	itself. */
//  541 	configASSERT( xEventGroup );
        MOV       A, [SP+0x1E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_45  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??uxEventGroupGetNumber_45:
        BNZ       ??uxEventGroupGetNumber_46  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_47  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_47:
        BR        S:??uxEventGroupGetNumber_47  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  542 	configASSERT( ( uxBitsToSet & eventEVENT_BITS_CONTROL_BYTES ) == 0 );
??uxEventGroupGetNumber_46:
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_48  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_49  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_49:
        BR        S:??uxEventGroupGetNumber_49  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  543 
//  544 	pxList = &( pxEventBits->xTasksWaitingForBits );
??uxEventGroupGetNumber_48:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  545 	pxListEnd = listGET_END_MARKER( pxList ); /*lint !e826 !e740 The mini list structure is used as the list end to save RAM.  This is checked and valid. */
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  546 	vTaskSuspendAll();
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
//  547 	{
//  548 		traceEVENT_GROUP_SET_BITS( xEventGroup, uxBitsToSet );
//  549 
//  550 		pxListItem = listGET_HEAD_ENTRY( pxList );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
//  551 
//  552 		/* Set the bits. */
//  553 		pxEventBits->uxEventBits |= uxBitsToSet;
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+36
        ; ------------------------------------- Block: 47 cycles
//  554 
//  555 		/* See if the new bit value should unblock any tasks. */
//  556 		while( pxListItem != pxListEnd )
??xEventGroupSetBits_0:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_50  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??uxEventGroupGetNumber_50:
        SKNZ                         ;; 4 cycles
        BR        R:??uxEventGroupGetNumber_51  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  557 		{
//  558 			pxNext = listGET_NEXT( pxListItem );
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x1A], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x18], AX      ;; 1 cycle
//  559 			uxBitsWaitedFor = listGET_LIST_ITEM_VALUE( pxListItem );
//  560 			xMatchFound = pdFALSE;
//  561 
//  562 			/* Split the bits waited for from the control bits. */
//  563 			uxControlBits = uxBitsWaitedFor & eventEVENT_BITS_CONTROL_BYTES;
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x0E], AX      ;; 1 cycle
//  564 			uxBitsWaitedFor &= ~eventEVENT_BITS_CONTROL_BYTES;
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x12], AX      ;; 1 cycle
//  565 
//  566 			if( ( uxControlBits & eventWAIT_FOR_ALL_BITS ) == ( EventBits_t ) 0 )
        MOV       A, [SP+0x1E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+36
        MOVW      [SP+0x14], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x16], AX      ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].2, ??uxEventGroupGetNumber_52  ;; 5 cycles
        ; ------------------------------------- Block: 55 cycles
        BR        S:??uxEventGroupGetNumber_53  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  567 			{
//  568 				/* Just looking for single bit being set. */
//  569 				if( ( uxBitsWaitedFor & pxEventBits->uxEventBits ) != ( EventBits_t ) 0 )
//  570 				{
//  571 					xMatchFound = pdTRUE;
//  572 				}
//  573 				else
//  574 				{
//  575 					mtCOVERAGE_TEST_MARKER();
//  576 				}
//  577 			}
//  578 			else if( ( uxBitsWaitedFor & pxEventBits->uxEventBits ) == uxBitsWaitedFor )
??uxEventGroupGetNumber_52:
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, [SP+0x18]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+36
        BNZ       ??uxEventGroupGetNumber_54  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
//  579 			{
//  580 				/* All bits are set. */
//  581 				xMatchFound = pdTRUE;
//  582 			}
//  583 			else
//  584 			{
//  585 				/* Need all bits to be set, but not all the bits were set. */
//  586 			}
//  587 
//  588 			if( xMatchFound != pdFALSE )
//  589 			{
//  590 				/* The bits match.  Should the bits be cleared on exit? */
//  591 				if( ( uxControlBits & eventCLEAR_EVENTS_ON_EXIT_BIT ) != ( EventBits_t ) 0 )
??xEventGroupSetBits_1:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].0, ??uxEventGroupGetNumber_55  ;; 5 cycles
        ; ------------------------------------- Block: 11 cycles
//  592 				{
//  593 					uxBitsToClear |= uxBitsWaitedFor;
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+36
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
//  594 				}
//  595 				else
//  596 				{
//  597 					mtCOVERAGE_TEST_MARKER();
//  598 				}
//  599 
//  600 				/* Store the actual event flag value in the task's event list
//  601 				item before removing the task from the event list.  The
//  602 				eventUNBLOCKED_DUE_TO_BIT_SET bit is set so the task knows
//  603 				that is was unblocked due to its required bits matching, rather
//  604 				than because it timed out. */
//  605 				( void ) xTaskRemoveFromUnorderedEventList( pxListItem, pxEventBits->uxEventBits | eventUNBLOCKED_DUE_TO_BIT_SET );
??uxEventGroupGetNumber_55:
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        XCH       A, B               ;; 1 cycle
        OR        A, #0x2            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromUnorderedEventList
        CALL      F:_xTaskRemoveFromUnorderedEventList  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+36
        BR        S:??uxEventGroupGetNumber_54  ;; 3 cycles
        ; ------------------------------------- Block: 24 cycles
//  606 			}
??uxEventGroupGetNumber_53:
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??xEventGroupSetBits_1  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  607 
//  608 			/* Move onto the next list item.  Note pxListItem->pxNext is not
//  609 			used here as the list item may have been removed from the event list
//  610 			and inserted into the ready/pending reading list. */
//  611 			pxListItem = pxNext;
??uxEventGroupGetNumber_54:
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        R:??xEventGroupSetBits_0  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
//  612 		}
??uxEventGroupGetNumber_51:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
//  613 
//  614 		/* Clear any bits that matched when the eventCLEAR_EVENTS_ON_EXIT_BIT
//  615 		bit was set in the control word. */
//  616 		pxEventBits->uxEventBits &= ~uxBitsToClear;
          CFI FunCall ?L_NOT_L03
        CALL      N:?L_NOT_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  617 	}
//  618 	( void ) xTaskResumeAll();
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  619 
//  620 	return pxEventBits->uxEventBits;
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      SP, #0x24          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 44 cycles
        ; ------------------------------------- Total: 331 cycles
//  621 }
//  622 /*-----------------------------------------------------------*/
//  623 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon0
          CFI Function _vEventGroupDelete
        CODE
//  624 void vEventGroupDelete( EventGroupHandle_t xEventGroup )
//  625 {
_vEventGroupDelete:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
//  626 EventGroup_t *pxEventBits = ( EventGroup_t * ) xEventGroup;
//  627 const List_t *pxTasksWaitingForBits = &( pxEventBits->xTasksWaitingForBits );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  628 
//  629 	vTaskSuspendAll();
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        BR        S:??uxEventGroupGetNumber_56  ;; 3 cycles
        ; ------------------------------------- Block: 19 cycles
//  630 	{
//  631 		traceEVENT_GROUP_DELETE( xEventGroup );
//  632 
//  633 		while( listCURRENT_LIST_LENGTH( pxTasksWaitingForBits ) > ( UBaseType_t ) 0 )
//  634 		{
//  635 			/* Unblock the task, returning 0 as the event list is being deleted
//  636 			and	cannot therefore have any bits set. */
//  637 			configASSERT( pxTasksWaitingForBits->xListEnd.pxNext != ( ListItem_t * ) &( pxTasksWaitingForBits->xListEnd ) );
//  638 			( void ) xTaskRemoveFromUnorderedEventList( pxTasksWaitingForBits->xListEnd.pxNext, eventUNBLOCKED_DUE_TO_BIT_SET );
??vEventGroupDelete_0:
        MOVW      AX, #0x200         ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromUnorderedEventList
        CALL      F:_xTaskRemoveFromUnorderedEventList  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        ; ------------------------------------- Block: 11 cycles
??uxEventGroupGetNumber_56:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_57  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_58  ;; 4 cycles
        ; ------------------------------------- Block: 30 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??uxEventGroupGetNumber_58:
        BNZ       ??vEventGroupDelete_0  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??uxEventGroupGetNumber_59  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??uxEventGroupGetNumber_59:
        BR        S:??uxEventGroupGetNumber_59  ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
//  639 		}
//  640 
//  641 		#if( ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 0 ) )
//  642 		{
//  643 			/* The event group can only have been allocated dynamically - free
//  644 			it again. */
//  645 			vPortFree( pxEventBits );
//  646 		}
//  647 		#elif( ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 1 ) )
//  648 		{
//  649 			/* The event group could have been allocated statically or
//  650 			dynamically, so check before attempting to free the memory. */
//  651 			if( pxEventBits->ucStaticallyAllocated == ( uint8_t ) pdFALSE )
//  652 			{
//  653 				vPortFree( pxEventBits );
//  654 			}
//  655 			else
//  656 			{
//  657 				mtCOVERAGE_TEST_MARKER();
//  658 			}
//  659 		}
//  660 		#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
//  661 	}
//  662 	( void ) xTaskResumeAll();
??uxEventGroupGetNumber_57:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  663 }
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 108 cycles
//  664 /*-----------------------------------------------------------*/
//  665 
//  666 /* For internal use only - execute a 'set bits' command that was pended from
//  667 an interrupt. */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon0
          CFI Function _vEventGroupSetBitsCallback
        CODE
//  668 void vEventGroupSetBitsCallback( void *pvEventGroup, const uint32_t ulBitsToSet )
//  669 {
_vEventGroupSetBitsCallback:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
//  670 	( void ) xEventGroupSetBits( pvEventGroup, ( EventBits_t ) ulBitsToSet );
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xEventGroupSetBits
        CALL      F:_xEventGroupSetBits  ;; 3 cycles
//  671 }
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 21 cycles
        ; ------------------------------------- Total: 21 cycles
//  672 /*-----------------------------------------------------------*/
//  673 
//  674 /* For internal use only - execute a 'clear bits' command that was pended from
//  675 an interrupt. */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon0
          CFI Function _vEventGroupClearBitsCallback
        CODE
//  676 void vEventGroupClearBitsCallback( void *pvEventGroup, const uint32_t ulBitsToClear )
//  677 {
_vEventGroupClearBitsCallback:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
//  678 	( void ) xEventGroupClearBits( pvEventGroup, ( EventBits_t ) ulBitsToClear );
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xEventGroupClearBits
        CALL      F:_xEventGroupClearBits  ;; 3 cycles
//  679 }
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 21 cycles
        ; ------------------------------------- Total: 21 cycles
//  680 /*-----------------------------------------------------------*/
//  681 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon1
          CFI Function _prvTestWaitCondition
        CODE
//  682 static BaseType_t prvTestWaitCondition( const EventBits_t uxCurrentEventBits, const EventBits_t uxBitsToWaitFor, const BaseType_t xWaitForAllBits )
//  683 {
_prvTestWaitCondition:
        ; * Stack frame (at entry) *
        ; Param size: 4
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
//  684 BaseType_t xWaitConditionMet = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
//  685 
//  686 	if( xWaitForAllBits == pdFALSE )
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_60  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
//  687 	{
//  688 		/* Task only has to wait for one bit within uxBitsToWaitFor to be
//  689 		set.  Is one already set? */
//  690 		if( ( uxCurrentEventBits & uxBitsToWaitFor ) != ( EventBits_t ) 0 )
        MOVW      AX, [SP]           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_61  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BR        S:??uxEventGroupGetNumber_62  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  691 		{
//  692 			xWaitConditionMet = pdTRUE;
//  693 		}
//  694 		else
//  695 		{
//  696 			mtCOVERAGE_TEST_MARKER();
//  697 		}
//  698 	}
//  699 	else
//  700 	{
//  701 		/* Task has to wait for all the bits in uxBitsToWaitFor to be set.
//  702 		Are they set already? */
//  703 		if( ( uxCurrentEventBits & uxBitsToWaitFor ) == uxBitsToWaitFor )
??uxEventGroupGetNumber_60:
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 10 cycles
//  704 		{
//  705 			xWaitConditionMet = pdTRUE;
??uxEventGroupGetNumber_61:
        INCW      DE                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  706 		}
//  707 		else
//  708 		{
//  709 			mtCOVERAGE_TEST_MARKER();
//  710 		}
//  711 	}
//  712 
//  713 	return xWaitConditionMet;
??uxEventGroupGetNumber_62:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 54 cycles
//  714 }
//  715 /*-----------------------------------------------------------*/
//  716 
//  717 #if ( ( configUSE_TRACE_FACILITY == 1 ) && ( INCLUDE_xTimerPendFunctionCall == 1 ) && ( configUSE_TIMERS == 1 ) )
//  718 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon0
          CFI Function _xEventGroupSetBitsFromISR
        CODE
//  719 	BaseType_t xEventGroupSetBitsFromISR( EventGroupHandle_t xEventGroup, const EventBits_t uxBitsToSet, BaseType_t *pxHigherPriorityTaskWoken )
//  720 	{
_xEventGroupSetBitsFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 8
//  721 	BaseType_t xReturn;
//  722 
//  723 		traceEVENT_GROUP_SET_BITS_FROM_ISR( xEventGroup, uxBitsToSet );
//  724 		xReturn = xTimerPendFunctionCallFromISR( vEventGroupSetBitsCallback, ( void * ) xEventGroup, ( uint32_t ) uxBitsToSet, pxHigherPriorityTaskWoken );
//  725 
//  726 		return xReturn;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_vEventGroupSetBitsCallback)  ;; 1 cycle
        MOV       A, #BYTE3(_vEventGroupSetBitsCallback)  ;; 1 cycle
          CFI FunCall _xTimerPendFunctionCallFromISR
        CALL      F:_xTimerPendFunctionCallFromISR  ;; 3 cycles
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 37 cycles
        ; ------------------------------------- Total: 37 cycles
//  727 	}
//  728 
//  729 #endif
//  730 /*-----------------------------------------------------------*/
//  731 
//  732 #if (configUSE_TRACE_FACILITY == 1)
//  733 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon0
          CFI Function _uxEventGroupGetNumber
          CFI NoCalls
        CODE
//  734 	UBaseType_t uxEventGroupGetNumber( void* xEventGroup )
//  735 	{
_uxEventGroupGetNumber:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  736 	UBaseType_t xReturn;
//  737 	EventGroup_t *pxEventBits = ( EventGroup_t * ) xEventGroup;
//  738 
//  739 		if( xEventGroup == NULL )
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??uxEventGroupGetNumber_63  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??uxEventGroupGetNumber_63:
        BNZ       ??uxEventGroupGetNumber_64  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  740 		{
//  741 			xReturn = 0;
        CLRW      AX                 ;; 1 cycle
        BR        S:??uxEventGroupGetNumber_65  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  742 		}
//  743 		else
//  744 		{
//  745 			xReturn = pxEventBits->uxEventGroupNumber;
??uxEventGroupGetNumber_64:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x16          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ; ------------------------------------- Block: 5 cycles
//  746 		}
//  747 
//  748 		return xReturn;
??uxEventGroupGetNumber_65:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 35 cycles
//  749 	}

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//  750 
//  751 #endif
//  752 
// 
// 2 075 bytes in section .textf
// 
// 2 075 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
