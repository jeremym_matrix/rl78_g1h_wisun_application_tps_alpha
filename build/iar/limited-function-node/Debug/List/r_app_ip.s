///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:31:02
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\r_app_ip.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW37A3.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\r_app_ip.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\r_app_ip.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_file_descriptor", "0"
        RTMODEL "__dlib_full_locale_support", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _r_auth_certs
        EXTERN _AppCmdBuf
        EXTERN ?C_LSH_L01
        EXTERN ?FAR_NEAR_MOVE_LONG_L06
        EXTERN ?L_IOR_L03
        EXTERN ?MEMCPY_FAR
        EXTERN ?SI_CMP_L02
        EXTERN _AppCmd_SkipWhiteSpace
        EXTERN _AppHexStrToNum
        EXTERN _AppStricmp
        EXTERN _ArrToUInt16
        EXTERN _R_APL_ClearAplGlobal
        EXTERN _R_APL_GetAplGlobal
        EXTERN _R_AUTH_DeleteGTKs
        EXTERN _R_AUTH_SUP_ClearGtkValidity
        EXTERN _R_AUTH_SUP_Init
        EXTERN _R_AUTH_SUP_ProcessEapolMessage
        EXTERN _R_AUTH_SUP_ProcessRelayMessage
        EXTERN _R_AUTH_SUP_Reset
        EXTERN _R_AUTH_SUP_StartJoin
        EXTERN _R_ICMP_EchoRequest
        EXTERN _R_IE_BroadcastScheduleCreate
        EXTERN _R_IE_BroadcastScheduleParse
        EXTERN _R_IE_ScheduleNumberOfMaskBytes
        EXTERN _R_IE_UnicastScheduleCreate
        EXTERN _R_IE_UnicastScheduleParse
        EXTERN _R_LOG_GetLog
        EXTERN _R_LOG_GetVersionID
        EXTERN _R_LOG_Init
        EXTERN _R_LOG_SetSeverityThreshold
        EXTERN _R_MemCmpZero
        EXTERN _R_Modem_print
        EXTERN _R_NWK_EapolDataProcessed
        EXTERN _R_NWK_GetAttrLength
        EXTERN _R_NWK_GetRequest
        EXTERN _R_NWK_GetRequestMultipart
        EXTERN _R_NWK_GetRequestVarSize
        EXTERN _R_NWK_IP_DataRequest
        EXTERN _R_NWK_Init
        EXTERN _R_NWK_JoinRequest
        EXTERN _R_NWK_LeaveNetworkRequest
        EXTERN _R_NWK_ResetRequest
        EXTERN _R_NWK_Resume
        EXTERN _R_NWK_SetRequest
        EXTERN _R_NWK_Suspend
        EXTERN _R_OS_GetTaskHandle
        EXTERN _R_Swap64
        EXTERN _R_UDP_DataRequest
        EXTERN _R_memcmp
        EXTERN _R_memcpy
        EXTERN _R_memset
        EXTERN _UInt16ToArr
        EXTERN _clock_seconds
        EXTERN _memmove
        EXTERN _memset
        EXTERN _pAppGetMacAddr
        EXTERN _r_log_append
        EXTERN _r_log_finish
        EXTERN _r_log_start
        EXTERN _umm_realloc
        EXTERN _uxTaskGetStackHighWaterMark
        EXTERN _vTaskSuspendAll
        EXTERN _xTaskResumeAll

        PUBLIC _AppCmd_Init
        PUBLIC _AppIpProcessCmd
        PUBLIC _AppIpReceiveMessage
        PUBLIC _FAN_STACK_VERSION
        PUBLIC _R_DHCPV6_VendorOptionRecvCallback
        PUBLIC _R_DHCPV6_VendorOptionSendCallback
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\r_app_ip.c
//    1 /******************************************************************************
//    2 * DISCLAIMER
//    3 * This software is supplied by Renesas Electronics Corporation and is only
//    4 * intended for use with Renesas products. No other uses are authorized.
//    5 * This software is owned by Renesas Electronics Corporation and is protected under
//    6 * all applicable laws, including copyright laws.
//    7 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
//    8 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
//    9 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//   10 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
//   11 * DISCLAIMED.
//   12 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   13 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   14 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
//   15 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
//   16 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   17 * Renesas reserves the right, without notice, to make changes to this
//   18 * software and to discontinue the availability of this software.
//   19 * By using this software, you agree to the additional terms and
//   20 * conditions found by accessing the following link:
//   21 * http://www.renesas.com/disclaimer
//   22 ******************************************************************************/
//   23 
//   24 /* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.  */
//   25 
//   26 /******************************************************************************
//   27 * File Name    : r_app_ip.c
//   28 * Version      : 1.00
//   29 * Device(s)    :
//   30 * Tool-Chain   : CubeSuite+
//   31 * H/W Platform :
//   32 * Description  : Sample application program
//   33 ******************************************************************************
//   34 * History : DD.MM.YYYY Version Description
//   35 *         : 25.08.2014 1.00    First Release
//   36 ******************************************************************************/
//   37 
//   38 /******************************************************************************
//   39    Includes   <System Includes> , "Project Includes"
//   40  ******************************************************************************/
//   41 #include <stdio.h>
//   42 #include <stdlib.h>
//   43 #include <string.h>
//   44 
//   45 #include "r_app_main.h"
//   46 #include "r_apl_global.h"
//   47 #include "r_nwk_api.h"
//   48 #include "r_icmpv6.h"
//   49 #include "sys/clock.h"
//   50 #include "mac_intr.h"
//   51 
//   52 #include "r_modem.h"
//   53 #include "r_modem_demo_msg.h"
//   54 
//   55 #include "r_byte_swap.h"
//   56 #include "r_impl_utils.h"
//   57 #include "r_mac_ie.h"
//   58 #include "r_mem_tools.h"
//   59 
//   60 #ifdef R_NETWORK_RACK
//   61 #include "lib/random.h"
//   62 #endif
//   63 
//   64 #if R_DEV_TBU_ENABLED && __RX
//   65 #include "iodefine.h"
//   66 #endif
//   67 
//   68 #if !R_DEV_DISABLE_AUTH
//   69 #include "r_auth_config.h"
//   70 #include "r_auth_sup.h"
//   71 #include "r_auth_common.h"
//   72 #include "r_auth_certs_config.h"
//   73 #if R_BR_AUTHENTICATOR_ENABLED
//   74 #include "r_auth_br.h"
//   75 #endif
//   76 #include "r_heap.h"
//   77 #endif
//   78 
//   79 #if R_BORDER_ROUTER_ENABLED
//   80 #if R_DHCPV6_SERVER_ENABLED
//   81 #include "r_dhcpv6.h"
//   82 #endif
//   83 #endif
//   84 
//   85 #ifdef UMM_INFO
//   86 #include "umm_malloc.h"
//   87 #endif // UMM_INFO
//   88 
//   89 // R_LOG_PREFIX is processed by RLog code generator
//   90 #define R_LOG_PREFIX APP
//   91 #include "r_log_internal.h"
//   92 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
//   93 #include "r_loggen_r_app_ip.h"
//   94 #endif
//   95 #include "r_log.h"
//   96 #include "r_log_flags.h"
//   97 
//   98 #if RTOS_USE == 1
//   99 #include "FreeRTOS.h"
//  100 #include "task.h"
//  101 #endif
//  102 
//  103 #define STACK_NAME "Wi-SUN-FAN V1.4.1"
//  104 
//  105 /******************************************************************************
//  106    Version definitions
//  107 ******************************************************************************/
//  108 #if defined(VIZMO_MODULE_28dBm)
//  109 const uint8_t FAN_STACK_VERSION[] = STACK_NAME "-VIZMO+28dBm";
//  110 #elif defined(FEM_SE2435L)
//  111 const uint8_t FAN_STACK_VERSION[] = STACK_NAME "-FEM_SE2435L";
//  112 #elif (R_WISUN_FAN_VERSION >= 110)
//  113 const uint8_t FAN_STACK_VERSION[] = STACK_NAME "-TPS1.1-Alpha";
//  114 #else
//  115 const uint8_t FAN_STACK_VERSION[] = STACK_NAME;
//  116 #endif
//  117 
//  118 /******************************************************************************
//  119    Macro definitions
//  120 ******************************************************************************/
//  121 #define APP_CMD_MAC_Channel_Default (0x00)
//  122 #define APP_CMD_MAC_PanId_Default   (0xffff)
//  123 
//  124 /** Example value for the UDP port of special "Kick" messages (must not interfere with Wi-SUN FAN UDP ports) */
//  125 #define KICK_DEVICE_MSG_UDP_PORT    3611
//  126 
//  127 /** Example value for the payload of a "Kick" message */
//  128 #define KICK_DEVICE_MSG_PAYLOAD     { 0xAB, 0xCD, 0xEF }
//  129 
//  130 #ifndef R_LOG_BUFFER_SIZE
//  131 
//  132 /** The size of the static buffer for the logging framework to store log records internally */
//  133 #define R_LOG_BUFFER_SIZE 256
//  134 #endif
//  135 
//  136 /** The size of the temporary buffer that is used to poll log records from the logging framework */
//  137 #define R_LOG_POLL_BUFFER_SIZE 256
//  138 
//  139 /******************************************************************************
//  140    Typedef definitions
//  141 ******************************************************************************/
//  142 typedef struct
//  143 {
//  144     uint8_t      ntfHandle;
//  145     uint8_t      deviceType;
//  146     char         networkName[R_NETWORK_NAME_STRING_MAX_BYTES];
//  147     uint16_t     panId;
//  148     uint16_t     panSize;
//  149     uint8_t      useParent_BS_IE;
//  150 
//  151     /* The following parameters are only configurable for border routers */
//  152     r_ipv6addr_t globalIpAddress;  //!< The GUA/LUA to be set on the BR and the local DHCP server (contains network prefix)
//  153 } r_app_config_t;
//  154 
//  155 typedef struct
//  156 {
//  157     const uint8_t __far* pCmd;
//  158     void (* pFunc)(uint8_t* pCmd);
//  159 } r_app_cmd_func_t;
//  160 
//  161 typedef union
//  162 {
//  163     r_ie_wp_serialized_sched_t schedule;
//  164     uint8_t placeholder[sizeof(r_ie_wp_serialized_sched_t) + R_SERIALIZED_SCHEDULE_IE_BYTES_MAX];
//  165 } r_app_schedule_buffer_t;  // Buffer for a serialized schedule that is passed between application and NWK layer
//  166 
//  167 typedef union
//  168 {
//  169     r_nwk_mac_whitelist_t whitelist;
//  170     uint8_t               placeholder[sizeof(r_nwk_mac_whitelist_t) + R_WHITELIST_TABLE_LENGTH * sizeof(r_eui64_t)];
//  171 } r_app_whitelist_buffer_t;  // Buffer for a MAC whitelist that is passed between application and NWK layer
//  172 
//  173 typedef union
//  174 {
//  175     r_nwk_nd_cache_t nd_cache;
//  176     uint8_t          placeholder[sizeof(r_nwk_nd_cache_t) + R_NWK_MAX_NEIGHBORS_PER_CONFIRM * sizeof(r_nwk_nd_cache_entry_t)];
//  177 } r_app_nd_cache_buffer_t;  // Buffer for the Neighbor Discovery cache that is passed between application and NWK layer
//  178 
//  179 #if R_BORDER_ROUTER_ENABLED
//  180 typedef union
//  181 {
//  182     r_nwk_routing_entries_t routingTable;
//  183     uint8_t                 placeholder[sizeof(r_nwk_routing_entries_t) + R_NWK_MAX_ROUTES_PER_CONFIRM * sizeof(r_nwk_route_t)];
//  184 } r_app_routing_table_buffer_t;  // Buffer for the source routes that are passed between application and NWK layer
//  185 #endif
//  186 
//  187 /******************************************************************************
//  188    Imported global variables and functions (from other files)
//  189 ******************************************************************************/
//  190 extern unsigned char AppHexStrToNum(unsigned char** ppBuf, void* pData, short dataSize, unsigned char isOctetStr);
//  191 
//  192 extern const uint8_t* pAppGetMacAddr(void);
//  193 
//  194 #if R_DEV_TBU_ENABLED && __RX
//  195 extern uint8_t rstr_handle;
//  196 #endif
//  197 
//  198 /******************************************************************************
//  199    Exported global variables and functions (to be accessed by other files)
//  200 ******************************************************************************/
//  201 void AppCmd_Init();
//  202 
//  203 /******************************************************************************
//  204    Private global variables
//  205 ******************************************************************************/
//  206 static r_apl_global_t* p_aplGlobal;
//  207 static r_app_config_t AppCmdConfig;
//  208 

        SECTION `.dataf`:DATA:REORDER:NOROOT(1)
//  209 static uint16_t featureMask = 0
_featureMask:
        DATA16
        DW 528
//  210 #if R_BORDER_ROUTER_ENABLED
//  211                               | (1 << 0)
//  212 #endif
//  213 #if R_BR_AUTHENTICATOR_ENABLED
//  214                               | (1 << 1)
//  215 #endif
//  216 #if R_DHCPV6_SERVER_ENABLED
//  217                               | (1 << 2)
//  218 #endif
//  219 #if R_MPL_SEED_ENABLED
//  220                               | (1 << 3)
//  221 #endif
//  222 #if R_LEAF_NODE_ENABLED
//  223                               | (1 << 4)
//  224 #endif
//  225 #if (!R_BORDER_ROUTER_ENABLED && !R_LEAF_NODE_ENABLED)
//  226                               | (1 << 5)
//  227 #endif
//  228 #if R_DEV_TBU_ENABLED && __RX
//  229                               | (1 << 8)
//  230 #endif
//  231 #if R_DEV_FAST_JOIN
//  232                               | (1 << 9)
//  233 #endif
//  234 #if R_DEV_DISABLE_SECURITY
//  235                               | (1 << 11)
//  236 #endif
//  237 #if R_DEV_DETERMINISTIC_RANDOM
//  238                               | (1 << 12)
//  239 #endif
//  240 ;
//  241 
//  242 /* Command */
//  243 static const uint8_t AppCmdStr_RSTR[] = "RSTR";
//  244 static const uint8_t AppCmdStr_STARTR[] = "STARTR";
//  245 static const uint8_t AppCmdStr_IPSR[] = "IPSR";
//  246 static const uint8_t AppCmdStr_UDPSR[] = "UDPSR";
//  247 static const uint8_t AppCmdStr_ICMPSR[] = "ICMPSR";
//  248 static const uint8_t AppCmdStr_SPDR[] = "SPDR";
//  249 static const uint8_t AppCmdStr_RSMR[] = "RSMR";
//  250 static const uint8_t AppCmdStr_REVOKE_KEYS[] = "REVOKEKEYSR";
//  251 static const uint8_t AppCmdStr_REVOKE_SUPP[] = "REVOKESUPPR";
//  252 static const uint8_t AppCmdStr_DEVICEKICKR[] = "DEVICEKICKR";
//  253 static const uint8_t AppCmdStr_LEAVE_NETWORKR[] = "LEAVENETWORKR";
//  254 static const uint8_t AppCmdStr_PIB_GETR[] = "PIB_GETR";
//  255 static const uint8_t AppCmdStr_PIB_SETR[] = "PIB_SETR";
//  256 static const uint8_t AppCmdStr_IPV6PREFIX_SETR[] = "IPV6PREFIX_SETR";
//  257 static const uint8_t AppCmdStr_DEVCONF_GETR[] = "DEVCONF_GETR";
//  258 static const uint8_t AppCmdStr_DEVCONF_SETR[] = "DEVCONF_SETR";
//  259 static const uint8_t AppCmdStr_PHYFAN_GETR[] = "PHYFAN_GETR";
//  260 static const uint8_t AppCmdStr_PHYFAN_SETR[] = "PHYFAN_SETR";
//  261 static const uint8_t AppCmdStr_PHYNET_GETR[] = "PHYNET_GETR";
//  262 static const uint8_t AppCmdStr_PHYNET_SETR[] = "PHYNET_SETR";
//  263 static const uint8_t AppCmdStr_UCSCH_GETR[] = "UCSCH_GETR";
//  264 static const uint8_t AppCmdStr_UCSCH_SETR[] = "UCSCH_SETR";
//  265 static const uint8_t AppCmdStr_BCSCH_GETR[] = "BCSCH_GETR";
//  266 static const uint8_t AppCmdStr_BCSCH_SETR[] = "BCSCH_SETR";
//  267 static const uint8_t AppCmdStr_WHTLST_GETR[] = "WHTLST_GETR";
//  268 static const uint8_t AppCmdStr_WHTLST_SETR[] = "WHTLST_SETR";
//  269 static const uint8_t AppCmdStr_GTKS_GETR[] = "GTKS_GETR";
//  270 static const uint8_t AppCmdStr_GTKS_SETR[] = "GTKS_SETR";
//  271 static const uint8_t AppCmdStr_LGTKS_GETR[] = "LGTKS_GETR";
//  272 static const uint8_t AppCmdStr_LGTKS_SETR[] = "LGTKS_SETR";
//  273 static const uint8_t AppCmdStr_KEYLIFETIMES_SETR[] = "KEYLIFETIMES_SETR";
//  274 static const uint8_t AppCmdStr_CERT_SWITCHR[] = "CERT_SWITCHR";
//  275 static const uint8_t AppCmdStr_VERBOSE_SETR[] = "VERBOSE_SETR";
//  276 static const uint8_t AppCmdStr_LOGVERSION_GETR[] = "LOGVERSION_GETR";
//  277 static const uint8_t AppCmdStr_LOGENTRIES_GETR[] = "LOGENTRIES_GETR";
//  278 static const uint8_t AppCmdStr_SUBSCP_SETR[] = "SUBSCP_SETR";
//  279 static const uint8_t AppCmdStr_PAN_VERSION_INCR[] = "PAN_VERSION_INCR";
//  280 static const uint8_t AppCmdStr_RPL_ROUTES_REFRESHR[] = "RPL_ROUTES_REFRESHR";
//  281 static const uint8_t AppCmdStr_RPL_GLOBALREPAIRR[] = "RPL_GLOBALREPAIRR";
//  282 static const uint8_t AppCmdStr_ROUTELST_GETR[] = "ROUTELST_GETR";
//  283 static const uint8_t AppCmdStr_RPLINFO_GETR[] = "RPLINFO_GETR";
//  284 static const uint8_t AppCmdStr_NDCACHE_GETR[] = "NDCACHE_GETR";
//  285 static const uint8_t AppCmdStr_VERSION_GETR[] = "VERSION_GETR";
//  286 static const uint8_t AppCmdStr_STATUS_GETR[] = "STATUS_GETR";
//  287 static const uint8_t AppCmdStr_TASKSTATUS_GETR[] = "TASKSTATUS_GETR";
//  288 #ifdef UMM_INFO
//  289 static const uint8_t AppCmdStr_MEMMARK_GETR[] = "MEMMARK_GETR";
//  290 static const uint8_t AppCmdStr_MEMINFO_GETR[] = "MEMINFO_GETR";
//  291 static const uint8_t AppCmdStr_MEMMAP_GETR[] = "MEMMAP_GETR";
//  292 #endif // UMM_INFO
//  293 #if R_PHY_TYPE_CWX_M && R_MDR_ENABLED
//  294 static const uint8_t AppCmdStr_MDRSR[] = "MDRSR";
//  295 #endif // R_PHY_TYPE_CWX_M && R_MDR_ENABLED
//  296 
//  297 /******************************************************************************
//  298    Private function prototypes
//  299 ******************************************************************************/
//  300 // static
//  301 static void       AppCmd_Reset(void);
//  302 static r_result_t AppCmd_LeaveNetwork();
//  303 
//  304 /* Command */
//  305 static void AppCmd_ProcessCmd_RSTR(uint8_t* pCmd);
//  306 static void AppCmd_ProcessCmd_STARTR(uint8_t* pCmd);
//  307 static void AppCmd_ProcessCmd_IPSR(uint8_t* pCmd);
//  308 static void AppCmd_ProcessCmd_UDPSR(uint8_t* pCmd);
//  309 static void AppCmd_ProcessCmd_ICMPSR(uint8_t* pCmd);
//  310 static void AppCmd_ProcessCmd_SPDR(uint8_t* pCmd);
//  311 static void AppCmd_ProcessCmd_RSMR(uint8_t* pCmd);
//  312 static void AppCmd_ProcessCmd_REVOKEKEYSR(uint8_t* pCmd);
//  313 static void AppCmd_ProcessCmd_REVOKESUPPR(uint8_t* pCmd);
//  314 static void AppCmd_ProcessCmd_DEVICEKICKR(uint8_t* pCmd);
//  315 static void AppCmd_ProcessCmd_LEAVENETWORKR(uint8_t* pCmd);
//  316 static void AppCmd_ProcessCmd_PIB_GETR(uint8_t* pCmd);
//  317 static void AppCmd_ProcessCmd_PIB_SETR(uint8_t* pCmd);
//  318 static void AppCmd_ProcessCmd_IPV6PREFIX_SETR(uint8_t* pCmd);
//  319 static void AppCmd_ProcessCmd_DEVCONF_GETR(uint8_t* pCmd);
//  320 static void AppCmd_ProcessCmd_DEVCONF_SETR(uint8_t* pCmd);
//  321 static void AppCmd_ProcessCmd_PHYFAN_GETR(uint8_t* pCmd);
//  322 static void AppCmd_ProcessCmd_PHYFAN_SETR(uint8_t* pCmd);
//  323 static void AppCmd_ProcessCmd_PHYNET_GETR(uint8_t* pCmd);
//  324 static void AppCmd_ProcessCmd_PHYNET_SETR(uint8_t* pCmd);
//  325 static void AppCmd_ProcessCmd_UCSCH_GETR(uint8_t* pCmd);
//  326 static void AppCmd_ProcessCmd_BCSCH_GETR(uint8_t* pCmd);
//  327 static void AppCmd_ProcessCmd_UCSCH_SETR(uint8_t* pCmd);
//  328 static void AppCmd_ProcessCmd_BCSCH_SETR(uint8_t* pCmd);
//  329 static void AppCmd_ProcessCmd_WHTLST_GETR(uint8_t* pCmd);
//  330 static void AppCmd_ProcessCmd_WHTLST_SETR(uint8_t* pCmd);
//  331 static void AppCmd_ProcessCmd_GTKS_GETR(uint8_t* pCmd);
//  332 static void AppCmd_ProcessCmd_GTKS_SETR(uint8_t* pCmd);
//  333 static void AppCmd_ProcessCmd_LGTKS_GETR(uint8_t* pCmd);
//  334 static void AppCmd_ProcessCmd_LGTKS_SETR(uint8_t* pCmd);
//  335 static void AppCmd_ProcessCmd_KEYLIFETIMES_SETR(uint8_t* pCmd);
//  336 static void AppCmd_ProcessCmd_CERT_SWITCHR(uint8_t* pCmd);
//  337 static void AppCmd_ProcessCmd_VERBOSE_SETR(uint8_t* pCmd);
//  338 static void AppCmd_ProcessCmd_LOGVERSION_GETR(uint8_t* pCmd);
//  339 static void AppCmd_ProcessCmd_LOGENTRIES_GETR(uint8_t* pCmd);
//  340 static void AppCmd_ProcessCmd_SUBSCP_SETR(uint8_t* pCmd);
//  341 static void AppCmd_ProcessCmd_PAN_VERSION_INCR(uint8_t* pCmd);
//  342 static void AppCmd_ProcessCmd_RPL_GLOBALREPAIRR(uint8_t* pCmd);
//  343 static void AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR(uint8_t* pCmd);
//  344 static void AppCmd_ProcessCmd_ROUTELST_GETR(uint8_t* pCmd);
//  345 static void AppCmd_ProcessCmd_RPLINFO_GETR(uint8_t* pCmd);
//  346 static void AppCmd_ProcessCmd_NDCACHE_GETR(uint8_t* pCmd);
//  347 static void AppCmd_ProcessCmd_VERSION_GETR(uint8_t* pCmd);
//  348 static void AppCmd_ProcessCmd_STATUS_GETR(uint8_t* pCmd);
//  349 static void AppCmd_ProcessCmd_TASKSTATUS_GETR(uint8_t* pCmd);
//  350 #ifdef UMM_INFO
//  351 static void AppCmd_ProcessCmd_MEMMARK_GETR(uint8_t* pCmd);
//  352 static void AppCmd_ProcessCmd_MEMINFO_GETR(uint8_t* pCmd);
//  353 static void AppCmd_ProcessCmd_MEMMAP_GETR(uint8_t* pCmd);
//  354 #endif // UMM_INFO
//  355 #if R_PHY_TYPE_CWX_M && R_MDR_ENABLED
//  356 static void AppCmd_ProcessCmd_MDRSR(uint8_t* pCmd);
//  357 #endif // R_PHY_TYPE_CWX_M && R_MDR_ENABLED
//  358 
//  359 /* Internal : sub function */
//  360 static void AppCmd_ProcessCmd_SCHEDULE_GETR(uint8_t* pCmd, uint8_t paramType);
//  361 static void AppCmd_ProcessCmd_SCHEDULE_SETR(uint8_t* pCmd, r_app_schedule_type_t paramType);
//  362 
//  363 // static
//  364 static r_result_t AppCmd_HexStrToNum(uint8_t** ppBuf, void* pData, int16_t dataSize, r_boolean_t isOctetStr);
//  365 
//  366 /* Internal : for output message */
//  367 static void AppPrintUdpDataInd(r_udp_data_ind_t* p_udp_ind);
//  368 static void AppPrintIcmpEchoInd(r_icmp_echo_reply_ind_t* p_icmp_echo_ind);
//  369 #if R_DEV_TBU_ENABLED && __RX
//  370 static void AppPrintSubScriptFrameInd(const r_subscription_frame_ind_t* p_subscript_frame_ind);
//  371 #endif
//  372 static void AppPrintIpDataIndication(const r_ip_data_ind_t* p_indication);
//  373 static void AppPrintBytesAsHexString(const uint8_t* bytes, size_t numBytes);
//  374 static void AppPrintIPv6Addr(const uint8_t* pIPAddr);
//  375 static void AppPrintMACAddr(const uint8_t* pMACAddr);
//  376 static void AppPrintNetworkName(const char* pNetworkName);
//  377 
//  378 /* *FORMATTING-OFF* (Uncrustify has issues with struct initializers and indents them repeatedly) */
//  379 /* Command List */
//  380 static const r_app_cmd_func_t AppCmdFunc[] = {
//  381     { AppCmdStr_RSTR,                &AppCmd_ProcessCmd_RSTR                },
//  382     { AppCmdStr_STARTR,              &AppCmd_ProcessCmd_STARTR              },
//  383     { AppCmdStr_IPSR,                &AppCmd_ProcessCmd_IPSR                },
//  384     { AppCmdStr_UDPSR,               &AppCmd_ProcessCmd_UDPSR               },
//  385     { AppCmdStr_ICMPSR,              &AppCmd_ProcessCmd_ICMPSR              },
//  386     { AppCmdStr_SPDR,                &AppCmd_ProcessCmd_SPDR                },
//  387     { AppCmdStr_RSMR,                &AppCmd_ProcessCmd_RSMR                },
//  388     { AppCmdStr_REVOKE_KEYS,         &AppCmd_ProcessCmd_REVOKEKEYSR         },
//  389     { AppCmdStr_REVOKE_SUPP,         &AppCmd_ProcessCmd_REVOKESUPPR         },
//  390     { AppCmdStr_DEVICEKICKR,         &AppCmd_ProcessCmd_DEVICEKICKR         },
//  391     { AppCmdStr_LEAVE_NETWORKR,      &AppCmd_ProcessCmd_LEAVENETWORKR       },
//  392     { AppCmdStr_PIB_GETR,            &AppCmd_ProcessCmd_PIB_GETR            },
//  393     { AppCmdStr_PIB_SETR,            &AppCmd_ProcessCmd_PIB_SETR            },
//  394     { AppCmdStr_IPV6PREFIX_SETR,     &AppCmd_ProcessCmd_IPV6PREFIX_SETR     },
//  395     { AppCmdStr_DEVCONF_GETR,        &AppCmd_ProcessCmd_DEVCONF_GETR        },
//  396     { AppCmdStr_DEVCONF_SETR,        &AppCmd_ProcessCmd_DEVCONF_SETR        },
//  397     { AppCmdStr_PHYFAN_GETR,         &AppCmd_ProcessCmd_PHYFAN_GETR         },
//  398     { AppCmdStr_PHYFAN_SETR,         &AppCmd_ProcessCmd_PHYFAN_SETR         },
//  399     { AppCmdStr_PHYNET_GETR,         &AppCmd_ProcessCmd_PHYNET_GETR         },
//  400     { AppCmdStr_PHYNET_SETR,         &AppCmd_ProcessCmd_PHYNET_SETR         },
//  401     { AppCmdStr_UCSCH_GETR,          &AppCmd_ProcessCmd_UCSCH_GETR          },
//  402     { AppCmdStr_UCSCH_SETR,          &AppCmd_ProcessCmd_UCSCH_SETR          },
//  403     { AppCmdStr_BCSCH_GETR,          &AppCmd_ProcessCmd_BCSCH_GETR          },
//  404     { AppCmdStr_BCSCH_SETR,          &AppCmd_ProcessCmd_BCSCH_SETR          },
//  405     { AppCmdStr_WHTLST_GETR,         &AppCmd_ProcessCmd_WHTLST_GETR         },
//  406     { AppCmdStr_WHTLST_SETR,         &AppCmd_ProcessCmd_WHTLST_SETR         },
//  407     { AppCmdStr_GTKS_GETR,           &AppCmd_ProcessCmd_GTKS_GETR           },
//  408     { AppCmdStr_GTKS_SETR,           &AppCmd_ProcessCmd_GTKS_SETR           },
//  409     { AppCmdStr_LGTKS_GETR,          &AppCmd_ProcessCmd_LGTKS_GETR          },
//  410     { AppCmdStr_LGTKS_SETR,          &AppCmd_ProcessCmd_LGTKS_SETR          },
//  411     { AppCmdStr_KEYLIFETIMES_SETR,   &AppCmd_ProcessCmd_KEYLIFETIMES_SETR   },
//  412     { AppCmdStr_CERT_SWITCHR,        &AppCmd_ProcessCmd_CERT_SWITCHR        },
//  413     { AppCmdStr_VERBOSE_SETR,        &AppCmd_ProcessCmd_VERBOSE_SETR        },
//  414     { AppCmdStr_LOGVERSION_GETR,     &AppCmd_ProcessCmd_LOGVERSION_GETR     },
//  415     { AppCmdStr_LOGENTRIES_GETR,     &AppCmd_ProcessCmd_LOGENTRIES_GETR     },
//  416     { AppCmdStr_SUBSCP_SETR,         &AppCmd_ProcessCmd_SUBSCP_SETR         },
//  417     { AppCmdStr_PAN_VERSION_INCR,    &AppCmd_ProcessCmd_PAN_VERSION_INCR    },
//  418     { AppCmdStr_RPL_ROUTES_REFRESHR, &AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR },
//  419     { AppCmdStr_RPL_GLOBALREPAIRR,   &AppCmd_ProcessCmd_RPL_GLOBALREPAIRR   },
//  420     { AppCmdStr_ROUTELST_GETR,       &AppCmd_ProcessCmd_ROUTELST_GETR       },
//  421     { AppCmdStr_RPLINFO_GETR,        &AppCmd_ProcessCmd_RPLINFO_GETR        },
//  422     { AppCmdStr_NDCACHE_GETR,        &AppCmd_ProcessCmd_NDCACHE_GETR        },
//  423     { AppCmdStr_VERSION_GETR,        &AppCmd_ProcessCmd_VERSION_GETR        },
//  424     { AppCmdStr_STATUS_GETR,         &AppCmd_ProcessCmd_STATUS_GETR         },
//  425     { AppCmdStr_TASKSTATUS_GETR,     &AppCmd_ProcessCmd_TASKSTATUS_GETR     },
//  426 #ifdef UMM_INFO
//  427     { AppCmdStr_MEMMARK_GETR,      &AppCmd_ProcessCmd_MEMMARK_GETR      },
//  428     { AppCmdStr_MEMINFO_GETR,      &AppCmd_ProcessCmd_MEMINFO_GETR      },
//  429     { AppCmdStr_MEMMAP_GETR,       &AppCmd_ProcessCmd_MEMMAP_GETR       },
//  430 #endif // UMM_INFO
//  431 # if R_PHY_TYPE_CWX_M && R_MDR_ENABLED
//  432 	{ AppCmdStr_MDRSR,             &AppCmd_ProcessCmd_MDRSR             },
//  433 #endif // R_PHY_TYPE_CWX_M && R_MDR_ENABLED
//  434 };
//  435 /* *FORMATTING-ON* */
//  436 
//  437 /******************************************************************************
//  438    Public function bodies
//  439 ******************************************************************************/
//  440 /********************************************************************************
//  441 * Function Name     : AppCmd_Init
//  442 * Description       : Initialization
//  443 * Arguments         : None
//  444 * Return Value      : None
//  445 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _AppCmd_Init
          CFI FunCall _R_NWK_Init
        CODE
//  446 void AppCmd_Init(void)
//  447 {
_AppCmd_Init:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  448     R_NWK_Init();  // Initialize network layer
        CALL      F:_R_NWK_Init      ;; 3 cycles
//  449 
//  450     /* Initialize global information structure of application layer */
//  451     R_APL_ClearAplGlobal();
          CFI FunCall _R_APL_ClearAplGlobal
        CALL      F:_R_APL_ClearAplGlobal  ;; 3 cycles
//  452     p_aplGlobal = R_APL_GetAplGlobal();
          CFI FunCall _R_APL_GetAplGlobal
        CALL      F:_R_APL_GetAplGlobal  ;; 3 cycles
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  453 
//  454     AppCmd_Reset();
          CFI FunCall _AppCmd_Reset
        CALL      F:_AppCmd_Reset    ;; 3 cycles
//  455 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 25 cycles
        ; ------------------------------------- Total: 25 cycles
//  456 
//  457 /********************************************************************************
//  458 * Function Name     : AppCmd_ProcessCmd
//  459 * Description       : Process a test command
//  460 * Arguments         : pCmd ... Pointer to command
//  461 * Return Value      : Success(1), Failed(0)
//  462 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon1
          CFI Function _AppIpProcessCmd
        CODE
//  463 unsigned char AppIpProcessCmd(unsigned char* pCmd)
//  464 {
_AppIpProcessCmd:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       X, A               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 14
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+18
//  465     unsigned char ret = 0;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
//  466     unsigned char loop, size;
//  467 
//  468     size = sizeof(AppCmdFunc) / sizeof(AppCmdFunc[0]);
//  469     for (loop = 0; loop < size; loop++)
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 24 cycles
//  470     {
//  471         if (!AppStricmp((void __far*)AppCmdFunc[loop].pCmd, (void*)&pCmd))
??AppIpProcessCmd_0:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #LWRD(_AppCmdFunc)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdFunc)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _AppStricmp
        CALL      F:_AppStricmp      ;; 3 cycles
        CMP0      A                  ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_0  ;; 4 cycles
        ; ------------------------------------- Block: 39 cycles
        MOV       A, [SP]            ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        CMP       A, #0x2D           ;; 1 cycle
        BC        ??AppIpProcessCmd_0  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  472         {
//  473             AppCmdFunc[loop].pFunc(pCmd);
//  474             ret = 1;
//  475             break;
//  476         }
//  477     }
//  478     return ret;
??AppIpProcessCmd_1:
        MOV       A, [SP+0x01]       ;; 1 cycle
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+18
        ; ------------------------------------- Block: 8 cycles
??R_DHCPV6_VendorOptionRecvCallback_0:
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, C               ;; 1 cycle
          CFI FunCall
        CALL      HL                 ;; 3 cycles
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??AppIpProcessCmd_1  ;; 3 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 31 cycles
        ; ------------------------------------- Total: 110 cycles
//  479 }
//  480 
//  481 /********************************************************************************
//  482 * Function Name     : AppReceiveMessage
//  483 * Description       : Receive message from NWK task
//  484 * Arguments         : pErase ... (output) enable/disable to release message
//  485 *                   : p_msg ... message
//  486 * Return Value      : Success(1), Failed(0)
//  487 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _AppIpReceiveMessage
        CODE
//  488 unsigned char AppIpReceiveMessage(unsigned char* pErase, r_os_msg_t p_msg)
//  489 {
_AppIpReceiveMessage:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 36
        SUBW      SP, #0x1C          ;; 1 cycle
          CFI CFA SP+40
//  490     *pErase = R_TRUE;  // Free all messages by default
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
        ONEB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  491 
//  492     /* Process message */
//  493     r_nwk_msg_t* p_nwk_msg = p_msg;
//  494     switch (p_nwk_msg->hdr.param_id)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SUB       A, #0x6E           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_1  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
        DEC       A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_2  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x35           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0xB            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_4  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x2            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_5  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x2            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_6  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x10           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_7  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x5            ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_8  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x5            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_9  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_10  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_11  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_12  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x2            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_13  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_14  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  495     {
//  496 #if R_DEV_TBU_ENABLED && __RX
//  497         case R_NWK_API_MSG_SUBSCRIPT_FRAME_IND:
//  498             R_Modem_print("SUBMSGI %02X %02X ", AppCmdConfig.ntfHandle++, R_RESULT_SUCCESS);
//  499             AppPrintSubScriptFrameInd(&p_nwk_msg->message.subscriptionFrameIndication);
//  500             break;
//  501 
//  502 #endif
//  503         case R_NWK_API_MSG_IP_DATA_IND:
//  504             R_Modem_print("IPFI %02X %02X ", AppCmdConfig.ntfHandle++, R_RESULT_SUCCESS);
??R_DHCPV6_VendorOptionRecvCallback_8:
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOV       X, ES:_AppCmdConfig  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       ES:_AppCmdConfig, A  ;; 2 cycles
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+42
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      DE, #LWRD(?_0+88)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  505             AppPrintIpDataIndication(&p_nwk_msg->message.ipInd);
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  506             break;
        POP       AX                 ;; 1 cycle
          CFI CFA SP+40
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
        ; ------------------------------------- Block: 77 cycles
//  507 
//  508         case R_NWK_API_MSG_DHCP_VENDOR_OPT_DATA:
//  509         {
//  510             const r_nwk_dhcp_vend_opt_data_ind_t* ind = &p_nwk_msg->message.dhcpVendorOptionInd;
??R_DHCPV6_VendorOptionRecvCallback_7:
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  511             R_DHCPV6_VendorOptionRecvCallback(&ind->src, ind->enterprise_number, ind->option_data, ind->option_data_len);
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0xD           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+48
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+52
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        SHRW      AX, 0x8            ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+50
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        SHLW      BC, 0x8            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        SHRW      AX, 0x8            ;; 1 cycle
        ADDW      AX, BC             ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+46
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+42
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+48
        MOV       X, A               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+46
          CFI FunCall _R_DHCPV6_VendorOptionRecvCallback
        CALL      F:_R_DHCPV6_VendorOptionRecvCallback  ;; 3 cycles
//  512             break;
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+40
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
        ; ------------------------------------- Block: 99 cycles
//  513         }
//  514 
//  515 #if R_BORDER_ROUTER_ENABLED
//  516         case R_NWK_API_MSG_DHCP_IND:
//  517         {
//  518 #if R_DHCPV6_SERVER_ENABLED
//  519             r_dhcp_data_ind_t* dhcpInd = &p_nwk_msg->message.dhcpInd;
//  520             R_DHCPV6_ServerProcessMsg(dhcpInd->srcAddress, dhcpInd->data, dhcpInd->dataLength);
//  521 #endif
//  522             break;
//  523         }
//  524 
//  525         case R_NWK_API_MSG_DEVICE_STATUS_UPDATE_IND:
//  526         {
//  527             r_nwk_device_status_ind_t* ind = &p_nwk_msg->message.deviceStatusUpdateInd;
//  528             LOG_ONLY_VAR(ind);
//  529 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  530             r_loggen_529(ind->deviceAddress.bytes, ind->updateType);
//  531 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  532             break;
//  533         }
//  534 #endif /* R_BORDER_ROUTER_ENABLED */
//  535 
//  536 #if R_BR_AUTHENTICATOR_ENABLED
//  537         case R_NWK_API_MSG_AUTH_BR_START_IND:
//  538         {
//  539             R_AUTH_BR_Start(p_aplGlobal);
//  540             break;
//  541         }
//  542 
//  543         case R_NWK_API_MSG_AUTH_BR_PER_UPDATE:
//  544         {
//  545             if (R_AUTH_BR_PeriodicUpdateGtks(p_aplGlobal))
//  546             {
//  547                 R_NWK_IncreasePanVersionRequest();
//  548             }
//  549             if (R_AUTH_BR_PeriodicUpdateLgtks(p_aplGlobal))
//  550             {
//  551                 R_NWK_IncreaseLfnVersionRequest();
//  552             }
//  553             break;
//  554         }
//  555 #endif /* R_BR_AUTHENTICATOR_ENABLED */
//  556 
//  557         case R_NWK_API_MSG_AUTH_RN_START:
//  558         {
//  559             r_result_t res = R_AUTH_SUP_StartJoin(p_aplGlobal, p_nwk_msg->message.authRnStartInd.target.bytes);
??R_DHCPV6_VendorOptionRecvCallback_10:
        MOVW      AX, BC             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
        POP       AX                 ;; 1 cycle
          CFI CFA SP+40
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+42
        POP       DE                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_AUTH_SUP_StartJoin
        CALL      F:_R_AUTH_SUP_StartJoin  ;; 3 cycles
//  560             R_NWK_EapolDataProcessed(res);  // Signal result to network layer
          CFI FunCall _R_NWK_EapolDataProcessed
        CALL      F:_R_NWK_EapolDataProcessed  ;; 3 cycles
//  561             break;
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
        ; ------------------------------------- Block: 29 cycles
//  562         }
//  563 
//  564         case R_NWK_API_MSG_AUTH_RN_RESET:
//  565             R_AUTH_SUP_Reset(p_aplGlobal);
??R_DHCPV6_VendorOptionRecvCallback_11:
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_AUTH_SUP_Reset
        CALL      F:_R_AUTH_SUP_Reset  ;; 3 cycles
//  566             break;
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
        ; ------------------------------------- Block: 15 cycles
//  567 
//  568         case R_NWK_API_MSG_AUTH_RN_CLEAR_GTK_VALIDITY:
//  569             R_AUTH_SUP_ClearGtkValidity(p_aplGlobal);
??R_DHCPV6_VendorOptionRecvCallback_13:
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_AUTH_SUP_ClearGtkValidity
        CALL      F:_R_AUTH_SUP_ClearGtkValidity  ;; 3 cycles
//  570             break;
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
        ; ------------------------------------- Block: 15 cycles
//  571 
//  572         case R_NWK_API_MSG_AUTH_RN_DELETE_GTK:
//  573         {
//  574             R_AUTH_DeleteGTKs(p_aplGlobal, p_nwk_msg->message.authRnGtkDeleteInd.mask);
??R_DHCPV6_VendorOptionRecvCallback_12:
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall _R_AUTH_DeleteGTKs
        CALL      F:_R_AUTH_DeleteGTKs  ;; 3 cycles
//  575             break;
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
        ; ------------------------------------- Block: 23 cycles
//  576         }
//  577 
//  578         case R_NWK_API_MSG_EAPOL_DATA_IND:
//  579         {
//  580             r_eapol_data_ind_t* ind = &p_nwk_msg->message.eapolDataInd;
??R_DHCPV6_VendorOptionRecvCallback_9:
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  581 
//  582 #if R_BR_AUTHENTICATOR_ENABLED
//  583             if (AppCmdConfig.deviceType == R_BORDERROUTER)
//  584             {
//  585                 R_AUTH_BR_ProcessDirectMessage(p_aplGlobal, ind->type, ind->src.bytes, ind->data, ind->dataLength);
//  586             }
//  587             else
//  588 #endif
//  589             {
//  590                 uint16_t eapolDataLength = ind->dataLength;
        ADDW      AX, #0x11          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  591                 r_mac_eapol_type_t type = ind->type;
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
//  592                 r_eui64_t src = ind->src;
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      BC, #0x8           ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
//  593                 r_eui64_t authenticator = ind->authenticator;
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
//  594 
//  595                 /* Processing function of supplicant authentication expects data in allocated auth heap buffer */
//  596                 void* eapolDataHeapBuf;
//  597 #if R_SHARED_NWK_MEM && (R_HEAP_ID_AUTH == R_HEAP_ID_NWK)
//  598                 // FreeRTOS msg is already on correct heap -> Move EAPOL data to front of buffer and resize it */
//  599                 eapolDataHeapBuf = p_msg;
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  600                 memmove(eapolDataHeapBuf, ind->data, eapolDataLength);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x13          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+46
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+48
        POP       DE                 ;; 1 cycle
          CFI CFA SP+46
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+44
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
          CFI FunCall _memmove
        CALL      F:_memmove         ;; 3 cycles
//  601                 void* reallocedBuf = r_realloc(R_HEAP_ID_AUTH, eapolDataHeapBuf, eapolDataLength);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x5           ;; 1 cycle
          CFI FunCall _umm_realloc
        CALL      F:_umm_realloc     ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+40
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  602                 // If realloc fails, the passed pointer is still valid and untouched -> only assign value on success
//  603                 if (reallocedBuf != NULL)
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_15  ;; 4 cycles
        ; ------------------------------------- Block: 97 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_15:
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_16  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  604                 {
//  605                     /* This branch is unreachable since realloc never fails if the new size is smaller than the old */
//  606                     eapolDataHeapBuf = reallocedBuf;
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
//  607                 }
//  608                 *pErase = R_FALSE;  // Ownership will be transferred to AUTH module
??R_DHCPV6_VendorOptionRecvCallback_16:
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  609 #else
//  610                 // Create auth heap buffer to hold EAPOL data
//  611                 eapolDataHeapBuf = r_alloc(R_HEAP_ID_AUTH, eapolDataLength);
//  612                 if (!eapolDataHeapBuf)
//  613                 {
//  614 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
//  615                     r_loggen_612(eapolDataLength);
//  616 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
//  617                     R_NWK_EapolDataProcessed(R_RESULT_INSUFFICIENT_BUFFER);  // Signal failure to network layer
//  618                     break;
//  619                 }
//  620                 memcpy(eapolDataHeapBuf, ind->data, eapolDataLength);
//  621 #endif /* if R_SHARED_NWK_MEM && (R_HEAP_ID_AUTH == R_HEAP_ID_NWK) */
//  622 
//  623                 r_result_t res = R_AUTH_SUP_ProcessEapolMessage(p_aplGlobal, type, src.bytes, authenticator.bytes,
//  624                                                                 eapolDataHeapBuf, eapolDataLength);
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+54
        POP       AX                 ;; 1 cycle
          CFI CFA SP+52
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+54
        POP       DE                 ;; 1 cycle
          CFI CFA SP+52
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_AUTH_SUP_ProcessEapolMessage
        CALL      F:_R_AUTH_SUP_ProcessEapolMessage  ;; 3 cycles
//  625                 eapolDataHeapBuf = NULL;       // Ownership transferred
//  626                 R_NWK_EapolDataProcessed(res); // Signal message processing result to network layer
          CFI FunCall _R_NWK_EapolDataProcessed
        CALL      F:_R_NWK_EapolDataProcessed  ;; 3 cycles
//  627             }
//  628             break;
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+40
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
        ; ------------------------------------- Block: 53 cycles
//  629         }
//  630 
//  631         case R_NWK_API_MSG_UDP_DATA_IND:
//  632         {
//  633             r_udp_data_ind_t* ind = &p_nwk_msg->message.udpInd;
??R_DHCPV6_VendorOptionRecvCallback_6:
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  634             if (ind->dstPort == R_AUTH_EAPOL_RELAY_PORT)
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x11          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        CMPW      AX, #0x280D        ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_17  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
//  635             {
//  636 #if R_BR_AUTHENTICATOR_ENABLED
//  637                 if (AppCmdConfig.deviceType == R_BORDERROUTER)
//  638                 {
//  639                     R_AUTH_BR_ProcessRelayMessage(p_aplGlobal, ind->srcAddress, ind->dstAddress, ind->data, ind->dataLength);
//  640                     break;
//  641                 }
//  642                 else
//  643 #endif
//  644                 {
//  645                     R_AUTH_SUP_ProcessRelayMessage(p_aplGlobal, ind->srcAddress, ind->dstAddress, ind->data, ind->dataLength);
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x25          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x29          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+52
        POP       AX                 ;; 1 cycle
          CFI CFA SP+50
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+52
        POP       DE                 ;; 1 cycle
          CFI CFA SP+50
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_AUTH_SUP_ProcessRelayMessage
        CALL      F:_R_AUTH_SUP_ProcessRelayMessage  ;; 3 cycles
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+40
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
        ; ------------------------------------- Block: 57 cycles
//  646                 }
//  647             }
//  648             else if (ind->dstPort == KICK_DEVICE_MSG_UDP_PORT)
??R_DHCPV6_VendorOptionRecvCallback_17:
        CMPW      AX, #0xE1B         ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_18  ;; 4 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 5 cycles
//  649             {
//  650 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  651                 r_loggen_646(ind->srcAddress);
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0x10          ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0x3           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_19  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      BC, #0x10          ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 13 cycles
??R_DHCPV6_VendorOptionRecvCallback_19:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  652 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  653                 uint8_t expectedPayload[] = KICK_DEVICE_MSG_PAYLOAD;
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_0+36)  ;; 1 cycle
        MOV       ES, #BYTE3(?_0)    ;; 1 cycle
        MOVW      BC, #0x4           ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
//  654                 if (sizeof(expectedPayload) == ind->dataLength && MEMEQUAL_A(expectedPayload, ind->data))
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x25          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        CMPW      AX, #0x3           ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_20  ;; 4 cycles
        ; ------------------------------------- Block: 35 cycles
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x29          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_memcmp
        CALL      F:_R_memcmp        ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+40
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_20  ;; 4 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 26 cycles
//  655                 {
//  656 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  657                     r_loggen_650();
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOV       X, #0x4            ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 12 cycles
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
??AppIpReceiveMessage_0:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  658 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  659                     AppCmd_LeaveNetwork();
          CFI FunCall _AppCmd_LeaveNetwork
        CALL      F:_AppCmd_LeaveNetwork  ;; 3 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 9 cycles
//  660                 }
//  661                 else
//  662                 {
//  663 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
//  664                     r_loggen_655();
??R_DHCPV6_VendorOptionRecvCallback_20:
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x2            ;; 1 cycle
        MOV       X, #0x5            ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 12 cycles
        CALL      F:_r_log_finish    ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??AppIpReceiveMessage_1:
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_21  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  665 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
//  666                 }
//  667             }
//  668             else
//  669             {
//  670                 R_Modem_print("RCVI %02X %02X ", AppCmdConfig.ntfHandle++, R_RESULT_SUCCESS);
??R_DHCPV6_VendorOptionRecvCallback_18:
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOV       X, ES:_AppCmdConfig  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       ES:_AppCmdConfig, A  ;; 2 cycles
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+42
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      DE, #LWRD(?_0+104)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  671                 AppPrintUdpDataInd(ind);
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppPrintUdpDataInd
        CALL      F:_AppPrintUdpDataInd  ;; 3 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_22  ;; 3 cycles
          CFI CFA SP+40
          CFI FunCall _clock_seconds
        ; ------------------------------------- Block: 27 cycles
//  672             }
//  673             break;
//  674         }
//  675 
//  676         case R_NWK_API_MSG_ICMP_ECHO_IND:
//  677 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  678             r_loggen_667(clock_seconds(), p_nwk_msg->message.icmpEchoInd.srcAddress);
??R_DHCPV6_VendorOptionRecvCallback_5:
        CALL      F:_clock_seconds   ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+36
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0x14          ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0x6           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_23  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        MOVW      BC, #0x4           ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        MOVW      BC, #0x10          ;; 1 cycle
        MOV       A, [SP+0x1E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 19 cycles
??R_DHCPV6_VendorOptionRecvCallback_23:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  679 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  680             R_Modem_print("RCVI %02X %02X ", AppCmdConfig.ntfHandle++, R_RESULT_SUCCESS);
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOV       X, ES:_AppCmdConfig  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       ES:_AppCmdConfig, A  ;; 2 cycles
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+42
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      DE, #LWRD(?_0+104)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  681             AppPrintIcmpEchoInd(&p_nwk_msg->message.icmpEchoInd);
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppPrintIcmpEchoInd
        CALL      F:_AppPrintIcmpEchoInd  ;; 3 cycles
//  682             break;
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_22  ;; 3 cycles
          CFI CFA SP+40
        ; ------------------------------------- Block: 31 cycles
//  683 
//  684         case R_NWK_API_MSG_JOIN_STATE_IND:
//  685         {
//  686             r_nwk_join_state_ind_t* ind = &p_nwk_msg->message.joinStateInd;
??R_DHCPV6_VendorOptionRecvCallback_4:
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  687             p_aplGlobal->nwk.joinState = ind->currentJoinState;  // Cache join state to avoid repeated requests to NWK
        MOV       A, ES:[HL]         ;; 2 cycles
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
        POP       AX                 ;; 1 cycle
          CFI CFA SP+40
        MOV       ES:[HL+0xB2], A    ;; 2 cycles
//  688             R_Modem_print("JSI %02X %02X\n", AppCmdConfig.ntfHandle++, ind->currentJoinState);
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOV       C, ES:_AppCmdConfig  ;; 2 cycles
        MOV       A, C               ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       ES:_AppCmdConfig, A  ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        CLRB      B                  ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      DE, #LWRD(?_0+120)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  689             break;
        ; ------------------------------------- Block: 44 cycles
??R_DHCPV6_VendorOptionRecvCallback_22:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+40
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_3  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  690         }
//  691 
//  692         /*-----------------------------------------------------------------------*/
//  693         /* The following messages are not supported by this sample application   */
//  694         /*-----------------------------------------------------------------------*/
//  695         /* Warning indication */
//  696         case R_NWK_API_MSG_NWK_WARNING_IND:
//  697             /*
//  698                Warning indication - This message can be ignored
//  699                   An indication messages could not be notified to the application layer
//  700                   due to lack of memory resources. The stack can continue normal operations.
//  701              */
//  702 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
//  703             r_loggen_690(p_nwk_msg->message.warningInd.status);
??R_DHCPV6_VendorOptionRecvCallback_1:
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x2            ;; 1 cycle
        MOV       X, #0x7            ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_21  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_24  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  704 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
//  705             break;
//  706 
//  707         /* Fatal error indication */
//  708         case R_NWK_API_MSG_NWK_FATAL_ERROR_IND:
//  709             /*
//  710                Fatal error indication - This message should not be ignored
//  711                   Fatal error occurred due to possibly hardware errors or unexpected behaviors
//  712                   in RFIC. The stack could not continue normal operations.
//  713              */
//  714 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
//  715             r_loggen_700(p_nwk_msg->message.fatalErrorInd.status);
??R_DHCPV6_VendorOptionRecvCallback_2:
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ONEB      C                  ;; 1 cycle
        MOV       X, #0x8            ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_25  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 9 cycles
??R_DHCPV6_VendorOptionRecvCallback_25:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  716 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
//  717             // PRQA S 2870 2
//  718             for ( ; ; )
??AppIpReceiveMessage_2:
        BR        S:??AppIpReceiveMessage_2  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  719             {
//  720                 /* Stop execution at this point using an infinite loop */
//  721             }
//  722 
//  723         /* ICMP error message */
//  724         case R_NWK_API_MSG_ICMP_ERROR_IND:
//  725             break;
//  726 
//  727         /* Confirm message
//  728              These messages will be notified if an API function is called with non-blocking
//  729              NOTE: As for this sample application, all API functions are called with blocking.
//  730                    So these messages will not be notified.
//  731          */
//  732         case R_NWK_API_MSG_NWK_JOIN_CFM:  //!< Network join confirm
//  733 #if R_BORDER_ROUTER_ENABLED
//  734         case R_NWK_API_MSG_NWK_START_CFM: //!< Network start confirm
//  735 #endif
//  736         case R_NWK_API_MSG_ICMP_ECHO_CFM: //!< ICMP echo confirm
//  737         case R_NWK_API_MSG_UDP_DATA_CFM:  //!< UDP data confirm
//  738             break;
//  739 
//  740         default:
//  741 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
//  742             r_loggen_725(p_nwk_msg->hdr.param_id);
??R_DHCPV6_VendorOptionRecvCallback_14:
        MOV       A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x2            ;; 1 cycle
        MOV       X, #0x9            ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_21  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
??R_DHCPV6_VendorOptionRecvCallback_24:
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 9 cycles
??R_DHCPV6_VendorOptionRecvCallback_21:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  743 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
//  744             break;
        ; ------------------------------------- Block: 3 cycles
//  745     }
//  746 
//  747     return 1;
??R_DHCPV6_VendorOptionRecvCallback_3:
        ONEB      A                  ;; 1 cycle
        ADDW      SP, #0x24          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 982 cycles
//  748 }
//  749 
//  750 /******************************************************************************
//  751    Private function bodies
//  752 ******************************************************************************/
//  753 /********************************************************************************
//  754 * Function Name     : AppCmd_Reset
//  755 * Description       : Reset application state
//  756 * Arguments         : None
//  757 * Return Value      : None
//  758 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon0
          CFI Function _AppCmd_Reset
        CODE
//  759 static void AppCmd_Reset(void)
//  760 {
_AppCmd_Reset:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 22
        SUBW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+26
//  761     /* Set MAC address */
//  762     const uint8_t* p_macAddr = pAppGetMacAddr();
          CFI FunCall _pAppGetMacAddr
        CALL      F:_pAppGetMacAddr  ;; 3 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  763     uint8_t eui64[8];
//  764     R_Swap64(p_macAddr, eui64);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _R_Swap64
        CALL      F:_R_Swap64        ;; 3 cycles
//  765     R_NWK_SetRequest(R_NWK_macExtendedAddress, eui64, 8);
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x80           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
//  766     r_app_config_t* p_config = &AppCmdConfig;
//  767     memset(p_config, 0, sizeof(*p_config));
        MOVW      AX, #0x3A          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdConfig)  ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdConfig)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
//  768 
//  769     /* Set current settings for macPANId and phyCurrentChannel  */
//  770     p_config->panId = APP_CMD_MAC_PanId_Default;
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      ES:_AppCmdConfig+36, AX  ;; 2 cycles
//  771     uint8_t channel = APP_CMD_MAC_Channel_Default;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
//  772     R_NWK_SetRequest(R_NWK_phyCurrentChannel, &channel, sizeof(channel));
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
//  773 
//  774 #if !R_DEV_DISABLE_AUTH
//  775 #if R_MODEM_CLIENT
//  776     R_AUTH_SUP_Init(p_aplGlobal, r_auth_certs, 2, 0);
//  777 #else
//  778     R_AUTH_SUP_Init(p_aplGlobal, r_auth_certs, 2, MAX(sizeof(RmMcpsDataRequestT), sizeof(RmMcpsDataConfirmT)));
        MOVW      AX, #0x165         ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOV       X, #0x2            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      BC, #LWRD(_r_auth_certs)  ;; 1 cycle
        MOV       X, #BYTE3(_r_auth_certs)  ;; 1 cycle
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        POP       AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_AUTH_SUP_Init
        CALL      F:_R_AUTH_SUP_Init  ;; 3 cycles
//  779 #endif
//  780 #if R_BR_AUTHENTICATOR_ENABLED
//  781     R_AUTH_BR_Reset(p_aplGlobal);
//  782 #endif
//  783 #endif
//  784 
//  785     /* Set default IPv6 prefix */
//  786     const uint8_t prefix[R_IPV6_PREFIX_LEN] = {0x20, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_44)    ;; 1 cycle
        MOV       ES, #BYTE3(?_44)   ;; 1 cycle
        MOVW      BC, #0x8           ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
//  787     R_memcpy(p_config->globalIpAddress.bytes, prefix, sizeof(prefix));
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdConfig+41)  ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdConfig)  ;; 1 cycle
          CFI FunCall _R_memcpy
        CALL      F:_R_memcpy        ;; 3 cycles
//  788 
//  789     /* Set default IPv6 interface Identifier (generated from EUI-64) */
//  790     R_memcpy(&p_config->globalIpAddress.bytes[R_IPV6_PREFIX_LEN], pAppGetMacAddr(), R_IPV6_ADDRESS_LENGTH - R_IPV6_PREFIX_LEN);
          CFI FunCall _pAppGetMacAddr
        CALL      F:_pAppGetMacAddr  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+26
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0x8            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+32
        POP       BC                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdConfig+49)  ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdConfig)  ;; 1 cycle
          CFI FunCall _R_memcpy
        CALL      F:_R_memcpy        ;; 3 cycles
//  791     p_config->globalIpAddress.bytes[R_IPV6_PREFIX_LEN] ^= 0x02u;  // Toggle U/L bit
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOV       A, ES:_AppCmdConfig+49  ;; 2 cycles
        XOR       A, #0x2            ;; 1 cycle
        MOV       ES:_AppCmdConfig+49, A  ;; 2 cycles
//  792 
//  793 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
//  794     static uint8_t r_app_log_buffer[R_LOG_BUFFER_SIZE];
//  795     R_LOG_Init(r_app_log_buffer, sizeof(r_app_log_buffer), R_LOG_SEVERITY_OFF);  // Initialize and disable log framework
        CLRB      X                  ;; 1 cycle
        MOVW      BC, #0x100         ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdConfig+62)  ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdConfig)  ;; 1 cycle
          CFI FunCall _R_LOG_Init
        CALL      F:_R_LOG_Init      ;; 3 cycles
//  796 #endif
//  797 }
        ADDW      SP, #0x1A          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 146 cycles
        ; ------------------------------------- Total: 146 cycles

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
_AppCmdConfig:
        DS 58
        DS 4
        DS 256
//  798 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon0
          CFI Function _AppCmd_LeaveNetwork
        CODE
//  799 static r_result_t AppCmd_LeaveNetwork()
//  800 {
_AppCmd_LeaveNetwork:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
//  801     /* Send NWK request to leave the network (NWK and lower layers will be reset) */
//  802     r_result_t res = R_NWK_LeaveNetworkRequest();
          CFI FunCall _R_NWK_LeaveNetworkRequest
        CALL      F:_R_NWK_LeaveNetworkRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
//  803 
//  804     /* Reset authentication (required to avoid inconsistent state between auth module and NWK after restart) */
//  805 #if !R_DEV_DISABLE_AUTH
//  806 #if R_MODEM_CLIENT
//  807     R_AUTH_SUP_Init(p_aplGlobal, r_auth_certs, 2, 0);
//  808 #else
//  809     R_AUTH_SUP_Init(p_aplGlobal, r_auth_certs, 2, MAX(sizeof(RmMcpsDataRequestT), sizeof(RmMcpsDataConfirmT)));
        MOVW      AX, #0x165         ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOV       X, #0x2            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      BC, #LWRD(_r_auth_certs)  ;; 1 cycle
        MOV       X, #BYTE3(_r_auth_certs)  ;; 1 cycle
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_AUTH_SUP_Init
        CALL      F:_R_AUTH_SUP_Init  ;; 3 cycles
//  810 #endif
//  811 #if R_BR_AUTHENTICATOR_ENABLED
//  812     R_AUTH_BR_Reset(p_aplGlobal);
//  813 #endif
//  814 #endif
//  815 
//  816     return res;
        MOV       A, [SP+0x04]       ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 36 cycles
        ; ------------------------------------- Total: 36 cycles
//  817 }
//  818 
//  819 /* Command */
//  820 /********************************************************************************
//  821 * Function Name     : AppCmd_ProcessCmd_RSTR
//  822 * Description       : Process "RSTR" command
//  823 * Arguments         : pCmd ... Pointer to command
//  824 *                   :   RSTR (Handle) [(Options)]
//  825 * Return Value      : None
//  826 * Output            : Confirm command
//  827 *                   :   RSTC (Handle) (Status)
//  828 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_RSTR
        CODE
//  829 static void AppCmd_ProcessCmd_RSTR(uint8_t* pCmd)
//  830 {
_AppCmd_ProcessCmd_RSTR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
//  831     /* Get command parameters */
//  832     uint8_t handle;
//  833     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
//  834     uint16_t options;
//  835     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
//  836 
//  837     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_26  ;; 4 cycles
          CFI FunCall _R_NWK_ResetRequest
        ; ------------------------------------- Block: 63 cycles
//  838 #if R_DEV_TBU_ENABLED && __RX
//  839     {
//  840         /* Store the original handle number for the confirm */
//  841         rstr_handle = handle;
//  842 
//  843         /* Perform Software Reset */
//  844         SYSTEM.PRCR.WORD = 0xA503; // Unlock register protection
//  845         SYSTEM.SWRR = 0xA501;      // Software Reset
//  846         SYSTEM.PRCR.WORD = 0xA500; // Lock register protection
//  847         while (1)
//  848             ;
//  849     }
//  850 #else /* R_DEV_TBU_ENABLED && __RX */
//  851     {
//  852         res = R_NWK_ResetRequest();
        CALL      F:_R_NWK_ResetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
//  853         if (res == R_RESULT_SUCCESS)
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 1 cycle
          CFI FunCall _AppCmd_Reset
        ; ------------------------------------- Block: 6 cycles
//  854         {
//  855             AppCmd_Reset();  // Reset configuration and state of application
        CALL      F:_AppCmd_Reset    ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  856         }
//  857     }
//  858 
//  859 #endif /* R_DEV_TBU_ENABLED && __RX */
//  860     R_Modem_print("RSTC %02X %02X\n", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_26:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, #LWRD(?_45)    ;; 1 cycle
        MOV       A, #BYTE3(?_45)    ;; 1 cycle
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 82 cycles
        REQUIRE ?Subroutine4
        ; // Fall through to label ?Subroutine4
//  861 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+20
          CFI FunCall _AppCmd_ProcessCmd_RSTR _R_Modem_print
          CFI FunCall _AppPrintUdpDataInd _R_Modem_print
        CODE
?Subroutine4:
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
//  862 
//  863 /********************************************************************************
//  864 * Function Name     : AppCmd_ProcessCmd_STARTR
//  865 * Description       : Process "STARTR" command
//  866 * Arguments         : pCmd ... Pointer to command
//  867 *                   :   STARTR (Handle) [(Options)]
//  868 * Return Value      : None
//  869 * Output            : Confirm command
//  870 *                   :   STARTC (Handle) (Status) (ChannelNumber) (PANId)
//  871 *                   :          (IPv6Addr) (CoordIPv6Addr) (CoordMACAddr)
//  872 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_STARTR
        CODE
//  873 static void AppCmd_ProcessCmd_STARTR(uint8_t* pCmd)
//  874 {
_AppCmd_ProcessCmd_STARTR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 24
        SUBW      SP, #0x14          ;; 1 cycle
          CFI CFA SP+28
//  875     r_app_config_t* p_config = &AppCmdConfig;
//  876 
//  877     /* Get command parameters */
//  878     uint8_t handle;
//  879     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFF2        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+34
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+32
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
//  880     uint16_t options;
//  881     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+38
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
//  882 
//  883     /* Force to set blocking. */
//  884     options &= (~R_OPTIONS_NON_BLOCKING);
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
//  885 
//  886     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+28
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_27  ;; 4 cycles
        ; ------------------------------------- Block: 70 cycles
//  887     {
//  888         if (p_config->deviceType == R_ROUTERNODE)
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        CMP       ES:_AppCmdConfig+1, #0x1  ;; 2 cycles
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_28  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  889         {
//  890             res = R_NWK_JoinRequest(p_config->networkName, options);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdConfig+2)  ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdConfig)  ;; 1 cycle
          CFI FunCall _R_NWK_JoinRequest
        CALL      F:_R_NWK_JoinRequest  ;; 3 cycles
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_29  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
//  891         }
//  892 #if R_BORDER_ROUTER_ENABLED
//  893         else if (p_config->deviceType == R_BORDERROUTER)
//  894         {
//  895 #if R_DHCPV6_SERVER_ENABLED
//  896 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  897             r_loggen_878(AppCmdConfig.globalIpAddress.bytes);
//  898 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  899             R_DHCPV6_ServerInit(pAppGetMacAddr(), &R_DHCPV6_ServerLookupAssignPrefix, p_config->globalIpAddress.bytes);
//  900 #endif
//  901             res = R_NWK_StartRequest(p_config->panId,
//  902                                      p_config->panSize,
//  903                                      p_config->useParent_BS_IE,
//  904                                      p_config->networkName,
//  905                                      &p_config->globalIpAddress,
//  906                                      options);
//  907         }
//  908 #endif /* R_BORDER_ROUTER_ENABLED */
//  909         else
//  910         {
//  911             res = R_RESULT_INVALID_PARAMETER;  // Invalid device type
??R_DHCPV6_VendorOptionRecvCallback_28:
        MOV       A, #0x11           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??R_DHCPV6_VendorOptionRecvCallback_29:
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  912         }
//  913     }
//  914     // get current Device Type
//  915     uint8_t deviceType;
//  916     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_27:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_30  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  917     {
//  918         res = R_NWK_GetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xE3           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
//  919     }
//  920     // get current MAC address
//  921     uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH] = { 0 };
??R_DHCPV6_VendorOptionRecvCallback_30:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_0+40)  ;; 1 cycle
        MOV       ES, #BYTE3(?_0)    ;; 1 cycle
        MOVW      BC, #0x8           ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
//  922     if (res == R_RESULT_SUCCESS)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_31  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
//  923     {
//  924         res = R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x80           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
//  925     }
//  926     // get current Pan ID
//  927     uint16_t panId;
//  928     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_31:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_32  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  929     {
//  930         res = R_NWK_GetRequest(R_NWK_macPANId, &panId, sizeof(panId));
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x50           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 10 cycles
//  931     }
//  932 
//  933     /* Confirm(STARTC) */
//  934     R_Modem_print("STARTC %02X %02X ", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_32:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      DE, #LWRD(?_0+136)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  935     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+28
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_33  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
//  936     {
//  937         /* DeviceType */
//  938         R_Modem_print("%02X ", deviceType);
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  939 
//  940         /* PANId */
//  941         R_Modem_print("%04X ", panId);
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  942 
//  943         /* NetworkName */
//  944         AppPrintNetworkName(p_config->networkName);
        MOVW      DE, #LWRD(_AppCmdConfig+2)  ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdConfig)  ;; 1 cycle
          CFI FunCall _AppPrintNetworkName
        CALL      F:_AppPrintNetworkName  ;; 3 cycles
//  945         R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  946 
//  947         /* MACAddr */
//  948         R_Swap64((uint8_t*)(&macAddr[0]), (uint8_t*)(&macAddr[0]));
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+28
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        POP       BC                 ;; 1 cycle
          CFI CFA SP+30
        POP       HL                 ;; 1 cycle
          CFI CFA SP+28
          CFI FunCall _R_Swap64
        CALL      F:_R_Swap64        ;; 3 cycles
//  949         AppPrintMACAddr(macAddr);
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
//  950     }
        ; ------------------------------------- Block: 52 cycles
//  951     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_33:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  952 }
        ADDW      SP, #0x18          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 225 cycles
//  953 
//  954 /********************************************************************************
//  955 * Function Name     : AppCmd_ProcessCmd_PIB_GETR
//  956 * Description       : Process "PIB_GETR" command
//  957 * Arguments         : pCmd ... Pointer to command
//  958 *                   :   PIB_GETR (Handle) (AttributeId) (IsNumeric)
//  959 * Return Value      : None
//  960 * Output            : Confirm command
//  961 *                   :   PIB_GETC (Handle) (Status) (AttributeLength) (AttributeValueBytes)
//  962 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_PIB_GETR
        CODE
//  963 static void AppCmd_ProcessCmd_PIB_GETR(uint8_t* pCmd)
//  964 {
_AppCmd_ProcessCmd_PIB_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 14
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+18
//  965     /* Get command parameters */
//  966     uint8_t handle;
//  967     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+24
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x07], A       ;; 1 cycle
//  968 
//  969     uint8_t attributeId;
//  970     res |= AppCmd_HexStrToNum(&pCmd, &attributeId, sizeof(attributeId), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x9           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x0C], A       ;; 1 cycle
//  971 
//  972     r_boolean_t isNumeric;
//  973     res |= AppCmd_HexStrToNum(&pCmd, &isNumeric, sizeof(isNumeric), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x0F]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x0F], A       ;; 1 cycle
//  974 
//  975     uint16_t attributeLength = 0;
        CLRW      AX                 ;; 1 cycle
//  976     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+18
        MOVW      [SP+0x04], AX      ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_34  ;; 4 cycles
        ; ------------------------------------- Block: 89 cycles
//  977     {
//  978         res = R_NWK_GetRequestVarSize(attributeId, AppCmdBuf, sizeof(AppCmdBuf), &attributeLength);
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      BC, #0xC5B         ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdBuf)  ;; 1 cycle
        MOV       X, #BYTE3(_AppCmdBuf)  ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
          CFI FunCall _R_NWK_GetRequestVarSize
        CALL      F:_R_NWK_GetRequestVarSize  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
        MOV       [SP+0x03], A       ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
//  979     }
//  980 
//  981     R_Modem_print("PIB_GETC %02X %02X", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_34:
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      DE, #LWRD(?_0+170)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  982 
//  983     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP+0x03]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_35  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
//  984     {
//  985         R_Modem_print(" %04X ", attributeLength);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, #LWRD(?_0+190)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  986         if (isNumeric)
        MOV       A, [SP+0x02]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        CMP0      A                  ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_36  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
//  987         {
//  988             /* Device uses little-endian -> Print numeric values in reverse order for big-endian output */
//  989             for (int i = attributeLength - 1; i >= 0; i--)
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        DECW      AX                 ;; 1 cycle
        BT        A.7, ??R_DHCPV6_VendorOptionRecvCallback_35  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  990             {
//  991                 R_Modem_print("%02X", AppCmdBuf[i]);
??AppCmd_ProcessCmd_PIB_GETR_0:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #LWRD(_AppCmdBuf)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdBuf)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, #LWRD(?_0+198)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
//  992             }
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        DECW      AX                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        BT        A.7, ??R_DHCPV6_VendorOptionRecvCallback_35  ;; 5 cycles
        ; ------------------------------------- Block: 23 cycles
        BR        S:??AppCmd_ProcessCmd_PIB_GETR_0  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  993         }
//  994         else
//  995         {
//  996             for (uint16_t i = 0; i < attributeLength; i++)
//  997             {
//  998                 R_Modem_print("%02X", AppCmdBuf[i]);
//  999             }
// 1000         }
// 1001     }
// 1002 
// 1003     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_35:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
        BR        F:?Subroutine5     ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??R_DHCPV6_VendorOptionRecvCallback_36:
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??AppCmd_ProcessCmd_PIB_GETR_1:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??R_DHCPV6_VendorOptionRecvCallback_35  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        ADDW      AX, #LWRD(_AppCmdBuf)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdBuf)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, #LWRD(?_0+198)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+18
        BR        S:??AppCmd_ProcessCmd_PIB_GETR_1  ;; 3 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 20 cycles
        ; ------------------------------------- Total: 206 cycles
// 1004 }
// 1005 
// 1006 /********************************************************************************
// 1007 * Function Name     : AppCmd_ProcessCmd_PIB_SETR
// 1008 * Description       : Process "PIB_SETR" command
// 1009 * Arguments         : pCmd ... Pointer to command
// 1010 *                   :   PIB_SETR (Handle) (AttributeId) (IsNumeric) (AttributeLength) (AttributeValueBytes)
// 1011 * Return Value      : None
// 1012 * Output            : Confirm command
// 1013 *                   :   PIB_SETC (Handle) (Status)
// 1014 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_PIB_SETR
        CODE
// 1015 static void AppCmd_ProcessCmd_PIB_SETR(uint8_t* pCmd)
// 1016 {
_AppCmd_ProcessCmd_PIB_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 54
        SUBW      SP, #0x32          ;; 1 cycle
          CFI CFA SP+58
// 1017     uint8_t handle;
// 1018     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFD6        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x9           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+64
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+66
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+64
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+62
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 1019 
// 1020     uint8_t attributeId;
// 1021     res |= AppCmd_HexStrToNum(&pCmd, &attributeId, sizeof(attributeId), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+64
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x9           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+68
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+66
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x0F], A       ;; 1 cycle
// 1022     r_boolean_t isNumeric;
// 1023     res |= AppCmd_HexStrToNum(&pCmd, &isNumeric, sizeof(isNumeric), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+74
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+72
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+70
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x12], A       ;; 1 cycle
// 1024     uint16_t attributeLength;
// 1025     res |= AppCmd_HexStrToNum(&pCmd, &attributeLength, sizeof(attributeLength), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+76
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+78
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+76
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+74
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x17]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x10], A       ;; 1 cycle
// 1026 
// 1027     uint8_t attributeValue[33];
// 1028     uint16_t expectedAttributeLength = R_NWK_GetAttrLength(attributeId);
        MOV       A, [SP+0x11]       ;; 1 cycle
          CFI FunCall _R_NWK_GetAttrLength
        CALL      F:_R_NWK_GetAttrLength  ;; 3 cycles
// 1029 
// 1030     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+58
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_37  ;; 4 cycles
        ; ------------------------------------- Block: 118 cycles
// 1031     {
// 1032         if (attributeLength > expectedAttributeLength || attributeLength == 0)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        BC        ??R_DHCPV6_VendorOptionRecvCallback_38  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_39  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1033         {
// 1034             res = R_RESULT_INVALID_PARAMETER;
??R_DHCPV6_VendorOptionRecvCallback_38:
        MOV       A, #0x11           ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_40  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1035         }
// 1036         else if (attributeLength > sizeof(attributeValue))
??R_DHCPV6_VendorOptionRecvCallback_39:
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x22          ;; 1 cycle
        BC        ??R_DHCPV6_VendorOptionRecvCallback_41  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1037         {
// 1038             res = R_RESULT_INSUFFICIENT_BUFFER;
        MOV       A, #0x14           ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_40  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1039         }
// 1040     }
// 1041 
// 1042     if (res == R_RESULT_SUCCESS)
// 1043     {
// 1044         res = AppCmd_HexStrToNum(&pCmd, attributeValue, attributeLength, !isNumeric);
??R_DHCPV6_VendorOptionRecvCallback_41:
        MOV       A, [SP+0x04]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
        ONEB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_PIB_SETR_0:
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+60
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+62
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+60
        POP       BC                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+60
        MOV       [SP+0x10], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+58
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+64
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+66
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+64
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+62
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1045     }
// 1046 
// 1047     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+58
        MOV       [SP], A            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_37  ;; 4 cycles
        ; ------------------------------------- Block: 38 cycles
// 1048     {
// 1049         // Expand attribute value with zeros to match attribute length expected by NWK API
// 1050         R_memset(&attributeValue[attributeLength], 0, expectedAttributeLength - attributeLength);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+60
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+64
        POP       DE                 ;; 1 cycle
          CFI CFA SP+62
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_42  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        CLRB      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_42:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_PIB_SETR_1:
        CLRB      X                  ;; 1 cycle
          CFI FunCall _R_memset
        CALL      F:_R_memset        ;; 3 cycles
// 1051         attributeLength = expectedAttributeLength;
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 1052     }
// 1053 
// 1054     if (res == R_RESULT_SUCCESS)
// 1055     {
// 1056         res = R_NWK_SetRequest(attributeId, attributeValue, attributeLength);
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+58
        ; ------------------------------------- Block: 15 cycles
??R_DHCPV6_VendorOptionRecvCallback_40:
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1057     }
// 1058 
// 1059     R_Modem_print("PIB_SETC %02X %02X", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_37:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        MOV       A, [SP+0x07]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        MOVW      DE, #LWRD(?_0+204)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1060     R_Modem_print("\n");
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1061 }
        ADDW      SP, #0x3A          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 25 cycles
        ; ------------------------------------- Total: 250 cycles
// 1062 
// 1063 /********************************************************************************
// 1064 * Function Name     : AppCmd_ProcessCmd_WHTLST_GETR
// 1065 * Description       : Process "WHTLST_GETR" command
// 1066 * Arguments         : pCmd ... Pointer to command
// 1067 *                   :   WHTLST_GETR (Handle) [(Options)]
// 1068 * Return Value      : None
// 1069 * Output            : Confirm command
// 1070 *                   :   WHTLST_GETC (Handle) (Status) (MacAddress1) (MacAddress2) ... (MacAddress16)
// 1071 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_WHTLST_GETR
        CODE
// 1072 static void AppCmd_ProcessCmd_WHTLST_GETR(uint8_t* pCmd)
// 1073 {
_AppCmd_ProcessCmd_WHTLST_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 142
        SUBW      SP, #0x8A          ;; 1 cycle
          CFI CFA SP+146
// 1074     /* Get command parameters */
// 1075     uint8_t handle;
// 1076     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFF7A        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+148
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+150
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+152
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+154
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+152
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+150
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 1077     uint16_t options;
// 1078     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+152
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+154
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+156
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+158
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+156
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+154
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1079 
// 1080     r_app_whitelist_buffer_t confirm_buffer = {{ 0 }};
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+146
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_46)    ;; 1 cycle
        MOV       ES, #BYTE3(?_46)   ;; 1 cycle
        MOVW      BC, #0x82          ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
// 1081     res |= R_NWK_GetRequest(R_NWK_macWhitelist, &confirm_buffer, sizeof(confirm_buffer));
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x83           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 1082     /* Always return R_WHITELIST_TABLE_LENGTH entries (required by developer UI). Since confirm_buffer is initialized
// 1083      * with zero, the remaining whitelist entries are all-zero addresses, which is fine. */
// 1084     confirm_buffer.whitelist.count = R_WHITELIST_TABLE_LENGTH;
        MOV       A, #0x10           ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
// 1085 
// 1086     R_Modem_print("WHTLST_GETC %02X %02X", handle, res);
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+148
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+150
        MOVW      DE, #LWRD(?_46+130)  ;; 1 cycle
        MOV       A, #BYTE3(?_46)    ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1087     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+146
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_43  ;; 4 cycles
        ; ------------------------------------- Block: 103 cycles
// 1088     {
// 1089         for (uint16_t i = 0; i < confirm_buffer.whitelist.count; i++)
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_44  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
// 1090         {
// 1091             R_Modem_print(" ");
??AppCmd_ProcessCmd_WHTLST_GETR_0:
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1092             AppPrintMACAddr(confirm_buffer.whitelist.entries[i].bytes);
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_45  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        CLRB      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_45:
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_WHTLST_GETR_1:
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 1093         }
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
??R_DHCPV6_VendorOptionRecvCallback_44:
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        BC        ??AppCmd_ProcessCmd_WHTLST_GETR_0  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 1094     }
// 1095     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_43:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1096 }
        ADDW      SP, #0x8E          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 165 cycles
// 1097 
// 1098 /********************************************************************************
// 1099 * Function Name     : AppCmd_ProcessCmd_WHTLST_SETR
// 1100 * Description       : Process "WHTLST_SETR" command
// 1101 * Arguments         : pCmd ... Pointer to command
// 1102 *                   :   WHTLST_SETR (Handle) [(Options)] [(MACAddress1) (MACAddress2) ...]
// 1103 * Return Value      : None
// 1104 * Output            : Confirm command
// 1105 *                   :   WHTLST_SETR (Handle) (Status)
// 1106 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_WHTLST_SETR
        CODE
// 1107 static void AppCmd_ProcessCmd_WHTLST_SETR(uint8_t* pCmd)
// 1108 {
_AppCmd_ProcessCmd_WHTLST_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 146
        SUBW      SP, #0x8E          ;; 1 cycle
          CFI CFA SP+150
// 1109     r_result_t res;
// 1110     uint8_t handle;
// 1111     uint16_t options;
// 1112     uint8_t i;
// 1113     r_app_whitelist_buffer_t request = {{ 0}};
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_48)    ;; 1 cycle
        MOV       ES, #BYTE3(?_48)   ;; 1 cycle
        MOVW      BC, #0x82          ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
// 1114 
// 1115     /* Get command parameters */
// 1116     /* (Handle) */
// 1117     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        ADDW      AX, #0x82          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFF7A        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+152
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+154
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+156
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+158
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+156
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+154
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 1118 
// 1119     /* [(Options)] */
// 1120     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+156
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+158
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+160
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+162
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+160
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+158
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 1121 
// 1122     /* Add MAC addresses to the whitelist set request */
// 1123     for (i = 0; i < R_WHITELIST_TABLE_LENGTH; i++)
        MOV       [SP+0x09], A       ;; 1 cycle
        MOV       A, #0x10           ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+150
        MOV       [SP+0x02], A       ;; 1 cycle
        ; ------------------------------------- Block: 71 cycles
// 1124     {
// 1125         AppCmd_SkipWhiteSpace(&pCmd);
??AppCmd_ProcessCmd_WHTLST_SETR_0:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppCmd_SkipWhiteSpace
        CALL      F:_AppCmd_SkipWhiteSpace  ;; 3 cycles
// 1126         if (res != R_RESULT_SUCCESS || *pCmd == '\n')
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_46  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       A, [SP+0x90]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x8E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0xA            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_47  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 1127         {
// 1128             break;  // Stop if previous step failed or there are no more arguments in the command string
// 1129         }
// 1130 
// 1131         res |= AppCmd_HexStrToNum(&pCmd, request.whitelist.entries[request.whitelist.count].bytes,
// 1132                                   sizeof(request.whitelist.entries[request.whitelist.count]), R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+152
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+154
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x11]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_48  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
        CLRB      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_48:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_WHTLST_SETR_1:
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+156
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+158
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+156
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+154
          CFI FunCall _AppHexStrToNum
        CALL      F:_AppHexStrToNum  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+150
        CMP0      A                  ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_49  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOV       A, #0x11           ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1133 
// 1134         /* Add MAC address to whitelist set request if it is not all-zero */
// 1135         if (MEMISNOTZERO_A(request.whitelist.entries[request.whitelist.count].bytes))
??R_DHCPV6_VendorOptionRecvCallback_49:
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+152
        MOV       X, #0x8            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+154
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x11]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_50  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
        CLRB      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_50:
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_MemCmpZero
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_WHTLST_SETR_2:
        CALL      F:_R_MemCmpZero    ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+150
        CMP0      A                  ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_51  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 1136         {
// 1137             request.whitelist.count++;
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x0D]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOV       [SP+0x0D], A       ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
// 1138         }
// 1139     }
??R_DHCPV6_VendorOptionRecvCallback_51:
        MOV       A, [SP+0x02]       ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??AppCmd_ProcessCmd_WHTLST_SETR_0  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
??R_DHCPV6_VendorOptionRecvCallback_46:
        MOV       A, [SP+0x01]       ;; 1 cycle
// 1140 
// 1141     if (res == R_RESULT_SUCCESS)
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_52  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1142     {
// 1143         res |= R_NWK_SetRequest(R_NWK_macWhitelist, &request,
// 1144                                 sizeof(request.whitelist) + (request.whitelist.count * sizeof(request.whitelist.entries[0])));
??R_DHCPV6_VendorOptionRecvCallback_47:
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x0D]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x83           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        ; ------------------------------------- Block: 19 cycles
// 1145     }
// 1146 
// 1147     R_Modem_print("WHTLST_SETC %02X %02X", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_52:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+152
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+154
        MOVW      DE, #LWRD(?_48+130)  ;; 1 cycle
        MOV       A, #BYTE3(?_48)    ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1148     R_Modem_print("\n");
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1149 }
        ADDW      SP, #0x96          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 24 cycles
        ; ------------------------------------- Total: 256 cycles
// 1150 
// 1151 /********************************************************************************
// 1152 * Function Name     : AppCmd_ProcessCmd_GTKS_GETR
// 1153 * Description       : Process "GTKS_GETR" command
// 1154 * Arguments         : pCmd ... Pointer to command
// 1155 *                   :   GTKS_GETR (Handle)
// 1156 * Return Value      : None
// 1157 * Output            : Confirm command
// 1158 *                   :   GTKS_GETC (Handle) (Status) (Mask) ([GTKs])
// 1159 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_GTKS_GETR
        CODE
// 1160 static void AppCmd_ProcessCmd_GTKS_GETR(uint8_t* pCmd)
// 1161 {
_AppCmd_ProcessCmd_GTKS_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
// 1162     r_result_t res;
// 1163 
// 1164     /* Get command parameters */
// 1165     uint8_t handle;
// 1166     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 1167     uint16_t options;
// 1168     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
// 1169 
// 1170     R_Modem_print("GTKS_GETC %02X %02X", handle, res);
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x0B]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      DE, #LWRD(?_0+224)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1171 
// 1172     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_53  ;; 4 cycles
        ; ------------------------------------- Block: 77 cycles
// 1173     {
// 1174 #if R_AUTH_NUM_GTKS
// 1175         R_Modem_print(" %02x", p_aplGlobal->auth.gtks_valid);
// 1176         for (size_t i = 0; i < R_AUTH_NUM_GTKS; i++)
// 1177         {
// 1178             if (p_aplGlobal->auth.gtks_valid & (1 << i))
// 1179             {
// 1180                 R_Modem_print(" ");
// 1181                 AppPrintBytesAsHexString(p_aplGlobal->auth.gtks[i], R_AUTH_GTK_SIZE);
// 1182             }
// 1183         }
// 1184 #else  /* if R_AUTH_NUM_GTKS */
// 1185         R_Modem_print(" %02x", 0);
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      DE, #LWRD(?_0+244)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 8 cycles
// 1186 #endif /* if R_AUTH_NUM_GTKS */
// 1187     }
// 1188     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_53:
        REQUIRE ?Subroutine0
        ; // Fall through to label ?Subroutine0
// 1189 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+16
        CODE
?Subroutine0:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _AppCmd_ProcessCmd_GTKS_GETR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_TASKSTATUS_GETR _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 12 cycles
// 1190 
// 1191 /********************************************************************************
// 1192 * Function Name     : AppCmd_ProcessCmd_GTKS_SETR
// 1193 * Description       : Process "GTKS_SETR" command
// 1194 * Arguments         : pCmd ... Pointer to command
// 1195 *                   :   GTKS_SETR (Handle) (Options) (Mask) ([GTKS])
// 1196 * Return Value      : None
// 1197 * Output            : Confirm command
// 1198 *                   :   GTKS_SETC (Handle) (Status)
// 1199 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_GTKS_SETR
        CODE
// 1200 static void AppCmd_ProcessCmd_GTKS_SETR(uint8_t* pCmd)
// 1201 {
_AppCmd_ProcessCmd_GTKS_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
// 1202     /* Get command parameters */
// 1203     uint8_t handle;
// 1204     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1205     uint16_t options;
// 1206     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1207     uint8_t mask;
// 1208     res |= AppCmd_HexStrToNum(&pCmd, &mask, sizeof(mask), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xD           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+30
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+28
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1209 
// 1210 #if R_BR_AUTHENTICATOR_ENABLED
// 1211     if (res == R_RESULT_SUCCESS)
// 1212     {
// 1213         uint8_t gtks[R_KEY_NUM][R_AUTH_GTK_SIZE];
// 1214         for (int i = 0; i < R_KEY_NUM; i++)
// 1215         {
// 1216             if (mask & (1 << i))
// 1217             {
// 1218                 AppCmd_HexStrToNum(&pCmd, gtks[i], sizeof(gtks[i]), R_TRUE);
// 1219                 R_AUTH_SetGTK(p_aplGlobal, i, gtks[i]);
// 1220             }
// 1221         }
// 1222 
// 1223         if (AppCmdConfig.deviceType == R_BORDERROUTER && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
// 1224         {
// 1225             res = R_NWK_IncreasePanVersionRequest();
// 1226         }
// 1227     }
// 1228 #else  /* if R_BR_AUTHENTICATOR_ENABLED */
// 1229     res = R_RESULT_UNSUPPORTED_FEATURE;
// 1230 #endif /* if R_BR_AUTHENTICATOR_ENABLED */
// 1231 
// 1232     R_Modem_print("GTKS_SETC %02X %02X\n", handle, res);
        MOVW      AX, #0x12          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      DE, #LWRD(?_50)    ;; 1 cycle
        MOV       A, #BYTE3(?_50)    ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 86 cycles
        ; ------------------------------------- Total: 86 cycles
// 1233 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+32
        CODE
?Subroutine2:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 2 cycles
        ; ------------------------------------- Total: 2 cycles
        REQUIRE ??Subroutine12_0
        ; // Fall through to label ??Subroutine12_0

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+32
          CFI FunCall _AppCmd_ProcessCmd_GTKS_SETR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_LGTKS_SETR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_VERBOSE_SETR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_LOGVERSION_GETR _R_Modem_print
        CODE
??Subroutine12_0:
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x1C          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 1234 
// 1235 /********************************************************************************
// 1236 * Function Name     : AppCmd_ProcessCmd_LGTKS_GETR
// 1237 * Description       : Process "LGTKS_GETR" command
// 1238 * Arguments         : pCmd ... Pointer to command
// 1239 *                   :   LGTKS_GETR (Handle)
// 1240 * Return Value      : None
// 1241 * Output            : Confirm command
// 1242 *                   :   LGTKS_GETC (Handle) (Status) (Mask) ([LGTKs])
// 1243 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_LGTKS_GETR
        CODE
// 1244 static void AppCmd_ProcessCmd_LGTKS_GETR(uint8_t* pCmd)
// 1245 {
_AppCmd_ProcessCmd_LGTKS_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 18
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+22
// 1246     r_result_t res;
// 1247 
// 1248     /* Get command parameters */
// 1249     uint8_t handle;
// 1250     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFF6        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 1251     uint16_t options;
// 1252     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
// 1253 
// 1254     R_Modem_print("LGTKS_GETC %02X %02X", handle, res);
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      DE, #LWRD(?_0+250)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1255 
// 1256     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_54  ;; 4 cycles
        ; ------------------------------------- Block: 79 cycles
// 1257     {
// 1258 #if R_AUTH_NUM_LGTKS
// 1259         uint8_t lgtkLiveness = (p_aplGlobal->auth.gtks_valid & R_AUTH_LGTK_LIVENESS_MASK) >> R_AUTH_NUM_GTKS;
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0x80]    ;; 2 cycles
        AND       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 1260         R_Modem_print(" %02x", lgtkLiveness);
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      DE, #LWRD(?_0+244)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1261         for (size_t i = 0; i < R_AUTH_NUM_LGTKS; i++)
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+22
        ; ------------------------------------- Block: 24 cycles
// 1262         {
// 1263             if (lgtkLiveness & (1 << i))
??AppCmd_ProcessCmd_LGTKS_GETR_0:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ONEB      A                  ;; 1 cycle
          CFI FunCall ?C_LSH_L01
        CALL      N:?C_LSH_L01       ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        AND       A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_55  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 1264             {
// 1265                 R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1266                 AppPrintBytesAsHexString(p_aplGlobal->auth.gtks[i + R_AUTH_NUM_GTKS], R_AUTH_GTK_SIZE);
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x81          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 26 cycles
??AppCmd_ProcessCmd_LGTKS_GETR_1:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      DE, #LWRD(?_0+198)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
        CMPW      AX, #0x10          ;; 1 cycle
        BC        ??AppCmd_ProcessCmd_LGTKS_GETR_1  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
// 1267             }
// 1268         }
??R_DHCPV6_VendorOptionRecvCallback_55:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, #0x3           ;; 1 cycle
        BC        ??AppCmd_ProcessCmd_LGTKS_GETR_0  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 1269 #else /* R_AUTH_NUM_LGTKS */
// 1270         R_Modem_print(" %02x", 0);
// 1271 #endif /* R_AUTH_NUM_LGTKS */
// 1272     }
// 1273     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_54:
        BR        F:?Subroutine1     ;; 3 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 182 cycles
// 1274 }
// 1275 
// 1276 /********************************************************************************
// 1277 * Function Name     : AppCmd_ProcessCmd_LGTKS_SETR
// 1278 * Description       : Process "LGTKS_SETR" command
// 1279 * Arguments         : pCmd ... Pointer to command
// 1280 *                   :   LGTKS_SETR (Handle) (Options) (Mask) ([LGTKS])
// 1281 * Return Value      : None
// 1282 * Output            : Confirm command
// 1283 *                   :   LGTKS_SETC (Handle) (Status)
// 1284 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock18 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_LGTKS_SETR
        CODE
// 1285 static void AppCmd_ProcessCmd_LGTKS_SETR(uint8_t* pCmd)
// 1286 {
_AppCmd_ProcessCmd_LGTKS_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
// 1287     /* Get command parameters */
// 1288     uint8_t handle;
// 1289     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1290     uint16_t options;
// 1291     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1292     uint8_t mask;
// 1293     res |= AppCmd_HexStrToNum(&pCmd, &mask, sizeof(mask), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xD           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+30
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+28
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1294 
// 1295 #if R_BR_AUTHENTICATOR_ENABLED
// 1296     if (res == R_RESULT_SUCCESS)
// 1297     {
// 1298         uint8_t gtks[R_AUTH_NUM_LGTKS][R_AUTH_GTK_SIZE];
// 1299         for (int i = 0; i < R_AUTH_NUM_LGTKS; i++)
// 1300         {
// 1301             if (mask & (1 << i))
// 1302             {
// 1303                 AppCmd_HexStrToNum(&pCmd, gtks[i], sizeof(gtks[i]), R_TRUE);
// 1304 
// 1305                 /* Convert key index since r_auth_br uses continuous key index (LGTK index begins after GTK indices) */
// 1306                 res = R_AUTH_SetGTK(p_aplGlobal, i + R_AUTH_NUM_GTKS, gtks[i]);
// 1307             }
// 1308         }
// 1309 
// 1310         if (res == R_RESULT_SUCCESS)
// 1311         {
// 1312             if (AppCmdConfig.deviceType == R_BORDERROUTER && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
// 1313             {
// 1314                 res = R_NWK_IncreaseLfnVersionRequest();
// 1315             }
// 1316         }
// 1317     }
// 1318 #else  /* if R_BR_AUTHENTICATOR_ENABLED */
// 1319     res = R_RESULT_UNSUPPORTED_FEATURE;
// 1320 #endif /* if R_BR_AUTHENTICATOR_ENABLED */
// 1321 
// 1322     R_Modem_print("LGTKS_SETC %02X %02X\n", handle, res);
        MOVW      AX, #0x12          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      DE, #LWRD(?_51)    ;; 1 cycle
        MOV       A, #BYTE3(?_51)    ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock18
        ; ------------------------------------- Block: 86 cycles
        ; ------------------------------------- Total: 86 cycles
// 1323 }
// 1324 
// 1325 /********************************************************************************
// 1326 * Function Name     : AppCmd_ProcessCmd_KEYLIFETIMES_SETR
// 1327 * Description       : Process "KEYLIFETIMES_SETR" command
// 1328 * Arguments         : pCmd ... Pointer to command
// 1329 *                   :   KEYLIFETIMES_SETR (Handle) (Options) (PMK_LIFETIME) (PTK_LIFETIME) (GTK_LIFETIME)
// 1330 *                                         (GTK_NEW_ACTIVATION_TIME_FRACTION) (REVOCATION_LIFETIME_REDUCTION_FRACTION)
// 1331 * Return Value      : None
// 1332 * Output            : Confirm command
// 1333 *                   :   KEYLIFETIMES_SETC (Handle) (Status)
// 1334 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock19 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_KEYLIFETIMES_SETR
        CODE
// 1335 static void AppCmd_ProcessCmd_KEYLIFETIMES_SETR(uint8_t* pCmd)
// 1336 {
_AppCmd_ProcessCmd_KEYLIFETIMES_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 32
        SUBW      SP, #0x1C          ;; 1 cycle
          CFI CFA SP+36
// 1337     r_result_t res;
// 1338     uint8_t handle;
// 1339     uint16_t options;
// 1340 
// 1341     /* Get command parameters */
// 1342     /* (Handle) */
// 1343     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFEA        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 1344 
// 1345     /* (Options) */
// 1346     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x16          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+46
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+44
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
// 1347 
// 1348     uint32_t pmk_lifetime_minutes;
// 1349     res |= AppCmd_HexStrToNum(&pCmd, &pmk_lifetime_minutes, sizeof(pmk_lifetime_minutes), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+50
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+48
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x11], A       ;; 1 cycle
// 1350 
// 1351     uint32_t ptk_lifetime_minutes;
// 1352     res |= AppCmd_HexStrToNum(&pCmd, &ptk_lifetime_minutes, sizeof(ptk_lifetime_minutes), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+54
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+52
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x14], A       ;; 1 cycle
// 1353 
// 1354     uint32_t gtk_lifetime_minutes;
// 1355     res |= AppCmd_HexStrToNum(&pCmd, &gtk_lifetime_minutes, sizeof(gtk_lifetime_minutes), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+58
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+56
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x17], A       ;; 1 cycle
// 1356 
// 1357     uint16_t gtk_new_activation_time;
// 1358     res |= AppCmd_HexStrToNum(&pCmd, &gtk_new_activation_time, sizeof(gtk_new_activation_time), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+64
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+62
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+60
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x1A], A       ;; 1 cycle
// 1359 
// 1360     uint16_t revocation_lifetime_reduction;
// 1361     res |= AppCmd_HexStrToNum(&pCmd, &revocation_lifetime_reduction, sizeof(revocation_lifetime_reduction), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+64
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x26          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+66
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+64
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 1362 
// 1363 #if R_BR_AUTHENTICATOR_ENABLED
// 1364     p_aplGlobal->auth.br.key_lifetimes.pmk_lifetime_minutes = pmk_lifetime_minutes;
// 1365     p_aplGlobal->auth.br.key_lifetimes.ptk_lifetime_minutes = ptk_lifetime_minutes;
// 1366     p_aplGlobal->auth.br.key_lifetimes.gtk_lifetime_minutes = gtk_lifetime_minutes;
// 1367     p_aplGlobal->auth.br.key_lifetimes.gtk_new_activation_time_fraction = gtk_new_activation_time;
// 1368     p_aplGlobal->auth.br.key_lifetimes.revocation_lifetime_reduction_fraction = revocation_lifetime_reduction;
// 1369 #else
// 1370     res |= R_RESULT_UNSUPPORTED_FEATURE;  // KEYLIFETIMES_SETR can only be processed by border routers
// 1371 #endif /* R_BR_AUTHENTICATOR_ENABLED */
// 1372 
// 1373     R_Modem_print("KEYLIFETIMES_SETC %02X %02X", handle, res);
        MOV       A, [SP+0x21]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x1C]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x20]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x1F]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x1E]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, #0x12           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOV       A, [SP+0x1F]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      DE, #LWRD(?_0+272)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1374     R_Modem_print("\n");
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1375 }
        ADDW      SP, #0x40          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock19
        ; ------------------------------------- Block: 202 cycles
        ; ------------------------------------- Total: 202 cycles
// 1376 
// 1377 /********************************************************************************
// 1378 * Function Name     : AppCmd_ProcessCmd_CERT_SWITCHR
// 1379 * Description       : Process "CERT_SWITCHR" command
// 1380 * Arguments         : pCmd ... Pointer to command
// 1381 *                   :   CERT_SWITCHR (Handle) (Index)
// 1382 * Return Value      : None
// 1383 * Output            : Confirm command
// 1384 *                   :   CERTSWITCHC (Handle) (Status)
// 1385 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock20 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_CERT_SWITCHR
        CODE
// 1386 static void AppCmd_ProcessCmd_CERT_SWITCHR(uint8_t* pCmd)
// 1387 {
_AppCmd_ProcessCmd_CERT_SWITCHR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 1388     uint8_t handle;
// 1389     AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1390 
// 1391     r_result_t res = R_RESULT_UNSUPPORTED_FEATURE;
// 1392 #if R_AUTH_MULTIPLE_CERTS
// 1393     uint8_t cert_index = 0;
// 1394     res = AppCmd_HexStrToNum(&pCmd, &cert_index, sizeof(cert_index), R_FALSE);
// 1395 
// 1396     if (res == R_RESULT_SUCCESS)
// 1397     {
// 1398         switch (cert_index)
// 1399         {
// 1400             case 0:
// 1401                 r_auth_certs_use_alternate = 0;
// 1402                 break;
// 1403 
// 1404             case 1:
// 1405                 r_auth_certs_use_alternate = 1;
// 1406                 break;
// 1407 
// 1408             default:
// 1409                 res = R_RESULT_INVALID_PARAMETER;
// 1410                 break;
// 1411         }
// 1412     }
// 1413 #endif /* R_AUTH_MULTIPLE_CERTS */
// 1414 
// 1415     R_Modem_print("CERT_SWITCHC %02X %02X\n", handle, res);
        MOVW      AX, #0x12          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      DE, #LWRD(?_52)    ;; 1 cycle
        MOV       A, #BYTE3(?_52)    ;; 1 cycle
        BR        F:?Subroutine5     ;; 3 cycles
          CFI EndBlock cfiBlock20
        ; ------------------------------------- Block: 36 cycles
        ; ------------------------------------- Total: 36 cycles
// 1416 }
// 1417 
// 1418 /********************************************************************************
// 1419 * Function Name     : AppCmd_ProcessCmd_VERBOSE_SETR
// 1420 * Description       : Process "VERBOSE_SETR" command
// 1421 * Arguments         : pCmd ... Pointer to command
// 1422 *                   :   VERBOSE_SETR (Handle) (Options) (VerbosityLevel)
// 1423 * Return Value      : None
// 1424 * Output            : Confirm command: VERBOSE_SETC (Handle) (Status) (RLog Version ID)
// 1425 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock21 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_VERBOSE_SETR
        CODE
// 1426 static void AppCmd_ProcessCmd_VERBOSE_SETR(uint8_t* pCmd)
// 1427 {
_AppCmd_ProcessCmd_VERBOSE_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 28
        SUBW      SP, #0x18          ;; 1 cycle
          CFI CFA SP+32
// 1428     uint8_t handle;
// 1429     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFEE        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+38
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 1430 
// 1431     uint16_t options;
// 1432     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x0B], A       ;; 1 cycle
// 1433 
// 1434     uint8_t verbosity;
// 1435     res |= AppCmd_HexStrToNum(&pCmd, &verbosity, sizeof(verbosity), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xD           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+48
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+46
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+44
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x0F]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 1436 
// 1437     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+32
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_56  ;; 4 cycles
        ; ------------------------------------- Block: 95 cycles
// 1438     {
// 1439 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
// 1440         if (verbosity <= R_LOG_THRESHOLD)
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BNC       ??R_DHCPV6_VendorOptionRecvCallback_57  ;; 4 cycles
          CFI FunCall _R_LOG_SetSeverityThreshold
        ; ------------------------------------- Block: 6 cycles
// 1441         {
// 1442             R_LOG_SetSeverityThreshold(verbosity);
        CALL      F:_R_LOG_SetSeverityThreshold  ;; 3 cycles
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_56  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1443 #if R_MODEM_CLIENT
// 1444             res = R_NWK_SetRequest(R_NWK_logVerbosity, &verbosity, sizeof(verbosity));  // Set log level on modem server
// 1445 #endif
// 1446         }
// 1447         else
// 1448         {
// 1449             res = R_RESULT_INVALID_PARAMETER;  // Logging framework was compiled with lower severity level
??R_DHCPV6_VendorOptionRecvCallback_57:
        MOV       A, #0x11           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1450         }
// 1451 #else
// 1452         if (verbosity > 0)
// 1453         {
// 1454             res = R_RESULT_UNSUPPORTED_FEATURE;  // Logging framework was disabled at compile time
// 1455         }
// 1456 #endif
// 1457     }
// 1458 
// 1459     uint8_t versionId[R_LOG_VERSION_ID_SIZE] = { 0 };
??R_DHCPV6_VendorOptionRecvCallback_56:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_0+48)  ;; 1 cycle
        MOV       ES, #BYTE3(?_0)    ;; 1 cycle
        MOVW      BC, #0x10          ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
// 1460 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
// 1461     size_t versionIdSize = R_LOG_GetVersionID(versionId, sizeof(versionId));
// 1462     if (versionIdSize != sizeof(versionId))
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_LOG_GetVersionID
        CALL      F:_R_LOG_GetVersionID  ;; 3 cycles
        CMPW      AX, #0x10          ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_58  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
// 1463     {
// 1464         res = R_RESULT_FAILED;
        MOV       A, #0x10           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1465     }
// 1466 #endif
// 1467 
// 1468     R_Modem_print("VERBOSE_SETC %02X %02X ", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_58:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      DE, #LWRD(?_0+300)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1469     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+32
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_59  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 1470     {
// 1471         for (size_t i = 0; i < sizeof(versionId); i++)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0x10           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
// 1472         {
// 1473             R_Modem_print("%02X", versionId[i]);
??AppCmd_ProcessCmd_VERBOSE_SETR_0:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      DE, #LWRD(?_0+198)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1474         }
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+32
        BNZ       ??AppCmd_ProcessCmd_VERBOSE_SETR_0  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
// 1475     }
// 1476     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_59:
        BR        F:?Subroutine2     ;; 3 cycles
          CFI EndBlock cfiBlock21
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 182 cycles
// 1477 }
// 1478 
// 1479 /********************************************************************************
// 1480 * Function Name     : AppCmd_ProcessCmd_LOGVERSION_GETR
// 1481 * Description       : Process "LOGVERSION_GETR" command
// 1482 * Arguments         : pCmd ... Pointer to command
// 1483 *                   :   LOGVERSION_GETR (Handle)
// 1484 * Return Value      : None
// 1485 * Output            : Confirm command
// 1486 *                   :   LOGVERSION_GETC (Handle) (Status) (RLog Version ID)
// 1487 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock22 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_LOGVERSION_GETR
        CODE
// 1488 static void AppCmd_ProcessCmd_LOGVERSION_GETR(uint8_t* pCmd)
// 1489 {
_AppCmd_ProcessCmd_LOGVERSION_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 28
        SUBW      SP, #0x18          ;; 1 cycle
          CFI CFA SP+32
// 1490     /* Get command parameters */
// 1491     uint8_t handle;
// 1492     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFEC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+38
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 1493     uint16_t options;
// 1494     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 1495 
// 1496 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
// 1497     uint8_t versionId[16] = { 0 };
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+32
        MOV       [SP+0x01], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_0+64)  ;; 1 cycle
        MOV       ES, #BYTE3(?_0)    ;; 1 cycle
        MOVW      BC, #0x10          ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
// 1498     size_t versionIdSize = R_LOG_GetVersionID(versionId, sizeof(versionId));
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_LOG_GetVersionID
        CALL      F:_R_LOG_GetVersionID  ;; 3 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 1499     if (versionIdSize)
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_60  ;; 4 cycles
        ; ------------------------------------- Block: 80 cycles
// 1500     {
// 1501         res = R_RESULT_INSUFFICIENT_OUTPUT_BUFFER;
        MOV       A, #0x15           ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1502     }
// 1503 #else
// 1504     res = R_RESULT_UNSUPPORTED_FEATURE;
// 1505 #endif
// 1506 
// 1507     R_Modem_print("LOGVERSION_GETC %02X %02X ", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_60:
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      DE, #LWRD(?_0+324)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1508 
// 1509 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
// 1510     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+32
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_61  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 1511     {
// 1512         for (size_t i = 0; i < versionIdSize; i++)
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_61  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 1513         {
// 1514             R_Modem_print("%02X", versionId[i]);
??AppCmd_ProcessCmd_LOGVERSION_GETR_0:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      DE, #LWRD(?_0+198)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1515         }
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        DECW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+32
        OR        A, X               ;; 1 cycle
        BNZ       ??AppCmd_ProcessCmd_LOGVERSION_GETR_0  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
// 1516     }
// 1517 #endif
// 1518 
// 1519     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_61:
        BR        F:?Subroutine2     ;; 3 cycles
          CFI EndBlock cfiBlock22
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 137 cycles
// 1520 }
// 1521 
// 1522 /********************************************************************************
// 1523 * Function Name     : AppCmd_ProcessCmd_LOGENTRIES_GETR
// 1524 * Description       : Process "LOGVERSION_GETR" command
// 1525 * Arguments         : pCmd ... Pointer to command
// 1526 *                   :   LOGENTRIES_GETR (Handle)
// 1527 * Return Value      : None
// 1528 * Output            : Confirm command
// 1529 *                   :   LOGENTRIES_GETC (Handle) (Status) [Binary RLog records]
// 1530 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock23 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_LOGENTRIES_GETR
        CODE
// 1531 static void AppCmd_ProcessCmd_LOGENTRIES_GETR(uint8_t* pCmd)
// 1532 {
_AppCmd_ProcessCmd_LOGENTRIES_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 272
        SUBW      SP, #0xFE          ;; 1 cycle
          CFI CFA SP+262
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+276
// 1533     /* Get command parameters */
// 1534     uint8_t handle;
// 1535     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10C         ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFEF8        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+278
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+280
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+282
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+284
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+282
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+280
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 1536     uint16_t options;
// 1537     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+282
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+284
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+286
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+288
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+286
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+284
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
// 1538 
// 1539 #if !(R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF)
// 1540     res = R_RESULT_UNSUPPORTED_FEATURE;
// 1541 #endif
// 1542     R_Modem_print("LOGENTRIES_GETC %02X %02X ", handle, res);
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+286
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+288
        MOVW      DE, #LWRD(?_0+352)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1543 
// 1544 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
// 1545     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+276
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_62  ;; 4 cycles
        ; ------------------------------------- Block: 80 cycles
// 1546     {
// 1547         uint8_t buf[R_LOG_POLL_BUFFER_SIZE];
// 1548         size_t bytesWritten = 0;
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
// 1549         while ((bytesWritten = R_LOG_GetLog(buf, sizeof(buf))) > 0)
??AppCmd_ProcessCmd_LOGENTRIES_GETR_0:
        MOVW      BC, #0x100         ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_LOG_GetLog
        CALL      F:_R_LOG_GetLog    ;; 3 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_62  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 1550         {
// 1551             AppPrintBytesAsHexString(buf, bytesWritten);
        MOVW      [SP+0x06], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??AppCmd_ProcessCmd_LOGENTRIES_GETR_1:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [DE]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+278
        MOVW      DE, #LWRD(?_0+198)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+276
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??AppCmd_ProcessCmd_LOGENTRIES_GETR_0  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
        BR        S:??AppCmd_ProcessCmd_LOGENTRIES_GETR_1  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1552         }
// 1553     }
// 1554 #endif
// 1555 
// 1556     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_62:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1557 }
        ADDW      SP, #0xFE          ;; 1 cycle
          CFI CFA SP+22
        ADDW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock23
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 144 cycles
// 1558 
// 1559 /********************************************************************************
// 1560 * Function Name     : AppCmd_ProcessCmd_SUBSCP_SETR
// 1561 * Description       : Process "SUBSCP_SETR" command
// 1562 * Arguments         : pCmd ... Pointer to command
// 1563 *                   :   SUBSCP_SETR (Handle) (Options) (StartStop)
// 1564 * Return Value      : None
// 1565 * Output            : Confirm command
// 1566 *                   :   SUBSCP_SETC (Handle) (Status)
// 1567 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock24 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_SUBSCP_SETR
        CODE
// 1568 static void AppCmd_ProcessCmd_SUBSCP_SETR(uint8_t* pCmd)
// 1569 {
_AppCmd_ProcessCmd_SUBSCP_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 1570     uint8_t handle;
// 1571     AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1572 
// 1573     r_result_t res = R_RESULT_UNSUPPORTED_FEATURE;
// 1574 #if R_DEV_TBU_ENABLED && __RX
// 1575     uint16_t options;
// 1576     res = AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
// 1577 
// 1578     uint8_t mode;
// 1579     res |= AppCmd_HexStrToNum(&pCmd, &mode, sizeof(mode), R_FALSE);
// 1580 
// 1581     r_boolean_t subscriptEnabled;
// 1582     if (mode != 0)
// 1583     {
// 1584         subscriptEnabled = R_TRUE;
// 1585     }
// 1586     else
// 1587     {
// 1588         subscriptEnabled = R_FALSE;
// 1589     }
// 1590 
// 1591     if (res == R_RESULT_SUCCESS)
// 1592     {
// 1593         res |= R_NWK_SetRequest(R_NWK_macFrameSubscriptionEnabled, &subscriptEnabled, sizeof(subscriptEnabled));
// 1594     }
// 1595 #endif /* R_DEV_TBU_ENABLED && __RX */
// 1596 
// 1597     R_Modem_print("SUBSCP_SETC %02X %02X\n", handle, res);
        MOVW      AX, #0x12          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      DE, #LWRD(?_53)    ;; 1 cycle
        MOV       A, #BYTE3(?_53)    ;; 1 cycle
          CFI EndBlock cfiBlock24
        ; ------------------------------------- Block: 33 cycles
        ; ------------------------------------- Total: 33 cycles
        REQUIRE ?Subroutine5
        ; // Fall through to label ?Subroutine5
// 1598 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock25 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+18
          CFI FunCall _AppCmd_ProcessCmd_PIB_GETR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_CERT_SWITCHR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_SUBSCP_SETR _R_Modem_print
        CODE
?Subroutine5:
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock25
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 1599 
// 1600 
// 1601 /********************************************************************************
// 1602 * Function Name     : AppCmd_ProcessCmd_VERSION_GETR
// 1603 * Description       : Process "VERSION_GETR" command
// 1604 * Arguments         : pCmd ... Pointer to command
// 1605 *                   :   VERSION_GETR (Handle) (Options)
// 1606 * Return Value      : None
// 1607 * Output            : Confirm command
// 1608 *                   :   VERSION_GETC (Handle) (Status) Wi-SUN-FAN
// 1609 *                                   (Version) (FeatureMask) (MACAddress)
// 1610 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock26 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_VERSION_GETR
        CODE
// 1611 static void AppCmd_ProcessCmd_VERSION_GETR(uint8_t* pCmd)
// 1612 {
_AppCmd_ProcessCmd_VERSION_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 20
        SUBW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+24
// 1613     r_result_t res;
// 1614     uint8_t handle;
// 1615     uint16_t options;
// 1616 
// 1617     uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH] = {0};
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_0+80)  ;; 1 cycle
        MOV       ES, #BYTE3(?_0)    ;; 1 cycle
        MOVW      BC, #0x8           ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
// 1618 
// 1619     /* Get command parameters */
// 1620     /* (Handle) */
// 1621     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFF2        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+30
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+28
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 1622 
// 1623     /* [(Options)] */
// 1624     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+34
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+32
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
// 1625 
// 1626     res |= R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+24
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x80           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 1627 
// 1628     R_Modem_print("VERSION_GETC %02X %02X %s %04X ", handle, res, FAN_STACK_VERSION, featureMask);
        MOV       ES, #BYTE3(_featureMask)  ;; 1 cycle
        MOVW      HL, ES:_featureMask  ;; 2 cycles
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       L, #BYTE3(?_0)     ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      HL, #LWRD(?_0+764)  ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x06]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, [SP+0x09]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      DE, #LWRD(?_0+380)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1629     R_Swap64(macAddr, macAddr);
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, #0xF            ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+40
        POP       DE                 ;; 1 cycle
          CFI CFA SP+38
        POP       BC                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
          CFI FunCall _R_Swap64
        CALL      F:_R_Swap64        ;; 3 cycles
// 1630     AppPrintMACAddr(macAddr);
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
          CFI EndBlock cfiBlock26
        ; ------------------------------------- Block: 120 cycles
        ; ------------------------------------- Total: 120 cycles
// 1631     R_Modem_print("\n");
        REQUIRE ?Subroutine3
        ; // Fall through to label ?Subroutine3
// 1632 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock27 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+34
        CODE
?Subroutine3:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI EndBlock cfiBlock27
        ; ------------------------------------- Block: 2 cycles
        ; ------------------------------------- Total: 2 cycles
        REQUIRE ??Subroutine13_0
        ; // Fall through to label ??Subroutine13_0

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock28 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+34
          CFI FunCall _AppCmd_ProcessCmd_IPV6PREFIX_SETR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_VERSION_GETR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_RPLINFO_GETR _R_Modem_print
        CODE
??Subroutine13_0:
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x1E          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock28
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 1633 
// 1634 /********************************************************************************
// 1635 * Function Name     : AppCmd_ProcessCmd_STATUS_GETR
// 1636 * Description       : Process "STATUS_GETR" command
// 1637 * Arguments         : pCmd ... Pointer to command
// 1638 *                   :   STATUS_GETR  (Handle) (Options)
// 1639 * Return Value      : None
// 1640 * Output            : Confirm command
// 1641 *                   :   STATUS_GETC  (Handle) (Status) (JoinState)
// 1642 *                                    (MacAddress) (IPv6Addr)
// 1643 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock29 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_STATUS_GETR
        CODE
// 1644 static void AppCmd_ProcessCmd_STATUS_GETR(uint8_t* pCmd)
// 1645 {
_AppCmd_ProcessCmd_STATUS_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 36
        SUBW      SP, #0x20          ;; 1 cycle
          CFI CFA SP+40
// 1646     r_result_t res;
// 1647     uint8_t handle;
// 1648     uint16_t option;
// 1649 
// 1650     uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH];
// 1651     uint8_t ipv6Addr[R_IPV6_ADDRESS_LENGTH];
// 1652 
// 1653     /* Get command parameters */
// 1654     /* (Handle) */
// 1655     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x20          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFE2        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+46
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+44
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 1656 
// 1657     /* (option) */
// 1658     res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+50
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+48
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 1659 
// 1660     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+40
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_63  ;; 4 cycles
        ; ------------------------------------- Block: 63 cycles
// 1661     {
// 1662         /* (MACAddr) */
// 1663         res |= R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x80           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 1664 
// 1665         /* (IPv6Addr) */
// 1666         res |= R_NWK_GetRequest(R_NWK_nwkIpv6Address, ipv6Addr, sizeof(ipv6Addr));
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xD1           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 23 cycles
// 1667     }
// 1668 
// 1669     /* Generate confirm STATUS_GETC*/
// 1670     R_Modem_print("STATUS_GETC %02X %02X ", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_63:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      DE, #LWRD(?_0+412)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1671     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+40
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_64  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 1672     {
// 1673         /* JoinState */
// 1674         R_Modem_print("%02X ", p_aplGlobal->nwk.joinState);
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0xB2]    ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1675 
// 1676         /* MACAddr */
// 1677         R_Swap64(macAddr, macAddr);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+46
        POP       DE                 ;; 1 cycle
          CFI CFA SP+44
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
          CFI FunCall _R_Swap64
        CALL      F:_R_Swap64        ;; 3 cycles
// 1678         AppPrintMACAddr(macAddr);
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 1679         R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1680 
// 1681         /* IPv6Addr */
// 1682         AppPrintIPv6Addr(ipv6Addr);
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 1683     }
        ; ------------------------------------- Block: 55 cycles
// 1684     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_64:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI EndBlock cfiBlock29
        ; ------------------------------------- Block: 2 cycles
        ; ------------------------------------- Total: 163 cycles
        REQUIRE ?Subroutine6
        ; // Fall through to label ?Subroutine6
// 1685 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock30 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+40
          CFI FunCall _AppCmd_ProcessCmd_STATUS_GETR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_PHYFAN_SETR _R_Modem_print
        CODE
?Subroutine6:
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x24          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock30
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 1686 
// 1687 /********************************************************************************
// 1688 * Function Name     : AppCmd_ProcessCmd_TASKSTATUS_GETR
// 1689 * Description       : Process "TASKSTATUS_GETR" command
// 1690 * Arguments         : pCmd ... Pointer to command
// 1691 *                   :   TASKSTATUS_GETR  (Handle) (Options)
// 1692 * Return Value      : None
// 1693 * Output            : Confirm command
// 1694 *                   :   TASKSTATUS_GETC  (Handle) (Status) (TaskHandle0 TaskHighWatermark0...)
// 1695 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock31 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_TASKSTATUS_GETR
        CODE
// 1696 static void AppCmd_ProcessCmd_TASKSTATUS_GETR(uint8_t* pCmd)
// 1697 {
_AppCmd_ProcessCmd_TASKSTATUS_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
// 1698     r_result_t res;
// 1699     uint8_t handle;
// 1700     uint16_t option;
// 1701 
// 1702     /* Get command parameters */
// 1703     /* (Handle) */
// 1704     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 1705 
// 1706     /* (option) */
// 1707     res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
// 1708 
// 1709 #if RTOS_USE != 1
// 1710     if (res == R_RESULT_SUCCESS)
// 1711     {
// 1712         res = R_RESULT_INVALID_PARAMETER;
// 1713     }
// 1714 #endif
// 1715 
// 1716     /* Generate confirm TASKSTATUS_GETC */
// 1717     R_Modem_print("TASKSTATUS_GETC %02X %02X ", handle, res);
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      DE, #LWRD(?_0+436)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1718 #if RTOS_USE == 1
// 1719     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_65  ;; 4 cycles
        ; ------------------------------------- Block: 79 cycles
// 1720     {
// 1721         r_os_id_t i = 0;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_66  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1722         while (1)
// 1723         {
// 1724             void* taskHandle = R_OS_GetTaskHandle(i++);
// 1725             if (!taskHandle)
// 1726             {
// 1727                 break;
// 1728             }
// 1729             R_Modem_print("%08x %04x ", (unsigned)(uintptr_t)(taskHandle),
// 1730                           (unsigned)(uxTaskGetStackHighWaterMark(taskHandle) * sizeof(StackType_t)));
??AppCmd_ProcessCmd_TASKSTATUS_GETR_0:
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxTaskGetStackHighWaterMark
        CALL      F:_uxTaskGetStackHighWaterMark  ;; 3 cycles
        ADDW      AX, AX             ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, #LWRD(?_0+464)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        ; ------------------------------------- Block: 14 cycles
??R_DHCPV6_VendorOptionRecvCallback_66:
        MOV       A, [SP+0x01]       ;; 1 cycle
          CFI FunCall _R_OS_GetTaskHandle
        CALL      F:_R_OS_GetTaskHandle  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_67  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_67:
        BNZ       ??AppCmd_ProcessCmd_TASKSTATUS_GETR_0  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1731         }
// 1732 #if defined(__RX)
// 1733         {
// 1734             const uint8_t* ptr = __sectop("SI");
// 1735             const uint8_t* end = __secend("SI");
// 1736             while (*ptr == 0x33 && ptr < end)
// 1737             {
// 1738                 ptr += 1;
// 1739             }
// 1740             R_Modem_print("%08x %04x ",
// 1741                           (unsigned)0xff,
// 1742                           (unsigned)(ptr - (const uint8_t*)__sectop("SI")));
// 1743         }
// 1744 #endif /* __RX */
// 1745     }
// 1746 #endif /* RTOS_USE == 1 */
// 1747     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_65:
        BR        F:?Subroutine0     ;; 3 cycles
          CFI EndBlock cfiBlock31
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 128 cycles
// 1748 }
// 1749 
// 1750 /********************************************************************************
// 1751 * Function Name     : AppCmd_ProcessCmd_RPL_GLOBALREPAIRR
// 1752 * Description       : Process "RPL_ROUTES_REFRESHR" command
// 1753 * Arguments         : pCmd ... Pointer to command
// 1754 *                   :   RPL_GLOBALREPAIRR (Handle)
// 1755 * Return Value      : None
// 1756 * Output            : Confirm command
// 1757 *                   :   RPL_GLOBALREPAIRC (Handle) (Status)
// 1758 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock32 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_RPL_GLOBALREPAIRR
        CODE
// 1759 static void AppCmd_ProcessCmd_RPL_GLOBALREPAIRR(uint8_t* pCmd)
// 1760 {
_AppCmd_ProcessCmd_RPL_GLOBALREPAIRR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 1761     uint8_t handle;
// 1762     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1763 
// 1764     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 28 cycles
// 1765     {
// 1766 #if R_BORDER_ROUTER_ENABLED
// 1767         res = R_NWK_RplGlobalRepairRequest();
// 1768 #else
// 1769         res = R_RESULT_UNSUPPORTED_FEATURE;
        MOV       A, #0x12           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1770 #endif
// 1771     }
// 1772     R_Modem_print("RPL_GLOBALREPAIRC %02X %02X\n", handle, res);
??AppCmd_ProcessCmd_RPL_GLOBALREPAIRR_0:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(?_54)    ;; 1 cycle
        MOV       A, #BYTE3(?_54)    ;; 1 cycle
        BR        F:?Subroutine7     ;; 3 cycles
          CFI EndBlock cfiBlock32
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 41 cycles
// 1773 }
// 1774 
// 1775 /********************************************************************************
// 1776 * Function Name     : AppCmd_ProcessCmd_PAN_VERSION_INCR
// 1777 * Description       : Process "PAN_VERSION_INCR" command
// 1778 * Arguments         : pCmd ... Pointer to command
// 1779 *                   :   PAN_VERSION_INCR (Handle)
// 1780 * Return Value      : None
// 1781 * Output            : Confirm command
// 1782 *                   :   PAN_VERSION_INCC (Handle) (Status)
// 1783 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock33 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_PAN_VERSION_INCR
        CODE
// 1784 static void AppCmd_ProcessCmd_PAN_VERSION_INCR(uint8_t* pCmd)
// 1785 {
_AppCmd_ProcessCmd_PAN_VERSION_INCR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 1786     uint8_t handle;
// 1787     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1788 
// 1789     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 28 cycles
// 1790     {
// 1791 #if R_BORDER_ROUTER_ENABLED
// 1792         res = R_NWK_IncreasePanVersionRequest();
// 1793 #else
// 1794         res = R_RESULT_UNSUPPORTED_FEATURE;
        MOV       A, #0x12           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1795 #endif
// 1796     }
// 1797     R_Modem_print("PAN_VERSION_INCC %02X %02X\n", handle, res);
??AppCmd_ProcessCmd_PAN_VERSION_INCR_0:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(?_55)    ;; 1 cycle
        MOV       A, #BYTE3(?_55)    ;; 1 cycle
        BR        F:?Subroutine7     ;; 3 cycles
          CFI EndBlock cfiBlock33
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 41 cycles
// 1798 }
// 1799 
// 1800 /********************************************************************************
// 1801 * Function Name     : AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR
// 1802 * Description       : Process "RPL_ROUTES_REFRESHR" command
// 1803 * Arguments         : pCmd ... Pointer to command
// 1804 *                   :   RPL_ROUTES_REFRESHR (Handle)
// 1805 * Return Value      : None
// 1806 * Output            : Confirm command
// 1807 *                   :   RPL_ROUTES_REFRESHC (Handle) (Status)
// 1808 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock34 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR
        CODE
// 1809 static void AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR(uint8_t* pCmd)
// 1810 {
_AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 1811     uint8_t handle;
// 1812     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1813 
// 1814     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 28 cycles
// 1815     {
// 1816 #if R_BORDER_ROUTER_ENABLED
// 1817         res = R_NWK_RplRouteRefreshRequest();
// 1818 #else
// 1819         res = R_RESULT_UNSUPPORTED_FEATURE;
        MOV       A, #0x12           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1820 #endif
// 1821     }
// 1822     R_Modem_print("RPL_ROUTES_REFRESHC %02X %02X\n", handle, res);
??AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR_0:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(?_56)    ;; 1 cycle
        MOV       A, #BYTE3(?_56)    ;; 1 cycle
          CFI EndBlock cfiBlock34
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 38 cycles
        REQUIRE ?Subroutine7
        ; // Fall through to label ?Subroutine7
// 1823 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock35 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+14
          CFI FunCall _AppCmd_ProcessCmd_RPL_GLOBALREPAIRR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_PAN_VERSION_INCR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_SPDR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_LEAVENETWORKR _R_Modem_print
        CODE
?Subroutine7:
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock35
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 1824 
// 1825 /********************************************************************************
// 1826 * Function Name     : AppCmd_ProcessCmd_ROUTELST_GETR
// 1827 * Description       : Process "ROUTELST_GETR" command
// 1828 * Arguments         : pCmd ... Pointer to command
// 1829 *                   :   STARTR (Handle) [(Options)]
// 1830 * Return Value      : None
// 1831 * Output            : Confirm command
// 1832 *                   :   STARTC (Handle) (Status) (ChannelNumber) (PANId)
// 1833 *                   :          (IPv6Addr) (CoordIPv6Addr) (CoordMACAddr)
// 1834 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock36 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_ROUTELST_GETR
        CODE
// 1835 static void AppCmd_ProcessCmd_ROUTELST_GETR(uint8_t* pCmd)
// 1836 {
_AppCmd_ProcessCmd_ROUTELST_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 68
        SUBW      SP, #0x40          ;; 1 cycle
          CFI CFA SP+72
// 1837     /* Get command parameters */
// 1838     uint8_t handle;
// 1839     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x40          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFC4        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+78
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+76
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1840     uint16_t options;
// 1841     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+82
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+80
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1842 
// 1843     r_boolean_t route_Found = R_FALSE;
// 1844 #if R_BORDER_ROUTER_ENABLED
// 1845     if (AppCmdConfig.deviceType == R_BORDERROUTER)
// 1846     {
// 1847         r_app_routing_table_buffer_t cfm;
// 1848         do
// 1849         {
// 1850             res = R_NWK_GetRequestMultipart(R_NWK_sourceRoutes, &cfm, sizeof(cfm), route_Found);
// 1851             if ((res == R_RESULT_SUCCESS || res == R_RESULT_SUCCESS_ADDITIONAL_DATA) && cfm.routingTable.count)
// 1852             {
// 1853                 if (!route_Found)
// 1854                 {
// 1855                     route_Found = R_TRUE;
// 1856                     R_Modem_print("ROUTELST_GETC %02X %02X 03 ", handle, R_RESULT_SUCCESS);
// 1857                 }
// 1858 
// 1859                 for (size_t i = 0; i < cfm.routingTable.count; i++)
// 1860                 {
// 1861                     AppPrintIPv6Addr(cfm.routingTable.entries[i].destination.bytes);
// 1862                     R_Modem_print(" ");
// 1863                     AppPrintIPv6Addr(cfm.routingTable.entries[i].preferredParent.bytes);
// 1864                     R_Modem_print(" ");
// 1865                     AppPrintIPv6Addr(cfm.routingTable.entries[i].alternateParent.bytes);
// 1866                     R_Modem_print(" ");
// 1867                     R_Modem_print("%08lX ", (unsigned long)cfm.routingTable.entries[i].remainingLifetime);
// 1868                 }
// 1869             }
// 1870         }
// 1871         while (res == R_RESULT_SUCCESS_ADDITIONAL_DATA);  // Continue as long as more data is available
// 1872 
// 1873         if (route_Found)
// 1874         {
// 1875             R_Modem_print("\n");
// 1876         }
// 1877     }
// 1878     else  // (deviceType == R_ROUTERNODE)
// 1879 #endif /* R_BORDER_ROUTER_ENABLED */
// 1880     {
// 1881         /* get default route (uplink)*/
// 1882         uint8_t preferredParent[R_IPV6_ADDRESS_LENGTH];
// 1883         uint8_t alternateParent[R_IPV6_ADDRESS_LENGTH];
// 1884         res = R_NWK_GetRequest(R_NWK_preferredParentAddress, preferredParent, sizeof(preferredParent));
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+72
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xD4           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
// 1885         res |= R_NWK_GetRequest(R_NWK_alternateParentAddress, alternateParent, sizeof(alternateParent));
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x20          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xD5           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 1886         if (res == R_RESULT_SUCCESS)
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_68  ;; 4 cycles
        ; ------------------------------------- Block: 92 cycles
// 1887         {
// 1888             uint8_t ipv6Addr[R_IPV6_ADDRESS_LENGTH];
// 1889             res = R_NWK_GetRequest(R_NWK_nwkIpv6Address, ipv6Addr, sizeof(ipv6Addr));
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xD1           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
// 1890             if (res == R_RESULT_SUCCESS)
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_68  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
// 1891             {
// 1892                 route_Found = R_TRUE;
// 1893                 R_Modem_print("ROUTELST_GETC %02X %02X 01 ", handle, res);
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      DE, #LWRD(?_0+476)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1894                 AppPrintIPv6Addr(ipv6Addr);
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 1895                 R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1896                 AppPrintIPv6Addr(preferredParent);
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 1897                 R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1898                 AppPrintIPv6Addr(alternateParent);
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 1899                 R_Modem_print(" FFFFFFFF ");  // Lifetime placeholder (RN routes format must be consistent with BR routes)
        MOVW      DE, #LWRD(?_0+504)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1900                 R_Modem_print("\n");
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_69  ;; 3 cycles
          CFI CFA SP+72
        ; ------------------------------------- Block: 51 cycles
// 1901             }
// 1902         }
// 1903     }
// 1904 
// 1905     if (!route_Found)
// 1906     {
// 1907         R_Modem_print("ROUTELST_GETC %02X %02X 00\n", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_68:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      DE, #LWRD(?_0+516)  ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
??R_DHCPV6_VendorOptionRecvCallback_69:
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+72
// 1908     }
// 1909 }
        ADDW      SP, #0x44          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock36
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 178 cycles
// 1910 
// 1911 /********************************************************************************
// 1912 * Function Name     : AppCmd_ProcessCmd_RPLINFO_GETR
// 1913 * Description       : Process "RPLINFO_GETR" command
// 1914 * Arguments         : pCmd ... Pointer to command
// 1915 *                   :   RPLINFO_GETR (Handle) (Options)
// 1916 * Return Value      : None
// 1917 * Output            : Confirm command
// 1918 *                   :   RPLINFO_GETC (Handle) (Status) [(InstanceId) (DodagId)
// 1919 *                                    (DodagVersion) (DodagPreference) (MOP) (Rank) (DTSN)]
// 1920 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock37 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_RPLINFO_GETR
        CODE
// 1921 static void AppCmd_ProcessCmd_RPLINFO_GETR(uint8_t* pCmd)
// 1922 {
_AppCmd_ProcessCmd_RPLINFO_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 30
        SUBW      SP, #0x1A          ;; 1 cycle
          CFI CFA SP+34
// 1923     /* Get command parameters */
// 1924     uint8_t handle;
// 1925     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFEA        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+38
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 1926     uint16_t options;
// 1927     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+46
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+44
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 1928 
// 1929     /* Get RPL info from NWK */
// 1930     r_nwk_rpl_info_t rplInfo;
// 1931     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+34
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_70  ;; 4 cycles
        ; ------------------------------------- Block: 67 cycles
// 1932     {
// 1933         res = R_NWK_GetRequest(R_NWK_rplInfo, &rplInfo, sizeof(rplInfo));
        MOVW      BC, #0x15          ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xF7           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
// 1934     }
// 1935 
// 1936     /* Print confirm */
// 1937     R_Modem_print("RPLINFO_GETC %02X %02X", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_70:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      DE, #LWRD(?_0+544)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1938     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+34
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_71  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 1939     {
// 1940         R_Modem_print(" %02X ", rplInfo.instance_id);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      DE, #LWRD(?_0+568)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1941         AppPrintIPv6Addr(rplInfo.dodag_id.bytes);
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x7           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_72  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        CLRB      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_72:
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_RPLINFO_GETR_0:
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 1942         R_Modem_print(" %02X %02X %02X %02X %02X", rplInfo.dodag_version, rplInfo.dodag_preference, rplInfo.mop, rplInfo.rank, rplInfo.dtsn);
// 1943     }
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, [SP+0x1B]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, [SP+0x1C]       ;; 1 cycle
        SHR       A, 0x4             ;; 1 cycle
        AND       A, #0x7            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, [SP+0x1E]       ;; 1 cycle
        AND       A, #0x7            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x1F]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      DE, #LWRD(?_0+576)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+34
        ; ------------------------------------- Block: 32 cycles
// 1944     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_71:
        BR        F:?Subroutine3     ;; 3 cycles
          CFI EndBlock cfiBlock37
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 154 cycles
// 1945 }
// 1946 
// 1947 /********************************************************************************
// 1948 * Function Name     : AppCmd_ProcessCmd_NDCACHE_GETR
// 1949 * Description       : Process "NDCACHE_GETR" command
// 1950 * Arguments         : pCmd ... Pointer to command
// 1951 *                   :   NDCACHE_GETR (Handle) [(Options)]
// 1952 * Return Value      : None
// 1953 * Output            : Confirm command
// 1954 *                   :   NDCACHE_GETC (Handle) (Status) (NdCacheEntries)
// 1955 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock38 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_NDCACHE_GETR
        CODE
// 1956 static void AppCmd_ProcessCmd_NDCACHE_GETR(uint8_t* pCmd)
// 1957 {
_AppCmd_ProcessCmd_NDCACHE_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 216
        SUBW      SP, #0xD4          ;; 1 cycle
          CFI CFA SP+220
// 1958     r_result_t res;
// 1959 
// 1960     /* Get command parameters */
// 1961     uint8_t handle;
// 1962     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xD4          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFF32        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+222
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+224
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+226
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+228
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+226
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+224
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1963     uint16_t options;
// 1964     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+226
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+228
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+230
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+232
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+230
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+228
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 1965 
// 1966     r_boolean_t isContinuation = R_FALSE;
        CLRB      A                  ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x16          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+230
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+232
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+230
        POP       BC                 ;; 1 cycle
          CFI CFA SP+228
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+230
        MOV       [SP+0x16], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x14], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+228
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+220
        MOV       [SP+0x06], A       ;; 1 cycle
        ; ------------------------------------- Block: 74 cycles
// 1967     do
// 1968     {
// 1969         r_app_nd_cache_buffer_t cfm;
// 1970         res = R_NWK_GetRequestMultipart(R_NWK_ndCache, &cfm, sizeof(cfm), isContinuation);
??AppCmd_ProcessCmd_NDCACHE_GETR_0:
        MOV       A, [SP+0x06]       ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+222
        MOVW      BC, #0xC5          ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xD2           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequestMultipart
        CALL      F:_R_NWK_GetRequestMultipart  ;; 3 cycles
        MOV       [SP+0x03], A       ;; 1 cycle
// 1971         if (res == R_RESULT_SUCCESS || res == R_RESULT_SUCCESS_ADDITIONAL_DATA)
        POP       BC                 ;; 1 cycle
          CFI CFA SP+220
        CMP0      A                  ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_73  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        CMP       A, #0x6            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_74  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1972         {
// 1973             if (!isContinuation)
??R_DHCPV6_VendorOptionRecvCallback_73:
        MOV       A, C               ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_75  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1974             {
// 1975                 isContinuation = R_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
// 1976                 R_Modem_print("NDCACHE_GETC %02X %02X ", handle, R_RESULT_SUCCESS);  // Write begin of confirm message
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+222
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+224
        MOVW      DE, #LWRD(?_0+602)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+220
        ; ------------------------------------- Block: 14 cycles
// 1977             }
// 1978 
// 1979             for (uint16_t i = 0; i < cfm.nd_cache.count; i++)
??R_DHCPV6_VendorOptionRecvCallback_75:
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_76  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
// 1980             {
// 1981                 /* MACAddr */
// 1982                 AppPrintMACAddr(cfm.nd_cache.entries[i].llAddress.bytes);
??AppCmd_ProcessCmd_NDCACHE_GETR_1:
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xFFF8        ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_77  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        CLRB      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_77:
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_NDCACHE_GETR_2:
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 1983                 R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1984 
// 1985                 /* etx */
// 1986                 R_Modem_print("%04X ", cfm.nd_cache.entries[i].linkMetric);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL]            ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, [DE]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+222
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1987 
// 1988                 /* rsl */
// 1989                 R_Modem_print("%04X ", cfm.nd_cache.entries[i].neighToNodeRsl);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+224
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1990 
// 1991                 /* rssi */
// 1992                 R_Modem_print("%04X ", cfm.nd_cache.entries[i].rssi);
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+226
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1993 
// 1994                 /* panSize */
// 1995                 R_Modem_print("%04X ", cfm.nd_cache.entries[i].panSize);
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL+0x04]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [DE+0x05]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+228
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1996 
// 1997                 /* RPL rank */
// 1998                 R_Modem_print("%04X ", cfm.nd_cache.entries[i].rank);
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL+0x06]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [DE+0x07]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+230
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 1999 
// 2000                 /* routingCost */
// 2001                 R_Modem_print("%04X ", cfm.nd_cache.entries[i].routingCost);
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL+0x08]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [DE+0x09]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+232
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2002 
// 2003                 /* IPv6Addr */
// 2004                 AppPrintIPv6Addr(cfm.nd_cache.entries[i].ipAddress.bytes);
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_78  ;; 4 cycles
        ; ------------------------------------- Block: 111 cycles
        CLRB      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_78:
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_NDCACHE_GETR_3:
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 2005                 R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2006 
// 2007                 /* timeSinceLastRx */
// 2008                 R_Modem_print("%04X ", (unsigned)(cfm.nd_cache.entries[i].lastReceiveSecondsAgo * 1000));
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL+0x1A]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [DE+0x1B]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      BC, #0x3E8         ;; 1 cycle
        MULHU                        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+234
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2009 
// 2010                 /* isParentStatus */
// 2011                 R_Modem_print("%04X ", cfm.nd_cache.entries[i].status);
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [HL+0x1E]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+236
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2012             }
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, #0x27          ;; 1 cycle
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+220
        ; ------------------------------------- Block: 46 cycles
??R_DHCPV6_VendorOptionRecvCallback_76:
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x0F]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??AppCmd_ProcessCmd_NDCACHE_GETR_1  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 2013         }
// 2014         else
// 2015         {
// 2016             R_Modem_print("NDCACHE_GETC %02X %02X ", handle, res);  // Write confirm message with error code
// 2017             break;
// 2018         }
// 2019     }
// 2020     while (res == R_RESULT_SUCCESS_ADDITIONAL_DATA);  // Continue as long as more data is available
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_79  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BR        R:??AppCmd_ProcessCmd_NDCACHE_GETR_0  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_DHCPV6_VendorOptionRecvCallback_74:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+222
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+224
        MOVW      DE, #LWRD(?_0+602)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+220
        ; ------------------------------------- Block: 13 cycles
// 2021 
// 2022     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_79:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2023 }
        ADDW      SP, #0xD8          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock38
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 348 cycles
// 2024 
// 2025 /********************************************************************************
// 2026 * Function Name     : AppCmd_ProcessCmd_IPSR
// 2027 * Description       : Process "IPSR" command
// 2028 * Arguments         : pCmd ... Pointer to command
// 2029 *                   :   IPSR (Handle) (DataLength) (Data) [(Options)]
// 2030 * Return Value      : None
// 2031 * Output            : Confirm command
// 2032 *                   :   UDPSC (Handle) (Status)
// 2033 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock39 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_IPSR
        CODE
// 2034 static void AppCmd_ProcessCmd_IPSR(uint8_t* pCmd)
// 2035 {
_AppCmd_ProcessCmd_IPSR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 18
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+22
// 2036     /* Get command parameters */
// 2037     /* (Handle) */
// 2038     uint8_t handle;
// 2039     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFF8        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 2040 
// 2041     /* (PayloadSize) */
// 2042     uint16_t dataLength;
// 2043     res |= AppCmd_HexStrToNum(&pCmd, &dataLength, sizeof(dataLength), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 2044 
// 2045     /* (Payload) */
// 2046     uint8_t* data = NULL;
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+22
        MOV       [SP], A            ;; 1 cycle
        MOV       [SP+0x0C], #0x0    ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
// 2047     if (res == R_RESULT_SUCCESS)
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_80  ;; 4 cycles
        ; ------------------------------------- Block: 66 cycles
// 2048     {
// 2049         /* We assume that the IPv6 tunneling header is already prepended -> Max size is IPv6 MTU + IPv6 header size */
// 2050         if (dataLength <= R_WAN_IP_MTU + R_IPV6_HEADER_SIZE)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        CMPW      AX, #0x529         ;; 1 cycle
        BNC       ??R_DHCPV6_VendorOptionRecvCallback_81  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2051         {
// 2052             data = pCmd;
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
// 2053             if (dataLength != 0)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_80  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 2054             {
// 2055                 res = AppCmd_HexStrToNum(&pCmd, data, dataLength, R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+22
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_82  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 2056             }
// 2057         }
// 2058         else
// 2059         {
// 2060             res = R_RESULT_INVALID_PARAMETER;
??R_DHCPV6_VendorOptionRecvCallback_81:
        MOV       A, #0x11           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??R_DHCPV6_VendorOptionRecvCallback_82:
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2061         }
// 2062     }
// 2063 
// 2064     /* [(Options)] */
// 2065     uint16_t options = 0;
??R_DHCPV6_VendorOptionRecvCallback_80:
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2066     AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        INCW      AX                 ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 2067 
// 2068     /* Force to set blocking. */
// 2069     options &= (~R_OPTIONS_NON_BLOCKING);
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2070 
// 2071     /* Allow IP data request only in Join State 5 */
// 2072     if (p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0xB2]    ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+22
        CMP       A, #0x5            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_83  ;; 4 cycles
        ; ------------------------------------- Block: 43 cycles
// 2073     {
// 2074         if (res == R_RESULT_SUCCESS)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_84  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2075         {
// 2076             res = R_NWK_IP_DataRequest(data, dataLength, options);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_NWK_IP_DataRequest
        CALL      F:_R_NWK_IP_DataRequest  ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+22
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_84  ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
// 2077         }
// 2078     }
// 2079     else
// 2080     {
// 2081         res = R_RESULT_ILLEGAL_STATE;
??R_DHCPV6_VendorOptionRecvCallback_83:
        MOV       A, #0x9            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2082     }
// 2083 
// 2084     /* Confirm(IPSC) */
// 2085     R_Modem_print("IPSC %02X %02X\n", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_84:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      DE, #LWRD(?_57)    ;; 1 cycle
        MOV       A, #BYTE3(?_57)    ;; 1 cycle
          CFI EndBlock cfiBlock39
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 193 cycles
        REQUIRE ?Subroutine8
        ; // Fall through to label ?Subroutine8
// 2086 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock40 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+26
          CFI FunCall _AppCmd_ProcessCmd_IPSR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_REVOKESUPPR _R_Modem_print
        CODE
?Subroutine8:
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock40
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 2087 
// 2088 /********************************************************************************
// 2089 * Function Name     : AppCmd_ProcessCmd_UDPSR
// 2090 * Description       : Process "UDPSR" command
// 2091 * Arguments         : pCmd ... Pointer to command
// 2092 *                   :   UDPSR (Handle) (FrameType) (DstIPv6Addr) (DstPort) (SrcPort)
// 2093 *                   :         (PayloadSize) (Payload) [(Options)]
// 2094 * Return Value      : None
// 2095 * Output            : Confirm command
// 2096 *                   :   UDPSC (Handle) (Status)
// 2097 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock41 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_UDPSR
        CODE
// 2098 static void AppCmd_ProcessCmd_UDPSR(uint8_t* pCmd)
// 2099 {
_AppCmd_ProcessCmd_UDPSR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 64
        SUBW      SP, #0x3C          ;; 1 cycle
          CFI CFA SP+68
// 2100     r_result_t res;
// 2101     uint8_t handle;
// 2102     uint8_t dstAddress[R_IPV6_ADDRESS_LENGTH];
// 2103     uint8_t srcAddress[R_IPV6_ADDRESS_LENGTH];
// 2104     uint16_t dstPort, srcPort, payloadSize;
// 2105     uint8_t* p_payload;
// 2106     uint16_t options;
// 2107     uint8_t isEdfe;
// 2108 
// 2109     /* Get command parameters */
// 2110     /* (Handle) */
// 2111     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFCA        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 2112 
// 2113     /* (DstIPv6Addr) */
// 2114     res |= AppCmd_HexStrToNum(&pCmd, dstAddress, sizeof(dstAddress), R_TRUE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x30          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+68
        ADDW      AX, #0xFFEC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x1A], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, #0x10          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, HL             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x12], A       ;; 1 cycle
// 2115 
// 2116     /* (SrcIPv6Addr) */
// 2117     res |= AppCmd_HexStrToNum(&pCmd, srcAddress, sizeof(srcAddress), R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        MOVW      AX, #0x10          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+78
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+76
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x15], A       ;; 1 cycle
// 2118 
// 2119     /* (DstPort), (SrcPort) */
// 2120     res |= AppCmd_HexStrToNum(&pCmd, &dstPort, sizeof(dstPort), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+82
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+80
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x18], A       ;; 1 cycle
// 2121     res |= AppCmd_HexStrToNum(&pCmd, &srcPort, sizeof(srcPort), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x20          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+86
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+88
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+86
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+84
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x15], A       ;; 1 cycle
// 2122 
// 2123     /* (FrameExchangeMode) */
// 2124     res |= AppCmd_HexStrToNum(&pCmd, &isEdfe, sizeof(handle), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+86
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+88
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x15          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+90
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+92
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+90
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+88
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 2125 
// 2126     /* (PayloadSize), (Payload) */
// 2127     res |= AppCmd_HexStrToNum(&pCmd, &payloadSize, sizeof(payloadSize), R_FALSE);
        MOV       A, [SP+0x21]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x22]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x20]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x19]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+90
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+92
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+94
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+96
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+98
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+96
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+94
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+90
        POP       HL                 ;; 1 cycle
          CFI CFA SP+88
        MOV       A, H               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x14]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 2128     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x14          ;; 1 cycle
          CFI CFA SP+68
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_85  ;; 4 cycles
        ; ------------------------------------- Block: 192 cycles
// 2129     {
// 2130         if (payloadSize <= R_UDP_MAX_UDP_DATA_LENGTH)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        CMPW      AX, #0x5F9         ;; 1 cycle
        BNC       ??R_DHCPV6_VendorOptionRecvCallback_86  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2131         {
// 2132             p_payload = pCmd;
        MOV       A, [SP+0x3E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x3C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x16], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x14], AX      ;; 1 cycle
// 2133             if (payloadSize != 0)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_85  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 2134             {
// 2135                 res = AppCmd_HexStrToNum(&pCmd, p_payload, payloadSize, R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+68
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_87  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 2136             }
// 2137         }
// 2138         else
// 2139         {
// 2140             res = R_RESULT_INVALID_PARAMETER;
??R_DHCPV6_VendorOptionRecvCallback_86:
        MOV       A, #0x11           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??R_DHCPV6_VendorOptionRecvCallback_87:
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2141         }
// 2142     }
// 2143 
// 2144     /* [(Options)] */
// 2145     AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
??R_DHCPV6_VendorOptionRecvCallback_85:
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 2146 
// 2147     /* Force to set blocking. */
// 2148     options &= (~R_OPTIONS_NON_BLOCKING);
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
// 2149 
// 2150     if (isEdfe)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+68
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_88  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
// 2151     {
// 2152 #if R_EDFE_INITIATOR_DISABLED
// 2153         res = R_RESULT_UNSUPPORTED_FEATURE;
        MOV       A, #0x12           ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_89  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2154 #else
// 2155         options |= R_OPTIONS_FRAME_EXCHANGE_EDFE;
// 2156 #endif
// 2157     }
// 2158 
// 2159     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_88:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_90  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2160     {
// 2161         res = R_UDP_DataRequest(dstAddress, dstPort, srcPort,
// 2162                                 p_payload, payloadSize, options);
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_UDP_DataRequest
        CALL      F:_R_UDP_DataRequest  ;; 3 cycles
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+68
        ; ------------------------------------- Block: 23 cycles
??R_DHCPV6_VendorOptionRecvCallback_89:
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2163     }
// 2164 
// 2165     /* Confirm(UDPSC) */
// 2166     R_Modem_print("UDPSC %02X %02X\n", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_90:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      DE, #LWRD(?_58)    ;; 1 cycle
        MOV       A, #BYTE3(?_58)    ;; 1 cycle
          CFI EndBlock cfiBlock41
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 317 cycles
        REQUIRE ?Subroutine9
        ; // Fall through to label ?Subroutine9
// 2167 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock42 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+72
          CFI FunCall _AppCmd_ProcessCmd_UDPSR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_ICMPSR _R_Modem_print
        CODE
?Subroutine9:
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x44          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock42
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 2168 
// 2169 /********************************************************************************
// 2170 * Function Name     : AppCmd_ProcessCmd_ICMPSR
// 2171 * Description       : Process "ICMPSR" command
// 2172 * Arguments         : pCmd ... Pointer to command
// 2173 *                   :   ICMPSR (Handle) (DstIPv6Addr) (Type=0x80)
// 2174 *                   :       (Identifier) (Sequence) (PayloadSize) (Payload) [(Options)]
// 2175 * Return Value      : None
// 2176 * Output            : Confirm command
// 2177 *                   :   ICMPSC (Handle) (Status)
// 2178 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock43 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_ICMPSR
        CODE
// 2179 static void AppCmd_ProcessCmd_ICMPSR(uint8_t* pCmd)
// 2180 {
_AppCmd_ProcessCmd_ICMPSR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 64
        SUBW      SP, #0x3C          ;; 1 cycle
          CFI CFA SP+68
// 2181     r_result_t res;
// 2182     uint8_t handle, type;
// 2183     uint8_t dstAddress[R_IPV6_ADDRESS_LENGTH];
// 2184     uint8_t srcAddress[R_IPV6_ADDRESS_LENGTH];
// 2185     uint8_t hoplimit;
// 2186     uint16_t identifier, sequence, payloadSize;
// 2187     uint8_t* p_payload;
// 2188     uint16_t options;
// 2189     uint8_t isEdfe;
// 2190 
// 2191     /* Get command parameters */
// 2192     /* (Handle) */
// 2193     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFC6        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 2194 
// 2195     /* (DstIPv6Addr) */
// 2196     res |= AppCmd_HexStrToNum(&pCmd, dstAddress, sizeof(dstAddress), R_TRUE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x30          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+68
        ADDW      AX, #0xFFEC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x1A], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, #0x10          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, HL             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x13], A       ;; 1 cycle
// 2197 
// 2198     /* (SrcIPv6Addr) */
// 2199     res |= AppCmd_HexStrToNum(&pCmd, srcAddress, sizeof(srcAddress), R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        MOVW      AX, #0x10          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+78
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+76
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x16], A       ;; 1 cycle
// 2200 
// 2201     /* (HopLimit) */
// 2202     res |= AppCmd_HexStrToNum(&pCmd, &hoplimit, sizeof(hoplimit), R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+82
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+80
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x17], A       ;; 1 cycle
// 2203 
// 2204     /* (Type) */
// 2205     res |= AppCmd_HexStrToNum(&pCmd, &type, sizeof(type), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x19          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+86
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+88
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+86
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+84
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x1E]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x1F]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x1B]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
// 2206     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+68
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_91  ;; 4 cycles
        ; ------------------------------------- Block: 142 cycles
// 2207     {
// 2208         switch (type)
        MOV       A, [SP+0x09]       ;; 1 cycle
        CMP       A, #0x80           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_92  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2209         {
// 2210             /* Type = 0x80: Echo Request */
// 2211             case R_ICMP_TYPE_ECHO_REQUEST:
// 2212 
// 2213                 /* (Identifier) */
// 2214                 res = AppCmd_HexStrToNum(&pCmd, &identifier, sizeof(identifier), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x16          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 2215 
// 2216                 /* (Sequence) */
// 2217                 res |= AppCmd_HexStrToNum(&pCmd, &sequence, sizeof(sequence), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+78
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+76
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x16], A       ;; 1 cycle
// 2218 
// 2219                 /* (FrameExchangeMode) */
// 2220                 res |= AppCmd_HexStrToNum(&pCmd, &isEdfe, sizeof(handle), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xD           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+82
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+80
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x17], A       ;; 1 cycle
// 2221 
// 2222                 /* (PayloadSize), (Payload) */
// 2223                 res |= AppCmd_HexStrToNum(&pCmd, &payloadSize, sizeof(payloadSize), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x16          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+86
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+88
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+86
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+84
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x1B]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x1E]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 2224                 if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+68
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_93  ;; 4 cycles
        ; ------------------------------------- Block: 98 cycles
// 2225                 {
// 2226                     if (payloadSize <= R_ICMP_MAX_ECHO_DATA_LENGTH)
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, #0x5F9         ;; 1 cycle
        BNC       ??R_DHCPV6_VendorOptionRecvCallback_94  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2227                     {
// 2228                         p_payload = pCmd;
        MOV       A, [SP+0x3E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x3C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x16], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x14], AX      ;; 1 cycle
// 2229                         if (payloadSize != 0)
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_93  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 2230                         {
// 2231                             res = AppCmd_HexStrToNum(&pCmd, p_payload, payloadSize, R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+68
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_95  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 2232                         }
// 2233                     }
// 2234                     else
// 2235                     {
// 2236                         res = R_RESULT_INVALID_PARAMETER;
??R_DHCPV6_VendorOptionRecvCallback_94:
        MOV       A, #0x11           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??R_DHCPV6_VendorOptionRecvCallback_95:
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2237                     }
// 2238                 }
// 2239 
// 2240                 /* [(Options)] */
// 2241                 res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
??R_DHCPV6_VendorOptionRecvCallback_93:
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
// 2242 
// 2243                 /* Force to set blocking. */
// 2244                 options &= (~R_OPTIONS_NON_BLOCKING);
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
// 2245 
// 2246                 if (isEdfe)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+68
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_96  ;; 4 cycles
        ; ------------------------------------- Block: 36 cycles
// 2247                 {
// 2248 #if R_EDFE_INITIATOR_DISABLED
// 2249                     res = R_RESULT_UNSUPPORTED_FEATURE;
        MOV       C, #0x12           ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_91  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2250 #else
// 2251                     options |= R_OPTIONS_FRAME_EXCHANGE_EDFE;
// 2252 #endif
// 2253                 }
// 2254 
// 2255                 if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_96:
        CMP0      C                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_91  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2256                 {
// 2257                     res = R_ICMP_EchoRequest(dstAddress,
// 2258                                              identifier,
// 2259                                              sequence,
// 2260                                              p_payload,
// 2261                                              payloadSize,
// 2262                                              options,
// 2263                                              hoplimit);
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       D, A               ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       H, A               ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_ICMP_EchoRequest
        CALL      F:_R_ICMP_EchoRequest  ;; 3 cycles
        MOV       C, A               ;; 1 cycle
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+68
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_91  ;; 3 cycles
        ; ------------------------------------- Block: 33 cycles
// 2264                 }
// 2265                 break;
// 2266 
// 2267             default:
// 2268                 res = R_RESULT_INVALID_PARAMETER;
??R_DHCPV6_VendorOptionRecvCallback_92:
        MOV       C, #0x11           ;; 1 cycle
// 2269                 break;
        ; ------------------------------------- Block: 1 cycles
// 2270         }
// 2271     }
// 2272 
// 2273     /* Confirm(ICMPSC) */
// 2274     R_Modem_print("ICMPSC %02X %02X\n", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_91:
        CLRB      B                  ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+70
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      DE, #LWRD(?_59)    ;; 1 cycle
        MOV       A, #BYTE3(?_59)    ;; 1 cycle
        BR        F:?Subroutine9     ;; 3 cycles
          CFI EndBlock cfiBlock43
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 385 cycles
// 2275 }
// 2276 
// 2277 #if R_PHY_TYPE_CWX_M && R_MDR_ENABLED
// 2278 /********************************************************************************
// 2279 * Function Name     : AppCmd_ProcessCmd_MDRSR
// 2280 * Description       : Process "MDRSR" command
// 2281 * Arguments         : pCmd ... Pointer to command
// 2282 *                   :   MDRSR (Handle) (Modulation)
// 2283 * Return Value      : None
// 2284 * Output            : Confirm command
// 2285 *                   :   MDRSR (Handle) (Status)
// 2286 ********************************************************************************/
// 2287 static void AppCmd_ProcessCmd_MDRSR(uint8_t* pCmd)
// 2288 {
// 2289     r_result_t res;
// 2290     uint8_t handle;
// 2291     uint8_t phyMDRNewModeSwitchBank;
// 2292 
// 2293     /* Get command parameters */
// 2294     /* (Handle) */
// 2295     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
// 2296 
// 2297     /* (Modulation) */
// 2298     res |= AppCmd_HexStrToNum(&pCmd, &phyMDRNewModeSwitchBank, sizeof(phyMDRNewModeSwitchBank), R_FALSE);
// 2299 
// 2300     // Set new bank
// 2301     if (res == R_RESULT_SUCCESS)
// 2302     {
// 2303         res |= R_NWK_SetRequest(R_NWK_phyMDRNewModeSwitchBank, &phyMDRNewModeSwitchBank, sizeof(phyMDRNewModeSwitchBank));
// 2304     }
// 2305 
// 2306     /* Confirm(MDRSC) */
// 2307     R_Modem_print("MDRSC %02X %02X\n", handle, res);
// 2308 }
// 2309 #endif /* R_PHY_TYPE_CWX_M && R_MDR_ENABLED */
// 2310 
// 2311 /********************************************************************************
// 2312 * Function Name     : AppCmd_ProcessCmd_SPDR
// 2313 * Description       : Process "SPDR" command
// 2314 * Arguments         : pCmd ... Pointer to command
// 2315 *                   :   SPDR (Handle)
// 2316 * Return Value      : None
// 2317 * Output            : Confirm command
// 2318 *                   :   SPDC (Handle) (Status)
// 2319 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock44 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_SPDR
        CODE
// 2320 static void AppCmd_ProcessCmd_SPDR(uint8_t* pCmd)
// 2321 {
_AppCmd_ProcessCmd_SPDR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 2322     r_result_t res;
// 2323     uint8_t handle;
// 2324 
// 2325     /* Get command parameters */
// 2326     /* (Handle) */
// 2327     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 2328 
// 2329     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 1 cycle
          CFI FunCall _R_NWK_Suspend
        ; ------------------------------------- Block: 28 cycles
// 2330     {
// 2331         res = R_NWK_Suspend();
        CALL      F:_R_NWK_Suspend   ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2332     }
// 2333 
// 2334     /* Confirm(SPDC) */
// 2335     R_Modem_print("SPDC %02X %02X\n", handle, res);
??AppCmd_ProcessCmd_SPDR_0:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(?_60)    ;; 1 cycle
        MOV       A, #BYTE3(?_60)    ;; 1 cycle
        BR        F:?Subroutine7     ;; 3 cycles
          CFI EndBlock cfiBlock44
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 43 cycles
// 2336 }
// 2337 
// 2338 /********************************************************************************
// 2339 * Function Name     : AppCmd_ProcessCmd_RSMR
// 2340 * Description       : Process "RSMR" command
// 2341 * Arguments         : pCmd ... Pointer to command
// 2342 *                   :   RSMR (Handle) (suspendedTime)
// 2343 * Return Value      : None
// 2344 * Output            : Confirm command
// 2345 *                   :   RSMC (Handle) (Status)
// 2346 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock45 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_RSMR
        CODE
// 2347 static void AppCmd_ProcessCmd_RSMR(uint8_t* pCmd)
// 2348 {
_AppCmd_ProcessCmd_RSMR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 14
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+18
// 2349     r_result_t res;
// 2350     uint8_t handle;
// 2351     uint32_t suspendedTimeMs;
// 2352 
// 2353     /* Get command parameters */
// 2354     /* (Handle) */
// 2355     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFF8        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+24
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 2356 
// 2357     /* (suspendedTime) */
// 2358     res |= AppCmd_HexStrToNum(&pCmd, &suspendedTimeMs, sizeof(suspendedTimeMs), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 2359 
// 2360     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+18
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_97  ;; 4 cycles
        ; ------------------------------------- Block: 64 cycles
// 2361     {
// 2362         res = R_NWK_Resume(suspendedTimeMs);
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
          CFI FunCall _R_NWK_Resume
        CALL      F:_R_NWK_Resume    ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2363     }
// 2364 
// 2365     /* Confirm(RSMC) */
// 2366     R_Modem_print("RSMC %02X %02X\n", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_97:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      DE, #LWRD(?_61)    ;; 1 cycle
        MOV       A, #BYTE3(?_61)    ;; 1 cycle
        BR        F:??Subroutine15_0  ;; 3 cycles
          CFI EndBlock cfiBlock45
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 82 cycles
// 2367 }
// 2368 
// 2369 /********************************************************************************
// 2370 * Function Name     : AppCmd_ProcessCmd_REVOKEKEYSR
// 2371 * Description       : Process "REVOKEKEYSR" command
// 2372 * Arguments         : pCmd ... Pointer to command
// 2373 *                   :   REVOKEKEYSR (Handle) (Options) (GTK)
// 2374 * Return Value      : None
// 2375 * Output            : Confirm command
// 2376 *                   :   REVOKEKEYSC (Handle) (Status)
// 2377 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock46 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_REVOKEKEYSR
        CODE
// 2378 static void AppCmd_ProcessCmd_REVOKEKEYSR(uint8_t* pCmd)
// 2379 {
_AppCmd_ProcessCmd_REVOKEKEYSR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 30
        SUBW      SP, #0x1A          ;; 1 cycle
          CFI CFA SP+34
// 2380     r_result_t res;
// 2381     uint8_t handle;
// 2382     uint16_t options;
// 2383     uint8_t newGtk[16];
// 2384 
// 2385     /* Get command parameters */
// 2386     /* (Handle) */
// 2387     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFEA        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+38
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
// 2388 
// 2389     /* (Options) */
// 2390     res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+44
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x09], A       ;; 1 cycle
// 2391 
// 2392     /* (GTK) */
// 2393     res |= AppCmd_HexStrToNum(&pCmd, newGtk, sizeof(newGtk), R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, #0x10          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x16          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+48
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+46
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x0D]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 2394     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+34
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 85 cycles
// 2395     {
// 2396 #if R_BR_AUTHENTICATOR_ENABLED
// 2397         if (AppCmdConfig.deviceType == R_BORDERROUTER && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
// 2398         {
// 2399             res = R_AUTH_BR_RevokeGTKs(p_aplGlobal, newGtk);
// 2400             if (res == R_RESULT_SUCCESS)
// 2401             {
// 2402                 res = R_NWK_IncreasePanVersionRequest();  // Increment PAN Version to finish revocation
// 2403             }
// 2404         }
// 2405 #else
// 2406         res = R_RESULT_UNSUPPORTED_FEATURE;  // Key revocation can only be performed on border routers
        MOV       A, #0x12           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2407 #endif
// 2408     }
// 2409 
// 2410     R_Modem_print("REVOKEKEYSC %02X %02X\n", handle, res);
??AppCmd_ProcessCmd_REVOKEKEYSR_0:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      DE, #LWRD(?_62)    ;; 1 cycle
        MOV       A, #BYTE3(?_62)    ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2411 }
        ADDW      SP, #0x22          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock46
        ; ------------------------------------- Block: 19 cycles
        ; ------------------------------------- Total: 105 cycles
// 2412 
// 2413 /********************************************************************************
// 2414 * Function Name     : AppCmd_ProcessCmd_REVOKESUPPR
// 2415 * Description       : Process "REVOKESUPPR" command
// 2416 * Arguments         : pCmd ... Pointer to command
// 2417 *                   :   REVOKESUPPR (Handle) (MAC)
// 2418 * Return Value      : None
// 2419 * Output            : Confirm command
// 2420 *                   :   REVOKESUPPC (Handle) (Status)
// 2421 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock47 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_REVOKESUPPR
        CODE
// 2422 static void AppCmd_ProcessCmd_REVOKESUPPR(uint8_t* pCmd)
// 2423 {
_AppCmd_ProcessCmd_REVOKESUPPR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 18
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+22
// 2424     r_result_t res;
// 2425     uint8_t handle;
// 2426     uint8_t supplicant[R_MAC_EXTENDED_ADDRESS_LENGTH];
// 2427 
// 2428     /* Get command parameters */
// 2429     /* (Handle) */
// 2430     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFF4        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 2431 
// 2432     /* (Supplicant) */
// 2433     res |= AppCmd_HexStrToNum(&pCmd, supplicant, sizeof(supplicant), R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 2434     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+22
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 61 cycles
// 2435     {
// 2436 #if R_BR_AUTHENTICATOR_ENABLED
// 2437         if (AppCmdConfig.deviceType == R_BORDERROUTER && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
// 2438         {
// 2439             res = R_AUTH_BR_RevokeSupplicant(p_aplGlobal, supplicant);
// 2440             if (res == R_RESULT_SUCCESS)
// 2441             {
// 2442                 res = R_NWK_IncreasePanVersionRequest();  // Increment PAN Version to finish revocation
// 2443             }
// 2444         }
// 2445 #else
// 2446         res = R_RESULT_UNSUPPORTED_FEATURE;  // Supplicant revocation can only be performed on border routers
        MOV       A, #0x12           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2447 #endif
// 2448     }
// 2449 
// 2450     R_Modem_print("REVOKESUPPC %02X %02X\n", handle, res);
??AppCmd_ProcessCmd_REVOKESUPPR_0:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      DE, #LWRD(?_63)    ;; 1 cycle
        MOV       A, #BYTE3(?_63)    ;; 1 cycle
        BR        F:?Subroutine8     ;; 3 cycles
          CFI EndBlock cfiBlock47
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 74 cycles
// 2451 }
// 2452 
// 2453 /********************************************************************************
// 2454 * Function Name     : AppCmd_ProcessCmd_DEVICEKICKR
// 2455 * Description       : Process "DEVICEKICKR" command
// 2456 * Arguments         : pCmd ... Pointer to command
// 2457 *                   :   DEVICEKICKR (Handle) (DstIPv6Addr)
// 2458 * Return Value      : None
// 2459 * Output            : Confirm command
// 2460 *                   :   DEVICEKICKC (Handle) (Status)
// 2461 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock48 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_DEVICEKICKR
        CODE
// 2462 static void AppCmd_ProcessCmd_DEVICEKICKR(uint8_t* pCmd)
// 2463 {
_AppCmd_ProcessCmd_DEVICEKICKR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 26
        SUBW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+30
// 2464     /* Get command parameters */
// 2465     uint8_t handle;
// 2466     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x16          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFEC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+36
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 2467 
// 2468     uint8_t dstIpAddr[R_IPV6_ADDRESS_LENGTH];
// 2469     res |= AppCmd_HexStrToNum(&pCmd, dstIpAddr, sizeof(dstIpAddr), R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, #0x10          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+38
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 2470 
// 2471 #if R_BORDER_ROUTER_ENABLED
// 2472     if (AppCmdConfig.deviceType == R_BORDERROUTER)
// 2473     {
// 2474         uint8_t srcAddress[R_IPV6_ADDRESS_LENGTH];
// 2475         res |= R_NWK_GetRequest(R_NWK_nwkIpv6Address, srcAddress, sizeof(srcAddress));
// 2476 
// 2477         uint16_t dstPort = KICK_DEVICE_MSG_UDP_PORT;
// 2478         uint16_t srcPort = KICK_DEVICE_MSG_UDP_PORT;
// 2479 
// 2480         uint8_t payload[] = KICK_DEVICE_MSG_PAYLOAD;
// 2481         uint16_t payloadSize = sizeof(payload);
// 2482 
// 2483         uint16_t options = 0;
// 2484         if (res == R_RESULT_SUCCESS)
// 2485         {
// 2486             res = R_UDP_DataRequest(dstIpAddr, dstPort, srcPort, payload, payloadSize, options);
// 2487         }
// 2488     }
// 2489     else
// 2490 #endif /* R_BORDER_ROUTER_ENABLED */
// 2491     {
// 2492         res = R_RESULT_UNSUPPORTED_FEATURE;  // Device kick message may only be sent by border routers
// 2493     }
// 2494     R_Modem_print("DEVICEKICKC %02X %02X\n", handle, res);
        MOVW      AX, #0x12          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      DE, #LWRD(?_64)    ;; 1 cycle
        MOV       A, #BYTE3(?_64)    ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2495 }
        ADDW      SP, #0x26          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock48
        ; ------------------------------------- Block: 73 cycles
        ; ------------------------------------- Total: 73 cycles
// 2496 
// 2497 /********************************************************************************
// 2498 * Function Name     : AppCmd_ProcessCmd_LEAVENETWORKR
// 2499 * Description       : Process "LEAVENETWORKR" command
// 2500 * Arguments         : pCmd ... Pointer to command
// 2501 *                   :   AppCmd_ProcessCmd_LEAVENETWORKR (Handle)
// 2502 * Return Value      : None
// 2503 * Output            : Confirm command
// 2504 *                   :   LEAVENETWORKC (Handle) (Status)
// 2505 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock49 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_LEAVENETWORKR
        CODE
// 2506 static void AppCmd_ProcessCmd_LEAVENETWORKR(uint8_t* pCmd)
// 2507 {
_AppCmd_ProcessCmd_LEAVENETWORKR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 2508     /* Get command parameters */
// 2509     uint8_t handle;
// 2510     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 2511 
// 2512     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        CMP0      X                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_98  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
// 2513     {
// 2514         res = R_RESULT_ILLEGAL_STATE;
        MOV       X, #0x9            ;; 1 cycle
// 2515         if (AppCmdConfig.deviceType == R_ROUTERNODE && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        CMP       ES:_AppCmdConfig+1, #0x1  ;; 2 cycles
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_98  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      HL, #LWRD(_AppCmdConfig+58)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES:[HL+0xB2]    ;; 2 cycles
        CMP       A, #0x5            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_98  ;; 4 cycles
          CFI FunCall _AppCmd_LeaveNetwork
        ; ------------------------------------- Block: 17 cycles
// 2516         {
// 2517             res = AppCmd_LeaveNetwork();
        CALL      F:_AppCmd_LeaveNetwork  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
// 2518         }
// 2519     }
// 2520 
// 2521     R_Modem_print("LEAVENETWORKC %02X %02X\n", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_98:
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(?_65)    ;; 1 cycle
        MOV       A, #BYTE3(?_65)    ;; 1 cycle
        BR        F:?Subroutine7     ;; 3 cycles
          CFI EndBlock cfiBlock49
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 72 cycles
// 2522 }
// 2523 
// 2524 /********************************************************************************
// 2525 * Function Name     : AppCmd_ProcessCmd_IPV6PREFIX_SETR
// 2526 * Description       : Process "IPV6PREFIX_SETR" command
// 2527 * Arguments         : pCmd ... Pointer to command
// 2528 *                   :   IPV6PREFIX_SETR (Handle) (Options) (IPv6 Prefix)
// 2529 * Return Value      : None
// 2530 * Output            : Confirm command
// 2531 *                   :   IPV6PREFIX_SETC (Handle) (Status)
// 2532 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock50 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_IPV6PREFIX_SETR
        CODE
// 2533 static void AppCmd_ProcessCmd_IPV6PREFIX_SETR(uint8_t* pCmd)
// 2534 {
_AppCmd_ProcessCmd_IPV6PREFIX_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 18
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+22
// 2535     uint8_t handle;
// 2536     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFF4        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 2537 
// 2538     uint8_t prefix[R_IPV6_PREFIX_LEN];
// 2539     res |= AppCmd_HexStrToNum(&pCmd, &prefix, sizeof(prefix), R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
// 2540 
// 2541 #if R_BORDER_ROUTER_ENABLED
// 2542     if (res == R_RESULT_SUCCESS)
// 2543     {
// 2544         if (res == R_RESULT_SUCCESS && p_aplGlobal->nwk.joinState == R_NWK_JoinState0_Reset)
// 2545         {
// 2546             R_memcpy(AppCmdConfig.globalIpAddress.bytes, prefix, sizeof(prefix));
// 2547         }
// 2548         else
// 2549         {
// 2550             res = R_RESULT_ILLEGAL_STATE;  // IPv6 prefix/address may only be set before starting the device
// 2551         }
// 2552     }
// 2553 #else  /* R_BORDER_ROUTER_ENABLED */
// 2554     res = R_RESULT_UNSUPPORTED_FEATURE;  // IPv6 prefix/address may only be set on border routers
// 2555 #endif  /* R_BORDER_ROUTER_ENABLED */
// 2556 
// 2557     R_Modem_print("IPV6PREFIX_SETC %02X %02X\n", handle, res);
        MOVW      AX, #0x12          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      DE, #LWRD(?_66)    ;; 1 cycle
        MOV       A, #BYTE3(?_66)    ;; 1 cycle
        BR        F:??Subroutine13_0  ;; 3 cycles
          CFI EndBlock cfiBlock50
        ; ------------------------------------- Block: 66 cycles
        ; ------------------------------------- Total: 66 cycles
// 2558 }
// 2559 
// 2560 /********************************************************************************
// 2561 * Function Name     : AppCmd_ProcessCmd_DEVCONF_SETR
// 2562 * Description       : Process "SETR" command - ParamType = ConfigParams
// 2563 * Arguments         : pCmd ... Pointer to command parameters
// 2564 *                   :   ConfigParams: (DeviceType) (PANId) (NetworkName) (MTUsize)
// 2565 * Return Value      : None
// 2566 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock51 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_DEVCONF_SETR
        CODE
// 2567 static void AppCmd_ProcessCmd_DEVCONF_SETR(uint8_t* pCmd)
// 2568 {
_AppCmd_ProcessCmd_DEVCONF_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 74
        SUBW      SP, #0x46          ;; 1 cycle
          CFI CFA SP+78
// 2569     r_result_t res;
// 2570     uint8_t handle;
// 2571     uint16_t option;
// 2572 
// 2573     uint8_t deviceType;
// 2574     uint16_t panId;
// 2575     uint16_t panSize;
// 2576     uint8_t useParent_BS_IE;
// 2577     uint8_t networkName[R_NETWORK_NAME_STRING_MAX_BYTES];
// 2578 
// 2579     uint8_t demo_mode;
// 2580     uint16_t sixLowPanMTU;
// 2581 
// 2582     /* Get command parameters */
// 2583     /* (Handle) */
// 2584     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFBE        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+86
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+84
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+82
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x11], A       ;; 1 cycle
// 2585 
// 2586     /* (option) */
// 2587     res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+86
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+88
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+90
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+88
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+86
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x14], A       ;; 1 cycle
// 2588 
// 2589     r_app_config_t* p_config = &AppCmdConfig;
// 2590 
// 2591     /* Get command parameters - ConfigParams */
// 2592     /* DeviceType */
// 2593     res |= AppCmd_HexStrToNum(&pCmd, &deviceType, sizeof(deviceType), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x9           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+78
        ADDW      AX, #0x1F          ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x22], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x20], AX      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        MOVW      AX, HL             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+86
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+84
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+82
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x0F], A       ;; 1 cycle
// 2594 
// 2595     /* PANId */
// 2596     res |= AppCmd_HexStrToNum(&pCmd, &panId, sizeof(panId), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+86
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+88
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+90
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+88
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+86
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x12], A       ;; 1 cycle
// 2597 
// 2598     /* PANSize */
// 2599     res |= AppCmd_HexStrToNum(&pCmd, &panSize, sizeof(panSize), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+88
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+90
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+92
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+94
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+92
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+90
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x15], A       ;; 1 cycle
// 2600 
// 2601     /* UseParent_BS_IE */
// 2602     res |= AppCmd_HexStrToNum(&pCmd, &useParent_BS_IE, sizeof(useParent_BS_IE), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+92
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+94
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+96
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+98
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+96
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+94
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x18], A       ;; 1 cycle
// 2603 
// 2604     /* NetworkName */
// 2605     res |= AppCmd_HexStrToNum(&pCmd, &networkName, sizeof(networkName), R_TRUE);
        ONEB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+96
        MOVW      AX, #0x21          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+98
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+100
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+102
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+100
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+98
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x17], A       ;; 1 cycle
// 2606 
// 2607     /* SixLowPanMTU */
// 2608     res |= AppCmd_HexStrToNum(&pCmd, &sixLowPanMTU, sizeof(sixLowPanMTU), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x14          ;; 1 cycle
          CFI CFA SP+78
        ADDW      AX, #0x8           ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x1E], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x1C], AX      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        MOVW      AX, HL             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+86
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+84
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+82
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 2609 
// 2610     /* DemoMode */
// 2611     res |= AppCmd_HexStrToNum(&pCmd, &demo_mode, sizeof(demo_mode), R_FALSE);
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+84
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+86
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+84
        POP       BC                 ;; 1 cycle
          CFI CFA SP+82
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+84
        MOV       [SP+0x20], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x1E], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+82
        MOV       A, [SP+0x0F]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x0D]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x07]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+86
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+88
        MOVW      AX, DE             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+90
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+92
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+90
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+88
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+84
        POP       HL                 ;; 1 cycle
          CFI CFA SP+82
        MOV       A, H               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x11]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 2612 
// 2613     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+78
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_99  ;; 4 cycles
        ; ------------------------------------- Block: 259 cycles
// 2614     {
// 2615         /* set PANID, PAN size and useParent_BS_IE */
// 2616         p_config->panId = panId;
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      ES:_AppCmdConfig+36, AX  ;; 2 cycles
// 2617         p_config->panSize = panSize;
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      ES:_AppCmdConfig+38, AX  ;; 2 cycles
// 2618         p_config->useParent_BS_IE = useParent_BS_IE;
        MOV       A, [SP]            ;; 1 cycle
        MOV       ES:_AppCmdConfig+40, A  ;; 2 cycles
// 2619 
// 2620         /* set global Network Name used for Network Name Information Element (NETNAME-IE) */
// 2621         memset(p_config->networkName, 0x00, sizeof(p_config->networkName));
        MOVW      AX, #0x21          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdConfig+2)  ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdConfig)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
// 2622         memcpy(p_config->networkName, networkName, sizeof(p_config->networkName));
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x26          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOVW      HL, #LWRD(_AppCmdConfig+2)  ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      BC, #0x21          ;; 1 cycle
          CFI FunCall ?MEMCPY_FAR
        CALL      N:?MEMCPY_FAR      ;; 3 cycles
// 2623         p_config->networkName[sizeof(p_config->networkName) - 1] = 0;  // Ensure null-termination
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        CLRB      ES:_AppCmdConfig+34  ;; 2 cycles
// 2624 
// 2625         /* Set operation mode (demo mode)*/
// 2626         res |= R_NWK_SetRequest(R_NWK_rplDemoMode, &demo_mode, sizeof(demo_mode));
        ONEW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xE1           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+78
        ; ------------------------------------- Block: 40 cycles
// 2627     }
// 2628     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_99:
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_100  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2629     {
// 2630         res |= R_NWK_SetRequest(R_NWK_sixLowpanMtu, &sixLowPanMTU, sizeof(sixLowPanMTU));
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xE0           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 2631     }
// 2632     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_100:
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_101  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2633     {
// 2634         p_config->deviceType = deviceType;
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOV       ES:_AppCmdConfig+1, A  ;; 2 cycles
// 2635         res |= R_NWK_SetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
        ONEW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xE3           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
// 2636     }
// 2637 
// 2638     /* Send Confirm -> DEVCONF_SETC */
// 2639     R_Modem_print("DEVCONF_SETC %02X %02X\n", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_101:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        MOVW      DE, #LWRD(?_67)    ;; 1 cycle
        MOV       A, #BYTE3(?_67)    ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2640 }
        ADDW      SP, #0x4E          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock51
        ; ------------------------------------- Block: 19 cycles
        ; ------------------------------------- Total: 348 cycles
// 2641 
// 2642 /********************************************************************************
// 2643 * Function Name     : AppCmd_ProcessCmd_DEVCONF_GETR
// 2644 * Description       : Process "GETR" command (Output common message)
// 2645 * Arguments         : r_result_t ... result when get command
// 2646 *                   : handle ... index
// 2647 *                   : paramType ... parameter type
// 2648 * Return Value      : None
// 2649 *                   : paramType ... parameter type (=PhyConfig)
// 2650 * Return Value      : None
// 2651 * Output            : Confirm command
// 2652 *                   :   ConfigParams: (DeviceType) (PANId) (NetworkName) (MTUsize)
// 2653 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock52 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_DEVCONF_GETR
        CODE
// 2654 static void AppCmd_ProcessCmd_DEVCONF_GETR(uint8_t* pCmd)
// 2655 {
_AppCmd_ProcessCmd_DEVCONF_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 44
        SUBW      SP, #0x28          ;; 1 cycle
          CFI CFA SP+48
// 2656 
// 2657     uint8_t deviceType;
// 2658     uint16_t panId;
// 2659 
// 2660     uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH];
// 2661     uint8_t ipv6Addr[R_IPV6_ADDRESS_LENGTH];
// 2662 
// 2663     uint8_t demo_mode;
// 2664     uint16_t sixLowPanMTU;
// 2665 
// 2666     /* Get command parameters */
// 2667     uint8_t handle;
// 2668     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFE2        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+54
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+52
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 2669 
// 2670     uint16_t option;
// 2671     res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x16          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+58
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+56
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 2672 
// 2673     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+48
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_102  ;; 4 cycles
        ; ------------------------------------- Block: 63 cycles
// 2674     {
// 2675         /* Obtain values from IPv6 stack */
// 2676         /* (DeviceType) */
// 2677         res |= R_NWK_GetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xE3           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 2678 
// 2679         /* (PANId) */
// 2680         res |= R_NWK_GetRequest(R_NWK_macPANId, &panId, sizeof(panId));
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x50           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2681 
// 2682         /* (MACAddr) */
// 2683         res |= R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x80           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 2684 
// 2685         /* (IPv6Addr) */
// 2686         res |= R_NWK_GetRequest(R_NWK_nwkIpv6Address, ipv6Addr, sizeof(ipv6Addr));
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xD1           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 2687 
// 2688         /* (DemoMode) */
// 2689         res |= R_NWK_GetRequest(R_NWK_rplDemoMode, &demo_mode, sizeof(demo_mode));
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xE1           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 62 cycles
// 2690     }
// 2691 
// 2692     /* Get sixLowpanMtu */
// 2693     res |= R_NWK_GetRequest(R_NWK_sixLowpanMtu, &sixLowPanMTU, sizeof(sixLowPanMTU));
??R_DHCPV6_VendorOptionRecvCallback_102:
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xE0           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2694 
// 2695 
// 2696     /* Generate confirm DEVCONF_GETC*/
// 2697     R_Modem_print("DEVCONF_GETC %02X %02X ", handle, res);
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      DE, #LWRD(?_0+626)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2698     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+48
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_103  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
// 2699     {
// 2700         /* DeviceType */
// 2701         R_Modem_print("%02X ", deviceType);
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2702 
// 2703         /* PANId */
// 2704         if (deviceType == R_ROUTERNODE)
        MOV       A, [SP+0x03]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+48
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_104  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 2705         {
// 2706             R_Modem_print("%04X ", panId);
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_105  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2707         }
// 2708         else
// 2709         {
// 2710             R_Modem_print("%04X ", AppCmdConfig.panId);
??R_DHCPV6_VendorOptionRecvCallback_104:
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      AX, ES:_AppCmdConfig+36  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??R_DHCPV6_VendorOptionRecvCallback_105:
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+48
// 2711         }
// 2712 
// 2713         /* PANSize */
// 2714         R_Modem_print("%04X ", AppCmdConfig.panSize);
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOVW      AX, ES:_AppCmdConfig+38  ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2715 
// 2716         /* UseParent_BS_IE */
// 2717         R_Modem_print("%02X ", AppCmdConfig.useParent_BS_IE);
        MOV       ES, #BYTE3(_AppCmdConfig)  ;; 1 cycle
        MOV       X, ES:_AppCmdConfig+40  ;; 2 cycles
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2718 
// 2719         /* NetworkName */
// 2720         AppPrintNetworkName(&(AppCmdConfig.networkName[0]));
        MOVW      DE, #LWRD(_AppCmdConfig+2)  ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdConfig)  ;; 1 cycle
          CFI FunCall _AppPrintNetworkName
        CALL      F:_AppPrintNetworkName  ;; 3 cycles
// 2721         R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2722 
// 2723         /* SixLowPanMTU */
// 2724         R_Modem_print("%04X ", sixLowPanMTU);
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2725 
// 2726         /* DemoMode */
// 2727         R_Modem_print("%02X ", demo_mode);
        MOV       A, [SP+0x09]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2728 
// 2729         /* MACAddr */
// 2730         R_Swap64((uint8_t*)(&macAddr[0]), (uint8_t*)(&macAddr[0]));
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+48
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+52
        MOV       A, #0xF            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+54
        POP       DE                 ;; 1 cycle
          CFI CFA SP+52
        POP       BC                 ;; 1 cycle
          CFI CFA SP+50
        POP       HL                 ;; 1 cycle
          CFI CFA SP+48
          CFI FunCall _R_Swap64
        CALL      F:_R_Swap64        ;; 3 cycles
// 2731         AppPrintMACAddr(macAddr);
        MOVW      BC, #0x8           ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 2732         R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2733 
// 2734         /* IPv6Addr */
// 2735         AppPrintIPv6Addr(ipv6Addr);
        MOVW      BC, #0x10          ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 2736     }
        ; ------------------------------------- Block: 88 cycles
// 2737     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_103:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 2738 }
        ADDW      SP, #0x2C          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock52
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 280 cycles
// 2739 
// 2740 /********************************************************************************
// 2741 * Function Name     : AppCmd_ProcessCmd_PHYFAN_SETR
// 2742 * Description       : Process "SETR" command - ParamType = ConfigParams
// 2743 * Arguments         : pCmd ... Pointer to command parameters
// 2744 *                   :   PhyConfig: (phy_band) (phy_operation_mode)
// 2745 *                   :                (txPower) (phy_RegulatoryMode)
// 2746 * Return Value      : None
// 2747 ********************************************************************************/
// 2748 #if R_PHY_TYPE_CWX_M
// 2749 static void AppCmd_ProcessCmd_PHYFAN_SETR(uint8_t* pCmd)
// 2750 {
// 2751     r_result_t res;
// 2752     uint8_t handle;
// 2753     uint16_t option;
// 2754 
// 2755     uint8_t wisun_regulatory_domain;
// 2756     uint8_t wisun_operating_class;
// 2757     uint8_t wisun_operating_mode;
// 2758     uint8_t phy_txPower;
// 2759     uint8_t phy_RegulatoryMode;
// 2760 
// 2761     /* Get command parameters */
// 2762     /* (Handle) */
// 2763     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
// 2764 
// 2765     /* (option) */
// 2766     res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);
// 2767 
// 2768     /* Get command parameters - ConfigParams */
// 2769     /* wisun_regulatory_domain */
// 2770     res |= AppCmd_HexStrToNum(&pCmd, &wisun_regulatory_domain, sizeof(wisun_regulatory_domain), R_FALSE);
// 2771 
// 2772     /* wisun_operating_class */
// 2773     res |= AppCmd_HexStrToNum(&pCmd, &wisun_operating_class, sizeof(wisun_operating_class), R_FALSE);
// 2774 
// 2775     /* wisun_operating_mode */
// 2776     res |= AppCmd_HexStrToNum(&pCmd, &wisun_operating_mode, sizeof(wisun_operating_mode), R_FALSE);
// 2777 
// 2778     /* phy_TxPower */
// 2779     res |= AppCmd_HexStrToNum(&pCmd, &phy_txPower, sizeof(phy_txPower), R_FALSE);
// 2780 
// 2781     /* phy_RegulatoryMode */
// 2782     res |= AppCmd_HexStrToNum(&pCmd, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode), R_FALSE);
// 2783 
// 2784     /* Apply PHY configuration according to the Wi-SUN PHY specification */
// 2785     if (res == R_RESULT_SUCCESS)
// 2786     {
// 2787         r_nwk_wisun_phy_config_t wisun_phy_config;
// 2788 
// 2789         wisun_phy_config.regulatory_domain = wisun_regulatory_domain;
// 2790         wisun_phy_config.operating_class = wisun_operating_class;
// 2791         wisun_phy_config.operating_mode = wisun_operating_mode;
// 2792         res |= R_NWK_SetRequest(R_NWK_phyWiSunPhyConfig, &wisun_phy_config, sizeof(wisun_phy_config));
// 2793     }
// 2794 
// 2795     if (res == R_RESULT_SUCCESS)
// 2796     {
// 2797         uint16_t phy_CcaVth = 0xFF5A;  // set to -85 dBm as default, depending on the region (regulatory domain) and
// 2798                                        // the data rate (operating mode) optimization of the CCA level threshold is recommended.
// 2799         res |= R_NWK_SetRequest(R_NWK_phyFskCcaVth, &phy_CcaVth, sizeof(phy_CcaVth));
// 2800         res |= R_NWK_SetRequest(R_NWK_phyOfdmCcaVth, &phy_CcaVth, sizeof(phy_CcaVth));
// 2801     }
// 2802 
// 2803     if (res == R_RESULT_SUCCESS)
// 2804     {
// 2805         phy_txPower = 0xFB;  // - 5dbm
// 2806         res |= R_NWK_SetRequest(R_NWK_phyTransmitPower, &phy_txPower, sizeof(phy_txPower));
// 2807         res |= R_NWK_GetRequest(R_NWK_phyTransmitPower, &phy_txPower, sizeof(phy_txPower));
// 2808     }
// 2809 
// 2810     /* Send Confirm -> PHY_SETC */
// 2811     R_Modem_print("PHYFAN_SETC %02X %02X\n", handle, res);
// 2812 }
// 2813 #else /* R_PHY_TYPE_CWX_M */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock53 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_PHYFAN_SETR
        CODE
// 2814 static void AppCmd_ProcessCmd_PHYFAN_SETR(uint8_t* pCmd)
// 2815 {
_AppCmd_ProcessCmd_PHYFAN_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 32
        SUBW      SP, #0x1C          ;; 1 cycle
          CFI CFA SP+36
// 2816     r_result_t res;
// 2817     uint8_t handle;
// 2818     uint16_t option;
// 2819 
// 2820     uint8_t wisun_regulatory_domain;
// 2821     uint8_t wisun_operating_class;
// 2822     uint8_t wisun_operating_mode;
// 2823     uint8_t phy_txPower;
// 2824     uint8_t phy_RegulatoryMode;
// 2825 
// 2826     uint8_t phy_AntennaSelectTx;
// 2827     uint16_t phy_CcaVth;
// 2828 
// 2829     /* Get command parameters */
// 2830     /* (Handle) */
// 2831     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFEE        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 2832 
// 2833     /* (option) */
// 2834     res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+46
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+44
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x11], A       ;; 1 cycle
// 2835 
// 2836     /* Get command parameters - ConfigParams */
// 2837     /* wisun_regulatory_domain */
// 2838     res |= AppCmd_HexStrToNum(&pCmd, &wisun_regulatory_domain, sizeof(wisun_regulatory_domain), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x11          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+50
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+48
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x14], A       ;; 1 cycle
// 2839 
// 2840     /* wisun_operating_class */
// 2841     res |= AppCmd_HexStrToNum(&pCmd, &wisun_operating_class, sizeof(wisun_operating_class), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+54
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+52
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x17], A       ;; 1 cycle
// 2842 
// 2843     /* wisun_operating_mode */
// 2844     res |= AppCmd_HexStrToNum(&pCmd, &wisun_operating_mode, sizeof(wisun_operating_mode), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+54
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x17          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+58
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+56
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x15], A       ;; 1 cycle
// 2845 
// 2846     /* phy_TxPower */
// 2847     res |= AppCmd_HexStrToNum(&pCmd, &phy_txPower, sizeof(phy_txPower), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x23          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x14          ;; 1 cycle
          CFI CFA SP+36
        ADDW      AX, #0x9           ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x1A], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, HL             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 2848 
// 2849     /* phy_RegulatoryMode */
// 2850     res |= AppCmd_HexStrToNum(&pCmd, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode), R_FALSE);
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       BC                 ;; 1 cycle
          CFI CFA SP+40
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+42
        MOV       [SP+0x1C], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x1A], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x0D]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x0B]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, DE             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+48
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+46
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+42
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, H               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 2851 
// 2852     /* Apply PHY configuration according to the Wi-SUN PHY specification */
// 2853     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+36
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_106  ;; 4 cycles
        ; ------------------------------------- Block: 205 cycles
// 2854     {
// 2855         r_nwk_wisun_phy_config_t wisun_phy_config;
// 2856 
// 2857         wisun_phy_config.regulatory_domain = wisun_regulatory_domain;
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
// 2858         wisun_phy_config.operating_class = wisun_operating_class;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
// 2859         wisun_phy_config.operating_mode = wisun_operating_mode;
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
// 2860         res |= R_NWK_SetRequest(R_NWK_phyWiSunPhyConfig, &wisun_phy_config, sizeof(wisun_phy_config));
        MOVW      BC, #0x3           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x7           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x20           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 16 cycles
// 2861     }
// 2862 
// 2863     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_106:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_107  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2864     {
// 2865         res |= R_NWK_SetRequest(R_NWK_phyTransmitPower, &phy_txPower, sizeof(phy_txPower));
        ONEW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x2            ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
// 2866     }
// 2867     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_107:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_108  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2868     {
// 2869         res |= R_NWK_SetRequest(R_NWK_phyRegulatoryMode, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode));
        ONEW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x1A           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
// 2870     }
// 2871     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_108:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_109  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2872     {
// 2873         phy_AntennaSelectTx = 1;  //  0 = TX and RX via connector J2;  1 = TX and RX via connector J3;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
// 2874         res |= R_NWK_SetRequest(R_NWK_phyAntennaSelectTx, &phy_AntennaSelectTx, sizeof(phy_AntennaSelectTx));
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x15           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
// 2875     }
// 2876     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_109:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_110  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2877     {
// 2878         phy_CcaVth = 0x01AA;  // set to -86 dBm as default, depending on the region (regulatory domain) and
        MOVW      AX, #0x1AA         ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
// 2879                               // the data rate (operating mode) optimization of the CCA level threshold is recommended.
// 2880         res |= R_NWK_SetRequest(R_NWK_phyCcaVth, &phy_CcaVth, sizeof(phy_CcaVth));
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x31           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
// 2881     }
// 2882 
// 2883 #if defined(FEM_SE2435L)
// 2884     // Initialize Front-end module_SE2435L (ANT1 port) / case2.
// 2885     // Adapt device minimum sense to support FEM_SE2435L.
// 2886 
// 2887     // The minimum receiver sensitivity level (0 -> -174 dBm; 254 -> +80 dBm).
// 2888     uint16_t phy_DataRate;
// 2889     res |= R_NWK_GetRequest(R_NWK_phyDataRate, &phy_DataRate, sizeof(phy_DataRate));
// 2890 
// 2891     // -------------------------------------------------
// 2892     //  DEVICE_MIN_SEN = Max. Sensitivity -4db +6dB
// 2893     // -------------------------------------------------
// 2894     //                      | 50kbps | 100kbps | 150kbps
// 2895     // -------------------------------------------------
// 2896     //  Maximum Sensitivity |  -102  |  -99    |  -97
// 2897     //  device_min_sens     |  -100  |  -97    |  -95
// 2898     // -------------------------------------------------
// 2899     uint8_t device_min_sens;
// 2900     if (phy_DataRate == 150)      // 150kbps mode
// 2901     {
// 2902         device_min_sens = 74;     // -100 dBm
// 2903     }
// 2904     else if (phy_DataRate == 100) // 100kbps mode
// 2905     {
// 2906         device_min_sens = 77;     // -97 dBm
// 2907     }
// 2908     else  // 50kbps mode
// 2909     {
// 2910         device_min_sens = 79;  // -95 dBm
// 2911     }
// 2912     res |= R_NWK_SetRequest(R_NWK_deviceMinSens, &device_min_sens, sizeof(device_min_sens));
// 2913 
// 2914     uint8_t phyAgcWaitGainOffset = 0x0D;
// 2915     res |= R_NWK_SetRequest(R_NWK_phyAgcWaitGainOffset, &phyAgcWaitGainOffset, sizeof(phyAgcWaitGainOffset));
// 2916 
// 2917     uint16_t phyCcaVth = 0x01AA;
// 2918     res |= R_NWK_SetRequest(R_NWK_phyCcaVth, &phyCcaVth, sizeof(phyCcaVth));
// 2919 
// 2920     uint8_t phyCcaVthOffset = 0x11;
// 2921     res |= R_NWK_SetRequest(R_NWK_phyCcaVthOffset, &phyCcaVthOffset, sizeof(phyCcaVthOffset));
// 2922     res |= R_NWK_SetRequest(R_NWK_phyRssiOutputOffset, &phyCcaVthOffset, sizeof(phyCcaVthOffset));
// 2923 
// 2924     uint8_t phyAntennaSwitchEna = 0x01;
// 2925     res |= R_NWK_SetRequest(R_NWK_phyAntennaSwitchEna, &phyAntennaSwitchEna, sizeof(phyAntennaSwitchEna));
// 2926 
// 2927     uint16_t phyAntennaSwitchEnaTiming = 0x012C;
// 2928     res |= R_NWK_SetRequest(R_NWK_phyAntennaSwitchEnaTiming, &phyAntennaSwitchEnaTiming, sizeof(phyAntennaSwitchEnaTiming));
// 2929 
// 2930 #elif defined(VIZMO_MODULE_28dBm)
// 2931     // Initialize Front-end module_SE2435L (ANT2 port) / case1.
// 2932     // Adapt device minimum sense.
// 2933 
// 2934     // The minimum receiver sensitivity level (0 -> -174 dBm; 254 -> +80 dBm).
// 2935     uint16_t phy_DataRate;
// 2936     res |= R_NWK_GetRequest(R_NWK_phyDataRate, &phy_DataRate, sizeof(phy_DataRate));
// 2937 
// 2938     // -------------------------------------------------
// 2939     //  DEVICE_MIN_SEN = Max. Sensitivity -4db +6dB
// 2940     // -------------------------------------------------
// 2941     //                      | 50kbps | 100kbps | 150kbps
// 2942     // -------------------------------------------------
// 2943     //  Maximum Sensitivity |  -102  |  -99    |  -97
// 2944     //  device_min_sens     |  -100  |  -97    |  -95
// 2945     // -------------------------------------------------
// 2946     uint8_t device_min_sens;
// 2947     if (phy_DataRate == 150)      // 150kbps mode
// 2948     {
// 2949         device_min_sens = 74;     // -100 dBm
// 2950     }
// 2951     else if (phy_DataRate == 100) // 100kbps mode
// 2952     {
// 2953         device_min_sens = 77;     // -97 dBm
// 2954     }
// 2955     else  // 50kbps mode
// 2956     {
// 2957         device_min_sens = 79;  // -95 dBm
// 2958     }
// 2959     res |= R_NWK_SetRequest(R_NWK_deviceMinSens, &device_min_sens, sizeof(device_min_sens));
// 2960 
// 2961     uint8_t phyAgcWaitGainOffset = 0x0D;
// 2962     res |= R_NWK_SetRequest(R_NWK_phyAgcWaitGainOffset, &phyAgcWaitGainOffset, sizeof(phyAgcWaitGainOffset));
// 2963 
// 2964     uint16_t phyCcaVth = 0x01AA;
// 2965     res |= R_NWK_SetRequest(R_NWK_phyCcaVth, &phyCcaVth, sizeof(phyCcaVth));
// 2966 
// 2967     uint8_t phyCcaVthOffset = 0x11;
// 2968     res |= R_NWK_SetRequest(R_NWK_phyCcaVthOffset, &phyCcaVthOffset, sizeof(phyCcaVthOffset));
// 2969     res |= R_NWK_SetRequest(R_NWK_phyRssiOutputOffset, &phyCcaVthOffset, sizeof(phyCcaVthOffset));
// 2970 
// 2971     uint8_t phyAntennaSwitchEna = 0x01;
// 2972     res |= R_NWK_SetRequest(R_NWK_phyAntennaSwitchEna, &phyAntennaSwitchEna, sizeof(phyAntennaSwitchEna));
// 2973 
// 2974     uint16_t phyAntennaSwitchEnaTiming = 0x012C;
// 2975     res |= R_NWK_SetRequest(R_NWK_phyAntennaSwitchEnaTiming, &phyAntennaSwitchEnaTiming, sizeof(phyAntennaSwitchEnaTiming));
// 2976 
// 2977     uint8_t phyGpio0Setting = 0x01;
// 2978     res |= R_NWK_SetRequest(R_NWK_phyGpio0Setting, &phyGpio0Setting, sizeof(phyGpio0Setting));
// 2979 #else  /* if defined(FEM_SE2435L) */
// 2980     uint16_t phy_DataRate;
// 2981     res |= R_NWK_GetRequest(R_NWK_phyDataRate, &phy_DataRate, sizeof(phy_DataRate));
??R_DHCPV6_VendorOptionRecvCallback_110:
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x36           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2982 
// 2983     // -------------------------------------------------
// 2984     //  DEVICE_MIN_SEN = Max. Sensitivity +6dB
// 2985     // -------------------------------------------------
// 2986     //                      | 50kbps | 100kbps | 150kbps
// 2987     // -------------------------------------------------
// 2988     //  Maximum Sensitivity |  -102  |  -99    |  -97
// 2989     //  device_min_sens     |   -96  |  -93    |  -91
// 2990     // -------------------------------------------------
// 2991     uint8_t device_min_sens;
// 2992     if (phy_DataRate == 150)      // 150kbps mode
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        CMPW      AX, #0x96          ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_111  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 2993     {
// 2994         device_min_sens = 83;     // -91 dBm
        MOV       A, #0x53           ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_112  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2995     }
// 2996     else if (phy_DataRate == 100) // 100kbps mode
??R_DHCPV6_VendorOptionRecvCallback_111:
        CMPW      AX, #0x64          ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_113  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2997     {
// 2998         device_min_sens = 81;     // -93 dBm
        MOV       A, #0x51           ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_112  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2999     }
// 3000     else  // 50kbps mode
// 3001     {
// 3002         device_min_sens = 78;  // -96 dBm
??R_DHCPV6_VendorOptionRecvCallback_113:
        MOV       A, #0x4E           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??R_DHCPV6_VendorOptionRecvCallback_112:
        MOV       [SP+0x01], A       ;; 1 cycle
// 3003     }
// 3004     res |= R_NWK_SetRequest(R_NWK_deviceMinSens, &device_min_sens, sizeof(device_min_sens));
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0xA3           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 3005 #endif /* if defined(FEM_SE2435L) */
// 3006 
// 3007     /* Send Confirm -> PHY_SETC */
// 3008     R_Modem_print("PHYFAN_SETC %02X %02X\n", handle, res);
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      DE, #LWRD(?_68)    ;; 1 cycle
        MOV       A, #BYTE3(?_68)    ;; 1 cycle
        BR        F:?Subroutine6     ;; 3 cycles
          CFI EndBlock cfiBlock53
        ; ------------------------------------- Block: 24 cycles
        ; ------------------------------------- Total: 344 cycles
// 3009 
// 3010 }
// 3011 #endif /* R_PHY_TYPE_CWX_M */
// 3012 
// 3013 /********************************************************************************
// 3014 * Function Name     : AppCmd_ProcessCmd_PHYFAN_GETR
// 3015 * Description       : Process "GETR" command (Output common message)
// 3016 * Arguments         : r_result_t ... result when get command
// 3017 *                   : handle ... index
// 3018 *                   : paramType ... parameter type
// 3019 * Return Value      : None
// 3020 *                   : paramType ... parameter type (=PhyConfig)
// 3021 * Return Value      : None
// 3022 * Output            : Confirm command
// 3023 *                   :   GETC (Handle) (Status) (ParamType)
// 3024 *                   :     PhyConfig:
// 3025 *                   :       (FreqBandId) (FSKOpeMode) (TxPower) (RegulatoryMode)
// 3026 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock54 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_PHYFAN_GETR
        CODE
// 3027 static void AppCmd_ProcessCmd_PHYFAN_GETR(uint8_t* pCmd)
// 3028 {
_AppCmd_ProcessCmd_PHYFAN_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 18
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+22
// 3029     r_result_t res;
// 3030     uint8_t handle;
// 3031     uint16_t option;
// 3032 
// 3033     r_nwk_wisun_phy_config_t wisun_phy_config;
// 3034 
// 3035     uint8_t phy_txpower;
// 3036     uint8_t phy_RegulatoryMode;
// 3037 
// 3038     /* Get command parameters */
// 3039     /* (Handle) */
// 3040     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x7           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 3041 
// 3042     /* (option) */
// 3043     res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+32
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 3044 
// 3045     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+22
        MOV       [SP], A            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_114  ;; 4 cycles
        ; ------------------------------------- Block: 63 cycles
// 3046     {
// 3047         /* (TxPower) */
// 3048         res |= R_NWK_GetRequest(R_NWK_phyTransmitPower, &phy_txpower, sizeof(phy_txpower));
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x2            ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 3049 
// 3050         /* (phy_RegulatoryMode) */
// 3051         res |= R_NWK_GetRequest(R_NWK_phyRegulatoryMode, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode));
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x1A           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 22 cycles
// 3052     }
// 3053 
// 3054     /* Obtain Wi-SUN PHY configuration parameters */
// 3055     if (res == R_RESULT_SUCCESS)
??R_DHCPV6_VendorOptionRecvCallback_114:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_115  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3056     {
// 3057         res |= R_NWK_GetRequest(R_NWK_phyWiSunPhyConfig, &wisun_phy_config, sizeof(wisun_phy_config));
        MOVW      BC, #0x3           ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x20           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
// 3058     }
// 3059 
// 3060     /* Generate confirm PHY_GETC*/
// 3061     R_Modem_print("PHYFAN_GETC %02X %02X ", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_115:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      DE, #LWRD(?_0+650)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3062     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_116  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 3063     {
// 3064         /* wisun_regulatory_domain */
// 3065         R_Modem_print("%02X ", wisun_phy_config.regulatory_domain);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3066 
// 3067         /* wisun_operating_class */
// 3068         R_Modem_print("%02X ", wisun_phy_config.operating_class);
        MOV       A, [SP+0x07]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3069 
// 3070         /* wisun_operating_mode */
// 3071         R_Modem_print("%02X ", wisun_phy_config.operating_mode);
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3072 
// 3073         /* phy_txPower */
// 3074         R_Modem_print("%02X ", phy_txpower);
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3075 
// 3076         /* phy_RegulatoryMode */
// 3077         R_Modem_print("%02X ", phy_RegulatoryMode);
        MOV       A, [SP+0x09]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+22
        ; ------------------------------------- Block: 46 cycles
// 3078     }
// 3079     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_116:
        BR        F:?Subroutine1     ;; 3 cycles
          CFI EndBlock cfiBlock54
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 172 cycles
// 3080 }
// 3081 
// 3082 
// 3083 /********************************************************************************
// 3084 * Function Name     : AppCmd_ProcessCmd_PHYNET_SETR
// 3085 * Description       : Process "SETR" command - ParamType = ConfigParams
// 3086 * Arguments         : pCmd ... Pointer to command parameters
// 3087 *                   :   PhyConfig for Netricity -> tbd
// 3088 * Return Value      : None
// 3089 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock55 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_PHYNET_SETR
          CFI NoCalls
        CODE
// 3090 static void AppCmd_ProcessCmd_PHYNET_SETR(uint8_t* pCmd)
// 3091 {
_AppCmd_ProcessCmd_PHYNET_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3092     UNUSED_SYM(pCmd);
// 3093 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock55
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 6 cycles
// 3094 
// 3095 
// 3096 /********************************************************************************
// 3097 * Function Name     : AppCmd_ProcessCmd_PHYNET_GETR
// 3098 * Description       : Process "GETR" command (Output common message)
// 3099 * Arguments         : r_result_t ... result when get command
// 3100 *                   : handle ... index
// 3101 *                   : paramType ... parameter type
// 3102 * Return Value      : None
// 3103 *                   : paramType ... parameter type (=PhyConfig)
// 3104 * Return Value      : None
// 3105 * Output            : Confirm command
// 3106 *                   :   GETC (Handle) (Status) (ParamType)
// 3107 *                   :     PhyConfig for Netricity -> tbd
// 3108 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock56 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_PHYNET_GETR
          CFI NoCalls
        CODE
// 3109 static void AppCmd_ProcessCmd_PHYNET_GETR(uint8_t* pCmd)
// 3110 {
_AppCmd_ProcessCmd_PHYNET_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3111     UNUSED_SYM(pCmd);
// 3112 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock56
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 6 cycles
// 3113 
// 3114 /********************************************************************************
// 3115 * Function Name     : AppCmd_ProcessCmd_SCHEDULE_SETR
// 3116 * Description       : Process "SETR" command - ParamType = ConfigParams
// 3117 * Arguments         : pCmd ... Pointer to command parameters
// 3118 *                   :   ConfigParams: (DeviceType) (ChannelMask) (PANId) (TxPower)
// 3119 *                   :                 (NetworkName) (RCVIEn)
// 3120 * Return Value      : None
// 3121 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock57 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_UCSCH_SETR
          CFI NoCalls
        CODE
// 3122 static void AppCmd_ProcessCmd_UCSCH_SETR(uint8_t* pCmd)
// 3123 {
_AppCmd_ProcessCmd_UCSCH_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3124     AppCmd_ProcessCmd_SCHEDULE_SETR(pCmd, APP_CMD_ParamType_UnicastSchedule);
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CLRB      X                  ;; 1 cycle
          CFI EndBlock cfiBlock57
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 7 cycles
        REQUIRE ?Subroutine10
        ; // Fall through to label ?Subroutine10
// 3125 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock58 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+8
          CFI FunCall _AppCmd_ProcessCmd_UCSCH_SETR _AppCmd_ProcessCmd_SCHEDULE_SETR
          CFI FunCall _AppCmd_ProcessCmd_BCSCH_SETR _AppCmd_ProcessCmd_SCHEDULE_SETR
        CODE
?Subroutine10:
        CALL      F:_AppCmd_ProcessCmd_SCHEDULE_SETR  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock58
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 3126 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock59 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_BCSCH_SETR
          CFI NoCalls
        CODE
// 3127 static void AppCmd_ProcessCmd_BCSCH_SETR(uint8_t* pCmd)
// 3128 {
_AppCmd_ProcessCmd_BCSCH_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3129     AppCmd_ProcessCmd_SCHEDULE_SETR(pCmd, APP_CMD_ParamType_BroadcastSchedule);
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ONEB      X                  ;; 1 cycle
        BR        F:?Subroutine10    ;; 3 cycles
          CFI EndBlock cfiBlock59
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 3130 }
// 3131 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock60 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_SCHEDULE_SETR
        CODE
// 3132 static void AppCmd_ProcessCmd_SCHEDULE_SETR(uint8_t* pCmd, r_app_schedule_type_t paramType)
// 3133 {
_AppCmd_ProcessCmd_SCHEDULE_SETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 188
        SUBW      SP, #0xB6          ;; 1 cycle
          CFI CFA SP+192
// 3134     r_result_t res;
// 3135     uint8_t handle;
// 3136     uint16_t option;
// 3137 
// 3138     uint8_t phyCurrentChannel;
// 3139 
// 3140     uint8_t i;
// 3141     r_ie_wp_broadcast_schedule_t schedule;     // structure to hold the parsed values from the command
// 3142     r_app_schedule_buffer_t request = {{ 0 }}; // buffer to hold the serialized schedule that is passed to the NWK layer
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x50          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_69)    ;; 1 cycle
        MOV       ES, #BYTE3(?_69)   ;; 1 cycle
        MOVW      BC, #0x3D          ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
// 3143     uint8_t bitfield_buffer = 0;               // buffer to temporarily store parsed values for bitfields
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 3144 
// 3145     /* Get command parameters */
// 3146     /* (Handle) */
// 3147     res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xB8          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFF4E        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 3148 
// 3149     /* (option) */
// 3150     res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+202
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+204
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+202
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+200
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
// 3151 
// 3152     /* Initialize schedule */
// 3153     MEMZERO_S(&schedule);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+192
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x4E], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x4C], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        MOV       X, #0x23           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+198
        POP       DE                 ;; 1 cycle
          CFI CFA SP+196
        CLRB      X                  ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_memset
        CALL      F:_R_memset        ;; 3 cycles
// 3154     union
// 3155     {
// 3156         r_ie_wp_excluded_channel_range_t excluded_ranges[R_SCHEDULE_IE_EXCLUDED_CHANNEL_RANGES_MAX];
// 3157         uint8_t excluded_mask[R_SCHEDULE_IE_EXCLUDED_CHANNEL_MASK_MAX_SIZE];
// 3158     } channel_exclusions;
// 3159     MEMZERO_S(&channel_exclusions);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x91          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+192
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x12], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        MOV       X, #0x28           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+198
        POP       DE                 ;; 1 cycle
          CFI CFA SP+196
        CLRB      X                  ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_memset
        CALL      F:_R_memset        ;; 3 cycles
// 3160 
// 3161     schedule.us.excluded_ranges.ranges = &channel_exclusions.excluded_ranges[0];
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+192
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOV       A, L               ;; 1 cycle
        MOV       [SP+0x3F], A       ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       [SP+0x40], A       ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x41], A       ;; 1 cycle
        MOV       A, [SP+0x0F]       ;; 1 cycle
        MOV       [SP+0x42], A       ;; 1 cycle
// 3162     schedule.us.excluded_channel_mask = &channel_exclusions.excluded_mask[0];
        MOV       A, L               ;; 1 cycle
        MOV       [SP+0x43], A       ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       [SP+0x44], A       ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       [SP+0x45], A       ;; 1 cycle
        MOV       A, [SP+0x0F]       ;; 1 cycle
        MOV       [SP+0x46], A       ;; 1 cycle
// 3163 
// 3164     /* Get command parameters - ConfigParams */
// 3165     if (paramType == APP_CMD_ParamType_BroadcastSchedule)
        MOV       A, [SP+0xB6]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_117  ;; 4 cycles
        ; ------------------------------------- Block: 138 cycles
// 3166     {
// 3167         /* broadcast_interval */
// 3168         res |= AppCmd_HexStrToNum(&pCmd, &schedule.broadcast_interval, sizeof(schedule.broadcast_interval), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      AX, [SP+0x50]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x07], A       ;; 1 cycle
// 3169 
// 3170         /* broadcast_schedule_identifier */
// 3171         res |= AppCmd_HexStrToNum(&pCmd, &schedule.broadcast_schedule_identifier, sizeof(schedule.broadcast_schedule_identifier), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x30          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 28 cycles
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_0:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_1:
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+202
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+204
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+202
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+200
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x0B]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+192
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 21 cycles
// 3172     }
// 3173 
// 3174     /* Dwell_Interval  */
// 3175     res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.dwell_interval, sizeof(schedule.us.dwell_interval), R_FALSE);
??R_DHCPV6_VendorOptionRecvCallback_117:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #0xF           ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        MOV       ES, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_2:
        ADDW      AX, #0x1E          ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x4A], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x48], AX      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      AX, HL             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x17], A       ;; 1 cycle
// 3176 
// 3177     /* Clock_Drift  */
// 3178     res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.clock_drift, sizeof(schedule.us.clock_drift), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x33          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 36 cycles
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_3:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_4:
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+202
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+204
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+202
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+200
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x1A], A       ;; 1 cycle
// 3179 
// 3180     /* Timing_Accuracy  */
// 3181     res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.timing_accuracy, sizeof(schedule.us.timing_accuracy), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+202
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+204
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 23 cycles
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_5:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_6:
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+206
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+208
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+206
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+204
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x1D], A       ;; 1 cycle
// 3182 
// 3183     /* ChannelPlan */
// 3184     res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xD           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+192
        ADDW      AX, #0x1F          ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x22], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x20], AX      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      AX, HL             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x14], A       ;; 1 cycle
// 3185     schedule.us.channel_plan = (r_ie_wp_schedule_channel_plan_t)bitfield_buffer;
        MOV       A, [SP+0x31]       ;; 1 cycle
        AND       A, #0xF8           ;; 1 cycle
        MOV       [SP+0x31], A       ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
        AND       A, #0x7            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x31          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        OR        A, [HL]            ;; 1 cycle
        MOV       [SP+0x31], A       ;; 1 cycle
// 3186 
// 3187     /* ChannelFunction */
// 3188     res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x28]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+202
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+204
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+202
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+200
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x17], A       ;; 1 cycle
// 3189     schedule.us.channel_function = (r_ie_wp_schedule_channel_function_t)bitfield_buffer;
        MOV       A, [SP+0x35]       ;; 1 cycle
        AND       A, #0xC7           ;; 1 cycle
        MOV       [SP+0x35], A       ;; 1 cycle
        MOV       A, [SP+0x09]       ;; 1 cycle
        SHL       A, 0x3             ;; 1 cycle
        AND       A, #0x38           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x35          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        OR        A, [HL]            ;; 1 cycle
        MOV       [SP+0x35], A       ;; 1 cycle
// 3190 
// 3191     /* ExcludedChannelControl */
// 3192     res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+202
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+204
        MOVW      AX, [SP+0x2C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+206
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+208
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+206
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+204
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x1A], A       ;; 1 cycle
// 3193     schedule.us.excluded_channel_control = (r_ie_wp_schedule_excluded_channel_control_t)bitfield_buffer;
        MOV       A, [SP+0x39]       ;; 1 cycle
        AND       A, #0x3F           ;; 1 cycle
        MOV       [SP+0x39], A       ;; 1 cycle
        MOV       A, [SP+0x0D]       ;; 1 cycle
        SHL       A, 0x6             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x39          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        OR        A, [HL]            ;; 1 cycle
        MOV       [SP+0x39], A       ;; 1 cycle
// 3194 
// 3195     /* RegulatoryDomain */
// 3196     res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+206
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+208
        MOVW      AX, [SP+0x30]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+210
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+212
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+210
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+208
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x1D], A       ;; 1 cycle
// 3197     schedule.us.regulatory_domain = (r_ie_wp_schedule_regulatory_domain_t)bitfield_buffer;
        MOV       A, [SP+0x11]       ;; 1 cycle
        MOV       [SP+0x3E], A       ;; 1 cycle
// 3198 
// 3199     /* OperatingClass */
// 3200     res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.operating_class, sizeof(schedule.us.operating_class), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+210
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+212
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x43          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 144 cycles
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_7:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_8:
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+214
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+216
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+214
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+212
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x20], A       ;; 1 cycle
// 3201 
// 3202     /* CH0 */
// 3203     res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.ch0, sizeof(schedule.us.ch0), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+214
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+216
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x48          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 23 cycles
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_9:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_10:
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+218
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+220
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+218
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+216
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x1D], A       ;; 1 cycle
// 3204 
// 3205     /* ChannelSpacing */
// 3206     res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+218
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+220
        MOVW      AX, [SP+0x3C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+222
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+224
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+222
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+220
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x1F], A       ;; 1 cycle
// 3207     schedule.us.channel_spacing = (r_ie_wp_schedule_channel_spacing_t)bitfield_buffer;
        MOV       A, [SP+0x50]       ;; 1 cycle
        AND       A, #0xF0           ;; 1 cycle
        MOV       [SP+0x50], A       ;; 1 cycle
        MOV       A, [SP+0x1D]       ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x50          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        OR        A, [HL]            ;; 1 cycle
        MOV       [SP+0x50], A       ;; 1 cycle
// 3208 
// 3209     /* NumberOfChannels */
// 3210     res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.no_of_channels, sizeof(schedule.us.no_of_channels), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+222
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+224
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x1           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 52 cycles
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_11:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_12:
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+226
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+228
        MOVW      AX, [SP+0x2A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+226
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+224
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 3211 
// 3212     /* FixedChannel  */
// 3213     res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.fixed_channel, sizeof(schedule.us.fixed_channel), R_FALSE);
        MOV       A, [SP+0x32]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x33]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x31]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x30]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x2F]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x2E]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x2D]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x2C]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x25]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x23]       ;; 1 cycle
        OR        A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+226
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+228
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+230
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5D          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 53 cycles
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_13:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_14:
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+232
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+234
        MOVW      AX, [SP+0x30]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+232
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+230
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+226
        POP       BC                 ;; 1 cycle
          CFI CFA SP+224
        MOV       A, B               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x20]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP+0x20], A       ;; 1 cycle
// 3214 
// 3215     if (schedule.us.excluded_channel_control == R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_RANGE)
        MOV       A, [SP+0x4D]       ;; 1 cycle
        AND       A, #0xC0           ;; 1 cycle
        ADDW      SP, #0x20          ;; 1 cycle
          CFI CFA SP+192
        CMP       A, #0x40           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_118  ;; 4 cycles
        ; ------------------------------------- Block: 30 cycles
// 3216     {
// 3217         /* No_of_excluded_ranges */
// 3218         res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.no_of_excluded_ranges, sizeof(schedule.us.no_of_excluded_ranges), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_15:
        MOV       A, #0xF            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_16:
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 3219 
// 3220         for (i = 0; i < schedule.us.no_of_excluded_ranges; i++)
        MOV       [SP+0x04], A       ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+192
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_119  ;; 3 cycles
        ; ------------------------------------- Block: 22 cycles
// 3221         {
??AppCmd_ProcessCmd_SCHEDULE_SETR_17:
        OR        A, #0x11           ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_120  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_18:
        OR        A, #0x11           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_19:
        MOV       [SP+0x05], A       ;; 1 cycle
// 3222             /* Start Channel Number */
// 3223             res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.excluded_ranges.ranges[i].start, sizeof(schedule.us.excluded_ranges.ranges[i].start), R_FALSE);
// 3224 
// 3225             /* End Channel Number */
// 3226             res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.excluded_ranges.ranges[i].end, sizeof(schedule.us.excluded_ranges.ranges[i].end), R_FALSE);
        MOV       A, [SP+0x3F]       ;; 1 cycle
        MOV       [SP+0x18], A       ;; 1 cycle
        MOV       A, [SP+0x40]       ;; 1 cycle
        MOV       [SP+0x19], A       ;; 1 cycle
        MOV       A, [SP+0x41]       ;; 1 cycle
        MOV       [SP+0x1A], A       ;; 1 cycle
        MOV       A, [SP+0x42]       ;; 1 cycle
        MOV       [SP+0x1B], A       ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x1E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _AppHexStrToNum
        CALL      F:_AppHexStrToNum  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+192
        CMP0      A                  ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
        BNZ       ??AppCmd_ProcessCmd_SCHEDULE_SETR_17  ;; 4 cycles
        ; ------------------------------------- Block: 44 cycles
??R_DHCPV6_VendorOptionRecvCallback_120:
        MOV       [SP], A            ;; 1 cycle
        MOV       A, [SP+0x03]       ;; 1 cycle
        INC       A                  ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??R_DHCPV6_VendorOptionRecvCallback_119:
        MOV       [SP+0x03], A       ;; 1 cycle
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x3E]       ;; 1 cycle
        CMP       X, A               ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_121  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOV       A, [SP+0x3F]       ;; 1 cycle
        MOV       [SP+0x1C], A       ;; 1 cycle
        MOV       A, [SP+0x40]       ;; 1 cycle
        MOV       [SP+0x1D], A       ;; 1 cycle
        MOV       A, [SP+0x41]       ;; 1 cycle
        MOV       [SP+0x1E], A       ;; 1 cycle
        MOV       A, [SP+0x42]       ;; 1 cycle
        MOV       [SP+0x1F], A       ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _AppHexStrToNum
        CALL      F:_AppHexStrToNum  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+192
        CMP0      A                  ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??AppCmd_ProcessCmd_SCHEDULE_SETR_18  ;; 4 cycles
        ; ------------------------------------- Block: 44 cycles
        BR        R:??AppCmd_ProcessCmd_SCHEDULE_SETR_19  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3227         }
// 3228     }
// 3229     else if (schedule.us.excluded_channel_control == R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_MASK)
??R_DHCPV6_VendorOptionRecvCallback_118:
        CMP       A, #0x80           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_121  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3230     {
// 3231         /* excluded_channel_mask */
// 3232         for (i = 0; i < R_IE_ScheduleNumberOfMaskBytes(&schedule.us); i++)
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_20:
        MOV       [SP+0x03], A       ;; 1 cycle
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOV       A, [SP+0x4A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x48]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_IE_ScheduleNumberOfMaskBytes
        CALL      F:_R_IE_ScheduleNumberOfMaskBytes  ;; 3 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
          CFI FunCall ?SI_CMP_L02
        CALL      N:?SI_CMP_L02      ;; 3 cycles
        SKC                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_121  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
// 3233         {
// 3234             res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.excluded_channel_mask[i], sizeof(schedule.us.excluded_channel_mask[i]), R_FALSE);
        MOV       A, [SP+0x43]       ;; 1 cycle
        MOV       [SP+0x14], A       ;; 1 cycle
        MOV       A, [SP+0x44]       ;; 1 cycle
        MOV       [SP+0x15], A       ;; 1 cycle
        MOV       A, [SP+0x45]       ;; 1 cycle
        MOV       [SP+0x16], A       ;; 1 cycle
        MOV       A, [SP+0x46]       ;; 1 cycle
        MOV       [SP+0x17], A       ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _AppHexStrToNum
        CALL      F:_AppHexStrToNum  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+192
        CMP0      A                  ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_122  ;; 4 cycles
        ; ------------------------------------- Block: 40 cycles
        MOV       A, [SP]            ;; 1 cycle
        OR        A, #0x11           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 3235         }
??R_DHCPV6_VendorOptionRecvCallback_122:
        MOV       A, [SP+0x03]       ;; 1 cycle
        INC       A                  ;; 1 cycle
        BR        S:??AppCmd_ProcessCmd_SCHEDULE_SETR_20  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3236     }
// 3237 
// 3238     if (res == R_RESULT_SUCCESS)
// 3239     {
// 3240         phyCurrentChannel = (uint8_t)schedule.us.fixed_channel;
// 3241         res = R_NWK_SetRequest(R_NWK_phyCurrentChannel, &phyCurrentChannel, sizeof(phyCurrentChannel));
// 3242     }
// 3243 
// 3244     if (res == R_RESULT_SUCCESS)
// 3245     {
// 3246         if (paramType == APP_CMD_ParamType_BroadcastSchedule)
// 3247         {
// 3248             request.schedule.size = R_IE_BroadcastScheduleCreate(&schedule, request.schedule.bytes, sizeof(request) - sizeof(request.schedule), 0);
// 3249             if (request.schedule.size > 0)
// 3250             {
// 3251                 res = R_NWK_SetRequest(R_NWK_nwkSchedule_Broadcast, &request, sizeof(request));
??AppCmd_ProcessCmd_SCHEDULE_SETR_21:
        MOVW      BC, #0x3D          ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x50          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x91           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_22:
        MOV       [SP], A            ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_123  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3252             }
// 3253             else
// 3254             {
// 3255                 res = (r_result_t)(-request.schedule.size);  // create function returns negative r_result_t on error -> negate again
// 3256             }
// 3257         }
// 3258         else
// 3259         {
// 3260             request.schedule.size = R_IE_UnicastScheduleCreate(&schedule.us, request.schedule.bytes, sizeof(request) - sizeof(request.schedule), 0);
??AppCmd_ProcessCmd_SCHEDULE_SETR_23:
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        MOV       X, #0x3B           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+200
        MOV       A, [SP+0x52]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x50]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, A               ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _R_IE_UnicastScheduleCreate
        CALL      F:_R_IE_UnicastScheduleCreate  ;; 3 cycles
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x54], A       ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOV       [SP+0x55], A       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
// 3261             if (request.schedule.size > 0)
        MOV       A, [SP+0x54]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+192
        OR        A, X               ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_124  ;; 4 cycles
        ; ------------------------------------- Block: 42 cycles
        MOV       A, L               ;; 1 cycle
        XOR       A, #0xFF           ;; 1 cycle
        INC       A                  ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_24:
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 3262             {
// 3263                 res = R_NWK_SetRequest(R_NWK_nwkSchedule_Unicast, &request, sizeof(request));
// 3264             }
// 3265             else
// 3266             {
// 3267                 res = (r_result_t)(-request.schedule.size);  // create function returns negative r_result_t on error -> negate again
// 3268             }
// 3269         }
// 3270     }
// 3271 
// 3272     /* Send confirm */
// 3273     if (paramType == APP_CMD_ParamType_BroadcastSchedule)
// 3274     {
// 3275         R_Modem_print("BCSCH_SETC %02X %02X\n", handle, res);
// 3276     }
// 3277     else
// 3278     {
// 3279         R_Modem_print("UCSCH_SETC %02X %02X\n", handle, res);
??AppCmd_ProcessCmd_SCHEDULE_SETR_25:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      DE, #LWRD(?_69+84)  ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_26:
        MOV       A, #BYTE3(?_69)    ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+192
// 3280     }
// 3281 }
        ADDW      SP, #0xBC          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+192
        ; ------------------------------------- Block: 12 cycles
??R_DHCPV6_VendorOptionRecvCallback_124:
        MOVW      BC, #0x3D          ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x50          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, #0x90           ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        BR        S:??AppCmd_ProcessCmd_SCHEDULE_SETR_24  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_27:
        MOV       A, [SP+0xB6]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??AppCmd_ProcessCmd_SCHEDULE_SETR_25  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??R_DHCPV6_VendorOptionRecvCallback_123:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      DE, #LWRD(?_69+62)  ;; 1 cycle
        BR        S:??AppCmd_ProcessCmd_SCHEDULE_SETR_26  ;; 3 cycles
          CFI CFA SP+192
        ; ------------------------------------- Block: 12 cycles
??R_DHCPV6_VendorOptionRecvCallback_121:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_125  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x37]       ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        CLRB      A                  ;; 1 cycle
          CFI FunCall _R_NWK_SetRequest
        CALL      F:_R_NWK_SetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
??R_DHCPV6_VendorOptionRecvCallback_125:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??AppCmd_ProcessCmd_SCHEDULE_SETR_27  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x52          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #0xF           ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        MOV       ES, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_SETR_28:
        ADDW      AX, #0xFFB4        ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP+0xB6]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??AppCmd_ProcessCmd_SCHEDULE_SETR_23  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+194
        MOV       X, #0x3B           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+196
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+198
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+200
        MOVW      AX, [SP+0x54]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+198
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+196
          CFI FunCall _R_IE_BroadcastScheduleCreate
        CALL      F:_R_IE_BroadcastScheduleCreate  ;; 3 cycles
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x54], A       ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOV       [SP+0x55], A       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x54]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+192
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??AppCmd_ProcessCmd_SCHEDULE_SETR_21  ;; 4 cycles
        ; ------------------------------------- Block: 39 cycles
        MOV       A, L               ;; 1 cycle
        XOR       A, #0xFF           ;; 1 cycle
        INC       A                  ;; 1 cycle
        BR        R:??AppCmd_ProcessCmd_SCHEDULE_SETR_22  ;; 3 cycles
          CFI EndBlock cfiBlock60
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 984 cycles
// 3282 
// 3283 /********************************************************************************
// 3284 * Function Name     : AppSubGetR_ScheduleConfig
// 3285 * Description       : Process "GETR" command (Output common message)
// 3286 * Arguments         : r_result_t ... result when get command
// 3287 *                   : handle ... index
// 3288 *                   : paramType ... parameter type
// 3289 * Return Value      : None
// 3290 *                   : paramType ... parameter type (=PhyConfig)
// 3291 * Return Value      : None
// 3292 * Output            : Confirm command
// 3293 *                   :   GETC (Handle) (Status) (ParamType) (FreqBandId) (FSKOpeMode) (RegulatoryMode)
// 3294 *                   :     PhyConfig:
// 3295 *                   :       (FreqBandId) (FSKOpeMode) (RegulatoryMode)
// 3296 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock61 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_UCSCH_GETR
          CFI NoCalls
        CODE
// 3297 static void AppCmd_ProcessCmd_UCSCH_GETR(uint8_t* pCmd)
// 3298 {
_AppCmd_ProcessCmd_UCSCH_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3299     AppCmd_ProcessCmd_SCHEDULE_GETR(pCmd, APP_CMD_ParamType_UnicastSchedule);
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CLRB      X                  ;; 1 cycle
          CFI EndBlock cfiBlock61
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 7 cycles
        REQUIRE ?Subroutine11
        ; // Fall through to label ?Subroutine11
// 3300 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock62 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+8
          CFI FunCall _AppCmd_ProcessCmd_UCSCH_GETR _AppCmd_ProcessCmd_SCHEDULE_GETR
          CFI FunCall _AppCmd_ProcessCmd_BCSCH_GETR _AppCmd_ProcessCmd_SCHEDULE_GETR
        CODE
?Subroutine11:
        CALL      F:_AppCmd_ProcessCmd_SCHEDULE_GETR  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock62
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 3301 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock63 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_BCSCH_GETR
          CFI NoCalls
        CODE
// 3302 static void AppCmd_ProcessCmd_BCSCH_GETR(uint8_t* pCmd)
// 3303 {
_AppCmd_ProcessCmd_BCSCH_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3304     AppCmd_ProcessCmd_SCHEDULE_GETR(pCmd, APP_CMD_ParamType_BroadcastSchedule);
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ONEB      X                  ;; 1 cycle
        BR        F:?Subroutine11    ;; 3 cycles
          CFI EndBlock cfiBlock63
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 3305 }
// 3306 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock64 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd_SCHEDULE_GETR
        CODE
// 3307 static void AppCmd_ProcessCmd_SCHEDULE_GETR(uint8_t* pCmd, uint8_t paramType)
// 3308 {
_AppCmd_ProcessCmd_SCHEDULE_GETR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 60
        SUBW      SP, #0x36          ;; 1 cycle
          CFI CFA SP+64
// 3309     r_ie_wp_broadcast_schedule_t schedule = {0};
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(?_0)     ;; 1 cycle
        MOV       ES, #BYTE3(?_0)    ;; 1 cycle
        MOVW      BC, #0x23          ;; 1 cycle
          CFI FunCall ?FAR_NEAR_MOVE_LONG_L06
        CALL      N:?FAR_NEAR_MOVE_LONG_L06  ;; 3 cycles
// 3310 
// 3311     uint8_t handle;
// 3312     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
        ADDW      AX, #0x26          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      AX, #0xFFCC        ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+70
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+68
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 3313     uint16_t option;
// 3314     res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+74
        MOV       X, #0xF            ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+72
          CFI FunCall _AppCmd_HexStrToNum
        CALL      F:_AppCmd_HexStrToNum  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
// 3315 
// 3316     if (res == R_RESULT_SUCCESS)
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+64
        MOV       [SP], A            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_126  ;; 4 cycles
        ; ------------------------------------- Block: 73 cycles
// 3317     {
// 3318         /* Obtain Schedule Information */
// 3319         r_ie_wp_serialized_sched_t* serializedSched = (r_ie_wp_serialized_sched_t*)AppCmdBuf;
// 3320         int16_t parseResult = R_RESULT_SUCCESS;
// 3321         if (paramType == APP_CMD_ParamType_BroadcastSchedule)
        MOV       A, [SP+0x36]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        MOVW      BC, #0xC5B         ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdBuf)  ;; 1 cycle
        MOV       X, #BYTE3(_AppCmdBuf)  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_127  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 3322         {
// 3323             res = R_NWK_GetRequest(R_NWK_nwkSchedule_Broadcast, AppCmdBuf, sizeof(AppCmdBuf));
        MOV       A, #0x91           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 3324             if (res == R_RESULT_SUCCESS)
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_128  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 3325             {
// 3326                 parseResult = R_IE_BroadcastScheduleParse(&schedule, serializedSched->bytes, serializedSched->size, 0);
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOV       ES, #BYTE3(_AppCmdBuf)  ;; 1 cycle
        MOV       X, ES:_AppCmdBuf   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:_AppCmdBuf+1  ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      BC, #LWRD(_AppCmdBuf+2)  ;; 1 cycle
        MOV       X, #BYTE3(_AppCmdBuf)  ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x16          ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_IE_BroadcastScheduleParse
        CALL      F:_R_IE_BroadcastScheduleParse  ;; 3 cycles
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_129  ;; 3 cycles
          CFI CFA SP+64
        ; ------------------------------------- Block: 27 cycles
// 3327             }
// 3328         }
// 3329         else
// 3330         {
// 3331             res = R_NWK_GetRequest(R_NWK_nwkSchedule_Unicast, AppCmdBuf, sizeof(AppCmdBuf));
??R_DHCPV6_VendorOptionRecvCallback_127:
        MOV       A, #0x90           ;; 1 cycle
          CFI FunCall _R_NWK_GetRequest
        CALL      F:_R_NWK_GetRequest  ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
// 3332             if (res == R_RESULT_SUCCESS)
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_130  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 3333             {
// 3334                 parseResult = R_IE_UnicastScheduleParse(&schedule.us, serializedSched->bytes, serializedSched->size, 0);
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOV       ES, #BYTE3(_AppCmdBuf)  ;; 1 cycle
        MOV       X, ES:_AppCmdBuf   ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:_AppCmdBuf+1  ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      BC, #LWRD(_AppCmdBuf+2)  ;; 1 cycle
        MOV       X, #BYTE3(_AppCmdBuf)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_131  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
        CLRB      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??R_DHCPV6_VendorOptionRecvCallback_131:
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_IE_UnicastScheduleParse
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_GETR_0:
        CALL      F:_R_IE_UnicastScheduleParse  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_DHCPV6_VendorOptionRecvCallback_129:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+64
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 3335             }
// 3336         }
// 3337         if (parseResult < 0)
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        BF        A.7, ??R_DHCPV6_VendorOptionRecvCallback_126  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 3338         {
// 3339             res = (r_result_t)-parseResult;  // parse function returns negative r_result_t on error -> negate again
        MOV       A, X               ;; 1 cycle
        XOR       A, #0xFF           ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
// 3340         }
// 3341     }
// 3342 
// 3343     /* Generate confirm */
// 3344     if (paramType == APP_CMD_ParamType_BroadcastSchedule)
??R_DHCPV6_VendorOptionRecvCallback_126:
        MOV       A, [SP+0x36]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_130  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3345     {
// 3346         /* Send Confirm -> BCSCH_SETC */
// 3347         R_Modem_print("BCSCH_GETC %02X %02X ", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_128:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      DE, #LWRD(?_0+674)  ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_132  ;; 3 cycles
          CFI CFA SP+64
        ; ------------------------------------- Block: 12 cycles
// 3348     }
// 3349     else
// 3350     {
// 3351         /* Send Confirm -> UCSCH_SETC */
// 3352         R_Modem_print("UCSCH_GETC %02X %02X ", handle, res);
??R_DHCPV6_VendorOptionRecvCallback_130:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      DE, #LWRD(?_0+696)  ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
??R_DHCPV6_VendorOptionRecvCallback_132:
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+64
// 3353     }
// 3354 
// 3355     if (res == R_RESULT_SUCCESS)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_133  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 3356     {
// 3357         if (paramType == APP_CMD_ParamType_BroadcastSchedule)
        MOV       A, [SP+0x36]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_134  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3358         {
// 3359             /* Broadcast Interval */
// 3360             R_Modem_print("%04X", (uint16_t)(schedule.broadcast_interval >> 16));
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x15]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOVW      DE, #LWRD(?_0+718)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3361             R_Modem_print("%04X ", (uint16_t)(schedule.broadcast_interval));
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x15]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3362 
// 3363             /* Broadcast Schedule Identifier */
// 3364             R_Modem_print("%04X ", schedule.broadcast_schedule_identifier);
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x1B]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+64
        ; ------------------------------------- Block: 43 cycles
// 3365         }
// 3366 
// 3367         /* dwell_interval */
// 3368         R_Modem_print("%02X ", schedule.us.dwell_interval);
??R_DHCPV6_VendorOptionRecvCallback_134:
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3369 
// 3370         /* clock_drift */
// 3371         R_Modem_print("%02X ", schedule.us.clock_drift);
        MOV       A, [SP+0x1B]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3372 
// 3373         /* timing_accuracy */
// 3374         R_Modem_print("%02X ", schedule.us.timing_accuracy);
        MOV       A, [SP+0x1E]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+70
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3375 
// 3376         /* channel_plan */
// 3377         R_Modem_print("%02X ", schedule.us.channel_plan);
        MOV       A, [SP+0x21]       ;; 1 cycle
        AND       A, #0x7            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+72
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3378 
// 3379         /* channel_function */
// 3380         R_Modem_print("%02X ", schedule.us.channel_function);
        MOV       A, [SP+0x23]       ;; 1 cycle
        SHR       A, 0x3             ;; 1 cycle
        AND       A, #0x7            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+74
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3381 
// 3382         /* excluded_channel_control */
// 3383         R_Modem_print("%02X ", schedule.us.excluded_channel_control);
        MOV       A, [SP+0x25]       ;; 1 cycle
        SHR       A, 0x6             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+76
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3384 
// 3385         /* regulatory_domain */
// 3386         R_Modem_print("%02X ", schedule.us.regulatory_domain);
        MOV       A, [SP+0x28]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+78
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3387 
// 3388         /* operating_class */
// 3389         R_Modem_print("%02X ", schedule.us.operating_class);
        MOV       A, [SP+0x2B]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+80
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3390 
// 3391         /* ch0 */
// 3392         R_Modem_print("%04X", (uint16_t)(schedule.us.ch0 >> 16));
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x31]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+82
        MOVW      DE, #LWRD(?_0+718)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3393         R_Modem_print("%04X ", (uint16_t)(schedule.us.ch0));
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x31]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+84
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3394 
// 3395         /* channel_spacing */
// 3396         R_Modem_print("%02X ", schedule.us.channel_spacing);
        MOV       A, [SP+0x36]       ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+86
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3397 
// 3398         /* no_of_channels */
// 3399         R_Modem_print("%02X ", schedule.us.no_of_channels);
        MOV       A, [SP+0x39]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x3A]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+88
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3400 
// 3401         /* fixed_channel */
// 3402         R_Modem_print("%02X ", schedule.us.fixed_channel);
        MOV       A, [SP+0x3D]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOV       A, [SP+0x3E]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+90
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3403 
// 3404         if (schedule.us.excluded_channel_control == R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_RANGE)
        MOV       A, [SP+0x35]       ;; 1 cycle
        AND       A, #0xC0           ;; 1 cycle
        ADDW      SP, #0x1A          ;; 1 cycle
          CFI CFA SP+64
        CMP       A, #0x40           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_135  ;; 4 cycles
        ; ------------------------------------- Block: 150 cycles
// 3405         {
// 3406             /* no_of_excluded_ranges */
// 3407             R_Modem_print("%02X ", schedule.us.no_of_excluded_ranges);
        MOV       A, [SP+0x2C]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3408 
// 3409             for (uint8_t i = 0; i < schedule.us.no_of_excluded_ranges; i++)
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 10 cycles
??AppCmd_ProcessCmd_SCHEDULE_GETR_1:
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+64
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x2C]       ;; 1 cycle
        CMP       X, A               ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_133  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 3410             {
// 3411                 /* Start Channel Number */
// 3412                 R_Modem_print("%02X ", schedule.us.excluded_ranges.ranges[i].start);
        MOV       A, [SP+0x2D]       ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOV       A, [SP+0x2E]       ;; 1 cycle
        MOV       [SP+0x0F], A       ;; 1 cycle
        MOV       A, [SP+0x2F]       ;; 1 cycle
        MOV       [SP+0x10], A       ;; 1 cycle
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       [SP+0x11], A       ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3413 
// 3414                 /* End Channel Number */
// 3415                 R_Modem_print("%02X ", schedule.us.excluded_ranges.ranges[i].end);
        MOV       A, [SP+0x2F]       ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        MOV       A, [SP+0x30]       ;; 1 cycle
        MOV       [SP+0x0D], A       ;; 1 cycle
        MOV       A, [SP+0x31]       ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOV       A, [SP+0x32]       ;; 1 cycle
        MOV       [SP+0x0F], A       ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+64
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x3           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3416             }
        MOV       A, [SP+0x02]       ;; 1 cycle
        INC       A                  ;; 1 cycle
        BR        R:??AppCmd_ProcessCmd_SCHEDULE_GETR_1  ;; 3 cycles
          CFI CFA SP+64
        ; ------------------------------------- Block: 85 cycles
// 3417         }
// 3418         else if (schedule.us.excluded_channel_control == R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_MASK)
??R_DHCPV6_VendorOptionRecvCallback_135:
        CMP       A, #0x80           ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_133  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3419         {
// 3420             /* excluded_channel_mask */
// 3421             for (uint8_t i = 0; i < R_IE_ScheduleNumberOfMaskBytes(&schedule.us); i++)
        CLRB      A                  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOV       ES, #0xF           ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
        MOV       ES, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??AppCmd_ProcessCmd_SCHEDULE_GETR_2:
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+66
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+66
        POP       BC                 ;; 1 cycle
          CFI CFA SP+64
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+66
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+64
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 13 cycles
??AppCmd_ProcessCmd_SCHEDULE_GETR_3:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_IE_ScheduleNumberOfMaskBytes
        CALL      F:_R_IE_ScheduleNumberOfMaskBytes  ;; 3 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall ?SI_CMP_L02
        CALL      N:?SI_CMP_L02      ;; 3 cycles
        BNC       ??R_DHCPV6_VendorOptionRecvCallback_133  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
// 3422             {
// 3423                 R_Modem_print("%02X ", schedule.us.excluded_channel_mask[i]);
        MOV       A, [SP+0x31]       ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOV       A, [SP+0x32]       ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
        MOV       A, [SP+0x33]       ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOV       A, [SP+0x34]       ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOVW      DE, #LWRD(?_0+154)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3424             }
        MOV       A, [SP+0x02]       ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+64
        BR        S:??AppCmd_ProcessCmd_SCHEDULE_GETR_3  ;; 3 cycles
        ; ------------------------------------- Block: 32 cycles
// 3425         }
// 3426     }
// 3427     R_Modem_print("\n");
??R_DHCPV6_VendorOptionRecvCallback_133:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3428 }
        ADDW      SP, #0x3C          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock64
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 604 cycles

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_0:
        DATA32
        DD 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DATA8
        DB 0
        DB 171, 205, 239, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0
        DB "IPFI %02X %02X "
        DB "RCVI %02X %02X "
        DB "JSI %02X %02X\012"
        DB 0
        DB "STARTC %02X %02X "
        DB "%02X "
        DB "%04X "
        DB " "
        DB "\012"
        DB "PIB_GETC %02X %02X"
        DB 0
        DB " %04X "
        DB 0
        DB "%02X"
        DB 0
        DB "PIB_SETC %02X %02X"
        DB 0
        DB "GTKS_GETC %02X %02X"
        DB " %02x"
        DB "LGTKS_GETC %02X %02X"
        DB 0
        DB "KEYLIFETIMES_SETC %02X %02X"
        DB "VERBOSE_SETC %02X %02X "
        DB "LOGVERSION_GETC %02X %02X "
        DB 0
        DB "LOGENTRIES_GETC %02X %02X "
        DB 0
        DB "VERSION_GETC %02X %02X %s %04X "
        DB "STATUS_GETC %02X %02X "
        DB 0
        DB "TASKSTATUS_GETC %02X %02X "
        DB 0
        DB "%08x %04x "
        DB 0
        DB "ROUTELST_GETC %02X %02X 01 "
        DB " FFFFFFFF "
        DB 0
        DB "ROUTELST_GETC %02X %02X 00\012"
        DB "RPLINFO_GETC %02X %02X"
        DB 0
        DB " %02X "
        DB 0
        DB " %02X %02X %02X %02X %02X"
        DB "NDCACHE_GETC %02X %02X "
        DB "DEVCONF_GETC %02X %02X "
        DB "PHYFAN_GETC %02X %02X "
        DB 0
        DB "BCSCH_GETC %02X %02X "
        DB "UCSCH_GETC %02X %02X "
        DB "%04X"
        DB 0
        DB "%04X %04X "
        DB 0
        DB " %04X %02X\012"
        DB "%02X %04X %04X "
_FAN_STACK_VERSION:
        DB "Wi-SUN-FAN V1.4.1-TPS1.1-Alpha"
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_RSTR:
        DB "RSTR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_STARTR:
        DB "STARTR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_IPSR:
        DB "IPSR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_UDPSR:
        DB "UDPSR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_ICMPSR:
        DB "ICMPSR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_SPDR:
        DB "SPDR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_RSMR:
        DB "RSMR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_REVOKE_KEYS:
        DB "REVOKEKEYSR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_REVOKE_SUPP:
        DB "REVOKESUPPR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_DEVICEKICKR:
        DB "DEVICEKICKR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_LEAVE_NETWORKR:
        DB "LEAVENETWORKR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_PIB_GETR:
        DB "PIB_GETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_PIB_SETR:
        DB "PIB_SETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_IPV6PREFIX_SETR:
        DB "IPV6PREFIX_SETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_DEVCONF_GETR:
        DB "DEVCONF_GETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_DEVCONF_SETR:
        DB "DEVCONF_SETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_PHYFAN_GETR:
        DB "PHYFAN_GETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_PHYFAN_SETR:
        DB "PHYFAN_SETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_PHYNET_GETR:
        DB "PHYNET_GETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_PHYNET_SETR:
        DB "PHYNET_SETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_UCSCH_GETR:
        DB "UCSCH_GETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_UCSCH_SETR:
        DB "UCSCH_SETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_BCSCH_GETR:
        DB "BCSCH_GETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_BCSCH_SETR:
        DB "BCSCH_SETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_WHTLST_GETR:
        DB "WHTLST_GETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_WHTLST_SETR:
        DB "WHTLST_SETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_GTKS_GETR:
        DB "GTKS_GETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_GTKS_SETR:
        DB "GTKS_SETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_LGTKS_GETR:
        DB "LGTKS_GETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_LGTKS_SETR:
        DB "LGTKS_SETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_KEYLIFETIMES_SETR:
        DB "KEYLIFETIMES_SETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_CERT_SWITCHR:
        DB "CERT_SWITCHR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_VERBOSE_SETR:
        DB "VERBOSE_SETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_LOGVERSION_GETR:
        DB "LOGVERSION_GETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_LOGENTRIES_GETR:
        DB "LOGENTRIES_GETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_SUBSCP_SETR:
        DB "SUBSCP_SETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_PAN_VERSION_INCR:
        DB "PAN_VERSION_INCR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_RPL_ROUTES_REFRESHR:
        DB "RPL_ROUTES_REFRESHR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_RPL_GLOBALREPAIRR:
        DB "RPL_GLOBALREPAIRR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_ROUTELST_GETR:
        DB "ROUTELST_GETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_RPLINFO_GETR:
        DB "RPLINFO_GETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_NDCACHE_GETR:
        DB "NDCACHE_GETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_VERSION_GETR:
        DB "VERSION_GETR"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_STATUS_GETR:
        DB "STATUS_GETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdStr_TASKSTATUS_GETR:
        DB "TASKSTATUS_GETR"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
_AppCmdFunc:
        DATA32
        DD _AppCmdStr_RSTR, _AppCmd_ProcessCmd_RSTR, _AppCmdStr_STARTR
        DD _AppCmd_ProcessCmd_STARTR, _AppCmdStr_IPSR, _AppCmd_ProcessCmd_IPSR
        DD _AppCmdStr_UDPSR, _AppCmd_ProcessCmd_UDPSR, _AppCmdStr_ICMPSR
        DD _AppCmd_ProcessCmd_ICMPSR, _AppCmdStr_SPDR, _AppCmd_ProcessCmd_SPDR
        DD _AppCmdStr_RSMR, _AppCmd_ProcessCmd_RSMR, _AppCmdStr_REVOKE_KEYS
        DD _AppCmd_ProcessCmd_REVOKEKEYSR, _AppCmdStr_REVOKE_SUPP
        DD _AppCmd_ProcessCmd_REVOKESUPPR, _AppCmdStr_DEVICEKICKR
        DD _AppCmd_ProcessCmd_DEVICEKICKR, _AppCmdStr_LEAVE_NETWORKR
        DD _AppCmd_ProcessCmd_LEAVENETWORKR, _AppCmdStr_PIB_GETR
        DD _AppCmd_ProcessCmd_PIB_GETR, _AppCmdStr_PIB_SETR
        DD _AppCmd_ProcessCmd_PIB_SETR, _AppCmdStr_IPV6PREFIX_SETR
        DD _AppCmd_ProcessCmd_IPV6PREFIX_SETR, _AppCmdStr_DEVCONF_GETR
        DD _AppCmd_ProcessCmd_DEVCONF_GETR, _AppCmdStr_DEVCONF_SETR
        DD _AppCmd_ProcessCmd_DEVCONF_SETR, _AppCmdStr_PHYFAN_GETR
        DD _AppCmd_ProcessCmd_PHYFAN_GETR, _AppCmdStr_PHYFAN_SETR
        DD _AppCmd_ProcessCmd_PHYFAN_SETR, _AppCmdStr_PHYNET_GETR
        DD _AppCmd_ProcessCmd_PHYNET_GETR, _AppCmdStr_PHYNET_SETR
        DD _AppCmd_ProcessCmd_PHYNET_SETR, _AppCmdStr_UCSCH_GETR
        DD _AppCmd_ProcessCmd_UCSCH_GETR, _AppCmdStr_UCSCH_SETR
        DD _AppCmd_ProcessCmd_UCSCH_SETR, _AppCmdStr_BCSCH_GETR
        DD _AppCmd_ProcessCmd_BCSCH_GETR, _AppCmdStr_BCSCH_SETR
        DD _AppCmd_ProcessCmd_BCSCH_SETR, _AppCmdStr_WHTLST_GETR
        DD _AppCmd_ProcessCmd_WHTLST_GETR, _AppCmdStr_WHTLST_SETR
        DD _AppCmd_ProcessCmd_WHTLST_SETR, _AppCmdStr_GTKS_GETR
        DD _AppCmd_ProcessCmd_GTKS_GETR, _AppCmdStr_GTKS_SETR
        DD _AppCmd_ProcessCmd_GTKS_SETR, _AppCmdStr_LGTKS_GETR
        DD _AppCmd_ProcessCmd_LGTKS_GETR, _AppCmdStr_LGTKS_SETR
        DD _AppCmd_ProcessCmd_LGTKS_SETR, _AppCmdStr_KEYLIFETIMES_SETR
        DD _AppCmd_ProcessCmd_KEYLIFETIMES_SETR, _AppCmdStr_CERT_SWITCHR
        DD _AppCmd_ProcessCmd_CERT_SWITCHR, _AppCmdStr_VERBOSE_SETR
        DD _AppCmd_ProcessCmd_VERBOSE_SETR, _AppCmdStr_LOGVERSION_GETR
        DD _AppCmd_ProcessCmd_LOGVERSION_GETR, _AppCmdStr_LOGENTRIES_GETR
        DD _AppCmd_ProcessCmd_LOGENTRIES_GETR, _AppCmdStr_SUBSCP_SETR
        DD _AppCmd_ProcessCmd_SUBSCP_SETR, _AppCmdStr_PAN_VERSION_INCR
        DD _AppCmd_ProcessCmd_PAN_VERSION_INCR, _AppCmdStr_RPL_ROUTES_REFRESHR
        DD _AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR, _AppCmdStr_RPL_GLOBALREPAIRR
        DD _AppCmd_ProcessCmd_RPL_GLOBALREPAIRR, _AppCmdStr_ROUTELST_GETR
        DD _AppCmd_ProcessCmd_ROUTELST_GETR, _AppCmdStr_RPLINFO_GETR
        DD _AppCmd_ProcessCmd_RPLINFO_GETR, _AppCmdStr_NDCACHE_GETR
        DD _AppCmd_ProcessCmd_NDCACHE_GETR, _AppCmdStr_VERSION_GETR
        DD _AppCmd_ProcessCmd_VERSION_GETR, _AppCmdStr_STATUS_GETR
        DD _AppCmd_ProcessCmd_STATUS_GETR, _AppCmdStr_TASKSTATUS_GETR
        DD _AppCmd_ProcessCmd_TASKSTATUS_GETR

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_46:
        DATA16
        DW 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB "WHTLST_GETC %02X %02X"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_48:
        DATA16
        DW 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB "WHTLST_SETC %02X %02X"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_69:
        DATA16
        DW 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DATA8
        DB 0
        DB "BCSCH_SETC %02X %02X\012"
        DB "UCSCH_SETC %02X %02X\012"
// 3429 
// 3430 #ifdef UMM_INFO
// 3431 static void AppCmd_ProcessCmd_MEMMARK_GETR(uint8_t* pCmd)
// 3432 {
// 3433     uint8_t handle;
// 3434     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
// 3435     R_Modem_print("MEMMARK_GETC %02X %02X ", handle, res);
// 3436 
// 3437     for (int heap_id = 0; heap_id < R_HEAP_COUNT; heap_id++)
// 3438     {
// 3439         R_Modem_print("%02X ", heap_id);
// 3440         R_Modem_print("%08X %08X ", umm_high_watermark(UMM_HEAP_ID_A), umm_heap_size(UMM_HEAP_ID_A));
// 3441     }
// 3442     R_Modem_print("\n");
// 3443 }
// 3444 
// 3445 static void AppCmd_ProcessCmd_MEMINFO_GETR(uint8_t* pCmd)
// 3446 {
// 3447     uint8_t handle;
// 3448     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
// 3449     R_Modem_print("MEMINFO_GETC %02X %02X ", handle, res);
// 3450 
// 3451     for (int heap_id = 0; heap_id < R_HEAP_COUNT; heap_id++)
// 3452     {
// 3453         R_Modem_print("%02X ", heap_id);
// 3454         umm_info(UMM_HEAP_ID_A_ NULL, 0);
// 3455         R_Modem_print("%04X %04X %04X ", ummHeapInfo.totalEntries, ummHeapInfo.usedEntries, ummHeapInfo.freeEntries);
// 3456         R_Modem_print("%04X %04X %04X ", ummHeapInfo.totalBlocks, ummHeapInfo.usedBlocks, ummHeapInfo.freeBlocks);
// 3457         R_Modem_print("%04X %04X ", ummHeapInfo.maxFreeContiguousBlocks, ummHeapInfo.maxAllocatedContiguousBlocks);
// 3458     }
// 3459     R_Modem_print("\n");
// 3460 }
// 3461 
// 3462 static void AppCmd_ProcessCmd_MEMMAP_GETR(uint8_t* pCmd)
// 3463 {
// 3464     uint8_t handle;
// 3465     r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
// 3466     R_Modem_print("MEMMAP_GETC %02X %02X ", handle, res);
// 3467 
// 3468     for (int heap_id = 0; heap_id < R_HEAP_COUNT; heap_id++)
// 3469     {
// 3470         R_Modem_print("%02X ", heap_id);
// 3471 
// 3472         umm_info(UMM_HEAP_ID_A_ NULL, 0);
// 3473         unsigned short int blockNo = 0;
// 3474         unsigned short int nextBlockNo = 0;
// 3475         int freeFlag = 0;
// 3476         size_t blockSize = 0;
// 3477 
// 3478         do
// 3479         {
// 3480             nextBlockNo = umm_map(UMM_HEAP_ID_A_ blockNo, &freeFlag, &blockSize);
// 3481             R_Modem_print("%04X %04X %08X %02X ", blockNo, nextBlockNo, blockSize, freeFlag);
// 3482             blockNo = nextBlockNo;
// 3483         }
// 3484         while (blockNo != 0);
// 3485     }
// 3486     R_Modem_print("\n");
// 3487 }
// 3488 #endif // UMM_INFO
// 3489 
// 3490 /********************************************************************************
// 3491 * Function Name     : AppCmd_HexStrToNum
// 3492 * Description       : Convert hex format string to number
// 3493 * Arguments         : ppBuf ... Double pointer buffer
// 3494 *                   : pData ... Pointer to data
// 3495 *                   : size ... Size of string
// 3496 *                   : isOctetStr ...
// 3497 *                   :   R_TRUE: Octet string
// 3498 *                   :   R_TRUE: Big endian format
// 3499 * Return Value      : r_result_t
// 3500 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock65 Using cfiCommon1
          CFI Function _AppCmd_HexStrToNum
        CODE
// 3501 static r_result_t AppCmd_HexStrToNum(uint8_t** ppBuf, void* pData, int16_t dataSize, r_boolean_t isOctetStr)
// 3502 {
_AppCmd_HexStrToNum:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 8
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
// 3503     unsigned char ret;
// 3504     r_result_t res;
// 3505 
// 3506     ret = AppHexStrToNum(ppBuf,  pData, dataSize, isOctetStr);
// 3507     if (0 == ret)
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
          CFI FunCall _AppHexStrToNum
        CALL      F:_AppHexStrToNum  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_DHCPV6_VendorOptionRecvCallback_136  ;; 4 cycles
        ; ------------------------------------- Block: 37 cycles
// 3508     {
// 3509         // TRUE
// 3510         res = R_RESULT_SUCCESS;
        CLRB      A                  ;; 1 cycle
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_137  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3511     }
// 3512     else
// 3513     {
// 3514         // FALSE
// 3515         res = R_RESULT_INVALID_PARAMETER;
??R_DHCPV6_VendorOptionRecvCallback_136:
        MOV       A, #0x11           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 3516     }
// 3517 
// 3518     return res;
??R_DHCPV6_VendorOptionRecvCallback_137:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock65
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 49 cycles
// 3519 }
// 3520 
// 3521 /********************************************************************************
// 3522 * Function Name     : AppPrintBytesAsHexString
// 3523 * Description       : Display bytes as hexadecimal string
// 3524 * Arguments         : const uint8_t *bytes ... Pointer to bytes
// 3525 *                   : size_t numBytes ... number of bytes to print
// 3526 * Return Value      : None
// 3527 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock66 Using cfiCommon2
          CFI Function _AppPrintBytesAsHexString
        CODE
// 3528 static void AppPrintBytesAsHexString(const uint8_t* bytes, size_t numBytes)
// 3529 {
_AppPrintBytesAsHexString:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 3530     for (size_t i = 0; i < numBytes; i++)
        MOVW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_138  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 3531     {
// 3532         R_Modem_print("%02X", bytes[i]);
??AppPrintBytesAsHexString_0:
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(?_0+198)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3533     }
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        CMPW      AX, HL             ;; 1 cycle
        BC        ??AppPrintBytesAsHexString_0  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
// 3534 }
??R_DHCPV6_VendorOptionRecvCallback_138:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock66
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 49 cycles
// 3535 /********************************************************************************
// 3536 * Function Name     : AppPrintIPv6Addr
// 3537 * Description       : Display IPv6 address
// 3538 * Arguments         : const uint8_t *pIPAddr ... Pointer to IPv6 address
// 3539 * Return Value      : None
// 3540 ********************************************************************************/
// 3541 static void AppPrintIPv6Addr(const uint8_t* pIPAddr)
// 3542 {
// 3543     AppPrintBytesAsHexString(pIPAddr, R_IPV6_ADDRESS_LENGTH);
// 3544 }
// 3545 
// 3546 /********************************************************************************
// 3547 * Function Name     : AppPrintMACAddr
// 3548 * Description       : Display MAC address
// 3549 * Arguments         : const uint8_t *pMACAddr ... Pointer to MAC address
// 3550 * Return Value      : None
// 3551 ********************************************************************************/
// 3552 static void AppPrintMACAddr(const uint8_t* pMACAddr)
// 3553 {
// 3554     AppPrintBytesAsHexString(pMACAddr, R_MAC_EXTENDED_ADDRESS_LENGTH);
// 3555 }
// 3556 
// 3557 /********************************************************************************
// 3558 * Function Name     : AppPrintNetworkName
// 3559 * Description       : Display Network Name address
// 3560 * Arguments         : const uint8_t *pNetworkName ... Pointer to Network Name
// 3561 * Return Value      : None
// 3562 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock67 Using cfiCommon1
          CFI Function _AppPrintNetworkName
        CODE
// 3563 static void AppPrintNetworkName(const char* pNetworkName)
// 3564 {
_AppPrintNetworkName:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_139  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3565     while (*pNetworkName)
// 3566     {
// 3567         R_Modem_print("%02X", *pNetworkName++);
??AppPrintNetworkName_0:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, #LWRD(?_0+198)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 18 cycles
// 3568     }
??R_DHCPV6_VendorOptionRecvCallback_139:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        BNZ       ??AppPrintNetworkName_0  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 3569 }
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock67
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 42 cycles
// 3570 
// 3571 /* For Network Rack testing reply original UDP packet
// 3572  * back to it's source within random time between 0-15 seconds */
// 3573 
// 3574 /********************************************************************************
// 3575 * Function Name     : AppPrintUdpDataInd
// 3576 * Description       : Display udp_ind message
// 3577 * Arguments         : r_udp_data_ind_t *p_udp_ind ... Pointer to message
// 3578 * Return Value      : None
// 3579 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock68 Using cfiCommon1
          CFI Function _AppPrintUdpDataInd
        CODE
// 3580 static void AppPrintUdpDataInd(r_udp_data_ind_t* p_udp_ind)
// 3581 {
_AppPrintUdpDataInd:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3582     /* (Protocol) */
// 3583     R_Modem_print("%04X ", R_IPV6_PROTOCOL_ID_UDP);
        MOVW      AX, #0x11          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3584 
// 3585     /* (DstIPv6Addr) */
// 3586     AppPrintIPv6Addr(p_udp_ind->dstAddress);
        MOVW      BC, #0x10          ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 3587     R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3588 
// 3589     /* (SrcIPv6Addr) */
// 3590     AppPrintIPv6Addr(p_udp_ind->srcAddress);
        MOVW      BC, #0x10          ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 3591     R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3592 
// 3593     /* (DstPort),(SrcPort) */
// 3594     R_Modem_print("%04X %04X ", p_udp_ind->dstPort, p_udp_ind->srcPort);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x23          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x11          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(?_0+724)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3595 
// 3596     /* (PayloadSize),(Payload) */
// 3597     R_Modem_print("%04X ", p_udp_ind->dataLength);
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x25          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3598     AppPrintBytesAsHexString(p_udp_ind->data, p_udp_ind->dataLength);
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x25          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x29          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 3599 
// 3600     /* (Status), (LQI) */
// 3601     R_Modem_print(" %04X %02X\n", p_udp_ind->status, p_udp_ind->lqi);
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x27          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, #LWRD(?_0+736)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
        BR        F:?Subroutine4     ;; 3 cycles
          CFI EndBlock cfiBlock68
        ; ------------------------------------- Block: 161 cycles
        ; ------------------------------------- Total: 161 cycles
// 3602 
// 3603 #ifdef R_NETWORK_RACK
// 3604     /* For Network Rack testing reply original UDP packet
// 3605      * back to it's source within random time between 0-15 seconds */
// 3606     clock_time_t delay = (random_rand() % R_RAND_MAX_VAL) * CLOCK_SECOND;
// 3607     R_Modem_print("TEST: Sending UDP reply in %lu ticks\n", delay);
// 3608     R_OS_DelayTaskMs(delay);
// 3609 
// 3610     R_UDP_DataRequest(p_udp_ind->srcAddress,
// 3611                       p_udp_ind->srcPort,
// 3612                       p_udp_ind->dstPort,
// 3613                       p_udp_ind->data,
// 3614                       p_udp_ind->dataLength, R_OPTIONS_NON_BLOCKING);
// 3615 #endif
// 3616 }
// 3617 
// 3618 /********************************************************************************
// 3619 * Function Name     : AppPrintIcmpEchoInd
// 3620 * Description       : Display echo_ind message
// 3621 * Arguments         : r_icmp_echo_reply_ind_t *p_icmp_echo_ind ... Pointer to message
// 3622 * Return Value      : None
// 3623 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock69 Using cfiCommon1
          CFI Function _AppPrintIcmpEchoInd
        CODE
// 3624 static void AppPrintIcmpEchoInd(r_icmp_echo_reply_ind_t* p_icmp_echo_ind)
// 3625 {
_AppPrintIcmpEchoInd:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 3626     /* (Protocol) */
// 3627     R_Modem_print("%04X ", R_IPV6_PROTOCOL_ID_ICMPV6);
        MOVW      AX, #0x3A          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3628 
// 3629     /* (DstIPv6Addr) */
// 3630     AppPrintIPv6Addr(p_icmp_echo_ind->dstAddress);
        MOVW      BC, #0x10          ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 3631     R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3632 
// 3633     /* (SrcIPv6Addr) */
// 3634     AppPrintIPv6Addr(p_icmp_echo_ind->srcAddress);
        MOVW      BC, #0x10          ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 3635     R_Modem_print(" ");
        MOVW      DE, #LWRD(?_0+166)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3636 
// 3637     /* (Type), (Identifier), (Sequence), PayloadSize */
// 3638     R_Modem_print("%02X %04X %04X ",
// 3639                   R_ICMP_TYPE_ECHO_REPLY,
// 3640                   p_icmp_echo_ind->identifier, p_icmp_echo_ind->sequence);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x23          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x20          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        DECW      DE                 ;; 1 cycle
        DECW      DE                 ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, #0x81          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      DE, #LWRD(?_0+748)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3641 
// 3642     /* (PayloadSize), (Payload) */
// 3643     R_Modem_print("%04X ", p_icmp_echo_ind->dataLength);
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x25          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      DE, #LWRD(?_0+160)  ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
// 3644     AppPrintBytesAsHexString(p_icmp_echo_ind->data, p_icmp_echo_ind->dataLength);
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x25          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x29          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppPrintBytesAsHexString
        CALL      F:_AppPrintBytesAsHexString  ;; 3 cycles
// 3645 
// 3646     /* (Status), (LQI) */
// 3647     R_Modem_print(" %04X %02X\n", p_icmp_echo_ind->status, p_icmp_echo_ind->lqi);
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x27          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      DE, #LWRD(?_0+736)  ;; 1 cycle
        BR        F:??Subroutine14_0  ;; 3 cycles
          CFI EndBlock cfiBlock69
        ; ------------------------------------- Block: 161 cycles
        ; ------------------------------------- Total: 161 cycles
// 3648 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock70 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+22
        CODE
?Subroutine1:
        MOVW      DE, #LWRD(?_0+168)  ;; 1 cycle
          CFI EndBlock cfiBlock70
        ; ------------------------------------- Block: 1 cycles
        ; ------------------------------------- Total: 1 cycles
        REQUIRE ??Subroutine14_0
        ; // Fall through to label ??Subroutine14_0

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock71 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+22
        CODE
??Subroutine14_0:
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI EndBlock cfiBlock71
        ; ------------------------------------- Block: 1 cycles
        ; ------------------------------------- Total: 1 cycles
        REQUIRE ??Subroutine15_0
        ; // Fall through to label ??Subroutine15_0

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock72 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+22
          CFI FunCall _AppCmd_ProcessCmd_RSMR _R_Modem_print
          CFI FunCall _AppPrintIcmpEchoInd _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_LGTKS_GETR _R_Modem_print
          CFI FunCall _AppCmd_ProcessCmd_PHYFAN_GETR _R_Modem_print
        CODE
??Subroutine15_0:
        CALL      F:_R_Modem_print   ;; 3 cycles
        ADDW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock72
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 3649 
// 3650 #if R_DEV_TBU_ENABLED && __RX
// 3651 /********************************************************************************
// 3652 * Function Name     : AppPrintSubScriptFrameInd
// 3653 * Description       : Display subscription frame
// 3654 * Arguments         : r_subscription_frame_ind_t *p_subscript_frame_ind ... Pointer to frame
// 3655 * Return Value      : None
// 3656 ********************************************************************************/
// 3657 static void AppPrintSubScriptFrameInd(const r_subscription_frame_ind_t* p_subscript_frame_ind)
// 3658 {
// 3659     R_Modem_print("%04X ", p_subscript_frame_ind->frameLength);
// 3660     AppPrintBytesAsHexString(p_subscript_frame_ind->frame, p_subscript_frame_ind->frameLength);
// 3661     R_Modem_print("\n");
// 3662 }
// 3663 #endif /* R_DEV_TBU_ENABLED && __RX */
// 3664 
// 3665 /********************************************************************************
// 3666 * Function Name     : AppPrintIpDataIndication
// 3667 * Description       : Display IP packet
// 3668 * Arguments         : r_ip_data_ind_t *p_ip_ind ... Pointer to IP indication
// 3669 * Return Value      : None
// 3670 ********************************************************************************/
// 3671 static void AppPrintIpDataIndication(const r_ip_data_ind_t* p_indication)
// 3672 {
// 3673     R_Modem_print("%04X ", p_indication->packetLength);
// 3674     AppPrintBytesAsHexString(p_indication->packet, p_indication->packetLength);
// 3675     R_Modem_print("\n");
// 3676 }
// 3677 
// 3678 /** This is just a placeholder; A valid Vendor-specific enterprise number (registered with IANA) must be used! */
// 3679 #define DHCPV6_VENDOR_OPT_EXAMPLE_ENTERPRISE_NUMBER    0xDEADBEEF
// 3680 #define DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FEATURES 0xACDC
// 3681 #define DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FILLER   0xCAFE
// 3682 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock73 Using cfiCommon1
          CFI Function _R_DHCPV6_VendorOptionSendCallback
        CODE
// 3683 uint16_t R_DHCPV6_VendorOptionSendCallback(uint32_t* enterprise_number, uint8_t* option_data, uint16_t max_option_data_size)
// 3684 {
_R_DHCPV6_VendorOptionSendCallback:
        ; * Stack frame (at entry) *
        ; Param size: 2
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 12
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
// 3685     *enterprise_number = DHCPV6_VENDOR_OPT_EXAMPLE_ENTERPRISE_NUMBER;
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, #0xBEEF        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, #0xDEAD        ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3686 
// 3687     /*
// 3688      * The encapsulated vendor-specific options field MUST be encoded as a sequence of code/length/value fields of
// 3689      * identical format to the DHCP options field.
// 3690      */
// 3691     uint16_t pos = 0;
// 3692 
// 3693     /* Write 2-byte option containing our feature mask */
// 3694     UInt16ToArr(DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FEATURES, &option_data[pos]); // option code
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        POP       DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0xACDC        ;; 1 cycle
          CFI FunCall _UInt16ToArr
        CALL      F:_UInt16ToArr     ;; 3 cycles
// 3695     pos += 2;
// 3696     UInt16ToArr(2, &option_data[pos]);                                              // option length
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
          CFI FunCall _UInt16ToArr
        CALL      F:_UInt16ToArr     ;; 3 cycles
// 3697     pos += 2;
// 3698     UInt16ToArr(featureMask, &option_data[pos]);                                    // option data
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES, #BYTE3(_featureMask)  ;; 1 cycle
        MOVW      AX, ES:_featureMask  ;; 2 cycles
          CFI FunCall _UInt16ToArr
        CALL      F:_UInt16ToArr     ;; 3 cycles
// 3699     pos += 2;
// 3700 
// 3701     /* Write another (useless) option with all bytes set to 0xFF to demonstrate usage of max supported option size */
// 3702     UInt16ToArr(DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FILLER, &option_data[pos]);  // option code
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0xCAFE        ;; 1 cycle
          CFI FunCall _UInt16ToArr
        CALL      F:_UInt16ToArr     ;; 3 cycles
// 3703     pos += 2;
// 3704     uint16_t remainingSize = max_option_data_size - pos - 2;
// 3705     UInt16ToArr(remainingSize, &option_data[pos]);  // option length
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0xFFF6        ;; 1 cycle
          CFI FunCall _UInt16ToArr
        CALL      F:_UInt16ToArr     ;; 3 cycles
// 3706     pos += 2;
        MOVW      AX, #0xA           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        CMPW      AX, #0xB           ;; 1 cycle
        BC        ??R_DHCPV6_VendorOptionRecvCallback_140  ;; 4 cycles
        ; ------------------------------------- Block: 81 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      DE, #0xA           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 3707     while (pos < max_option_data_size)
// 3708     {
// 3709         option_data[pos++] = 0xFF;  // option data
??R_DHCPV6_VendorOptionSendCallback_0:
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xFF           ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
// 3710     }
        INCW      AX                 ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        BC        ??R_DHCPV6_VendorOptionSendCallback_0  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 3711 
// 3712 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
// 3713     r_loggen_3692(pos);
??R_DHCPV6_VendorOptionRecvCallback_140:
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0x2           ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0xB           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_141  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 11 cycles
??R_DHCPV6_VendorOptionRecvCallback_141:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
// 3714 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
// 3715     return pos;
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock73
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 138 cycles
// 3716 }
// 3717 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock74 Using cfiCommon1
          CFI Function _R_DHCPV6_VendorOptionRecvCallback
        CODE
// 3718 void R_DHCPV6_VendorOptionRecvCallback(const r_eui64_t* src, uint32_t enterprise_number,
// 3719                                        const uint8_t* option_data, uint16_t option_len)
// 3720 {
_R_DHCPV6_VendorOptionRecvCallback:
        ; * Stack frame (at entry) *
        ; Param size: 6
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 26
        SUBW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+30
// 3721 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
// 3722     r_loggen_3699(option_len, src->bytes);
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0xA           ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0xC           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_142  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        MOVW      BC, #0x8           ;; 1 cycle
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 18 cycles
??R_DHCPV6_VendorOptionRecvCallback_142:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
// 3723 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
// 3724 
// 3725     /* Validate enterprise number */
// 3726     if (enterprise_number != DHCPV6_VENDOR_OPT_EXAMPLE_ENTERPRISE_NUMBER)
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0xDEAD        ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 10 cycles
        CMPW      AX, #0xBEEF        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??R_DHCPV6_VendorOptionRecvCallback_143:
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_144  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 3727     {
// 3728 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
// 3729         r_loggen_3704(enterprise_number);
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0x4           ;; 1 cycle
        MOV       C, #0x2            ;; 1 cycle
        MOVW      AX, #0xD           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_145  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      BC, #0x4           ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
??R_DHCPV6_VendorOptionRecvCallback_145:
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_146  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3730 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
// 3731         return;
// 3732     }
// 3733 
// 3734     /* Process option */
// 3735     uint16_t pos = 0;
??R_DHCPV6_VendorOptionRecvCallback_144:
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x12], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+30
        BR        S:??R_DHCPV6_VendorOptionRecvCallback_147  ;; 3 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 35 cycles
// 3736     while ((option_len - pos) >= 4)
// 3737     {
// 3738         uint16_t code = ArrToUInt16(&option_data[pos]);
// 3739         pos += 2;
// 3740         uint16_t current_option_len = ArrToUInt16(&option_data[pos]);
// 3741         pos += 2;
// 3742         if (current_option_len > (option_len - pos))
// 3743         {
// 3744 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
// 3745             r_loggen_3718(code, option_len - pos, current_option_len);
// 3746 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
// 3747             pos = option_len;
// 3748             break;
// 3749         }
// 3750         switch (code)
// 3751         {
// 3752             case DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FEATURES:
// 3753             {
// 3754                 if (current_option_len == 2)
// 3755                 {
// 3756                     uint16_t remoteFeatureMask = ArrToUInt16(&option_data[pos]);
// 3757                     LOG_ONLY_VAR(remoteFeatureMask);
// 3758 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
// 3759                     r_loggen_3730(remoteFeatureMask);
// 3760 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
// 3761                 }
// 3762                 else
// 3763                 {
// 3764 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
// 3765                     r_loggen_3734();
??R_DHCPV6_VendorOptionRecvCallback_148:
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x2            ;; 1 cycle
        MOV       X, #0x10           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 12 cycles
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
??R_DHCPV6_VendorOptionRecvCallback_149:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
// 3766 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
// 3767                 }
// 3768                 pos += current_option_len;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
??R_DHCPV6_VendorOptionRecvCallback_147:
        MOVW      [SP], AX           ;; 1 cycle
// 3769                 break;
        ; ------------------------------------- Block: 1 cycles
??R_DHCPV6_VendorOptionRecvCallback_150:
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x4           ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_151  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        POP       DE                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _ArrToUInt16
        CALL      F:_ArrToUInt16     ;; 3 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _ArrToUInt16
        CALL      F:_ArrToUInt16     ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        BH        ??R_DHCPV6_VendorOptionRecvCallback_152  ;; 4 cycles
        ; ------------------------------------- Block: 39 cycles
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        SUBW      AX, #0xACDC        ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_153  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        SUBW      AX, #0x1E22        ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_154  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3770             }
// 3771 
// 3772             case DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FILLER:
// 3773             {
// 3774 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
// 3775                 r_loggen_3742(current_option_len);
// 3776 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
// 3777                 pos += current_option_len;
// 3778                 break;
// 3779             }
// 3780 
// 3781             default:
// 3782             {
// 3783 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
// 3784                 r_loggen_3749(code);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0x2           ;; 1 cycle
        MOV       C, #0x2            ;; 1 cycle
        MOVW      AX, #0x12          ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_155  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 10 cycles
??R_DHCPV6_VendorOptionRecvCallback_155:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
// 3785 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
// 3786                 break;
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_150  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3787             }
??R_DHCPV6_VendorOptionRecvCallback_152:
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0x6           ;; 1 cycle
        MOV       C, #0x2            ;; 1 cycle
        MOVW      AX, #0xE           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_156  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 27 cycles
??R_DHCPV6_VendorOptionRecvCallback_156:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
// 3788         }
// 3789     }
// 3790 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
// 3791     r_loggen_3754(pos, src->bytes);
??R_DHCPV6_VendorOptionRecvCallback_157:
        MOVW      [SP], AX           ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0xA           ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0x13          ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_146  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        MOVW      BC, #0x8           ;; 1 cycle
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 18 cycles
??R_DHCPV6_VendorOptionRecvCallback_146:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
// 3792 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
// 3793 }
        ADDW      SP, #0x1A          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+30
        ; ------------------------------------- Block: 10 cycles
??R_DHCPV6_VendorOptionRecvCallback_153:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        CMPW      AX, #0x2           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_148  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _ArrToUInt16
        CALL      F:_ArrToUInt16     ;; 3 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0x2           ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0xF           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_158  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
??R_DHCPV6_VendorOptionRecvCallback_158:
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_149  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_DHCPV6_VendorOptionRecvCallback_154:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0x2           ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0x11          ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_DHCPV6_VendorOptionRecvCallback_159  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      BC, #0x2           ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
??R_DHCPV6_VendorOptionRecvCallback_159:
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_149  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_DHCPV6_VendorOptionRecvCallback_151:
        MOVW      AX, HL             ;; 1 cycle
        BR        R:??R_DHCPV6_VendorOptionRecvCallback_157  ;; 3 cycles
          CFI EndBlock cfiBlock74
        ; ------------------------------------- Block: 4 cycles
        ; ------------------------------------- Total: 409 cycles

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_44:
        DATA8
        DB 32, 23, 0, 0, 0, 0, 0, 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_45:
        DB "RSTC %02X %02X\012"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_50:
        DB "GTKS_SETC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_51:
        DB "LGTKS_SETC %02X %02X\012"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_52:
        DB "CERT_SWITCHC %02X %02X\012"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_53:
        DB "SUBSCP_SETC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_54:
        DB "RPL_GLOBALREPAIRC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_55:
        DB "PAN_VERSION_INCC %02X %02X\012"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_56:
        DB "RPL_ROUTES_REFRESHC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_57:
        DB "IPSC %02X %02X\012"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_58:
        DB "UDPSC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_59:
        DB "ICMPSC %02X %02X\012"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_60:
        DB "SPDC %02X %02X\012"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_61:
        DB "RSMC %02X %02X\012"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_62:
        DB "REVOKEKEYSC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_63:
        DB "REVOKESUPPC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_64:
        DB "DEVICEKICKC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_65:
        DB "LEAVENETWORKC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_66:
        DB "IPV6PREFIX_SETC %02X %02X\012"
        DATA8
        DB 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_67:
        DB "DEVCONF_SETC %02X %02X\012"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_68:
        DB "PHYFAN_SETC %02X %02X\012"
        DATA8
        DB 0

        END
// 3794 
// 3795 #ifdef R_DEV_AUTO_START
// 3796 /********************************************************************************
// 3797 * Function Name     : AppCmd_AutoStart
// 3798 * Description       : Automatic device configuration and device start
// 3799 * Arguments         : None
// 3800 * Return Value      : None
// 3801 ********************************************************************************/
// 3802 void AppCmd_AutoStart(void)
// 3803 {
// 3804     r_result_t res = R_RESULT_SUCCESS;
// 3805     r_app_config_t* p_config = &AppCmdConfig;
// 3806     r_ie_wp_broadcast_schedule_t schedule = {0};
// 3807     r_app_schedule_buffer_t sched_buf = {{ 0 }};
// 3808 
// 3809     uint8_t rf_phyFreqBandId;
// 3810     uint8_t rf_phyFSKOpeMode;
// 3811     uint8_t rf_phyChanConv;
// 3812     uint16_t rf_phyCcaDuration;
// 3813     uint8_t phyCurrentChannel;
// 3814 
// 3815     uint8_t handle = 0;
// 3816     uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH];
// 3817     uint8_t cnt;
// 3818 
// 3819 
// 3820     /***************************************************************************
// 3821     *
// 3822     * Start: Change desired AutoStart device configuration from here
// 3823     *
// 3824     ***************************************************************************/
// 3825 
// 3826     /***************************************************************************
// 3827     * Device Configuration
// 3828     ***************************************************************************/
// 3829     uint8_t networkName[] = "WiSUN PAN";
// 3830     uint8_t deviceType = R_ROUTERNODE;
// 3831     uint16_t panId = 0xffff;
// 3832     uint16_t panSize = 0;
// 3833     uint16_t sixLowPanMTU = 1280u;
// 3834     uint8_t useParent_BS_IE = 1;
// 3835     uint8_t demo_mode = 0;
// 3836 
// 3837     /***************************************************************************
// 3838     * PHY Configuration
// 3839     ***************************************************************************/
// 3840     uint8_t phy_txPower = 47u;
// 3841     uint8_t phy_RegulatoryMode = 0;
// 3842     uint8_t phy_AntennaSelectTx = 1;  //  0 = TX and RX via connector J2; 1 = TX and RX via connector;
// 3843 
// 3844 #if R_DEV_FREQUENCY_HOPPING
// 3845 
// 3846     uint8_t wisun_regulatory_domain = R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_NORTH_AMERICA;
// 3847     uint8_t wisun_operating_class = R_OPERATING_CLASS_2;
// 3848     uint8_t wisun_operating_mode = R_OPERATING_MODE_3;
// 3849 
// 3850     /***************************************************************************
// 3851     * Unicast and Broadcast Schedule Configuration
// 3852     ***************************************************************************/
// 3853     schedule.broadcast_interval = 1020u;
// 3854     schedule.broadcast_schedule_identifier = 1u;
// 3855 
// 3856     schedule.us.dwell_interval = 255u;
// 3857     schedule.us.clock_drift = 255u;
// 3858     schedule.us.timing_accuracy = 0u;
// 3859     schedule.us.channel_plan = R_IE_WP_SCHEDULE_CHANNEL_PLAN_INDIRECT;
// 3860     schedule.us.channel_function = R_IE_WP_SCHEDULE_CHANNEL_FUNCTION_DIRECT_HASH;
// 3861     schedule.us.excluded_channel_control = R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_NONE;
// 3862     schedule.us.regulatory_domain = wisun_regulatory_domain;
// 3863     schedule.us.operating_class = wisun_operating_class;
// 3864     schedule.us.ch0 = 902400u;
// 3865     schedule.us.channel_spacing = R_IE_WP_SCHEDULE_CHANNEL_SPACING_400;
// 3866     schedule.us.no_of_channels = 64u;
// 3867     schedule.us.fixed_channel = 0u;
// 3868 
// 3869 #else /* R_DEV_FREQUENCY_HOPPING */
// 3870 
// 3871     uint8_t wisun_regulatory_domain = R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_NORTH_AMERICA;
// 3872     uint8_t wisun_operating_class = R_OPERATING_CLASS_1;
// 3873     uint8_t wisun_operating_mode = R_OPERATING_MODE_1b;
// 3874 
// 3875     /***************************************************************************
// 3876     * Unicast and Broadcast Schedule Configuration
// 3877     ***************************************************************************/
// 3878     schedule.broadcast_interval = 1020u;
// 3879     schedule.broadcast_schedule_identifier = 1u;
// 3880 
// 3881     schedule.us.dwell_interval = 255u;
// 3882     schedule.us.clock_drift = 255u;
// 3883     schedule.us.timing_accuracy = 0u;
// 3884     schedule.us.channel_plan = R_IE_WP_SCHEDULE_CHANNEL_PLAN_INDIRECT;
// 3885     schedule.us.channel_function = R_IE_WP_SCHEDULE_CHANNEL_FUNCTION_FIXED;
// 3886     schedule.us.excluded_channel_control = R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_NONE;
// 3887     schedule.us.regulatory_domain = wisun_regulatory_domain;
// 3888     schedule.us.operating_class = wisun_operating_class;
// 3889     schedule.us.ch0 = 902200u;
// 3890     schedule.us.channel_spacing = R_IE_WP_SCHEDULE_CHANNEL_SPACING_200;
// 3891     schedule.us.no_of_channels = 129u;
// 3892     schedule.us.fixed_channel = 0u;
// 3893 
// 3894 #endif /* R_DEV_FREQUENCY_HOPPING */
// 3895 
// 3896     /***************************************************************************
// 3897     *
// 3898     * End: Change desired AutoStart device configuration from here
// 3899     *
// 3900     ***************************************************************************/
// 3901 
// 3902     /***************************************************************************
// 3903     * Set Device Configuration
// 3904     ***************************************************************************/
// 3905     p_config->deviceType = deviceType;
// 3906     p_config->panId = panId;
// 3907     p_config->panSize = panSize;
// 3908     p_config->useParent_BS_IE = useParent_BS_IE;
// 3909 
// 3910     /* set global Network Name used for Network Name Information Element (NETNAME-IE) */
// 3911     memset(p_config->networkName, 0x00, sizeof(p_config->networkName));
// 3912     memcpy(p_config->networkName, networkName, sizeof(p_config->networkName));
// 3913     p_config->networkName[sizeof(p_config->networkName) - 1] = 0;  // Ensure null-termination
// 3914 
// 3915     /* Set operation mode (demo mode)*/
// 3916     res = R_NWK_SetRequest(R_NWK_rplDemoMode, &demo_mode, sizeof(demo_mode));
// 3917 
// 3918     if (res == R_RESULT_SUCCESS)
// 3919     {
// 3920         res = R_NWK_SetRequest(R_NWK_sixLowpanMtu, &sixLowPanMTU, sizeof(sixLowPanMTU));
// 3921     }
// 3922     if (res == R_RESULT_SUCCESS)
// 3923     {
// 3924         res = R_NWK_SetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
// 3925     }
// 3926 
// 3927     /***************************************************************************
// 3928      * Set PHY Configuration
// 3929     ***************************************************************************/
// 3930 
// 3931     /* Apply PHY configuration according to the Wi-SUN PHY specification */
// 3932     if (res == R_RESULT_SUCCESS)
// 3933     {
// 3934         r_nwk_wisun_phy_config_t wisun_phy_config;
// 3935 
// 3936         wisun_phy_config.regulatory_domain = wisun_regulatory_domain;
// 3937         wisun_phy_config.operating_class = wisun_operating_class;
// 3938         wisun_phy_config.operating_mode = wisun_operating_mode;
// 3939         res |= R_NWK_SetRequest(R_NWK_phyWiSunPhyConfig, &wisun_phy_config, sizeof(wisun_phy_config));
// 3940     }
// 3941 
// 3942     if (res == R_RESULT_SUCCESS)
// 3943     {
// 3944         res = R_NWK_SetRequest(R_NWK_phyTransmitPower, &phy_txPower, sizeof(phy_txPower));
// 3945     }
// 3946     if (res == R_RESULT_SUCCESS)
// 3947     {
// 3948         res = R_NWK_SetRequest(R_NWK_phyRegulatoryMode, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode));
// 3949     }
// 3950     if (res == R_RESULT_SUCCESS)
// 3951     {
// 3952         res = R_NWK_SetRequest(R_NWK_phyAntennaSelectTx, &phy_AntennaSelectTx, sizeof(phy_AntennaSelectTx));
// 3953     }
// 3954     if (res == R_RESULT_SUCCESS)
// 3955     {
// 3956         uint16_t phy_CcaVth = 0x01AA;  // set to -86 dBm as default, depending on the region (regulatory domain) and
// 3957                                        // the data rate (operating mode) optimization of the CCA level threshold is recommended.
// 3958         res = R_NWK_SetRequest(R_NWK_phyCcaVth, &phy_CcaVth, sizeof(phy_CcaVth));
// 3959     }
// 3960 
// 3961     /***************************************************************************
// 3962     * Set Unicast and Broadcast Schedule Configuration
// 3963     ***************************************************************************/
// 3964     if (res == R_RESULT_SUCCESS)
// 3965     {
// 3966         phyCurrentChannel = (uint8_t)schedule.us.fixed_channel;
// 3967         res = R_NWK_SetRequest(R_NWK_phyCurrentChannel, &phyCurrentChannel, sizeof(phyCurrentChannel));
// 3968     }
// 3969 
// 3970     if (res == R_RESULT_SUCCESS)
// 3971     {
// 3972         sched_buf.schedule.size = R_IE_UnicastScheduleCreate(&schedule.us, sched_buf.schedule.bytes, sizeof(sched_buf) - sizeof(sched_buf.schedule), 0);
// 3973         if (sched_buf.schedule.size > 0)
// 3974         {
// 3975             res = R_NWK_SetRequest(R_NWK_nwkSchedule_Unicast, &sched_buf, sizeof(sched_buf));
// 3976         }
// 3977         else
// 3978         {
// 3979             res = (r_result_t)(-sched_buf.schedule.size);  // create function returns negative r_result_t on error -> negate again
// 3980         }
// 3981     }
// 3982 
// 3983     if (res == R_RESULT_SUCCESS)
// 3984     {
// 3985         sched_buf.schedule.size = R_IE_BroadcastScheduleCreate(&schedule, sched_buf.schedule.bytes, sizeof(sched_buf) - sizeof(sched_buf.schedule), 0);
// 3986         if (sched_buf.schedule.size > 0)
// 3987         {
// 3988             res = R_NWK_SetRequest(R_NWK_nwkSchedule_Broadcast, &sched_buf, sizeof(sched_buf));
// 3989         }
// 3990         else
// 3991         {
// 3992             res = (r_result_t)(-sched_buf.schedule.size);  // create function returns negative r_result_t on error -> negate again
// 3993         }
// 3994     }
// 3995 
// 3996     /***************************************************************************
// 3997     * Set WhiteList Configuration
// 3998     ***************************************************************************/
// 3999     if (res == R_RESULT_SUCCESS)
// 4000     {
// 4001         r_nwk_mac_whitelist_t empty_whitelist = {0};
// 4002         res = R_NWK_SetRequest(R_NWK_macWhitelist, &empty_whitelist, sizeof(empty_whitelist));
// 4003     }
// 4004 
// 4005 #if R_DLMS_UA_CTT
// 4006     /***************************************************************************
// 4007     * Enable IP indications for incoming UDP messages (for external forwarding)
// 4008     ***************************************************************************/
// 4009     if (res == R_RESULT_SUCCESS)
// 4010     {
// 4011         r_boolean_t enabled = R_TRUE;
// 4012         res = R_NWK_SetRequest(R_NWK_udpIpIndication, &enabled, sizeof(enabled));
// 4013     }
// 4014 #endif
// 4015 
// 4016     /***************************************************************************
// 4017     * Enable Frame Counter Checking
// 4018     ***************************************************************************/
// 4019     if (res == R_RESULT_SUCCESS)
// 4020     {
// 4021         uint8_t macFrameCounterCheckEnabled = R_TRUE;
// 4022         res = R_NWK_SetRequest(R_NWK_macFrameCounterCheckEnabled, &macFrameCounterCheckEnabled, sizeof(macFrameCounterCheckEnabled));
// 4023     }
// 4024 
// 4025     /***************************************************************************
// 4026     * Adjust PAN timeout to 90 minutes: Since DAO/DAO-ACK are sent approximately
// 4027     * every 60 min, PAN timeout should never occur even without application data
// 4028     * traffic
// 4029     ***************************************************************************/
// 4030     if (res == R_RESULT_SUCCESS)
// 4031     {
// 4032         uint8_t panTimeout = 90;
// 4033         res = R_NWK_SetRequest(R_NWK_panTimeout, &panTimeout, sizeof(panTimeout));
// 4034     }
// 4035 
// 4036     /***************************************************************************
// 4037     * Authentication Join Time Optimization:
// 4038     *
// 4039     * For "smaller networks" where the overall join time is critical, please
// 4040     * enable the define "R_DEV_AUTH_JOIN_TIME_OPT". By enabling this define, the
// 4041     * stack makes use of optimized timing settings speeding up authentication.
// 4042     *
// 4043     * For "larger networks" please do not change the default authentication
// 4044     * timing settings.
// 4045     ***************************************************************************/
// 4046 #if R_DEV_AUTH_JOIN_TIME_OPT
// 4047 
// 4048     /***************************************************************************
// 4049     * Adjust initial retransmission time of unsuccessful authentication
// 4050     * to 2 minutes to speed up authentication time.
// 4051     ***************************************************************************/
// 4052     if (res == R_RESULT_SUCCESS)
// 4053     {
// 4054         uint16_t authJoinTimerBase = 2 * 60;
// 4055         res = R_NWK_SetRequest(R_NWK_authJoinTimerBase, &authJoinTimerBase, sizeof(authJoinTimerBase));
// 4056     }
// 4057 
// 4058     /***************************************************************************
// 4059     * Adjust backoff factor of authentication retransmissions
// 4060     * to 1 to speed up authentication time.
// 4061     ***************************************************************************/
// 4062     if (res == R_RESULT_SUCCESS)
// 4063     {
// 4064         uint8_t authJoinTimerBackoffFactor = 1;
// 4065         res = R_NWK_SetRequest(R_NWK_authJoinTimerBackoffFactor, &authJoinTimerBackoffFactor, sizeof(authJoinTimerBackoffFactor));
// 4066     }
// 4067 
// 4068     /***************************************************************************
// 4069      * Reduce maximum value for authentication join timer to 10 minutes:
// 4070      * The node falls back to join state 1 after two failing authentication
// 4071      * attempt, which avoids retrying over an EAPOL target with bad connectivity.
// 4072      * This value needs to be increased for larger networks.
// 4073      ***************************************************************************/
// 4074     if (res == R_RESULT_SUCCESS)
// 4075     {
// 4076         uint16_t authJoinTimerMax = (10 * 60);
// 4077         res = R_NWK_SetRequest(R_NWK_authJoinTimerMax, &authJoinTimerMax, sizeof(authJoinTimerMax));
// 4078     }
// 4079 
// 4080 #endif /* R_DEV_AUTH_JOIN_TIME_OPT */
// 4081 
// 4082     /***************************************************************************
// 4083     * Enable verbose logging, severity level = error messages
// 4084     ***************************************************************************/
// 4085     if (res == R_RESULT_SUCCESS)
// 4086     {
// 4087         uint8_t verbosity = R_LOG_SEVERITY_ERR;
// 4088         R_LOG_SetSeverityThreshold(verbosity);
// 4089     }
// 4090 
// 4091     /***************************************************************************
// 4092     * Start the Device in Router Node Configuration
// 4093     ***************************************************************************/
// 4094     if (res == R_RESULT_SUCCESS)
// 4095     {
// 4096         if (deviceType == R_ROUTERNODE)
// 4097         {
// 4098             res = R_NWK_JoinRequest(p_config->networkName, 0);
// 4099         }
// 4100 #if R_BORDER_ROUTER_ENABLED
// 4101         else if (p_config->deviceType == R_BORDERROUTER)
// 4102         {
// 4103 #if R_DHCPV6_SERVER_ENABLED
// 4104             R_DHCPV6_ServerInit(pAppGetMacAddr(), &R_DHCPV6_ServerLookupAssignPrefix, p_config->globalIpAddress.bytes);
// 4105 #endif
// 4106             res = R_NWK_StartRequest(p_config->panId,
// 4107                                      p_config->panSize,
// 4108                                      p_config->useParent_BS_IE,
// 4109                                      p_config->networkName,
// 4110                                      &p_config->globalIpAddress,
// 4111                                      0);
// 4112         }
// 4113 #endif /* R_BORDER_ROUTER_ENABLED */
// 4114         else
// 4115         {
// 4116             res = R_RESULT_INVALID_PARAMETER;  // Invalid device type
// 4117         }
// 4118     }
// 4119 
// 4120     // get current Device Type
// 4121     if (res == R_RESULT_SUCCESS)
// 4122     {
// 4123         res = R_NWK_GetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
// 4124     }
// 4125     // get current MAC address
// 4126     if (res == R_RESULT_SUCCESS)
// 4127     {
// 4128         res = R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));
// 4129     }
// 4130     // get current Pan ID
// 4131     if (res == R_RESULT_SUCCESS)
// 4132     {
// 4133         res = R_NWK_GetRequest(R_NWK_macPANId, &panId, sizeof(panId));
// 4134     }
// 4135 
// 4136     /* Confirm device start */
// 4137     R_Modem_print("STARTC %02X %02X ", handle, res);
// 4138     if (res == R_RESULT_SUCCESS)
// 4139     {
// 4140         /* DeviceType */
// 4141         R_Modem_print("%02X ", deviceType);
// 4142 
// 4143         /* PANId */
// 4144         R_Modem_print("%04X ", panId);
// 4145 
// 4146         /* NetworkName */
// 4147         AppPrintNetworkName(p_config->networkName);
// 4148         R_Modem_print(" ");
// 4149 
// 4150         /* MACAddr */
// 4151         R_Swap64((uint8_t*)(&macAddr[0]), (uint8_t*)(&macAddr[0]));
// 4152         AppPrintMACAddr(macAddr);
// 4153     }
// 4154     R_Modem_print("\n");
// 4155 }
// 4156 #endif /* R_DEV_AUTO_START */
// 
//    318 bytes in section .bssf
//  2 584 bytes in section .constf
//      2 bytes in section .dataf
// 14 778 bytes in section .textf
// 
//    320 bytes of DATA    memory
// 17 362 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
