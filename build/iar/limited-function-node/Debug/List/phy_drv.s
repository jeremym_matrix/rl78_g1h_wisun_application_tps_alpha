///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:56
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\phy_drv.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3354.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\phy_drv.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\phy_drv.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHT_IAR_NOINIT 0xabdc5467
        #define SHF_WRITE 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _RpConfig
        EXTERN _RpIntpHdr
        EXTERN _RpMemcpy
        EXTERN _RpRdEvaReg2
        EXTERN _RpTxLimitTimerForward
        EXTERN _RpWrEvaReg2
        EXTERN ___interrupt_tab_0x0E
        EXTERN ___interrupt_tab_0x2E

        PUBLIC _INTPx_Interrupt
        PUBLIC _INTTM_TXLIMIT_Interrupt
        PUBLIC _RpCheckRfIRQ
        PUBLIC _RpClkTick
        PUBLIC _RpExecuteCalibration
        PUBLIC _RpGetExtaddr
        PUBLIC _RpGetInterruptLevel
        PUBLIC _RpLimitTimerControl
        PUBLIC _RpMcuPeripheralInit
        PUBLIC _RpPowerdownSequence
        PUBLIC _RpRegBlockRead
        PUBLIC _RpRegBlockWrite
        PUBLIC _RpRegRead
        PUBLIC _RpRegWrite
        PUBLIC _RpSetMcuInt
        PUBLIC _RpWakeupSequence
        PUBLIC __A_DFLCTL
        PUBLIC __A_DTCBAR
        PUBLIC __A_DTCEN0
        PUBLIC __A_DTCEN1
        PUBLIC __A_DTCEN2
        PUBLIC __A_DTCEN3
        PUBLIC __A_DTCEN4
        PUBLIC __A_EGN0
        PUBLIC __A_EGP0
        PUBLIC __A_IF0
        PUBLIC __A_IF1
        PUBLIC __A_MK0
        PUBLIC __A_MK1
        PUBLIC __A_OSMC
        PUBLIC __A_P0
        PUBLIC __A_P1
        PUBLIC __A_P10
        PUBLIC __A_P11
        PUBLIC __A_P13
        PUBLIC __A_P14
        PUBLIC __A_P15
        PUBLIC __A_P2
        PUBLIC __A_P3
        PUBLIC __A_P4
        PUBLIC __A_P5
        PUBLIC __A_P6
        PUBLIC __A_P7
        PUBLIC __A_P8
        PUBLIC __A_PER0
        PUBLIC __A_PER1
        PUBLIC __A_PM0
        PUBLIC __A_PM1
        PUBLIC __A_PM10
        PUBLIC __A_PM11
        PUBLIC __A_PM14
        PUBLIC __A_PM15
        PUBLIC __A_PM2
        PUBLIC __A_PM3
        PUBLIC __A_PM4
        PUBLIC __A_PM5
        PUBLIC __A_PM6
        PUBLIC __A_PM7
        PUBLIC __A_PM8
        PUBLIC __A_PR00
        PUBLIC __A_PR01
        PUBLIC __A_PR10
        PUBLIC __A_PR11
        PUBLIC __A_PU1
        PUBLIC __A_PU3
        PUBLIC __A_SCR10
        PUBLIC __A_SDR10
        PUBLIC __A_SIR10
        PUBLIC __A_SMR10
        PUBLIC __A_SO1
        PUBLIC __A_SOE1
        PUBLIC __A_SPS1
        PUBLIC __A_SS1
        PUBLIC __A_SSR10
        PUBLIC __A_ST1
        PUBLIC __A_TCR01
        PUBLIC __A_TDR00
        PUBLIC __A_TDR01
        PUBLIC __A_TIS0
        PUBLIC __A_TMR00
        PUBLIC __A_TMR01
        PUBLIC __A_TOE0
        PUBLIC __A_TOL0
        PUBLIC __A_TOM0
        PUBLIC __A_TPS0
        PUBLIC __A_TS0
        PUBLIC __A_TT0
        PUBLIC ___interrupt_0x0E
        PUBLIC ___interrupt_0x2E
        PUBLIC _dtc_controldata_0
        PUBLIC _dtc_controldata_1
        PUBLIC _dtc_vectortable
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET CODE
          CFI CFA SP+4
          CFI A SameValue
          CFI X SameValue
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H SameValue
          CFI L SameValue
          CFI CS_REG SameValue
          CFI ES_REG SameValue
          CFI ?RET Frame(CFA, -4)
          CFI MACRH SameValue
          CFI MACRL SameValue
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
        
          CFI Common cfiCommon3 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon3
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\phy_drv.c
//    1 /***********************************************************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
//    4  * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
//    5  * applicable laws, including copyright laws. 
//    6  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
//    7  * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
//    8  * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//    9  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
//   10  * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
//   11  * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
//   12  * DAMAGES.
//   13  * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
//   14  * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
//   15  * following link:
//   16  * http://www.renesas.com/disclaimer 
//   17  **********************************************************************************************************************/
//   18 /***********************************************************************************************************************
//   19  * file name	: phy_drv.c
//   20  * description	: This is the RF driver's driver code.
//   21  ***********************************************************************************************************************
//   22  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
//   23  **********************************************************************************************************************/
//   24 /***************************************************************************************************************
//   25  * pragma
//   26  **************************************************************************************************************/
//   27 #if 0
//   28 #pragma NOP
//   29 #pragma STOP
//   30 #pragma HALT
//   31 #pragma EI
//   32 #pragma DI
//   33 #endif
//   34 
//   35 /***************************************************************************************************************
//   36  * includes
//   37  **************************************************************************************************************/
//   38 
//   39 #if defined (__CCRL__)
//   40 #define EI()  __EI()
//   41 #define NOP() __nop()
//   42 #elif defined(__ICCRL78__)
//   43 #include <intrinsics.h>
//   44 #define EI()  __enable_interrupt()
//   45 #define NOP() __no_operation()
//   46 #else
//   47 #define EI()  __EI()
//   48 #define NOP() __nop()
//   49 #endif
//   50 
//   51 
//   52 #include <ior5f11fll.h>

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff00H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P0
// union <unnamed>#3 volatile __saddr _A_P0
__A_P0:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff01H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P1
// union <unnamed>#4 volatile __saddr _A_P1
__A_P1:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff02H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P2
// union <unnamed>#5 volatile __saddr _A_P2
__A_P2:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff03H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P3
// union <unnamed>#6 volatile __saddr _A_P3
__A_P3:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff04H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P4
// union <unnamed>#7 volatile __saddr _A_P4
__A_P4:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff05H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P5
// union <unnamed>#8 volatile __saddr _A_P5
__A_P5:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff06H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P6
// union <unnamed>#9 volatile __saddr _A_P6
__A_P6:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff07H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P7
// union <unnamed>#10 volatile __saddr _A_P7
__A_P7:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff08H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P8
// union <unnamed>#11 volatile __saddr _A_P8
__A_P8:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff0aH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P10
// union <unnamed>#12 volatile __saddr _A_P10
__A_P10:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff0bH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P11
// union <unnamed>#13 volatile __saddr _A_P11
__A_P11:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff0dH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P13
// union <unnamed>#15 volatile __saddr _A_P13
__A_P13:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff0eH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P14
// union <unnamed>#16 volatile __saddr _A_P14
__A_P14:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff0fH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P15
// union <unnamed>#17 volatile __saddr _A_P15
__A_P15:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff18H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TDR00
// union <unnamed>#24 volatile __saddr __no_bit_access _A_TDR00
__A_TDR00:
        DS 2

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff1aH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TDR01
// union <unnamed>#25 volatile __saddr __no_bit_access _A_TDR01
__A_TDR01:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff20H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM0
// union <unnamed>#32 volatile __sfr _A_PM0
__A_PM0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff21H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM1
// union <unnamed>#33 volatile __sfr _A_PM1
__A_PM1:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff22H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM2
// union <unnamed>#34 volatile __sfr _A_PM2
__A_PM2:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff23H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM3
// union <unnamed>#35 volatile __sfr _A_PM3
__A_PM3:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff24H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM4
// union <unnamed>#36 volatile __sfr _A_PM4
__A_PM4:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff25H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM5
// union <unnamed>#37 volatile __sfr _A_PM5
__A_PM5:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff26H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM6
// union <unnamed>#38 volatile __sfr _A_PM6
__A_PM6:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff27H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM7
// union <unnamed>#39 volatile __sfr _A_PM7
__A_PM7:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff28H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM8
// union <unnamed>#40 volatile __sfr _A_PM8
__A_PM8:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff2aH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM10
// union <unnamed>#41 volatile __sfr _A_PM10
__A_PM10:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff2bH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM11
// union <unnamed>#42 volatile __sfr _A_PM11
__A_PM11:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff2eH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM14
// union <unnamed>#44 volatile __sfr _A_PM14
__A_PM14:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff2fH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM15
// union <unnamed>#45 volatile __sfr _A_PM15
__A_PM15:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff38H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_EGP0
// union <unnamed>#50 volatile __sfr _A_EGP0
__A_EGP0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff39H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_EGN0
// union <unnamed>#51 volatile __sfr _A_EGN0
__A_EGN0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff48H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SDR10
// union <unnamed>#60 volatile __sfr __no_bit_access _A_SDR10
__A_SDR10:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe0H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_IF0
// union <unnamed>#153 volatile __sfr _A_IF0
__A_IF0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe2H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_IF1
// union <unnamed>#160 volatile __sfr _A_IF1
__A_IF1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe4H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_MK0
// union <unnamed>#168 volatile __sfr _A_MK0
__A_MK0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe6H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_MK1
// union <unnamed>#175 volatile __sfr _A_MK1
__A_MK1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe8H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR00
// union <unnamed>#183 volatile __sfr _A_PR00
__A_PR00:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffeaH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR01
// union <unnamed>#190 volatile __sfr _A_PR01
__A_PR01:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffecH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR10
// union <unnamed>#198 volatile __sfr _A_PR10
__A_PR10:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffeeH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR11
// union <unnamed>#205 volatile __sfr _A_PR11
__A_PR11:
        DS 2
//   53 #include <ior5f11fll_ext.h>

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0031H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PU1
// union <unnamed>#223 volatile __near _A_PU1
__A_PU1:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0033H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PU3
// union <unnamed>#224 volatile __near _A_PU3
__A_PU3:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0074H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TIS0
// union <unnamed>#244 volatile __near __no_bit_access _A_TIS0
__A_TIS0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f007aH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PER1
// union <unnamed>#247 volatile __near _A_PER1
__A_PER1:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0090H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_DFLCTL
// union <unnamed>#250 volatile __near _A_DFLCTL
__A_DFLCTL:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f00f0H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PER0
// union <unnamed>#258 volatile __near _A_PER0
__A_PER0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f00f3H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_OSMC
// union <unnamed>#260 volatile __near __no_bit_access _A_OSMC
__A_OSMC:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0140H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SSR10
// union <unnamed>#299 const volatile __near __no_bit_access _A_SSR10
__A_SSR10:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0148H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SIR10
// union <unnamed>#311 volatile __near __no_bit_access _A_SIR10
__A_SIR10:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0150H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SMR10
// union <unnamed>#323 volatile __near __no_bit_access _A_SMR10
__A_SMR10:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0158H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SCR10
// union <unnamed>#327 volatile __near __no_bit_access _A_SCR10
__A_SCR10:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0162H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SS1
// union <unnamed>#334 volatile __near _A_SS1
__A_SS1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0164H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_ST1
// union <unnamed>#337 volatile __near _A_ST1
__A_ST1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0166H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SPS1
// union <unnamed>#340 volatile __near __no_bit_access _A_SPS1
__A_SPS1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0168H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SO1
// union <unnamed>#343 volatile __near __no_bit_access _A_SO1
__A_SO1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f016aH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SOE1
// union <unnamed>#344 volatile __near _A_SOE1
__A_SOE1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0182H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TCR01
// union <unnamed>#351 const volatile __near __no_bit_access _A_TCR01
__A_TCR01:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0190H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TMR00
// union <unnamed>#354 volatile __near __no_bit_access _A_TMR00
__A_TMR00:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0192H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TMR01
// union <unnamed>#355 volatile __near __no_bit_access _A_TMR01
__A_TMR01:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01b2H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TS0
// union <unnamed>#373 volatile __near _A_TS0
__A_TS0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01b4H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TT0
// union <unnamed>#376 volatile __near _A_TT0
__A_TT0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01b6H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TPS0
// union <unnamed>#379 volatile __near __no_bit_access _A_TPS0
__A_TPS0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01baH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TOE0
// union <unnamed>#383 volatile __near _A_TOE0
__A_TOE0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01bcH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TOL0
// union <unnamed>#386 volatile __near __no_bit_access _A_TOL0
__A_TOL0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01beH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TOM0
// union <unnamed>#389 volatile __near __no_bit_access _A_TOM0
__A_TOM0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f02e0H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_DTCBAR
// union <unnamed>#450 volatile __near __no_bit_access _A_DTCBAR
__A_DTCBAR:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f02e8H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_DTCEN0
// union <unnamed>#451 volatile __near _A_DTCEN0
__A_DTCEN0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f02e9H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_DTCEN1
// union <unnamed>#452 volatile __near _A_DTCEN1
__A_DTCEN1:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f02eaH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_DTCEN2
// union <unnamed>#453 volatile __near _A_DTCEN2
__A_DTCEN2:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f02ebH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_DTCEN3
// union <unnamed>#454 volatile __near _A_DTCEN3
__A_DTCEN3:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f02ecH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_DTCEN4
// union <unnamed>#455 volatile __near _A_DTCEN4
__A_DTCEN4:
        DS 1
//   54 
//   55 #include "phy_config.h"
//   56 
//   57 #if defined (__CCRL__)
//   58 	#pragma interrupt INTPx_Interrupt (vect=INTP3)
//   59 	#if (RP_USR_RF_TAU_CH_SELECT == 1)
//   60 		#pragma interrupt INTTM_TXLIMIT_Interrupt (vect=INTTM01)
//   61 	#else
//   62 		#pragma interrupt INTTM_TXLIMIT_Interrupt (vect=INTTM02)
//   63 	#endif
//   64 #elif defined (__ICCRL78__)
//   65         #pragma vector = INTP3_vect
//   66         __interrupt void INTPx_Interrupt(void);
//   67         #if (RP_USR_RF_TAU_CH_SELECT == 1)
//   68             #pragma vector = INTTM01_vect
//   69             __interrupt void INTTM_TXLIMIT_Interrupt(void);
//   70         #else
//   71             #pragma vector = INTTM02_vect
//   72             __interrupt void INTTM_TXLIMIT_Interrupt(void);
//   73 	#endif
//   74 #else /* __CCRL__ */
//   75 	#if R_USE_RENESAS_STACK
//   76 		#pragma interrupt INTP3 INTPx_Interrupt SP=RpIntHdlStk+320
//   77 		#if (RP_USR_RF_TAU_CH_SELECT == 1)
//   78 			#pragma interrupt INTTM01 INTTM_TXLIMIT_Interrupt SP=RpIntHdlStk+320
//   79 		#else
//   80 			#pragma interrupt INTTM02 INTTM_TXLIMIT_Interrupt SP=RpIntHdlStk+320
//   81 		#endif
//   82 	#else
//   83 		#pragma interrupt INTP3 INTPx_Interrupt
//   84 		#if (RP_USR_RF_TAU_CH_SELECT == 1)
//   85 			#pragma interrupt INTTM01 INTTM_TXLIMIT_Interrupt
//   86 		#else
//   87 			#pragma interrupt INTTM02 INTTM_TXLIMIT_Interrupt
//   88 		#endif
//   89 	#endif // #if R_USE_RENESAS_STACK
//   90 #endif /* __CCRL__ */
//   91 
//   92 #include <string.h>
//   93 #include "hardware_sfr.h"
//   94 #include "phy.h"
//   95 #include "phy_def.h"
//   96 #include "phy_drv.h"
//   97 
//   98 /***************************************************************************************************************
//   99  * extern definitions
//  100  **************************************************************************************************************/
//  101 extern const RP_CONFIG_CB RpConfig;
//  102 
//  103 /***************************************************************************************************************
//  104  * macro definitions
//  105  **************************************************************************************************************/
//  106 #define RpCSI_CSActivate() (RP_CSI_CS_PORT = RP_PORT_LO)
//  107 #define RpCSI_CSDeactivate() (RP_CSI_CS_PORT = RP_PORT_HI)
//  108 #define BBTXRXRAMEND	(0x400 << 3)
//  109 
//  110 #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
//  111 	#define DTC_VECTBL_ADDRESS 			0xFFD00
//  112 	#define DTC_CTRL0_ADDRESS			0xFFD40
//  113 	#define DTC_CTRL1_ADDRESS			0xFFD48
//  114 
//  115 	#define DTC_VECTBL_ADDRESS_SHORT		((uint16_t)DTC_VECTBL_ADDRESS)
//  116 	#define DTC_VECTBL_ADDRESS_BARSET	((uint8_t)(DTC_VECTBL_ADDRESS_SHORT >> 8))
//  117 
//  118 	/* dtc_vectortable[16] */
//  119 	#define RP_DTC_VECTORTABLE16        *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x10))  // 0xffd10
//  120 	/* dtc_controldata_0 */
//  121 	#define RP_DTC_CONTROLDATA0_DTCCR   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x40))  // 0xffd40
//  122 	#define RP_DTC_CONTROLDATA0_DTBLS   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x41))  // 0xffd41
//  123 	#define RP_DTC_CONTROLDATA0_DTCCT   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x42))  // 0xffd42
//  124 	#define RP_DTC_CONTROLDATA0_DTRLD   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x43))  // 0xffd43
//  125 	#define RP_DTC_CONTROLDATA0_DTSAR   *((uint16_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x44)) // 0xffd44
//  126 	#define RP_DTC_CONTROLDATA0_DTDAR   *((uint16_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x46)) // 0xffd46
//  127 	/* dtc_controldata_1 */
//  128 	#define RP_DTC_CONTROLDATA1_DTCCR   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x48))  // 0xffd48
//  129 	#define RP_DTC_CONTROLDATA1_DTBLS   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x49))  // 0xffd49
//  130 	#define RP_DTC_CONTROLDATA1_DTCCT   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x4a))  // 0xffd4a
//  131 	#define RP_DTC_CONTROLDATA1_DTRLD   *((uint8_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x4b))  // 0xffd4b
//  132 	#define RP_DTC_CONTROLDATA1_DTSAR   *((uint16_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x4c)) // 0xffd4c
//  133 	#define RP_DTC_CONTROLDATA1_DTDAR   *((uint16_t __near *)(DTC_VECTBL_ADDRESS_SHORT + 0x4e)) // 0xffd4e
//  134 
//  135 	// DTC Control Register j (DTCCRj) (j = 0 to 23)
//  136 	// Data size selection (SZ)
//  137 	#define RP_00_DTC_DATA_SIZE_8BITS             (0x00U) // 8 bits
//  138 	#define RP_40_DTC_DATA_SIZE_16BITS            (0x40U) // 16 bits
//  139 	// Repeat mode interrupt enable bit (RPTINT)
//  140 	#define RP_00_DTC_REPEAT_INT_DISABLE          (0x00U) // disable interrupt generation
//  141 	#define RP_20_DTC_REPEAT_INT_ENABLE           (0x20U) // enable interrupt generation
//  142 	// Chain transfer enable bit (CHNE)
//  143 	#define RP_00_DTC_CHAIN_TRANSFER_DISABLE      (0x00U) // disable chain transfers
//  144 	#define RP_10_DTC_CHAIN_TRANSFER_ENABLE       (0x10U) // enable chain transfers
//  145 	// Destination address control bit (DAMOD)
//  146 	#define RP_00_DTC_DEST_ADDR_FIXED             (0x00U) // destination address fixed
//  147 	#define RP_08_DTC_DEST_ADDR_INCREMENTED       (0x08U) // destination address incremented
//  148 	// Source address control bit (SAMOD)
//  149 	#define RP_00_DTC_SOURCE_ADDR_FIXED           (0x00U) // source address fixed
//  150 	#define RP_04_DTC_SOURCE_ADDR_INCREMENTED     (0x04U) // source address incremented
//  151 	// Repeat area select bit (RPTSEL)
//  152 	#define RP_00_DTC_REPEAT_AREA_DEST            (0x00U) // transfer destination is the repeat area
//  153 	#define RP_02_DTC_REPEAT_AREA_SOURCE          (0x02U) // transfer source is the repeat area
//  154 	// Transfer mode select bit (MODE)
//  155 	#define RP_00_DTC_TRANSFER_MODE_NORMAL        (0x00U) // normal mode
//  156 	#define RP_01_DTC_TRANSFER_MODE_REPEAT        (0x01U) // repeat mode
//  157 
//  158 	// DTC Activation Enable Register 2 (DTCEN2)
//  159 	// DTC activation enable bit (DTCEN27 - DTCEN20)
//  160 	#define RP_00_DTC_UART2T_ACTIVATION_DISABLE   (0x00U) // disable activation (UART2 transmission/CSI20/IIC20 transfer)
//  161 	#define RP_80_DTC_UART2T_ACTIVATION_ENABLE    (0x80U) // enable activation (UART2 transmission/CSI20/IIC20 transfer)
//  162 
//  163 	/* --- --- */
//  164 	#define RP_FF1E_DTCD0_SRC_ADDRESS             (0xFF1EU)
//  165 	#define RP_F500_DTCD0_DEST_ADDRESS            (0xF500U)
//  166 	#define RP_04_DTCD0_TRANSFER_BYTE             (0x04U)
//  167 	#define RP_02_DTCD0_TRANSFER_BLOCKSIZE        (0x02U)
//  168 	#define RP_01_DTCD0_TRANSFER_BYTE             (0x01U)
//  169 	#define RP_00_DTCD0_TRANSFER_BYTE             (0x00U)
//  170 	#define RP_FF48_DTCD0_DST_ADDRESS             (0xFF48U)
//  171 	#define RP_FF48_DTCD1_SRC_ADDRESS             (0xFF48U)
//  172 #endif// RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
//  173 
//  174 #ifdef R_USE_RENESAS_STACK
//  175 	#ifndef RP_WKUPTIMER_USE
//  176 		#define RP_WKUPTIMER_USE 1
//  177 	#endif
//  178 	#if RP_WKUPTIMER_USE == 2
//  179 		extern uint32_t RpGetTime( void );
//  180 	#endif
//  181 #endif	// #ifdef R_USE_RENESAS_STACK
//  182 
//  183 #ifdef R_USE_RENESAS_STACK
//  184 	#if RP_WKUPTIMER_USE
//  185 		#if RP_WKUPTIMER_USE == 2
//  186 			#define TAxIC	bbtim2ic
//  187 		#elif RP_WKUPTIMER_USE == 1
//  188 			#define TAxS	tstart_tracr
//  189 			#define TAxMR	tramr
//  190 			#define TAxIC	traic
//  191 			#define TAxPRE	trapre
//  192 			#define TAx	tra
//  193 		#endif
//  194 		#define RP_IPL_WUP_TSK		2		// interrupt level of Timer(TAx) (used to invoke RpWupTskHdr)
//  195 	#endif
//  196 #endif	// #ifdef R_USE_RENESAS_STACK
//  197 
//  198 // MAC Address stored in Flash
//  199 #define RP_ADDR_EXTADDR	((uint8_t RP_FAR *)0xf1000)
//  200 
//  201 #if ( (RP_PHY_INTLEVEL != 1) && (RP_PHY_INTLEVEL != 2) )
//  202 	#error "error: out of range (RP_PHY_INTLEVEL)"
//  203 #endif
//  204 
//  205 // Interrupt Level
//  206 #define RP_PHY_INTLEVEL_RFINTP			RP_PHY_INTLEVEL
//  207 #define RP_PHY_INTLEVEL_RFTimers		RP_PHY_INTLEVEL
//  208 #define RP_PHY_INTLEVEL_DMADone			RP_PHY_INTLEVEL
//  209 
//  210 #define RP_PHY_INTLEVEL_RFINTP_b0		(RP_PHY_INTLEVEL_RFINTP & 0x01)
//  211 #define RP_PHY_INTLEVEL_RFINTP_b1		((RP_PHY_INTLEVEL_RFINTP & 0x02) >> 1)
//  212 #define RP_PHY_INTLEVEL_RFTimers_b0		(RP_PHY_INTLEVEL_RFTimers & 0x01)
//  213 #define RP_PHY_INTLEVEL_RFTimers_b1		((RP_PHY_INTLEVEL_RFTimers & 0x02) >> 1)
//  214 #define RP_PHY_INTLEVEL_DMADone_b0		(RP_PHY_INTLEVEL_DMADone & 0x01)
//  215 #define RP_PHY_INTLEVEL_DMADone_b1		((RP_PHY_INTLEVEL_DMADone & 0x02) >> 1)
//  216 
//  217 // CSI Registers
//  218 #define	RP_CSI_STATUS			SSR10L
//  219 #define RP_SAU_UNDER_EXECUTE	(0x40)
//  220 #define RP_CSI_IRQ				CSIIF20
//  221 #define RP_CSI_BUF				SIO20
//  222 
//  223 #define	RP_CSI_NOP				0xff
//  224 #define	RP_CSI_MEM_WR_H(adr)	(adr >> 8)
//  225 #define	RP_CSI_MEM_WR_L(adr)	(adr)
//  226 #define	RP_CSI_MEM_RD_H(adr)	(adr >> 8)
//  227 #define	RP_CSI_MEM_RD_L(adr)	(adr + 0x06)
//  228 
//  229 #define RP_REG_WRITE(wData) RP_CSI_IRQ = 0; \ 
//  230 	                       RP_CSI_BUF = wData;\ 
//  231 						   while(RP_CSI_IRQ == 0)
//  232 // Wait Wakeup
//  233 #if defined(RP_WAKEUP_MCU_HALT)
//  234 	#define RP_WAIT_WAKEUP_ACTIVE()  TMMK00 = 0;\ 
//  235 									TS0 |= TAU_CH0_START_TRG_ON;\ 
//  236 									HALT();\ 
//  237 									while (TMIF00 == 0);\ 
//  238 									TT0 |= TAU_CH0_STOP_TRG_ON;\ 
//  239 									TMMK00 = 1
//  240 #else
//  241 	#define  RP_WAIT_WAKEUP_ACTIVE() TS0 |= TAU_CH0_START_TRG_ON;\ 
//  242 									while (TMIF00 == 0);\ 
//  243 									TT0 |= TAU_CH0_STOP_TRG_ON
//  244 #endif
//  245 
//  246 /***************************************************************************************************************
//  247  * static function prototypes
//  248  **************************************************************************************************************/
//  249 static void RpSetMcuIntMaskOnly( uint8_t enable );
//  250 static void RpCSIInitPort( void );
//  251 static void RpCSIInitialize( void );
//  252 static void RpRFTimerInitialize( void );
//  253 #if RP_DMA_WRITE_RAM_ENA
//  254 	static void RpDmaTerminationWriteSer( void );
//  255 #endif // #if RP_DMA_WRITE_RAM_ENA
//  256 #if RP_DMA_READ_RAM_ENA
//  257 	static void RpDmaTerminationReadSer( void );
//  258 #endif // #if RP_DMA_READ_RAM_ENA
//  259 #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
//  260 	static void RpDmaSerInitial( void );
//  261 	static void RpDmaSerStart( uint8_t readmode, uint8_t *data, uint8_t bytes );
//  262 	static void RpDmaSerFinishChk( void );
//  263 	static void RpDmaSerStop( void );
//  264 #endif// RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
//  265 
//  266 /***************************************************************************************************************
//  267  * private variables
//  268  **************************************************************************************************************/
//  269 #ifdef RP_DEBUG_WRITE_MONITOR
//  270 	#define RP_DEBUG_RAM_SIZE	1024
//  271 	uint8_t RpDebugRamReg[RP_DEBUG_RAM_SIZE];
//  272 	uint16_t RpDebugRamRegCnt;
//  273 #endif
//  274 
//  275 #ifdef __CCRL__
//  276 	#ifdef R_USE_RENESAS_STACK
//  277 		#define RP_INTHDRSTACK_SIZE     (160)
//  278 		static uint16_t	RpIntHdlStk[RP_INTHDRSTACK_SIZE];
//  279 	#endif // #ifdef R_USE_RENESAS_STACK
//  280 #endif // #ifdef __CCRL__
//  281 

        SECTION `.bssf`:DATA:REORDER:NOROOT(0)
//  282 uint8_t	RpClkTick = 0;
_RpClkTick:
        DS 1
//  283 
//  284 /***************************************************************************************************************
//  285  * program
//  286  **************************************************************************************************************/
//  287 /***************************************************************************************************************
//  288  * Inline asm
//  289  **************************************************************************************************************/
//  290 #ifdef __CCRL__
//  291 #ifdef R_USE_RENESAS_STACK
//  292 
//  293 #pragma inline_asm RpIntrSpChange
//  294 void RpIntrSpChange(uint16_t pstk)
//  295 {
//  296 	movw	bc, sp
//  297 	movw	sp, ax   ; move SP
//  298 	push	bc      ; store original SP to the buffer
//  299 	subw	sp, #0x02
//  300 }
//  301 
//  302 #pragma inline_asm RpIntrSpBack
//  303 void RpIntrSpBack(void)
//  304 {
//  305 	addw	sp, #0x02
//  306 	pop		ax
//  307 	movw	sp, ax   ; move SP to the original
//  308 }
//  309 
//  310 #endif // #ifdef R_USE_RENESAS_STACK
//  311 #endif // #ifdef __CCRL__
//  312 
//  313 /***************************************************************************************************************
//  314  * function name  : RpGetExtaddr
//  315  * description    : get self IEEE address.
//  316  * parameters     : pExtAddr:the pointer of "reading IEEE address"
//  317  * return value   : none
//  318  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _RpGetExtaddr
        CODE
//  319 void RpGetExtaddr( uint8_t *pExtAddr )
//  320 {
_RpGetExtaddr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  321 #if defined(RM_RAM_EXTADDR)	// get extended address from global varialbe
//  322 	extern uint32_t RpLocalExtAddr[];
//  323 
//  324 	RpMemcpy(pExtAddr, RpLocalExtAddr, 8);
//  325 #else					// get extended address from internal flash (block A)
//  326 	DFLCTL = 0x01;		// enable data flash
        MOV       0x90, #0x1         ;; 1 cycle
//  327 	RpMemcpy(pExtAddr, RP_ADDR_EXTADDR, 8);
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      BC, #0x1000        ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _RpMemcpy
        CALL      F:_RpMemcpy        ;; 3 cycles
//  328 	DFLCTL = 0x00;		// disable data flash
        MOV       0x90, #0x0         ;; 1 cycle
//  329 #endif
//  330 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 29 cycles
        ; ------------------------------------- Total: 29 cycles
        REQUIRE __A_DFLCTL
//  331 
//  332 /***************************************************************************************************************
//  333  * function name  : INTPx_Interrupt
//  334  * description    : INTP interrupt handler.
//  335  * parameters     : none
//  336  * return value   : none
//  337  **************************************************************************************************************/
//  338 #ifdef __CCRL__
//  339 __near void INTPx_Interrupt( void )
//  340 #else

        SECTION `.text`:CODE:NOROOT(0)
        CALL_GRAPH_ROOT _INTPx_Interrupt, "interrupt"
          CFI Block cfiBlock1 Using cfiCommon1
          CFI Function _INTPx_Interrupt
        CODE
//  341 __interrupt void INTPx_Interrupt( void )
//  342 #endif /* __CCRL__ */
//  343 {
_INTPx_Interrupt:
___interrupt_0x0E:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI X Frame(CFA, -6)
          CFI A Frame(CFA, -5)
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI C Frame(CFA, -8)
          CFI B Frame(CFA, -7)
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI E Frame(CFA, -10)
          CFI D Frame(CFA, -9)
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI L Frame(CFA, -12)
          CFI H Frame(CFA, -11)
          CFI CFA SP+12
        MOVW      AX, 0xFFFFC        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        ; Auto size: 0
//  344 	#if R_USE_RENESAS_STACK && defined(__CCRL__)
//  345 	RpIntrSpChange((uint16_t)(&RpIntHdlStk[RP_INTHDRSTACK_SIZE]));    // (no need '-1')
//  346 	#endif
//  347 
//  348 	EI();
        EI                           ;; 4 cycles
//  349 	RpIntpHdr();
          CFI FunCall _RpIntpHdr
        CALL      F:_RpIntpHdr       ;; 3 cycles
//  350 
//  351 	#if R_USE_RENESAS_STACK && defined(__CCRL__)
//  352 	RpIntrSpBack();
//  353 	#endif
//  354 }
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      0xFFFFC, AX        ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI L SameValue
          CFI H SameValue
          CFI CFA SP+10
        POP       DE                 ;; 1 cycle
          CFI E SameValue
          CFI D SameValue
          CFI CFA SP+8
        POP       BC                 ;; 1 cycle
          CFI C SameValue
          CFI B SameValue
          CFI CFA SP+6
        POP       AX                 ;; 1 cycle
          CFI X SameValue
          CFI A SameValue
          CFI CFA SP+4
        RETI                         ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 25 cycles
        ; ------------------------------------- Total: 25 cycles
        REQUIRE ___interrupt_tab_0x0E
//  355 
//  356 /***************************************************************************************************************
//  357  * function name  : INTTM_TXLIMIT_Interrupt
//  358  * description    : 10% Limitation Timer interrupt.
//  359  * parameters     : none
//  360  * return value   : none
//  361  **************************************************************************************************************/
//  362 #ifdef __CCRL__
//  363 __near void INTTM_TXLIMIT_Interrupt( void )
//  364 {
//  365 	#ifdef R_USE_RENESAS_STACK
//  366 	RpIntrSpChange( (uint16_t)(&RpIntHdlStk[RP_INTHDRSTACK_SIZE]) );    // (no need '-1')
//  367 	#endif // #ifdef R_USE_RENESAS_STACK
//  368 
//  369 	EI();
//  370 	RpTxLimitTimerForward(1000);
//  371 
//  372 	#ifdef R_USE_RENESAS_STACK
//  373 	RpIntrSpBack();
//  374 	#endif // #ifdef R_USE_RENESAS_STACK
//  375 }
//  376 #else

        SECTION `.text`:CODE:NOROOT(0)
        CALL_GRAPH_ROOT _INTTM_TXLIMIT_Interrupt, "interrupt"
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _INTTM_TXLIMIT_Interrupt
        CODE
//  377 __interrupt void INTTM_TXLIMIT_Interrupt( void )
//  378 {
_INTTM_TXLIMIT_Interrupt:
___interrupt_0x2E:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI X Frame(CFA, -6)
          CFI A Frame(CFA, -5)
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI C Frame(CFA, -8)
          CFI B Frame(CFA, -7)
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI E Frame(CFA, -10)
          CFI D Frame(CFA, -9)
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI L Frame(CFA, -12)
          CFI H Frame(CFA, -11)
          CFI CFA SP+12
        MOVW      AX, 0xFFFFC        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        ; Auto size: 0
//  379 	EI();
        EI                           ;; 4 cycles
//  380 	RpTxLimitTimerForward( 1000 );
        MOVW      AX, #0x3E8         ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
          CFI FunCall _RpTxLimitTimerForward
        CALL      F:_RpTxLimitTimerForward  ;; 3 cycles
//  381 }
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      0xFFFFC, AX        ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI L SameValue
          CFI H SameValue
          CFI CFA SP+10
        POP       DE                 ;; 1 cycle
          CFI E SameValue
          CFI D SameValue
          CFI CFA SP+8
        POP       BC                 ;; 1 cycle
          CFI C SameValue
          CFI B SameValue
          CFI CFA SP+6
        POP       AX                 ;; 1 cycle
          CFI X SameValue
          CFI A SameValue
          CFI CFA SP+4
        RETI                         ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 27 cycles
        ; ------------------------------------- Total: 27 cycles
        REQUIRE ___interrupt_tab_0x2E
//  382 #endif // #ifdef __CCRL__
//  383 
//  384 /***************************************************************************************************************
//  385  * function name  : RpSetMcuInt
//  386  * description    : control of using RF interrput.
//  387  * parameters     : enable: 0:disable interrupt, 1:enable interrupt nothing to do
//  388  * return value   : none
//  389  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon2
          CFI Function _RpSetMcuInt
          CFI NoCalls
        CODE
//  390 void RpSetMcuInt( uint8_t enable )
//  391 {
_RpSetMcuInt:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  392 	if (enable == RP_TRUE)
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpGetInterruptLevel_0  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  393 	{
//  394 		EGP0	|=	0x08;							// INTP3 rising edge(EGP3	= 	1)
        SET1      0xFFF38.3          ;; 2 cycles
//  395 		EGN0	&=	~0x08;							// INTP3 rising edge(EGN3	=	0)
        CLR1      0xFFF39.3          ;; 2 cycles
//  396 		PPR03	= 	RP_PHY_INTLEVEL_RFINTP_b0;
        SET1      0xFFFE8.5          ;; 2 cycles
//  397 		PPR13	= 	RP_PHY_INTLEVEL_RFINTP_b1;		// INTP3 priority 1
        CLR1      0xFFFEC.5          ;; 2 cycles
        CLR1      0xFFFE0.5          ;; 2 cycles
        CLR1      0xFFFE4.5          ;; 2 cycles
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 18 cycles
//  398 	}
//  399 	RpSetMcuIntMaskOnly(enable);
??RpGetInterruptLevel_0:
        CLR1      0xFFFE0.5          ;; 2 cycles
        SET1      0xFFFE4.5          ;; 2 cycles
//  400 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 33 cycles
        REQUIRE __A_EGP0
        REQUIRE __A_EGN0
        REQUIRE __A_PR00
        REQUIRE __A_PR10
        REQUIRE __A_IF0
        REQUIRE __A_MK0
//  401 
//  402 /***************************************************************************************************************
//  403  * function name  : RpSetMcuIntMaskOnly
//  404  * description    : set interrupt mask only
//  405  * parameters     : 
//  406  * return value   : 
//  407  **************************************************************************************************************/
//  408 static void RpSetMcuIntMaskOnly( uint8_t enable )
//  409 {
//  410 	// accept RF IC interrupt and moreover previous RF IC interrupt
//  411 	PIF3 = RP_FALSE;			// INTP3 REQ clear
//  412 	if (enable == RP_TRUE)
//  413 	{
//  414 		PMK3	= RP_FALSE;		// INTP3 MASK enable
//  415 	}
//  416 	else
//  417 	{
//  418 		PMK3	= RP_TRUE;		// INTP3 MASK disable
//  419 	}
//  420 }
//  421 
//  422 /***************************************************************************************************************
//  423  * function name  : RpRegWrite
//  424  * description    : Write a data to RFIC function.
//  425  * parameters     : 
//  426  * return value   : 
//  427  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon3
          CFI Function _RpRegWrite
          CFI NoCalls
        CODE
//  428 void RpRegWrite( uint16_t adr, uint8_t dat )
//  429 {
_RpRegWrite:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOVW      DE, AX             ;; 1 cycle
//  430 	RpCSI_CSActivate();
        CLR1      S:0xFFF01.6        ;; 2 cycles
//  431 
//  432 #ifdef RP_TXRX_RAM_BIT_SWAP
//  433 	SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
//  434 #endif
//  435 
//  436 	SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_BUFFER_EMPTY;
        MOVW      AX, #0x8021        ;; 1 cycle
        MOVW      0x150, AX          ;; 1 cycle
//  437 #ifdef RP_DEBUG_WRITE_MONITOR
//  438 	RpDebugRamReg[RpDebugRamRegCnt] = 0xfe;
//  439 	RpDebugRamRegCnt++;
//  440 	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
//  441 	RpDebugRamReg[RpDebugRamRegCnt] = 0xfd;
//  442 	RpDebugRamRegCnt++;
//  443 	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
//  444 #endif
//  445 	RP_REG_WRITE((uint8_t)RP_CSI_MEM_WR_H(adr));
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
??RpRegWrite_0:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpRegWrite_0  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  446 #ifdef RP_DEBUG_WRITE_MONITOR
//  447 	RP_CSI_IRQ = 0;
//  448 	RP_CSI_BUF = (uint8_t)RP_CSI_MEM_WR_L(adr);
//  449 	{
//  450 		uint8_t test_ram[2];
//  451 
//  452 		test_ram[0] = (uint8_t)(adr >> 3);
//  453 		test_ram[1] = (uint8_t)(adr >> 11);
//  454 		RpDebugRamReg[RpDebugRamRegCnt] = test_ram[1];
//  455 		RpDebugRamReg[RpDebugRamRegCnt + 1] = test_ram[0];
//  456 	}
//  457 	RpDebugRamRegCnt += 2;
//  458 	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
//  459 	while (RP_CSI_IRQ == 0);
//  460 	RP_CSI_IRQ = 0;
//  461 	RP_CSI_BUF = dat;
//  462 	RpDebugRamReg[RpDebugRamRegCnt] = dat;
//  463 	RpDebugRamRegCnt++;
//  464 	RpDebugRamRegCnt &= (RP_DEBUG_RAM_SIZE - 1);
//  465 	while (RP_CSI_IRQ == 0);
//  466 #else
//  467 	RP_REG_WRITE((uint8_t)RP_CSI_MEM_WR_L(adr));
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       A, E               ;; 1 cycle
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??RpRegWrite_1:
        BF        [HL].0, ??RpRegWrite_1  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  468 
//  469 #ifdef RP_TXRX_RAM_BIT_SWAP
//  470 	if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
//  471 	{
//  472 		SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_LSB | SAU_LENGTH_8;
//  473 	}
//  474 #endif
//  475 
//  476 	RP_REG_WRITE(dat);
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       A, C               ;; 1 cycle
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??RpRegWrite_2:
        BF        [HL].0, ??RpRegWrite_2  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  477 #endif
//  478 	RP_CSI_IRQ = 0;
        CLR1      0xFFFE1.0          ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
//  479 	while (RP_CSI_STATUS & RP_SAU_UNDER_EXECUTE);
??RpRegWrite_3:
        MOV       A, 0x140           ;; 1 cycle
        BT        A.6, ??RpRegWrite_3  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  480 	RpCSI_CSDeactivate();
        SET1      S:0xFFF01.6        ;; 2 cycles
//  481 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 49 cycles
        REQUIRE __A_P1
        REQUIRE __A_SMR10
        REQUIRE __A_IF0
        REQUIRE __A_SDR10
        REQUIRE __A_SSR10
//  482 
//  483 /***************************************************************************************************************
//  484  * function name  : RpRegRead
//  485  * description    : Read a data from RFIC function.
//  486  * parameters     : 
//  487  * return value   : 
//  488  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon2
          CFI Function _RpRegRead
          CFI NoCalls
        CODE
//  489 uint8_t RpRegRead( uint16_t adr )
//  490 {
_RpRegRead:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOVW      DE, AX             ;; 1 cycle
//  491 	uint8_t rData;
//  492 
//  493 	RpCSI_CSActivate();
        CLR1      S:0xFFF01.6        ;; 2 cycles
//  494 #ifdef RP_TXRX_RAM_BIT_SWAP
//  495 	SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
//  496 #endif
//  497 	SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_TRANSFER_END;
        MOVW      AX, #0x8020        ;; 1 cycle
        MOVW      0x150, AX          ;; 1 cycle
//  498 	RP_REG_WRITE((uint8_t)RP_CSI_MEM_RD_H(adr));
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
??RpRegRead_0:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpRegRead_0  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  499 	RP_REG_WRITE((uint8_t)RP_CSI_MEM_RD_L(adr));
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       A, E               ;; 1 cycle
        ADD       A, #0x6            ;; 1 cycle
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RpRegRead_1:
        BF        [HL].0, ??RpRegRead_1  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  500 	RP_REG_WRITE(RP_CSI_NOP);
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       0xFFF48, #0xFF     ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??RpRegRead_2:
        BF        [HL].0, ??RpRegRead_2  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  501 
//  502 #ifdef RP_TXRX_RAM_BIT_SWAP
//  503 	if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
//  504 	{
//  505 		SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_LSB | SAU_LENGTH_8;
//  506 	}
//  507 #endif
//  508 
//  509 	rData = RP_CSI_BUF;
        MOV       A, 0xFFF48         ;; 1 cycle
//  510 	RpCSI_CSDeactivate();
        SET1      S:0xFFF01.6        ;; 2 cycles
//  511 
//  512 	return  rData;
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 42 cycles
        REQUIRE __A_P1
        REQUIRE __A_SMR10
        REQUIRE __A_IF0
        REQUIRE __A_SDR10
//  513 }
//  514 
//  515 /***************************************************************************************************************
//  516  * function name  : RpCSIInitPort
//  517  * description    : CS line control function.
//  518  * parameters     : none
//  519  * return value   : none
//  520  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon2
          CFI Function _RpCSIInitPort
          CFI NoCalls
        CODE
//  521 static void RpCSIInitPort( void )
//  522 {
_RpCSIInitPort:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  523 	// STANDBY,OSCDRVSEL,DON,RFRESETB Low output
//  524 	RP_STANDBY_PORT 	= RP_PORT_LO;
        CLR1      S:0xFFF0D.0        ;; 2 cycles
//  525 	RP_OSCDRVSEL_PORT 	= RP_PORT_LO;
        CLR1      S:0xFFF01.0        ;; 2 cycles
//  526 	RP_OSCDRVSEL_PM 	= RP_PORT_OUTPUT;
        CLR1      0xFFF21.0          ;; 2 cycles
//  527 	RP_DON_PORT 		= RP_PORT_LO;
        CLR1      S:0xFFF01.1        ;; 2 cycles
//  528 	RP_DON_PM 			= RP_PORT_OUTPUT;
        CLR1      0xFFF21.1          ;; 2 cycles
//  529 	RP_RFRESETB_PORT 	= RP_PORT_LO;
        CLR1      S:0xFFF01.2        ;; 2 cycles
//  530 	RP_RFRESETB_PM 		= RP_PORT_OUTPUT;
        CLR1      0xFFF21.2          ;; 2 cycles
//  531 
//  532 	// MCU - RFIC Serial Setting 4Mbps, CS, RX, TX, CLK
//  533 	RP_CSI_CS_PORT 		= RP_PORT_HI;
        SET1      S:0xFFF01.6        ;; 2 cycles
//  534 	RP_CSI_CS_PM 		= RP_PORT_OUTPUT;
        CLR1      0xFFF21.6          ;; 2 cycles
//  535 	RP_CSI_RX_PM		= RP_PORT_INPUT;
        SET1      0xFFF21.4          ;; 2 cycles
//  536 	RP_CSI_RX_PULLUP	= 1;
        SET1      0xF0031.4          ;; 2 cycles
//  537 	RP_CSI_TX_PORT 		= RP_PORT_HI;
        SET1      S:0xFFF01.3        ;; 2 cycles
//  538 	RP_CSI_TX_PM		= RP_PORT_OUTPUT;
        CLR1      0xFFF21.3          ;; 2 cycles
//  539 	RP_CSI_CLK_PORT 	= RP_PORT_HI;
        SET1      S:0xFFF01.5        ;; 2 cycles
//  540 	RP_CSI_CLK_PM 		= RP_PORT_OUTPUT;
        CLR1      0xFFF21.5          ;; 2 cycles
//  541 
//  542 	// INTP
//  543 	RP_INTP_PM			= RP_PORT_INPUT;
        SET1      0xFFF23.0          ;; 2 cycles
//  544 	RP_INTP_PULLUP		= 1;
        SET1      0xF0033.0          ;; 2 cycles
//  545 
//  546 #if defined(MCU_R78G1H)
//  547 	// unused ports
//  548 	P0_bit.no0 = RP_PORT_LO;
        CLR1      S:0xFFF00.0        ;; 2 cycles
//  549 	PM0_bit.no0 = RP_PORT_OUTPUT;
        CLR1      0xFFF20.0          ;; 2 cycles
//  550 	P0_bit.no1 = RP_PORT_LO;
        CLR1      S:0xFFF00.1        ;; 2 cycles
//  551 	PM0_bit.no1 = RP_PORT_OUTPUT;
        CLR1      0xFFF20.1          ;; 2 cycles
//  552 	P0_bit.no5 = RP_PORT_LO;
        CLR1      S:0xFFF00.5        ;; 2 cycles
//  553 	PM0_bit.no5 = RP_PORT_OUTPUT;
        CLR1      0xFFF20.5          ;; 2 cycles
//  554 	P0_bit.no6 = RP_PORT_LO;
        CLR1      S:0xFFF00.6        ;; 2 cycles
//  555 	PM0_bit.no6 = RP_PORT_OUTPUT;
        CLR1      0xFFF20.6          ;; 2 cycles
//  556 	P1_bit.no7 = RP_PORT_LO;
        CLR1      S:0xFFF01.7        ;; 2 cycles
//  557 	PM1_bit.no7 = RP_PORT_OUTPUT;
        CLR1      0xFFF21.7          ;; 2 cycles
//  558 	P2_bit.no3 = RP_PORT_LO;
        CLR1      S:0xFFF02.3        ;; 2 cycles
//  559 	PM2_bit.no3 = RP_PORT_OUTPUT;
        CLR1      0xFFF22.3          ;; 2 cycles
//  560 	P2_bit.no4 = RP_PORT_LO;
        CLR1      S:0xFFF02.4        ;; 2 cycles
//  561 	PM2_bit.no4 = RP_PORT_OUTPUT;
        CLR1      0xFFF22.4          ;; 2 cycles
//  562 	P2_bit.no5 = RP_PORT_LO;
        CLR1      S:0xFFF02.5        ;; 2 cycles
//  563 	PM2_bit.no5 = RP_PORT_OUTPUT;
        CLR1      0xFFF22.5          ;; 2 cycles
//  564 	P2_bit.no6 = RP_PORT_LO;
        CLR1      S:0xFFF02.6        ;; 2 cycles
//  565 	PM2_bit.no6 = RP_PORT_OUTPUT;
        CLR1      0xFFF22.6          ;; 2 cycles
//  566 	P2_bit.no7 = RP_PORT_LO;
        CLR1      S:0xFFF02.7        ;; 2 cycles
//  567 	PM2_bit.no7 = RP_PORT_OUTPUT;
        CLR1      0xFFF22.7          ;; 2 cycles
//  568 	P4_bit.no1 = RP_PORT_LO;
        CLR1      S:0xFFF04.1        ;; 2 cycles
//  569 	PM4_bit.no1 = RP_PORT_OUTPUT;
        CLR1      0xFFF24.1          ;; 2 cycles
//  570 	P4_bit.no2 = RP_PORT_LO;
        CLR1      S:0xFFF04.2        ;; 2 cycles
//  571 	PM4_bit.no2 = RP_PORT_OUTPUT;
        CLR1      0xFFF24.2          ;; 2 cycles
//  572 	P4_bit.no3 = RP_PORT_LO;
        CLR1      S:0xFFF04.3        ;; 2 cycles
//  573 	PM4_bit.no3 = RP_PORT_OUTPUT;
        CLR1      0xFFF24.3          ;; 2 cycles
//  574 	P4_bit.no4 = RP_PORT_LO;
        CLR1      S:0xFFF04.4        ;; 2 cycles
//  575 	PM4_bit.no4 = RP_PORT_OUTPUT;
        CLR1      0xFFF24.4          ;; 2 cycles
//  576 	P4_bit.no5 = RP_PORT_LO;
        CLR1      S:0xFFF04.5        ;; 2 cycles
//  577 	PM4_bit.no5 = RP_PORT_OUTPUT;
        CLR1      0xFFF24.5          ;; 2 cycles
//  578 	P4_bit.no6 = RP_PORT_LO;
        CLR1      S:0xFFF04.6        ;; 2 cycles
//  579 	PM4_bit.no6 = RP_PORT_OUTPUT;
        CLR1      0xFFF24.6          ;; 2 cycles
//  580 	P4_bit.no7 = RP_PORT_LO;
        CLR1      S:0xFFF04.7        ;; 2 cycles
//  581 	PM4_bit.no7 = RP_PORT_OUTPUT;
        CLR1      0xFFF24.7          ;; 2 cycles
//  582 	P5_bit.no0 = RP_PORT_LO;
        CLR1      S:0xFFF05.0        ;; 2 cycles
//  583 	PM5_bit.no0 = RP_PORT_OUTPUT;
        CLR1      0xFFF25.0          ;; 2 cycles
//  584 	P5_bit.no1 = RP_PORT_LO;
        CLR1      S:0xFFF05.1        ;; 2 cycles
//  585 	PM5_bit.no1 = RP_PORT_OUTPUT;
        CLR1      0xFFF25.1          ;; 2 cycles
//  586 	P5_bit.no2 = RP_PORT_LO;
        CLR1      S:0xFFF05.2        ;; 2 cycles
//  587 	PM5_bit.no2 = RP_PORT_OUTPUT;
        CLR1      0xFFF25.2          ;; 2 cycles
//  588 	P5_bit.no3 = RP_PORT_LO;
        CLR1      S:0xFFF05.3        ;; 2 cycles
//  589 	PM5_bit.no3 = RP_PORT_OUTPUT;
        CLR1      0xFFF25.3          ;; 2 cycles
//  590 	P5_bit.no4 = RP_PORT_LO;
        CLR1      S:0xFFF05.4        ;; 2 cycles
//  591 	PM5_bit.no4 = RP_PORT_OUTPUT;
        CLR1      0xFFF25.4          ;; 2 cycles
//  592 	P5_bit.no5 = RP_PORT_LO;
        CLR1      S:0xFFF05.5        ;; 2 cycles
//  593 	PM5_bit.no5 = RP_PORT_OUTPUT;
        CLR1      0xFFF25.5          ;; 2 cycles
//  594 	P5_bit.no6 = RP_PORT_LO;
        CLR1      S:0xFFF05.6        ;; 2 cycles
//  595 	PM5_bit.no6 = RP_PORT_OUTPUT;
        CLR1      0xFFF25.6          ;; 2 cycles
//  596 	P5_bit.no7 = RP_PORT_LO;
        CLR1      S:0xFFF05.7        ;; 2 cycles
//  597 	PM5_bit.no7 = RP_PORT_OUTPUT;
        CLR1      0xFFF25.7          ;; 2 cycles
//  598 	P6_bit.no4 = RP_PORT_LO;
        CLR1      S:0xFFF06.4        ;; 2 cycles
//  599 	PM6_bit.no4 = RP_PORT_OUTPUT;
        CLR1      0xFFF26.4          ;; 2 cycles
//  600 	P6_bit.no5 = RP_PORT_LO;
        CLR1      S:0xFFF06.5        ;; 2 cycles
//  601 	PM6_bit.no5 = RP_PORT_OUTPUT;
        CLR1      0xFFF26.5          ;; 2 cycles
//  602 	P6_bit.no6 = RP_PORT_LO;
        CLR1      S:0xFFF06.6        ;; 2 cycles
//  603 	PM6_bit.no6 = RP_PORT_OUTPUT;
        CLR1      0xFFF26.6          ;; 2 cycles
//  604 	P6_bit.no7 = RP_PORT_LO;
        CLR1      S:0xFFF06.7        ;; 2 cycles
//  605 	PM6_bit.no7 = RP_PORT_OUTPUT;
        CLR1      0xFFF26.7          ;; 2 cycles
//  606 	P7_bit.no3 = RP_PORT_LO;
        CLR1      S:0xFFF07.3        ;; 2 cycles
//  607 	PM7_bit.no3 = RP_PORT_OUTPUT;
        CLR1      0xFFF27.3          ;; 2 cycles
//  608 	P7_bit.no4 = RP_PORT_LO;
        CLR1      S:0xFFF07.4        ;; 2 cycles
//  609 	PM7_bit.no4 = RP_PORT_OUTPUT;
        CLR1      0xFFF27.4          ;; 2 cycles
//  610 	P8_bit.no3 = RP_PORT_LO;
        CLR1      S:0xFFF08.3        ;; 2 cycles
//  611 	PM8_bit.no3 = RP_PORT_OUTPUT;
        CLR1      0xFFF28.3          ;; 2 cycles
//  612 	P8_bit.no4 = RP_PORT_LO;
        CLR1      S:0xFFF08.4        ;; 2 cycles
//  613 	PM8_bit.no4 = RP_PORT_OUTPUT;
        CLR1      0xFFF28.4          ;; 2 cycles
//  614 	P8_bit.no5 = RP_PORT_LO;
        CLR1      S:0xFFF08.5        ;; 2 cycles
//  615 	PM8_bit.no5 = RP_PORT_OUTPUT;
        CLR1      0xFFF28.5          ;; 2 cycles
//  616 	P8_bit.no6 = RP_PORT_LO;
        CLR1      S:0xFFF08.6        ;; 2 cycles
//  617 	PM8_bit.no6 = RP_PORT_OUTPUT;
        CLR1      0xFFF28.6          ;; 2 cycles
//  618 	P8_bit.no7 = RP_PORT_LO;
        CLR1      S:0xFFF08.7        ;; 2 cycles
//  619 	PM8_bit.no7 = RP_PORT_OUTPUT;
        CLR1      0xFFF28.7          ;; 2 cycles
//  620 	P10_bit.no1 = RP_PORT_LO;
        CLR1      S:0xFFF0A.1        ;; 2 cycles
//  621 	PM10_bit.no1 = RP_PORT_OUTPUT;
        CLR1      0xFFF2A.1          ;; 2 cycles
//  622 	P10_bit.no2 = RP_PORT_LO;
        CLR1      S:0xFFF0A.2        ;; 2 cycles
//  623 	PM10_bit.no2 = RP_PORT_OUTPUT;
        CLR1      0xFFF2A.2          ;; 2 cycles
//  624 	P11_bit.no0 = RP_PORT_LO;
        CLR1      S:0xFFF0B.0        ;; 2 cycles
//  625 	PM11_bit.no0 = RP_PORT_OUTPUT;
        CLR1      0xFFF2B.0          ;; 2 cycles
//  626 	P11_bit.no1 = RP_PORT_LO;
        CLR1      S:0xFFF0B.1        ;; 2 cycles
//  627 	PM11_bit.no1 = RP_PORT_OUTPUT;
        CLR1      0xFFF2B.1          ;; 2 cycles
//  628 	P14_bit.no5 = RP_PORT_LO;
        CLR1      S:0xFFF0E.5        ;; 2 cycles
//  629 	PM14_bit.no5 = RP_PORT_OUTPUT;
        CLR1      0xFFF2E.5          ;; 2 cycles
//  630 	P14_bit.no6 = RP_PORT_LO;
        CLR1      S:0xFFF0E.6        ;; 2 cycles
//  631 	PM14_bit.no6 = RP_PORT_OUTPUT;
        CLR1      0xFFF2E.6          ;; 2 cycles
//  632 	P14_bit.no7 = RP_PORT_LO;
        CLR1      S:0xFFF0E.7        ;; 2 cycles
//  633 	PM14_bit.no7 = RP_PORT_OUTPUT;
        CLR1      0xFFF2E.7          ;; 2 cycles
//  634 	P15_bit.no0 = RP_PORT_LO;
        CLR1      S:0xFFF0F.0        ;; 2 cycles
//  635 	PM15_bit.no0 = RP_PORT_OUTPUT;
        CLR1      0xFFF2F.0          ;; 2 cycles
//  636 	P15_bit.no1 = RP_PORT_LO;
        CLR1      S:0xFFF0F.1        ;; 2 cycles
//  637 	PM15_bit.no1 = RP_PORT_OUTPUT;
        CLR1      0xFFF2F.1          ;; 2 cycles
//  638 	P15_bit.no2 = RP_PORT_LO;
        CLR1      S:0xFFF0F.2        ;; 2 cycles
//  639 	PM15_bit.no2 = RP_PORT_OUTPUT;
        CLR1      0xFFF2F.2          ;; 2 cycles
//  640 	P15_bit.no3 = RP_PORT_LO;
        CLR1      S:0xFFF0F.3        ;; 2 cycles
//  641 	PM15_bit.no3 = RP_PORT_OUTPUT;
        CLR1      0xFFF2F.3          ;; 2 cycles
//  642 	P15_bit.no4 = RP_PORT_LO;
        CLR1      S:0xFFF0F.4        ;; 2 cycles
//  643 	PM15_bit.no4 = RP_PORT_OUTPUT;
        CLR1      0xFFF2F.4          ;; 2 cycles
//  644 #endif
//  645 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 232 cycles
        ; ------------------------------------- Total: 232 cycles
        REQUIRE __A_P13
        REQUIRE __A_P1
        REQUIRE __A_PM1
        REQUIRE __A_PU1
        REQUIRE __A_PM3
        REQUIRE __A_PU3
        REQUIRE __A_P0
        REQUIRE __A_PM0
        REQUIRE __A_P2
        REQUIRE __A_PM2
        REQUIRE __A_P4
        REQUIRE __A_PM4
        REQUIRE __A_P5
        REQUIRE __A_PM5
        REQUIRE __A_P6
        REQUIRE __A_PM6
        REQUIRE __A_P7
        REQUIRE __A_PM7
        REQUIRE __A_P8
        REQUIRE __A_PM8
        REQUIRE __A_P10
        REQUIRE __A_PM10
        REQUIRE __A_P11
        REQUIRE __A_PM11
        REQUIRE __A_P14
        REQUIRE __A_PM14
        REQUIRE __A_P15
        REQUIRE __A_PM15
//  646 
//  647 /***************************************************************************************************************
//  648  * function name  : RpCheckRfIRQ
//  649  * description    : Check IRQ function.
//  650  * parameters     : none
//  651  * return value   : none
//  652  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon2
          CFI Function _RpCheckRfIRQ
          CFI NoCalls
        CODE
//  653 uint8_t RpCheckRfIRQ( void )
//  654 {
_RpCheckRfIRQ:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  655 	uint8_t rtn;
//  656 
//  657 	if (RP_INTP_PORT == RP_INTP_PORT_ACTIVE)
//  658 	{
//  659 		rtn = RP_TRUE;
//  660 	}
//  661 	else
//  662 	{
//  663 		rtn = RP_FALSE;
//  664 	}
//  665 
//  666 	return (rtn);
        MOV       A, S:0xFFF03       ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
        REQUIRE __A_P3
//  667 }
//  668 
//  669 /***************************************************************************************************************
//  670  * function name  : RpCSIInitialize
//  671  * description    : CSI Initialize function.
//  672  * parameters     : none
//  673  * return value   : none
//  674  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon2
          CFI Function _RpCSIInitialize
          CFI NoCalls
        CODE
//  675 static void RpCSIInitialize( void )
//  676 {
_RpCSIInitialize:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  677 	SAU1EN = 1;	// SAU1 clock supply
        SET1      0xF00F0.3          ;; 2 cycles
//  678 
//  679 	NOP();
        NOP                          ;; 1 cycle
//  680 	NOP();
        NOP                          ;; 1 cycle
//  681 	NOP();
        NOP                          ;; 1 cycle
//  682 	NOP();
        NOP                          ;; 1 cycle
//  683 
//  684 #if RP_CPU_CLK == 32
//  685 	SPS1 = SAU_CK01_FCLK_1 | (SPS1 & 0x0F);		// base CK01
        MOVW      AX, 0x166          ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
        OR        A, #0x10           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x166, AX          ;; 1 cycle
//  686 #else // RP_CPU_CLK == 8
//  687 	SPS1 = SAU_CK01_FCLK_0 | (SPS1 & 0x0F);		// base CK01
//  688 #endif
//  689 	// CSI20 initial setting
//  690 	ST1 |= SAU_CH0_STOP_TRG_ON;		// CSI20 disable
        SET1      0xF0164.0          ;; 2 cycles
//  691 	CSIMK20 = 1;					// INTCSI20 disable
        SET1      0xFFFE5.0          ;; 2 cycles
//  692 	CSIIF20 = 0;					// INTCSI20 IF clear
        CLR1      0xFFFE1.0          ;; 2 cycles
//  693 	SIR10 = SAU_SIRMN_FECTMN | SAU_SIRMN_PECTMN | SAU_SIRMN_OVCTMN;		// error flag clear
        MOV       X, #0x7            ;; 1 cycle
        MOVW      0x148, AX          ;; 1 cycle
//  694 	SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_TRANSFER_END;
        MOVW      AX, #0x8020        ;; 1 cycle
        MOVW      0x150, AX          ;; 1 cycle
//  695 	SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
        MOVW      AX, #0xC007        ;; 1 cycle
        MOVW      0x158, AX          ;; 1 cycle
//  696 	SDR10 = 0x0200; // 32MHz / 2 / 4 = 4Mbps or 8MHz / 4 = 2Mbps
        MOVW      0xFFF48, #0x200    ;; 1 cycle
//  697 	SO1	&= ~SAU_CH0_CLOCK_OUTPUT_1;		// CSI20 clock initial level
        MOVW      AX, 0x168          ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        MOVW      0x168, AX          ;; 1 cycle
//  698 	SO1	&= ~SAU_CH0_DATA_OUTPUT_1;		// CSI20 SO initial level
        MOVW      AX, 0x168          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x168, AX          ;; 1 cycle
//  699 	SOE1 |= SAU_CH0_OUTPUT_ENABLE;		// CSI20 output enable
        SET1      0xF016A.0          ;; 2 cycles
//  700 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 42 cycles
        ; ------------------------------------- Total: 42 cycles
        REQUIRE __A_PER0
        REQUIRE __A_SPS1
        REQUIRE __A_ST1
        REQUIRE __A_MK0
        REQUIRE __A_IF0
        REQUIRE __A_SIR10
        REQUIRE __A_SMR10
        REQUIRE __A_SCR10
        REQUIRE __A_SDR10
        REQUIRE __A_SO1
        REQUIRE __A_SOE1
//  701 
//  702 /***************************************************************************************************************
//  703  * function name  : RpRFTimerInitialize
//  704  * description    : RF Timer Initialize function.
//  705  * parameters     : none
//  706  * return value   : none
//  707  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon2
          CFI Function _RpRFTimerInitialize
          CFI NoCalls
        CODE
//  708 static void RpRFTimerInitialize( void )
//  709 {
_RpRFTimerInitialize:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  710 	// General Timer Setting (TA0 1uS)
//  711 	TAU0EN = 1;					// supplies input clock
        SET1      0xF00F0.0          ;; 2 cycles
//  712 	TT0 |= TAU_CH0_STOP_TRG_ON;
        SET1      0xF01B4.0          ;; 2 cycles
//  713 	TPS0 &= ~0x00f0;			// rewrite CK01
        MOVW      AX, 0x1B6          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x1B6, AX          ;; 1 cycle
//  714 #if RP_CPU_CLK == 32
//  715 	TPS0 |= TAU_CK01_FCLK_10;	// rewrite! for 31.25KHz(32uS) CK01
        MOVW      AX, 0x1B6          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, #0xA0           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x1B6, AX          ;; 1 cycle
//  716 #else // if RP_CPU_CLK == 8
//  717 	TPS0 |= TAU_CK01_FCLK_8;	// rewrite! for 31.25KHz(32uS) CK01
//  718 #endif
//  719 #if (RP_USR_RF_TAU_CH_SELECT == 1)
//  720 	if (RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_FASTOCO)
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        CMP0      ES:_RpConfig+9     ;; 2 cycles
        BNZ       ??RpGetInterruptLevel_1  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
//  721 #endif
//  722 	{
//  723 		RpClkTick = 32;
        MOV       ES, #BYTE3(_RpClkTick)  ;; 1 cycle
        MOV       ES:_RpClkTick, #0x20  ;; 2 cycles
        BR        S:??RpGetInterruptLevel_2  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  724 	}// 	if(RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_FASTOCO
//  725 #if (RP_USR_RF_TAU_CH_SELECT == 1)
//  726 	else if (RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_32KHz)
??RpGetInterruptLevel_1:
        CMP       ES:_RpConfig+9, #0x1  ;; 2 cycles
        BNZ       ??RpGetInterruptLevel_2  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  727 	{
//  728 		RpClkTick = 30;
        MOV       ES, #BYTE3(_RpClkTick)  ;; 1 cycle
        MOV       ES:_RpClkTick, #0x1E  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
//  729 	}
//  730 #endif
//  731 	// Mask channel 0 interrupt
//  732 	TMMK00 = 1;		// INTTM00 disabled
??RpGetInterruptLevel_2:
        SET1      0xFFFE6.4          ;; 2 cycles
//  733 	TMIF00 = 0;		// INTTM00 interrupt flag clear
        CLR1      0xFFFE2.4          ;; 2 cycles
//  734 	// Set INTTM00 low priority
//  735 	TMPR100 = RP_PHY_INTLEVEL_RFTimers_b1;
        CLR1      0xFFFEE.4          ;; 2 cycles
//  736 	TMPR000 = RP_PHY_INTLEVEL_RFTimers_b0;	// Level_1
        SET1      0xFFFEA.4          ;; 2 cycles
//  737 	// Channel 0 used as interval timer
//  738 	TMR00 = TAU_TMR0_INITIALVALUE | TAU_CLOCK_SELECT_CK01 | TAU_CLOCK_MODE_CKS | TAU_MODE_INTERVAL_TIMER | TAU_START_INT_UNUSED;
        MOVW      AX, #0x8000        ;; 1 cycle
        MOVW      0x190, AX          ;; 1 cycle
//  739 	TDR00 = 0xffff;						// default
        MOVW      S:0xFFF18, #0xFFFF  ;; 1 cycle
//  740 	TOM0 &= ~TAU_CH0_OUTPUT_COMBIN ;
        MOVW      AX, 0x1BE          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x1BE, AX          ;; 1 cycle
//  741 	TOL0 &= ~TAU_CH0_OUTPUT_LEVEL_L;
        MOVW      AX, 0x1BC          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFE           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x1BC, AX          ;; 1 cycle
//  742 	TOE0 &= ~TAU_CH0_OUTPUT_ENABLE;
        CLR1      0xF01BA.0          ;; 2 cycles
//  743 
//  744 	// 10% Timer Setting
//  745 #if (RP_USR_RF_TAU_CH_SELECT == 1)
//  746 	//   Timer settings(16bit Timer01) for 10% Limitation
//  747 	TT0 |= TAU_CH1_STOP_TRG_ON;
        MOVW      HL, #0x1B4         ;; 1 cycle
        SET1      [HL].1             ;; 2 cycles
//  748 	// Mask channel 1 interrupt
//  749 	TMMK01 = 1;		// INTTM01 disabled
        SET1      0xFFFE6.5          ;; 2 cycles
//  750 	TMIF01 = 0;		// INTTM01 interrupt flag clear
        CLR1      0xFFFE2.5          ;; 2 cycles
//  751 	// Set INTTM01 low priority
//  752 	TMPR001 = RP_PHY_INTLEVEL_RFTimers_b0;
        SET1      0xFFFEA.5          ;; 2 cycles
//  753 	TMPR101 = RP_PHY_INTLEVEL_RFTimers_b1;
        CLR1      0xFFFEE.5          ;; 2 cycles
//  754 	// Channel 1 used as interval timer
//  755 	if (RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_FASTOCO)
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        CMP0      ES:_RpConfig+9     ;; 2 cycles
        BNZ       ??RpGetInterruptLevel_3  ;; 4 cycles
        ; ------------------------------------- Block: 41 cycles
//  756 	{
//  757 		TMR01 = TAU_TMR0_INITIALVALUE | TAU_CLOCK_SELECT_CK01 | TAU_CLOCK_MODE_CKS | TAU_MODE_INTERVAL_TIMER | TAU_START_INT_UNUSED;
        MOVW      AX, #0x8000        ;; 1 cycle
        MOVW      0x192, AX          ;; 1 cycle
//  758 		TIS0 = 0;
        MOV       0x74, #0x0         ;; 1 cycle
//  759 		TDR01 = 31719;//31250; mergine plus 1.5%
        MOVW      S:0xFFF1A, #0x7BE7  ;; 1 cycle
        BR        S:??RpGetInterruptLevel_4  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  760 	}
//  761 	else if (RpConfig.tmrClkSelect == RP_CLK_LIMIT_TMR_32KHz)
??RpGetInterruptLevel_3:
        CMP       ES:_RpConfig+9, #0x1  ;; 2 cycles
        BNZ       ??RpGetInterruptLevel_4  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  762 	{
//  763 		TMR01 = TAU_TMR0_INITIALVALUE | TAU_CLOCK_SELECT_CK00 | TAU_CLOCK_MODE_CKS | TAU_MODE_INTERVAL_TIMER | TAU_START_INT_UNUSED | TAU_CLOCK_MODE_TI0N;
        MOVW      AX, #0x1000        ;; 1 cycle
        MOVW      0x192, AX          ;; 1 cycle
//  764 		OSMC &= (~0x10);// WUTMMCK0 = 0(SubClk);	12bitTimer, RTC, TRJ Clock Source is Sub Clock
        MOVW      HL, #0xF3          ;; 1 cycle
        MOV       A, [HL]            ;; 1 cycle
        AND       A, #0xEF           ;; 1 cycle
        MOV       [HL], A            ;; 1 cycle
//  765 		TIS0 = 0x05;	// Sub-CLK
        MOV       0x74, #0x5         ;; 1 cycle
//  766 		TDR01 = 32768;
        MOVW      S:0xFFF1A, #0x8000  ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
//  767 	}
//  768 	TOM0 &= ~TAU_CH1_OUTPUT_COMBIN ;
??RpGetInterruptLevel_4:
        MOVW      AX, 0x1BE          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFD           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x1BE, AX          ;; 1 cycle
//  769 	TOL0 &= ~TAU_CH1_OUTPUT_LEVEL_L;
        MOVW      AX, 0x1BC          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xFD           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x1BC, AX          ;; 1 cycle
//  770 	TOE0 &= ~TAU_CH1_OUTPUT_ENABLE;
        CLR1      0xF01BA.1          ;; 2 cycles
//  771 
//  772 #else  /* RP_USR_RF_TAU_CH_SELECT != 1 */
//  773 	//   Timer settings(16bit Timer02) for 10% Limitation
//  774 	TT0 |= TAU_CH2_STOP_TRG_ON;
//  775 	// Mask channel 2 interrupt
//  776 	TMMK02 = 1;		// INTTM02 disabled
//  777 	TMIF02 = 0;		// INTTM02 interrupt flag clear
//  778 	// Set INTTM02 low priority
//  779 	TMPR002 = RP_PHY_INTLEVEL_RFTimers_b0;
//  780 	TMPR102 = RP_PHY_INTLEVEL_RFTimers_b1;
//  781 	// Channel 2 used as interval timer
//  782 	TMR02 = TAU_TMR0_INITIALVALUE | TAU_CLOCK_SELECT_CK01 | TAU_CLOCK_MODE_CKS | TAU_MODE_INTERVAL_TIMER | TAU_START_INT_UNUSED;
//  783 	TIS0 = 0;
//  784 	TDR02 = 31719;//31250; mergine plus 1.5%
//  785 
//  786 	TOM0 &= ~TAU_CH2_OUTPUT_COMBIN ;
//  787 	TOL0 &= ~TAU_CH2_OUTPUT_LEVEL_L;
//  788 	TOE0 &= ~TAU_CH2_OUTPUT_ENABLE;
//  789 
//  790 #endif  /* RP_USR_RF_TAU_CH_SELECT */
//  791 };
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 116 cycles
        REQUIRE __A_PER0
        REQUIRE __A_TT0
        REQUIRE __A_TPS0
        REQUIRE __A_MK1
        REQUIRE __A_IF1
        REQUIRE __A_PR11
        REQUIRE __A_PR01
        REQUIRE __A_TMR00
        REQUIRE __A_TDR00
        REQUIRE __A_TOM0
        REQUIRE __A_TOL0
        REQUIRE __A_TOE0
        REQUIRE __A_TMR01
        REQUIRE __A_TIS0
        REQUIRE __A_TDR01
        REQUIRE __A_OSMC
//  792 
//  793 /***************************************************************************************************************
//  794  * function name  : RpLimitTimerControl
//  795  * description    : Limitation Timer Control
//  796  * parameters     : timerOn: start timer
//  797  * return value   : lack time
//  798  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon3
          CFI Function _RpLimitTimerControl
          CFI NoCalls
        CODE
//  799 uint32_t RpLimitTimerControl( uint8_t timerOn )
//  800 {
_RpLimitTimerControl:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  801 	uint32_t	lackTime = 0;
        CLRB      X                  ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
//  802 
//  803 #if (RP_USR_RF_TAU_CH_SELECT == 1)
//  804 	TT0 |= TAU_CH1_STOP_TRG_ON;
        MOVW      HL, #0x1B4         ;; 1 cycle
        SET1      [HL].1             ;; 2 cycles
//  805 	TMMK01 = 1;			// INTTM01 disabled
        SET1      0xFFFE6.5          ;; 2 cycles
//  806 	if (timerOn == RP_TRUE)
        CMP       A, #0x1            ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        BNZ       ??RpGetInterruptLevel_5  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
//  807 	{
//  808 		TMMK01 = 0;		// INTTM01 enabled
        CLR1      0xFFFE6.5          ;; 2 cycles
//  809 		TS0 |= TAU_CH1_START_TRG_ON;
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        SET1      [HL].1             ;; 2 cycles
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 12 cycles
//  810 	}
//  811 	else
//  812 	{
//  813 		lackTime = (uint32_t)((uint32_t)(TDR01 - TCR01) * (uint32_t)RpClkTick);
??RpGetInterruptLevel_5:
        MOVW      AX, S:0xFFF1A      ;; 1 cycle
        MOV       ES, #BYTE3(_RpClkTick)  ;; 1 cycle
        MOV       C, ES:_RpClkTick   ;; 2 cycles
        SUBW      AX, 0x182          ;; 1 cycle
        MULHU                        ;; 2 cycles
//  814 	}
//  815 
//  816 #else /* RP_USR_RF_TAU_CH_SELECT != 1 */
//  817 	TT0 |= TAU_CH2_STOP_TRG_ON;
//  818 	TMMK02 = 1;			// INTTM02 disabled
//  819 	if (timerOn == RP_TRUE)
//  820 	{
//  821 		TMMK02 = 0;		// INTTM02 enabled
//  822 		TS0 |= TAU_CH2_START_TRG_ON;
//  823 	}
//  824 	else
//  825 	{
//  826 		lackTime = (uint32_t)((uint32_t)(TDR02 - TCR02) * (uint32_t)RpClkTick);
//  827 	}
//  828 #endif
//  829 
//  830 	return (lackTime);
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 38 cycles
        REQUIRE __A_TT0
        REQUIRE __A_MK1
        REQUIRE __A_TS0
        REQUIRE __A_TDR01
        REQUIRE __A_TCR01
//  831 }
//  832 
//  833 /***************************************************************************************************************
//  834  * function name  : RpMcuPeripheralInit
//  835  * description    : CS line control function.
//  836  * parameters     : none
//  837  * return value   : none
//  838  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon2
          CFI Function _RpMcuPeripheralInit
          CFI FunCall _RpCSIInitPort
        CODE
//  839 void RpMcuPeripheralInit( void )
//  840 {
_RpMcuPeripheralInit:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  841 	RpCSIInitPort();
        CALL      F:_RpCSIInitPort   ;; 3 cycles
//  842 	RpCSIInitialize();
          CFI FunCall _RpCSIInitialize
        CALL      F:_RpCSIInitialize  ;; 3 cycles
//  843 #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
//  844 	RpDmaSerInitial();
          CFI FunCall _RpDmaSerInitial
        CALL      F:_RpDmaSerInitial  ;; 3 cycles
//  845 #endif
//  846 	RpRFTimerInitialize();
          CFI FunCall _RpRFTimerInitialize
        CALL      F:_RpRFTimerInitialize  ;; 3 cycles
//  847 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 18 cycles
//  848 
//  849 /***************************************************************************************************************
//  850  * function name  : RpWakeupSequence
//  851  * description    : RF Initial Sequence function.
//  852  * parameters     : none
//  853  * return value   : none
//  854  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon2
          CFI Function _RpWakeupSequence
        CODE
//  855 void RpWakeupSequence( void )
//  856 {
_RpWakeupSequence:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  857 	SS1 |= SAU_CH0_START_TRG_ON;	// CSI20 enable
        MOVW      HL, #0x162         ;; 1 cycle
        SET1      [HL].0             ;; 2 cycles
//  858 
//  859 	RP_CSI_BUF = 0xff;
        MOV       0xFFF48, #0xFF     ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
//  860 	while (RP_CSI_IRQ == 0);
??RpWakeupSequence_0:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpWakeupSequence_0  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  861 	RP_CSI_IRQ = 0;
        CLR1      0xFFFE1.0          ;; 2 cycles
//  862 
//  863 	TT0 |= TAU_CH0_STOP_TRG_ON;
        MOVW      HL, #0x1B4         ;; 1 cycle
        SET1      [HL].0             ;; 2 cycles
//  864 
//  865 	if (RpConfig.rfClkSource == RP_RFCLK_SOURCE_CRYSTAL)
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        CMP0      ES:_RpConfig+8     ;; 2 cycles
        BNZ       ??RpGetInterruptLevel_6  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  866 	{
//  867 		// STANDBY = High, 500uS wait
//  868 		TMIF00 = 0;		// INTTM00 interrupt flag clear
        CLR1      0xFFFE2.4          ;; 2 cycles
//  869 		TDR00 = 16 - 1/* 500 - 2 */;
        MOVW      S:0xFFF18, #0xF    ;; 1 cycle
//  870 		RP_STANDBY_PORT = RP_PORT_HI;
        SET1      S:0xFFF0D.0        ;; 2 cycles
//  871 		RP_WAIT_WAKEUP_ACTIVE();
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        SET1      [HL].0             ;; 2 cycles
        ; ------------------------------------- Block: 9 cycles
??RpWakeupSequence_1:
        MOVW      HL, #0xFFE2        ;; 1 cycle
        BF        [HL].4, ??RpWakeupSequence_1  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        SET1      0xF01B4.0          ;; 2 cycles
//  872 		// OSDDRVSEL = High, 50uS wait
//  873 		TMIF00 = 0;		// INTTM00 interrupt flag clear
        CLR1      0xFFFE2.4          ;; 2 cycles
//  874 		TDR00 = 1 - 1/* 50 - 2 */;
        MOVW      S:0xFFF18, #0x0    ;; 1 cycle
//  875 		RP_OSCDRVSEL_PORT = RP_PORT_HI;
        SET1      S:0xFFF01.0        ;; 2 cycles
//  876 		RP_WAIT_WAKEUP_ACTIVE();
        MOVW      HL, #0x1B2         ;; 1 cycle
        SET1      [HL].0             ;; 2 cycles
        ; ------------------------------------- Block: 10 cycles
??RpWakeupSequence_2:
        MOVW      HL, #0xFFE2        ;; 1 cycle
        BF        [HL].4, ??RpWakeupSequence_2  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      HL, #0x1B4         ;; 1 cycle
        SET1      [HL].0             ;; 2 cycles
        BR        S:??RpGetInterruptLevel_7  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  877 	}
//  878 	else// if RpConfig.rfClkSource == RP_RFCLK_SOURCE_CRYSTAL
//  879 	{
//  880 		RP_STANDBY_PORT = RP_PORT_HI;
??RpGetInterruptLevel_6:
        SET1      S:0xFFF0D.0        ;; 2 cycles
//  881 		RP_OSCDRVSEL_PORT = RP_PORT_HI;
        SET1      S:0xFFF01.0        ;; 2 cycles
        ; ------------------------------------- Block: 4 cycles
//  882 	}
//  883 	// DON = High, 450uS wait
//  884 	TMIF00 = 0;		// INTTM00 interrupt flag clear
??RpGetInterruptLevel_7:
        CLR1      0xFFFE2.4          ;; 2 cycles
//  885 	TDR00 = 14 - 1/* 450 - 2 */;
        MOVW      S:0xFFF18, #0xD    ;; 1 cycle
//  886 	RP_DON_PORT = RP_PORT_HI;
        SET1      S:0xFFF01.1        ;; 2 cycles
//  887 	RP_WAIT_WAKEUP_ACTIVE();
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        SET1      [HL].0             ;; 2 cycles
        ; ------------------------------------- Block: 9 cycles
??RpWakeupSequence_3:
        MOVW      HL, #0xFFE2        ;; 1 cycle
        BF        [HL].4, ??RpWakeupSequence_3  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        SET1      0xF01B4.0          ;; 2 cycles
//  888 	// RFRESETB = High calibration start
//  889 	RP_RFRESETB_PORT = RP_PORT_HI;
        SET1      S:0xFFF01.2        ;; 2 cycles
//  890 	// RFSIO and RFIRQ pull-up off
//  891 	RP_CSI_RX_PULLUP	= 0;
        CLR1      0xF0031.4          ;; 2 cycles
//  892 	RP_INTP_PULLUP		= 0;
        MOVW      HL, #0x33          ;; 1 cycle
        CLR1      [HL].0             ;; 2 cycles
//  893 	RpRegWrite(BBRFCON, RFSTART | ANARESETB);			// Analog Reset
        CLR1      S:0xFFF01.6        ;; 2 cycles
        MOVW      AX, #0x8021        ;; 1 cycle
        MOVW      0x150, AX          ;; 1 cycle
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       0xFFF48, #0x0      ;; 1 cycle
        ; ------------------------------------- Block: 16 cycles
??RpWakeupSequence_4:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpWakeupSequence_4  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       0xFFF48, #0x0      ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??RpWakeupSequence_5:
        BF        [HL].0, ??RpWakeupSequence_5  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       0xFFF48, #0x3      ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??RpWakeupSequence_6:
        BF        [HL].0, ??RpWakeupSequence_6  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
        CLR1      0xFFFE1.0          ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
??RpWakeupSequence_7:
        MOV       A, 0x140           ;; 1 cycle
        BT        A.6, ??RpWakeupSequence_7  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        SET1      S:0xFFF01.6        ;; 2 cycles
//  894 	RpExecuteCalibration();
          CFI FunCall _RpExecuteCalibration
        CALL      F:_RpExecuteCalibration  ;; 3 cycles
//  895 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 135 cycles
        REQUIRE __A_SS1
        REQUIRE __A_SDR10
        REQUIRE __A_IF0
        REQUIRE __A_TT0
        REQUIRE __A_IF1
        REQUIRE __A_TDR00
        REQUIRE __A_P13
        REQUIRE __A_TS0
        REQUIRE __A_P1
        REQUIRE __A_PU1
        REQUIRE __A_PU3
        REQUIRE __A_SMR10
        REQUIRE __A_SSR10
//  896 
//  897 /***************************************************************************************************************
//  898  * function name  : RpExecuteCalibration
//  899  * description    : Check RF Calibration function.
//  900  * parameters     : none
//  901  * return value   : none
//  902  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon2
          CFI Function _RpExecuteCalibration
        CODE
//  903 void RpExecuteCalibration( void )
//  904 {
_RpExecuteCalibration:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
//  905 #if defined(MCU_R78G1H)
//  906 	uint8_t readRomData[4];
//  907 	uint8_t mode450KHz = RpRdEvaReg2(0x0E00) & 0x10;
        MOVW      AX, #0xE00         ;; 1 cycle
          CFI FunCall _RpRdEvaReg2
        CALL      F:_RpRdEvaReg2     ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
//  908 
//  909 	RpMemcpy(readRomData, (uint8_t RP_FAR *)0x000EFFEC, sizeof(readRomData));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      BC, #0xFFEC        ;; 1 cycle
        MOV       X, #0xE            ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _RpMemcpy
        CALL      F:_RpMemcpy        ;; 3 cycles
//  910 
//  911 	//	readRomData[0]	0x000EFFEC	0xFA0C
//  912 	//	readRomData[1]	0x000EFFED	0xFA0D
//  913 	//	readRomData[2]	0x000EFFEE	0xFDA0
//  914 	//	readRomData[3]	0x000EFFEF	0xFDA1
//  915 
//  916 	if (mode450KHz)
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      HL, SP             ;; 1 cycle
        BF        [HL].4, ??RpGetInterruptLevel_8  ;; 5 cycles
        ; ------------------------------------- Block: 26 cycles
//  917 	{
//  918 		RpWrEvaReg2(0x0b00 | readRomData[2]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        BR        S:??RpGetInterruptLevel_9  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  919 	}
//  920 	else
//  921 	{
//  922 		RpWrEvaReg2(0x0b00 | readRomData[3]);
??RpGetInterruptLevel_8:
        MOV       A, [SP+0x05]       ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpGetInterruptLevel_9:
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xB            ;; 1 cycle
          CFI FunCall _RpWrEvaReg2
        CALL      F:_RpWrEvaReg2     ;; 3 cycles
//  923 	}
//  924 	RpWrEvaReg2(0x0100 | (readRomData[0] & 0x0f));
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
          CFI FunCall _RpWrEvaReg2
        CALL      F:_RpWrEvaReg2     ;; 3 cycles
//  925 	RpWrEvaReg2(0x0200 | readRomData[1]);
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0x2            ;; 1 cycle
          CFI FunCall _RpWrEvaReg2
        CALL      F:_RpWrEvaReg2     ;; 3 cycles
//  926 #else
//  927 	uint16_t reg;
//  928 
//  929 	RpRegWrite(BBRFCON, RFSTART | ANARESETB | CSONSET);	// Calibration On
//  930 	RpRegWrite(BBINTEN0, (uint8_t)((RpRegRead(BBINTEN0)) | CALINTEN));// Caribration Interrupt Ennable
//  931 	RpRegRead(BBINTREQ0);								// dummy read for clear bit of Caribration Interrupt
//  932 	reg = 0x0000;
//  933 	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
//  934 	reg = 0x0732;
//  935 	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
//  936 	// Calibration Int Wait
//  937 	RpRegWrite(BBCAL, CALSTART);	// Calibration Start
//  938 	while ((RpRegRead(BBINTREQ0) & CALINTREQ) == 0);
//  939 	RpRegWrite(BBRFCON, RFSTART | ANARESETB);			// Calibration Off
//  940 	RpRegWrite(BBINTEN0,	(uint8_t)((RpRegRead(BBINTEN0)) & ~CALINTEN));// Caribration Interrupt Disable
//  941 #endif
//  942 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 27 cycles
        ; ------------------------------------- Total: 58 cycles
//  943 
//  944 /***************************************************************************************************************
//  945  * function name  : RpPowerdownSequence
//  946  * description    : RF Powerdown Sequence function.
//  947  * parameters     : none
//  948  * return value   : none
//  949  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon2
          CFI Function _RpPowerdownSequence
          CFI NoCalls
        CODE
//  950 void RpPowerdownSequence( void )
//  951 {
_RpPowerdownSequence:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  952 	// RFSIO and RFIRQ pull-up on
//  953 	RP_CSI_RX_PULLUP	= 1;
        SET1      0xF0031.4          ;; 2 cycles
//  954 	RP_INTP_PULLUP		= 1;
        SET1      0xF0033.0          ;; 2 cycles
//  955 	// STANDBY,OSCDRVSEL,DON,RFRESETB Low output
//  956 	RP_STANDBY_PORT 	= RP_PORT_LO;
        CLR1      S:0xFFF0D.0        ;; 2 cycles
//  957 	if (RpConfig.rfClkSource == RP_RFCLK_SOURCE_CRYSTAL)
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        CMP0      ES:_RpConfig+8     ;; 2 cycles
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 10 cycles
//  958 	{
//  959 		RP_OSCDRVSEL_PORT 	= RP_PORT_LO;
        CLR1      S:0xFFF01.0        ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
//  960 	}
//  961 	RP_DON_PORT 		= RP_PORT_LO;
??RpPowerdownSequence_0:
        CLR1      S:0xFFF01.1        ;; 2 cycles
//  962 	RP_RFRESETB_PORT 	= RP_PORT_LO;
        CLR1      S:0xFFF01.2        ;; 2 cycles
//  963 	
//  964 	ST1 |= SAU_CH0_STOP_TRG_ON; 	// CSI20 disable
        SET1      0xF0164.0          ;; 2 cycles
//  965 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 24 cycles
        REQUIRE __A_PU1
        REQUIRE __A_PU3
        REQUIRE __A_P13
        REQUIRE __A_P1
        REQUIRE __A_ST1
//  966 
//  967 #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
//  968 /***************************************************************************************************************
//  969  * function name  : RpDmaSerInitial
//  970  * description    : DTC initial function.
//  971  * parameters     : none
//  972  * return value   : none
//  973  **************************************************************************************************************/
//  974 #ifdef __CCRL__
//  975 	#pragma address (dtc_vectortable = DTC_VECTBL_ADDRESS)
//  976 	uint8_t dtc_vectortable[0x40];
//  977 	#pragma address (dtc_controldata_0 = DTC_CTRL0_ADDRESS)
//  978 	uint8_t dtc_controldata_0[8];
//  979 	#pragma address (dtc_controldata_1 = DTC_CTRL1_ADDRESS)
//  980 	uint8_t dtc_controldata_1[8];
//  981 #else
//  982 	#pragma location=0xFFD00
//  983 	uint8_t dtc_vectortable[0x40];
//  984 	#pragma location=0xFFD40
//  985 	uint8_t dtc_controldata_0[8];

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//  986 	uint8_t dtc_controldata_1[8];
_dtc_controldata_1:
        DS 8

        ASEGN `.bssf`:DATA:NOROOT,0ffd00H
_dtc_vectortable:
        DS 64

        ASEGN `.bssf`:DATA:NOROOT,0ffd40H
_dtc_controldata_0:
        DS 8
//  987 #endif
//  988 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon2
          CFI Function _RpDmaSerInitial
          CFI NoCalls
        CODE
//  989 static void RpDmaSerInitial( void )
//  990 {
_RpDmaSerInitial:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  991 	// Enable input clock supply
//  992 	DTCEN = 1U;
        SET1      0xF007A.3          ;; 2 cycles
//  993 	// Disable all DTC channels operation
//  994 	DTCEN0 = 0x00U;
        MOV       0x2E8, #0x0        ;; 1 cycle
//  995 	DTCEN1 = 0x00U;
        MOV       0x2E9, #0x0        ;; 1 cycle
//  996 	DTCEN2 = 0x00U;
        MOV       0x2EA, #0x0        ;; 1 cycle
//  997 	DTCEN3 = 0x00U;
        MOV       0x2EB, #0x0        ;; 1 cycle
//  998 	DTCEN4 = 0x00U;
        MOV       0x2EC, #0x0        ;; 1 cycle
//  999 	// Set base address
// 1000 	DTCBAR = DTC_VECTBL_ADDRESS_BARSET;
        MOV       0x2E0, #0xFD       ;; 1 cycle
// 1001 	// Set CSI20 Interrupt
// 1002 	CSIPR020 = RP_PHY_INTLEVEL_DMADone_b0;
        SET1      0xFFFE9.0          ;; 2 cycles
// 1003 	CSIPR120 = RP_PHY_INTLEVEL_DMADone_b1;
        CLR1      0xFFFED.0          ;; 2 cycles
// 1004 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 18 cycles
        REQUIRE __A_PER1
        REQUIRE __A_DTCEN0
        REQUIRE __A_DTCEN1
        REQUIRE __A_DTCEN2
        REQUIRE __A_DTCEN3
        REQUIRE __A_DTCEN4
        REQUIRE __A_DTCBAR
        REQUIRE __A_PR00
        REQUIRE __A_PR10
// 1005 #endif // #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
// 1006 
// 1007 #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
// 1008 
// 1009 /* Mask Compiler Warning[Pe767]: conversion from pointer to smaller integer */
// 1010 #pragma diag_suppress=Pe767
// 1011 /* Reason: The DTC destination / source address registers must hold a 16-bit address
// 1012    for data transfer. Therefore the conversion of the pointer to an 16-bit integer
// 1013    carring the source and destination address is the desired operation. */
// 1014 
// 1015 /***************************************************************************************************************
// 1016  * function name  : RpDmaSerStart
// 1017  * description    : 
// 1018  * parameters     : 
// 1019  * return value   : none
// 1020  **************************************************************************************************************/
// 1021 static void RpDmaSerStart( uint8_t readmode, uint8_t *data, uint8_t bytes )
// 1022 {
// 1023 	uint8_t dummy = 0xff, *nextptr = data + 1;
// 1024 
// 1025 	RP_DTC_VECTORTABLE16 = 0x40;
// 1026 
// 1027 	if (readmode)
// 1028 	{
// 1029 		// Set DTCD0 for Reading
// 1030 		RP_DTC_CONTROLDATA0_DTCCR = RP_00_DTC_DATA_SIZE_8BITS | RP_00_DTC_TRANSFER_MODE_NORMAL |
// 1031 									RP_00_DTC_SOURCE_ADDR_FIXED | RP_08_DTC_DEST_ADDR_INCREMENTED |
// 1032 									RP_10_DTC_CHAIN_TRANSFER_ENABLE | RP_00_DTC_REPEAT_INT_DISABLE;
// 1033 		RP_DTC_CONTROLDATA0_DTBLS = RP_01_DTCD0_TRANSFER_BYTE;
// 1034 		RP_DTC_CONTROLDATA0_DTCCT = bytes;                      // number of read data
// 1035 		RP_DTC_CONTROLDATA0_DTRLD = bytes;                      // (don't care)
// 1036 		RP_DTC_CONTROLDATA0_DTSAR = RP_FF48_DTCD1_SRC_ADDRESS;  // SIO20
// 1037 		RP_DTC_CONTROLDATA0_DTDAR = (uint16_t)data;             // address for read data
// 1038 		// Set DTCD1 for Writing
// 1039 		RP_DTC_CONTROLDATA1_DTCCR = RP_00_DTC_DATA_SIZE_8BITS | RP_00_DTC_TRANSFER_MODE_NORMAL |
// 1040 									RP_00_DTC_SOURCE_ADDR_FIXED | RP_00_DTC_DEST_ADDR_FIXED |
// 1041 									RP_00_DTC_CHAIN_TRANSFER_DISABLE | RP_00_DTC_REPEAT_INT_DISABLE;
// 1042 		RP_DTC_CONTROLDATA1_DTBLS = RP_01_DTCD0_TRANSFER_BYTE;
// 1043 		RP_DTC_CONTROLDATA1_DTCCT = bytes - 1;                  // number of write data
// 1044 		RP_DTC_CONTROLDATA1_DTRLD = bytes - 1;                  // (don't care)
// 1045 		RP_DTC_CONTROLDATA1_DTSAR = (uint16_t)&dummy;           // dummy data address
// 1046 		RP_DTC_CONTROLDATA1_DTDAR = RP_FF48_DTCD0_DST_ADDRESS;  // SIO20
// 1047 	}
// 1048 	else
// 1049 	{
// 1050 		// Set DTCD0 for Writing
// 1051 		RP_DTC_CONTROLDATA0_DTCCR = RP_00_DTC_DATA_SIZE_8BITS | RP_00_DTC_TRANSFER_MODE_NORMAL |
// 1052 									RP_04_DTC_SOURCE_ADDR_INCREMENTED | RP_00_DTC_DEST_ADDR_FIXED |
// 1053 									RP_00_DTC_CHAIN_TRANSFER_DISABLE | RP_00_DTC_REPEAT_INT_DISABLE;
// 1054 		RP_DTC_CONTROLDATA0_DTBLS = RP_01_DTCD0_TRANSFER_BYTE;
// 1055 		RP_DTC_CONTROLDATA0_DTCCT = bytes - 1;                  // number of write data
// 1056 		RP_DTC_CONTROLDATA0_DTRLD = bytes - 1;                  // (don't care)
// 1057 		RP_DTC_CONTROLDATA0_DTSAR = (uint16_t)nextptr;          // address for write data
// 1058 		RP_DTC_CONTROLDATA0_DTDAR = RP_FF48_DTCD0_DST_ADDRESS;  // SIO20
// 1059 	}
// 1060 
// 1061 	DTCEN2 |= RP_80_DTC_UART2T_ACTIVATION_ENABLE;
// 1062 	CSIIF20 = 0;
// 1063 	CSIMK20 = 1;  // INTCSI20 disable
// 1064 	RP_CSI_IRQ = 0;
// 1065 	RP_CSI_BUF = *data;
// 1066 }
// 1067 #endif // #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
// 1068 
// 1069 #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
// 1070 /***************************************************************************************************************
// 1071  * function name  : RpDmaSerFinishChk
// 1072  * description    : 
// 1073  * parameters     : none
// 1074  * return value   : none
// 1075  **************************************************************************************************************/
// 1076 static void RpDmaSerFinishChk( void )
// 1077 {
// 1078 	while (DTCEN2 & RP_80_DTC_UART2T_ACTIVATION_ENABLE);
// 1079 }
// 1080 #endif // #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
// 1081 
// 1082 #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
// 1083 /***************************************************************************************************************
// 1084  * function name  : RpDmaSerStop
// 1085  * description    : 
// 1086  * parameters     : none
// 1087  * return value   : none
// 1088  **************************************************************************************************************/
// 1089 static void RpDmaSerStop( void )
// 1090 {
// 1091 	DTCEN2 &= (uint8_t)~RP_80_DTC_UART2T_ACTIVATION_ENABLE;
// 1092 	while (RP_CSI_STATUS & RP_SAU_UNDER_EXECUTE);
// 1093 	CSIMK20 = 1;		// INTCSI20 disable
// 1094 	RpCSI_CSDeactivate();
// 1095 }
// 1096 #endif // #if RP_DMA_WRITE_RAM_ENA || RP_DMA_READ_RAM_ENA
// 1097 
// 1098 #if RP_DMA_WRITE_RAM_ENA
// 1099 /***************************************************************************************************************
// 1100  * function name  : RpRegBlockWrite
// 1101  * description    : Write meny data to RFIC with DMA function.
// 1102  * parameters     : 
// 1103  * return value   : none
// 1104  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon2
          CFI Function _RpRegBlockWrite
          CFI NoCalls
        CODE
// 1105 void RpRegBlockWrite( uint16_t adr, uint8_t RP_FAR *dat, uint16_t len )
// 1106 {
_RpRegBlockWrite:
        ; * Stack frame (at entry) *
        ; Param size: 2
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
// 1107 	if (len)
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpGetInterruptLevel_10  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        POP       BC                 ;; 1 cycle
          CFI CFA SP+8
// 1108 	{
// 1109 		RpCSI_CSActivate();
        CLR1      S:0xFFF01.6        ;; 2 cycles
// 1110 
// 1111 #ifdef RP_TXRX_RAM_BIT_SWAP
// 1112 		SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
// 1113 #endif
// 1114 
// 1115 		SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_BUFFER_EMPTY;
        MOVW      AX, #0x8021        ;; 1 cycle
        MOVW      0x150, AX          ;; 1 cycle
// 1116 		RP_REG_WRITE((uint8_t)RP_CSI_MEM_WR_H(adr));
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 10 cycles
??RpRegBlockWrite_0:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpRegBlockWrite_0  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1117 		RP_REG_WRITE((uint8_t)RP_CSI_MEM_WR_L(adr));
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       A, C               ;; 1 cycle
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??RpRegBlockWrite_1:
        BF        [HL].0, ??RpRegBlockWrite_1  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 1118 
// 1119 #ifdef RP_TXRX_RAM_BIT_SWAP
// 1120 		if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
// 1121 		{
// 1122 			SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_LSB | SAU_LENGTH_8;
// 1123 		}
// 1124 #endif
// 1125 
// 1126 		RP_CSI_IRQ = 0;
        CLR1      0xFFFE1.0          ;; 2 cycles
// 1127 
// 1128 		/* check length is more than 4 and dat is in RAM */
// 1129 		if ((len > 4) && ((uint32_t)(dat) >= 0x000F0000))
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x5           ;; 1 cycle
        BC        ??RpGetInterruptLevel_11  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        CLRB      X                  ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+8
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0xF           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 13 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpRegBlockWrite_2:
        BC        ??RpGetInterruptLevel_11  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1130 		{
// 1131 			RpDmaSerStart(0, (uint8_t *)dat, (uint8_t)len);
        MOV       A, E               ;; 1 cycle
        MOV       0xFD10, #0x40      ;; 1 cycle
        MOV       0xFD40, #0x4       ;; 1 cycle
        MOV       0xFD41, #0x1       ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       0xFD42, A          ;; 1 cycle
        MOV       0xFD43, A          ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      0xFD44, AX         ;; 1 cycle
        MOVW      AX, #0xFF48        ;; 1 cycle
        MOVW      0xFD46, AX         ;; 1 cycle
        SET1      0xF02EA.7          ;; 2 cycles
        CLR1      0xFFFE1.0          ;; 2 cycles
        SET1      0xFFFE5.0          ;; 2 cycles
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       0xFFF48, A         ;; 1 cycle
// 1132 			RpDmaTerminationWriteSer();
        ; ------------------------------------- Block: 25 cycles
??RpRegBlockWrite_3:
        MOV       A, 0x2EA           ;; 1 cycle
        BT        A.7, ??RpRegBlockWrite_3  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      HL, #0x2EA         ;; 1 cycle
        CLR1      [HL].7             ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??RpRegBlockWrite_4:
        MOV       A, 0x140           ;; 1 cycle
        BT        A.6, ??RpRegBlockWrite_4  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        SET1      0xFFFE5.0          ;; 2 cycles
        BR        S:??RpGetInterruptLevel_12  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpGetInterruptLevel_11:
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       BC                 ;; 1 cycle
          CFI CFA SP+8
        ; ------------------------------------- Block: 2 cycles
// 1133 		}
// 1134 		else
// 1135 		{
// 1136 			do
// 1137 			{
// 1138 				RP_CSI_BUF = *dat;
??RpRegBlockWrite_5:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
// 1139 				while (RP_CSI_IRQ == 0);
??RpRegBlockWrite_6:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpRegBlockWrite_6  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1140 				RP_CSI_IRQ = 0;
        CLR1      0xFFFE1.0          ;; 2 cycles
// 1141 				dat++;
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1142 			}
// 1143 			while (--len);
        MOVW      AX, BC             ;; 1 cycle
        ADDW      AX, #0xFFFF        ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        BNZ       ??RpRegBlockWrite_5  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 1144 
// 1145 			while (RP_CSI_STATUS & RP_SAU_UNDER_EXECUTE);
??RpRegBlockWrite_7:
        MOV       A, 0x140           ;; 1 cycle
        BT        A.6, ??RpRegBlockWrite_7  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1146 			RpCSI_CSDeactivate();
??RpGetInterruptLevel_12:
        SET1      S:0xFFF01.6        ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 1147 		}
// 1148 	}
// 1149 }
??RpGetInterruptLevel_10:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 152 cycles
        REQUIRE __A_P1
        REQUIRE __A_SMR10
        REQUIRE __A_IF0
        REQUIRE __A_SDR10
        REQUIRE __A_SSR10
        REQUIRE __A_DTCEN2
        REQUIRE __A_MK0
// 1150 #endif // #if RP_DMA_WRITE_RAM_ENA
// 1151 
// 1152 #if RP_DMA_WRITE_RAM_ENA
// 1153 /***************************************************************************************************************
// 1154  * function name  : RpDmaTerminationWriteSer
// 1155  * description    : Wait process of DMA write function.
// 1156  * parameters     : none
// 1157  * return value   : none
// 1158  **************************************************************************************************************/
// 1159 static void RpDmaTerminationWriteSer( void )
// 1160 {
// 1161 	RpDmaSerFinishChk();
// 1162 	RpDmaSerStop();
// 1163 }
// 1164 #endif // #if RP_DMA_WRITE_RAM_ENA
// 1165 
// 1166 #if RP_DMA_READ_RAM_ENA
// 1167 /***************************************************************************************************************
// 1168  * function name  : RpRegBlockRead
// 1169  * description    : Read meny data from RFIC with DMA function.
// 1170  * parameters     : 
// 1171  * return value   : none
// 1172  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon2
          CFI Function _RpRegBlockRead
          CFI NoCalls
        CODE
// 1173 void RpRegBlockRead( uint16_t adr, uint8_t *pDat, uint16_t len )
// 1174 {
_RpRegBlockRead:
        ; * Stack frame (at entry) *
        ; Param size: 2
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
// 1175 	uint8_t rData;
// 1176 
// 1177 	if (len)
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpGetInterruptLevel_13  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
// 1178 	{
// 1179 		RpCSI_CSActivate();
        CLR1      S:0xFFF01.6        ;; 2 cycles
// 1180 
// 1181 #ifdef RP_TXRX_RAM_BIT_SWAP
// 1182 		SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_MSB | SAU_LENGTH_8;
// 1183 #endif
// 1184 
// 1185 		SMR10 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK01 | SAU_CLOCK_MODE_CKS | SAU_TRIGGER_SOFTWARE | SAU_CSI | SAU_TRANSFER_END;
        MOVW      AX, #0x8020        ;; 1 cycle
        MOVW      0x150, AX          ;; 1 cycle
// 1186 		RP_REG_WRITE((uint8_t)RP_CSI_MEM_RD_H(adr));
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 10 cycles
??RpRegBlockRead_0:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpRegBlockRead_0  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1187 		RP_REG_WRITE((uint8_t)RP_CSI_MEM_RD_L(adr));
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       A, E               ;; 1 cycle
        ADD       A, #0x6            ;; 1 cycle
        MOV       0xFFF48, A         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RpRegBlockRead_1:
        BF        [HL].0, ??RpRegBlockRead_1  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 1188 
// 1189 #ifdef RP_TXRX_RAM_BIT_SWAP
// 1190 		if ((BBTXRAM0 <= adr) && (adr < BBTXRXRAMEND))
// 1191 		{
// 1192 			SCR10 = SAU_RECEPTION_TRANSMISSION | SAU_TIMING_1 | SAU_LSB | SAU_LENGTH_8;
// 1193 		}
// 1194 #endif
// 1195 
// 1196 		if (len > 4)
        MOVW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x5           ;; 1 cycle
        BC        ??RpGetInterruptLevel_14  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1197 		{
// 1198 			RpDmaSerStart(1, pDat, (uint8_t)len);
        MOV       A, #0xFF           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       0xFD10, #0x40      ;; 1 cycle
        MOV       0xFD40, #0x18      ;; 1 cycle
        MOV       0xFD41, #0x1       ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       0xFD42, A          ;; 1 cycle
        MOV       0xFD43, A          ;; 1 cycle
        MOVW      AX, #0xFF48        ;; 1 cycle
        MOVW      0xFD44, AX         ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      0xFD46, AX         ;; 1 cycle
        MOV       0xFD48, #0x0       ;; 1 cycle
        MOV       0xFD49, #0x1       ;; 1 cycle
        DEC       C                  ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       0xFD4A, A          ;; 1 cycle
        MOV       0xFD4B, A          ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      0xFD4C, AX         ;; 1 cycle
        MOVW      AX, #0xFF48        ;; 1 cycle
        MOVW      0xFD4E, AX         ;; 1 cycle
        SET1      0xF02EA.7          ;; 2 cycles
        CLR1      0xFFFE1.0          ;; 2 cycles
        SET1      0xFFFE5.0          ;; 2 cycles
        CLR1      0xFFFE1.0          ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       0xFFF48, A         ;; 1 cycle
// 1199 			RpDmaTerminationReadSer();
        ; ------------------------------------- Block: 37 cycles
??RpRegBlockRead_2:
        MOV       A, 0x2EA           ;; 1 cycle
        BT        A.7, ??RpRegBlockRead_2  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
??RpRegBlockRead_3:
        MOV       A, 0x140           ;; 1 cycle
        BT        A.6, ??RpRegBlockRead_3  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        SET1      0xFFFE5.0          ;; 2 cycles
        BR        R:??RpGetInterruptLevel_15  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1200 		}
// 1201 		else // if(len >= 1 && len <= 4)
// 1202 		{
// 1203 			RP_CSI_IRQ = 0;
??RpGetInterruptLevel_14:
        CLR1      0xFFFE1.0          ;; 2 cycles
// 1204 			RP_CSI_BUF = RP_CSI_NOP;
        MOV       0xFFF48, #0xFF     ;; 1 cycle
// 1205 			switch (len)
        SUBW      AX, #0x1           ;; 1 cycle
        BZ        ??RpGetInterruptLevel_16  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        SUBW      AX, #0x1           ;; 1 cycle
        BZ        ??RpGetInterruptLevel_17  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0x1           ;; 1 cycle
        BZ        ??RpGetInterruptLevel_18  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0x1           ;; 1 cycle
        BNZ       ??RpGetInterruptLevel_15  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1206 			{
// 1207 				case 4:
// 1208 					while (RP_CSI_IRQ == 0);
??RpRegBlockRead_4:
        BF        [HL].0, ??RpRegBlockRead_4  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 1209 					rData = RP_CSI_BUF;
        MOV       A, 0xFFF48         ;; 1 cycle
        MOV       B, A               ;; 1 cycle
// 1210 					RP_CSI_IRQ = 0;
        CLR1      0xFFFE1.0          ;; 2 cycles
// 1211 					RP_CSI_BUF = RP_CSI_NOP;
        MOV       0xFFF48, #0xFF     ;; 1 cycle
// 1212 					*pDat = rData;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 1213 					pDat++;
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 17 cycles
// 1214 				case 3:
// 1215 					while (RP_CSI_IRQ == 0);
??RpGetInterruptLevel_18:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpGetInterruptLevel_18  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1216 					rData = RP_CSI_BUF;
        MOV       A, 0xFFF48         ;; 1 cycle
        MOV       B, A               ;; 1 cycle
// 1217 					RP_CSI_IRQ = 0;
        CLR1      0xFFFE1.0          ;; 2 cycles
// 1218 					RP_CSI_BUF = RP_CSI_NOP;
        MOV       0xFFF48, #0xFF     ;; 1 cycle
// 1219 					*pDat = rData;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 1220 					pDat++;
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 17 cycles
// 1221 				case 2:
// 1222 					while (RP_CSI_IRQ == 0);
??RpGetInterruptLevel_17:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpGetInterruptLevel_17  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1223 					rData = RP_CSI_BUF;
        MOV       A, 0xFFF48         ;; 1 cycle
        MOV       B, A               ;; 1 cycle
// 1224 					RP_CSI_IRQ = 0;
        CLR1      0xFFFE1.0          ;; 2 cycles
// 1225 					RP_CSI_BUF = RP_CSI_NOP;
        MOV       0xFFF48, #0xFF     ;; 1 cycle
// 1226 					*pDat = rData;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 1227 					pDat++;
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 17 cycles
// 1228 				case 1:
// 1229 					while (RP_CSI_IRQ == 0);
??RpGetInterruptLevel_16:
        MOVW      HL, #0xFFE1        ;; 1 cycle
        BF        [HL].0, ??RpGetInterruptLevel_16  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1230 					*pDat = RP_CSI_BUF;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, 0xFFF48         ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 7 cycles
// 1231 				default:
// 1232 					break;
// 1233 			}
// 1234 			RpCSI_CSDeactivate();
??RpGetInterruptLevel_15:
        SET1      S:0xFFF01.6        ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 1235 		}
// 1236 	}
// 1237 }
??RpGetInterruptLevel_13:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 210 cycles
        REQUIRE __A_P1
        REQUIRE __A_SMR10
        REQUIRE __A_IF0
        REQUIRE __A_SDR10
        REQUIRE __A_SSR10
        REQUIRE __A_MK0
        REQUIRE __A_DTCEN2
// 1238 #endif // #if RP_DMA_READ_RAM_ENA
// 1239 
// 1240 #if RP_DMA_READ_RAM_ENA
// 1241 /***************************************************************************************************************
// 1242  * function name  : RpDmaTerminationReadSer
// 1243  * description    : Wait process of DMA read function.
// 1244  * parameters     : none
// 1245  * return value   : none
// 1246  **************************************************************************************************************/
// 1247 static void RpDmaTerminationReadSer( void )
// 1248 {
// 1249 	RpDmaSerFinishChk();
// 1250 	while (RP_CSI_STATUS & RP_SAU_UNDER_EXECUTE);
// 1251 	CSIMK20 = 1;		// INTCSI20 disable
// 1252 	RpCSI_CSDeactivate();
// 1253 }
// 1254 #endif // #if RP_DMA_READ_RAM_ENA
// 1255 
// 1256 /***************************************************************************************************************
// 1257  * function name  : RpGetIntLevel
// 1258  * description    : Get RFdriver Interrupt Level
// 1259  * parameters     : none
// 1260  * return value   : Inerruput Level
// 1261  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock18 Using cfiCommon2
          CFI Function _RpGetInterruptLevel
          CFI NoCalls
        CODE
// 1262 uint8_t RpGetInterruptLevel( void )
// 1263 {
_RpGetInterruptLevel:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 1264 	return( RP_PHY_INTLEVEL );
        ONEB      A                  ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock18
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 7 cycles
// 1265 }

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
// 1266 
// 1267 /***********************************************************************************************************************
// 1268  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
// 1269  **********************************************************************************************************************/
// 1270  
// 
//    82 bytes in section .bss.noinit   (abs)
//     9 bytes in section .bssf
//    72 bytes in section .bssf         (abs)
//    18 bytes in section .sbss.noinit  (abs)
//    50 bytes in section .text
// 1 680 bytes in section .textf
// 
//    50 bytes of CODE    memory
//    81 bytes of DATA    memory (+ 100 bytes shared)
// 1 680 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
