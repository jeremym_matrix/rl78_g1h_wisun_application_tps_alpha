///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:31:02
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_api.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW32A5.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_api.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\phy_api.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _RpConfig
        EXTERN _RpAckCheckCallback
        EXTERN ?L_MUL_FAST_L03
        EXTERN ?MEMCPY_FAR
        EXTERN ?SI_CMP_L02
        EXTERN ?SI_MOD_L02
        EXTERN ?UL_CMP_L03
        EXTERN _RpAntSelAssist_RecoveryProc
        EXTERN _RpAntSelAssist_StartProc
        EXTERN _RpAntSelAssist_StopProc
        EXTERN _RpChangeDefaultFilter
        EXTERN _RpChangeFilter
        EXTERN _RpChangeNarrowBandFilter
        EXTERN _RpCheckRfIRQ
        EXTERN _RpExecuteCalibration
        EXTERN _RpLimitTimerControl
        EXTERN _RpMcuPeripheralInit
        EXTERN _RpPowerdownSequence
        EXTERN _RpRegBlockRead
        EXTERN _RpRegBlockWrite
        EXTERN _RpRegRead
        EXTERN _RpRegWrite
        EXTERN _RpSetAttr_phyAckWithCca
        EXTERN _RpSetAttr_phyFrequencyOffset
        EXTERN _RpSetMcuInt
        EXTERN _RpStartTransmitWithCca
        EXTERN _RpWakeupSequence
        EXTERN ___get_psw
        EXTERN ___set_psw
        EXTERN _memset

        PUBLIC _RpAddressFilterSetting
        PUBLIC _RpAvailableRcvRamEnable
        PUBLIC _RpCalcTotalBytes
        PUBLIC _RpCalcTxInterval
        PUBLIC _RpCb
        PUBLIC _RpCcaVthOffsetTblDefault
        PUBLIC _RpCcaVthOffsetTblNarrow
        PUBLIC _RpCheckLongerThanTotalTxTime
        PUBLIC _RpClrLowPowerProhibit
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId14
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId15
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId16
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId17
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId4
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId5
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId6
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId7
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId8
        PUBLIC _RpCurrentPageToChannelsSupprotedAtBandId9
        PUBLIC _RpExtChkErrFrameLen
        PUBLIC _RpExtChkIdLenGetReq
        PUBLIC _RpExtChkIdLenSetReq
        PUBLIC _RpFreqBandIdToDataRate
        PUBLIC _RpFreqBandTbl
        PUBLIC _RpGetAgcStartVth
        PUBLIC _RpGetAntennaDiversityStartVth
        PUBLIC _RpGetAntennaSwitchingTime
        PUBLIC _RpGetLowPowerProhibit
        PUBLIC _RpGetPowerMode
        PUBLIC _RpGetRxBuf
        PUBLIC _RpGetTime
        PUBLIC _RpInit
        PUBLIC _RpInverseTxAnt
        PUBLIC _RpLog_Event
        PUBLIC _RpMemcpy
        PUBLIC _RpMemset
        PUBLIC _RpPdDataReq
        PUBLIC _RpPlmeCcaReq
        PUBLIC _RpPlmeEdReq
        PUBLIC _RpPrevSentTimeReSetting
        PUBLIC _RpRampUpDownStableTotalByte
        PUBLIC _RpRdEvaReg2
        PUBLIC _RpReadIrq
        PUBLIC _RpRegAdcVgaDefault
        PUBLIC _RpRegCcaBandwidth225k
        PUBLIC _RpRegRxDataRateDefault
        PUBLIC _RpRegTxRxDataRateDefault
        PUBLIC _RpRelRxBuf
        PUBLIC _RpResetReq
        PUBLIC _RpRfStat
        PUBLIC _RpRxBuf
        PUBLIC _RpRxOffBeforeReplyingAck
        PUBLIC _RpRxOnStart
        PUBLIC _RpRxOnStop
        PUBLIC _RpSetAckReplyTimeVal
        PUBLIC _RpSetAckWaitDurationVal
        PUBLIC _RpSetAdrfAndAutoAckVal
        PUBLIC _RpSetAgcStartVth
        PUBLIC _RpSetAgcWaitGain
        PUBLIC _RpSetAntennaDiversityStartVth
        PUBLIC _RpSetAntennaDiversityVal
        PUBLIC _RpSetAntennaSelectTxVal
        PUBLIC _RpSetAntennaSwitchEnaTiming
        PUBLIC _RpSetAntennaSwitchVal
        PUBLIC _RpSetBackOffSeedVal
        PUBLIC _RpSetCcaDurationVal
        PUBLIC _RpSetCcaEdBandwidth
        PUBLIC _RpSetCcaVthVal
        PUBLIC _RpSetChannelVal
        PUBLIC _RpSetChannelsSupportedPageAndVal
        PUBLIC _RpSetCsmaBackoffPeriod
        PUBLIC _RpSetFcsLengthVal
        PUBLIC _RpSetFecVal
        PUBLIC _RpSetFskOpeModeVal
        PUBLIC _RpSetFskScramblePsduVal
        PUBLIC _RpSetGpio0Setting
        PUBLIC _RpSetGpio3Setting
        PUBLIC _RpSetLowPowerProhibit
        PUBLIC _RpSetLvlVthVal
        PUBLIC _RpSetMacTxLimitMode
        PUBLIC _RpSetMaxBeVal
        PUBLIC _RpSetMaxCsmaBackoffVal
        PUBLIC _RpSetMrFskSfdVal
        PUBLIC _RpSetPowerMode
        PUBLIC _RpSetPreamble4ByteRxMode
        PUBLIC _RpSetPreambleLengthVal
        PUBLIC _RpSetRxOffReq
        PUBLIC _RpSetRxOnReq
        PUBLIC _RpSetSfdDetectionExtend
        PUBLIC _RpSetSfdDetectionExtendWrite
        PUBLIC _RpSetSpecificModeVal
        PUBLIC _RpSetStateRxOnToTrxOff
        PUBLIC _RpSetTxPowerVal
        PUBLIC _RpSetTxTriggerTimer
        PUBLIC _RpStackVersion
        PUBLIC _RpTC0SetReg
        PUBLIC _RpTc0WasteTime
        PUBLIC _RpTrnxHdrFunc
        PUBLIC _RpTxLimitTimerForward
        PUBLIC _RpTxTime
        PUBLIC _RpUpdateTxTime
        PUBLIC _RpWait4us
        PUBLIC _RpWrEvaReg2
        PUBLIC _RptConTrxStop
        PUBLIC _RptConTxNoModu
        PUBLIC _RptPulseTxPrbs9
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
        
          CFI Common cfiCommon3 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon3
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_api.c
//    1 /***********************************************************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
//    4  * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
//    5  * applicable laws, including copyright laws. 
//    6  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
//    7  * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
//    8  * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//    9  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
//   10  * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
//   11  * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
//   12  * DAMAGES.
//   13  * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
//   14  * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
//   15  * following link:
//   16  * http://www.renesas.com/disclaimer 
//   17  **********************************************************************************************************************/
//   18 /***********************************************************************************************************************
//   19  * file name	: phy_api.c
//   20  * description	: This is the RF driver's api code.
//   21  ***********************************************************************************************************************
//   22  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
//   23  **********************************************************************************************************************/
//   24 /***************************************************************************************************************
//   25  * includes
//   26  **************************************************************************************************************/
//   27 #include <string.h>
//   28 //
//   29 #include "phy.h"
//   30 #include "phy_def.h"
//   31 #include "phy_table.h"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpTxPowerModeSetJpn[102][4]
_RpTxPowerModeSetJpn:
        DATA8
        DB 3, 0, 0, 0, 3, 0, 0, 31, 3, 1, 0, 31, 3, 2, 0, 31, 3, 3, 0, 31, 3, 4
        DB 0, 31, 3, 5, 0, 31, 3, 6, 0, 31, 3, 7, 0, 31, 3, 8, 0, 31, 3, 9, 0
        DB 31, 3, 10, 0, 31, 3, 11, 0, 31, 3, 12, 0, 31, 3, 13, 0, 31, 3, 14, 0
        DB 31, 3, 15, 0, 31, 3, 16, 0, 31, 3, 17, 0, 31, 3, 18, 0, 31, 3, 19, 0
        DB 31, 3, 20, 0, 31, 2, 21, 0, 31, 2, 22, 0, 31, 2, 23, 0, 31, 2, 24, 0
        DB 31, 2, 25, 0, 31, 2, 26, 0, 31, 2, 27, 0, 31, 2, 28, 0, 31, 2, 29, 0
        DB 31, 2, 30, 0, 31, 2, 31, 0, 31, 3, 5, 1, 21, 3, 6, 1, 21, 3, 7, 1
        DB 21, 3, 8, 1, 21, 3, 9, 1, 21, 3, 10, 1, 21, 3, 11, 1, 21, 3, 12, 1
        DB 21, 3, 13, 1, 21, 3, 14, 1, 21, 3, 15, 1, 21, 3, 16, 1, 21, 3, 17, 1
        DB 21, 3, 18, 1, 21, 3, 19, 1, 21, 2, 20, 1, 21, 2, 21, 1, 21, 2, 22, 1
        DB 21, 2, 23, 1, 21, 2, 24, 1, 21, 2, 25, 1, 21, 2, 26, 1, 21, 2, 27, 1
        DB 21, 2, 28, 1, 21, 2, 29, 1, 21, 2, 30, 1, 21, 2, 31, 1, 21, 3, 7, 2
        DB 31, 3, 8, 2, 31, 3, 9, 2, 31, 3, 10, 2, 31, 3, 11, 2, 31, 3, 12, 2
        DB 31, 3, 13, 2, 31, 3, 14, 2, 31, 3, 15, 2, 31, 3, 16, 2, 31, 3, 17, 2
        DB 31, 3, 18, 2, 31, 3, 19, 2, 31, 3, 20, 2, 31, 2, 21, 2, 31, 2, 22, 2
        DB 31, 2, 23, 2, 31, 2, 24, 2, 31, 2, 25, 2, 31, 2, 26, 2, 31, 2, 27, 2
        DB 31, 2, 28, 2, 31, 2, 29, 2, 31, 2, 30, 2, 31, 2, 31, 2, 31, 3, 17, 4
        DB 22, 3, 18, 4, 22, 3, 19, 4, 22, 3, 20, 4, 22, 3, 17, 6, 22, 3, 18, 6
        DB 22, 3, 19, 6, 22, 3, 20, 6, 22, 2, 21, 7, 21, 2, 22, 7, 21, 2, 23, 7
        DB 21, 1, 27, 7, 21, 1, 28, 7, 21, 1, 29, 7, 21, 1, 30, 7, 21, 1, 30, 8
        DB 17, 1, 31, 8, 17
        DB 3, 0, 0, 0, 3, 0, 0, 31, 3, 1, 0, 31, 3, 2, 0, 31, 3, 3, 0, 31, 3, 4
        DB 0, 31, 3, 5, 0, 31, 3, 6, 0, 31, 3, 7, 0, 31, 3, 8, 0, 31, 3, 9, 0
        DB 31, 3, 10, 0, 31, 3, 11, 0, 31, 3, 12, 0, 31, 3, 13, 0, 31, 3, 14, 0
        DB 31, 3, 15, 0, 31, 3, 16, 0, 31, 3, 17, 0, 31, 3, 18, 0, 31, 3, 19, 0
        DB 31, 3, 20, 0, 31, 2, 21, 0, 31, 2, 22, 0, 31, 2, 23, 0, 31, 2, 24, 0
        DB 31, 2, 25, 0, 31, 2, 26, 0, 31, 2, 27, 0, 31, 2, 28, 0, 31, 2, 29, 0
        DB 31, 2, 30, 0, 31, 2, 31, 0, 31, 3, 6, 1, 15, 3, 7, 1, 15, 3, 8, 1
        DB 15, 3, 9, 1, 15, 3, 10, 1, 15, 3, 11, 1, 15, 3, 12, 1, 15, 3, 13, 1
        DB 15, 3, 14, 1, 15, 3, 15, 1, 15, 3, 16, 1, 15, 3, 17, 1, 15, 3, 18, 1
        DB 15, 3, 19, 1, 15, 3, 20, 1, 15, 2, 21, 1, 15, 2, 22, 1, 15, 2, 23, 1
        DB 15, 2, 24, 1, 15, 2, 25, 1, 15, 2, 26, 1, 15, 2, 27, 1, 15, 2, 28, 1
        DB 15, 2, 29, 1, 15, 2, 30, 1, 15, 2, 31, 1, 15, 3, 5, 2, 27, 3, 6, 2
        DB 27, 3, 7, 2, 27, 3, 8, 2, 27, 3, 9, 2, 27, 3, 10, 2, 27, 3, 11, 2
        DB 27, 3, 12, 2, 27, 3, 13, 2, 27, 3, 14, 2, 27, 3, 15, 2, 27, 3, 16, 2
        DB 27, 3, 17, 2, 27, 3, 18, 2, 27, 3, 19, 2, 27, 3, 20, 2, 27, 3, 12, 3
        DB 31, 3, 13, 3, 31, 3, 14, 3, 31, 3, 15, 3, 31, 3, 16, 3, 31, 3, 17, 3
        DB 31, 3, 18, 3, 31, 3, 19, 3, 31, 3, 20, 3, 31, 3, 17, 4, 27, 3, 18, 4
        DB 27, 3, 19, 4, 27, 3, 20, 4, 27, 3, 17, 6, 27, 3, 18, 6, 27, 3, 19, 6
        DB 27, 3, 20, 6, 27, 1, 20, 7, 22, 1, 21, 7, 22, 1, 22, 7, 22, 1, 23, 7
        DB 22, 1, 24, 7, 22, 1, 25, 7, 22, 1, 26, 7, 22, 1, 27, 7, 22, 1, 28, 7
        DB 22, 1, 30, 8, 21, 1, 31, 8, 21
        DB 3, 0, 0, 0, 3, 0, 0, 31, 3, 1, 0, 31, 3, 2, 0, 31, 3, 3, 0, 31, 3, 4
        DB 0, 31, 3, 5, 0, 31, 3, 6, 0, 31, 3, 7, 0, 31, 3, 8, 0, 31, 3, 9, 0
        DB 31, 3, 10, 0, 31, 3, 11, 0, 31, 3, 12, 0, 31, 3, 13, 0, 31, 3, 14, 0
        DB 31, 3, 15, 0, 31, 3, 16, 0, 31, 3, 17, 0, 31, 3, 18, 0, 31, 3, 19, 0
        DB 31, 3, 20, 0, 31, 2, 21, 0, 31, 2, 22, 0, 31, 2, 23, 0, 31, 2, 24, 0
        DB 31, 2, 25, 0, 31, 2, 26, 0, 31, 2, 27, 0, 31, 2, 28, 0, 31, 2, 29, 0
        DB 31, 2, 30, 0, 31, 2, 31, 0, 31, 3, 5, 1, 21, 3, 6, 1, 21, 3, 7, 1
        DB 21, 3, 8, 1, 21, 3, 9, 1, 21, 3, 10, 1, 21, 3, 11, 1, 21, 3, 12, 1
        DB 21, 3, 13, 1, 21, 3, 14, 1, 21, 3, 15, 1, 21, 3, 16, 1, 21, 3, 17, 1
        DB 21, 3, 18, 1, 21, 3, 19, 1, 21, 3, 20, 1, 21, 2, 21, 1, 21, 2, 22, 1
        DB 21, 2, 23, 1, 21, 2, 24, 1, 21, 2, 25, 1, 21, 2, 26, 1, 21, 2, 27, 1
        DB 21, 2, 28, 1, 21, 2, 29, 1, 21, 2, 30, 1, 21, 2, 31, 1, 21, 3, 8, 2
        DB 31, 3, 9, 2, 31, 3, 10, 2, 31, 3, 11, 2, 31, 3, 12, 2, 31, 3, 13, 2
        DB 31, 3, 14, 2, 31, 3, 15, 2, 31, 3, 16, 2, 31, 3, 17, 2, 31, 3, 18, 2
        DB 31, 3, 19, 2, 31, 3, 20, 2, 31, 2, 21, 2, 31, 2, 22, 2, 31, 2, 23, 2
        DB 31, 2, 24, 2, 31, 2, 25, 2, 31, 2, 26, 2, 31, 2, 27, 2, 31, 2, 28, 2
        DB 31, 2, 29, 2, 31, 2, 30, 2, 31, 2, 31, 2, 31, 3, 17, 4, 22, 3, 18, 4
        DB 22, 3, 19, 4, 22, 3, 20, 4, 22, 3, 18, 6, 22, 3, 19, 6, 22, 3, 20, 6
        DB 22, 1, 21, 7, 20, 1, 22, 7, 20, 1, 23, 7, 20, 1, 24, 7, 20, 1, 25, 7
        DB 20, 1, 26, 7, 20, 1, 27, 7, 20, 1, 28, 7, 20, 1, 29, 7, 20, 1, 30, 7
        DB 20, 1, 30, 8, 17, 1, 31, 8, 17
        DB 3, 0, 0, 0, 3, 0, 0, 31, 3, 1, 0, 31, 3, 2, 0, 31, 3, 3, 0, 31, 3, 4
        DB 0, 31, 3, 5, 0, 31, 3, 6, 0, 31, 3, 7, 0, 31, 3, 8, 0, 31, 3, 9, 0
        DB 31, 3, 10, 0, 31, 3, 11, 0, 31, 3, 12, 0, 31, 3, 13, 0, 31, 3, 14, 0
        DB 31, 3, 15, 0, 31, 3, 16, 0, 31, 3, 17, 0, 31, 3, 18, 0, 31, 3, 19, 0
        DB 31, 3, 20, 0, 31, 2, 21, 0, 31, 2, 22, 0, 31, 2, 23, 0, 31, 2, 24, 0
        DB 31, 2, 25, 0, 31, 2, 26, 0, 31, 2, 27, 0, 31, 2, 28, 0, 31, 2, 29, 0
        DB 31, 2, 30, 0, 31, 2, 31, 0, 31, 3, 5, 1, 21, 3, 6, 1, 21, 3, 7, 1
        DB 21, 3, 8, 1, 21, 3, 9, 1, 21, 3, 10, 1, 21, 3, 11, 1, 21, 3, 12, 1
        DB 21, 3, 13, 1, 21, 3, 14, 1, 21, 3, 15, 1, 21, 3, 16, 1, 21, 3, 17, 1
        DB 21, 3, 18, 1, 21, 3, 19, 1, 21, 3, 20, 1, 21, 2, 21, 1, 21, 2, 22, 1
        DB 21, 2, 23, 1, 21, 2, 24, 1, 21, 2, 25, 1, 21, 2, 26, 1, 21, 2, 27, 1
        DB 21, 2, 28, 1, 21, 2, 29, 1, 21, 2, 30, 1, 21, 2, 31, 1, 21, 3, 6, 2
        DB 31, 3, 7, 2, 31, 3, 8, 2, 31, 3, 9, 2, 31, 3, 10, 2, 31, 3, 11, 2
        DB 31, 3, 12, 2, 31, 3, 13, 2, 31, 3, 14, 2, 31, 3, 15, 2, 31, 3, 16, 2
        DB 31, 3, 17, 2, 31, 3, 18, 2, 31, 3, 19, 2, 31, 3, 20, 2, 31, 2, 21, 2
        DB 31, 2, 22, 2, 31, 2, 23, 2, 31, 2, 24, 2, 31, 2, 25, 2, 31, 2, 26, 2
        DB 31, 2, 27, 2, 31, 2, 28, 2, 31, 2, 29, 2, 31, 2, 30, 2, 31, 2, 31, 2
        DB 31, 3, 17, 4, 22, 3, 18, 4, 22, 3, 19, 4, 22, 3, 20, 4, 22, 3, 17, 6
        DB 22, 3, 18, 6, 22, 3, 19, 6, 22, 3, 20, 6, 22, 1, 20, 7, 20, 1, 21, 7
        DB 20, 1, 22, 7, 20, 1, 23, 7, 20, 1, 24, 7, 20, 1, 25, 7, 20, 1, 26, 7
        DB 20, 1, 27, 7, 20, 1, 28, 7, 20, 1, 29, 8, 17, 1, 30, 8, 17

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static RP_DDC_TABLE const RpFreq863MHzOpe1DdcTbl[14]
_RpFreq863MHzOpe1DdcTbl:
        DATA32
        DD 863187500
        DATA8
        DB 8, 0
        DATA32
        DD 864800000
        DATA8
        DB 8, 0
        DATA32
        DD 865875000
        DATA8
        DB 8, 0
        DATA32
        DD 866125000
        DATA8
        DB 72, 0
        DATA32
        DD 866450000
        DATA8
        DB 40, 0
        DATA32
        DD 867487500
        DATA8
        DB 8, 0
        DATA32
        DD 867750000
        DATA8
        DB 104, 0
        DATA32
        DD 868012500
        DATA8
        DB 72, 0
        DATA32
        DD 868312500
        DATA8
        DB 40, 0
        DATA32
        DD 868750000
        DATA8
        DB 8, 0
        DATA32
        DD 869162500
        DATA8
        DB 72, 0
        DATA32
        DD 869300000
        DATA8
        DB 104, 0
        DATA32
        DD 869825000
        DATA8
        DB 72, 0
        DATA32
        DD 870000000
        DATA8
        DB 40, 0
        DATA32
        DD 863187500
        DATA8
        DB 8, 0
        DATA32
        DD 864800000
        DATA8
        DB 8, 0
        DATA32
        DD 865875000
        DATA8
        DB 8, 0
        DATA32
        DD 866137500
        DATA8
        DB 72, 0
        DATA32
        DD 866450000
        DATA8
        DB 40, 0
        DATA32
        DD 867537500
        DATA8
        DB 8, 0
        DATA32
        DD 868037500
        DATA8
        DB 104, 0
        DATA32
        DD 868362500
        DATA8
        DB 40, 0
        DATA32
        DD 868662500
        DATA8
        DB 8, 0
        DATA32
        DD 868750000
        DATA8
        DB 104, 0
        DATA32
        DD 869825000
        DATA8
        DB 72, 0
        DATA32
        DD 870000000
        DATA8
        DB 40, 0
        DATA32
        DD 863187500
        DATA8
        DB 8, 0
        DATA32
        DD 864800000
        DATA8
        DB 8, 0
        DATA32
        DD 865875000
        DATA8
        DB 8, 0
        DATA32
        DD 866137500
        DATA8
        DB 72, 0
        DATA32
        DD 866450000
        DATA8
        DB 40, 0
        DATA32
        DD 867537500
        DATA8
        DB 8, 0
        DATA32
        DD 868025000
        DATA8
        DB 104, 0
        DATA32
        DD 868100000
        DATA8
        DB 72, 0
        DATA32
        DD 868362500
        DATA8
        DB 40, 0
        DATA32
        DD 868662500
        DATA8
        DB 8, 0
        DATA32
        DD 869037500
        DATA8
        DB 104, 0
        DATA32
        DD 869825000
        DATA8
        DB 72, 0
        DATA32
        DD 870000000
        DATA8
        DB 40, 0
        DATA32
        DD 896300000
        DATA8
        DB 104, 0
        DATA32
        DD 896662500
        DATA8
        DB 8, 0
        DATA32
        DD 896887500
        DATA8
        DB 40, 0
        DATA32
        DD 897237500
        DATA8
        DB 72, 0
        DATA32
        DD 898450000
        DATA8
        DB 104, 0
        DATA32
        DD 899537500
        DATA8
        DB 8, 0
        DATA32
        DD 900450000
        DATA8
        DB 40, 0
        DATA32
        DD 900662500
        DATA8
        DB 8, 0
        DATA32
        DD 901000000
        DATA8
        DB 72, 0
        DATA32
        DD 896300000
        DATA8
        DB 104, 0
        DATA32
        DD 896662500
        DATA8
        DB 8, 0
        DATA32
        DD 896887500
        DATA8
        DB 40, 0
        DATA32
        DD 897237500
        DATA8
        DB 72, 0
        DATA32
        DD 898450000
        DATA8
        DB 104, 0
        DATA32
        DD 899537500
        DATA8
        DB 8, 0
        DATA32
        DD 900450000
        DATA8
        DB 40, 0
        DATA32
        DD 900662500
        DATA8
        DB 8, 0
        DATA32
        DD 901000000
        DATA8
        DB 72, 0
        DATA32
        DD 896375000
        DATA8
        DB 104, 0
        DATA32
        DD 896762500
        DATA8
        DB 8, 0
        DATA32
        DD 896887500
        DATA8
        DB 40, 0
        DATA32
        DD 897337500
        DATA8
        DB 72, 0
        DATA32
        DD 898450000
        DATA8
        DB 104, 0
        DATA32
        DD 899537500
        DATA8
        DB 8, 0
        DATA32
        DD 900462500
        DATA8
        DB 40, 0
        DATA32
        DD 900762500
        DATA8
        DB 8, 0
        DATA32
        DD 901000000
        DATA8
        DB 72, 0
        DATA32
        DD 901262500
        DATA8
        DB 72, 0
        DATA32
        DD 901537500
        DATA8
        DB 40, 0
        DATA32
        DD 901712500
        DATA8
        DB 8, 0
        DATA32
        DD 902000000
        DATA8
        DB 104, 0
        DATA32
        DD 901262500
        DATA8
        DB 72, 0
        DATA32
        DD 901537500
        DATA8
        DB 40, 0
        DATA32
        DD 901712500
        DATA8
        DB 8, 0
        DATA32
        DD 902000000
        DATA8
        DB 104, 0
        DATA32
        DD 901262500
        DATA8
        DB 72, 0
        DATA32
        DD 901412500
        DATA8
        DB 104, 0
        DATA32
        DD 901537500
        DATA8
        DB 8, 0
        DATA32
        DD 901687500
        DATA8
        DB 40, 0
        DATA32
        DD 902000000
        DATA8
        DB 104, 0
        DATA32
        DD 902162500
        DATA8
        DB 104, 0
        DATA32
        DD 902362500
        DATA8
        DB 72, 0
        DATA32
        DD 903662500
        DATA8
        DB 8, 0
        DATA32
        DD 903912500
        DATA8
        DB 104, 0
        DATA32
        DD 904037500
        DATA8
        DB 72, 0
        DATA32
        DD 904287500
        DATA8
        DB 40, 0
        DATA32
        DD 904687500
        DATA8
        DB 8, 0
        DATA32
        DD 905112500
        DATA8
        DB 104, 0
        DATA32
        DD 905387500
        DATA8
        DB 40, 0
        DATA32
        DD 905587500
        DATA8
        DB 8, 0
        DATA32
        DD 905787500
        DATA8
        DB 72, 0
        DATA32
        DD 906037500
        DATA8
        DB 8, 0
        DATA32
        DD 906162500
        DATA8
        DB 40, 0
        DATA32
        DD 906450000
        DATA8
        DB 104, 0
        DATA32
        DD 907537500
        DATA8
        DB 8, 0
        DATA32
        DD 907850000
        DATA8
        DB 40, 0
        DATA32
        DD 908112500
        DATA8
        DB 72, 0
        DATA32
        DD 908400000
        DATA8
        DB 104, 0
        DATA32
        DD 908650000
        DATA8
        DB 8, 0
        DATA32
        DD 909075000
        DATA8
        DB 40, 0
        DATA32
        DD 909212500
        DATA8
        DB 72, 0
        DATA32
        DD 909412500
        DATA8
        DB 104, 0
        DATA32
        DD 911187500
        DATA8
        DB 8, 0
        DATA32
        DD 913500000
        DATA8
        DB 8, 0
        DATA32
        DD 913875000
        DATA8
        DB 8, 0
        DATA32
        DD 914137500
        DATA8
        DB 72, 0
        DATA32
        DD 914450000
        DATA8
        DB 40, 0
        DATA32
        DD 915537500
        DATA8
        DB 8, 0
        DATA32
        DD 915875000
        DATA8
        DB 104, 0
        DATA32
        DD 916137500
        DATA8
        DB 72, 0
        DATA32
        DD 916450000
        DATA8
        DB 40, 0
        DATA32
        DD 916762500
        DATA8
        DB 8, 0
        DATA32
        DD 916850000
        DATA8
        DB 104, 0
        DATA32
        DD 917112500
        DATA8
        DB 72, 0
        DATA32
        DD 917300000
        DATA8
        DB 40, 0
        DATA32
        DD 917412500
        DATA8
        DB 72, 0
        DATA32
        DD 917537500
        DATA8
        DB 8, 0
        DATA32
        DD 917825000
        DATA8
        DB 72, 0
        DATA32
        DD 918450000
        DATA8
        DB 40, 0
        DATA32
        DD 919662500
        DATA8
        DB 8, 0
        DATA32
        DD 919975000
        DATA8
        DB 40, 0
        DATA32
        DD 920400000
        DATA8
        DB 104, 0
        DATA32
        DD 920762500
        DATA8
        DB 8, 0
        DATA32
        DD 920925000
        DATA8
        DB 40, 0
        DATA32
        DD 921212500
        DATA8
        DB 72, 0
        DATA32
        DD 921412500
        DATA8
        DB 104, 0
        DATA32
        DD 921725000
        DATA8
        DB 8, 0
        DATA32
        DD 922450000
        DATA8
        DB 40, 0
        DATA32
        DD 923537500
        DATA8
        DB 8, 0
        DATA32
        DD 923725000
        DATA8
        DB 40, 0
        DATA32
        DD 924162500
        DATA8
        DB 104, 0
        DATA32
        DD 924450000
        DATA8
        DB 72, 0
        DATA32
        DD 924650000
        DATA8
        DB 8, 0
        DATA32
        DD 925212500
        DATA8
        DB 72, 0
        DATA32
        DD 926312500
        DATA8
        DB 40, 0
        DATA32
        DD 926362500
        DATA8
        DB 72, 0
        DATA32
        DD 927537500
        DATA8
        DB 8, 0
        DATA32
        DD 927875000
        DATA8
        DB 104, 0
        DATA32
        DD 928000000
        DATA8
        DB 72, 0
        DATA32
        DD 902237500
        DATA8
        DB 104, 0
        DATA32
        DD 902450000
        DATA8
        DB 40, 0
        DATA32
        DD 903625000
        DATA8
        DB 8, 0
        DATA32
        DD 903750000
        DATA8
        DB 104, 0
        DATA32
        DD 904037500
        DATA8
        DB 72, 0
        DATA32
        DD 904312500
        DATA8
        DB 40, 0
        DATA32
        DD 904662500
        DATA8
        DB 8, 0
        DATA32
        DD 905062500
        DATA8
        DB 104, 0
        DATA32
        DD 905312500
        DATA8
        DB 40, 0
        DATA32
        DD 905937500
        DATA8
        DB 8, 0
        DATA32
        DD 906162500
        DATA8
        DB 40, 0
        DATA32
        DD 906450000
        DATA8
        DB 104, 0
        DATA32
        DD 907537500
        DATA8
        DB 8, 0
        DATA32
        DD 907850000
        DATA8
        DB 40, 0
        DATA32
        DD 908237500
        DATA8
        DB 72, 0
        DATA32
        DD 908300000
        DATA8
        DB 104, 0
        DATA32
        DD 908662500
        DATA8
        DB 8, 0
        DATA32
        DD 908950000
        DATA8
        DB 40, 0
        DATA32
        DD 909312500
        DATA8
        DB 104, 0
        DATA32
        DD 909625000
        DATA8
        DB 8, 0
        DATA32
        DD 909887500
        DATA8
        DB 40, 0
        DATA32
        DD 910112500
        DATA8
        DB 72, 0
        DATA32
        DD 911187500
        DATA8
        DB 8, 0
        DATA32
        DD 913500000
        DATA8
        DB 8, 0
        DATA32
        DD 913875000
        DATA8
        DB 8, 0
        DATA32
        DD 914137500
        DATA8
        DB 72, 0
        DATA32
        DD 914450000
        DATA8
        DB 40, 0
        DATA32
        DD 915537500
        DATA8
        DB 8, 0
        DATA32
        DD 916025000
        DATA8
        DB 104, 0
        DATA32
        DD 916337500
        DATA8
        DB 40, 0
        DATA32
        DD 916662500
        DATA8
        DB 8, 0
        DATA32
        DD 916750000
        DATA8
        DB 104, 0
        DATA32
        DD 917012500
        DATA8
        DB 72, 0
        DATA32
        DD 917312500
        DATA8
        DB 40, 0
        DATA32
        DD 917537500
        DATA8
        DB 8, 0
        DATA32
        DD 917825000
        DATA8
        DB 72, 0
        DATA32
        DD 918450000
        DATA8
        DB 40, 0
        DATA32
        DD 919662500
        DATA8
        DB 8, 0
        DATA32
        DD 919975000
        DATA8
        DB 40, 0
        DATA32
        DD 920300000
        DATA8
        DB 104, 0
        DATA32
        DD 920662500
        DATA8
        DB 8, 0
        DATA32
        DD 920925000
        DATA8
        DB 40, 0
        DATA32
        DD 921237500
        DATA8
        DB 72, 0
        DATA32
        DD 921312500
        DATA8
        DB 104, 0
        DATA32
        DD 921625000
        DATA8
        DB 8, 0
        DATA32
        DD 922450000
        DATA8
        DB 40, 0
        DATA32
        DD 923662500
        DATA8
        DB 8, 0
        DATA32
        DD 923825000
        DATA8
        DB 72, 0
        DATA32
        DD 924162500
        DATA8
        DB 104, 0
        DATA32
        DD 925225000
        DATA8
        DB 72, 0
        DATA32
        DD 925312500
        DATA8
        DB 40, 0
        DATA32
        DD 925537500
        DATA8
        DB 8, 0
        DATA32
        DD 926312500
        DATA8
        DB 40, 0
        DATA32
        DD 927537500
        DATA8
        DB 8, 0
        DATA32
        DD 928000000
        DATA8
        DB 104, 0
        DATA32
        DD 902237500
        DATA8
        DB 104, 0
        DATA32
        DD 902450000
        DATA8
        DB 40, 0
        DATA32
        DD 903662500
        DATA8
        DB 8, 0
        DATA32
        DD 903937500
        DATA8
        DB 104, 0
        DATA32
        DD 904012500
        DATA8
        DB 72, 0
        DATA32
        DD 904312500
        DATA8
        DB 40, 0
        DATA32
        DD 904662500
        DATA8
        DB 8, 0
        DATA32
        DD 905062500
        DATA8
        DB 104, 0
        DATA32
        DD 905312500
        DATA8
        DB 40, 0
        DATA32
        DD 905937500
        DATA8
        DB 8, 0
        DATA32
        DD 906162500
        DATA8
        DB 40, 0
        DATA32
        DD 906450000
        DATA8
        DB 104, 0
        DATA32
        DD 907537500
        DATA8
        DB 8, 0
        DATA32
        DD 907850000
        DATA8
        DB 40, 0
        DATA32
        DD 908112500
        DATA8
        DB 72, 0
        DATA32
        DD 908300000
        DATA8
        DB 104, 0
        DATA32
        DD 908662500
        DATA8
        DB 8, 0
        DATA32
        DD 908975000
        DATA8
        DB 40, 0
        DATA32
        DD 909237500
        DATA8
        DB 72, 0
        DATA32
        DD 909312500
        DATA8
        DB 104, 0
        DATA32
        DD 909625000
        DATA8
        DB 8, 0
        DATA32
        DD 909887500
        DATA8
        DB 40, 0
        DATA32
        DD 910112500
        DATA8
        DB 72, 0
        DATA32
        DD 911187500
        DATA8
        DB 8, 0
        DATA32
        DD 913500000
        DATA8
        DB 8, 0
        DATA32
        DD 913875000
        DATA8
        DB 8, 0
        DATA32
        DD 914137500
        DATA8
        DB 72, 0
        DATA32
        DD 914450000
        DATA8
        DB 40, 0
        DATA32
        DD 915537500
        DATA8
        DB 8, 0
        DATA32
        DD 916025000
        DATA8
        DB 104, 0
        DATA32
        DD 916100000
        DATA8
        DB 72, 0
        DATA32
        DD 916362500
        DATA8
        DB 40, 0
        DATA32
        DD 916662500
        DATA8
        DB 8, 0
        DATA32
        DD 916750000
        DATA8
        DB 104, 0
        DATA32
        DD 916975000
        DATA8
        DB 72, 0
        DATA32
        DD 917012500
        DATA8
        DB 104, 0
        DATA32
        DD 917312500
        DATA8
        DB 40, 0
        DATA32
        DD 917537500
        DATA8
        DB 8, 0
        DATA32
        DD 917825000
        DATA8
        DB 72, 0
        DATA32
        DD 918450000
        DATA8
        DB 40, 0
        DATA32
        DD 919662500
        DATA8
        DB 8, 0
        DATA32
        DD 919975000
        DATA8
        DB 40, 0
        DATA32
        DD 920300000
        DATA8
        DB 104, 0
        DATA32
        DD 920662500
        DATA8
        DB 8, 0
        DATA32
        DD 920925000
        DATA8
        DB 40, 0
        DATA32
        DD 921237500
        DATA8
        DB 72, 0
        DATA32
        DD 921400000
        DATA8
        DB 104, 0
        DATA32
        DD 921625000
        DATA8
        DB 8, 0
        DATA32
        DD 922450000
        DATA8
        DB 40, 0
        DATA32
        DD 923537500
        DATA8
        DB 8, 0
        DATA32
        DD 923750000
        DATA8
        DB 40, 0
        DATA32
        DD 923825000
        DATA8
        DB 72, 0
        DATA32
        DD 924162500
        DATA8
        DB 104, 0
        DATA32
        DD 924450000
        DATA8
        DB 72, 0
        DATA32
        DD 924662500
        DATA8
        DB 8, 0
        DATA32
        DD 925225000
        DATA8
        DB 72, 0
        DATA32
        DD 925312500
        DATA8
        DB 40, 0
        DATA32
        DD 925537500
        DATA8
        DB 8, 0
        DATA32
        DD 926387500
        DATA8
        DB 40, 0
        DATA32
        DD 927625000
        DATA8
        DB 8, 0
        DATA32
        DD 928000000
        DATA8
        DB 104, 0
        DATA32
        DD 917825000
        DATA8
        DB 72, 0
        DATA32
        DD 918450000
        DATA8
        DB 40, 0
        DATA32
        DD 919650000
        DATA8
        DB 8, 0
        DATA32
        DD 919950000
        DATA8
        DB 40, 0
        DATA32
        DD 920337500
        DATA8
        DB 104, 0
        DATA32
        DD 920762500
        DATA8
        DB 8, 0
        DATA32
        DD 920987500
        DATA8
        DB 40, 0
        DATA32
        DD 921212500
        DATA8
        DB 72, 0
        DATA32
        DD 921462500
        DATA8
        DB 104, 0
        DATA32
        DD 923500000
        DATA8
        DB 8, 0
        DATA32
        DD 917825000
        DATA8
        DB 72, 0
        DATA32
        DD 918450000
        DATA8
        DB 40, 0
        DATA32
        DD 919662500
        DATA8
        DB 8, 0
        DATA32
        DD 919975000
        DATA8
        DB 40, 0
        DATA32
        DD 920300000
        DATA8
        DB 104, 0
        DATA32
        DD 920662500
        DATA8
        DB 8, 0
        DATA32
        DD 920937500
        DATA8
        DB 40, 0
        DATA32
        DD 921200000
        DATA8
        DB 72, 0
        DATA32
        DD 921375000
        DATA8
        DB 104, 0
        DATA32
        DD 923500000
        DATA8
        DB 8, 0
        DATA32
        DD 917825000
        DATA8
        DB 72, 0
        DATA32
        DD 918450000
        DATA8
        DB 40, 0
        DATA32
        DD 919662500
        DATA8
        DB 8, 0
        DATA32
        DD 919975000
        DATA8
        DB 40, 0
        DATA32
        DD 920300000
        DATA8
        DB 104, 0
        DATA32
        DD 920662500
        DATA8
        DB 8, 0
        DATA32
        DD 920937500
        DATA8
        DB 40, 0
        DATA32
        DD 921200000
        DATA8
        DB 72, 0
        DATA32
        DD 921375000
        DATA8
        DB 104, 0
        DATA32
        DD 923500000
        DATA8
        DB 8, 0
        DATA32
        DD 920312500
        DATA8
        DB 104, 0
        DATA32
        DD 920762500
        DATA8
        DB 8, 0
        DATA32
        DD 920975000
        DATA8
        DB 40, 0
        DATA32
        DD 921212500
        DATA8
        DB 72, 0
        DATA32
        DD 921412500
        DATA8
        DB 104, 0
        DATA32
        DD 921725000
        DATA8
        DB 8, 0
        DATA32
        DD 922450000
        DATA8
        DB 40, 0
        DATA32
        DD 923537500
        DATA8
        DB 8, 0
        DATA32
        DD 923725000
        DATA8
        DB 40, 0
        DATA32
        DD 923850000
        DATA8
        DB 72, 0
        DATA32
        DD 924162500
        DATA8
        DB 104, 0
        DATA32
        DD 924450000
        DATA8
        DB 72, 0
        DATA32
        DD 924650000
        DATA8
        DB 8, 0
        DATA32
        DD 925212500
        DATA8
        DB 72, 0
        DATA32
        DD 926312500
        DATA8
        DB 40, 0
        DATA32
        DD 926362500
        DATA8
        DB 72, 0
        DATA32
        DD 927537500
        DATA8
        DB 8, 0
        DATA32
        DD 927875000
        DATA8
        DB 104, 0
        DATA32
        DD 928000000
        DATA8
        DB 72, 0
        DATA32
        DD 920312500
        DATA8
        DB 104, 0
        DATA32
        DD 920762500
        DATA8
        DB 8, 0
        DATA32
        DD 920925000
        DATA8
        DB 40, 0
        DATA32
        DD 921337500
        DATA8
        DB 72, 0
        DATA32
        DD 921412500
        DATA8
        DB 104, 0
        DATA32
        DD 921725000
        DATA8
        DB 8, 0
        DATA32
        DD 922450000
        DATA8
        DB 40, 0
        DATA32
        DD 923575000
        DATA8
        DB 8, 0
        DATA32
        DD 923675000
        DATA8
        DB 40, 0
        DATA32
        DD 924162500
        DATA8
        DB 104, 0
        DATA32
        DD 924487500
        DATA8
        DB 72, 0
        DATA32
        DD 924600000
        DATA8
        DB 8, 0
        DATA32
        DD 925212500
        DATA8
        DB 72, 0
        DATA32
        DD 926312500
        DATA8
        DB 40, 0
        DATA32
        DD 927537500
        DATA8
        DB 8, 0
        DATA32
        DD 927925000
        DATA8
        DB 104, 0
        DATA32
        DD 928000000
        DATA8
        DB 72, 0
        DATA32
        DD 920375000
        DATA8
        DB 104, 0
        DATA32
        DD 921087500
        DATA8
        DB 8, 0
        DATA32
        DD 921462500
        DATA8
        DB 72, 0
        DATA32
        DD 921837500
        DATA8
        DB 104, 0
        DATA32
        DD 922150000
        DATA8
        DB 8, 0
        DATA32
        DD 922375000
        DATA8
        DB 40, 0
        DATA32
        DD 923287500
        DATA8
        DB 8, 0
        DATA32
        DD 923775000
        DATA8
        DB 104, 0
        DATA32
        DD 923975000
        DATA8
        DB 40, 0
        DATA32
        DD 924387500
        DATA8
        DB 104, 0
        DATA32
        DD 924700000
        DATA8
        DB 72, 0
        DATA32
        DD 924925000
        DATA8
        DB 8, 0
        DATA32
        DD 926062500
        DATA8
        DB 40, 0
        DATA32
        DD 926412500
        DATA8
        DB 8, 0
        DATA32
        DD 926612500
        DATA8
        DB 72, 0
        DATA32
        DD 927287500
        DATA8
        DB 8, 0
        DATA32
        DD 928000000
        DATA8
        DB 104, 0
        DATA32
        DD 920350000
        DATA8
        DB 104, 0
        DATA32
        DD 921137500
        DATA8
        DB 8, 0
        DATA32
        DD 921575000
        DATA8
        DB 72, 0
        DATA32
        DD 921875000
        DATA8
        DB 104, 0
        DATA32
        DD 922150000
        DATA8
        DB 8, 0
        DATA32
        DD 922375000
        DATA8
        DB 40, 0
        DATA32
        DD 923287500
        DATA8
        DB 8, 0
        DATA32
        DD 924387500
        DATA8
        DB 104, 0
        DATA32
        DD 925137500
        DATA8
        DB 72, 0
        DATA32
        DD 926062500
        DATA8
        DB 40, 0
        DATA32
        DD 927287500
        DATA8
        DB 8, 0
        DATA32
        DD 928000000
        DATA8
        DB 104, 0
        DATA32
        DD 920375000
        DATA8
        DB 104, 0
        DATA32
        DD 921087500
        DATA8
        DB 8, 0
        DATA32
        DD 921462500
        DATA8
        DB 72, 0
        DATA32
        DD 921837500
        DATA8
        DB 104, 0
        DATA32
        DD 922150000
        DATA8
        DB 8, 0
        DATA32
        DD 922375000
        DATA8
        DB 40, 0
        DATA32
        DD 923287500
        DATA8
        DB 8, 0
        DATA32
        DD 923775000
        DATA8
        DB 104, 0
        DATA32
        DD 923975000
        DATA8
        DB 40, 0
        DATA32
        DD 924387500
        DATA8
        DB 104, 0
        DATA32
        DD 924700000
        DATA8
        DB 72, 0
        DATA32
        DD 924925000
        DATA8
        DB 8, 0
        DATA32
        DD 926062500
        DATA8
        DB 40, 0
        DATA32
        DD 926412500
        DATA8
        DB 8, 0
        DATA32
        DD 926612500
        DATA8
        DB 72, 0
        DATA32
        DD 927287500
        DATA8
        DB 8, 0
        DATA32
        DD 928000000
        DATA8
        DB 104, 0
        DATA32
        DD 902162500
        DATA8
        DB 104, 0
        DATA32
        DD 902362500
        DATA8
        DB 72, 0
        DATA32
        DD 903662500
        DATA8
        DB 8, 0
        DATA32
        DD 903912500
        DATA8
        DB 104, 0
        DATA32
        DD 904037500
        DATA8
        DB 72, 0
        DATA32
        DD 904287500
        DATA8
        DB 40, 0
        DATA32
        DD 904687500
        DATA8
        DB 8, 0
        DATA32
        DD 905112500
        DATA8
        DB 104, 0
        DATA32
        DD 905387500
        DATA8
        DB 40, 0
        DATA32
        DD 905587500
        DATA8
        DB 8, 0
        DATA32
        DD 905787500
        DATA8
        DB 72, 0
        DATA32
        DD 906037500
        DATA8
        DB 8, 0
        DATA32
        DD 906162500
        DATA8
        DB 40, 0
        DATA32
        DD 906450000
        DATA8
        DB 104, 0
        DATA32
        DD 907537500
        DATA8
        DB 8, 0
        DATA32
        DD 907850000
        DATA8
        DB 40, 0
        DATA32
        DD 908112500
        DATA8
        DB 72, 0
        DATA32
        DD 908400000
        DATA8
        DB 104, 0
        DATA32
        DD 908650000
        DATA8
        DB 8, 0
        DATA32
        DD 909075000
        DATA8
        DB 40, 0
        DATA32
        DD 909212500
        DATA8
        DB 72, 0
        DATA32
        DD 909412500
        DATA8
        DB 104, 0
        DATA32
        DD 911187500
        DATA8
        DB 8, 0
        DATA32
        DD 913500000
        DATA8
        DB 8, 0
        DATA32
        DD 913875000
        DATA8
        DB 8, 0
        DATA32
        DD 914137500
        DATA8
        DB 72, 0
        DATA32
        DD 914450000
        DATA8
        DB 40, 0
        DATA32
        DD 915537500
        DATA8
        DB 8, 0
        DATA32
        DD 915875000
        DATA8
        DB 104, 0
        DATA32
        DD 916137500
        DATA8
        DB 72, 0
        DATA32
        DD 916450000
        DATA8
        DB 40, 0
        DATA32
        DD 916762500
        DATA8
        DB 8, 0
        DATA32
        DD 916850000
        DATA8
        DB 104, 0
        DATA32
        DD 917112500
        DATA8
        DB 72, 0
        DATA32
        DD 917300000
        DATA8
        DB 40, 0
        DATA32
        DD 917412500
        DATA8
        DB 72, 0
        DATA32
        DD 917537500
        DATA8
        DB 8, 0
        DATA32
        DD 917825000
        DATA8
        DB 72, 0
        DATA32
        DD 918450000
        DATA8
        DB 40, 0
        DATA32
        DD 919662500
        DATA8
        DB 8, 0
        DATA32
        DD 919975000
        DATA8
        DB 40, 0
        DATA32
        DD 920400000
        DATA8
        DB 104, 0
        DATA32
        DD 920762500
        DATA8
        DB 8, 0
        DATA32
        DD 920925000
        DATA8
        DB 40, 0
        DATA32
        DD 921212500
        DATA8
        DB 72, 0
        DATA32
        DD 921412500
        DATA8
        DB 104, 0
        DATA32
        DD 921725000
        DATA8
        DB 8, 0
        DATA32
        DD 922450000
        DATA8
        DB 40, 0
        DATA32
        DD 923537500
        DATA8
        DB 8, 0
        DATA32
        DD 923725000
        DATA8
        DB 40, 0
        DATA32
        DD 924162500
        DATA8
        DB 104, 0
        DATA32
        DD 924450000
        DATA8
        DB 72, 0
        DATA32
        DD 924650000
        DATA8
        DB 8, 0
        DATA32
        DD 925212500
        DATA8
        DB 72, 0
        DATA32
        DD 926312500
        DATA8
        DB 40, 0
        DATA32
        DD 926362500
        DATA8
        DB 72, 0
        DATA32
        DD 927537500
        DATA8
        DB 8, 0
        DATA32
        DD 927875000
        DATA8
        DB 104, 0
        DATA32
        DD 928000000
        DATA8
        DB 72, 0
        DATA32
        DD 902700000
        DATA8
        DB 104, 0
        DATA32
        DD 903637500
        DATA8
        DB 8, 0
        DATA32
        DD 903912500
        DATA8
        DB 104, 0
        DATA32
        DD 904562500
        DATA8
        DB 72, 0
        DATA32
        DD 905137500
        DATA8
        DB 8, 0
        DATA32
        DD 905487500
        DATA8
        DB 104, 0
        DATA32
        DD 905537500
        DATA8
        DB 72, 0
        DATA32
        DD 905762500
        DATA8
        DB 40, 0
        DATA32
        DD 906287500
        DATA8
        DB 72, 0
        DATA32
        DD 906700000
        DATA8
        DB 104, 0
        DATA32
        DD 907287500
        DATA8
        DB 8, 0
        DATA32
        DD 907600000
        DATA8
        DB 40, 0
        DATA32
        DD 908212500
        DATA8
        DB 72, 0
        DATA32
        DD 908700000
        DATA8
        DB 104, 0
        DATA32
        DD 909137500
        DATA8
        DB 8, 0
        DATA32
        DD 909450000
        DATA8
        DB 40, 0
        DATA32
        DD 909712500
        DATA8
        DB 72, 0
        DATA32
        DD 909862500
        DATA8
        DB 104, 0
        DATA32
        DD 911387500
        DATA8
        DB 8, 0
        DATA32
        DD 914200000
        DATA8
        DB 8, 0
        DATA32
        DD 914387500
        DATA8
        DB 72, 0
        DATA32
        DD 914700000
        DATA8
        DB 40, 0
        DATA32
        DD 915287500
        DATA8
        DB 8, 0
        DATA32
        DD 916012500
        DATA8
        DB 104, 0
        DATA32
        DD 916387500
        DATA8
        DB 72, 0
        DATA32
        DD 916700000
        DATA8
        DB 40, 0
        DATA32
        DD 917137500
        DATA8
        DB 8, 0
        DATA32
        DD 917512500
        DATA8
        DB 104, 0
        DATA32
        DD 918075000
        DATA8
        DB 72, 0
        DATA32
        DD 918700000
        DATA8
        DB 40, 0
        DATA32
        DD 919550000
        DATA8
        DB 8, 0
        DATA32
        DD 920000000
        DATA8
        DB 72, 0
        DATA32
        DD 920337500
        DATA8
        DB 104, 0
        DATA32
        DD 921137500
        DATA8
        DB 8, 0
        DATA32
        DD 921575000
        DATA8
        DB 72, 0
        DATA32
        DD 921875000
        DATA8
        DB 104, 0
        DATA32
        DD 922150000
        DATA8
        DB 8, 0
        DATA32
        DD 922375000
        DATA8
        DB 40, 0
        DATA32
        DD 923287500
        DATA8
        DB 8, 0
        DATA32
        DD 923425000
        DATA8
        DB 72, 0
        DATA32
        DD 924387500
        DATA8
        DB 104, 0
        DATA32
        DD 925137500
        DATA8
        DB 72, 0
        DATA32
        DD 926062500
        DATA8
        DB 40, 0
        DATA32
        DD 926237500
        DATA8
        DB 104, 0
        DATA32
        DD 927287500
        DATA8
        DB 8, 0
        DATA32
        DD 927775000
        DATA8
        DB 104, 0
        DATA32
        DD 928000000
        DATA8
        DB 72, 0
        DATA32
        DD 871537500
        DATA8
        DB 8, 0
        DATA32
        DD 872275000
        DATA8
        DB 104, 0
        DATA32
        DD 873825000
        DATA8
        DB 72, 0
        DATA32
        DD 875537500
        DATA8
        DB 8, 0
        DATA32
        DD 875750000
        DATA8
        DB 40, 0
        DATA32
        DD 876000000
        DATA8
        DB 104, 0
        DATA32
        DD 902162500
        DATA8
        DB 104, 0
        DATA32
        DD 902362500
        DATA8
        DB 72, 0
        DATA32
        DD 903662500
        DATA8
        DB 8, 0
        DATA32
        DD 903912500
        DATA8
        DB 104, 0
        DATA32
        DD 904037500
        DATA8
        DB 72, 0
        DATA32
        DD 904287500
        DATA8
        DB 40, 0
        DATA32
        DD 904687500
        DATA8
        DB 8, 0
        DATA32
        DD 905112500
        DATA8
        DB 8, 0
        DATA32
        DD 905537500
        DATA8
        DB 104, 0
        DATA32
        DD 905587500
        DATA8
        DB 72, 0
        DATA32
        DD 905787500
        DATA8
        DB 72, 0
        DATA32
        DD 906037500
        DATA8
        DB 8, 0
        DATA32
        DD 906162500
        DATA8
        DB 8, 0
        DATA32
        DD 906450000
        DATA8
        DB 104, 0
        DATA32
        DD 907537500
        DATA8
        DB 8, 0
        DATA32
        DD 907850000
        DATA8
        DB 40, 0
        DATA32
        DD 908112500
        DATA8
        DB 72, 0
        DATA32
        DD 908400000
        DATA8
        DB 104, 0
        DATA32
        DD 908650000
        DATA8
        DB 8, 0
        DATA32
        DD 909075000
        DATA8
        DB 40, 0
        DATA32
        DD 909450000
        DATA8
        DB 72, 0
        DATA32
        DD 909812500
        DATA8
        DB 104, 0
        DATA32
        DD 911187500
        DATA8
        DB 8, 0
        DATA32
        DD 913500000
        DATA8
        DB 8, 0
        DATA32
        DD 913875000
        DATA8
        DB 8, 0
        DATA32
        DD 914137500
        DATA8
        DB 72, 0
        DATA32
        DD 914450000
        DATA8
        DB 40, 0
        DATA32
        DD 915537500
        DATA8
        DB 8, 0
        DATA32
        DD 915875000
        DATA8
        DB 104, 0
        DATA32
        DD 916137500
        DATA8
        DB 72, 0
        DATA32
        DD 916450000
        DATA8
        DB 40, 0
        DATA32
        DD 916762500
        DATA8
        DB 8, 0
        DATA32
        DD 916850000
        DATA8
        DB 104, 0
        DATA32
        DD 917112500
        DATA8
        DB 104, 0
        DATA32
        DD 917300000
        DATA8
        DB 104, 0
        DATA32
        DD 917412500
        DATA8
        DB 72, 0
        DATA32
        DD 917537500
        DATA8
        DB 72, 0
        DATA32
        DD 917825000
        DATA8
        DB 72, 0
        DATA32
        DD 918450000
        DATA8
        DB 40, 0
        DATA32
        DD 919662500
        DATA8
        DB 8, 0
        DATA32
        DD 919975000
        DATA8
        DB 40, 0
        DATA32
        DD 920400000
        DATA8
        DB 104, 0
        DATA32
        DD 920762500
        DATA8
        DB 8, 0
        DATA32
        DD 920925000
        DATA8
        DB 8, 0
        DATA32
        DD 921212500
        DATA8
        DB 72, 0
        DATA32
        DD 921412500
        DATA8
        DB 72, 0
        DATA32
        DD 921725000
        DATA8
        DB 72, 0
        DATA32
        DD 922450000
        DATA8
        DB 40, 0
        DATA32
        DD 923537500
        DATA8
        DB 8, 0
        DATA32
        DD 923725000
        DATA8
        DB 40, 0
        DATA32
        DD 924162500
        DATA8
        DB 104, 0
        DATA32
        DD 924450000
        DATA8
        DB 72, 0
        DATA32
        DD 924650000
        DATA8
        DB 8, 0
        DATA32
        DD 925212500
        DATA8
        DB 72, 0
        DATA32
        DD 926312500
        DATA8
        DB 40, 0
        DATA32
        DD 926362500
        DATA8
        DB 72, 0
        DATA32
        DD 927537500
        DATA8
        DB 8, 0
        DATA32
        DD 927875000
        DATA8
        DB 104, 0
        DATA32
        DD 928000000
        DATA8
        DB 72, 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
        DATA8
        DB 72, 8, 96, 32

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpIdLenChkGetReq[68][2]
_RpIdLenChkGetReq:
        DATA8
        DB 0, 1, 1, 8, 2, 1, 3, 1, 4, 1, 5, 1, 6, 1, 7, 2, 8, 2, 9, 1, 10, 1
        DB 11, 1, 12, 2, 13, 2, 14, 8, 15, 1, 16, 1, 17, 1, 18, 2, 19, 2, 20, 8
        DB 21, 1, 22, 1, 23, 1, 24, 1, 25, 1, 26, 1, 27, 2, 28, 2, 29, 1, 30, 1
        DB 31, 1, 32, 1, 33, 2, 34, 2, 35, 1, 36, 1, 37, 1, 38, 1, 39, 1, 40, 1
        DB 41, 1, 43, 2, 42, 1, 44, 1, 45, 1, 46, 2, 47, 1, 48, 2, 49, 1, 50, 1
        DB 51, 2, 52, 2, 53, 1, 54, 1, 55, 1, 56, 2, 57, 1, 58, 1, 59, 2, 60, 2
        DB 61, 2, 62, 4, 63, 4, 64, 1, 65, 1, 66, 4, 67, 4

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpIdLenChkSetReq[63][2]
_RpIdLenChkSetReq:
        DATA8
        DB 0, 1, 2, 1, 4, 1, 5, 1, 6, 1, 7, 2, 8, 2, 9, 1, 10, 1, 11, 1, 12, 2
        DB 13, 2, 14, 8, 15, 1, 16, 1, 17, 1, 18, 2, 19, 2, 20, 8, 21, 1, 22, 1
        DB 23, 1, 24, 1, 25, 1, 26, 1, 27, 2, 28, 2, 29, 1, 30, 1, 31, 1, 32, 1
        DB 33, 2, 34, 2, 35, 1, 36, 1, 37, 1, 38, 1, 39, 1, 40, 1, 41, 1, 42, 1
        DB 44, 1, 45, 1, 46, 2, 47, 1, 48, 2, 49, 1, 50, 1, 51, 2, 52, 2, 53, 1
        DB 54, 1, 55, 1, 56, 2, 57, 1, 58, 1, 59, 2, 60, 2, 61, 2, 62, 4, 64, 1
        DB 65, 1, 67, 4

        SECTION `.dataf`:DATA:REORDER:NOROOT(1)
// static uint8_t const *__far RpFreqBandTblRssiLossPtr[6]
_RpFreqBandTblRssiLossPtr:
        DATA32
        DD _RpFreqBandTblRssiLoss0xFFdBm, _RpFreqBandTblRssiLoss0x00dBm
        DD _RpFreqBandTblRssiLoss0x01dBm, _RpFreqBandTblRssiLoss0x02dBm
        DD _RpFreqBandTblRssiLoss0x03dBm, _RpFreqBandTblRssiLoss0x04dBm

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpBandModeOffset[18][8]
_RpBandModeOffset:
        DATA8
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 10, 18, 3, 7, 11, 4, 7, 0, 1, 2, 0, 0
        DB 0, 0, 3, 0, 1, 2, 0, 0, 0, 0, 3, 4, 13, 15, 5, 0, 0, 0, 4, 4, 13, 15
        DB 5, 0, 0, 0, 4, 4, 9, 16, 19, 14, 5, 0, 6, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
        DB 17, 7, 17, 0, 0, 0, 0, 3, 3, 7, 12, 0, 0, 0, 0, 3, 13, 0, 0, 0, 0, 0
        DB 0, 1, 6, 8, 11, 5, 0, 0, 0, 4
// uint16_t const RpFreqBandIdToDataRate[20]
_RpFreqBandIdToDataRate:
        DATA16
        DW 10, 20, 40, 50, 50, 50, 50, 100, 100, 100, 100, 150, 150, 150, 150
        DW 200, 200, 300, 200, 400

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint32_t const RpOpeModeIndex920MHz[7][4]
_RpOpeModeIndex920MHz:
        DATA32
        DD 0, 0, 0, 0, 920600000, 200000, 928000000, 37, 920700000, 200000
        DD 927900000, 36, 920800000, 200000, 927800000, 35, 920800000, 200000
        DD 927800000, 35, 920700000, 200000, 927900000, 36, 920600000, 200000
        DD 928000000, 37
        DD 0, 0, 0, 0, 863125000, 200000, 869725000, 33, 863225000, 400000
        DD 869625000, 16, 863225000, 400000, 869625000, 16, 863100000, 100000
        DD 869900000, 68, 863100000, 100000, 869900000, 68, 863100000, 100000
        DD 869900000, 68, 863100000, 100000, 869900000, 68
        DD 0, 0, 0, 0, 896012500, 25000, 900962500, 198, 896025000, 50000
        DD 900925000, 98, 896050000, 100000, 900850000, 48
        DD 0, 0, 0, 0, 901012500, 25000, 901962500, 38, 901025000, 50000
        DD 901925000, 18, 901050000, 100000, 901850000, 8
        DD 0, 0, 0, 0, 902200000, 200000, 927800000, 128, 902400000, 400000
        DD 927600000, 63, 902400000, 400000, 927600000, 63, 902200000, 200000
        DD 927800000, 128
        DD 0, 0, 0, 0, 917100000, 200000, 923300000, 31, 917300000, 400000
        DD 923300000, 15, 917300000, 400000, 923300000, 15, 917100000, 200000
        DD 923300000, 31
        DD 0, 0, 0, 0, 920800000, 200000, 927800000, 35, 902200000, 200000
        DD 927800000, 128, 902600000, 600000, 927200000, 41
        DD 0, 0, 0, 0, 870100000, 100000, 875900000, 58, 870100000, 100000
        DD 875900000, 58, 870100000, 100000, 875900000, 58
        DD 0, 0, 0, 0, 902200000, 200000, 927800000, 128
        DD 0, 0, 0, 0, 920625000, 250000, 924875000, 17, 920625000, 250000
        DD 924875000, 17, 920625000, 250000, 924875000, 17, 920625000, 250000
        DD 924875000, 17

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// uint8_t const RpFreqBandTbl[20][80]
_RpFreqBandTbl:
        DATA8
        DB 96, 9, 2, 170, 2, 0, 11, 11, 116, 0, 116, 0, 6, 10, 95, 13, 163, 4
        DB 16, 241, 9, 60, 60, 60, 60, 60, 120, 120, 0, 0, 146, 146, 40, 2, 2
        DB 183, 183, 1, 1, 5, 246, 166, 27, 15, 3, 52, 0, 2, 2, 2, 0, 0, 0, 130
        DB 130, 70, 102, 32, 85, 54, 22, 22, 4, 56, 72, 72, 0, 0, 3, 3, 5, 48
        DB 255, 255, 255, 63, 63, 255, 63, 63, 176, 4, 2, 170, 2, 1, 17, 17
        DB 146, 0, 146, 0, 6, 10, 95, 13, 164, 4, 16, 241, 9, 60, 60, 60, 2, 2
        DB 16, 16, 0, 0, 158, 158, 40, 2, 2, 33, 33, 2, 2, 4, 246, 166, 27, 16
        DB 3, 52, 0, 85, 85, 85, 0, 0, 0, 130, 130, 85, 85, 32, 85, 54, 22, 22
        DB 4, 56, 72, 72, 0, 0, 3, 3, 0, 48, 255, 255, 255, 63, 63, 255, 63, 63
        DB 88, 2, 2, 170, 2, 1, 19, 19, 164, 0, 164, 0, 6, 10, 95, 13, 160, 4
        DB 16, 241, 9, 30, 30, 30, 12, 12, 30, 30, 0, 0, 150, 150, 40, 2, 2, 64
        DB 64, 3, 3, 4, 246, 166, 26, 15, 3, 52, 0, 50, 50, 50, 0, 0, 0, 130
        DB 130, 70, 85, 32, 85, 54, 22, 22, 4, 56, 72, 72, 1, 1, 3, 3, 28, 43
        DB 255, 255, 255, 63, 63, 255, 63, 63, 224, 1, 2, 170, 2, 1, 25, 25
        DB 205, 0, 205, 0, 6, 10, 95, 13, 160, 4, 16, 241, 9, 34, 34, 13, 4, 2
        DB 32, 24, 0, 24, 158, 254, 40, 2, 6, 230, 145, 0, 3, 4, 246, 166, 27
        DB 15, 3, 52, 0, 85, 85, 102, 5, 5, 6, 130, 129, 85, 85, 32, 68, 54, 22
        DB 4, 16, 0, 72, 65, 1, 1, 3, 0, 18, 38, 50, 255, 255, 3, 63, 255, 3
        DB 63, 224, 1, 66, 170, 2, 0, 11, 14, 120, 0, 135, 0, 6, 10, 95, 13
        DB 157, 4, 16, 0, 1, 24, 9, 24, 12, 2, 24, 12, 0, 12, 150, 254, 46, 6
        DB 6, 230, 145, 0, 3, 4, 246, 166, 27, 15, 3, 52, 0, 187, 85, 102, 11
        DB 5, 6, 130, 129, 70, 102, 32, 85, 54, 22, 1, 16, 0, 72, 65, 2, 1, 3
        DB 0, 18, 38, 50, 255, 255, 6, 63, 255, 6, 63, 224, 1, 66, 170, 2, 1
        DB 11, 14, 120, 0, 135, 0, 6, 10, 95, 13, 157, 4, 16, 0, 1, 24, 9, 24
        DB 2, 2, 16, 12, 32, 12, 190, 254, 46, 6, 6, 230, 145, 0, 3, 4, 246
        DB 166, 27, 15, 3, 52, 0, 187, 0, 102, 11, 0, 6, 130, 129, 87, 102, 32
        DB 68, 54, 22, 1, 16, 0, 72, 65, 2, 1, 3, 0, 18, 38, 50, 255, 255, 6
        DB 63, 255, 6, 63, 224, 1, 66, 170, 2, 1, 11, 11, 120, 0, 120, 0, 6, 10
        DB 95, 13, 157, 4, 16, 0, 1, 24, 9, 24, 12, 2, 24, 12, 0, 12, 150, 254
        DB 46, 6, 6, 230, 145, 0, 3, 4, 246, 166, 27, 15, 3, 52, 0, 187, 85
        DB 102, 11, 5, 6, 130, 129, 70, 102, 32, 85, 54, 22, 1, 16, 0, 72, 65
        DB 2, 1, 3, 0, 18, 38, 50, 255, 255, 6, 63, 255, 6, 63, 240, 0, 2, 170
        DB 2, 1, 19, 19, 171, 0, 171, 0, 14, 23, 95, 15, 156, 4, 16, 241, 9, 10
        DB 10, 12, 2, 2, 12, 12, 0, 12, 150, 254, 40, 2, 6, 167, 167, 5, 5, 4
        DB 246, 167, 27, 15, 3, 52, 0, 0, 0, 102, 0, 0, 6, 130, 129, 87, 85, 32
        DB 67, 54, 22, 2, 4, 56, 69, 65, 2, 2, 3, 0, 23, 51, 255, 255, 255, 63
        DB 63, 255, 63, 63, 240, 0, 2, 170, 2, 2, 19, 19, 171, 0, 171, 0, 14
        DB 23, 95, 15, 156, 4, 16, 241, 9, 10, 10, 12, 2, 2, 18, 12, 0, 12, 158
        DB 254, 40, 2, 6, 167, 167, 5, 5, 4, 246, 167, 27, 15, 3, 52, 0, 0, 0
        DB 102, 0, 0, 6, 130, 129, 87, 85, 32, 67, 54, 22, 2, 4, 56, 69, 65, 2
        DB 2, 3, 0, 23, 51, 255, 255, 86, 4, 4, 86, 4, 4, 240, 0, 66, 170, 2, 1
        DB 22, 27, 187, 0, 212, 0, 9, 15, 95, 17, 156, 4, 16, 0, 1, 10, 10, 12
        DB 2, 2, 20, 6, 0, 6, 158, 254, 46, 6, 6, 164, 164, 5, 5, 4, 251, 166
        DB 28, 15, 3, 52, 32, 0, 0, 102, 0, 0, 6, 130, 129, 87, 85, 32, 85, 54
        DB 22, 15, 16, 0, 72, 65, 2, 2, 3, 0, 21, 28, 50, 50, 86, 3, 3, 86, 3
        DB 3, 240, 0, 66, 170, 2, 1, 22, 22, 187, 0, 187, 0, 9, 15, 95, 17, 156
        DB 4, 16, 0, 1, 10, 10, 12, 2, 2, 10, 6, 0, 6, 158, 254, 46, 6, 6, 164
        DB 164, 5, 5, 4, 251, 166, 28, 15, 3, 52, 32, 0, 0, 102, 0, 0, 6, 130
        DB 129, 87, 85, 32, 85, 54, 22, 15, 16, 0, 72, 65, 2, 2, 3, 0, 21, 28
        DB 255, 255, 255, 63, 63, 255, 63, 63, 160, 0, 2, 170, 2, 1, 24, 24
        DB 196, 0, 196, 0, 23, 37, 95, 13, 170, 4, 16, 241, 9, 8, 8, 8, 2, 2
        DB 12, 6, 0, 6, 158, 254, 46, 6, 6, 245, 245, 8, 8, 4, 246, 102, 27, 15
        DB 3, 52, 1, 50, 50, 102, 0, 0, 6, 130, 129, 85, 85, 32, 85, 54, 22, 12
        DB 4, 56, 72, 65, 2, 2, 3, 0, 18, 38, 255, 255, 86, 4, 4, 86, 4, 4, 160
        DB 0, 2, 170, 2, 1, 24, 24, 196, 0, 196, 0, 23, 37, 95, 13, 170, 4, 16
        DB 241, 9, 8, 8, 8, 2, 2, 14, 6, 0, 6, 158, 254, 46, 6, 6, 245, 245, 8
        DB 8, 4, 246, 102, 27, 15, 3, 52, 1, 0, 0, 102, 0, 0, 6, 130, 129, 85
        DB 85, 32, 85, 54, 22, 12, 4, 56, 72, 65, 2, 2, 3, 0, 18, 38, 255, 255
        DB 86, 4, 4, 86, 4, 4, 160, 0, 2, 170, 2, 0, 24, 24, 195, 0, 195, 0, 23
        DB 37, 95, 13, 170, 4, 16, 241, 9, 8, 8, 8, 2, 2, 12, 6, 0, 6, 158, 254
        DB 46, 6, 6, 245, 245, 8, 8, 4, 246, 102, 27, 15, 3, 52, 1, 0, 0, 102
        DB 0, 0, 6, 130, 129, 87, 85, 32, 85, 54, 22, 12, 4, 56, 72, 65, 2, 2
        DB 3, 0, 18, 38, 255, 255, 86, 4, 4, 86, 4, 4, 160, 0, 2, 170, 2, 0, 24
        DB 40, 195, 0, 20, 1, 23, 37, 95, 13, 170, 4, 16, 241, 9, 8, 8, 8, 2, 2
        DB 12, 6, 0, 6, 158, 254, 46, 6, 6, 245, 245, 8, 8, 4, 246, 102, 27, 15
        DB 3, 52, 1, 0, 0, 102, 0, 0, 6, 130, 129, 87, 85, 32, 85, 54, 22, 12
        DB 4, 56, 72, 65, 2, 2, 3, 0, 18, 38, 255, 255, 86, 4, 4, 86, 4, 4, 120
        DB 0, 2, 170, 2, 0, 27, 27, 220, 0, 220, 0, 38, 61, 159, 6, 172, 4, 16
        DB 241, 9, 6, 6, 6, 2, 2, 11, 6, 0, 6, 158, 254, 40, 2, 6, 70, 70, 12
        DB 12, 4, 246, 166, 27, 15, 3, 52, 0, 0, 0, 102, 0, 0, 6, 130, 129, 87
        DB 85, 32, 85, 54, 22, 12, 4, 56, 72, 65, 2, 2, 3, 0, 22, 44, 255, 255
        DB 86, 4, 4, 86, 4, 4, 120, 0, 66, 170, 2, 2, 27, 49, 220, 0, 76, 1, 14
        DB 23, 95, 15, 160, 4, 16, 0, 1, 6, 6, 6, 2, 2, 11, 4, 0, 4, 158, 254
        DB 46, 6, 6, 70, 70, 12, 12, 4, 246, 166, 27, 15, 3, 52, 1, 0, 0, 102
        DB 0, 0, 6, 130, 129, 87, 85, 32, 85, 54, 22, 8, 16, 0, 72, 65, 2, 2, 3
        DB 0, 19, 24, 255, 255, 255, 63, 63, 255, 63, 63, 80, 0, 2, 170, 2, 2
        DB 32, 32, 8, 1, 8, 1, 55, 88, 95, 4, 192, 4, 16, 241, 9, 16, 4, 16, 2
        DB 2, 12, 6, 0, 6, 158, 254, 40, 2, 6, 221, 221, 19, 19, 4, 246, 167
        DB 27, 14, 2, 51, 2, 0, 0, 102, 0, 0, 6, 130, 129, 70, 85, 32, 84, 54
        DB 22, 12, 4, 56, 72, 65, 2, 2, 3, 0, 21, 33, 255, 255, 255, 0, 0, 255
        DB 0, 0, 240, 0, 131, 238, 6, 0, 38, 38, 86, 1, 86, 1, 14, 23, 95, 15
        DB 157, 12, 4, 241, 9, 12, 10, 12, 2, 2, 16, 16, 0, 0, 158, 158, 40, 2
        DB 2, 29, 29, 7, 7, 4, 246, 166, 27, 15, 3, 53, 0, 0, 0, 0, 0, 0, 0
        DB 130, 130, 87, 102, 48, 64, 47, 22, 22, 16, 48, 72, 72, 2, 2, 3, 3
        DB 16, 36, 255, 255, 86, 6, 6, 86, 6, 6, 120, 0, 131, 238, 6, 2, 54, 99
        DB 184, 1, 152, 2, 14, 23, 95, 15, 160, 12, 4, 241, 9, 6, 6, 6, 2, 2, 9
        DB 9, 0, 0, 158, 158, 46, 6, 6, 125, 125, 12, 12, 4, 246, 167, 27, 15
        DB 3, 52, 1, 0, 0, 0, 0, 0, 0, 130, 130, 87, 102, 48, 64, 47, 22, 22
        DB 16, 48, 72, 72, 2, 2, 3, 3, 17, 37, 255, 255, 86, 2, 2, 86, 2, 2

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint16_t const RpFreqBandTblSerial[20][3]
_RpFreqBandTblSerial:
        DATA16
        DW 3600, 8976, 20064, 3601, 8976, 20064, 3601, 8976, 20064, 3602, 9200
        DW 20080, 3600, 8976, 20064, 3602, 9200, 20080, 3601, 8976, 20064, 3601
        DW 8976, 20064, 3602, 8976, 20064, 3601, 8976, 20064, 3601, 8976, 20064
        DW 3601, 8976, 20064, 3602, 9200, 20080, 3600, 8976, 20064, 3600, 8976
        DW 20064, 3600, 8976, 20064, 3586, 9024, 20064, 3602, 8976, 20064, 3600
        DW 8976, 20064, 3586, 9024, 20064
        DATA8
        DB 0, 0, 0, 0, 0, 0, 0, 0, 218, 218, 202, 202, 202, 202, 186, 186, 186
        DB 186, 186, 186, 0, 0, 0, 0, 0, 0, 0, 0, 186, 186, 218, 218, 186, 186
        DB 186, 186
        DATA16
        DW 0, 0, 0, 0, 25362, 25354, 25354, 25354, 25354, 25354, 0, 0, 0, 0
        DW 25354, 25362, 25354, 25354

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// uint16_t const RpCcaVthOffsetTblDefault[20]
_RpCcaVthOffsetTblDefault:
        DATA16
        DW 12, 12, 4, 5, 0, 0, 0, 0, 0, 0, 0, 11, 11, 11, 11, 11, 0, 30, 0, 0
// uint16_t const RpCcaVthOffsetTblNarrow[20]
_RpCcaVthOffsetTblNarrow:
        DW 12, 12, 4, 0, 0, 0, 0, 0, 0, 2, 2, 1, 1, 1, 1, 1, 1, 65535, 0, 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpFreqBandTblRssiLoss0xFFdBm[20][2]
_RpFreqBandTblRssiLoss0xFFdBm:
        DATA8
        DB 138, 7, 142, 7, 143, 0, 141, 1, 141, 59, 141, 59, 141, 59, 148, 59
        DB 148, 59, 148, 59, 148, 59, 152, 6, 152, 6, 152, 6, 152, 6, 151, 6
        DB 153, 58, 155, 26, 148, 59, 147, 58

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpFreqBandTblRssiLoss0x00dBm[20][2]
_RpFreqBandTblRssiLoss0x00dBm:
        DATA8
        DB 139, 8, 143, 8, 144, 1, 142, 2, 142, 60, 142, 60, 142, 60, 149, 60
        DB 149, 60, 149, 60, 149, 60, 153, 7, 153, 7, 153, 7, 153, 7, 152, 7
        DB 154, 59, 156, 27, 149, 60, 148, 59

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpFreqBandTblRssiLoss0x01dBm[20][2]
_RpFreqBandTblRssiLoss0x01dBm:
        DATA8
        DB 140, 9, 144, 9, 145, 2, 143, 3, 143, 61, 143, 61, 143, 61, 150, 61
        DB 150, 61, 150, 61, 150, 61, 154, 8, 154, 8, 154, 8, 154, 8, 153, 8
        DB 155, 60, 157, 28, 150, 61, 149, 60

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpFreqBandTblRssiLoss0x02dBm[20][2]
_RpFreqBandTblRssiLoss0x02dBm:
        DATA8
        DB 141, 10, 145, 10, 146, 3, 144, 4, 144, 62, 144, 62, 144, 62, 151, 62
        DB 151, 62, 151, 62, 151, 62, 155, 9, 155, 9, 155, 9, 155, 9, 154, 9
        DB 156, 61, 158, 29, 151, 62, 150, 61

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpFreqBandTblRssiLoss0x03dBm[20][2]
_RpFreqBandTblRssiLoss0x03dBm:
        DATA8
        DB 142, 11, 146, 11, 147, 4, 145, 5, 145, 63, 145, 63, 145, 63, 152, 63
        DB 152, 63, 152, 63, 152, 63, 156, 10, 156, 10, 156, 10, 156, 10, 155
        DB 10, 157, 62, 159, 30, 152, 63, 151, 62

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint8_t const RpFreqBandTblRssiLoss0x04dBm[20][2]
_RpFreqBandTblRssiLoss0x04dBm:
        DATA8
        DB 143, 12, 147, 12, 148, 5, 146, 6, 146, 0, 146, 0, 146, 0, 153, 0
        DB 153, 0, 153, 0, 153, 0, 157, 11, 157, 11, 157, 11, 157, 11, 156, 11
        DB 158, 63, 160, 31, 153, 0, 152, 63

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// uint16_t const RpTc0WasteTime[20]
_RpTc0WasteTime:
        DATA16
        DW 2, 4, 8, 8, 8, 8, 8, 16, 16, 16, 16, 24, 24, 24, 24, 32, 32, 48, 32
        DW 64

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// uint8_t const RpRampUpDownStableTotalByte[20]
_RpRampUpDownStableTotalByte:
        DATA8
        DB 1, 1, 1, 1, 1, 1, 1, 2, 2, 2, 2, 2, 2, 2, 2, 3, 3, 4, 3, 6

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId4[11][2]
_RpCurrentPageToChannelsSupprotedAtBandId4:
        DATA32
        DD 4294967295, 3, 131071, 0, 131071, 0, 4294705153, 4294967295, 31, 0
        DD 4294705153, 4294967295, 31, 0, 4294443008, 4294967295, 31, 0
        DD 4294705153, 4294967295, 31, 0
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId5[7][2]
_RpCurrentPageToChannelsSupprotedAtBandId5:
        DD 4294967295, 4294967295, 4294967295, 4294967295, 4294967295
        DD 4294967295, 127, 0, 4294967295, 4294967295, 4294967295, 7
        DD 4294967295, 131071
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId6[3][2]
_RpCurrentPageToChannelsSupprotedAtBandId6:
        DD 4294967295, 127, 524287, 0, 511, 0
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId7[8][2]
_RpCurrentPageToChannelsSupprotedAtBandId7:
        DD 4294967295, 4261421055, 4294967295, 4294967295, 1, 0, 4030726143
        DD 4294967295, 4028628991, 4294967295, 4294967295, 4261421055
        DD 4294967295, 4294967295, 1, 0
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId8[4][2]
_RpCurrentPageToChannelsSupprotedAtBandId8:
        DD 4294967295, 0, 65535, 0, 65535, 0, 4294967295, 0
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId9[6][2]
_RpCurrentPageToChannelsSupprotedAtBandId9:
        DD 4294967295, 63, 4294967039, 31, 4294966911, 15, 4294966911, 15
        DD 4294967039, 31, 4294967295, 63
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId14[5][2]
_RpCurrentPageToChannelsSupprotedAtBandId14:
        DD 4294967295, 15, 4294967295, 4261421055, 4294967295, 4294967295, 1, 0
        DD 4293926911, 1023
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId15[3][2]
_RpCurrentPageToChannelsSupprotedAtBandId15:
        DD 4294967295, 134217727, 4294967295, 134217727, 4294967295, 134217727
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId16[3][2]
_RpCurrentPageToChannelsSupprotedAtBandId16:
        DD 4294967295, 4294967295, 4294967295, 4294967295, 1, 0
// uint32_t const RpCurrentPageToChannelsSupprotedAtBandId17[4][2]
_RpCurrentPageToChannelsSupprotedAtBandId17:
        DD 262143, 0, 262143, 0, 262143, 0, 262143, 0
//   32 //
//   33 #if defined(__arm)
//   34 	#include "cpx3.h"
//   35 	#include "phy_drv.h"
//   36 #endif
//   37 
//   38 /***************************************************************************************************************
//   39  * extern definitions
//   40  **************************************************************************************************************/
//   41 extern const RP_CONFIG_CB RpConfig;
//   42 
//   43 /***************************************************************************************************************
//   44  * private function prototypes
//   45  **************************************************************************************************************/
//   46 static uint32_t RpCsmaBytesToTime( uint16_t txTotalLength );
//   47 static void RpSetAdrfRegConverion( uint8_t convert );
//   48 static int16_t RpCurStatCheck( uint16_t status );
//   49 static void RpChkTmoutTrxStateOff( void );
//   50 static int16_t RpSetStateRxOn( uint8_t options, uint32_t time );
//   51 static uint8_t RpChkRxTriggerTimer( uint32_t time );
//   52 static void RpClrAutoRxFunc( void );
//   53 static int16_t RpAvailableRcvOnCsmaca( void );
//   54 static void RpInitRfic( void );
//   55 static void RpWrEvaReg1( uint16_t aData );
//   56 static uint8_t RpSetChannelCommonVal( uint8_t channel, uint8_t freqBandId, const uint32_t *pTblPtr );
//   57 static void RpSetCcaDurationTimeUpdate( uint8_t channel );
//   58 static void RpSetFreqBandVal( uint8_t afterReset );
//   59 static uint8_t RpCmpRefVthVal( uint16_t vthVal );
//   60 static void RpSetRssiOffsetVal( void );
//   61 static void RpSetMiscellaneousIniVal( void );
//   62 static void RpInitVar( uint8_t refreshFlg );
//   63 static void RpInitRfOnly( void );
//   64 static void RpSetRegBeforeIdle( void );
//   65 static void RpSetFreqAddReg( uint32_t freq, uint8_t freqBandId );
//   66 #ifndef RP_ST_ENV
//   67 static void RpSetRfInt( uint8_t enable );
//   68 #endif // #ifndef RP_ST_ENV
//   69 #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
//   70 static void RpWait4us( void );
//   71 #else
//   72 void RpWait4us( void );
//   73 #endif /* !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV) */
//   74 static void RpInitRxBuf( void );
//   75 static int16_t RpExtChkOptErrDataReq( uint8_t options );
//   76 static int16_t RpExtChkOptErrRxStateReq( uint8_t options );
//   77 static void RpProgressSamplingTxTime( uint8_t progressSample );
//   78 static void RpCalcTotalTxTime( void );
//   79 static uint32_t RpBytesToTime( uint16_t psduLength, uint8_t useRxFcsLength );
//   80 static void RpRegCcaBandwidth200k( void );
//   81 static void RpRegCcaBandwidth150k( void );
//   82 static void RpRegAdcVgaModify( void );
//   83 static void RpRegTxRxDataRate100kbps( void );
//   84 static void RpRegRxDataRate100kbps( void );
//   85 
//   86 /***************************************************************************************************************
//   87  * private variables
//   88  **************************************************************************************************************/
//   89 RP_PHY_CB		RpCb;
//   90 RP_PHY_ERROR	RpRfStat;

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
//   91 const uint8_t	RpStackVersion[] = "V308_10719";
_RpStackVersion:
        DB "V308_10719"
        DATA8
        DB 0

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   92 RP_RX_BUF		RpRxBuf[RP_RX_BUF_NUM];
_RpRxBuf:
        DS 3202
_RpCb:
        DS 386

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
_RpRfStat:
        DS 12

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   93 uint32_t		RpTxTime[RP_MAX_NUM_TX_LMT_BUF];
_RpTxTime:
        DS 244
//   94 
//   95 #if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)
//   96 	uint32_t RpLogRam[RP_LOG_NUMBER];
//   97 	uint16_t RpLogRamCnt;
//   98 #endif // #if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)
//   99 
//  100 /***************************************************************************************************************
//  101  * program
//  102  **************************************************************************************************************/
//  103 /***************************************************************************************************************
//  104  * function name  : RpInit
//  105  * description    : RF Driver Initialization
//  106  * parameters     : pDataIndCallback...Pointer to the RpPdDataIndCallback function
//  107  * 				  : pDataCfmCallback...Pointer to the RpPdDataCfmCallback function
//  108  * 				  : pCcaCfmCallback...Pointer to the RpPlmeCcaCfmCallback function
//  109  * 				  : pEdCfmCallback...Pointer to the RpPlmeEdCfmCallback function
//  110  * 				  : pRxOffIndCallback...Pointer to the RpRxOffIndCallback function
//  111  * 				  : pFatalErrorIndCallback...Pointer to the RpFatalErrorIndCallback function
//  112  * 				  : pWarningIndCallback...Pointer to the RpWarningIndCallback function
//  113  * 				  : pCalcLqiCallback...Pointer to the RpCalcLqiCallback function
//  114  * 				  : pAckCheckCallback...Pointer to the RpAckCheckCallback function
//  115  * return value   : RP_SUCCESS, RP_INVALID_PARAMTER
//  116  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _RpInit
        CODE
//  117 int16_t RpInit(RpPdDataIndCallbackT pDataIndCallback, RpPdDataCfmCallbackT pDataCfmCallback,
//  118 	   			RpPlmeCcaCfmCallbackT pCcaCfmCallback, RpPlmeEdCfmCallbackT pEdCfmCallback,
//  119 	   			RpRxOffIndCallbackT pRxOffIndCallback, RpFatalErrorIndCallbackT pFatalErrorIndCallback,
//  120 	   			RpWarningIndCallbackT pWarningIndCallback, RpCalcLqiCallbackT pCalcLqiCallback,
//  121 	   			RpAckCheckCallbackT pAckCheckCallback)
//  122 {
_RpInit:
        ; * Stack frame (at entry) *
        ; Param size: 28
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 14
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+18
//  123 	#if defined(__RX)
//  124 	uint32_t bkupPsw;
//  125 #elif defined(__CCRL__) || defined(__ICCRL78__)
//  126 	uint8_t  bkupPsw;
//  127 	#elif defined(__arm)
//  128 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
//  129 	#endif
//  130 
//  131 	/* Disable interrupt */
//  132 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  133 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  134 	#else
//  135 	RP_PHY_DI();
//  136 	#endif
//  137 
//  138 	/* Set the initial value for the log element counter */
//  139 	#if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)
//  140 	RpLogRamCnt = 0;
//  141 	#endif // #if defined(RP_LOG_IRQ) || defined(RP_LOG_EVENT)
//  142 
//  143 	/* API function execution Log */
//  144 	RpLog_Event( RP_LOG_API_INIT, RP_NULL );
//  145 
//  146 	if ((pDataIndCallback == RP_NULL) || (pDataCfmCallback == RP_NULL) || (pCcaCfmCallback  == RP_NULL)
//  147 			|| (pEdCfmCallback  == RP_NULL) || (pRxOffIndCallback == RP_NULL)
//  148 			|| (pFatalErrorIndCallback == RP_NULL) || (pWarningIndCallback == RP_NULL) || (pCalcLqiCallback == RP_NULL))
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_0    ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_0:
        BZ        ??RpLog_Event_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_2    ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_2:
        BZ        ??RpLog_Event_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_3    ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_3:
        BZ        ??RpLog_Event_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_4    ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_4:
        BZ        ??RpLog_Event_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_5    ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_5:
        BZ        ??RpLog_Event_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x20]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_6    ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_6:
        BZ        ??RpLog_Event_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x24]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_7    ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_7:
        BZ        ??RpLog_Event_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x28]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_8    ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_8:
        BNZ       ??RpLog_Event_9    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  149 	{
//  150 		/* API function execution Log */
//  151 		RpLog_Event( RP_LOG_API_INIT | RP_LOG_API_RETERR, RP_INVALID_PARAMETER );
//  152 
//  153 		/* Enable interrupt */
//  154 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  155 		RP_PHY_EI(bkupPsw);
??RpLog_Event_1:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  156 		#else
//  157 		RP_PHY_EI();
//  158 		#endif
//  159 
//  160 		return (RP_INVALID_PARAMETER);
        MOVW      AX, #0x5           ;; 1 cycle
        BR        R:??RpLog_Event_10  ;; 3 cycles
          CFI FunCall _RpMcuPeripheralInit
        ; ------------------------------------- Block: 8 cycles
//  161 	}
//  162 
//  163 	// initialize MCU Peripheral
//  164 	RpMcuPeripheralInit();			// including Rp_Powerdown_Sequence
??RpLog_Event_9:
        CALL      F:_RpMcuPeripheralInit  ;; 3 cycles
//  165 
//  166 	// RF IC Interrupt Disable
//  167 	RpSetMcuInt(RP_FALSE);			// RF interrupts request clear and disable
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetMcuInt
        CALL      F:_RpSetMcuInt     ;; 3 cycles
//  168 
//  169 	// initialize PHY
//  170 	RpInitVar(RP_FALSE);
        MOVW      AX, #0x182         ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOVW      AX, #0xF4          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpTxTime)  ;; 1 cycle
        MOV       A, #BYTE3(_RpTxTime)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3384)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3400)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3437   ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_RpRxBuf+3462, AX  ;; 2 cycles
        MOVW      AX, #0x3E8         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3464, AX  ;; 2 cycles
        MOVW      AX, #0x3C          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3466, AX  ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3468)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       ES:_RpRxBuf+3354, #0x9  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3364   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3356)  ;; 1 cycle
        MOVW      AX, #0xFEFF        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, #0xFF           ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3360)  ;; 1 cycle
        MOVW      AX, #0x1F          ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       A, ES:_RpConfig    ;; 2 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3355, A  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3378   ;; 2 cycles
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      ES:_RpRxBuf+3382, AX  ;; 2 cycles
        MOVW      ES:_RpRxBuf+3380, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3392   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3393   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3394   ;; 2 cycles
        MOVW      ES:_RpRxBuf+3398, AX  ;; 2 cycles
        MOVW      ES:_RpRxBuf+3396, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3408   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3409   ;; 2 cycles
        MOV       ES:_RpRxBuf+3410, #0x4  ;; 2 cycles
        MOV       ES:_RpRxBuf+3411, #0x3  ;; 2 cycles
        MOV       ES:_RpRxBuf+3412, #0x5  ;; 2 cycles
        MOV       ES:_RpRxBuf+3413, #0x3  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3374   ;; 2 cycles
        MOV       ES:_RpRxBuf+3375, #0x55  ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOVW      AX, ES:_RpConfig+2  ;; 2 cycles
        AND       A, #0x1            ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3368, AX  ;; 2 cycles
        MOVW      AX, #0x24          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3414, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3365   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3366   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3367   ;; 2 cycles
        MOV       X, #0xF            ;; 1 cycle
        MOVW      ES:_RpRxBuf+3416, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3418   ;; 2 cycles
        ONEB      ES:_RpRxBuf+3419   ;; 2 cycles
        MOV       ES:_RpRxBuf+3420, #0x2  ;; 2 cycles
        MOV       ES:_RpRxBuf+3421, #0x2  ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       A, ES:_RpConfig+4  ;; 2 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3426, A  ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       A, ES:_RpConfig+5  ;; 2 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3427, A  ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       A, ES:_RpConfig+6  ;; 2 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3428, A  ;; 2 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+18
        CMP       ES:_RpRxBuf+3428, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_11   ;; 4 cycles
        ; ------------------------------------- Block: 171 cycles
        CLRB      ES:_RpRxBuf+3429   ;; 2 cycles
        BR        S:??RpLog_Event_12  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_11:
        MOV       ES:_RpRxBuf+3429, #0xFF  ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_12:
        CLRB      ES:_RpRxBuf+3430   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3431   ;; 2 cycles
        MOVW      AX, #0x3C          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3422, AX  ;; 2 cycles
        MOVW      AX, #0x1F4         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3424, AX  ;; 2 cycles
        ONEB      ES:_RpRxBuf+3432   ;; 2 cycles
        MOV       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        MOV       X, #0x7E           ;; 1 cycle
        MOVW      ES:_RpRxBuf+3370, AX  ;; 2 cycles
        MOVW      ES:_RpRxBuf+3372, AX  ;; 2 cycles
        MOVW      AX, #0x25          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3442, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3444   ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_RpRxBuf+3446, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3448   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3449   ;; 2 cycles
        MOVW      ES:_RpRxBuf+3450, AX  ;; 2 cycles
        MOVW      ES:_RpRxBuf+3452, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3454   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3455   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3456   ;; 2 cycles
        MOVW      AX, #0x12C         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3458, AX  ;; 2 cycles
        MOV       ES:_RpRxBuf+3460, #0xFF  ;; 2 cycles
        MOV       ES:_RpRxBuf+3461, #0xFF  ;; 2 cycles
        MOVW      AX, #0xC           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpRfStat)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRfStat)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOVW      AX, #0x10          ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3204   ;; 2 cycles
          CFI FunCall _RpSetAttr_phyAckWithCca
        CALL      F:_RpSetAttr_phyAckWithCca  ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3484   ;; 2 cycles
        MOVW      AX, #0x122         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3352, AX  ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3438)  ;; 1 cycle
        MOVW      AX, #0x3BA0        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, #0x36FC        ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
          CFI FunCall _RpSetAttr_phyFrequencyOffset
        CALL      F:_RpSetAttr_phyFrequencyOffset  ;; 3 cycles
//  171 
//  172 	// register callbacks
//  173 	RpCb.callback.pDataIndCallback  = pDataIndCallback;
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3490)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  174 	RpCb.callback.pDataCfmCallback  = pDataCfmCallback;
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3494)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  175 	RpCb.callback.pCcaCfmCallback   = pCcaCfmCallback;
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3498)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  176 	RpCb.callback.pEdCfmCallback    = pEdCfmCallback;
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3502)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  177 	RpCb.callback.pRxOffIndCallback = pRxOffIndCallback;
        MOV       A, [SP+0x1E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3506)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  178 	RpCb.callback.pFatalErrorIndCallback = pFatalErrorIndCallback;
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3510)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  179 	RpCb.callback.pWarningIndCallback = pWarningIndCallback;
        MOV       A, [SP+0x26]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3514)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  180 	RpCb.callback.pCalcLqiCallback = pCalcLqiCallback;
        MOV       A, [SP+0x2A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x28]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3518)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  181 	if (pAckCheckCallback == RP_NULL)
        MOV       A, [SP+0x2E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_13   ;; 4 cycles
        ; ------------------------------------- Block: 190 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_13:
        BNZ       ??RpLog_Event_14   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  182 	{
//  183 		RpCb.callback.pAckCheckCallback = RpAckCheckCallback;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3524, #BYTE3(_RpAckCheckCallback)  ;; 2 cycles
        MOVW      AX, #LWRD(_RpAckCheckCallback)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3522, AX  ;; 2 cycles
        BR        S:??RpLog_Event_15  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  184 	}
//  185 	else
//  186 	{
//  187 		RpCb.callback.pAckCheckCallback = pAckCheckCallback;
??RpLog_Event_14:
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3522)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
          CFI FunCall _RpWakeupSequence
        ; ------------------------------------- Block: 8 cycles
//  188 	}
//  189 
//  190 	// initialize RF IC
//  191 	RpInitRfic();
??RpLog_Event_15:
        CALL      F:_RpWakeupSequence  ;; 3 cycles
          CFI FunCall _RpSetRegBeforeIdle
        CALL      F:_RpSetRegBeforeIdle  ;; 3 cycles
          CFI FunCall _RpInitRfOnly
        CALL      F:_RpInitRfOnly    ;; 3 cycles
//  192 	RpSetMcuInt(RP_TRUE);			// RF interrupts enable
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetMcuInt
        CALL      F:_RpSetMcuInt     ;; 3 cycles
//  193 	RpSetRfInt(RP_TRUE);
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x1B0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3546   ;; 2 cycles
        MOV       ES:_RpRxBuf+3547, #0xEF  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3548   ;; 2 cycles
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      DE, #LWRD(_RpRxBuf+3546)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
//  194 
//  195 	// initialize Rx buffer
//  196 	RpInitRxBuf();
          CFI FunCall _RpInitRxBuf
        CALL      F:_RpInitRxBuf     ;; 3 cycles
//  197 
//  198 	/* API function execution Log */
//  199 	RpLog_Event( RP_LOG_API_INIT | RP_LOG_API_RET, RP_SUCCESS );
//  200 
//  201 	/* Enable interrupt */
//  202 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  203 	RP_PHY_EI(bkupPsw);
        MOV       A, [SP+0x04]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  204 	#else
//  205 	RP_PHY_EI();
//  206 	#endif
//  207 
//  208 	return (RP_SUCCESS);
        MOVW      AX, #0x7           ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+18
        ; ------------------------------------- Block: 52 cycles
??RpLog_Event_10:
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 601 cycles
//  209 }
//  210 
//  211 /***************************************************************************************************************
//  212  * function name  : RpResetReq
//  213  * description    : RF driver reset
//  214  * parameters     : none
//  215  * return value   : none
//  216  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon1
          CFI Function _RpResetReq
        CODE
//  217 void RpResetReq( void )
//  218 {
_RpResetReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 52
        SUBW      SP, #0x34          ;; 1 cycle
          CFI CFA SP+56
//  219 	#if defined(__RX)
//  220 	uint32_t bkupPsw;
//  221 	#elif defined(__CCRL__) || defined(__ICCRL78__)
//  222 	uint8_t  bkupPsw;
//  223 	#elif defined(__arm)
//  224 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
//  225 	#endif
//  226 
//  227 	/* Disable interrupt */
//  228 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  229 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  230 	#else
//  231 	RP_PHY_DI();
//  232 	#endif
//  233 
//  234 	/* API function execution Log */
//  235 	RpLog_Event( RP_LOG_API_RESET, RP_NULL );
//  236 
//  237 	// Pervious Timer value Re-Setting
//  238 	RpPrevSentTimeReSetting();
          CFI FunCall _RpPrevSentTimeReSetting
        CALL      F:_RpPrevSentTimeReSetting  ;; 3 cycles
//  239 
//  240 	// initialize MCU Peripheral
//  241 	RpMcuPeripheralInit();			// including Rp_Powerdown_Sequence
          CFI FunCall _RpMcuPeripheralInit
        CALL      F:_RpMcuPeripheralInit  ;; 3 cycles
//  242 
//  243 	// RF IC Interrupt Disable
//  244 	RpSetMcuInt(RP_FALSE);			// RF interrupts request clear and disable
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetMcuInt
        CALL      F:_RpSetMcuInt     ;; 3 cycles
//  245 
//  246 	// initialize PHY
//  247 	RpInitVar(RP_TRUE);
        MOVW      DE, #LWRD(_RpRxBuf+3384)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOV       ES, #0xF           ;; 1 cycle
        MOVW      BC, #0x8           ;; 1 cycle
          CFI FunCall ?MEMCPY_FAR
        CALL      N:?MEMCPY_FAR      ;; 3 cycles
        MOVW      DE, #LWRD(_RpRxBuf+3400)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOV       ES, #0xF           ;; 1 cycle
        MOVW      BC, #0x8           ;; 1 cycle
          CFI FunCall ?MEMCPY_FAR
        CALL      N:?MEMCPY_FAR      ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3437  ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3462  ;; 2 cycles
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3464  ;; 2 cycles
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3466  ;; 2 cycles
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3468)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x20], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x22], AX      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x1C], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x1E], AX      ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3282  ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3284)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x18], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x1A], AX      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3292)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x14], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x16], AX      ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3290  ;; 2 cycles
        MOV       [SP+0x03], A       ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3296)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x10], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x12], AX      ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3304  ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3306  ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3307  ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        MOVW      AX, #0x66          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3206)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOVW      AX, #0x2E          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3308)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOVW      AX, #0x88          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3354)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3544   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3545   ;; 2 cycles
        MOVW      AX, #0x1A          ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+64
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3546)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3384)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      BC, #0x8           ;; 1 cycle
          CFI FunCall ?MEMCPY_FAR
        CALL      N:?MEMCPY_FAR      ;; 3 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3400)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      BC, #0x8           ;; 1 cycle
          CFI FunCall ?MEMCPY_FAR
        CALL      N:?MEMCPY_FAR      ;; 3 cycles
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3437, A  ;; 2 cycles
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      ES:_RpRxBuf+3462, AX  ;; 2 cycles
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      ES:_RpRxBuf+3464, AX  ;; 2 cycles
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      ES:_RpRxBuf+3466, AX  ;; 2 cycles
        MOVW      AX, [SP+0x2A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x28]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3468)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      ES:_RpRxBuf+3282, AX  ;; 2 cycles
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3284)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3292)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       A, [SP+0x0B]       ;; 1 cycle
        MOV       ES:_RpRxBuf+3290, A  ;; 2 cycles
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3296)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      ES:_RpRxBuf+3304, AX  ;; 2 cycles
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES:_RpRxBuf+3306, A  ;; 2 cycles
        MOV       A, [SP+0x09]       ;; 1 cycle
        MOV       ES:_RpRxBuf+3307, A  ;; 2 cycles
        MOV       ES:_RpRxBuf+3354, #0x9  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3364   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3356)  ;; 1 cycle
        MOVW      AX, #0xFEFF        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, #0xFF           ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3360)  ;; 1 cycle
        MOVW      AX, #0x1F          ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       A, ES:_RpConfig    ;; 2 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3355, A  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3378   ;; 2 cycles
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      ES:_RpRxBuf+3382, AX  ;; 2 cycles
        MOVW      ES:_RpRxBuf+3380, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3392   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3393   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3394   ;; 2 cycles
        MOVW      ES:_RpRxBuf+3398, AX  ;; 2 cycles
        MOVW      ES:_RpRxBuf+3396, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3408   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3409   ;; 2 cycles
        MOV       ES:_RpRxBuf+3410, #0x4  ;; 2 cycles
        MOV       ES:_RpRxBuf+3411, #0x3  ;; 2 cycles
        MOV       ES:_RpRxBuf+3412, #0x5  ;; 2 cycles
        MOV       ES:_RpRxBuf+3413, #0x3  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3374   ;; 2 cycles
        MOV       ES:_RpRxBuf+3375, #0x55  ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOVW      AX, ES:_RpConfig+2  ;; 2 cycles
        AND       A, #0x1            ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3368, AX  ;; 2 cycles
        MOVW      AX, #0x24          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3414, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3365   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3366   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3367   ;; 2 cycles
        MOV       X, #0xF            ;; 1 cycle
        MOVW      ES:_RpRxBuf+3416, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3418   ;; 2 cycles
        ONEB      ES:_RpRxBuf+3419   ;; 2 cycles
        MOV       ES:_RpRxBuf+3420, #0x2  ;; 2 cycles
        MOV       ES:_RpRxBuf+3421, #0x2  ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       A, ES:_RpConfig+4  ;; 2 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3426, A  ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       A, ES:_RpConfig+5  ;; 2 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3427, A  ;; 2 cycles
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       A, ES:_RpConfig+6  ;; 2 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3428, A  ;; 2 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+56
        CMP       ES:_RpRxBuf+3428, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_16   ;; 4 cycles
        ; ------------------------------------- Block: 343 cycles
        CLRB      ES:_RpRxBuf+3429   ;; 2 cycles
        BR        S:??RpLog_Event_17  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_16:
        MOV       ES:_RpRxBuf+3429, #0xFF  ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_17:
        CLRB      ES:_RpRxBuf+3430   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3431   ;; 2 cycles
        MOVW      AX, #0x3C          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3422, AX  ;; 2 cycles
        MOVW      AX, #0x1F4         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3424, AX  ;; 2 cycles
        ONEB      ES:_RpRxBuf+3432   ;; 2 cycles
        MOV       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        MOV       X, #0x7E           ;; 1 cycle
        MOVW      ES:_RpRxBuf+3370, AX  ;; 2 cycles
        MOVW      ES:_RpRxBuf+3372, AX  ;; 2 cycles
        MOVW      AX, #0x25          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3442, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3444   ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_RpRxBuf+3446, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3448   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3449   ;; 2 cycles
        MOVW      ES:_RpRxBuf+3450, AX  ;; 2 cycles
        MOVW      ES:_RpRxBuf+3452, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3454   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3455   ;; 2 cycles
        CLRB      ES:_RpRxBuf+3456   ;; 2 cycles
        MOVW      AX, #0x12C         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3458, AX  ;; 2 cycles
        MOV       ES:_RpRxBuf+3460, #0xFF  ;; 2 cycles
        MOV       ES:_RpRxBuf+3461, #0xFF  ;; 2 cycles
        MOVW      AX, #0xC           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpRfStat)  ;; 1 cycle
        MOV       A, #BYTE3(_RpRfStat)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        MOVW      AX, #0x10          ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3204   ;; 2 cycles
          CFI FunCall _RpSetAttr_phyAckWithCca
        CALL      F:_RpSetAttr_phyAckWithCca  ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3484   ;; 2 cycles
        MOVW      AX, #0x122         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3352, AX  ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3438)  ;; 1 cycle
        MOVW      AX, #0x3BA0        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, #0x36FC        ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
          CFI FunCall _RpSetAttr_phyFrequencyOffset
        CALL      F:_RpSetAttr_phyFrequencyOffset  ;; 3 cycles
//  248 
//  249 	// initialize RF IC
//  250 	RpInitRfic();
          CFI FunCall _RpWakeupSequence
        CALL      F:_RpWakeupSequence  ;; 3 cycles
          CFI FunCall _RpSetRegBeforeIdle
        CALL      F:_RpSetRegBeforeIdle  ;; 3 cycles
          CFI FunCall _RpInitRfOnly
        CALL      F:_RpInitRfOnly    ;; 3 cycles
//  251 	RpSetMcuInt(RP_TRUE);			// RF interrupts enable
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetMcuInt
        CALL      F:_RpSetMcuInt     ;; 3 cycles
//  252 	RpSetRfInt(RP_TRUE);
        POP       AX                 ;; 1 cycle
          CFI CFA SP+56
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x1B0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3546   ;; 2 cycles
        MOV       ES:_RpRxBuf+3547, #0xEF  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3548   ;; 2 cycles
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      DE, #LWRD(_RpRxBuf+3546)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
//  253 
//  254 	// initialize Rx buffer
//  255 	RpInitRxBuf();
          CFI FunCall _RpInitRxBuf
        CALL      F:_RpInitRxBuf     ;; 3 cycles
//  256 
//  257 	/* API function execution Log */
//  258 	RpLog_Event( RP_LOG_API_RESET | RP_LOG_API_RET, RP_NULL );
//  259 
//  260 	/* Enable interrupt */
//  261 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  262 	RP_PHY_EI(bkupPsw);
        MOV       A, [SP+0x04]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  263 	#else
//  264 	RP_PHY_EI();
//  265 	#endif
//  266 }
        ADDW      SP, #0x38          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 141 cycles
        ; ------------------------------------- Total: 491 cycles
//  267 
//  268 /***************************************************************************************************************
//  269  * function name  : RpSetRxOffReq
//  270  * description    : Idle Setting Request
//  271  * parameters     : none
//  272  * return value   : RP_SUCESS, RP_BUSY_LOWPOWER
//  273  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _RpSetRxOffReq
        CODE
//  274 int16_t RpSetRxOffReq( void )
//  275 {
_RpSetRxOffReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
//  276 	uint16_t	curStat;
//  277 	int16_t	rtnVal = RP_SUCCESS;
        MOVW      AX, #0x7           ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  278 
//  279 	#if defined(__RX)
//  280 	uint32_t bkupPsw;
//  281 	#elif defined(__CCRL__) || defined(__ICCRL78__)
//  282 	uint8_t  bkupPsw;
//  283 	#elif defined(__arm)
//  284 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
//  285 	#endif
//  286 
//  287 	/* Disable interrupt */
//  288 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  289 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  290 	#else
//  291 	RP_PHY_DI();
//  292 	#endif
//  293 
//  294 	/* API function execution Log */
//  295 	RpLog_Event( RP_LOG_API_RXOFF, RP_NULL );
//  296 
//  297 	// copy current status
//  298 	curStat = RpCb.status;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  299 
//  300 	if (curStat & RP_PHY_STAT_LOWPOWER)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].5, ??RpLog_Event_18  ;; 5 cycles
        ; ------------------------------------- Block: 24 cycles
//  301 	{
//  302 		rtnVal = RP_BUSY_LOWPOWER;
        MOVW      AX, #0xF3          ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        BR        S:??RpLog_Event_19  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  303 	}
//  304 	else if (curStat & RP_PHY_STAT_TRX_OFF)
??RpLog_Event_18:
        DECW      HL                 ;; 1 cycle
        BT        [HL].4, ??RpLog_Event_19  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  305 	{
//  306 		;	// rtnVal = RP_SUCCESS;
//  307 	}
//  308 	else
//  309 	{
//  310 		if (curStat & RP_PHY_STAT_RX)
        BF        [HL].0, ??RpLog_Event_20  ;; 5 cycles
          CFI FunCall _RpRxOffBeforeReplyingAck
        ; ------------------------------------- Block: 5 cycles
//  311 		{
//  312 			if (RpRxOffBeforeReplyingAck() == RP_TRUE)
        CALL      F:_RpRxOffBeforeReplyingAck  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_21   ;; 4 cycles
          CFI FunCall _RpChkTmoutTrxStateOff
        ; ------------------------------------- Block: 8 cycles
//  313 			{
//  314 				RpSetStateRxOnToTrxOff();
//  315 			}
//  316 			else
//  317 			{
//  318 				//TX(DATA,ACK),CCA,ED
//  319 				RpChkTmoutTrxStateOff();
//  320 			}
//  321 		}
//  322 		else if ((curStat & RP_PHY_STAT_TX) && (RP_PHY_STAT_TX_BUSY() == 0))
//  323 		{
//  324 			RpSetStateRxOnToTrxOff();
//  325 		}
//  326 		else
//  327 		{
//  328 			//TX(DATA,ACK),CCA,ED
//  329 			RpChkTmoutTrxStateOff();
??RpSetRxOffReq_0:
        CALL      F:_RpChkTmoutTrxStateOff  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  330 		}
//  331 	}
//  332 
//  333 	/* API function execution Log */
//  334 	RpLog_Event( RP_LOG_API_RXOFF | RP_LOG_API_RET, (uint8_t)rtnVal );
//  335 
//  336 	/* Enable interrupt */
//  337 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  338 	RP_PHY_EI(bkupPsw);
??RpLog_Event_19:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  339 	#else
//  340 	RP_PHY_EI();
//  341 	#endif
//  342 
//  343 	return (rtnVal);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+10
        ; ------------------------------------- Block: 12 cycles
??RpLog_Event_20:
        BF        [HL].1, ??RpSetRxOffReq_0  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, #0x330         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].1, ??RpSetRxOffReq_0  ;; 5 cycles
          CFI FunCall _RpSetStateRxOnToTrxOff
        ; ------------------------------------- Block: 13 cycles
??RpLog_Event_21:
        CALL      F:_RpSetStateRxOnToTrxOff  ;; 3 cycles
        BR        S:??RpLog_Event_19  ;; 3 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 87 cycles
//  344 }
//  345 
//  346 /******************************************************************************
//  347 Function Name:       RpRxOffBeforeReplyingAck
//  348 Parameters:          none
//  349 Return value:        RP_TRUE:successful of Rxoff before Replying Ack
//  350 				  RP_FALSE:could not Rxoff cause on Replying Ack
//  351 Description:         Rx to TRX_OFF before Replying Ack
//  352 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon1
          CFI Function _RpRxOffBeforeReplyingAck
        CODE
//  353 uint8_t RpRxOffBeforeReplyingAck( void )
//  354 {
_RpRxOffBeforeReplyingAck:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
//  355 	uint8_t stillRxOff = RP_FALSE;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
//  356 
//  357 	#if defined(__RX)
//  358 	uint32_t bkupPsw;
//  359 	#elif defined(__CCRL__) || defined(__ICCRL78__)
//  360 	uint8_t  bkupPsw;
//  361 	#elif defined(__arm)
//  362 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
//  363 	#endif
//  364 
//  365 	/* Disable interrupt */
//  366 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  367 	RP_PHY_ALL_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
//  368 	#else
//  369 	RP_PHY_ALL_DI();
//  370 	#endif
//  371 
//  372 	if (RP_PHY_STAT_ONACKREPLY() == 0)
        MOVW      AX, #0x330         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].1, ??RpLog_Event_22  ;; 5 cycles
        ; ------------------------------------- Block: 32 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3344   ;; 2 cycles
        BNZ       ??RpLog_Event_22   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  373 	{
//  374 		RpRegWrite(BBTXRXRST, RFSTOP);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x8           ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  375 		stillRxOff = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
//  376 	}
//  377 
//  378 	/* Enable interrupt */
//  379 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  380 	RP_PHY_ALL_EI(bkupPsw);
??RpLog_Event_22:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  381 	#else
//  382 	RP_PHY_ALL_EI();
//  383 	#endif
//  384 
//  385 	return (stillRxOff);
        MOV       A, [SP+0x01]       ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 58 cycles
//  386 }
//  387 
//  388 /***************************************************************************************************************
//  389  * function name  : RpSetRxOnReq
//  390  * description    : Reception ON Setting Request
//  391  * parameters     : options...Options
//  392  * 			      : time...Start time [Unit:Symbol]
//  393  * return value   : RP_SUCESS, RP_INVALID_PARAMETER, RP_RX_ON, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_LOWPOWER
//  394  *				  : RP_NOT_GET_RXBUF, RP_INVALID_SET_TIME
//  395  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon1
          CFI Function _RpSetRxOnReq
        CODE
//  396 int16_t RpSetRxOnReq( uint8_t options, uint32_t time )
//  397 {
_RpSetRxOnReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
//  398 	uint16_t curStat;
//  399 	int16_t	rtnVal = RP_SUCCESS;
//  400 
//  401 	#if defined(__RX)
//  402 	uint32_t bkupPsw;
//  403 	#elif defined(__CCRL__) || defined(__ICCRL78__)
//  404 	uint8_t  bkupPsw;
//  405 	#elif defined(__arm)
//  406 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
//  407 	#endif
//  408 
//  409 	/* Disable interrupt */
//  410 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  411 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  412 	#else
//  413 	RP_PHY_DI();
//  414 	#endif
//  415 
//  416 	/* API function execution Log */
//  417 	RpLog_Event( RP_LOG_API_RXON, options );
//  418 
//  419 	if (RpExtChkOptErrRxStateReq(options) == RP_TRUE)
        MOV       A, [SP+0x05]       ;; 1 cycle
        AND       A, #0x21           ;; 1 cycle
        CMP       A, #0x21           ;; 1 cycle
        BZ        ??RpLog_Event_23   ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
//  420 	{
//  421 		/* API function execution Log */
//  422 		RpLog_Event( RP_LOG_API_RXON | RP_LOG_API_RETERR, RP_INVALID_PARAMETER );
//  423 
//  424 		/* Enable interrupt */
//  425 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  426 		RP_PHY_EI(bkupPsw);
//  427 		#else
//  428 		RP_PHY_EI();
//  429 		#endif
//  430 
//  431 		return (RP_INVALID_PARAMETER);
//  432 	}
//  433 
//  434 	if ( RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE )
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3428, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_24   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  435 	{
//  436 		if ( RpCb.pib.phyDataRate == RP_PHY_ANTDV_DATARATE ) // 100kbps only
        MOVW      AX, ES:_RpRxBuf+3434  ;; 2 cycles
        CMPW      AX, #0x64          ;; 1 cycle
        BNZ       ??RpLog_Event_24   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  437 		{
//  438 			if (( RpCb.pib.phySfdDetectionExtend == RP_TRUE ) ||
//  439 				( RpCb.pib.phyPreamble4ByteRxMode == RP_FALSE ))
        CMP       ES:_RpRxBuf+3454, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_23   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP0      ES:_RpRxBuf+3444   ;; 2 cycles
        BNZ       ??RpLog_Event_24   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  440 			{
//  441 				/* API function execution Log */
//  442 				RpLog_Event( RP_LOG_API_RXON | RP_LOG_API_RETERR, RP_INVALID_PARAMETER );
//  443 
//  444 				/* Enable interrupt */
//  445 				#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  446 				RP_PHY_EI(bkupPsw);
//  447 				#else
//  448 				RP_PHY_EI();
//  449 				#endif
//  450 
//  451 				return (RP_INVALID_PARAMETER);
//  452 			}
//  453 		}
//  454 	}
??RpLog_Event_23:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        MOVW      AX, #0x5           ;; 1 cycle
        BR        S:??RpLog_Event_25  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
//  455 
//  456 	// copy current status
//  457 	curStat = RpCb.status;
??RpLog_Event_24:
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  458 	if (curStat & RP_PHY_STAT_LOWPOWER)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].5, ??RpLog_Event_26  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
//  459 	{
//  460 		rtnVal = RP_BUSY_LOWPOWER;
        MOVW      AX, #0xF3          ;; 1 cycle
        BR        S:??RpLog_Event_27  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  461 	}
//  462 	else if (curStat & RP_PHY_STAT_TRX_OFF)
??RpLog_Event_26:
        DECW      HL                 ;; 1 cycle
        BF        [HL].4, ??RpLog_Event_28  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
//  463 	{
//  464 		// TRX_OFF -> RX_ON
//  465 		rtnVal = RpSetStateRxOn(options, time);
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
          CFI FunCall _RpSetStateRxOn
        CALL      F:_RpSetStateRxOn  ;; 3 cycles
        BR        S:??RpLog_Event_27  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
//  466 	}
//  467 	else if (curStat & RP_PHY_STAT_TX)
??RpLog_Event_28:
        BF        [HL].1, ??RpLog_Event_29  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  468 	{
//  469 		rtnVal = RP_BUSY_TX;
        MOVW      AX, #0x2           ;; 1 cycle
        BR        S:??RpLog_Event_27  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  470 	}
//  471 	else if (curStat & RP_PHY_STAT_RX)
??RpLog_Event_29:
        BF        [HL].0, ??RpLog_Event_30  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  472 	{
//  473 		if (RP_PHY_STAT_RX_BUSY())
        MOVW      AX, #0x330         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].3, ??RpLog_Event_30  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, #0x330         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BT        [HL].1, ??RpLog_Event_30  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3344   ;; 2 cycles
        BNZ       ??RpLog_Event_30   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  474 		{
//  475 			rtnVal = RP_BUSY_RX;
//  476 		}
//  477 		else
//  478 		{
//  479 			rtnVal = RP_RX_ON;
        MOVW      AX, #0x6           ;; 1 cycle
        BR        S:??RpLog_Event_27  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  480 		}
//  481 	}
//  482 	else
//  483 	{
//  484 		// CCA or ED or RX
//  485 		rtnVal = RP_BUSY_RX;
??RpLog_Event_30:
        ONEW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_27:
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  486 	}
//  487 
//  488 	/* API function execution Log */
//  489 	RpLog_Event( RP_LOG_API_RXON | RP_LOG_API_RET, (uint8_t)rtnVal );
//  490 
//  491 	/* Enable interrupt */
//  492 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  493 	RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  494 	#else
//  495 	RP_PHY_EI();
//  496 	#endif
//  497 
//  498 	return (rtnVal);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
??RpLog_Event_25:
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 151 cycles
//  499 }
//  500 
//  501 /***************************************************************************************************************
//  502  * function name  : RpPdDataReq
//  503  * description    : Frame Transmission Request
//  504  * parameters     : pData...Pointer to the beginning of the area storing the transmission data.
//  505  *				  : len...Size of the transmission data (PSDU size minus FCS length)
//  506  *				  : options...Transmission options
//  507  * 			      : time...Transmission (frame transmission or CSMA-CA) start time [Unit:Symbol]
//  508  * return value   : RP_PENDING, RP_INVALID_PARAMETER, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_LOWPOWER,
//  509  *				  : RP_INVALID_API_OPTION
//  510  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon2
          CFI Function _RpPdDataReq
        CODE
//  511 int16_t RpPdDataReq( uint8_t *pData, uint16_t len, uint8_t options, uint32_t time )
//  512 {
_RpPdDataReq:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 22
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+26
//  513 	uint16_t status, txFlen;
//  514 	uint8_t minBe = 0;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x03], A       ;; 1 cycle
//  515 	uint8_t ccaDurPibValReq;
//  516 	uint32_t futureTime;
//  517 	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3448  ;; 2 cycles
        MOV       [SP+0x07], A       ;; 1 cycle
//  518 	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
        MOV       A, ES:_RpRxBuf+3449  ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
//  519 	uint8_t index = RpCb.freqIdIndex;
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
//  520 
//  521 	#if defined(__RX)
//  522 	uint32_t bkupPsw;
//  523 	uint32_t bkupPsw2;
//  524 	#elif defined(__CCRL__) || defined(__ICCRL78__)
//  525 	uint8_t  bkupPsw;
//  526 	uint8_t  bkupPsw2;
//  527 	#elif defined(__arm)
//  528 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
//  529 	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
//  530 	#endif
//  531 
//  532 	/* Disable interrupt */
//  533 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  534 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  535 	#else
//  536 	RP_PHY_DI();
//  537 	#endif
//  538 
//  539 	/* API function execution Log */
//  540 	RpLog_Event( RP_LOG_API_DATAREQ, options );
//  541 
//  542 	if (pData == RP_NULL)
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_31   ;; 4 cycles
        ; ------------------------------------- Block: 37 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_31:
        BZ        ??RpLog_Event_32   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  543 	{
//  544 		/* API function execution Log */
//  545 		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_LOG_API_RETERR_NODATA );
//  546 
//  547 		/* Enable interrupt */
//  548 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  549 		RP_PHY_EI(bkupPsw);
//  550 		#else
//  551 		RP_PHY_EI();
//  552 		#endif
//  553 
//  554 		return (RP_INVALID_PARAMETER);
//  555 	}
//  556 
//  557 	if (RpExtChkErrFrameLen(len, RP_FALSE) == RP_TRUE)
        CLRB      C                  ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
          CFI FunCall _RpExtChkErrFrameLen
        CALL      F:_RpExtChkErrFrameLen  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_32   ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
//  558 	{
//  559 		/* API function execution Log */
//  560 		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_LOG_API_RETERR_FRAMELEN );
//  561 
//  562 		/* Enable interrupt */
//  563 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  564 		RP_PHY_EI(bkupPsw);
//  565 		#else
//  566 		RP_PHY_EI();
//  567 		#endif
//  568 
//  569 		return (RP_INVALID_PARAMETER);
//  570 	}
//  571 
//  572 	if (RpExtChkOptErrDataReq(options) == RP_TRUE)
        MOV       A, [SP+0x0E]       ;; 1 cycle
        AND       A, #0x48           ;; 1 cycle
        CMP       A, #0x8            ;; 1 cycle
        BZ        ??RpLog_Event_32   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        AND       A, #0x60           ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        CMP       A, #0x20           ;; 1 cycle
        BZ        ??RpLog_Event_32   ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  573 	{
//  574 		/* API function execution Log */
//  575 		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_LOG_API_RETERR_DATAREQ );
//  576 
//  577 		/* Enable interrupt */
//  578 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  579 		RP_PHY_EI(bkupPsw);
//  580 		#else
//  581 		RP_PHY_EI();
//  582 		#endif
//  583 
//  584 		return (RP_INVALID_PARAMETER);
//  585 	}
//  586 
//  587 	if ((RpCb.pib.phyCcaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) ||
//  588 		(RpCb.pib.phyCcaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K))
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3448, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_33   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_RpRxBuf+3448, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_34   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  589 	{
//  590 		if (((options & RP_TX_CSMACA) && ((index != RP_PHY_100K_M10_JP) && (index != RP_PHY_100K_M10_IN)))
//  591 			|| (options & RP_TX_CSMACA_RX) || (options & RP_TX_ACK))
??RpLog_Event_33:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].6, ??RpLog_Event_35  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        CMP       A, #0x9            ;; 1 cycle
        BZ        ??RpLog_Event_35   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0xA            ;; 1 cycle
        BNZ       ??RpLog_Event_36   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_35:
        MOV       A, [SP+0x0E]       ;; 1 cycle
        AND       A, #0x30           ;; 1 cycle
        BZ        ??RpLog_Event_37   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  592 		{
//  593 			/* API function execution Log */
//  594 			RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_INVALID_API_OPTION );
//  595 
//  596 			/* Enable interrupt */
//  597 #if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  598 			RP_PHY_EI(bkupPsw);
??RpLog_Event_36:
        BR        R:??RpLog_Event_38  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  599 #else
//  600 			RP_PHY_EI();
//  601 #endif
//  602 			return (RP_INVALID_API_OPTION);
//  603 		}
//  604 	}
??RpLog_Event_32:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        MOVW      AX, #0x5           ;; 1 cycle
        BR        R:??RpLog_Event_39  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
//  605 	else if (RpCb.pib.phyCcaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW)
??RpLog_Event_34:
        CMP       ES:_RpRxBuf+3448, #0x3  ;; 2 cycles
        BNZ       ??RpLog_Event_37   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  606 	{
//  607 		if ((options & RP_TX_CSMACA_RX) || (options & RP_TX_ACK))
        MOV       A, [SP+0x0E]       ;; 1 cycle
        AND       A, #0x30           ;; 1 cycle
        BNZ       ??RpLog_Event_38   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  608 		{
//  609 			/* API function execution Log */
//  610 			RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_INVALID_API_OPTION );
//  611 	
//  612 			/* Enable interrupt */
//  613 #if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  614 			RP_PHY_EI(bkupPsw);
//  615 #else
//  616 			RP_PHY_EI();
//  617 #endif
//  618 			return (RP_INVALID_API_OPTION);
//  619 
//  620 		}
//  621 	}
//  622 
//  623 	status = RpCb.status;
??RpLog_Event_37:
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  624 
//  625 	if ((status & RP_PHY_STAT_TRX_OFF) == 0)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].4, ??RpLog_Event_40  ;; 5 cycles
        ; ------------------------------------- Block: 11 cycles
//  626 	{
//  627 		/* API function execution Log */
//  628 		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, (uint8_t)RpCurStatCheck(status) );
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _RpCurStatCheck
        CALL      F:_RpCurStatCheck  ;; 3 cycles
//  629 
//  630 		/* Enable interrupt */
//  631 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  632 		RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  633 		#else
//  634 		RP_PHY_EI();
//  635 		#endif
//  636 
//  637 		return (RpCurStatCheck(status));
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _RpCurStatCheck
        CALL      F:_RpCurStatCheck  ;; 3 cycles
        BR        R:??RpLog_Event_39  ;; 3 cycles
        ; ------------------------------------- Block: 15 cycles
//  638 	}
//  639 
//  640 	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
//  641 		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz) &&
//  642 		 (RpCb.pib.phyCurrentChannel >= RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)))
??RpLog_Event_40:
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_41   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_42   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_42   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, ES:_RpRxBuf+3354  ;; 2 cycles
        CMP       A, #0x9            ;; 1 cycle
        BC        ??RpLog_Event_42   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  643 	{
//  644 		if (RpCheckLongerThanTotalTxTime(len, options, RP_FALSE) == RP_TRUE)
??RpLog_Event_41:
        CLRB      B                  ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
          CFI FunCall _RpCheckLongerThanTotalTxTime
        CALL      F:_RpCheckLongerThanTotalTxTime  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_42   ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  645 		{
//  646 			/* Callback function execution Log */
//  647 			RpLog_Event( RP_LOG_CB, RP_LOG_CB_DATACFM );
//  648 			/* Callback function execution */
//  649 			INDIRECT_RpPdDataCfmCallback( RP_TRANSMIT_TIME_OVERFLOW, RP_NO_FRMPENBIT_ACK, 0 );
        CLRB      C                  ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        MOV       A, #0xD            ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3494)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+28
        POP       AX                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall
        CALL      HL                 ;; 3 cycles
//  650 
//  651 			/* API function execution Log */
//  652 			RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_LOG_API_RETERR_TOTALTXTIME );
//  653 
//  654 			/* Enable interrupt */
//  655 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  656 			RP_PHY_EI(bkupPsw);
        BR        R:??RpLog_Event_43  ;; 3 cycles
        ; ------------------------------------- Block: 20 cycles
//  657 			#else
//  658 			RP_PHY_EI();
//  659 			#endif
//  660 
//  661 			return (RP_PENDING);
//  662 		}
//  663 	}
//  664 
//  665 	if ((options & RP_TX_CCA) && (options & (RP_TX_CSMACA | RP_TX_CSMACA_RX | RP_TX_SLOTTED)))
??RpLog_Event_42:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].2, ??RpLog_Event_44  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        AND       A, #0x68           ;; 1 cycle
        BZ        ??RpLog_Event_44   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  666 	{
//  667 		/* API function execution Log */
//  668 		RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RETERR, RP_INVALID_API_OPTION );
//  669 
//  670 		/* Enable interrupt */
//  671 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  672 		RP_PHY_EI(bkupPsw);
??RpLog_Event_38:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  673 		#else
//  674 		RP_PHY_EI();
//  675 		#endif
//  676 
//  677 		return (RP_INVALID_API_OPTION);
        MOVW      AX, #0xF           ;; 1 cycle
        BR        R:??RpLog_Event_39  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
//  678 	}
//  679 
//  680 	if (((options & (RP_TX_CSMACA | RP_TX_CSMACA_RX)) == (RP_TX_CSMACA | RP_TX_CSMACA_RX)) || (len > (RP_BB_TX_RAM_SIZE * 2)))
??RpLog_Event_44:
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x60           ;; 1 cycle
        BZ        ??RpLog_Event_45   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        CMPW      AX, #0x101         ;; 1 cycle
        BC        ??RpLog_Event_46   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  681 	{
//  682 #if defined(__ICCRL78__)
//  683         RpCb.tx.pOrgData = pData;               // set pointer to original frame to be sent
??RpLog_Event_45:
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3270)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        ; ------------------------------------- Block: 12 cycles
//  684 #else
//  685 		RpMemcpy(RpCb.tx.data, pData, len);
//  686 		pData = RpCb.tx.data;
//  687 #endif
//  688 	}
//  689 
//  690 	RpCb.status = RP_PHY_STAT_TX;
??RpLog_Event_46:
        MOVW      AX, #0x2           ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
//  691 	RpClrAutoRxFunc();		//	clear auto functions
          CFI FunCall _RpClrAutoRxFunc
        CALL      F:_RpClrAutoRxFunc  ;; 3 cycles
//  692 	RpCb.rx.onReplyingAck = RP_FALSE;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3344   ;; 2 cycles
//  693 
//  694 	txFlen = (uint16_t)(len + RpCb.pib.phyFcsLength);	// len not includes FCS size(=2 or 4), Length Set
        MOV       X, ES:_RpRxBuf+3421  ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  695 	RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x520         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
//  696 	RpCb.reg.bbTxRxMode2 &= (uint8_t)(~RETRN);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3557  ;; 2 cycles
        AND       A, #0xF            ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3557, A  ;; 2 cycles
//  697 	RpRegWrite(BBTXRXMODE2, (uint8_t)(RpCb.reg.bbTxRxMode2));
        MOVW      AX, #0x48          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  698 
//  699 	if ((options & RP_TX_TIME) || (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
//  700 		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
        POP       AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, ??RpLog_Event_47  ;; 5 cycles
        ; ------------------------------------- Block: 47 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_47   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_48   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_48   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  701 	{
//  702 		RpCb.reg.bbTimeCon &= (uint8_t)(~(COMP0TRG | COMP0TRGSEL));
??RpLog_Event_47:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3554  ;; 2 cycles
        AND       A, #0xF5           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3554, A  ;; 2 cycles
//  703 		RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
//  704 	}
//  705 	RpCb.reg.bbTxRxMode4 = CCAINTSEL;
??RpLog_Event_48:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        ONEB      ES:_RpRxBuf+3559   ;; 2 cycles
//  706 	RpRegWrite(BBTXRXMODE4, (uint8_t)(RpCb.reg.bbTxRxMode4));
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x88          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  707 	RpCb.reg.bbCsmaCon0 |= CSMATRNST;
//  708 	RpCb.reg.bbCsmaCon0 &= ~(CSMAST | UNICASTFRM);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3560  ;; 2 cycles
        AND       A, #0xEE           ;; 1 cycle
        OR        A, #0x2            ;; 1 cycle
        MOV       ES:_RpRxBuf+3560, A  ;; 2 cycles
//  709 
//  710 	if (options & RP_TX_ACK)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].4, ??RpLog_Event_49  ;; 5 cycles
        ; ------------------------------------- Block: 23 cycles
//  711 	{
//  712 		RpCb.status |= RP_PHY_STAT_TRX_ACK;
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].1          ;; 3 cycles
//  713 		if (RpCb.pib.phyProfileSpecificMode == RP_SPECIFIC_WSUN)
        CMP       ES:_RpRxBuf+3426, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_50   ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
//  714 		{
//  715 			RpCb.reg.bbCsmaCon0 |= UNICASTFRM;
        MOVW      HL, #LWRD(_RpRxBuf+3560)  ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  716 		}
//  717 		RpRegWrite(BBTXRXCON, ACKRCVEN);
??RpLog_Event_50:
        MOV       C, #0x8            ;; 1 cycle
        BR        S:??RpLog_Event_51  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  718 	}
//  719 	else
//  720 	{
//  721 		RpRegWrite(BBTXRXCON, 0x00);
??RpLog_Event_49:
        CLRB      C                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_51:
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  722 	}
//  723 
//  724 	if (options & RP_TX_CSMACA)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].6, ??RpLog_Event_52  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
//  725 	{
//  726 		RpCb.status |= RP_PHY_STAT_TX_CCA;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3202.7  ;; 3 cycles
//  727 
//  728 		if (options & RP_TX_SLOTTED)
        BF        [HL].3, ??RpLog_Event_53  ;; 5 cycles
        ; ------------------------------------- Block: 9 cycles
//  729 		{
//  730 			RpCb.status |= RP_PHY_STAT_SLOTTED;
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].3          ;; 3 cycles
//  731 			RpCb.reg.bbTxRxMode0 |= BEACON;
        SET1      ES:_RpRxBuf+3555.6  ;; 3 cycles
//  732 			RpRegWrite(BBTXRXMODE0, (uint8_t)(RpCb.reg.bbTxRxMode0));
        MOV       C, ES:_RpRxBuf+3555  ;; 2 cycles
        MOVW      AX, #0x10          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 14 cycles
//  733 		}
//  734 
//  735 		if ((options & RP_TX_CSMACA_RX) && (RpAvailableRcvOnCsmaca() == RP_SUCCESS))
??RpLog_Event_53:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].5, ??RpLog_Event_54  ;; 5 cycles
          CFI FunCall _RpAvailableRcvOnCsmaca
        ; ------------------------------------- Block: 8 cycles
        CALL      F:_RpAvailableRcvOnCsmaca  ;; 3 cycles
        CMPW      AX, #0x7           ;; 1 cycle
        BNZ       ??RpLog_Event_54   ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  736 		{
//  737 			RpCb.status |= RP_PHY_STAT_RXON_BACKOFF;
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].0          ;; 3 cycles
//  738 			RpCb.reg.bbCsmaCon0 |= CSMARCVEN;
        MOVW      HL, #LWRD(_RpRxBuf+3560)  ;; 1 cycle
        SET1      ES:[HL].2          ;; 3 cycles
        BR        S:??RpLog_Event_55  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
//  739 		}
//  740 		else
//  741 		{
//  742 			RpCb.reg.bbCsmaCon0 &= ~CSMARCVEN;
??RpLog_Event_54:
        MOVW      HL, #LWRD(_RpRxBuf+3560)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:[HL].2          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  743 		}
//  744 		minBe = RpCb.pib.macMinBe & BEMIN;
??RpLog_Event_55:
        MOV       A, ES:_RpRxBuf+3411  ;; 2 cycles
        AND       A, #0xF            ;; 1 cycle
        MOV       [SP+0x03], A       ;; 1 cycle
//  745 		RpCb.reg.bbCsmaCon2 |= MACMINBECON;
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
        BR        S:??RpLog_Event_56  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
//  746 	}
//  747 	else if (options & RP_TX_CCA)
??RpLog_Event_52:
        BF        [HL].2, ??RpLog_Event_57  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  748 	{
//  749 		RpCb.status |= RP_PHY_STAT_CCA;
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:[HL].2          ;; 3 cycles
//  750 		RpCb.status |= RP_PHY_STAT_TX_CCA;
        SET1      ES:[HL].7          ;; 3 cycles
//  751 
//  752 		RpCb.reg.bbCsmaCon0 &= ~CSMARCVEN;
        CLR1      ES:_RpRxBuf+3560.2  ;; 3 cycles
//  753 		minBe = 0x00;
//  754 		RpCb.reg.bbCsmaCon2 |= MACMINBECON;
        SET1      ES:_RpRxBuf+3562.4  ;; 3 cycles
//  755 		
//  756 		RpStartTransmitWithCca();
          CFI FunCall _RpStartTransmitWithCca
        CALL      F:_RpStartTransmitWithCca  ;; 3 cycles
        BR        S:??RpLog_Event_56  ;; 3 cycles
        ; ------------------------------------- Block: 20 cycles
//  757 	}
//  758 	else
//  759 	{
//  760 		RpCb.reg.bbCsmaCon2 &= ~MACMINBECON;
??RpLog_Event_57:
        MOVW      HL, #LWRD(_RpRxBuf+3562)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:[HL].4          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  761 	}
//  762 	RpRegWrite(BBCSMACON3, (uint8_t)(minBe));
??RpLog_Event_56:
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  763 	RpRegWrite(BBCSMACON2, RpCb.reg.bbCsmaCon2);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3562  ;; 2 cycles
        MOVW      AX, #0x98          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  764 	RpCb.tx.rtrnTimes = RpCb.pib.macMaxFrameRetries;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3413  ;; 2 cycles
        MOV       ES:_RpRxBuf+3268, A  ;; 2 cycles
//  765 	RpCb.tx.ccaTimes = 0x00;
        CLRB      ES:_RpRxBuf+3269   ;; 2 cycles
//  766 
//  767 	if (options & RP_TX_AUTO_RCV)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].7, ??RpLog_Event_58  ;; 5 cycles
        ; ------------------------------------- Block: 28 cycles
//  768 	{
//  769 		RpCb.status |= RP_PHY_STAT_TRX_TO_RX_AUTO;
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  770 	}
//  771 
//  772 	if (RpCb.status & RP_PHY_STAT_TX_CCA)
??RpLog_Event_58:
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        BF        A.7, ??RpLog_Event_59  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
//  773 	{
//  774 		if ((RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE) && (RpCb.status & RP_PHY_STAT_RXON_BACKOFF))
        CMP       ES:_RpRxBuf+3428, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_60   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        CLRB      X                  ;; 1 cycle
        BF        A.0, ??RpLog_Event_60  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
//  775 		{
//  776 			ccaDurPibValReq = RP_FALSE;
//  777 
//  778 			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_61   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_61   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  779 			{
//  780 				if (RpCb.pib.phyCurrentChannel < RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)
        MOV       A, ES:_RpRxBuf+3354  ;; 2 cycles
        CMP       A, #0x9            ;; 1 cycle
        SKNC                         ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
//  781 				{
//  782 					ccaDurPibValReq = RP_TRUE;
//  783 				}
//  784 			}
//  785 		}
//  786 		else
//  787 		{
//  788 			ccaDurPibValReq = RP_TRUE;
??RpLog_Event_60:
        ONEB      X                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  789 		}
//  790 		RpSetCcaDurationVal(ccaDurPibValReq);
??RpLog_Event_61:
        MOV       A, X               ;; 1 cycle
          CFI FunCall _RpSetCcaDurationVal
        CALL      F:_RpSetCcaDurationVal  ;; 3 cycles
//  791 
//  792 		RpInverseTxAnt(RP_TRUE);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
//  793 		RpCb.tx.onCsmaCa = RP_TRUE;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        ONEB      ES:_RpRxBuf+3288   ;; 2 cycles
//  794 		RpSetMaxCsmaBackoffVal();
          CFI FunCall _RpSetMaxCsmaBackoffVal
        CALL      F:_RpSetMaxCsmaBackoffVal  ;; 3 cycles
//  795 		RpCb.tx.ccaTimesOneFrame = 0;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3289   ;; 2 cycles
        BR        S:??RpLog_Event_62  ;; 3 cycles
        ; ------------------------------------- Block: 20 cycles
//  796 	}
//  797 	else
//  798 	{
//  799 		RpInverseTxAnt(RP_FALSE);
??RpLog_Event_59:
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
//  800 		RpCb.tx.onCsmaCa = RP_FALSE;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3288   ;; 2 cycles
        ; ------------------------------------- Block: 7 cycles
//  801 	}
//  802 
//  803 	if (options & RP_TX_TIME)
??RpLog_Event_62:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].0, ??RpLog_Event_63  ;; 5 cycles
          CFI FunCall _RpGetTime
        ; ------------------------------------- Block: 8 cycles
//  804 	{
//  805 		// Timer Compare Reset
//  806 		futureTime = RpGetTime() - 1;
        CALL      F:_RpGetTime       ;; 3 cycles
        SUBW      AX, #0x1           ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
//  807 		RpRegBlockWrite(BBTCOMP0REG0, (uint8_t RP_FAR *)&futureTime, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x120         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
//  808 		RpReadIrq();
        POP       AX                 ;; 1 cycle
          CFI CFA SP+26
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x1B0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
//  809 		
//  810 		// TX with timer trigger
//  811 		RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3202.6  ;; 3 cycles
//  812 		if ((options & (RP_TX_CCA | RP_TX_CSMACA)) == 0)
        MOV       A, [SP+0x10]       ;; 1 cycle
        AND       A, #0x44           ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        BNZ       ??RpLog_Event_64   ;; 4 cycles
        ; ------------------------------------- Block: 49 cycles
//  813 		{
//  814 			// modify transmit time for RF IC warmup
//  815 			time -= RP_PHY_TX_WARM_UP_TIME;	// warmup time
//  816 			time &= RP_TIME_MASK;
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3434  ;; 2 cycles
        MOVW      BC, #0x15E         ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x1A], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x1C], AX      ;; 1 cycle
        ; ------------------------------------- Block: 44 cycles
//  817 		}
//  818 		RpCb.tx.time = time;
??RpLog_Event_64:
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3278)  ;; 1 cycle
        BR        S:??RpLog_Event_65  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  819 	}
//  820 	else
//  821 	{
//  822 		// TX immediately
//  823 		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
//  824 			((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
??RpLog_Event_63:
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_66   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_67   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_67   ;; 4 cycles
          CFI FunCall _RpGetTime
        ; ------------------------------------- Block: 6 cycles
//  825 		{
//  826 			RpCb.tx.time = RpGetTime();
??RpLog_Event_66:
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3278)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_65:
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 5 cycles
//  827 		}
//  828 	}
//  829 
//  830 	if ((RpCb.status & RP_PHY_STAT_TX_CCA) == RP_FALSE)
??RpLog_Event_67:
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        BT        A.7, ??RpLog_Event_68  ;; 5 cycles
          CFI FunCall _RpChangeFilter
        ; ------------------------------------- Block: 8 cycles
//  831 	{
//  832 		RpChangeFilter();
        CALL      F:_RpChangeFilter  ;; 3 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 3 cycles
//  833 	}
//  834 
//  835 	/* Disable interrupt */
//  836 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  837 	RP_PHY_ALL_DI(bkupPsw2);
??RpLog_Event_68:
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
//  838 	#else
//  839 	RP_PHY_ALL_DI();
//  840 	#endif
//  841 
//  842 	if (RpSetTxTriggerTimer(RpCb.tx.time, RP_FALSE, options) == RP_TRUE)	// TX trigger
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       D, A               ;; 1 cycle
        MOV       E, #0x0            ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3278)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall _RpSetTxTriggerTimer
        CALL      F:_RpSetTxTriggerTimer  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_69   ;; 4 cycles
        ; ------------------------------------- Block: 34 cycles
//  843 	{
//  844 		RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:[HL].6          ;; 3 cycles
        BR        S:??RpLog_Event_70  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
//  845 	}
//  846 	else
//  847 	{
//  848 		RpCb.reg.bbCsmaCon0 |= CSMAST;
??RpLog_Event_69:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3560.0  ;; 3 cycles
//  849 		RpCb.status |= RP_PHY_STAT_BUSY;
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        SET1      ES:[HL].5          ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
//  850 	}
//  851 	RpRegWrite(BBCSMACON0, RpCb.reg.bbCsmaCon0);	// TX trigger
??RpLog_Event_70:
        MOV       C, ES:_RpRxBuf+3560  ;; 2 cycles
        MOVW      AX, #0x68          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  852 
//  853 	if (RpCb.status & RP_PHY_STAT_TX_CCA)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        BF        A.7, ??RpLog_Event_71  ;; 5 cycles
        ; ------------------------------------- Block: 15 cycles
//  854 	{
//  855 		switch (ccaBandwidth)
        MOV       A, [SP+0x07]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_72   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_73   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_74   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_75   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??RpLog_Event_71  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  856 		{
//  857 			case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
//  858 				switch (edBandwidth)
??RpLog_Event_72:
        MOV       A, [SP+0x06]       ;; 1 cycle
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpLog_Event_76   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        BZ        ??RpLog_Event_77   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        S:??RpLog_Event_71  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth225k
        ; ------------------------------------- Block: 3 cycles
//  859 				{
//  860 					case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
//  861 					case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
//  862 						RpRegCcaBandwidth225k();
??RpLog_Event_76:
        CALL      F:_RpRegCcaBandwidth225k  ;; 3 cycles
//  863 						RpRegAdcVgaDefault();
          CFI FunCall _RpRegAdcVgaDefault
        CALL      F:_RpRegAdcVgaDefault  ;; 3 cycles
//  864 						break;
        BR        S:??RpLog_Event_71  ;; 3 cycles
          CFI FunCall _RpChangeDefaultFilter
        ; ------------------------------------- Block: 9 cycles
//  865 					case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
//  866 						RpChangeDefaultFilter();
??RpLog_Event_77:
        CALL      F:_RpChangeDefaultFilter  ;; 3 cycles
//  867 						break;
        BR        S:??RpLog_Event_71  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth200k
        ; ------------------------------------- Block: 6 cycles
//  868 					default:
//  869 						break;
//  870 				}
//  871 				break;
//  872 			case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
//  873 				RpRegCcaBandwidth200k();
??RpLog_Event_73:
        CALL      F:_RpRegCcaBandwidth200k  ;; 3 cycles
//  874 				RpRegAdcVgaModify();
        BR        S:??RpLog_Event_78  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth150k
        ; ------------------------------------- Block: 6 cycles
//  875 				break;
//  876 			case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
//  877 				RpRegCcaBandwidth150k();
??RpLog_Event_74:
        CALL      F:_RpRegCcaBandwidth150k  ;; 3 cycles
//  878 				RpRegAdcVgaModify();
          CFI FunCall _RpRegAdcVgaModify
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_78:
        CALL      F:_RpRegAdcVgaModify  ;; 3 cycles
//  879 				break;
        BR        S:??RpLog_Event_71  ;; 3 cycles
          CFI FunCall _RpChangeNarrowBandFilter
        ; ------------------------------------- Block: 6 cycles
//  880 			case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
//  881 				RpChangeNarrowBandFilter();
??RpLog_Event_75:
        CALL      F:_RpChangeNarrowBandFilter  ;; 3 cycles
//  882 				break;
        ; ------------------------------------- Block: 3 cycles
//  883 			default:
//  884 				break;
//  885 		}
//  886 	}
//  887 	RpSetSfdDetectionExtendWrite(RpCb.status);
??RpLog_Event_71:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
          CFI FunCall _RpSetSfdDetectionExtendWrite
        CALL      F:_RpSetSfdDetectionExtendWrite  ;; 3 cycles
//  888 
//  889 	RpCb.reg.bbIntEn[0] |= (TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3546  ;; 2 cycles
        OR        A, #0xB8           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3546, A  ;; 2 cycles
//  890 	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  891 	RpCb.tx.len = len;
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3206, AX  ;; 2 cycles
//  892 	RpCb.tx.cnt = 0x00;
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_RpRxBuf+3208, AX  ;; 2 cycles
//  893 	RpCb.tx.pNextData = pData;
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3274)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  894 	RpTrnxHdrFunc(RP_PHY_BANK_0);
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
//  895 	RpTrnxHdrFunc(RP_PHY_BANK_1);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpTrnxHdrFunc
        CALL      F:_RpTrnxHdrFunc   ;; 3 cycles
//  896 
//  897 	/* API function execution Log */
//  898 	RpLog_Event( RP_LOG_API_DATAREQ | RP_LOG_API_RET, RP_PENDING );
//  899 
//  900 	#if defined(__arm)
//  901    	RpLedTxOff();
//  902 	#endif
//  903 
//  904 	/* Enable interrupt */
//  905 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  906 	RP_PHY_ALL_EI(bkupPsw2);
        MOV       A, [SP+0x01]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  907 	RP_PHY_EI(bkupPsw);
        ; ------------------------------------- Block: 48 cycles
??RpLog_Event_43:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  908 	#else
//  909 	RP_PHY_ALL_EI();
//  910 	RP_PHY_EI();
//  911 	#endif
//  912 
//  913 	return (RP_PENDING);
        MOVW      AX, #0xC           ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_39:
        ADDW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 931 cycles
//  914 }
//  915 
//  916 /******************************************************************************
//  917 Function Name:		 RpExtChkErrFrameLen
//  918 Parameters: 		 len:
//  919                      useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
//  920                                     : 1:FCS length is RpCb.rx.fcsLength.
//  921 Return value:		 RP_TRUE:Illegal Frame Length
//  922 Description:		 Check Illegal Frame Length.
//  923 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon3
          CFI Function _RpExtChkErrFrameLen
        CODE
//  924 uint8_t
//  925 RpExtChkErrFrameLen(uint16_t len, uint8_t useRxFcsLength)
//  926 {
_RpExtChkErrFrameLen:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
//  927 	uint16_t totalLen, sduLen;
//  928 	uint8_t rtnVal = RP_FALSE;
        CLRB      B                  ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, B               ;; 1 cycle
//  929 	uint8_t fcsLength = RpCb.pib.phyFcsLength;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       E, A               ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3421  ;; 2 cycles
        XCH       A, E               ;; 1 cycle
//  930 
//  931 	if (useRxFcsLength == RP_TRUE)
        XCH       A, C               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        BNZ       ??RpLog_Event_79   ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
//  932 	{
//  933 		fcsLength = RpCb.rx.fcsLength;
        MOV       E, A               ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3336  ;; 2 cycles
        XCH       A, E               ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_79:
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  934 	}
//  935 	if ((len == 0) || (len > (RP_aMaxPHYPacketSize - fcsLength)))
        OR        A, X               ;; 1 cycle
        BZ        ??RpLog_Event_80   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       D, #0x0            ;; 1 cycle
        MOVW      AX, #0x640         ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        BC        ??RpLog_Event_80   ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
//  936 	{
//  937 		rtnVal = RP_TRUE;
//  938 	}
//  939 	else
//  940 	{
//  941 		if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_81   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  942 		{
//  943 			if((((uint32_t)(RpCb.pib.phyRmodeTonMax)) * 1000) < RpBytesToTime(len, useRxFcsLength))
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall _RpBytesToTime
        CALL      F:_RpBytesToTime   ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3462  ;; 2 cycles
        MOVW      BC, #0x3E8         ;; 1 cycle
        MULHU                        ;; 2 cycles
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        BNC       ??RpLog_Event_82   ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        BR        S:??RpLog_Event_80  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  944 			{
//  945 				rtnVal = RP_TRUE;
//  946 			}
//  947 		}
//  948 		else if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
??RpLog_Event_81:
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_82   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_82   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  949 		{
//  950 			totalLen = RpCalcTotalBytes(len, &sduLen, useRxFcsLength);
        MOV       A, C               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall _RpCalcTotalBytes
        CALL      F:_RpCalcTotalBytes  ;; 3 cycles
//  951 
//  952 			if (RpCb.pib.phyFskOpeMode == RP_PHY_FSK_OPEMODE_4)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3420, #0x4  ;; 2 cycles
        BNZ       ??RpLog_Event_83   ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
//  953 			{
//  954 				if (totalLen > RP_MAXVAL_FRAMELEN_ARIB * 2)
        CMPW      AX, #0x1389        ;; 1 cycle
        BC        ??RpLog_Event_82   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??RpLog_Event_80  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  955 				{
//  956 					rtnVal = RP_TRUE;
//  957 				}
//  958 			}
//  959 			else
//  960 			{
//  961 				if (totalLen > RP_MAXVAL_FRAMELEN_ARIB)
??RpLog_Event_83:
        CMPW      AX, #0x9C5         ;; 1 cycle
        BC        ??RpLog_Event_82   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  962 				{
//  963 					rtnVal = RP_TRUE;
??RpLog_Event_80:
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 2 cycles
//  964 				}
//  965 			}
//  966 		}
//  967 	}
//  968 	return (rtnVal);
??RpLog_Event_82:
        REQUIRE ?Subroutine9
        ; // Fall through to label ?Subroutine9
//  969 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon3
          CFI NoFunction
          CFI CFA SP+10
        CODE
?Subroutine9:
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
//  970 
//  971 /******************************************************************************
//  972 Function Name:       RpTC0SetReg
//  973 Parameters:          csmaStaTC0Ena: TC0 Start CSMA-CA
//  974 					 time: transmit time
//  975 Return value:		 none
//  976 Description:         TC0 register setting.
//  977 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon1
          CFI Function _RpTC0SetReg
        CODE
//  978 void
//  979 RpTC0SetReg(uint8_t csmaStaEna, uint32_t time)
//  980 {
_RpTC0SetReg:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 6
//  981 	RpRegBlockWrite(BBTCOMP0REG0, (uint8_t RP_FAR *)&time, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x120         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
//  982 	RpCb.reg.bbIntEn[0] |= TIM0INTEN;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3546.0  ;; 3 cycles
//  983 	RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpRxBuf+3546  ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
//  984 	RpCb.reg.bbTimeCon &= ~(COMP0TRG | COMP0TRGSEL);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3554  ;; 2 cycles
        AND       A, #0xF5           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3554, A  ;; 2 cycles
//  985 	if (csmaStaEna)
        MOV       A, [SP+0x03]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_84   ;; 4 cycles
        ; ------------------------------------- Block: 39 cycles
//  986 	{
//  987 		RpCb.reg.bbTimeCon |= (COMP0TRG | COMP0TRGSEL);	// CSMA-CA starts timer0 trigger
        MOV       A, X               ;; 1 cycle
        OR        A, #0xA            ;; 1 cycle
        MOV       ES:_RpRxBuf+3554, A  ;; 2 cycles
        BR        S:??RpLog_Event_85  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  988 	}
//  989 	else
//  990 	{
//  991 		RpCb.reg.bbTimeCon |= COMP0TRG;					// Tx starts timer0 trigger
??RpLog_Event_84:
        MOVW      HL, #LWRD(_RpRxBuf+3554)  ;; 1 cycle
        SET1      ES:[HL].1          ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  992 	}
//  993 	RpRegWrite(BBTIMECON, RpCb.reg.bbTimeCon);
??RpLog_Event_85:
        MOV       C, ES:_RpRxBuf+3554  ;; 2 cycles
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 53 cycles
        REQUIRE ?Subroutine4
        ; // Fall through to label ?Subroutine4
//  994 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+10
          CFI FunCall _RpTC0SetReg _RpRegWrite
          CFI FunCall _RpSetAntennaDiversityVal _RpRegWrite
        CODE
?Subroutine4:
        CALL      F:_RpRegWrite      ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
//  995 
//  996 /******************************************************************************
//  997 Function Name:       RpSetTxTriggerTimer
//  998 Parameters:          willTxTime: will transmit time
//  999 					 pRealTxTime: if so
// 1000 					 onRetrn: on Retransmission
// 1001 					 willTotalTxLength: will transmit total length
// 1002 Return value:		 need delay time
// 1003 Description:         set tx timer trigger with check tx interval time.
// 1004 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon2
          CFI Function _RpSetTxTriggerTimer
        CODE
// 1005 uint8_t
// 1006 RpSetTxTriggerTimer(uint32_t willTxTime, uint8_t onRetrn, uint8_t options)
// 1007 {
_RpSetTxTriggerTimer:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 22
        SUBW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+26
// 1008 	uint8_t  needDelayTime = RP_FALSE;
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 1009 	uint32_t realTime;
// 1010 	uint32_t difTime, difTime2, nowTime;
// 1011 	uint8_t waitTilIntervalTime = RP_FALSE;
        MOV       [SP+0x01], A       ;; 1 cycle
// 1012 	uint8_t csmaStEna;
// 1013 	uint8_t freqId = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
// 1014 
// 1015 	if ((RpCb.tx.needTxWaitRegulatoryMode) &&
// 1016 		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
// 1017 		 ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))))
        CMP0      ES:_RpRxBuf+3290   ;; 2 cycles
        BZ        ??RpLog_Event_86   ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_87   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3284)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 1018 	{
// 1019 		realTime = RpCb.tx.sentTime;
// 1020 		if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
// 1021 		{
// 1022 			difTime = (uint32_t)(RpCb.pib.phyRmodeToffMin) * (uint32_t)(RpCb.pib.phyDataRate);
        MOVW      BC, ES:_RpRxBuf+3434  ;; 2 cycles
        MOVW      AX, ES:_RpRxBuf+3464  ;; 2 cycles
        MULHU                        ;; 2 cycles
        BR        S:??RpLog_Event_88  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 1023 		}
??RpLog_Event_87:
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_86   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_86   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, E               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3284)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 1024 		else
// 1025 		{
// 1026 			difTime = RpCalcTxInterval(RpCb.tx.sentLen, onRetrn);
        MOVW      AX, ES:_RpRxBuf+3282  ;; 2 cycles
          CFI FunCall _RpCalcTxInterval
        CALL      F:_RpCalcTxInterval  ;; 3 cycles
        ; ------------------------------------- Block: 14 cycles
??RpLog_Event_88:
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
// 1027 		}
// 1028 		realTime += (uint32_t)difTime;
// 1029 		realTime &= (uint32_t)RP_TIME_MASK;
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 1030 		difTime2 = (uint32_t)((willTxTime - realTime /* tx time considered interval time */) & RP_TIME_MASK);
// 1031 		if ((difTime2 > RP_TIME_LIMIT) || (difTime2 < RP_RETX_TIMER_WASTE_TIME))
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, #0x2           ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x7FFF        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 43 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetTxTriggerTimer_0:
        BC        ??RpLog_Event_86   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1032 		{
// 1033 			waitTilIntervalTime = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpLog_Event_89  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1034 		}
// 1035 	}
// 1036 	if (waitTilIntervalTime == RP_FALSE)
// 1037 	{
// 1038 		realTime = willTxTime;
??RpLog_Event_86:
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
// 1039 	}
// 1040 	if ((waitTilIntervalTime == RP_TRUE) || (options & RP_TX_TIME))
??RpLog_Event_89:
        MOV       A, [SP+0x11]       ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_90  ;; 4 cycles
          CFI FunCall _RpGetTime
        ; ------------------------------------- Block: 9 cycles
// 1041 	{
// 1042 		nowTime = RpGetTime();
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
// 1043 		difTime = (realTime - nowTime) & RP_TIME_MASK;
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
// 1044 
// 1045 		if ((difTime > RP_TIME_LIMIT) || (difTime < (uint32_t)RpTc0WasteTime[freqId]))
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpTc0WasteTime)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpTc0WasteTime)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 41 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetTxTriggerTimer_1:
        BNC       ??RpLog_Event_91   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+30
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+26
        BNC       ??RpLog_Event_92   ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 1046 		{
// 1047 			realTime = (uint32_t)((nowTime + (uint32_t)RpTc0WasteTime[freqId]) & RP_TIME_MASK);
??RpLog_Event_91:
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
// 1048 		}
// 1049 		needDelayTime = RP_TRUE;
??RpLog_Event_92:
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 1050 	}
// 1051 	if (needDelayTime == RP_TRUE)
// 1052 	{
// 1053 		// set TC0 for TX or CSMA-CA
// 1054 		csmaStEna = (uint8_t)((RpCb.status & RP_PHY_STAT_TX_CCA) ? RP_TRUE : RP_FALSE);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 1055 		RpTC0SetReg(csmaStEna, realTime);			// TX trigger
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV1      CY, [HL].7         ;; 1 cycle
        ROLC      A, 0x1             ;; 1 cycle
          CFI FunCall _RpTC0SetReg
        CALL      F:_RpTC0SetReg     ;; 3 cycles
// 1056 		nowTime = RpGetTime();
// 1057 		difTime = (realTime - nowTime) & RP_TIME_MASK;
          CFI FunCall _RpGetTime
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      DE, AX             ;; 1 cycle
// 1058 
// 1059 		if ((difTime > RP_TIME_LIMIT) || (difTime == 0))
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 44 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetTxTriggerTimer_2:
        BNC       ??RpLog_Event_93   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        OR        A, E               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        OR        A, H               ;; 1 cycle
        BNZ       ??RpLog_Event_90   ;; 4 cycles
          CFI FunCall _RpCheckRfIRQ
        ; ------------------------------------- Block: 7 cycles
// 1060 		{
// 1061 			if (RpCheckRfIRQ() == RP_FALSE)
??RpLog_Event_93:
        CALL      F:_RpCheckRfIRQ    ;; 3 cycles
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_90   ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 1062 			{
// 1063 				RpCb.reg.bbIntEn[0] &= ~TIM0INTEN;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:_RpRxBuf+3546.0  ;; 3 cycles
// 1064 				RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpRxBuf+3546  ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1065 				RpCb.reg.bbTimeCon &= ~(COMP0TRG | COMP0TRGSEL);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3554  ;; 2 cycles
        AND       A, #0xF5           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3554, A  ;; 2 cycles
// 1066 				RpRegWrite(BBTIMECON, RpCb.reg.bbTimeCon);
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1067 				needDelayTime = RP_FALSE;
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 23 cycles
// 1068 			}
// 1069 		}
// 1070 	}
// 1071 
// 1072 	return (needDelayTime);
??RpLog_Event_90:
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 294 cycles
// 1073 }
// 1074 
// 1075 /******************************************************************************
// 1076 Function Name:       RpCalcTxInterval
// 1077 Parameters:          totalTxlength:
// 1078 Return value:		 intervall time
// 1079 Description:         calc tx interval time.
// 1080 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon3
          CFI Function _RpCalcTxInterval
        CODE
// 1081 uint32_t
// 1082 RpCalcTxInterval(uint16_t totalTxLength, uint8_t onRetrn)
// 1083 {
_RpCalcTxInterval:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 16
        SUBW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+20
// 1084 	uint32_t sendTime = RpCsmaBytesToTime(totalTxLength);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3434  ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, #0x1F40        ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
// 1085 	uint16_t symbol = RpCb.pib.phyDataRate;
// 1086 	uint8_t fskMode = RpCb.pib.phyFskOpeMode;
        MOV       B, ES:_RpRxBuf+3420  ;; 2 cycles
// 1087 	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
// 1088 	uint32_t difTime = 0;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 1089 
// 1090 	if (RpCb.pib.phyCurrentChannel < RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)
        MOV       A, ES:_RpRxBuf+3354  ;; 2 cycles
        CMP       A, #0x9            ;; 1 cycle
        BNC       ??RpLog_Event_94   ;; 4 cycles
        ; ------------------------------------- Block: 49 cycles
        MOV       A, [SP+0x0C]       ;; 1 cycle
// 1091 	{
// 1092 		if (onRetrn == RP_FALSE)
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_95  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1093 		{
// 1094 			difTime = ((uint32_t)(RP_PHY_TX_INTERVAL_TIME_LOW_CHANNEL * symbol) / (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT);
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, #0xC350        ;; 1 cycle
        BR        R:??RpLog_Event_96  ;; 3 cycles
          CFI CFA SP+20
        ; ------------------------------------- Block: 9 cycles
// 1095 		}
// 1096 	}
// 1097 	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
??RpLog_Event_94:
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_95  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1098 	{
// 1099 		if ((fskMode == RP_PHY_FSK_OPEMODE_1) || (fskMode == RP_PHY_FSK_OPEMODE_6))
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_97   ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0x6            ;; 1 cycle
        BNZ       ??RpLog_Event_98   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1100 		{
// 1101 			if ((sendTime > RP_PHY_SENT_FSK1_MIDDLE_FRAME_LOWER_LIMIT) && (sendTime <= RP_PHY_SENT_FSK1_MIDDLE_FRAME_UPPER_LIMIT))
??RpLog_Event_97:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        SUBW      AX, #0x1771        ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x2           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 13 cycles
        CMPW      AX, #0xF5D0        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpCalcTxInterval_0:
        BC        ??RpLog_Event_99   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1102 			{
// 1103 				difTime = ((uint32_t)(RP_PHY_TX_INTERVAL_TIME_SHORT_FRAME * symbol) / (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT);
// 1104 			}
// 1105 			else if ((sendTime > RP_PHY_SENT_FSK1_LONG_FRAME_LOWER_LIMIT) && (sendTime <= RP_PHY_SENT_FSK1_LONG_FRAME_UPPER_LIMIT))
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        SUBW      AX, #0xD41         ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, #0x3           ;; 1 cycle
        CMPW      AX, #0x3           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
        CMPW      AX, #0xD40         ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpCalcTxInterval_1:
        BNC       ??RpLog_Event_95   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1106 			{
// 1107 				difTime = ((uint32_t)(((sendTime * RP_PHY_TX_INTERVAL_LONG_FRAME_10TIMES) * symbol)) / (uint32_t)(RP_PHY_DATA_RATE_BPS_UNIT * 10));
        MOVW      DE, #0x2710        ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       X, #0x65           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
          CFI FunCall ?L_MUL_FAST_L03
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+28
        BR        S:??RpLog_Event_100  ;; 3 cycles
          CFI CFA SP+20
        ; ------------------------------------- Block: 21 cycles
// 1108 			}
// 1109 		}
// 1110 		else if ((fskMode == RP_PHY_FSK_OPEMODE_2) || (fskMode == RP_PHY_FSK_OPEMODE_5))
??RpLog_Event_98:
        CMP       A, #0x2            ;; 1 cycle
        BZ        ??RpLog_Event_101  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x5            ;; 1 cycle
        BNZ       ??RpLog_Event_102  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1111 		{
// 1112 			if ((sendTime > RP_PHY_SENT_FSK2_LONG_FRAME_LOWER_LIMIT) && (sendTime <= RP_PHY_SENT_FSK2_LONG_FRAME_UPPER_LIMIT))
??RpLog_Event_101:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        SUBW      AX, #0xBB9         ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 13 cycles
        CMPW      AX, #0x188         ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpCalcTxInterval_2:
        BC        ??RpLog_Event_99   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        S:??RpLog_Event_95  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1113 			{
// 1114 				difTime = ((uint32_t)(RP_PHY_TX_INTERVAL_TIME_SHORT_FRAME * symbol) / (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT);
// 1115 			}
// 1116 		}
// 1117 		else if ((fskMode == RP_PHY_FSK_OPEMODE_3) || (fskMode == RP_PHY_FSK_OPEMODE_4))
??RpLog_Event_102:
        CMP       A, #0x3            ;; 1 cycle
        BZ        ??RpLog_Event_103  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x4            ;; 1 cycle
        BNZ       ??RpLog_Event_95   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1118 		{
// 1119 			if ((sendTime > RP_PHY_SENT_FSK3_LONG_FRAME_LOWER_LIMIT) && (sendTime <= RP_PHY_SENT_FSK3_LONG_FRAME_UPPER_LIMIT))
??RpLog_Event_103:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        SUBW      AX, #0x7D1         ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x1           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 13 cycles
        CMPW      AX, #0x7ED0        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpCalcTxInterval_3:
        BNC       ??RpLog_Event_95   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1120 			{
// 1121 				difTime = ((uint32_t)(RP_PHY_TX_INTERVAL_TIME_SHORT_FRAME * symbol) / (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT);
??RpLog_Event_99:
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, #0x7D0         ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
??RpLog_Event_96:
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        CLRW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
          CFI FunCall ?L_MUL_FAST_L03
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_100:
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+24
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        ; ------------------------------------- Block: 27 cycles
// 1122 			}
// 1123 		}
// 1124 	}
// 1125 
// 1126 	return (difTime);
??RpLog_Event_95:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 244 cycles
// 1127 }
// 1128 
// 1129 /***************************************************************************
// 1130  * Name			: RpCalcTotalBytes
// 1131  * Parameters	: dataPayloadLength
// 1132                 : useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
// 1133                 :                : 1:FCS length is RpCb.rx.fcsLength.
// 1134  * Returns		: Total frame byte
// 1135  * Description	:
// 1136  **************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon3
          CFI Function _RpCalcTotalBytes
          CFI NoCalls
        CODE
// 1137 uint16_t
// 1138 RpCalcTotalBytes(uint16_t dataPayloadLength, uint16_t *pSduLen, uint8_t useRxFcsLength)
// 1139 {
_RpCalcTotalBytes:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        MOV       A, B               ;; 1 cycle
        MOV       H, A               ;; 1 cycle
// 1140 	uint16_t preambleLength = RpCb.pib.phyFskPreambleLength;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3416  ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1141 	uint16_t tailAndPaddingLength = 0;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1142 	uint8_t fecTxEna = RpCb.pib.phyFskFecTxEna;
        MOV       A, ES:_RpRxBuf+3365  ;; 2 cycles
        MOV       L, A               ;; 1 cycle
// 1143 	uint8_t fcsLength = RpCb.pib.phyFcsLength;
        MOV       A, ES:_RpRxBuf+3421  ;; 2 cycles
// 1144 	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
        MOV       X, ES:_RpRxBuf+3436  ;; 2 cycles
// 1145 	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
        MOV       B, ES:_RpRxBuf+3420  ;; 2 cycles
// 1146 	uint8_t sfdLength = RP_SFD_LEN_2;
        MOV       E, #0x2            ;; 1 cycle
// 1147 	uint8_t preLength = RP_PHR_LEN;
        MOV       C, #0x2            ;; 1 cycle
// 1148 	uint8_t freqId = RpCb.freqIdIndex;
        MOV       D, A               ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
        XCH       A, D               ;; 1 cycle
// 1149 
// 1150 	if (useRxFcsLength == RP_TRUE)
        XCH       A, H               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 31 cycles
// 1151 	{
// 1152 		fcsLength = RpCb.rx.fcsLength;
        MOV       A, ES:_RpRxBuf+3336  ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 1153 	}
// 1154 	if (fecTxEna == RP_TRUE)
??RpCalcTotalBytes_0:
        XCH       A, L               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        MOV       A, L               ;; 1 cycle
        BNZ       ??RpLog_Event_104  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 1155 	{
// 1156 		if (dataPayloadLength % 2)
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        BF        [HL].0, ??RpLog_Event_105  ;; 5 cycles
        ; ------------------------------------- Block: 9 cycles
// 1157 		{
// 1158 			tailAndPaddingLength = (1 << (uint16_t)1);
        MOVW      HL, #0x2           ;; 1 cycle
        BR        S:??RpLog_Event_106  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1159 		}
// 1160 		else
// 1161 		{
// 1162 			tailAndPaddingLength = (2 << (uint16_t)1);
??RpLog_Event_105:
        MOVW      HL, #0x4           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_106:
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1163 		}
// 1164 		dataPayloadLength <<= (uint16_t)1;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
// 1165 		fcsLength <<= (uint8_t)1;
        SHL       A, 0x1             ;; 1 cycle
// 1166 		preLength <<= (uint8_t)1;
        MOV       C, #0x4            ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
// 1167 	}
// 1168 
// 1169 	if (((freqBandId == RP_PHY_FREQ_BAND_920MHz) && (opeMode == RP_PHY_FSK_OPEMODE_4))
// 1170 			|| ((freqBandId == RP_PHY_FREQ_BAND_863MHz) && (opeMode == RP_PHY_FSK_OPEMODE_3)))
??RpLog_Event_104:
        XCH       A, X               ;; 1 cycle
        CMP       A, #0x9            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        BNZ       ??RpLog_Event_107  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        XCH       A, B               ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        BNZ       ??RpLog_Event_108  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        BR        S:??RpLog_Event_109  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_107:
        XCH       A, X               ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        BNZ       ??RpLog_Event_108  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        XCH       A, B               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
// 1171 	{
// 1172 		sfdLength = RP_SFD_LEN_4;
??RpLog_Event_109:
        MOV       E, #0x4            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1173 	}
// 1174 
// 1175 	*pSduLen = (uint16_t)(dataPayloadLength + fcsLength + tailAndPaddingLength);
??RpLog_Event_108:
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       L, A               ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1176 
// 1177 	return ((uint16_t)(preambleLength + sfdLength + preLength + *pSduLen + RpRampUpDownStableTotalByte[freqId]));
        MOV       A, D               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, #LWRD(_RpRampUpDownStableTotalByte)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpRampUpDownStableTotalByte)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        CLRB      B                  ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        MOV       D, #0x0            ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        ADDW      AX, HL             ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        ADDW      AX, HL             ;; 1 cycle
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 50 cycles
        ; ------------------------------------- Total: 141 cycles
// 1178 }
// 1179 
// 1180 /***************************************************************************
// 1181  * Name			: RpCsmaBytesToTime
// 1182  * Parameters	: txTotalLength
// 1183  * Returns		: frame time [usec]
// 1184  * Description	:
// 1185  **************************************************************************/
// 1186 static uint32_t RpCsmaBytesToTime( uint16_t txTotalLength )
// 1187 {
// 1188 	return ((uint32_t)(txTotalLength) * 8 * 1000 / (uint32_t)(RpCb.pib.phyDataRate));
// 1189 }
// 1190 
// 1191 /******************************************************************************
// 1192 Function Name:       RpTrnxHdrFunc
// 1193 Parameters:          trnxNo:transmitted rambank number: 0 or 1
// 1194 Return value:        none
// 1195 Description:         this function is called trn0/1 interrupt handler.
// 1196 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon1
          CFI Function _RpTrnxHdrFunc
        CODE
// 1197 void
// 1198 RpTrnxHdrFunc(uint8_t trnxNo)
// 1199 {
_RpTrnxHdrFunc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 12
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+16
// 1200 	uint16_t	status;
// 1201 	uint16_t	len;
// 1202 	uint8_t	*pData, ramOffset, ramOffsetAddr;
// 1203 	uint16_t wrAddr;
// 1204 
// 1205 	status = RpCb.status;
// 1206 	if (status & RP_PHY_STAT_TX)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        BT        A.1, $+7           ;; 5 cycles
        BR        F:??RpLog_Event_110  ;; 5 cycles
        ; ------------------------------------- Block: 11 cycles
// 1207 	{
// 1208 		if (RpCb.tx.pNextData != RP_NULL)
        MOVW      HL, #LWRD(_RpRxBuf+3274)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_111  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_111:
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_110  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1209 		{
// 1210 			RpRegBlockRead(BBTXCOUNT, (uint8_t *)&len, sizeof(uint16_t));
        MOVW      HL, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x5E0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 1211 			if (RpCb.tx.cnt < len)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      HL, ES:_RpRxBuf+3208  ;; 2 cycles
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        CMPW      AX, HL             ;; 1 cycle
        SKNH                         ;; 4 cycles
        BR        R:??RpLog_Event_112  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
// 1212 			{
// 1213 				RpCb.status |= RP_PHY_STAT_TX_UNDERFLOW;
// 1214 				RpRfStat.txRamUnderrun++;
// 1215 
// 1216 				return;
// 1217 			}
// 1218 			ramOffset = (uint8_t)(RpCb.tx.cnt % RP_BB_TX_RAM_SIZE);
        MOV       C, #0x80           ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall ?SI_MOD_L02
        CALL      N:?SI_MOD_L02      ;; 3 cycles
        MOV       A, X               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
// 1219 			if ((RpCb.tx.len - RpCb.tx.cnt) > RP_BB_TX_RAM_SIZE)
        MOVW      AX, ES:_RpRxBuf+3206  ;; 2 cycles
        SUBW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8081        ;; 1 cycle
        BC        ??RpLog_Event_113  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
// 1220 			{
// 1221 				len = (uint16_t)(RP_BB_TX_RAM_SIZE - ramOffset);
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, #0x80          ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1222 				pData = RpCb.tx.pNextData;
        MOVW      HL, #LWRD(_RpRxBuf+3274)  ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1223 				RpCb.tx.pNextData = pData + len;
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, X               ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+16
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??RpLog_Event_114  ;; 3 cycles
        ; ------------------------------------- Block: 41 cycles
// 1224 			}
// 1225 			else
// 1226 			{
// 1227 				len = (uint16_t)(RpCb.tx.len - RpCb.tx.cnt);
??RpLog_Event_113:
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1228 				pData = RpCb.tx.pNextData;
        MOVW      HL, #LWRD(_RpRxBuf+3274)  ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1229 				RpCb.tx.pNextData = RP_NULL;
        CLRB      ES:_RpRxBuf+3276   ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 18 cycles
??RpLog_Event_114:
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x0B]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
// 1230 			}
// 1231 			ramOffsetAddr = (uint8_t)(ramOffset << 3);
        MOV       A, C               ;; 1 cycle
        SHL       A, 0x3             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
// 1232 			wrAddr = (uint16_t)(trnxNo ? BBTXRAM1 + ramOffsetAddr : BBTXRAM0 + ramOffsetAddr);
        CLRB      A                  ;; 1 cycle
        CMP0      B                  ;; 1 cycle
        BZ        ??RpLog_Event_115  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        ADDW      AX, #0x1400        ;; 1 cycle
        BR        S:??RpLog_Event_116  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_115:
        ADDW      AX, #0x1000        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_116:
        MOVW      HL, AX             ;; 1 cycle
// 1233 			RpRegBlockWrite(wrAddr, (uint8_t RP_FAR *)pData, len);	// Data Set to transmitted RAMx
        MOVW      AX, [SP]           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 1234 			RpCb.tx.cnt += len;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3208  ;; 2 cycles
        ADDW      AX, HL             ;; 1 cycle
        MOVW      ES:_RpRxBuf+3208, AX  ;; 2 cycles
// 1235 			RpRegBlockRead(BBTXCOUNT, (uint8_t *)&len, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x5E0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 1236 
// 1237 			if (RpCb.tx.cnt < len)
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      HL, ES:_RpRxBuf+3208  ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        CMPW      AX, HL             ;; 1 cycle
        BNH       ??RpLog_Event_110  ;; 4 cycles
        ; ------------------------------------- Block: 40 cycles
// 1238 			{
// 1239 				RpCb.status |= RP_PHY_STAT_TX_UNDERFLOW;
??RpLog_Event_112:
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].7          ;; 3 cycles
// 1240 				RpRfStat.txRamUnderrun++;
        MOVW      HL, #LWRD(_RpRfStat+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRfStat)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 23 cycles
// 1241 			}
// 1242 		}
// 1243 	}
// 1244 }
??RpLog_Event_110:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 218 cycles
// 1245 
// 1246 /***************************************************************************************************************
// 1247  * function name  : RpPlmeEdReq
// 1248  * description    : ED (Energy Detection) Request Function
// 1249  * parameters     : none
// 1250  * return value   : RP_PENDING, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_LOWPOWER
// 1251  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon1
          CFI Function _RpPlmeEdReq
        CODE
// 1252 int16_t RpPlmeEdReq( void )
// 1253 {
_RpPlmeEdReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 10
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+14
// 1254 	uint16_t	status;
// 1255 	int16_t	rtn = RP_PENDING;
        MOVW      AX, #0xC           ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 1256 	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3449  ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
// 1257 	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
        MOV       A, ES:_RpRxBuf+3448  ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 1258 	uint16_t ccaDuration = 31;	// AGC+delay+ED
        MOVW      AX, #0x1F          ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 1259 	uint8_t index = RpCb.freqIdIndex;
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
// 1260 	uint8_t reg488h;
// 1261 
// 1262 	#if defined(__RX)
// 1263 	uint32_t bkupPsw;
// 1264 	uint32_t bkupPsw2;
// 1265 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1266 	uint8_t  bkupPsw;
// 1267 	uint8_t  bkupPsw2;
// 1268 	#elif defined(__arm)
// 1269 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 1270 	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
// 1271 	#endif
// 1272 
// 1273 	/* Disable interrupt */
// 1274 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1275 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x03], A       ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1276 	#else
// 1277 	RP_PHY_DI();
// 1278 	#endif
// 1279 
// 1280 	/* API function execution Log */
// 1281 	RpLog_Event( RP_LOG_API_ED, RP_NULL );
// 1282 
// 1283 	// status check
// 1284 	status = RpCb.status;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 1285 
// 1286 	if ((status & RP_PHY_STAT_TRX_OFF) == 0)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].4, ??RpLog_Event_117  ;; 5 cycles
        ; ------------------------------------- Block: 35 cycles
// 1287 	{
// 1288 		rtn = RpCurStatCheck(status);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _RpCurStatCheck
        CALL      F:_RpCurStatCheck  ;; 3 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        BR        R:??RpLog_Event_118  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 1289 	}
// 1290 	else
// 1291 	{
// 1292 		RpCb.status = (RP_PHY_STAT_ED | RP_PHY_STAT_BUSY);	// set ED Mode Set
??RpLog_Event_117:
        MOVW      AX, #0x28          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 1293 		RpClrAutoRxFunc();		//	clear auto functions
          CFI FunCall _RpClrAutoRxFunc
        CALL      F:_RpClrAutoRxFunc  ;; 3 cycles
// 1294 		RpSetCcaDurationVal(RP_TRUE);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetCcaDurationVal
        CALL      F:_RpSetCcaDurationVal  ;; 3 cycles
// 1295 		RpInverseTxAnt(RP_TRUE);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
// 1296 		RpRegWrite(BBCSMACON3, 0x01);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1297 		RpCb.reg.bbTxRxMode4 = 0x00;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3559   ;; 2 cycles
// 1298 		RpRegWrite(BBTXRXMODE4, (uint8_t)(RpCb.reg.bbTxRxMode4));
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x88          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1299 		RpCb.reg.bbTxRxMode1 &= ~CCASEL;	// CCA/ED
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:_RpRxBuf+3556.0  ;; 3 cycles
// 1300 		RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
        MOV       C, ES:_RpRxBuf+3556  ;; 2 cycles
        MOVW      AX, #0x18          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1301 		RpCb.reg.bbIntEn[0] |= CCAINTEN;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3546.7  ;; 3 cycles
// 1302 		RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpRxBuf+3546  ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1303 
// 1304 		if ((index != RP_PHY_100K_M10_JP) && (index != RP_PHY_100K_M10_IN)) // except for 100kbps m=1
        MOV       A, [SP+0x02]       ;; 1 cycle
        CMP       A, #0x9            ;; 1 cycle
        BZ        ??RpLog_Event_119  ;; 4 cycles
        ; ------------------------------------- Block: 53 cycles
        CMP       A, #0xA            ;; 1 cycle
        BZ        ??RpLog_Event_119  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1305 		{
// 1306 			if ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K)) // filter change case
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_120  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_121  ;; 4 cycles
          CFI FunCall _RpRegTxRxDataRate100kbps
        ; ------------------------------------- Block: 5 cycles
// 1307 			{
// 1308 				RpRegTxRxDataRate100kbps();
??RpLog_Event_120:
        CALL      F:_RpRegTxRxDataRate100kbps  ;; 3 cycles
// 1309 				RpRegBlockWrite(BBCCATIME, (uint8_t RP_FAR *)&ccaDuration, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x590         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 1310 				reg488h = RpRegRead(0x0488 << 3);
// 1311 
// 1312 				if (reg488h == RP_PHY_REG488H_DEFAULT)
        MOVW      AX, #0x2440        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        CMP       A, #0x82           ;; 1 cycle
        BNZ       ??RpLog_Event_119  ;; 4 cycles
          CFI FunCall _RpRegRxDataRate100kbps
        ; ------------------------------------- Block: 23 cycles
// 1313 				{
// 1314 					RpRegRxDataRate100kbps();
        CALL      F:_RpRegRxDataRate100kbps  ;; 3 cycles
        BR        S:??RpLog_Event_119  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1315 				}
// 1316 			}
// 1317 			else if ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K)) // && ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) || (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW))
??RpLog_Event_121:
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_122  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_119  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1318 			{
// 1319 				reg488h = RpRegRead(0x0488 << 3);
// 1320 
// 1321 				if (reg488h != RP_PHY_REG488H_DEFAULT)
??RpLog_Event_122:
        MOVW      AX, #0x2440        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        CMP       A, #0x82           ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _RpRegRxDataRateDefault
        ; ------------------------------------- Block: 6 cycles
// 1322 				{
// 1323 					RpRegRxDataRateDefault();
        CALL      F:_RpRegRxDataRateDefault  ;; 3 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 3 cycles
// 1324 				}
// 1325 			}
// 1326 			else // ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) && (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K))
// 1327 			{
// 1328 				// Setting at PIB
// 1329 			}
// 1330 		}
// 1331 
// 1332 		/* Disable interrupt */
// 1333 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1334 		RP_PHY_ALL_DI(bkupPsw2);
??RpLog_Event_119:
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 1335 		#else
// 1336 		RP_PHY_ALL_DI();
// 1337 		#endif
// 1338 
// 1339 		RpRegWrite(BBTXRXCON, CCATRG);	// cca trigger enable
        MOV       C, #0x4            ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1340 
// 1341 		switch (edBandwidth)
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_123  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_124  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_125  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_126  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??RpLog_Event_127  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1342 		{
// 1343 			case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
// 1344 				switch (ccaBandwidth)
??RpLog_Event_123:
        MOV       A, [SP]            ;; 1 cycle
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpLog_Event_128  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        BZ        ??RpLog_Event_129  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        S:??RpLog_Event_127  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth225k
        ; ------------------------------------- Block: 3 cycles
// 1345 				{
// 1346 					case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
// 1347 					case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
// 1348 						RpRegCcaBandwidth225k();
??RpLog_Event_128:
        CALL      F:_RpRegCcaBandwidth225k  ;; 3 cycles
// 1349 						RpRegAdcVgaDefault();
          CFI FunCall _RpRegAdcVgaDefault
        CALL      F:_RpRegAdcVgaDefault  ;; 3 cycles
// 1350 						break;
        BR        S:??RpLog_Event_127  ;; 3 cycles
          CFI FunCall _RpChangeDefaultFilter
        ; ------------------------------------- Block: 9 cycles
// 1351 					case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
// 1352 						RpChangeDefaultFilter();
??RpLog_Event_129:
        CALL      F:_RpChangeDefaultFilter  ;; 3 cycles
// 1353 						break;
        BR        S:??RpLog_Event_127  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth200k
        ; ------------------------------------- Block: 6 cycles
// 1354 					default:
// 1355 						break;
// 1356 				}
// 1357 				break;
// 1358 			case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
// 1359 				RpRegCcaBandwidth200k();
??RpLog_Event_124:
        CALL      F:_RpRegCcaBandwidth200k  ;; 3 cycles
// 1360 				RpRegAdcVgaModify();
        BR        S:??RpLog_Event_130  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth150k
        ; ------------------------------------- Block: 6 cycles
// 1361 				break;
// 1362 			case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
// 1363 				RpRegCcaBandwidth150k();
??RpLog_Event_125:
        CALL      F:_RpRegCcaBandwidth150k  ;; 3 cycles
// 1364 				RpRegAdcVgaModify();
          CFI FunCall _RpRegAdcVgaModify
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_130:
        CALL      F:_RpRegAdcVgaModify  ;; 3 cycles
// 1365 				break;
        BR        S:??RpLog_Event_127  ;; 3 cycles
          CFI FunCall _RpChangeNarrowBandFilter
        ; ------------------------------------- Block: 6 cycles
// 1366 			case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
// 1367 				RpChangeNarrowBandFilter();
??RpLog_Event_126:
        CALL      F:_RpChangeNarrowBandFilter  ;; 3 cycles
// 1368 				break;
        ; ------------------------------------- Block: 3 cycles
// 1369 			default:
// 1370 				break;
// 1371 		}
// 1372 
// 1373 		/* Enable interrupt */
// 1374 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1375 		RP_PHY_ALL_EI(bkupPsw2);
??RpLog_Event_127:
        MOV       A, [SP+0x02]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1376 		#else
// 1377 		RP_PHY_ALL_EI();
// 1378 		#endif
// 1379 	}
// 1380 
// 1381 	/* API function execution Log */
// 1382 	RpLog_Event( RP_LOG_API_ED | RP_LOG_API_RET, (uint8_t)rtn );
// 1383 
// 1384 	/* Enable interrupt */
// 1385 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1386 	RP_PHY_EI(bkupPsw);
??RpLog_Event_118:
        MOV       A, [SP+0x03]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1387 	#else
// 1388 	RP_PHY_EI();
// 1389 	#endif
// 1390 
// 1391 	return (rtn);
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 269 cycles
// 1392 }
// 1393 
// 1394 /***************************************************************************************************************
// 1395  * function name  : RpPlmeCcaReq
// 1396  * description    : CCA Request Function
// 1397  * parameters     : none
// 1398  * return value   : RP_PENDING, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_LOWPOWER
// 1399  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon1
          CFI Function _RpPlmeCcaReq
        CODE
// 1400 int16_t RpPlmeCcaReq( void )
// 1401 {
_RpPlmeCcaReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 8
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
// 1402 	uint16_t	status;
// 1403 	int16_t	rtn = RP_PENDING;
        MOVW      AX, #0xC           ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 1404 	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3449  ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
// 1405 	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
        MOV       A, ES:_RpRxBuf+3448  ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 1406 	uint8_t index = RpCb.freqIdIndex;
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
// 1407 	uint8_t reg488h;
// 1408 
// 1409 	#if defined(__RX)
// 1410 	uint32_t bkupPsw;
// 1411 	uint32_t bkupPsw2;
// 1412 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1413 	uint8_t  bkupPsw;
// 1414 	uint8_t  bkupPsw2;
// 1415 	#elif defined(__arm)
// 1416 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 1417 	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
// 1418 	#endif
// 1419 
// 1420 	/* Disable interrupt */
// 1421 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1422 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x03], A       ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1423 	#else
// 1424 	RP_PHY_DI();
// 1425 	#endif
// 1426 
// 1427 	/* API function execution Log */
// 1428 	RpLog_Event( RP_LOG_API_CCA, RP_NULL );
// 1429 
// 1430 	// status check
// 1431 	status = RpCb.status;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 1432 	if ((status & RP_PHY_STAT_TRX_OFF) == 0)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].4, ??RpLog_Event_131  ;; 5 cycles
        ; ------------------------------------- Block: 33 cycles
// 1433 	{
// 1434 		rtn = RpCurStatCheck(status);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _RpCurStatCheck
        CALL      F:_RpCurStatCheck  ;; 3 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        BR        R:??RpLog_Event_132  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 1435 	}
// 1436 	else
// 1437 	{
// 1438 		RpCb.status = (RP_PHY_STAT_CCA | RP_PHY_STAT_BUSY);	// set CCA Mode Set
??RpLog_Event_131:
        MOVW      AX, #0x24          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 1439 		RpClrAutoRxFunc();		//	clear auto functions
          CFI FunCall _RpClrAutoRxFunc
        CALL      F:_RpClrAutoRxFunc  ;; 3 cycles
// 1440 		RpSetCcaDurationVal(RP_TRUE);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetCcaDurationVal
        CALL      F:_RpSetCcaDurationVal  ;; 3 cycles
// 1441 		RpInverseTxAnt(RP_TRUE);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
// 1442 		RpRegWrite(BBCSMACON3, 0x01);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1443 		RpCb.reg.bbTxRxMode4 = 0x00;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3559   ;; 2 cycles
// 1444 		RpRegWrite(BBTXRXMODE4, (uint8_t)(RpCb.reg.bbTxRxMode4));
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x88          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1445 		RpCb.reg.bbTxRxMode1 &= ~CCASEL;	// CCA/ED
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:_RpRxBuf+3556.0  ;; 3 cycles
// 1446 		RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
        MOV       C, ES:_RpRxBuf+3556  ;; 2 cycles
        MOVW      AX, #0x18          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1447 		RpCb.reg.bbIntEn[0] |= CCAINTEN;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3546.7  ;; 3 cycles
// 1448 		RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       C, ES:_RpRxBuf+3546  ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1449 
// 1450 		if ((index != RP_PHY_100K_M10_JP) && (index != RP_PHY_100K_M10_IN)) // except for 100kbps m=1
        MOV       A, [SP+0x02]       ;; 1 cycle
        CMP       A, #0x9            ;; 1 cycle
        BZ        ??RpLog_Event_133  ;; 4 cycles
        ; ------------------------------------- Block: 53 cycles
        CMP       A, #0xA            ;; 1 cycle
        BZ        ??RpLog_Event_133  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1451 		{
// 1452 			if ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K))	// filter change case
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_134  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_135  ;; 4 cycles
          CFI FunCall _RpRegTxRxDataRate100kbps
        ; ------------------------------------- Block: 5 cycles
// 1453 			{
// 1454 				RpRegTxRxDataRate100kbps();
??RpLog_Event_134:
        CALL      F:_RpRegTxRxDataRate100kbps  ;; 3 cycles
// 1455 				reg488h = RpRegRead(0x0488 << 3);
// 1456 
// 1457 				if (reg488h == RP_PHY_REG488H_DEFAULT)
        MOVW      AX, #0x2440        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        CMP       A, #0x82           ;; 1 cycle
        BNZ       ??RpLog_Event_133  ;; 4 cycles
          CFI FunCall _RpRegRxDataRate100kbps
        ; ------------------------------------- Block: 12 cycles
// 1458 				{
// 1459 					RpRegRxDataRate100kbps();
        CALL      F:_RpRegRxDataRate100kbps  ;; 3 cycles
        BR        S:??RpLog_Event_133  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1460 				}
// 1461 			}
// 1462 			else if ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K)) // && ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) || (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW))
??RpLog_Event_135:
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_136  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_133  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1463 			{
// 1464 				reg488h = RpRegRead(0x0488 << 3);
// 1465 
// 1466 				if (reg488h != RP_PHY_REG488H_DEFAULT)
??RpLog_Event_136:
        MOVW      AX, #0x2440        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        CMP       A, #0x82           ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _RpRegRxDataRateDefault
        ; ------------------------------------- Block: 6 cycles
// 1467 				{
// 1468 					RpRegRxDataRateDefault();
        CALL      F:_RpRegRxDataRateDefault  ;; 3 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 3 cycles
// 1469 				}
// 1470 			}
// 1471 			else	// ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) && (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K))
// 1472 			{
// 1473 				// Setting at PIB
// 1474 			}
// 1475 		}
// 1476 
// 1477 		/* Disable interrupt */
// 1478 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1479 		RP_PHY_ALL_DI(bkupPsw2);
??RpLog_Event_133:
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 1480 		#else
// 1481 		RP_PHY_ALL_DI();
// 1482 		#endif
// 1483 
// 1484 		RpRegWrite(BBTXRXCON, CCATRG);	// cca trigger enable
        MOV       C, #0x4            ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1485 
// 1486 		switch (ccaBandwidth)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_137  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_138  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_139  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_140  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??RpLog_Event_141  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1487 		{
// 1488 			case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
// 1489 				switch (edBandwidth)
??RpLog_Event_137:
        MOV       A, [SP+0x01]       ;; 1 cycle
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpLog_Event_142  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        BZ        ??RpLog_Event_143  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        S:??RpLog_Event_141  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth225k
        ; ------------------------------------- Block: 3 cycles
// 1490 				{
// 1491 					case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
// 1492 					case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
// 1493 						RpRegCcaBandwidth225k();
??RpLog_Event_142:
        CALL      F:_RpRegCcaBandwidth225k  ;; 3 cycles
// 1494 						RpRegAdcVgaDefault();
          CFI FunCall _RpRegAdcVgaDefault
        CALL      F:_RpRegAdcVgaDefault  ;; 3 cycles
// 1495 						break;
        BR        S:??RpLog_Event_141  ;; 3 cycles
          CFI FunCall _RpChangeDefaultFilter
        ; ------------------------------------- Block: 9 cycles
// 1496 					case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
// 1497 						RpChangeDefaultFilter();
??RpLog_Event_143:
        CALL      F:_RpChangeDefaultFilter  ;; 3 cycles
// 1498 						break;
        BR        S:??RpLog_Event_141  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth200k
        ; ------------------------------------- Block: 6 cycles
// 1499 					default:
// 1500 						break;
// 1501 				}
// 1502 				break;
// 1503 			case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
// 1504 				RpRegCcaBandwidth200k();
??RpLog_Event_138:
        CALL      F:_RpRegCcaBandwidth200k  ;; 3 cycles
// 1505 				RpRegAdcVgaModify();
        BR        S:??RpLog_Event_144  ;; 3 cycles
          CFI FunCall _RpRegCcaBandwidth150k
        ; ------------------------------------- Block: 6 cycles
// 1506 				break;
// 1507 			case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
// 1508 				RpRegCcaBandwidth150k();
??RpLog_Event_139:
        CALL      F:_RpRegCcaBandwidth150k  ;; 3 cycles
// 1509 				RpRegAdcVgaModify();
          CFI FunCall _RpRegAdcVgaModify
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_144:
        CALL      F:_RpRegAdcVgaModify  ;; 3 cycles
// 1510 				break;
        BR        S:??RpLog_Event_141  ;; 3 cycles
          CFI FunCall _RpChangeNarrowBandFilter
        ; ------------------------------------- Block: 6 cycles
// 1511 			case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
// 1512 				RpChangeNarrowBandFilter();
??RpLog_Event_140:
        CALL      F:_RpChangeNarrowBandFilter  ;; 3 cycles
// 1513 				break;
        ; ------------------------------------- Block: 3 cycles
// 1514 			default:
// 1515 				break;
// 1516 		}
// 1517 
// 1518 		/* Enable interrupt */
// 1519 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1520 		RP_PHY_ALL_EI(bkupPsw2);
??RpLog_Event_141:
        MOV       A, [SP+0x02]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1521 		#else
// 1522 		RP_PHY_ALL_EI();
// 1523 		#endif
// 1524 	}
// 1525 
// 1526 	/* API function execution Log */
// 1527 	RpLog_Event( RP_LOG_API_CCA | RP_LOG_API_RET, (uint8_t)rtn );
// 1528 
// 1529 	/* Enable interrupt */
// 1530 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1531 	RP_PHY_EI(bkupPsw);
??RpLog_Event_132:
        MOV       A, [SP+0x03]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1532 	#else
// 1533 	RP_PHY_EI();
// 1534 	#endif
// 1535 
// 1536 	return (rtn);
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 256 cycles
// 1537 }
// 1538 
// 1539 /******************************************************************************
// 1540 Function Name:       RpSetLowPowerProhibit
// 1541 Parameters:          none
// 1542 Return value:        none
// 1543 Description:         set prohibit-bit to go to low power mode
// 1544 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon1
          CFI Function _RpSetLowPowerProhibit
          CFI NoCalls
        CODE
// 1545 void
// 1546 RpSetLowPowerProhibit(void)
// 1547 {
_RpSetLowPowerProhibit:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 1548 	RpCb.prohibitLowPower = RP_TRUE;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        ONEB      ES:_RpRxBuf+3545   ;; 2 cycles
// 1549 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
// 1550 
// 1551 /******************************************************************************
// 1552 Function Name:       RpClrLowPowerProhibit
// 1553 Parameters:          none
// 1554 Return value:        none
// 1555 Description:         clear prohibit-bit to go to low power mode
// 1556 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon1
          CFI Function _RpClrLowPowerProhibit
          CFI NoCalls
        CODE
// 1557 void
// 1558 RpClrLowPowerProhibit(void)
// 1559 {
_RpClrLowPowerProhibit:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 1560 	RpCb.prohibitLowPower = RP_FALSE;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3545   ;; 2 cycles
// 1561 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
// 1562 
// 1563 /******************************************************************************
// 1564 Function Name:       RpGetLowPowerProhibit
// 1565 Parameters:          none
// 1566 Return value:        none
// 1567 Description:         refer prohibit-bit to go to low power mode
// 1568 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock18 Using cfiCommon1
          CFI Function _RpGetLowPowerProhibit
          CFI NoCalls
        CODE
// 1569 uint8_t
// 1570 RpGetLowPowerProhibit(void)
// 1571 {
_RpGetLowPowerProhibit:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 1572 	uint8_t rtn;
// 1573 
// 1574 	rtn = RpCb.prohibitLowPower;
// 1575 
// 1576 	return (rtn);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3545  ;; 2 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock18
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
// 1577 }
// 1578 
// 1579 /***************************************************************************************************************
// 1580  * function name  : RpSetPowerMode
// 1581  * description    : Base Band Unit Power Mode Setting
// 1582  * parameters     : nxRf...Specify the power mode of the base band unit.
// 1583  * return value   : RP_SUCCESS, RP_SAME_STATUS, RP_INVALID_PARAMETER, RP_BUSY_RX, RP_BUSY_TX, RP_BUSY_OTHER
// 1584  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock19 Using cfiCommon1
          CFI Function _RpSetPowerMode
        CODE
// 1585 int16_t RpSetPowerMode( uint8_t nxtRf )
// 1586 {
_RpSetPowerMode:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 8
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+12
// 1587 	int16_t rtn;
// 1588 	uint16_t status;
// 1589 
// 1590 	#if defined(__RX)
// 1591 	uint32_t bkupPsw;
// 1592 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1593 	uint8_t  bkupPsw;
// 1594 	#elif defined(__arm)
// 1595 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 1596 	#endif
// 1597 
// 1598 	/* Disable interrupt */
// 1599 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1600 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1601 	#else
// 1602 	RP_PHY_DI();
// 1603 	#endif
// 1604 
// 1605 	/* API function execution Log */
// 1606 	RpLog_Event( RP_LOG_API_SETPOW, RP_NULL );
// 1607 
// 1608 	if ((nxtRf != RP_RF_IDLE) && (nxtRf != RP_RF_LOWPOWER))
        MOV       A, [SP+0x07]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_145  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_145  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1609 	{
// 1610 		/* API function execution Log */
// 1611 		RpLog_Event( RP_LOG_API_SETPOW | RP_LOG_API_RETERR, RP_INVALID_PARAMETER );
// 1612 
// 1613 		/* Enable interrupt */
// 1614 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1615 		RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1616 		#else
// 1617 		RP_PHY_EI();
// 1618 		#endif
// 1619 
// 1620 		return (RP_INVALID_PARAMETER);
        MOVW      AX, #0x5           ;; 1 cycle
        BR        R:??RpLog_Event_146  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 1621 	}
// 1622 
// 1623 	status = RpCb.status;
??RpLog_Event_145:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1624 	if (status & RP_PHY_STAT_TRX_OFF)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].4, $+7        ;; 5 cycles
        BR        F:??RpLog_Event_147  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
// 1625 	{
// 1626 		if (nxtRf == RP_RF_LOWPOWER)
        MOV       A, [SP+0x07]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_148  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1627 		{
// 1628 			if (RpCb.prohibitLowPower == RP_FALSE)
        CMP0      ES:_RpRxBuf+3545   ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_149  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1629 			{
// 1630 				RpSetMcuInt(RP_FALSE);			// RF interrupts request clear and disable
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetMcuInt
        CALL      F:_RpSetMcuInt     ;; 3 cycles
// 1631 				RpRegWrite(BBRFCON, RP_RF_POWEROFF);
        CLRB      C                  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1632 
// 1633 				if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
// 1634 					((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_150  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_151  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_151  ;; 4 cycles
          CFI FunCall _RpPrevSentTimeReSetting
        ; ------------------------------------- Block: 6 cycles
// 1635 				{
// 1636 					RpPrevSentTimeReSetting();
??RpLog_Event_150:
        CALL      F:_RpPrevSentTimeReSetting  ;; 3 cycles
// 1637 					RpCb.reg.bbTimeCon &= ~(TIMEEN | COMP0TRG);	// Timer Count Disable, Comp0 transmit Disable
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3554  ;; 2 cycles
        AND       A, #0xFC           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3554, A  ;; 2 cycles
// 1638 					RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1639 					RpCb.txLmtTimer.lackTime += RpLimitTimerControl(RP_FALSE);
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpLimitTimerControl
        CALL      F:_RpLimitTimerControl  ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3540)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3540)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 1640 #if defined(__RL78__)
// 1641 					if (RpCb.txLmtTimer.lackTime > (uint32_t)1000)
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 54 cycles
        CMPW      AX, #0x3E9         ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetPowerMode_0:
        BC        ??RpLog_Event_151  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1642 					{
// 1643 						RpCb.txLmtTimer.lackTime -= (uint32_t)1000;
        SUBW      AX, #0x3E8         ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 1644 						RpTxLimitTimerForward(1);
        ONEW      AX                 ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
          CFI FunCall _RpTxLimitTimerForward
        CALL      F:_RpTxLimitTimerForward  ;; 3 cycles
          CFI FunCall _RpPowerdownSequence
        ; ------------------------------------- Block: 16 cycles
// 1645 					}
// 1646 #endif
// 1647 				}
// 1648 				RpPowerdownSequence();
??RpLog_Event_151:
        CALL      F:_RpPowerdownSequence  ;; 3 cycles
// 1649 				RpCb.status = RP_PHY_STAT_LOWPOWER;
        MOVW      AX, #0x2000        ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 1650 				rtn = RP_SUCCESS;
        MOVW      AX, #0x7           ;; 1 cycle
        BR        R:??RpLog_Event_152  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
// 1651 			}
// 1652 			else // if(RpCb.prohibitLowPower == RP_TRUE)
// 1653 			{
// 1654 				rtn = RP_BUSY_OTHER;
??RpLog_Event_149:
        MOVW      AX, #0xE           ;; 1 cycle
        BR        R:??RpLog_Event_152  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1655 			}
// 1656 		}
// 1657 		else // if (nxtrf == RP_RF_IDLE)
// 1658 		{
// 1659 			rtn = RP_SAME_STATUS;
// 1660 		}
// 1661 	}
// 1662 	else if	(status & RP_PHY_STAT_LOWPOWER)
??RpLog_Event_147:
        INCW      HL                 ;; 1 cycle
        BT        [HL].5, $+7        ;; 5 cycles
        BR        F:??RpLog_Event_153  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1663 	{
// 1664 		if (nxtRf == RP_RF_IDLE)
        MOV       A, [SP+0x07]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_148  ;; 4 cycles
          CFI FunCall _RpWakeupSequence
        ; ------------------------------------- Block: 6 cycles
// 1665 		{
// 1666 			RpInitRfic();				// Restart of MAC
        CALL      F:_RpWakeupSequence  ;; 3 cycles
          CFI FunCall _RpSetRegBeforeIdle
        CALL      F:_RpSetRegBeforeIdle  ;; 3 cycles
          CFI FunCall _RpInitRfOnly
        CALL      F:_RpInitRfOnly    ;; 3 cycles
// 1667 			RpSetMcuInt(RP_TRUE);		// RF interrupts request clear and enable
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetMcuInt
        CALL      F:_RpSetMcuInt     ;; 3 cycles
// 1668 			RpSetRfInt(RP_TRUE);
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x1B0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3546   ;; 2 cycles
        MOV       ES:_RpRxBuf+3547, #0xEF  ;; 2 cycles
        CLRB      ES:_RpRxBuf+3548   ;; 2 cycles
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      DE, #LWRD(_RpRxBuf+3546)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 1669 
// 1670 			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
// 1671 				((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_154  ;; 4 cycles
        ; ------------------------------------- Block: 51 cycles
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_155  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_155  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1672 			{
// 1673 				RpCb.reg.bbTimeCon |= TIMEEN;	// Timer Count Start, Comp0 transmit Disable Stay
??RpLog_Event_154:
        SET1      ES:_RpRxBuf+3554.0  ;; 3 cycles
// 1674 				RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOV       C, ES:_RpRxBuf+3554  ;; 2 cycles
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1675 				RpLimitTimerControl(RP_TRUE);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpLimitTimerControl
        CALL      F:_RpLimitTimerControl  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
// 1676 			}
// 1677 			RpCb.status = RP_PHY_STAT_TRX_OFF;
??RpLog_Event_155:
        MOVW      AX, #0x10          ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 1678 			rtn = RP_SUCCESS;
        MOV       X, #0x7            ;; 1 cycle
        BR        S:??RpLog_Event_152  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 1679 		}
// 1680 		else // if (nxtRf == RP_RF_LOWPOWER)
// 1681 		{
// 1682 			rtn = RP_SAME_STATUS;
??RpLog_Event_148:
        MOVW      AX, #0xF8          ;; 1 cycle
        BR        S:??RpLog_Event_152  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1683 		}
// 1684 	}
// 1685 	else if (status & RP_PHY_STAT_TX)
??RpLog_Event_153:
        DECW      HL                 ;; 1 cycle
        BF        [HL].1, ??RpLog_Event_156  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 1686 	{
// 1687 		rtn = RP_BUSY_TX;
        MOVW      AX, #0x2           ;; 1 cycle
        BR        S:??RpLog_Event_152  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1688 	}
// 1689 	else
// 1690 	{
// 1691 		rtn = RP_BUSY_RX;
??RpLog_Event_156:
        ONEW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_152:
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1692 	}
// 1693 
// 1694 	/* API function execution Log */
// 1695 	RpLog_Event( RP_LOG_API_SETPOW | RP_LOG_API_RET, (uint8_t)rtn );
// 1696 
// 1697 	/* Enable interrupt */
// 1698 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1699 	RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1700 	#else
// 1701 	RP_PHY_EI();
// 1702 	#endif
// 1703 
// 1704 	return (rtn);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
??RpLog_Event_146:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock19
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 295 cycles
// 1705 }
// 1706 
// 1707 /***************************************************************************************************************
// 1708  * function name  : RpGetPowerMode
// 1709  * description    : Base Band Unit Power Mode Reference
// 1710  * parameters     : none
// 1711  * return value   : RP_RF_IDLE, RP_RF_LOWPOWER
// 1712  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock20 Using cfiCommon1
          CFI Function _RpGetPowerMode
        CODE
// 1713 uint16_t RpGetPowerMode( void )
// 1714 {
_RpGetPowerMode:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 1715 	uint16_t lpMode = RP_RF_IDLE;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1716 	uint16_t status;
// 1717 
// 1718 	#if defined(__RX)
// 1719 	uint32_t bkupPsw;
// 1720 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1721 	uint8_t  bkupPsw;
// 1722 	#elif defined(__arm)
// 1723 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 1724 	#endif
// 1725 
// 1726 	/* Disable interrupt */
// 1727 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1728 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1729 	#else
// 1730 	RP_PHY_DI();
// 1731 	#endif
// 1732 
// 1733 	/* API function execution Log */
// 1734 	RpLog_Event( RP_LOG_API_GETPOW, RP_NULL );
// 1735 
// 1736 	status = RpCb.status;
// 1737 	if (status & RP_PHY_STAT_LOWPOWER)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        BF        A.5, ??RpLog_Event_157  ;; 5 cycles
        ; ------------------------------------- Block: 19 cycles
// 1738 	{
// 1739 		lpMode = RP_RF_LOWPOWER;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1740 	}
// 1741 
// 1742 	/* API function execution Log */
// 1743 	RpLog_Event( RP_LOG_API_GETPOW | RP_LOG_API_RET, (uint8_t)lpMode );
// 1744 
// 1745 	/* Enable interrupt */
// 1746 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1747 	RP_PHY_EI(bkupPsw);
??RpLog_Event_157:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1748 	#else
// 1749 	RP_PHY_EI();
// 1750 	#endif
// 1751 
// 1752 	return (lpMode);
        BR        F:??Subroutine13_0  ;; 3 cycles
          CFI EndBlock cfiBlock20
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 28 cycles
// 1753 }
// 1754 
// 1755 /***************************************************************************************************************
// 1756  * function name  : RpGetTime
// 1757  * description    : Present Time Acquisition
// 1758  * parameters     : none
// 1759  * return value   : Present time of the base band unit
// 1760  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock21 Using cfiCommon3
          CFI Function _RpGetTime
        CODE
// 1761 uint32_t RpGetTime( void )
// 1762 {
_RpGetTime:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 1763 	uint32_t time;
// 1764 	uint16_t status;
// 1765 
// 1766 	#if defined(__RX)
// 1767 	uint32_t bkupPsw;
// 1768 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1769 	uint8_t  bkupPsw;
// 1770 	#elif defined(__arm)
// 1771 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 1772 	#endif
// 1773 
// 1774 	/* Disable interrupt */
// 1775 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1776 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1777 	#else
// 1778 	RP_PHY_DI();
// 1779 	#endif
// 1780 
// 1781 	/* get current status */
// 1782 	status = RpCb.status;
// 1783 
// 1784 	/* check current status */
// 1785 	if ( status & RP_PHY_STAT_LOWPOWER )
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        BF        A.5, ??RpLog_Event_158  ;; 5 cycles
        ; ------------------------------------- Block: 17 cycles
// 1786 	{
// 1787 		time = 0xFFFFFFFF;	// busy lowpower
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        BR        S:??RpLog_Event_159  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1788 	}
// 1789 	else
// 1790 	{
// 1791 		RpRegBlockRead(BBTIMEREAD0, (uint8_t *)&time, sizeof(uint32_t));
??RpLog_Event_158:
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x100         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        ; ------------------------------------- Block: 13 cycles
// 1792 	}
// 1793 
// 1794 	/* Enable interrupt */
// 1795 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1796 	RP_PHY_EI(bkupPsw);
??RpLog_Event_159:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
          CFI EndBlock cfiBlock21
        ; ------------------------------------- Block: 4 cycles
        ; ------------------------------------- Total: 40 cycles
// 1797 	#else
// 1798 	RP_PHY_EI();
// 1799 	#endif
// 1800 
// 1801 	return (time & RP_TIME_MASK);
        REQUIRE ?Subroutine3
        ; // Fall through to label ?Subroutine3
// 1802 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock22 Using cfiCommon3
          CFI NoFunction
          CFI CFA SP+10
        CODE
?Subroutine3:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock22
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 1803 
// 1804 /***************************************************************************************************************
// 1805  * function name  : RptConTxNoModu
// 1806  * description    : Continuous Unmodulated Transmission <for RF Characteristic Evaluation>
// 1807  * parameters     : none
// 1808  * return value   : none
// 1809  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock23 Using cfiCommon1
          CFI Function _RptConTxNoModu
        CODE
// 1810 void RptConTxNoModu( void )
// 1811 {
_RptConTxNoModu:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 1812 	uint16_t txFlen = 5;
        MOVW      AX, #0x5           ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1813 
// 1814 	#if defined(__RX)
// 1815 	uint32_t bkupPsw;
// 1816 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1817 	uint8_t  bkupPsw;
// 1818 	#elif defined(__arm)
// 1819 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 1820 	#endif
// 1821 
// 1822 	/* Disable interrupt */
// 1823 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1824 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1825 	#else
// 1826 	RP_PHY_DI();
// 1827 	#endif
// 1828 
// 1829 	RpCb.status = RP_PHY_STAT_TX | RP_PHY_STAT_TRX_CONTINUOUS;
        MOVW      AX, #0x4002        ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 1830 	RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x520         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 1831 	RpRegWrite(BBEVAREG, CONTTX | NOMOD);
        MOV       C, #0x3            ;; 1 cycle
          CFI EndBlock cfiBlock23
        ; ------------------------------------- Block: 28 cycles
        ; ------------------------------------- Total: 28 cycles
        REQUIRE ?Subroutine1
        ; // Fall through to label ?Subroutine1
// 1832 	RpRegWrite(BBTXRXCON, TRNTRG);				// transmit trigger enable
// 1833 
// 1834 	#if defined(__arm)
// 1835    	RpLedTxOff();
// 1836 	#endif
// 1837 
// 1838 	/* Enable interrupt */
// 1839 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1840 	RP_PHY_EI(bkupPsw);
// 1841 	#else
// 1842 	RP_PHY_EI();
// 1843 	#endif
// 1844 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock24 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+10
        CODE
?Subroutine1:
        MOVW      AX, #0x340         ;; 1 cycle
          CFI FunCall _RptConTxNoModu _RpRegWrite
          CFI FunCall _RptPulseTxPrbs9 _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       C, #0x2            ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RptConTxNoModu _RpRegWrite
          CFI FunCall _RptPulseTxPrbs9 _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
          CFI FunCall _RptConTxNoModu ___set_psw
          CFI FunCall _RptPulseTxPrbs9 ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock24
        ; ------------------------------------- Block: 20 cycles
        ; ------------------------------------- Total: 20 cycles
// 1845 
// 1846 /***************************************************************************************************************
// 1847  * function name  : RptConTrxStop
// 1848  * description    : Continuous Transmission/Reception Stop <for RF Characteristic Evaluation>
// 1849  * parameters     : none
// 1850  * return value   : none
// 1851  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock25 Using cfiCommon1
          CFI Function _RptConTrxStop
        CODE
// 1852 void RptConTrxStop( void )
// 1853 {
_RptConTrxStop:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 1854 	#if defined(__RX)
// 1855 	uint32_t bkupPsw;
// 1856 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1857 	uint8_t  bkupPsw;
// 1858 	#elif defined(__arm)
// 1859 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 1860 	#endif
// 1861 
// 1862 	/* Disable interrupt */
// 1863 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1864 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1865 	#else
// 1866 	RP_PHY_DI();
// 1867 	#endif
// 1868 
// 1869 	RpRxOnStop();
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x8           ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOVW      AX, #0x10          ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 1870 	RpRegWrite(BBEVAREG, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x340         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1871 	RpSetPowerMode(RP_RF_LOWPOWER);	// for RF reset
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetPowerMode
        CALL      F:_RpSetPowerMode  ;; 3 cycles
// 1872 	RpSetPowerMode(RP_RF_IDLE);		// for RF reset
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetPowerMode
        CALL      F:_RpSetPowerMode  ;; 3 cycles
// 1873 	RpCb.status = RP_PHY_STAT_TRX_OFF;
        MOVW      AX, #0x10          ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 1874 
// 1875 	/* Enable interrupt */
// 1876 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1877 	RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1878 	#else
// 1879 	RP_PHY_EI();
// 1880 	#endif
// 1881 }
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock25
        ; ------------------------------------- Block: 46 cycles
        ; ------------------------------------- Total: 46 cycles
// 1882 
// 1883 /***************************************************************************************************************
// 1884  * function name  : RptPulseTxPrbs9
// 1885  * description    : PN9 Continuous Modulated Transmission <for RF Characteristic Evaluation>
// 1886  * parameters     : none
// 1887  * return value   : none
// 1888  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock26 Using cfiCommon1
          CFI Function _RptPulseTxPrbs9
        CODE
// 1889 void RptPulseTxPrbs9( void )
// 1890 {
_RptPulseTxPrbs9:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 1891 	uint16_t txFlen;
// 1892 
// 1893 	#if defined(__RX)
// 1894 	uint32_t bkupPsw;
// 1895 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 1896 	uint8_t  bkupPsw;
// 1897 	#elif defined(__arm)
// 1898 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 1899 	#endif
// 1900 
// 1901 	/* Disable interrupt */
// 1902 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1903 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 1904 	#else
// 1905 	RP_PHY_DI();
// 1906 	#endif
// 1907 
// 1908 	RpRegWrite(BBTXRAM0, 0x00);														// psdu is a 0x00 only
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x1000        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1909 	RpCb.reg.bbSubCon |= DWEN;	// forced DW enable
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3566.3  ;; 3 cycles
// 1910 	RpRegWrite(BBSUBGCON, (uint8_t)(RpCb.reg.bbSubCon));
        MOV       C, ES:_RpRxBuf+3566  ;; 2 cycles
        MOVW      AX, #0x580         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1911 	txFlen = (uint16_t)(RpCb.pib.phyFcsLength + 1/* need one byte + FCS Len */);	// 0x00(1byte) + FCS Length
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3421  ;; 2 cycles
        CLRB      A                  ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1912 	RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x520         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 1913 	RpCb.status = RP_PHY_STAT_TX | RP_PHY_STAT_TRX_CONTINUOUS;
        MOVW      AX, #0x4002        ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 1914 	RpRegWrite(BBEVAREG, CONTTX);
        ONEB      C                  ;; 1 cycle
        BR        F:?Subroutine1     ;; 3 cycles
          CFI EndBlock cfiBlock26
        ; ------------------------------------- Block: 50 cycles
        ; ------------------------------------- Total: 50 cycles
// 1915 	RpRegWrite(BBTXRXCON, TRNTRG);				// transmit trigger enable
// 1916 
// 1917 	#if defined(__arm)
// 1918    	RpLedTxOff();
// 1919 	#endif
// 1920 
// 1921 	/* Enable interrupt */
// 1922 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 1923 	RP_PHY_EI(bkupPsw);
// 1924 	#else
// 1925 	RP_PHY_EI();
// 1926 	#endif
// 1927 }
// 1928 
// 1929 /******************************************************************************
// 1930 Function Name:       RpSetAdrfAndAutoAckVal
// 1931 Parameters:          none
// 1932 Return value:        none
// 1933 Description:         set register for Address filter and Auto Ack.
// 1934 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock27 Using cfiCommon1
          CFI Function _RpSetAdrfAndAutoAckVal
        CODE
// 1935 void RpSetAdrfAndAutoAckVal( void )
// 1936 {
_RpSetAdrfAndAutoAckVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 1937 	uint8_t addressFilter1 = RpCb.pib.macAddressFilter1Ena;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3378  ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
// 1938 	uint8_t addressFilter2 = RpCb.pib.macAddressFilter2Ena;
        MOV       A, ES:_RpRxBuf+3394  ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 1939 	uint8_t regConvert = RP_FALSE;
        CLRB      B                  ;; 1 cycle
// 1940 	uint16_t byteRcvNum = RP_PHY_RX_ADDRESS_DECODE_LEN;
        MOVW      AX, #0x1B          ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1941 
// 1942 	RpCb.rx.softwareAdf = RP_FALSE;
        CLRB      ES:_RpRxBuf+3345   ;; 2 cycles
// 1943 	if (RpCb.callback.pAckCheckCallback != RpAckCheckCallback)
        MOVW      HL, #LWRD(_RpRxBuf+3522)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #BYTE3(_RpAckCheckCallback)  ;; 1 cycle
        BNZ       ??RpLog_Event_160  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #LWRD(_RpAckCheckCallback)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_160:
        BZ        ??RpLog_Event_161  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1944 	{
// 1945 		RpCb.rx.softwareAdf = RP_TRUE;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        ONEB      ES:_RpRxBuf+3345   ;; 2 cycles
// 1946 		RpCb.reg.bbTxRxMode3 &= ~ADRSFILEN;
        CLR1      ES:_RpRxBuf+3558.0  ;; 3 cycles
// 1947 		RpCb.reg.bbTxRxMode0 &= ~AUTOACKEN;
        CLR1      ES:_RpRxBuf+3555.2  ;; 3 cycles
// 1948 		RpCb.reg.bbTxRxMode3 &= ~ADFEXTEN;
        CLR1      ES:_RpRxBuf+3558.6  ;; 3 cycles
// 1949 		RpRegBlockWrite(BBRCVINTCOMP, (uint8_t RP_FAR *)&byteRcvNum, sizeof(byteRcvNum));
        BR        R:??RpLog_Event_162  ;; 3 cycles
        ; ------------------------------------- Block: 15 cycles
// 1950 	}
// 1951 	else
// 1952 	{
// 1953 		if ((addressFilter1 == RP_FALSE) && (addressFilter2 == RP_FALSE))
??RpLog_Event_161:
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_163  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_164  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1954 		{
// 1955 			RpCb.reg.bbTxRxMode3 &= ~ADRSFILEN;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:_RpRxBuf+3558.0  ;; 3 cycles
// 1956 			RpCb.reg.bbTxRxMode0 &= ~AUTOACKEN;
        MOVW      HL, #LWRD(_RpRxBuf+3555)  ;; 1 cycle
        CLR1      ES:[HL].2          ;; 3 cycles
        BR        S:??RpLog_Event_165  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
// 1957 		}
// 1958 		else
// 1959 		{
// 1960 			if (((addressFilter1 == RP_TRUE) && (RpCb.pib.macPanId1 == 0xFFFF)) ||
// 1961 					((addressFilter2 == RP_TRUE) && (RpCb.pib.macPanId2 == 0xFFFF)))
??RpLog_Event_163:
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_164  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3382  ;; 2 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BZ        ??RpLog_Event_166  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
??RpLog_Event_164:
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_167  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3398  ;; 2 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BNZ       ??RpLog_Event_167  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 1962 			{
// 1963 				RpCb.rx.softwareAdf = RP_TRUE;
??RpLog_Event_166:
        ONEB      ES:_RpRxBuf+3345   ;; 2 cycles
// 1964 				RpCb.reg.bbTxRxMode3 &= ~ADRSFILEN;
        CLR1      ES:_RpRxBuf+3558.0  ;; 3 cycles
// 1965 				RpCb.reg.bbTxRxMode0 &= ~AUTOACKEN;
        MOVW      HL, #LWRD(_RpRxBuf+3555)  ;; 1 cycle
        CLR1      ES:[HL].2          ;; 3 cycles
        BR        S:??RpLog_Event_168  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
// 1966 			}
// 1967 			else
// 1968 			{
// 1969 				RpCb.reg.bbTxRxMode3 |= ADRSFILEN;
??RpLog_Event_167:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3558.0  ;; 3 cycles
// 1970 				RpCb.reg.bbTxRxMode0 |= AUTOACKEN;
        MOVW      HL, #LWRD(_RpRxBuf+3555)  ;; 1 cycle
        SET1      ES:[HL].2          ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 1971 			}
// 1972 		}
// 1973 
// 1974 		if ((addressFilter1 == RP_FALSE) && (addressFilter2 == RP_TRUE))
??RpLog_Event_168:
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_165  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 1975 		{
// 1976 			regConvert = RP_TRUE;
        ONEB      B                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1977 		}
// 1978 		RpSetAdrfRegConverion(regConvert);
??RpLog_Event_165:
        MOV       A, B               ;; 1 cycle
          CFI FunCall _RpSetAdrfRegConverion
        CALL      F:_RpSetAdrfRegConverion  ;; 3 cycles
// 1979 		if ((addressFilter1 == RP_TRUE) && (addressFilter2 == RP_TRUE))
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_169  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_169  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1980 		{
// 1981 			RpCb.reg.bbTxRxMode3 |= ADFEXTEN;
        MOVW      HL, #LWRD(_RpRxBuf+3558)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:[HL].6          ;; 3 cycles
        BR        S:??RpLog_Event_170  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 1982 		}
// 1983 		else
// 1984 		{
// 1985 			RpCb.reg.bbTxRxMode3 &= ~ADFEXTEN;
??RpLog_Event_169:
        MOVW      HL, #LWRD(_RpRxBuf+3558)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:[HL].6          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1986 		}
// 1987 		if (RpCb.rx.softwareAdf == RP_TRUE)
??RpLog_Event_170:
        CMP       ES:_RpRxBuf+3345, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_171  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1988 		{
// 1989 			RpRegBlockWrite(BBRCVINTCOMP, (uint8_t RP_FAR *)&byteRcvNum, sizeof(byteRcvNum));
??RpLog_Event_162:
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x690         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+8
        ; ------------------------------------- Block: 13 cycles
// 1990 		}
// 1991 	}
// 1992 	RpRegWrite(BBTXRXMODE0, (uint8_t)(RpCb.reg.bbTxRxMode0));
??RpLog_Event_171:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3555  ;; 2 cycles
        MOVW      AX, #0x10          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 1993 	RpRegWrite(BBTXRXMODE3, (uint8_t)(RpCb.reg.bbTxRxMode3));
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3558  ;; 2 cycles
        MOVW      AX, #0x50          ;; 1 cycle
        BR        F:?Subroutine5     ;; 3 cycles
          CFI EndBlock cfiBlock27
        ; ------------------------------------- Block: 14 cycles
        ; ------------------------------------- Total: 189 cycles
// 1994 }
// 1995 
// 1996 /******************************************************************************
// 1997 Function Name:       RpSetAdrfRegConverion
// 1998 Parameters:          convert
// 1999 Return value:        none
// 2000 Description:         convert Adrf1 and Adrf2 register.
// 2001 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock28 Using cfiCommon1
          CFI Function _RpSetAdrfRegConverion
        CODE
// 2002 static void RpSetAdrfRegConverion( uint8_t convert )
// 2003 {
_RpSetAdrfRegConverion:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 4
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
// 2004 	uint8_t adfCon;
// 2005 
// 2006 	adfCon = (RpRegRead(BBADFCON) & (~(PANCORD2 | FLMPEND2)));
        MOVW      AX, #0x868         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        AND       A, #0xFC           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2007 	if (convert)
        MOV       A, [SP+0x03]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        BZ        ??RpLog_Event_172  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
// 2008 	{
// 2009 		RpRegBlockWrite(BBPANID0, (uint8_t RP_FAR *)&RpCb.pib.macPanId2, sizeof(uint16_t));
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, #LWRD(_RpRxBuf+3398)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, #0xA0           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2010 		RpRegBlockWrite(BBSHORTAD0, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr2, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, #LWRD(_RpRxBuf+3396)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, #0xB0           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2011 		RpRegBlockWrite(BBEXTENDAD00, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress2, sizeof(uint32_t[2]));
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(_RpRxBuf+3400)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, #0xC0           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2012 		if (RpCb.pib.macPanCoord2)
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+8
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3408   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3558)  ;; 1 cycle
        BZ        ??RpLog_Event_173  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
// 2013 		{
// 2014 			RpCb.reg.bbTxRxMode3 |= PANCORD;
        SET1      ES:[HL].1          ;; 3 cycles
        BR        S:??RpLog_Event_174  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2015 		}
// 2016 		else
// 2017 		{
// 2018 			RpCb.reg.bbTxRxMode3 &= ~PANCORD;
??RpLog_Event_173:
        CLR1      ES:[HL].1          ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2019 		}
// 2020 		if (RpCb.pib.macPendBit2)
??RpLog_Event_174:
        CMP0      ES:_RpRxBuf+3409   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3557)  ;; 1 cycle
        BZ        ??RpLog_Event_175  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2021 		{
// 2022 			RpCb.reg.bbTxRxMode2 |= FLMPEND;
        SET1      ES:[HL].1          ;; 3 cycles
        BR        R:??RpLog_Event_176  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2023 		}
// 2024 		else
// 2025 		{
// 2026 			RpCb.reg.bbTxRxMode2 &= ~FLMPEND;
??RpLog_Event_175:
        CLR1      ES:[HL].1          ;; 3 cycles
        BR        R:??RpLog_Event_176  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2027 		}
// 2028 	}
// 2029 	else
// 2030 	{
// 2031 		RpRegBlockWrite(BBPANID0, (uint8_t RP_FAR *)&RpCb.pib.macPanId1, sizeof(uint16_t));
??RpLog_Event_172:
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, #LWRD(_RpRxBuf+3382)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, #0xA0           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2032 		RpRegBlockWrite(BBSHORTAD0, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr1, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, #LWRD(_RpRxBuf+3380)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, #0xB0           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2033 		RpRegBlockWrite(BBEXTENDAD00, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress1, sizeof(uint32_t[2]));
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(_RpRxBuf+3384)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, #0xC0           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2034 		if (RpCb.pib.macPanCoord1)
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+8
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3392   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3558)  ;; 1 cycle
        BZ        ??RpLog_Event_177  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
// 2035 		{
// 2036 			RpCb.reg.bbTxRxMode3 |= PANCORD;
        SET1      ES:[HL].1          ;; 3 cycles
        BR        S:??RpLog_Event_178  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2037 		}
// 2038 		else
// 2039 		{
// 2040 			RpCb.reg.bbTxRxMode3 &= ~PANCORD;
??RpLog_Event_177:
        CLR1      ES:[HL].1          ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2041 		}
// 2042 		if (RpCb.pib.macPendBit1)
??RpLog_Event_178:
        CMP0      ES:_RpRxBuf+3393   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3557)  ;; 1 cycle
        BZ        ??RpLog_Event_179  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2043 		{
// 2044 			RpCb.reg.bbTxRxMode2 |= FLMPEND;
        SET1      ES:[HL].1          ;; 3 cycles
        BR        S:??RpLog_Event_180  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2045 		}
// 2046 		else
// 2047 		{
// 2048 			RpCb.reg.bbTxRxMode2 &= ~FLMPEND;
??RpLog_Event_179:
        CLR1      ES:[HL].1          ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2049 		}
// 2050 		RpRegBlockWrite(BBPANID1, (uint8_t RP_FAR *)&RpCb.pib.macPanId2, sizeof(uint16_t));
??RpLog_Event_180:
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, #LWRD(_RpRxBuf+3398)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x700         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2051 		RpRegBlockWrite(BBSHORTAD1, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr2, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, #LWRD(_RpRxBuf+3396)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x710         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2052 		RpRegBlockWrite(BBEXTENDAD10, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress2, sizeof(uint32_t[2]));
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, #LWRD(_RpRxBuf+3400)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x720         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2053 		if (RpCb.pib.macAddressFilter2Ena == RP_TRUE)
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+8
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3394, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_176  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
// 2054 		{
// 2055 			if (RpCb.pib.macPanCoord2)
        CMP0      ES:_RpRxBuf+3408   ;; 2 cycles
        BZ        ??RpLog_Event_181  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2056 			{
// 2057 				adfCon |= PANCORD2;
        MOV       A, [SP]            ;; 1 cycle
        OR        A, #0x1            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 2058 			}
// 2059 			if (RpCb.pib.macPendBit2)
??RpLog_Event_181:
        CMP0      ES:_RpRxBuf+3409   ;; 2 cycles
        BZ        ??RpLog_Event_176  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2060 			{
// 2061 				adfCon |= FLMPEND2;
        MOV       A, [SP]            ;; 1 cycle
        OR        A, #0x2            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 2062 			}
// 2063 		}
// 2064 	}
// 2065 	RpRegWrite(BBTXRXMODE2, (uint8_t)(RpCb.reg.bbTxRxMode2));
??RpLog_Event_176:
        MOV       C, ES:_RpRxBuf+3557  ;; 2 cycles
        MOVW      AX, #0x48          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2066 	RpRegWrite(BBTXRXMODE3, (uint8_t)(RpCb.reg.bbTxRxMode3));
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3558  ;; 2 cycles
        MOVW      AX, #0x50          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2067 	RpRegWrite(BBADFCON, adfCon);
        MOV       A, [SP]            ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x868         ;; 1 cycle
        BR        F:?Subroutine5     ;; 3 cycles
          CFI EndBlock cfiBlock28
        ; ------------------------------------- Block: 19 cycles
        ; ------------------------------------- Total: 201 cycles
// 2068 }
// 2069 
// 2070 /******************************************************************************
// 2071 Function Name:       RpAvailableRcvRamEnable
// 2072 Parameters:          none:
// 2073 Return value:        none:
// 2074 Description:         receive bank(s) enable
// 2075 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock29 Using cfiCommon1
          CFI Function _RpAvailableRcvRamEnable
        CODE
// 2076 void
// 2077 RpAvailableRcvRamEnable(void)
// 2078 {
_RpAvailableRcvRamEnable:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 2079 	uint8_t rcvStRst;
// 2080 
// 2081 	RpRegWrite(BBTXRXST0, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x38          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2082 	RpCb.rx.unreadRamPrev = RP_FALSE;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3347   ;; 2 cycles
// 2083 	rcvStRst = (uint8_t)((RpRegRead(BBTXRXST1) & RCVSTOREST) ? RP_TRUE : RP_FALSE);
        MOVW      AX, #0x58          ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        BT        [HL].1, ??RpLog_Event_182  ;; 5 cycles
        ; ------------------------------------- Block: 20 cycles
// 2084 	// Select Privious RcvDataBank
// 2085 	if (rcvStRst == RP_FALSE)
// 2086 	{
// 2087 		RpCb.reg.bbTxRxMode3 &= ~RCVSTOREBANKSEL;
        MOVW      HL, #LWRD(_RpRxBuf+3558)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:[HL].4          ;; 3 cycles
        BR        S:??RpLog_Event_183  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 2088 	}
// 2089 	else
// 2090 	{
// 2091 		RpCb.reg.bbTxRxMode3 |= RCVSTOREBANKSEL;
??RpLog_Event_182:
        MOVW      HL, #LWRD(_RpRxBuf+3558)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2092 	}
// 2093 	RpRegWrite(BBTXRXMODE3, RpCb.reg.bbTxRxMode3);
??RpLog_Event_183:
        MOV       C, ES:_RpRxBuf+3558  ;; 2 cycles
        MOVW      AX, #0x50          ;; 1 cycle
          CFI EndBlock cfiBlock29
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 36 cycles
        REQUIRE ?Subroutine7
        ; // Fall through to label ?Subroutine7
// 2094 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock30 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+6
          CFI FunCall _RpAvailableRcvRamEnable _RpRegWrite
          CFI FunCall _RpSetSpecificModeVal _RpRegWrite
        CODE
?Subroutine7:
        CALL      F:_RpRegWrite      ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock30
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 2095 
// 2096 /******************************************************************************
// 2097 Function Name:       RpCurStatCheck
// 2098 Parameters:          status:current status
// 2099 Return value:        RP_BUSY_LOWPOWER:Now Lowpower mode
// 2100 					 RP_BUSY_RX:No transmit or ED or CCA setting because now RX_ON or CCA or ED
// 2101 					 RP_BUSY_TX:No transmit or ED or CCA setting because now TX_ON
// 2102 Description:         Current status check.
// 2103 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock31 Using cfiCommon1
          CFI Function _RpCurStatCheck
          CFI NoCalls
        CODE
// 2104 static int16_t RpCurStatCheck( uint16_t status )
// 2105 {
_RpCurStatCheck:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 2
// 2106 	int16_t rtnVal;
// 2107 
// 2108 	if (status & RP_PHY_STAT_LOWPOWER)
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].5, ??RpLog_Event_184  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 2109 	{
// 2110 		rtnVal = RP_BUSY_LOWPOWER;
        MOVW      AX, #0xF3          ;; 1 cycle
        BR        S:??RpLog_Event_185  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2111 	}
// 2112 	else if (status & RP_PHY_STAT_TX)
??RpLog_Event_184:
        DECW      HL                 ;; 1 cycle
        BF        [HL].1, ??RpLog_Event_186  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 2113 	{
// 2114 		rtnVal = RP_BUSY_TX;
        MOVW      AX, #0x2           ;; 1 cycle
        BR        S:??RpLog_Event_185  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2115 	}
// 2116 	else
// 2117 	{
// 2118 		// CCA or ED or RX
// 2119 		rtnVal =  RP_BUSY_RX;
??RpLog_Event_186:
        ONEW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2120 	}
// 2121 
// 2122 	return (rtnVal);
??RpLog_Event_185:
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock31
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 30 cycles
// 2123 }
// 2124 
// 2125 /******************************************************************************
// 2126 Function Name:       RpChkTmoutTrxStateOff
// 2127 Parameters:          stat:none
// 2128 Return value:        RP_ERR_HARDWARE:Fatal Error has occured
// 2129 					 RP_SUCCESS:phy_status has be RP_TRX_OFF successfully.
// 2130 					 RP_PENDING:need to RP_TRX_OFF again because it must be Auto Rx
// 2131 Description:         tmout_trx_state_off.
// 2132 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock32 Using cfiCommon1
          CFI Function _RpChkTmoutTrxStateOff
        CODE
// 2133 static void RpChkTmoutTrxStateOff( void )
// 2134 {
_RpChkTmoutTrxStateOff:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 2135 	uint32_t tmoutCnt;
// 2136 	uint16_t status;
// 2137 
// 2138 	#if defined(__RX)
// 2139 	uint32_t bkupPsw;
// 2140 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 2141 	uint8_t  bkupPsw;
// 2142 	#endif
// 2143 
// 2144 	status = RpCb.status;
// 2145 	if (status & RP_PHY_STAT_TX)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        BF        A.1, ??RpLog_Event_187  ;; 5 cycles
        ; ------------------------------------- Block: 10 cycles
// 2146 	{
// 2147 		tmoutCnt =  RP_TMOUT_CNT_WHEN_SET_TRX_STATE_OFF_FOR_TX;
        MOVW      AX, #0x8480        ;; 1 cycle
        MOVW      BC, #0x1E          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpChkTmoutTrxStateOff_0:
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 3 cycles
// 2148 	}
// 2149 	else
// 2150 	{
// 2151 		tmoutCnt =  RP_TMOUT_CNT_WHEN_SET_TRX_STATE_OFF;
// 2152 	}
// 2153 	while (1)
// 2154 	{
// 2155 		#if defined(__RX) || defined(__CCRL__) || defined(__ICCRL78__)
// 2156 		RP_PHY_EI_INV(bkupPsw);	/* Enable interrupt */
??RpChkTmoutTrxStateOff_1:
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x2            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 2157 		RP_PHY_DI_INV(bkupPsw);	/* Disable interrupt */
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 2158 		#else
// 2159 		RP_PHY_EI_INV();		/* Enable interrupt */
// 2160 		RP_PHY_DI_INV();		/* Disable interrupt */
// 2161 		#endif
// 2162 
// 2163 		status = RpCb.status;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 2164 		if (status & RP_PHY_STAT_TRX_OFF)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].4, ??RpLog_Event_188  ;; 5 cycles
        ; ------------------------------------- Block: 32 cycles
// 2165 		{
// 2166 			RpSetStateRxOnToTrxOff();
// 2167 			break;
// 2168 		}
// 2169 		else if ((status & RP_PHY_STAT_RX) && RpRxOffBeforeReplyingAck() == RP_TRUE)
        BF        [HL].0, ??RpLog_Event_189  ;; 5 cycles
          CFI FunCall _RpRxOffBeforeReplyingAck
        ; ------------------------------------- Block: 5 cycles
        CALL      F:_RpRxOffBeforeReplyingAck  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_188  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 2170 		{
// 2171 			RpSetStateRxOnToTrxOff();
// 2172 			break;
// 2173 		}
// 2174 		else if ((status & RP_PHY_STAT_TX) && (RP_PHY_STAT_TX_BUSY() == 0))
??RpLog_Event_189:
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].1, ??RpLog_Event_190  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      AX, #0x330         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].1, ??RpLog_Event_188  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
// 2175 		{
// 2176 			RpSetStateRxOnToTrxOff();
// 2177 			break;
// 2178 		}
// 2179 		else
// 2180 		{
// 2181 			RpWait4us();
// 2182 			tmoutCnt -= 4;
??RpLog_Event_190:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        SUBW      AX, #0x4           ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2183 			if (!tmoutCnt)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??RpChkTmoutTrxStateOff_1  ;; 4 cycles
          CFI FunCall _RpSetStateRxOnToTrxOff
        ; ------------------------------------- Block: 20 cycles
// 2184 			{
// 2185 				RpSetStateRxOnToTrxOff();
??RpLog_Event_188:
        CALL      F:_RpSetStateRxOnToTrxOff  ;; 3 cycles
// 2186 				break;
// 2187 			}
// 2188 		}
// 2189 	}
// 2190 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+10
        ; ------------------------------------- Block: 10 cycles
??RpLog_Event_187:
        MOVW      AX, #0x1388        ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        BR        S:??RpChkTmoutTrxStateOff_0  ;; 3 cycles
          CFI EndBlock cfiBlock32
        ; ------------------------------------- Block: 5 cycles
        ; ------------------------------------- Total: 114 cycles
// 2191 
// 2192 /******************************************************************************
// 2193 Function Name:       RpSetStateRxOn
// 2194 Parameters:          options:RX_ON options
// 2195 					 time:RX_ON start or end time
// 2196 Return value:        none
// 2197 Description:         set to RX_ON
// 2198  *		this function can be called in only TRX_OFF state
// 2199  *      time: lower 16bits are valid
// 2200 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock33 Using cfiCommon1
          CFI Function _RpSetStateRxOn
        CODE
// 2201 static int16_t RpSetStateRxOn( uint8_t options, uint32_t time )
// 2202 {
_RpSetStateRxOn:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 16
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+20
// 2203 	uint32_t futureTime;
// 2204 
// 2205 	#if defined(__RX)
// 2206 	uint32_t bkupPsw;
// 2207 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 2208 	uint8_t  bkupPsw;
// 2209 	#elif defined(__arm)
// 2210 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 2211 	#endif
// 2212 
// 2213 	RpCb.rx.pTopRxBuf = RpGetRxBuf();
          CFI FunCall _RpGetRxBuf
        CALL      F:_RpGetRxBuf      ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3314)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 2214 	if (RpCb.rx.pTopRxBuf == RP_NULL)
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_191  ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_191:
        BNZ       ??RpLog_Event_192  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2215 	{
// 2216 		/* Callback function execution Log */
// 2217 		RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
// 2218 		/* Callback function execution */
// 2219 		INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );
        MOVW      HL, #LWRD(_RpRxBuf+3514)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x21           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
// 2220 
// 2221 		RpRfStat.failToGetRxBuf++;
        MOVW      HL, #LWRD(_RpRfStat+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRfStat)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2222 		return( RP_NOT_GET_RXBUF );
        MOVW      AX, #0xF9          ;; 1 cycle
        BR        R:??RpLog_Event_193  ;; 3 cycles
        ; ------------------------------------- Block: 34 cycles
// 2223 	}
// 2224 	RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
??RpLog_Event_192:
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3312, AX  ;; 2 cycles
// 2225 	RpCb.status = RP_PHY_STAT_RX;
        ONEW      AX                 ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 2226 	RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;
        CLRB      ES:_RpRxBuf+3204   ;; 2 cycles
// 2227 
// 2228 	RpCb.rx.onReplyingAck = RP_FALSE;
        CLRB      ES:_RpRxBuf+3344   ;; 2 cycles
// 2229 	RpCb.tx.onCsmaCa = RP_FALSE;
        CLRB      ES:_RpRxBuf+3288   ;; 2 cycles
// 2230 	RpCb.reg.bbTxRxMode0 &= (uint8_t)(~(AUTORCV0 | AUTORCV1 | BEACON));
        MOV       A, ES:_RpRxBuf+3555  ;; 2 cycles
        AND       A, #0xA7           ;; 1 cycle
        MOV       ES:_RpRxBuf+3555, A  ;; 2 cycles
// 2231 
// 2232 	RpChangeFilter();
          CFI FunCall _RpChangeFilter
        CALL      F:_RpChangeFilter  ;; 3 cycles
// 2233 
// 2234 	if (RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3428, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_194  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
// 2235 	{
// 2236 		RpSetCcaDurationVal(RP_FALSE);
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetCcaDurationVal
        CALL      F:_RpSetCcaDurationVal  ;; 3 cycles
          CFI FunCall _RpAntSelAssist_StartProc
        ; ------------------------------------- Block: 4 cycles
// 2237 	}
// 2238 
// 2239 	/* Start of antenna selection assistance process */
// 2240 	RpAntSelAssist_StartProc();
??RpLog_Event_194:
        CALL      F:_RpAntSelAssist_StartProc  ;; 3 cycles
// 2241 
// 2242 	RpInverseTxAnt(RP_TRUE);
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpInverseTxAnt
        CALL      F:_RpInverseTxAnt  ;; 3 cycles
// 2243 
// 2244 	// clear autorcvTx->Rx, clear autorcvRx->Rx, clear autoack reply, clear slotted, clear intrapan enable
// 2245 	if (options & RP_RX_AUTO_RCV)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xB           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].7, ??RpLog_Event_195  ;; 5 cycles
        ; ------------------------------------- Block: 15 cycles
// 2246 	{
// 2247 		RpCb.status |= RP_PHY_STAT_TRX_TO_RX_AUTO;
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2248 	}
// 2249 
// 2250 	RpCb.reg.bbTxRxMode1 |= CCASEL;	// RSSI
??RpLog_Event_195:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3556.0  ;; 3 cycles
// 2251 	RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
        MOV       C, ES:_RpRxBuf+3556  ;; 2 cycles
        MOVW      AX, #0x18          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2252 	RpAddressFilterSetting((uint8_t)(RpCb.pib.macAddressFilter1Ena | RpCb.pib.macAddressFilter2Ena));
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3378  ;; 2 cycles
        OR        A, ES:_RpRxBuf+3394  ;; 2 cycles
          CFI FunCall _RpAddressFilterSetting
        CALL      F:_RpAddressFilterSetting  ;; 3 cycles
// 2253 	if (((RpCb.pib.macAddressFilter1Ena == RP_TRUE) || (RpCb.pib.macAddressFilter2Ena == RP_TRUE))
// 2254 			&& (RpCb.rx.softwareAdf == RP_FALSE))
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3378, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_196  ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
        CMP       ES:_RpRxBuf+3394, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_197  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpLog_Event_196:
        CMP0      ES:_RpRxBuf+3345   ;; 2 cycles
        BNZ       ??RpLog_Event_197  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2255 	{
// 2256 		if (options & RP_RX_SLOTTED)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xB           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].3, ??RpLog_Event_197  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
// 2257 		{
// 2258 			RpCb.status |= RP_PHY_STAT_SLOTTED;
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].3          ;; 3 cycles
// 2259 			RpCb.reg.bbTxRxMode0 |= BEACON;
        SET1      ES:_RpRxBuf+3555.6  ;; 3 cycles
// 2260 			RpRegWrite(BBTXRXMODE0, (uint8_t)(RpCb.reg.bbTxRxMode0));
        MOV       C, ES:_RpRxBuf+3555  ;; 2 cycles
        MOVW      AX, #0x10          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 14 cycles
// 2261 		}
// 2262 	}
// 2263 
// 2264 	RpSetSfdDetectionExtendWrite(RpCb.status);
??RpLog_Event_197:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
          CFI FunCall _RpSetSfdDetectionExtendWrite
        CALL      F:_RpSetSfdDetectionExtendWrite  ;; 3 cycles
// 2265 
// 2266 	if (options & RP_RX_TIME)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xB           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, $+7        ;; 5 cycles
        BR        F:??RpLog_Event_198  ;; 5 cycles
          CFI FunCall _RpGetTime
        ; ------------------------------------- Block: 14 cycles
// 2267 	{
// 2268 		// Timer Compare Reset
// 2269 		futureTime = RpGetTime() - 1;
        CALL      F:_RpGetTime       ;; 3 cycles
        SUBW      AX, #0x1           ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2270 		RpRegBlockWrite(BBTCOMP0REG0, (uint8_t RP_FAR *)&futureTime, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x120         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2271 		RpReadIrq();
        POP       AX                 ;; 1 cycle
          CFI CFA SP+20
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x1B0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 2272 
// 2273 		// modify receive time for RF IC warmup
// 2274 		time -= RP_PHY_RX_WARM_UP_TIME; // warmup time
// 2275 		time &= RP_TIME_MASK;
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3434  ;; 2 cycles
        MOVW      BC, #0xF0          ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
// 2276 		// RX with timer trigger
// 2277 		RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;
        SET1      ES:_RpRxBuf+3202.6  ;; 3 cycles
// 2278 		// set TC0 for RX
// 2279 		RpRegBlockWrite(BBTCOMP0REG0, (uint8_t RP_FAR *)&time, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x120         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2280 		RpCb.reg.bbIntEn[0] |= TIM0INTEN;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3546.0  ;; 3 cycles
// 2281 
// 2282 		/* Disable interrupt */
// 2283 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2284 		RP_PHY_ALL_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
// 2285 		#else
// 2286 		RP_PHY_ALL_DI();
// 2287 		#endif
// 2288 
// 2289 		RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3546  ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2290 
// 2291 		if (RpChkRxTriggerTimer(time) == RP_TRUE)
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
          CFI FunCall _RpChkRxTriggerTimer
        CALL      F:_RpChkRxTriggerTimer  ;; 3 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        CMP       A, #0x1            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_199  ;; 4 cycles
        ; ------------------------------------- Block: 136 cycles
// 2292 		{
// 2293 			/* Enable interrupt */
// 2294 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2295 			RP_PHY_ALL_EI(bkupPsw);
// 2296 			#else
// 2297 			RP_PHY_ALL_EI();
// 2298 			#endif
// 2299 
// 2300 			return( RP_INVALID_SET_TIME );
// 2301 		}
// 2302 	}
// 2303 	else
// 2304 	{
// 2305 		RpAvailableRcvRamEnable();
// 2306 
// 2307 		/* Disable interrupt */
// 2308 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2309 		RP_PHY_ALL_DI(bkupPsw);
// 2310 		#else
// 2311 		RP_PHY_ALL_DI();
// 2312 		#endif
// 2313 
// 2314 		RpRxOnStart();	// RX trigger
// 2315 
// 2316 		if (options & RP_RX_TMOUT)
// 2317 		{
// 2318 			// Timer Compare Reset
// 2319 			futureTime = RpGetTime() - 1;
// 2320 			RpRegBlockWrite(BBTCOMP1REG0, (uint8_t RP_FAR *)&futureTime, sizeof(uint32_t));
// 2321 			RpReadIrq();
// 2322 
// 2323 			// RX with timeout
// 2324 			// set TC1
// 2325 			RpCb.status |= RP_PHY_STAT_RX_TMOUT;
// 2326 			RpCb.rx.timeout = time;
// 2327 			RpRegBlockWrite(BBTCOMP1REG0, (uint8_t RP_FAR *)&time, sizeof(uint32_t));
// 2328 			RpCb.reg.bbIntEn[0] |= TIM1INTEN;
// 2329 			RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
// 2330 			if (RpChkRxTriggerTimer(time) == RP_TRUE)
// 2331 			{
// 2332 				/* Enable interrupt */
// 2333 				#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2334 				RP_PHY_ALL_EI(bkupPsw);
// 2335 				#else
// 2336 				RP_PHY_ALL_EI();
// 2337 				#endif
// 2338 
// 2339 				return( RP_INVALID_SET_TIME );
// 2340 			}
// 2341 		}
// 2342 	}
// 2343 
// 2344 	/* Enable interrupt */
// 2345 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2346 	RP_PHY_ALL_EI(bkupPsw);
??RpSetStateRxOn_0:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 2347 	#else
// 2348 	RP_PHY_ALL_EI();
// 2349 	#endif
// 2350 
// 2351 	return (RP_SUCCESS);
        MOVW      AX, #0x7           ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_193:
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+20
          CFI FunCall _RpAvailableRcvRamEnable
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_198:
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xB           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].5, ??RpSetStateRxOn_0  ;; 5 cycles
          CFI FunCall _RpGetTime
        ; ------------------------------------- Block: 32 cycles
        CALL      F:_RpGetTime       ;; 3 cycles
        SUBW      AX, #0x1           ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x140         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+20
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x1B0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3202)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SET1      ES:[HL].2          ;; 3 cycles
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3340)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x140         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3546.1  ;; 3 cycles
        MOV       C, ES:_RpRxBuf+3546  ;; 2 cycles
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
          CFI FunCall _RpChkRxTriggerTimer
        CALL      F:_RpChkRxTriggerTimer  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
        CMP       A, #0x1            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetStateRxOn_0  ;; 4 cycles
        ; ------------------------------------- Block: 85 cycles
??RpLog_Event_199:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        MOVW      AX, #0xFA          ;; 1 cycle
        BR        R:??RpLog_Event_193  ;; 3 cycles
          CFI EndBlock cfiBlock33
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 472 cycles
// 2352 }
// 2353 
// 2354 /******************************************************************************
// 2355 Function Name:       RpChkRxTriggerTimer
// 2356 Parameters:          time:RX_ON start or end time
// 2357 Return value:        none
// 2358 Description:         Check Rx Trigger Timer
// 2359 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock34 Using cfiCommon3
          CFI Function _RpChkRxTriggerTimer
        CODE
// 2360 static uint8_t RpChkRxTriggerTimer( uint32_t time )
// 2361 {
_RpChkRxTriggerTimer:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 2362 	uint8_t rtn = RP_FALSE;
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2363 	uint32_t nowTime, difTime;
// 2364 
// 2365 	nowTime = RpGetTime();
// 2366 	difTime = (time - nowTime) & RP_TIME_MASK;
          CFI FunCall _RpGetTime
        CALL      F:_RpGetTime       ;; 3 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, AX             ;; 1 cycle
// 2367 	if ((difTime > RP_TIME_LIMIT) || (difTime == 0))
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 30 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpChkRxTriggerTimer_0:
        BNC       ??RpLog_Event_200  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        OR        A, E               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        OR        A, H               ;; 1 cycle
        BNZ       ??RpLog_Event_201  ;; 4 cycles
          CFI FunCall _RpCheckRfIRQ
        ; ------------------------------------- Block: 7 cycles
// 2368 	{
// 2369 		if (RpCheckRfIRQ() == RP_FALSE)
??RpLog_Event_200:
        CALL      F:_RpCheckRfIRQ    ;; 3 cycles
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_201  ;; 4 cycles
          CFI FunCall _RpSetStateRxOnToTrxOff
        ; ------------------------------------- Block: 8 cycles
// 2370 		{
// 2371 			RpSetStateRxOnToTrxOff();
        CALL      F:_RpSetStateRxOnToTrxOff  ;; 3 cycles
// 2372 
// 2373 			rtn = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 2374 		}
// 2375 	}
// 2376 
// 2377 	return (rtn);
??RpLog_Event_201:
        BR        F:?Subroutine9     ;; 3 cycles
          CFI EndBlock cfiBlock34
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 58 cycles
// 2378 }
// 2379 
// 2380 /******************************************************************************
// 2381 Function Name:       RpRxOnStart
// 2382 Parameters:          none
// 2383 Return value:        none;
// 2384 Description:         start RX
// 2385 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock35 Using cfiCommon1
          CFI Function _RpRxOnStart
          CFI NoCalls
        CODE
// 2386 void
// 2387 RpRxOnStart(void)
// 2388 {
_RpRxOnStart:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 2389 	RpRegWrite(BBTXRXCON, RCVTRG);	//receive trigger enable
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x60          ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock35
        ; ------------------------------------- Block: 5 cycles
        ; ------------------------------------- Total: 5 cycles
// 2390 }
// 2391 
// 2392 /******************************************************************************
// 2393 Function Name:       RpSetStateRxOnToTrxOff
// 2394 Parameters:          none
// 2395 Return value:        none
// 2396 Description:         set to RX_OFF(RX_ON -> TRX_OFF)
// 2397  *		this function can be called in only RX_ON state
// 2398 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock36 Using cfiCommon1
          CFI Function _RpSetStateRxOnToTrxOff
        CODE
// 2399 void
// 2400 RpSetStateRxOnToTrxOff(void)
// 2401 {
_RpSetStateRxOnToTrxOff:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 2402 	uint16_t status;
// 2403 
// 2404 	status = RpCb.status;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 2405 
// 2406 	RpRxOnStop();							// with rfstop_bbtxrxrst = 1;
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x8           ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOVW      AX, #0x10          ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 2407 	RpClrAutoRxFunc();						// clear auto function
          CFI FunCall _RpClrAutoRxFunc
        CALL      F:_RpClrAutoRxFunc  ;; 3 cycles
// 2408 
// 2409 	RpCb.reg.bbIntEn[0] &= (uint8_t)(~(TIM0INTEN | TIM1INTEN | TIM2INTEN | TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN));
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3546  ;; 2 cycles
        AND       A, #0x40           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3546, A  ;; 2 cycles
// 2410 	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
        MOVW      AX, #0x1C8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2411 
// 2412 	/* Stop Antenna selection assistance */
// 2413 	RpAntSelAssist_StopProc( status );
        MOVW      AX, [SP]           ;; 1 cycle
          CFI FunCall _RpAntSelAssist_StopProc
        CALL      F:_RpAntSelAssist_StopProc  ;; 3 cycles
// 2414 
// 2415 	RpCb.reg.bbTimeCon &= ~COMP0TRG;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:_RpRxBuf+3554.1  ;; 3 cycles
// 2416 	RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOV       C, ES:_RpRxBuf+3554  ;; 2 cycles
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2417 	RpRelRxBuf(RpCb.rx.pTopRxBuf);
        MOVW      HL, #LWRD(_RpRxBuf+3314)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _RpRelRxBuf
        CALL      F:_RpRelRxBuf      ;; 3 cycles
// 2418 
// 2419 	RpCb.rx.waitRelRxBufWhenAutoRx = RP_FALSE;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3346   ;; 2 cycles
// 2420 	RpCb.rx.onReplyingAck = RP_FALSE;
        CLRB      ES:_RpRxBuf+3344   ;; 2 cycles
// 2421 	RpCb.tx.onCsmaCa = RP_FALSE;
        CLRB      ES:_RpRxBuf+3288   ;; 2 cycles
// 2422 }
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock36
        ; ------------------------------------- Block: 68 cycles
        ; ------------------------------------- Total: 68 cycles
// 2423 
// 2424 /******************************************************************************
// 2425 Function Name:       RpRxOnStop
// 2426 Parameters:          rfstop:true means set to rfstop_bbtxrxrst = 1;
// 2427 Return value:        none;
// 2428 Description:         pause RX
// 2429 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock37 Using cfiCommon1
          CFI Function _RpRxOnStop
        CODE
// 2430 void
// 2431 RpRxOnStop(void)
// 2432 {
_RpRxOnStop:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 2433 	RpRegWrite(BBTXRXRST, RFSTOP);		// RF Stop
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x8           ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2434 	RpCb.status = RP_PHY_STAT_TRX_OFF;
        MOVW      AX, #0x10          ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3202, AX  ;; 2 cycles
// 2435 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock37
        ; ------------------------------------- Block: 15 cycles
        ; ------------------------------------- Total: 15 cycles
// 2436 
// 2437 /******************************************************************************
// 2438 Function Name:       RpClrAutoRxFunc
// 2439 Parameters:          none
// 2440 Return value:        none
// 2441 Description:         clear auto Rx function of H/W
// 2442 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock38 Using cfiCommon1
          CFI Function _RpClrAutoRxFunc
        CODE
// 2443 static void RpClrAutoRxFunc( void )
// 2444 {
_RpClrAutoRxFunc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 2445 	RpCb.reg.bbTxRxMode0 &= (uint8_t)(~(AUTORCV0 | AUTORCV1 | BEACON));
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3555  ;; 2 cycles
        AND       A, #0xA7           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3555, A  ;; 2 cycles
// 2446 	// clear autorcvTx->Rx, clear autorcvRx->Rx, clear autoack reply, clear slotted, clear intrapan enable
// 2447 	RpRegWrite(BBTXRXMODE0, (uint8_t)(RpCb.reg.bbTxRxMode0));
        MOVW      AX, #0x10          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2448 	RpReadIrq();
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x1B0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 2449 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock38
        ; ------------------------------------- Block: 32 cycles
        ; ------------------------------------- Total: 32 cycles
// 2450 
// 2451 /******************************************************************************
// 2452 Function Name:       RpReadIrq
// 2453 Parameters:          none
// 2454 Return value:        irq read
// 2455 Description:         Read IRQs
// 2456 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock39 Using cfiCommon3
          CFI Function _RpReadIrq
        CODE
// 2457 uint32_t
// 2458 RpReadIrq(void)
// 2459 {
_RpReadIrq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 2460 	uint32_t	ireq = RP_BB_NO_IREQ;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 2461 
// 2462 	RpRegBlockRead(BBINTREQ0, (uint8_t *)&ireq, 3);
        MOV       X, #0x3            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x1B0         ;; 1 cycle
          CFI FunCall _RpRegBlockRead
        CALL      F:_RpRegBlockRead  ;; 3 cycles
// 2463 #ifdef RP_LOG_IRQ
// 2464 	RpLogRam[RpLogRamCnt] = ireq;
// 2465 	if (RpCb.rx.onReplyingAck == RP_TRUE)
// 2466 	{
// 2467 		RpLogRam[RpLogRamCnt] |= 0x80000000;
// 2468 	}
// 2469 
// 2470 	if (RpCb.tx.onCsmaCa == RP_TRUE)
// 2471 	{
// 2472 		RpLogRam[RpLogRamCnt] |= 0x40000000;
// 2473 	}
// 2474 
// 2475 	if ((ireq & (RP_BBRCVFIN_IREQ | RP_BBTRNFIN_IREQ)) == (RP_BBRCVFIN_IREQ | RP_BBTRNFIN_IREQ))
// 2476 	{
// 2477 		RpRfStat.txRamUnderrun++;
// 2478 	}
// 2479 	RpLogRamCnt++;
// 2480 
// 2481 	if ( RP_LOG_NUMBER <= RpLogRamCnt )
// 2482 	{
// 2483 		RpLogRamCnt = 0;
// 2484 	}
// 2485 #endif
// 2486 
// 2487 	return (ireq);
        BR        F:?Subroutine3     ;; 3 cycles
          CFI EndBlock cfiBlock39
        ; ------------------------------------- Block: 17 cycles
        ; ------------------------------------- Total: 17 cycles
// 2488 }
// 2489 
// 2490 /******************************************************************************
// 2491 Function Name:       RpAvailableRcvOnCsmaca
// 2492 Parameters:          none
// 2493 Return value:        none
// 2494 Description:         Rx on csmaca of H/W
// 2495 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock40 Using cfiCommon1
          CFI Function _RpAvailableRcvOnCsmaca
        CODE
// 2496 static int16_t RpAvailableRcvOnCsmaca( void )
// 2497 {
_RpAvailableRcvOnCsmaca:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 2498 	int16_t rtn = RP_SUCCESS;
        MOVW      AX, #0x7           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 2499 
// 2500 	RpCb.rx.pTopRxBuf  = RpGetRxBuf();
          CFI FunCall _RpGetRxBuf
        CALL      F:_RpGetRxBuf      ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3314)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 2501 	if (RpCb.rx.pTopRxBuf == RP_NULL)
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_202  ;; 4 cycles
        ; ------------------------------------- Block: 30 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_202:
        BNZ       ??RpLog_Event_203  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2502 	{
// 2503 		/* Callback function execution Log */
// 2504 		RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
// 2505 		/* Callback function execution */
// 2506 		INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );
        MOVW      HL, #LWRD(_RpRxBuf+3514)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x21           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
// 2507 
// 2508 		RpRfStat.failToGetRxBuf++;
        MOVW      HL, #LWRD(_RpRfStat+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRfStat)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2509 		rtn = RP_TRX_OFF;
        MOVW      AX, #0x8           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        S:??RpLog_Event_204  ;; 3 cycles
        ; ------------------------------------- Block: 35 cycles
// 2510 	}
// 2511 	else
// 2512 	{
// 2513 		RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
??RpLog_Event_203:
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3312, AX  ;; 2 cycles
// 2514 
// 2515 		RpAddressFilterSetting((uint8_t)(RpCb.pib.macAddressFilter1Ena | RpCb.pib.macAddressFilter2Ena));
        MOV       A, ES:_RpRxBuf+3378  ;; 2 cycles
        OR        A, ES:_RpRxBuf+3394  ;; 2 cycles
          CFI FunCall _RpAddressFilterSetting
        CALL      F:_RpAddressFilterSetting  ;; 3 cycles
// 2516 		RpCb.reg.bbTxRxMode1 |= CCASEL; // RSSI
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3556.0  ;; 3 cycles
// 2517 		RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
        MOV       C, ES:_RpRxBuf+3556  ;; 2 cycles
        MOVW      AX, #0x18          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 2518 		RpAvailableRcvRamEnable();
          CFI FunCall _RpAvailableRcvRamEnable
        CALL      F:_RpAvailableRcvRamEnable  ;; 3 cycles
        ; ------------------------------------- Block: 24 cycles
// 2519 	}
// 2520 
// 2521 	return rtn;
??RpLog_Event_204:
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock40
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 103 cycles
// 2522 }
// 2523 
// 2524 /******************************************************************************
// 2525 Function Name:       RpAddressFilterSetting
// 2526 Parameters:          addressFilterMode
// 2527 Return value:        none
// 2528 Description:         Rx on with addressfilter
// 2529 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock41 Using cfiCommon1
          CFI Function _RpAddressFilterSetting
          CFI NoCalls
        CODE
// 2530 void
// 2531 RpAddressFilterSetting(uint8_t addressFilterMode)
// 2532 {
_RpAddressFilterSetting:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 2533 	if ((addressFilterMode == RP_TRUE) && (RpCb.rx.softwareAdf == RP_FALSE))
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_205  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3345   ;; 2 cycles
        BNZ       ??RpLog_Event_205  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2534 	{
// 2535 		RpCb.reg.bbIntEn[2] &= ~FLINTEN;
        CLR1      ES:_RpRxBuf+3548.5  ;; 3 cycles
// 2536 		RpCb.reg.bbIntEn[1] |= ADRSINTEN;
        MOVW      HL, #LWRD(_RpRxBuf+3547)  ;; 1 cycle
        SET1      ES:[HL].4          ;; 3 cycles
        BR        S:??RpLog_Event_206  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
// 2537 	}
// 2538 	else
// 2539 	{
// 2540 		RpCb.reg.bbIntEn[1] &= ~ADRSINTEN;
??RpLog_Event_205:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:_RpRxBuf+3547.4  ;; 3 cycles
// 2541 		RpCb.reg.bbIntEn[2] |= FLINTEN;
        MOVW      HL, #LWRD(_RpRxBuf+3548)  ;; 1 cycle
        SET1      ES:[HL].5          ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 2542 	}
// 2543 
// 2544 	if (RpCb.rx.softwareAdf == RP_TRUE)
??RpLog_Event_206:
        CMP       ES:_RpRxBuf+3345, #0x1  ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3548)  ;; 1 cycle
        BNZ       ??RpLog_Event_207  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2545 	{
// 2546 		RpCb.reg.bbIntEn[2] |= RCVCUNTINTEN;
        SET1      ES:[HL].4          ;; 3 cycles
        BR        S:??RpLog_Event_208  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2547 	}
// 2548 	else
// 2549 	{
// 2550 		RpCb.reg.bbIntEn[2] &= ~RCVCUNTINTEN;
??RpLog_Event_207:
        CLR1      ES:[HL].4          ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2551 	}
// 2552 
// 2553 	RpRegBlockWrite(BBINTEN1,	(uint8_t RP_FAR *)(&RpCb.reg.bbIntEn[1]), 2);
??RpLog_Event_208:
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3547)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x1D0         ;; 1 cycle
        BR        F:?Subroutine8     ;; 3 cycles
          CFI EndBlock cfiBlock41
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 54 cycles
// 2554 }
// 2555 
// 2556 /******************************************************************************
// 2557 Function Name:       RpInitRfic
// 2558 Parameters:          none
// 2559 Return value:        none
// 2560 Description:         RF initialize.
// 2561 ******************************************************************************/
// 2562 static void RpInitRfic( void )
// 2563 {
// 2564 	RpWakeupSequence();
// 2565 	RpSetRegBeforeIdle();		// Modem Set
// 2566 	RpInitRfOnly();				// MAC Set
// 2567 }
// 2568 
// 2569 /******************************************************************************
// 2570 Function Name:      RpWrEvaReg1
// 2571 Parameters:          write data
// 2572 Return value:        none
// 2573 Description:         write evaregs.
// 2574 ******************************************************************************/
// 2575 static void RpWrEvaReg1( uint16_t aData )
// 2576 {
// 2577 	aData &= (~0x8000);	// bit15 = 0 write for RFINI00 access
// 2578 	RpRegBlockWrite(RFINI00, (uint8_t RP_FAR *)&aData, sizeof(uint16_t));
// 2579 }
// 2580 
// 2581 /******************************************************************************
// 2582 Function Name:       RpWrEvaReg2
// 2583 Parameters:          write data
// 2584 Return value:        none
// 2585 Description:         write evaregs.
// 2586 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock42 Using cfiCommon1
          CFI Function _RpWrEvaReg2
          CFI NoCalls
        CODE
// 2587 void
// 2588 RpWrEvaReg2(uint16_t aData)
// 2589 {
_RpWrEvaReg2:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 2
// 2590 	aData &= (~0x8000);	// bit15 = 0 write for RFINI10 access
        AND       A, #0x7F           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 2591 	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&aData, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
        BR        F:?Subroutine6     ;; 3 cycles
          CFI EndBlock cfiBlock42
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 13 cycles
// 2592 }
// 2593 
// 2594 /******************************************************************************
// 2595 Function Name:       RpRdEvaReg2
// 2596 Parameters:          write data
// 2597 Return value:        none
// 2598 Description:         read evaregs.
// 2599 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock43 Using cfiCommon1
          CFI Function _RpRdEvaReg2
        CODE
// 2600 uint8_t
// 2601 RpRdEvaReg2(uint16_t aData)
// 2602 {
_RpRdEvaReg2:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 2
// 2603 	aData |= (0x8000);// bit15 = 1 read for RFINI10 access
        OR        A, #0x80           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 2604 	RpRegBlockWrite(RFINI10, (uint8_t RP_FAR *)&aData, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2605 
// 2606 	return (RpRegRead(RFINI12));
        MOVW      AX, #0x6F0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock43
        ; ------------------------------------- Block: 24 cycles
        ; ------------------------------------- Total: 24 cycles
// 2607 }
// 2608 
// 2609 /******************************************************************************
// 2610 Function Name:       RpSetChannelVal
// 2611 Parameters:          channel number
// 2612 Return value:        set result
// 2613 Description:         set freq channel.
// 2614 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock44 Using cfiCommon1
          CFI Function _RpSetChannelVal
        CODE
// 2615 uint8_t RpSetChannelVal( uint8_t channel )
// 2616 {
_RpSetChannelVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOV       B, A               ;; 1 cycle
// 2617 	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3436  ;; 2 cycles
// 2618 	uint8_t rtn = 0xff;
        MOV       X, #0xFF           ;; 1 cycle
// 2619 
// 2620 	if (freqBandId == RP_PHY_FREQ_BAND_863MHz)
        CMP       A, #0x4            ;; 1 cycle
        BNZ       ??RpLog_Event_209  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 2621 	{
// 2622 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex863MHz);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz+112)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0x4            ;; 1 cycle
        BR        S:??RpLog_Event_210  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2623 	}
// 2624 	else if (freqBandId == RP_PHY_FREQ_BAND_896MHz)
??RpLog_Event_209:
        CMP       A, #0x5            ;; 1 cycle
        BNZ       ??RpLog_Event_211  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2625 	{
// 2626 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex896MHz);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz+240)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0x5            ;; 1 cycle
        BR        S:??RpLog_Event_210  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2627 	}
// 2628 	else if (freqBandId == RP_PHY_FREQ_BAND_901MHz)
??RpLog_Event_211:
        CMP       A, #0x6            ;; 1 cycle
        BNZ       ??RpLog_Event_212  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2629 	{
// 2630 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex901MHz);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz+304)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0x6            ;; 1 cycle
        BR        S:??RpLog_Event_210  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2631 	}
// 2632 	else if (freqBandId == RP_PHY_FREQ_BAND_915MHz)
??RpLog_Event_212:
        CMP       A, #0x7            ;; 1 cycle
        BNZ       ??RpLog_Event_213  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2633 	{
// 2634 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex915MHz);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz+368)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0x7            ;; 1 cycle
        BR        S:??RpLog_Event_210  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2635 	}
// 2636 	else if (freqBandId == RP_PHY_FREQ_BAND_917MHz)
??RpLog_Event_213:
        CMP       A, #0x8            ;; 1 cycle
        BNZ       ??RpLog_Event_214  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2637 	{
// 2638 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex917MHz);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz+448)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0x8            ;; 1 cycle
        BR        S:??RpLog_Event_210  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2639 	}
// 2640 	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
??RpLog_Event_214:
        CMP       A, #0x9            ;; 1 cycle
        BNZ       ??RpLog_Event_215  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2641 	{
// 2642 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex920MHz);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0x9            ;; 1 cycle
        BR        S:??RpLog_Event_210  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2643 	}
// 2644 	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz_Others)
??RpLog_Event_215:
        CMP       A, #0xE            ;; 1 cycle
        BNZ       ??RpLog_Event_216  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2645 	{
// 2646 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex920MHzOthers);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz+528)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0xE            ;; 1 cycle
        BR        S:??RpLog_Event_210  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2647 	}
// 2648 	else if (freqBandId == RP_PHY_FREQ_BAND_870MHz)
??RpLog_Event_216:
        CMP       A, #0xF            ;; 1 cycle
        BNZ       ??RpLog_Event_217  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2649 	{
// 2650 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex870MHz);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz+592)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        BR        S:??RpLog_Event_210  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2651 	}
// 2652 	else if (freqBandId == RP_PHY_FREQ_BAND_902MHz)
??RpLog_Event_217:
        CMP       A, #0x10           ;; 1 cycle
        BNZ       ??RpLog_Event_218  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2653 	{
// 2654 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex902MHz);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz+656)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0x10           ;; 1 cycle
        BR        S:??RpLog_Event_210  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2655 	}
// 2656 	else if (freqBandId == RP_PHY_FREQ_BAND_921MHz)
??RpLog_Event_218:
        CMP       A, #0x11           ;; 1 cycle
        BNZ       ??RpLog_Event_219  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2657 	{
// 2658 		rtn = RpSetChannelCommonVal(channel, freqBandId, (const uint32_t *)RpOpeModeIndex921MHz);
        MOVW      DE, #LWRD(_RpOpeModeIndex920MHz+688)  ;; 1 cycle
        MOV       C, #BYTE3(_RpOpeModeIndex920MHz)  ;; 1 cycle
        MOV       X, #0x11           ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_210:
        MOV       A, B               ;; 1 cycle
          CFI FunCall _RpSetChannelCommonVal
        CALL      F:_RpSetChannelCommonVal  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 2659 	}
// 2660 
// 2661 	return (rtn);
??RpLog_Event_219:
        MOV       A, X               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock44
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 124 cycles
// 2662 }
// 2663 
// 2664 /******************************************************************************
// 2665 Function Name:       RpSetChannelCommonVal
// 2666 Parameters:          channel number
// 2667                      freqBandId
// 2668 Return value:        set result
// 2669 Description:         set freq 920 MHz channel.
// 2670 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock45 Using cfiCommon1
          CFI Function _RpSetChannelCommonVal
        CODE
// 2671 static uint8_t RpSetChannelCommonVal( uint8_t channel, uint8_t freqBandId, const uint32_t *pTblPtr )
// 2672 {
_RpSetChannelCommonVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 24
        SUBW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+28
// 2673 	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       B, ES:_RpRxBuf+3420  ;; 2 cycles
// 2674 	uint32_t	frequency;
// 2675 	uint8_t	rtn;
// 2676 #ifdef R_FREQUENCY_OFFSET_ENABLED
// 2677 	int32_t phyFrequencyOffset = RpCb.pib.phyFrequencyOffset;
        MOVW      HL, #LWRD(_RpRxBuf+3486)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x10], AX      ;; 1 cycle
// 2678 #endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
// 2679 
// 2680 	frequency =  *(pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_FSTART);
        MOV       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x4            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2681 	frequency += (channel * (*(pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_FSPAN)));
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, [SP+0x17]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
          CFI FunCall ?L_MUL_FAST_L03
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+34
        POP       HL                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+28
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2682 	if (frequency > (*(pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_FEND)))
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??RpLog_Event_220  ;; 4 cycles
        ; ------------------------------------- Block: 92 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??RpLog_Event_220  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??RpLog_Event_220  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_220:
        BNC       ??RpLog_Event_221  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2683 	{
// 2684 		frequency = *(pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_FEND);
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2685 		rtn = (uint8_t) * (pTblPtr + (RP_TABLE_ITEM * opeMode) + RP_ENDINDEX);
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        BR        S:??RpLog_Event_222  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
// 2686 	}
// 2687 	else
// 2688 	{
// 2689 		rtn = channel;
??RpLog_Event_221:
        MOV       A, [SP+0x13]       ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_222:
        MOV       [SP], A            ;; 1 cycle
// 2690 	}
// 2691 
// 2692 #ifdef R_FREQUENCY_OFFSET_ENABLED
// 2693 	frequency = ((uint32_t)( (int32_t)frequency + phyFrequencyOffset )) & 0x3FFFFFFF;
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        AND       A, #0x3F           ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2694 #endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
// 2695 
// 2696 	RpCb.pib.phyFrequency = frequency;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3438)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2697 	RpRegBlockWrite(BBFREQ, (uint8_t RP_FAR *)&frequency, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x540         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 2698 	RpSetFreqAddReg(frequency, freqBandId);
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       E, A               ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _RpSetFreqAddReg
        CALL      F:_RpSetFreqAddReg  ;; 3 cycles
// 2699 
// 2700 	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        POP       AX                 ;; 1 cycle
          CFI CFA SP+28
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_223  ;; 4 cycles
        ; ------------------------------------- Block: 59 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_223  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2701 	{
// 2702 		RpSetCcaDurationTimeUpdate(rtn);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall _RpSetCcaDurationTimeUpdate
        CALL      F:_RpSetCcaDurationTimeUpdate  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2703 	}
// 2704 
// 2705 	return (rtn);
??RpLog_Event_223:
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x18          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock45
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 201 cycles
// 2706 }
// 2707 
// 2708 /******************************************************************************
// 2709 Function Name:       RpSetCcaDurationTimeUpdate
// 2710 Parameters:          channel number
// 2711 Return value:        none
// 2712 Description:         set cca duration time update.
// 2713 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock46 Using cfiCommon1
          CFI Function _RpSetCcaDurationTimeUpdate
          CFI NoCalls
        CODE
// 2714 static void RpSetCcaDurationTimeUpdate( uint8_t channel )
// 2715 {
_RpSetCcaDurationTimeUpdate:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 2716 	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3420  ;; 2 cycles
// 2717 	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
// 2718 
// 2719 	if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_224  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 2720 	{
// 2721 		if (channel < RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)
        CMP       A, #0x9            ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        BNC       ??RpLog_Event_225  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2722 		{
// 2723 			// 5mS
// 2724 			switch (opeMode)
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_226  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_227  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_228  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_229  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_230  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_226  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 6 cycles
// 2725 			{
// 2726 				case RP_PHY_FSK_OPEMODE_1:
// 2727 					RpCb.pib.phyCcaDuration = 272;	// 50kbps
// 2728 					break;
// 2729 				case RP_PHY_FSK_OPEMODE_2:
// 2730 					RpCb.pib.phyCcaDuration = 521;	// 100kbps
??RpLog_Event_227:
        MOVW      AX, #0x209         ;; 1 cycle
        BR        S:??RpLog_Event_231  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2731 					break;
// 2732 				case RP_PHY_FSK_OPEMODE_3:
// 2733 					RpCb.pib.phyCcaDuration = 1031;	// 200kbps
??RpLog_Event_228:
        MOVW      AX, #0x407         ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_231:
        MOVW      ES:_RpRxBuf+3414, AX  ;; 2 cycles
// 2734 					break;
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 8 cycles
// 2735 				case RP_PHY_FSK_OPEMODE_4:
// 2736 					RpCb.pib.phyCcaDuration = 2061;	// 400kbps
??RpLog_Event_229:
        MOVW      AX, #0x80D         ;; 1 cycle
        BR        S:??RpLog_Event_231  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2737 					break;
// 2738 				case RP_PHY_FSK_OPEMODE_5:
// 2739 					RpCb.pib.phyCcaDuration = 777;	// 150kbps
??RpLog_Event_230:
        MOVW      AX, #0x309         ;; 1 cycle
        BR        S:??RpLog_Event_231  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2740 					break;
// 2741 				case RP_PHY_FSK_OPEMODE_6:
// 2742 					RpCb.pib.phyCcaDuration = 272;	// 50kbps
??RpLog_Event_226:
        MOVW      AX, #0x110         ;; 1 cycle
        BR        S:??RpLog_Event_231  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2743 					break;
// 2744 			}
// 2745 		}
// 2746 		else
// 2747 		{
// 2748 			// 128uS
// 2749 			switch (opeMode)
??RpLog_Event_225:
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_232  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_233  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_234  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_235  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_236  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_232  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 6 cycles
// 2750 			{
// 2751 				case RP_PHY_FSK_OPEMODE_1:
// 2752 					RpCb.pib.phyCcaDuration = 28;	// 50kbps
// 2753 					break;
// 2754 				case RP_PHY_FSK_OPEMODE_2:
// 2755 					RpCb.pib.phyCcaDuration = 36;	// 100kbps
??RpLog_Event_233:
        MOVW      AX, #0x24          ;; 1 cycle
        BR        S:??RpLog_Event_231  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2756 					break;
// 2757 				case RP_PHY_FSK_OPEMODE_3:
// 2758 					RpCb.pib.phyCcaDuration = 55;	// 200kbps
??RpLog_Event_234:
        MOVW      AX, #0x37          ;; 1 cycle
        BR        S:??RpLog_Event_231  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2759 					break;
// 2760 				case RP_PHY_FSK_OPEMODE_4:
// 2761 					RpCb.pib.phyCcaDuration = 110;	// 400kbps
??RpLog_Event_235:
        MOVW      AX, #0x6E          ;; 1 cycle
        BR        S:??RpLog_Event_231  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2762 					break;
// 2763 				case RP_PHY_FSK_OPEMODE_5:
// 2764 					RpCb.pib.phyCcaDuration = 46;	// 150kbps
??RpLog_Event_236:
        MOVW      AX, #0x2E          ;; 1 cycle
        BR        S:??RpLog_Event_231  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2765 					break;
// 2766 				case RP_PHY_FSK_OPEMODE_6:
// 2767 					RpCb.pib.phyCcaDuration = 28;	// 50kbps
??RpLog_Event_232:
        MOVW      AX, #0x1C          ;; 1 cycle
        MOVW      ES:_RpRxBuf+3414, AX  ;; 2 cycles
// 2768 					break;
        ; ------------------------------------- Block: 3 cycles
// 2769 			}
// 2770 		}
// 2771 	}
// 2772 }
??RpLog_Event_224:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock46
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 137 cycles
// 2773 
// 2774 /******************************************************************************
// 2775 Function Name:       RpSetFskOpeModeVal
// 2776 Parameters:          afterReset
// 2777 Return value:        none
// 2778 Description:         set opeMode.
// 2779 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock47 Using cfiCommon1
          CFI Function _RpSetFskOpeModeVal
        CODE
// 2780 void RpSetFskOpeModeVal( uint8_t afterReset )
// 2781 {
_RpSetFskOpeModeVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
        MOV       B, A               ;; 1 cycle
// 2782 	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3436  ;; 2 cycles
// 2783 	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
        MOV       C, ES:_RpRxBuf+3420  ;; 2 cycles
// 2784 	uint8_t idIndex, tmpChannel;
// 2785 	uint8_t	tmpTxPower;
// 2786 
// 2787 	if (((freqBandId >= RP_PHY_FREQ_BAND_863MHz) && (freqBandId <= RP_PHY_FREQ_BAND_920MHz)) ||
// 2788 		((freqBandId >= RP_PHY_FREQ_BAND_920MHz_Others) && (freqBandId <= RP_PHY_FREQ_BAND_921MHz)))
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xFC           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BC        ??RpLog_Event_237  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xF2           ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??RpLog_Event_238  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2789 	{
// 2790 		if ((opeMode >= RP_PHY_FSK_OPEMODE_1) && (opeMode <= RpBandModeOffset[freqBandId][RP_OPEMODE_LIMIT]))
??RpLog_Event_237:
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpBandModeOffset)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOV       A, #BYTE3(_RpBandModeOffset)  ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        CMP0      C                  ;; 1 cycle
        BZ        ??RpLog_Event_239  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOV       A, L               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x7           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, C               ;; 1 cycle
        BNC       ??RpLog_Event_240  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 2791 		{
// 2792 			idIndex = (uint8_t)RpBandModeOffset[freqBandId][(opeMode-1)];
// 2793 		}
// 2794 		else
// 2795 		{
// 2796 			opeMode = RpBandModeOffset[freqBandId][RP_OPEMODE_LIMIT];
??RpLog_Event_239:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x7           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
// 2797 			idIndex = (uint8_t)RpBandModeOffset[freqBandId][(opeMode-1)];
        ; ------------------------------------- Block: 8 cycles
??RpLog_Event_240:
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        POP       DE                 ;; 1 cycle
          CFI CFA SP+8
        MOV       D, #0x0            ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        XCH       A, B               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2798 		}
// 2799 
// 2800 		RpCb.pib.phyFskOpeMode = opeMode;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:_RpRxBuf+3420, A  ;; 2 cycles
        MOV       A, B               ;; 1 cycle
// 2801 		RpCb.freqIdIndex = (uint8_t)idIndex;
        MOV       ES:_RpRxBuf+3544, A  ;; 2 cycles
// 2802 		RpCb.pib.phyDataRate = RpFreqBandIdToDataRate[RpCb.freqIdIndex];
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpBandModeOffset+144)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpBandModeOffset)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3434, AX  ;; 2 cycles
// 2803 		RpSetChannelsSupportedPageAndVal();
          CFI FunCall _RpSetChannelsSupportedPageAndVal
        CALL      F:_RpSetChannelsSupportedPageAndVal  ;; 3 cycles
// 2804 		RpSetFreqBandVal(afterReset);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall _RpSetFreqBandVal
        CALL      F:_RpSetFreqBandVal  ;; 3 cycles
// 2805 
// 2806 		if (afterReset == RP_FALSE)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_241  ;; 4 cycles
        ; ------------------------------------- Block: 42 cycles
// 2807 		{
// 2808 			tmpChannel = RpSetChannelVal(RpCb.pib.phyCurrentChannel);
// 2809 			if (tmpChannel != RP_ERROR)
// 2810 			{
// 2811 				RpCb.pib.phyCurrentChannel = tmpChannel;
// 2812 			}
// 2813 		}
// 2814 		else
// 2815 		{
// 2816 			if ((RpCb.pib.phyCurrentChannel != RP_RF_PIB_DFLT_CURRENT_CHANNEL) || 
// 2817 			   (RpCb.pib.phyFreqBandId != RP_RF_HW_DFLT_FREQ_BAND_ID) ||
// 2818 			   (RpCb.pib.phyFskOpeMode != RP_RF_HW_DFLT_FSK_OPE_MODE))
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3354, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_241  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_241  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3420, #0x2  ;; 2 cycles
        BZ        ??RpLog_Event_242  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2819 			{
// 2820 				tmpChannel = RpSetChannelVal(RpCb.pib.phyCurrentChannel);
??RpLog_Event_241:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3354  ;; 2 cycles
          CFI FunCall _RpSetChannelVal
        CALL      F:_RpSetChannelVal  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 2821 				if (tmpChannel != RP_ERROR)
        INC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_242  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 2822 				{
// 2823 					RpCb.pib.phyCurrentChannel = tmpChannel;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       ES:_RpRxBuf+3354, A  ;; 2 cycles
        ; ------------------------------------- Block: 4 cycles
// 2824 				}
// 2825 			}
// 2826 		}
// 2827 
// 2828 		tmpTxPower = RpSetTxPowerVal(RpCb.pib.phyTransmitPower);
??RpLog_Event_242:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3355  ;; 2 cycles
          CFI FunCall _RpSetTxPowerVal
        CALL      F:_RpSetTxPowerVal  ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 2829 		if (tmpTxPower != RP_ERROR)
        INC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_243  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 2830 		{
// 2831 			RpCb.pib.phyTransmitPower = tmpTxPower;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       ES:_RpRxBuf+3355, A  ;; 2 cycles
        ; ------------------------------------- Block: 4 cycles
// 2832 		}
// 2833 
// 2834 		if ((afterReset == RP_FALSE)
// 2835 			|| (RpCb.pib.phyMrFskSfd != RP_RF_HW_DFLT_MRFSK_SFD)
// 2836 			|| (RpCb.pib.phyFskOpeMode != RP_RF_HW_DFLT_FSK_OPE_MODE)
// 2837 			|| (RpCb.pib.phyFreqBandId != RP_RF_HW_DFLT_FREQ_BAND_ID)
// 2838 			|| (RpCb.pib.phyFskFecTxEna != RP_RF_HW_DFLT_FSK_FEC_TX_ENA)
// 2839 			|| (RpCb.pib.phyFskFecRxEna != RP_RF_HW_DFLT_FSK_FEC_RX_ENA))
??RpLog_Event_243:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_244  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3418   ;; 2 cycles
        BNZ       ??RpLog_Event_244  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_RpRxBuf+3420, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_244  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_244  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP0      ES:_RpRxBuf+3365   ;; 2 cycles
        BNZ       ??RpLog_Event_244  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP0      ES:_RpRxBuf+3366   ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetMrFskSfdVal
        ; ------------------------------------- Block: 3 cycles
// 2840 		{
// 2841 			RpSetMrFskSfdVal();
??RpLog_Event_244:
        CALL      F:_RpSetMrFskSfdVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2842 		}
// 2843 	}
// 2844 }
??RpLog_Event_238:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock47
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 199 cycles
// 2845 
// 2846 /******************************************************************************
// 2847 Function Name:       RpSetChannelsSupportedPageAndVal
// 2848 Parameters:          none
// 2849 Return value:        none
// 2850 Description:         set current page and channels supported.
// 2851 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock48 Using cfiCommon1
          CFI Function _RpSetChannelsSupportedPageAndVal
          CFI NoCalls
        CODE
// 2852 void RpSetChannelsSupportedPageAndVal( void )
// 2853 {
_RpSetChannelsSupportedPageAndVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 10
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+14
// 2854 	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3436  ;; 2 cycles
// 2855 	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
        MOV       B, ES:_RpRxBuf+3420  ;; 2 cycles
// 2856 	uint32_t channelsSupported[2];
// 2857 	uint8_t modeTblIndex;
// 2858 	uint8_t channelsSupportedPage = RpCb.pib.phyChannelsSupportedPage;
        MOV       A, ES:_RpRxBuf+3364  ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 2859 
// 2860 	channelsSupported[0] = 0;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2861 	channelsSupported[1] = 0;
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 2862 
// 2863 	if (freqBandId == RP_PHY_FREQ_BAND_863MHz)
        XCH       A, C               ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_245  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
// 2864 	{
// 2865 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) || (opeMode == RP_PHY_FSK_OPEMODE_2) || (opeMode == RP_PHY_FSK_OPEMODE_3))
        XCH       A, B               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        BZ        ??RpLog_Event_246  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        XCH       A, B               ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        BZ        ??RpLog_Event_246  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        XCH       A, B               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        BNZ       ??RpLog_Event_247  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2866 		{
// 2867 			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
??RpLog_Event_246:
        MOV       [SP], A            ;; 1 cycle
// 2868 			modeTblIndex = (opeMode - 1);
        DEC       B                  ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        BR        S:??RpLog_Event_248  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2869 		}
// 2870 		else if (opeMode == RP_PHY_FSK_OPEMODE_4)
??RpLog_Event_247:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BNZ       ??RpLog_Event_249  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2871 		{
// 2872 			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpLog_Event_250  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2873 			{
// 2874 				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2875 			}
// 2876 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_3 + channelsSupportedPage;
??RpLog_Event_250:
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, #0x3            ;; 1 cycle
        BR        S:??RpLog_Event_248  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2877 		}
// 2878 		else if (opeMode == RP_PHY_FSK_OPEMODE_5)
??RpLog_Event_249:
        CMP       A, #0x5            ;; 1 cycle
        BNZ       ??RpLog_Event_251  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2879 		{
// 2880 			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpLog_Event_252  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2881 			{
// 2882 				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2883 			}
// 2884 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_5 + channelsSupportedPage;
??RpLog_Event_252:
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, #0x5            ;; 1 cycle
        BR        S:??RpLog_Event_248  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2885 		}
// 2886 		else if (opeMode == RP_PHY_FSK_OPEMODE_6)
??RpLog_Event_251:
        CMP       A, #0x6            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        BNZ       ??RpLog_Event_253  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2887 		{
// 2888 			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpLog_Event_254  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2889 			{
// 2890 				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2891 			}
// 2892 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_7 + channelsSupportedPage;
??RpLog_Event_254:
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, #0x7            ;; 1 cycle
        BR        S:??RpLog_Event_248  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2893 		}
// 2894 		else // if (opeMode >= RP_PHY_FSK_OPEMODE_7)
// 2895 		{
// 2896 			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
??RpLog_Event_253:
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpLog_Event_255  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2897 			{
// 2898 				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2899 			}
// 2900 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_9 + channelsSupportedPage;
??RpLog_Event_255:
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, #0x9            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2901 		}
// 2902 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId4[modeTblIndex][0];
??RpLog_Event_248:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2903 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId4[modeTblIndex][1];
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        BR        R:??RpLog_Event_256  ;; 3 cycles
        ; ------------------------------------- Block: 24 cycles
// 2904 	}
// 2905 	else if (freqBandId == RP_PHY_FREQ_BAND_896MHz)
??RpLog_Event_245:
        XCH       A, C               ;; 1 cycle
        CMP       A, #0x5            ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        BNZ       ??RpLog_Event_257  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2906 	{
// 2907 		if (opeMode == RP_PHY_FSK_OPEMODE_1)
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_258  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2908 		{
// 2909 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_0 + channelsSupportedPage;
        MOV       A, [SP]            ;; 1 cycle
        BR        S:??RpLog_Event_259  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2910 		}
// 2911 		else if (opeMode == RP_PHY_FSK_OPEMODE_2)
??RpLog_Event_258:
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_260  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2912 		{
// 2913 			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_1)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpLog_Event_261  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2914 			{
// 2915 				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_1;
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2916 			}
// 2917 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_4 + channelsSupportedPage;
??RpLog_Event_261:
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, #0x4            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_259:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BR        S:??RpLog_Event_262  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 2918 		}
// 2919 		else // if (opeMode == RP_PHY_FSK_OPEMODE_3)
// 2920 		{
// 2921 			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
??RpLog_Event_260:
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2922 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_6;
        MOVW      HL, #0x30          ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 2923 		}
// 2924 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId5[modeTblIndex][0];
??RpLog_Event_262:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x58          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2925 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId5[modeTblIndex][1];
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x5C          ;; 1 cycle
        BR        R:??RpLog_Event_256  ;; 3 cycles
        ; ------------------------------------- Block: 23 cycles
// 2926 	}
// 2927 	else if (freqBandId == RP_PHY_FREQ_BAND_901MHz)
??RpLog_Event_257:
        XCH       A, C               ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        BNZ       ??RpLog_Event_263  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2928 	{
// 2929 		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
        MOV       [SP], A            ;; 1 cycle
// 2930 		modeTblIndex = (opeMode - 1);
        DEC       B                  ;; 1 cycle
        MOV       A, B               ;; 1 cycle
// 2931 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId6[modeTblIndex][0];
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x90          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2932 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId6[modeTblIndex][1];
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x94          ;; 1 cycle
        BR        R:??RpLog_Event_256  ;; 3 cycles
        ; ------------------------------------- Block: 28 cycles
// 2933 	}
// 2934 	else if (freqBandId == RP_PHY_FREQ_BAND_915MHz)
??RpLog_Event_263:
        XCH       A, C               ;; 1 cycle
        CMP       A, #0x7            ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        BNZ       ??RpLog_Event_264  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2935 	{
// 2936 		if (opeMode == RP_PHY_FSK_OPEMODE_1)
        XCH       A, B               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        BNZ       ??RpLog_Event_265  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2937 		{
// 2938 			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_2)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpLog_Event_266  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2939 			{
// 2940 				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_2;
        MOV       A, #0x2            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2941 			}
// 2942 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_0 + channelsSupportedPage;
??RpLog_Event_266:
        MOV       A, [SP]            ;; 1 cycle
        BR        S:??RpLog_Event_267  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2943 		}
// 2944 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) || (opeMode == RP_PHY_FSK_OPEMODE_3))
??RpLog_Event_265:
        XCH       A, B               ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        BZ        ??RpLog_Event_268  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        XCH       A, B               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        BNZ       ??RpLog_Event_269  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2945 		{
// 2946 			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
??RpLog_Event_268:
        MOV       [SP], A            ;; 1 cycle
// 2947 			modeTblIndex = (opeMode + 1);
        INC       B                  ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        BR        S:??RpLog_Event_267  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2948 		}
// 2949 		else // if (opeMode == RP_PHY_FSK_OPEMODE_4)
// 2950 		{
// 2951 			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_2)
??RpLog_Event_269:
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpLog_Event_270  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2952 			{
// 2953 				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_2;
        MOV       A, #0x2            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2954 			}
// 2955 			modeTblIndex = (opeMode + 1) + channelsSupportedPage;
??RpLog_Event_270:
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, B               ;; 1 cycle
        INC       A                  ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 2956 		}
// 2957 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId7[modeTblIndex][0];
??RpLog_Event_267:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xA8          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2958 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId7[modeTblIndex][1];
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xAC          ;; 1 cycle
        BR        R:??RpLog_Event_256  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
// 2959 	}
// 2960 	else if (freqBandId == RP_PHY_FREQ_BAND_917MHz)
??RpLog_Event_264:
        XCH       A, C               ;; 1 cycle
        CMP       A, #0x8            ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        BNZ       ??RpLog_Event_271  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2961 	{
// 2962 		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
        MOV       [SP], A            ;; 1 cycle
// 2963 		modeTblIndex = (opeMode - 1);
        DEC       B                  ;; 1 cycle
        MOV       A, B               ;; 1 cycle
// 2964 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId8[modeTblIndex][0];
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xE8          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2965 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId8[modeTblIndex][1];
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xEC          ;; 1 cycle
        BR        R:??RpLog_Event_256  ;; 3 cycles
        ; ------------------------------------- Block: 28 cycles
// 2966 	}
// 2967 	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
??RpLog_Event_271:
        XCH       A, C               ;; 1 cycle
        CMP       A, #0x9            ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        BNZ       ??RpLog_Event_272  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2968 	{
// 2969 		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
        MOV       [SP], A            ;; 1 cycle
// 2970 		modeTblIndex = (opeMode - 1);
        DEC       B                  ;; 1 cycle
        MOV       A, B               ;; 1 cycle
// 2971 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId9[modeTblIndex][0];
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x108         ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2972 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId9[modeTblIndex][1];
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x10C         ;; 1 cycle
        BR        R:??RpLog_Event_256  ;; 3 cycles
        ; ------------------------------------- Block: 28 cycles
// 2973 	}
// 2974 	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz_Others)
??RpLog_Event_272:
        XCH       A, C               ;; 1 cycle
        CMP       A, #0xE            ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        BNZ       ??RpLog_Event_273  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2975 	{
// 2976 		if (opeMode == RP_PHY_FSK_OPEMODE_1)
        XCH       A, B               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        BNZ       ??RpLog_Event_274  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2977 		{
// 2978 			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
        MOV       [SP], A            ;; 1 cycle
// 2979 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_0;
        BR        S:??RpLog_Event_275  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2980 		}
// 2981 		else if (opeMode == RP_PHY_FSK_OPEMODE_2)
??RpLog_Event_274:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_276  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2982 		{
// 2983 			if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_2)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpLog_Event_277  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2984 			{
// 2985 				channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_2;
        MOV       A, #0x2            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 2986 			}
// 2987 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_1 + channelsSupportedPage;
??RpLog_Event_277:
        MOV       A, [SP]            ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_275:
        MOVW      HL, AX             ;; 1 cycle
        BR        S:??RpLog_Event_278  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2988 		}
// 2989 		else //if (opeMode == RP_PHY_FSK_OPEMODE_3)
// 2990 		{
// 2991 			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
??RpLog_Event_276:
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2992 			modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_4;
        MOVW      HL, #0x20          ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 2993 		}
// 2994 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId14[modeTblIndex][0];
??RpLog_Event_278:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x138         ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2995 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId14[modeTblIndex][1];
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x13C         ;; 1 cycle
        BR        R:??RpLog_Event_256  ;; 3 cycles
        ; ------------------------------------- Block: 23 cycles
// 2996 	}
// 2997 	else if (freqBandId == RP_PHY_FREQ_BAND_870MHz)
??RpLog_Event_273:
        XCH       A, C               ;; 1 cycle
        CMP       A, #0xF            ;; 1 cycle
        XCH       A, C               ;; 1 cycle
        BNZ       ??RpLog_Event_279  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2998 	{
// 2999 		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
        MOV       [SP], A            ;; 1 cycle
// 3000 		modeTblIndex = (opeMode - 1);
        DEC       B                  ;; 1 cycle
        MOV       A, B               ;; 1 cycle
// 3001 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId15[modeTblIndex][0];
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x160         ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 3002 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId15[modeTblIndex][1];
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x164         ;; 1 cycle
        BR        S:??RpLog_Event_256  ;; 3 cycles
        ; ------------------------------------- Block: 28 cycles
// 3003 	}
// 3004 	else if (freqBandId == RP_PHY_FREQ_BAND_902MHz)
??RpLog_Event_279:
        MOV       A, C               ;; 1 cycle
        CMP       A, #0x10           ;; 1 cycle
        BNZ       ??RpLog_Event_280  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3005 	{
// 3006 		if (channelsSupportedPage > RP_PHY_CHANNELS_SUPPORTED_INDEX_2)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpLog_Event_281  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3007 		{
// 3008 			channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_2;
        MOV       A, #0x2            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 3009 		}
// 3010 		modeTblIndex = RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_0 + channelsSupportedPage;
// 3011 
// 3012 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId16[modeTblIndex][0];
??RpLog_Event_281:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x178         ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 3013 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId16[modeTblIndex][1];
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x17C         ;; 1 cycle
        BR        S:??RpLog_Event_256  ;; 3 cycles
        ; ------------------------------------- Block: 26 cycles
// 3014 	}
// 3015 	else if (freqBandId == RP_PHY_FREQ_BAND_921MHz)
??RpLog_Event_280:
        CMP       A, #0x11           ;; 1 cycle
        BNZ       ??RpLog_Event_282  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3016 	{
// 3017 		channelsSupportedPage = RP_PHY_CHANNELS_SUPPORTED_INDEX_0;
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 3018 		modeTblIndex = (opeMode - 1);
        DEC       B                  ;; 1 cycle
        MOV       A, B               ;; 1 cycle
// 3019 		channelsSupported[0] = RpCurrentPageToChannelsSupprotedAtBandId17[modeTblIndex][0];
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x3            ;; 1 cycle
        ADDW      AX, #LWRD(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpCurrentPageToChannelsSupprotedAtBandId4)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x190         ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 3020 		channelsSupported[1] = RpCurrentPageToChannelsSupprotedAtBandId17[modeTblIndex][1];
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x194         ;; 1 cycle
        ; ------------------------------------- Block: 26 cycles
??RpLog_Event_256:
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
// 3021 	}
// 3022 
// 3023 	RpCb.pib.phyChannelsSupportedPage = channelsSupportedPage;
??RpLog_Event_282:
        MOV       A, [SP]            ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3364, A  ;; 2 cycles
// 3024 	RpCb.pib.phyChannelsSupported[0] = channelsSupported[0];
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3356)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3025 	RpCb.pib.phyChannelsSupported[1] = channelsSupported[1];
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3360)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 3026 }
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock48
        ; ------------------------------------- Block: 29 cycles
        ; ------------------------------------- Total: 597 cycles
// 3027 
// 3028 /******************************************************************************
// 3029 Function Name:       RpSetFreqBandVal
// 3030 Parameters:          afterReset
// 3031 Return value:        none
// 3032 Description:         set freq band.
// 3033 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock49 Using cfiCommon1
          CFI Function _RpSetFreqBandVal
        CODE
// 3034 static void RpSetFreqBandVal( uint8_t afterReset )
// 3035 {
_RpSetFreqBandVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 32
        SUBW      SP, #0x20          ;; 1 cycle
          CFI CFA SP+36
        MOV       X, A               ;; 1 cycle
// 3036 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
// 3037 	uint8_t antDivRxEna = RpCb.pib.phyAntennaDiversityRxEna;
        MOV       A, ES:_RpRxBuf+3428  ;; 2 cycles
        MOV       [SP+0x14], A       ;; 1 cycle
// 3038 	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
        MOV       A, ES:_RpRxBuf+3436  ;; 2 cycles
        MOV       [SP+0x0D], A       ;; 1 cycle
// 3039 	uint8_t fecRxEna = RpCb.pib.phyFskFecRxEna;
        MOV       A, ES:_RpRxBuf+3366  ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 3040 	const uint8_t *rssiLossPtr = (const uint8_t *)RpFreqBandTblRssiLossPtr[RpConfig.rssiLoss + 1];
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       C, ES:_RpConfig+7  ;; 2 cycles
        MOV       A, C               ;; 1 cycle
        SAR       A, 0x7             ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        SHLW      BC, 0x2            ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, #LWRD(_RpFreqBandTblRssiLossPtr+4)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV       ES, #BYTE3(_RpFreqBandTblRssiLossPtr)  ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+38
        POP       BC                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       [SP+0x20], A       ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        MOVW      [SP+0x1E], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+36
// 3041 	uint8_t arrayChar[5];
// 3042 
// 3043 	if (index < (sizeof(RpFreqBandTbl) / sizeof(uint8_t) / RP_MAXNUM_OFFSET))
        MOV       A, [SP+0x05]       ;; 1 cycle
        CMP       A, #0x14           ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??RpLog_Event_283  ;; 4 cycles
        ; ------------------------------------- Block: 48 cycles
        XCH       A, X               ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
// 3044 	{
// 3045 		if (afterReset == RP_FALSE || ((RpCb.pib.phyFreqBandId != RP_RF_HW_DFLT_FREQ_BAND_ID) || (RpCb.pib.phyFskOpeMode != RP_RF_HW_DFLT_FSK_OPE_MODE)))
        CLRB      A                  ;; 1 cycle
        MOVW      [SP+0x12], AX      ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x10], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_284  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_284  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_RpRxBuf+3420, #0x2  ;; 2 cycles
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_285  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3046 		{
// 3047 			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
// 3048 				((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
??RpLog_Event_284:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_286  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_287  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        SKNZ                         ;; 1 cycle
          CFI FunCall _RpPrevSentTimeReSetting
        ; ------------------------------------- Block: 3 cycles
// 3049 			{
// 3050 				RpPrevSentTimeReSetting();
??RpLog_Event_286:
        CALL      F:_RpPrevSentTimeReSetting  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3051 			}
// 3052 			RpCb.reg.bbTimeCon &= ~(TIMEEN);// Timer Count Disable, Comp0 transmit Disable
??RpLog_Event_287:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:_RpRxBuf+3554.0  ;; 3 cycles
// 3053 			RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOV       C, ES:_RpRxBuf+3554  ;; 2 cycles
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3054 
// 3055 			RpRegWrite(BBSYMBLRATE, (uint8_t)RpFreqBandTbl[index][RP_SYMBOL_OFFSET_L]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x560         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3056 			RpRegWrite(BBSYMBLRATE_1, (uint8_t)RpFreqBandTbl[index][RP_SYMBOL_OFFSET_H]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x568         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3057 			RpRegWrite(BBMODSET, (uint8_t)RpFreqBandTbl[index][RP_MODESET_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x588         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3058 
// 3059 			RpCb.reg.bbTimeCon |= TIMEEN;	// Timer Count Start, Comp0 transmit Disable Stay
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3554.0  ;; 3 cycles
// 3060 			RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOV       C, ES:_RpRxBuf+3554  ;; 2 cycles
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3061 
// 3062 			RpRegWrite((0x00F1 << 3), (uint8_t)RpFreqBandTbl[index][RP_TXCPISL1_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x788         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3063 			RpRegWrite((0x00F2 << 3), (uint8_t)RpFreqBandTbl[index][RP_TXCPISL2_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0xD           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x790         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3064 			RpRegWrite((0x00F3 << 3), (uint8_t)RpFreqBandTbl[index][RP_TXCR0_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x798         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3065 			RpRegWrite((0x00F4 << 3), (uint8_t)RpFreqBandTbl[index][RP_TXCR1_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0xF           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x7A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3066 			RpRegWrite((0x0423 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0423_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2118        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3067 			RpRegWrite((0x0471 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0471_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x27          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2388        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3068 			RpRegWrite((0x047D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047D_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x23E8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3069 
// 3070 			arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0486_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0486_OFFSET];
        MOV       A, [SP+0x04]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        BNZ       ??RpLog_Event_288  ;; 4 cycles
        ; ------------------------------------- Block: 148 cycles
        ADDW      AX, #0x2F          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        BR        S:??RpLog_Event_289  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
??RpLog_Event_288:
        ADDW      AX, #0x30          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x33          ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_289:
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x07], A       ;; 1 cycle
// 3071 			arrayChar[1] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0487_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0487_OFFSET];
// 3072 			RpRegBlockWrite((0x0486 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2430        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3073 
// 3074 			RpRegWrite((0x048D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x048D_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x37          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2468        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3075 			RpRegWrite((0x048F << 3), (uint8_t)RpFreqBandTbl[index][RP_0x048F_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2478        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3076 			RpRegWrite((0x0405 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0405_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2028        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3077 			RpRegWrite((0x0505 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0505_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x40          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2828        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3078 			RpRegWrite((0x0403 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0403_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x11          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2018        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3079 			RpRegWrite((0x04D9 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x04D9_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x3B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x26C8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3080 			RpWrEvaReg2(RpFreqBandTblSerial[index][RP_SER0x23_OFFSET]);
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, #0x6           ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTblSerial)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTblSerial)  ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
        MOV       [SP+0x1A], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        AND       A, #0x7F           ;; 1 cycle
        MOVW      [SP+0x16], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3081 			RpWrEvaReg2(RpFreqBandTblSerial[index][RP_SER0x0A_OFFSET]);
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        AND       A, #0x7F           ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3082 			RpRegWrite(BBPABL, (uint8_t)RpFreqBandTbl[index][RP_PABL_OFFSET]);
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x3           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x600         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3083 			RpRegWrite(BBSHRCON, (uint8_t)RpFreqBandTbl[index][RP_SHRCON_OFFSET]);
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x630         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3084 
// 3085 			RpRegWrite((0x00F5 << 3), (uint8_t)RpFreqBandTbl2[freqBandId][RP_0x00F5_OFFSET]);
        MOV       A, [SP+0x11]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpFreqBandTblSerial)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTblSerial)  ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+36
        MOV       [SP+0x18], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x16], AX      ;; 1 cycle
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x78          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x7A8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3086 			RpRegWrite((0x00FB << 3), (uint8_t)RpFreqBandTbl2[freqBandId][RP_0x00FB_OFFSET]);
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        ADDW      AX, #0x79          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x7D8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3087 			RpWrEvaReg2(RpFreqBandTblSerial2[freqBandId]);
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        ADDW      AX, #0x9C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        AND       A, #0x7F           ;; 1 cycle
        MOVW      [SP+0x16], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3088 		}
        POP       AX                 ;; 1 cycle
          CFI CFA SP+36
        ; ------------------------------------- Block: 213 cycles
// 3089 
// 3090 		RpRegWrite((0x00CC << 3), (uint8_t)RpFreqBandTbl[index][RP_0x00CC_OFFSET]);
??RpLog_Event_285:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x660         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3091 		arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x0430_OFFSET];	// 0x0430
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
// 3092 		arrayChar[1] = 0x00;											// 0x0431 (h/W initial)
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
// 3093 		arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x0432_OFFSET];	// 0x0432
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
// 3094 		RpRegBlockWrite((0x0430 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2180        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3095 		RpRegWrite((0x0436 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0436_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x21B0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3096 		RpRegWrite((0x043A << 3), (uint8_t)RpFreqBandTbl[index][RP_0x043A_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x21D0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3097 		RpRegBlockWrite((0x0473 << 3), (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_0x0473_OFFSET]), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2398        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3098 		RpRegWrite((0x047C << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047C_OFFSET]);
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x2A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x23E0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3099 		RpRegBlockWrite((0x047E << 3), (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_0x047E_OFFSET]), 3);
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x23F0        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3100 		RpRegWrite((0x0488 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0488_OFFSET]);
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x35          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2440        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3101 		RpRegBlockWrite((0x0493 << 3), (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_0x0493_OFFSET]), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x39          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2498        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3102 		RpRegWrite((0x04F4 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x04F4_OFFSET]);
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x27A0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3103 
// 3104 		RpRegBlockWrite((0x0581 << 3), (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_0x0581_OFFSET]), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x46          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2C08        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3105 		arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x04F6_OFFSET];	// 0x04F6
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x3E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x10], A       ;; 1 cycle
// 3106 		arrayChar[1] = 0x00;											// 0x04F7 (h/W initial)
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x11], A       ;; 1 cycle
// 3107 		arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x04F8_OFFSET];	// 0x04F8
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x12], A       ;; 1 cycle
// 3108 		RpRegBlockWrite((0x04F6 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x27B0        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3109 		RpRegWrite((0x0415 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0415_OFFSET]);
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x13          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x20A8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3110 		RpWrEvaReg2(RpFreqBandTblSerial[index][RP_SER0x0E_OFFSET]);
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      BC, #0x6           ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTblSerial)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpFreqBandTblSerial)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        AND       A, #0x7F           ;; 1 cycle
        MOVW      [SP+0x1E], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x20          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3111 
// 3112 		RpCb.pib.refVthVal = 0x0100 | (uint16_t)(*(rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_LVLVTH_OFFSET)));
        MOV       A, [SP+0x13]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x2C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2A]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+36
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x18], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x16], AX      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3372, AX  ;; 2 cycles
// 3113 		RpSetLvlVthVal();
          CFI FunCall _RpSetLvlVthVal
        CALL      F:_RpSetLvlVthVal  ;; 3 cycles
// 3114 		RpSetCcaVthVal();
          CFI FunCall _RpSetCcaVthVal
        CALL      F:_RpSetCcaVthVal  ;; 3 cycles
// 3115 
// 3116 		RpCb.pib.phyRssiLoss = (uint16_t) * (rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_0x0483_OFFSET));
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3376, AX  ;; 2 cycles
// 3117 		RpSetRssiOffsetVal();
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3F           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2418        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3118 		arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x0454_OFFSET];// 0x0454
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x20          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
// 3119 		arrayChar[1] = 0xA9;	// 0x0455 (h/W initial)
        MOV       A, #0xA9           ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
// 3120 		arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x0456_OFFSET];// 0x0456
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
// 3121 		RpRegBlockWrite((0x0454 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x22A0        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        MOV       A, [SP+0x06]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
        CMP       A, #0x1            ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        BNZ       ??RpLog_Event_290  ;; 4 cycles
        ; ------------------------------------- Block: 285 cycles
        ADDW      AX, #0x42          ;; 1 cycle
        BR        S:??RpLog_Event_291  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_290:
        ADDW      AX, #0x43          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_291:
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
// 3122 
// 3123 		arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x050F_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x050F_OFFSET];
// 3124 		RpRegBlockWrite((0x050F << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 1);
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2878        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3125 		arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x058F_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x058F_OFFSET];
        MOV       A, [SP+0x06]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
        CMP       A, #0x1            ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        BNZ       ??RpLog_Event_292  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        ADDW      AX, #0x48          ;; 1 cycle
        BR        S:??RpLog_Event_293  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_292:
        ADDW      AX, #0x49          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_293:
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
// 3126 		RpRegBlockWrite((0x058F << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 1);
        ONEW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2C78        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3127 
// 3128 		arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x059F_OFFSET];
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
// 3129 		arrayChar[1] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x05A0_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x05A0_OFFSET];
        MOV       A, [SP+0x06]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
        CMP       A, #0x1            ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        BNZ       ??RpLog_Event_294  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
        ADDW      AX, #0x4B          ;; 1 cycle
        BR        S:??RpLog_Event_295  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_294:
        ADDW      AX, #0x4C          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_295:
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x07], A       ;; 1 cycle
// 3130 		arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x05A1_OFFSET];
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4D          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
// 3131 		arrayChar[3] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x05A2_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x05A2_OFFSET];
        MOV       A, [SP+0x04]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
        INCW      HL                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqBandVal_0:
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x09], A       ;; 1 cycle
// 3132 		RpRegBlockWrite((0x059F << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 4);
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2CF8        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3133 
// 3134 		if (antDivRxEna == RP_TRUE)
        MOV       A, [SP+0x16]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+36
        CMP       A, #0x1            ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        BNZ       ??RpLog_Event_296  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
// 3135 		{
// 3136 			RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_ANDVON_OFFSET]);
        ADDW      AX, #0x16          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2168        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3137 
// 3138 			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_297  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_297  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3139 			{
// 3140 				RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_ANTDVT_TIMEOUT_ARIB]), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x680         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3141 				RpRegWrite((0x0472 << 3), 0x04);
        MOV       C, #0x4            ;; 1 cycle
        MOVW      AX, #0x2390        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        BR        S:??RpLog_Event_298  ;; 3 cycles
          CFI CFA SP+36
        ; ------------------------------------- Block: 21 cycles
// 3142 			}
// 3143 			else
// 3144 			{
// 3145 				RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(RpFreqBandTbl[index][RP_ANTDVT_TIMEOUT]), 2);
??RpLog_Event_297:
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x680         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3146 				RpSetPreamble4ByteRxMode();
          CFI FunCall _RpSetPreamble4ByteRxMode
        CALL      F:_RpSetPreamble4ByteRxMode  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
??RpLog_Event_298:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+36
        BR        S:??RpLog_Event_299  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3147 			}
// 3148 		}
// 3149 		else
// 3150 		{
// 3151 			RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_OFFSET]);
??RpLog_Event_296:
        ADDW      AX, #0x15          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2168        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3152 			RpSetPreamble4ByteRxMode();
          CFI FunCall _RpSetPreamble4ByteRxMode
        CALL      F:_RpSetPreamble4ByteRxMode  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
// 3153 		}
// 3154 
// 3155 		if (afterReset == RP_FALSE)
??RpLog_Event_299:
        MOV       A, [SP+0x0C]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 1 cycle
          CFI FunCall _RpExecuteCalibration
        ; ------------------------------------- Block: 3 cycles
// 3156 		{
// 3157 			RpExecuteCalibration();
        CALL      F:_RpExecuteCalibration  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3158 		}
// 3159 	}
// 3160 }
??RpLog_Event_283:
        ADDW      SP, #0x20          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock49
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 962 cycles
// 3161 
// 3162 /******************************************************************************
// 3163 Function Name:       RpSetTxPowerVal
// 3164 Parameters:          set tx power
// 3165 Return value:        set tx power
// 3166 Description:         set transmit power.
// 3167 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock50 Using cfiCommon1
          CFI Function _RpSetTxPowerVal
        CODE
// 3168 uint8_t RpSetTxPowerVal( uint8_t gainSet )
// 3169 {
_RpSetTxPowerVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 14
        SUBW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+18
// 3170 	uint16_t regSerVal;
// 3171 	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3436  ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 3172 	uint8_t regSetVal[4];
// 3173 	uint8_t arrayChar[3];
// 3174 
// 3175 	if (gainSet < RP_MAXNUM_TXPOWER)
        MOV       A, [SP+0x0D]       ;; 1 cycle
        CMP       A, #0x69           ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??RpLog_Event_300  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 3176 	{
// 3177 		if ((freqBandId == RP_PHY_FREQ_BAND_863MHz) || (freqBandId == RP_PHY_FREQ_BAND_870MHz))
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BZ        ??RpLog_Event_301  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0xF            ;; 1 cycle
        BNZ       ??RpLog_Event_302  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3178 		{
// 3179 			if (gainSet >= RP_MAXNUM_TXPOWER_EU)
??RpLog_Event_301:
        MOV       A, [SP+0x0D]       ;; 1 cycle
        CMP       A, #0x67           ;; 1 cycle
        BC        ??RpLog_Event_303  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3180 			{
// 3181 				gainSet = RP_MAXNUM_TXPOWER_EU - 1;
        MOV       A, #0x66           ;; 1 cycle
        MOV       [SP+0x0D], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 3182 			}
// 3183 			regSetVal[0] = RpTxPowerModeSetEu[gainSet][0];
??RpLog_Event_303:
        MOV       A, [SP+0x0D]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_RpTxPowerModeSetJpn)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpTxPowerModeSetJpn)  ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x198         ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BR        R:??RpLog_Event_304  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
// 3184 			regSetVal[1] = RpTxPowerModeSetEu[gainSet][1];
// 3185 			regSetVal[2] = RpTxPowerModeSetEu[gainSet][2];
// 3186 			regSetVal[3] = RpTxPowerModeSetEu[gainSet][3];
// 3187 		}
// 3188 		else if ((freqBandId == RP_PHY_FREQ_BAND_896MHz) || (freqBandId == RP_PHY_FREQ_BAND_901MHz) || (freqBandId == RP_PHY_FREQ_BAND_915MHz) || (freqBandId == RP_PHY_FREQ_BAND_902MHz))
??RpLog_Event_302:
        CMP       A, #0x5            ;; 1 cycle
        BZ        ??RpLog_Event_305  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x6            ;; 1 cycle
        BZ        ??RpLog_Event_305  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x7            ;; 1 cycle
        BZ        ??RpLog_Event_305  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x10           ;; 1 cycle
        BNZ       ??RpLog_Event_306  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3189 		{
// 3190 			if (gainSet >= RP_MAXNUM_TXPOWER_US)
// 3191 			{
// 3192 				gainSet = RP_MAXNUM_TXPOWER_US - 1;
// 3193 			}
// 3194 			regSetVal[0] = RpTxPowerModeSetUs[gainSet][0];
??RpLog_Event_305:
        MOV       A, [SP+0x0D]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_RpTxPowerModeSetJpn)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpTxPowerModeSetJpn)  ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4D0         ;; 1 cycle
        BR        S:??RpLog_Event_307  ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
// 3195 			regSetVal[1] = RpTxPowerModeSetUs[gainSet][1];
// 3196 			regSetVal[2] = RpTxPowerModeSetUs[gainSet][2];
// 3197 			regSetVal[3] = RpTxPowerModeSetUs[gainSet][3];
// 3198 		}
// 3199 		else if (freqBandId == RP_PHY_FREQ_BAND_917MHz)
??RpLog_Event_306:
        CMP       A, #0x8            ;; 1 cycle
        BNZ       ??RpLog_Event_308  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3200 		{
// 3201 			if (gainSet >= RP_MAXNUM_TXPOWER_KOREA)
        MOV       A, [SP+0x0D]       ;; 1 cycle
        CMP       A, #0x67           ;; 1 cycle
        BC        ??RpLog_Event_309  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3202 			{
// 3203 				gainSet = RP_MAXNUM_TXPOWER_KOREA - 1;
        MOV       A, #0x66           ;; 1 cycle
        MOV       [SP+0x0D], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 3204 			}
// 3205 			regSetVal[0] = RpTxPowerModeSetKorea[gainSet][0];
??RpLog_Event_309:
        MOV       A, [SP+0x0D]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_RpTxPowerModeSetJpn)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpTxPowerModeSetJpn)  ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x334         ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
??RpLog_Event_307:
        MOVW      HL, AX             ;; 1 cycle
        BR        S:??RpLog_Event_304  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3206 			regSetVal[1] = RpTxPowerModeSetKorea[gainSet][1];
// 3207 			regSetVal[2] = RpTxPowerModeSetKorea[gainSet][2];
// 3208 			regSetVal[3] = RpTxPowerModeSetKorea[gainSet][3];
// 3209 		}
// 3210 		else if ((freqBandId == RP_PHY_FREQ_BAND_920MHz) || (freqBandId == RP_PHY_FREQ_BAND_920MHz_Others) || (freqBandId == RP_PHY_FREQ_BAND_921MHz))
??RpLog_Event_308:
        CMP       A, #0x9            ;; 1 cycle
        BZ        ??RpLog_Event_310  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0xE            ;; 1 cycle
        BZ        ??RpLog_Event_310  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x11           ;; 1 cycle
        BNZ       ??RpLog_Event_311  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3211 		{
// 3212 			if (gainSet >= RP_MAXNUM_TXPOWER_JPN)
??RpLog_Event_310:
        MOV       A, [SP+0x0D]       ;; 1 cycle
        CMP       A, #0x66           ;; 1 cycle
        BC        ??RpLog_Event_312  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3213 			{
// 3214 				gainSet = (RP_MAXNUM_TXPOWER_JPN - 1);
        MOV       A, #0x65           ;; 1 cycle
        MOV       [SP+0x0D], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 3215 			}
// 3216 			regSetVal[0] = RpTxPowerModeSetJpn[gainSet][0];
??RpLog_Event_312:
        MOV       A, [SP+0x0D]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_RpTxPowerModeSetJpn)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpTxPowerModeSetJpn)  ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        ; ------------------------------------- Block: 14 cycles
??RpLog_Event_304:
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
// 3217 			regSetVal[1] = RpTxPowerModeSetJpn[gainSet][1];
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
// 3218 			regSetVal[2] = RpTxPowerModeSetJpn[gainSet][2];
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
// 3219 			regSetVal[3] = RpTxPowerModeSetJpn[gainSet][3];
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 15 cycles
// 3220 		}
// 3221 		// VREG
// 3222 		RpCb.reg.bbTxCon0 &= (~TXDDCVAR);
// 3223 		RpCb.reg.bbTxCon0 |= regSetVal[0];
??RpLog_Event_311:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3567  ;; 2 cycles
        AND       A, #0xFC           ;; 1 cycle
        OR        A, C               ;; 1 cycle
        MOV       ES:_RpRxBuf+3567, A  ;; 2 cycles
// 3224 		arrayChar[0] = RpCb.reg.bbTxCon0;
        MOV       [SP+0x08], A       ;; 1 cycle
// 3225 		arrayChar[1] = 0x00;// 0x0091 H/W Initial
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
// 3226 		// VLDO CONT
// 3227 		RpCb.reg.bbTxCon2 &= (~TXLDOTRXVAR);
// 3228 		RpCb.reg.bbTxCon2 |= regSetVal[1];
        MOV       A, ES:_RpRxBuf+3568  ;; 2 cycles
        AND       A, #0xE0           ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       ES:_RpRxBuf+3568, A  ;; 2 cycles
// 3229 		arrayChar[2] = RpCb.reg.bbTxCon2;
        MOV       [SP+0x0A], A       ;; 1 cycle
// 3230 		RpRegBlockWrite(BBTXCON0, (uint8_t *)&(arrayChar[0]), 3);
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x480         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3231 		// TXGF
// 3232 		RpCb.reg.bb19Er2 &= (~0x1F);
// 3233 		RpCb.reg.bb19Er2 |= (regSetVal[2]);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3569  ;; 2 cycles
        AND       A, #0xE0           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       ES:_RpRxBuf+3569, A  ;; 2 cycles
// 3234 		regSerVal = (uint16_t)(0x1900 | RpCb.reg.bb19Er2);
// 3235 		RpWrEvaReg2(regSerVal);
        MOV       X, A               ;; 1 cycle
        MOV       A, #0x19           ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3236 		// CTN OUT
// 3237 		RpCb.reg.bb25Er2 &= (~0x1F);
// 3238 		RpCb.reg.bb25Er2 |= (regSetVal[3]);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3570  ;; 2 cycles
        AND       A, #0xE0           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       ES:_RpRxBuf+3570, A  ;; 2 cycles
// 3239 		regSerVal = (uint16_t)(0x2500 | RpCb.reg.bb25Er2);
// 3240 		RpWrEvaReg2(regSerVal);
        MOV       X, A               ;; 1 cycle
        MOV       A, #0x25           ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3241 		// CTN NOT
// 3242 		RpCb.reg.bb1AEr2 &= (uint8_t)(~0xF0);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3571  ;; 2 cycles
        AND       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3571, A  ;; 2 cycles
// 3243 		if (gainSet)
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP+0x0D]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_313  ;; 4 cycles
        ; ------------------------------------- Block: 85 cycles
// 3244 		{
// 3245 			if ((freqBandId == RP_PHY_FREQ_BAND_863MHz) || (freqBandId == RP_PHY_FREQ_BAND_870MHz))
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BZ        ??RpLog_Event_314  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0xF            ;; 1 cycle
        BNZ       ??RpLog_Event_315  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3246 			{
// 3247 				RpCb.reg.bb1AEr2 |= 0x70;
??RpLog_Event_314:
        MOV       A, X               ;; 1 cycle
        OR        A, #0x70           ;; 1 cycle
        BR        S:??RpLog_Event_316  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3248 			}
// 3249 			else
// 3250 			{
// 3251 				RpCb.reg.bb1AEr2 |= 0x60;
??RpLog_Event_315:
        MOV       A, X               ;; 1 cycle
        OR        A, #0x60           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_316:
        MOV       ES:_RpRxBuf+3571, A  ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 3252 			}
// 3253 		}
// 3254 		regSerVal = (uint16_t)(0x1A00 | RpCb.reg.bb1AEr2);
// 3255 		RpWrEvaReg2(regSerVal);
??RpLog_Event_313:
        MOV       X, ES:_RpRxBuf+3571  ;; 2 cycles
        MOV       A, #0x1A           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3256 	}
        POP       AX                 ;; 1 cycle
          CFI CFA SP+18
        BR        S:??RpLog_Event_317  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
// 3257 	else
// 3258 	{
// 3259 		gainSet = RP_ERROR;
??RpLog_Event_300:
        MOV       A, #0xFF           ;; 1 cycle
        MOV       [SP+0x0D], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 3260 	}
// 3261 
// 3262 	return (gainSet);
??RpLog_Event_317:
        MOV       A, [SP+0x0D]       ;; 1 cycle
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock50
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 302 cycles
// 3263 }
// 3264 
// 3265 /******************************************************************************
// 3266 Function Name:       RpSetAntennaSwitchVal
// 3267 Parameters:          none
// 3268 Return value:        none
// 3269 Description:         set antenna switch.
// 3270 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock51 Using cfiCommon1
          CFI Function _RpSetAntennaSwitchVal
          CFI NoCalls
        CODE
// 3271 void RpSetAntennaSwitchVal( void )
// 3272 {
_RpSetAntennaSwitchVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3273 	// ANTSW
// 3274 	RpRegWrite(BBANTSWCON, RpCb.pib.phyAntennaSwitchEna);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3427  ;; 2 cycles
        MOVW      AX, #0x400         ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock51
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 7 cycles
// 3275 }
// 3276 
// 3277 /******************************************************************************
// 3278 Function Name:       RpSetAntennaDiversityVal
// 3279 Parameters:          none
// 3280 Return value:        none
// 3281 Description:         set antenna diversity.
// 3282 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock52 Using cfiCommon1
          CFI Function _RpSetAntennaDiversityVal
        CODE
// 3283 void RpSetAntennaDiversityVal( void )
// 3284 {
_RpSetAntennaDiversityVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 3285 	uint8_t antDvrEn = RpCb.pib.phyAntennaDiversityRxEna;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3428  ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 3286 	uint8_t antAckTxSel = RpCb.pib.phyAntennaSelectAckTx;
        MOV       A, ES:_RpRxBuf+3430  ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
// 3287 	uint8_t antAckRxSel = RpCb.pib.phyAntennaSelectAckRx;
        MOV       A, ES:_RpRxBuf+3431  ;; 2 cycles
        MOV       [SP+0x03], A       ;; 1 cycle
// 3288 	uint8_t antDvrCon, antDvrCon2;
// 3289 
// 3290 	// Antenna Diversity
// 3291 	antDvrCon = RpRegRead(BBANTDIV);
        MOVW      AX, #0x5A0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
// 3292 	antDvrCon &= (~(ANTDIVEN));
// 3293 	antDvrCon &= (~(ANTACKRTNDIS | ANTACKRCVDIS));
        AND       A, #0xF2           ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 3294 	antDvrCon2 = RpRegRead(BBANTDIV2);
        MOVW      AX, #0x870         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
// 3295 	antDvrCon2 &= (~(ANTACKRTNSET | ANTACKRCVSET | ACKRTNRVS));
        AND       A, #0xF4           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 3296 	if (antDvrEn)
        MOV       A, [SP+0x04]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_318  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
// 3297 	{
// 3298 		antDvrCon |= ANTDIVEN;
        MOV       A, [SP+0x01]       ;; 1 cycle
        OR        A, #0x1            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 3299 		// AckTx Antenna Select
// 3300 		if (antAckTxSel == RP_ANT_SEL_ACK_TX_ANT0)
        MOV       A, [SP+0x02]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_319  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 3301 		{
// 3302 			// non opreation
// 3303 		}
// 3304 		else if (antAckTxSel == RP_ANT_SEL_ACK_TX_ANT1)
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_320  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3305 		{
// 3306 			antDvrCon2 |= ANTACKRTNSET;
        MOV       A, [SP]            ;; 1 cycle
        OR        A, #0x1            ;; 1 cycle
        BR        S:??RpLog_Event_321  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3307 		}
// 3308 		else
// 3309 		{
// 3310 			antDvrCon |= ANTACKRTNDIS;
??RpLog_Event_320:
        MOV       A, [SP+0x01]       ;; 1 cycle
        OR        A, #0x4            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 3311 			if (antAckTxSel == RP_ANT_SEL_ACK_TX_RCVD_INV_ANT)
        MOV       A, [SP+0x02]       ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??RpLog_Event_319  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 3312 			{
// 3313 				antDvrCon2 |= ACKRTNRVS;
        MOV       A, [SP]            ;; 1 cycle
        OR        A, #0x8            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_321:
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 3314 			}
// 3315 		}
// 3316 		// AckRx Antenna Select
// 3317 		if (antAckRxSel == RP_ANT_SEL_ACK_RX_ANT0)
??RpLog_Event_319:
        MOV       A, [SP+0x03]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_322  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3318 		{
// 3319 			// non opreation
// 3320 		}
// 3321 		else if (antAckRxSel == RP_ANT_SEL_ACK_RX_ANT1)
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_323  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3322 		{
// 3323 			antDvrCon2 |= ANTACKRCVSET;
        MOV       A, [SP]            ;; 1 cycle
        OR        A, #0x2            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpLog_Event_322  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3324 		}
// 3325 		else
// 3326 		{
// 3327 			antDvrCon |= ANTACKRCVDIS;
??RpLog_Event_323:
        MOV       A, [SP+0x01]       ;; 1 cycle
        OR        A, #0x8            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 3328 		}
// 3329 
// 3330 		RpRegWrite(BBANTDIVCON, 0x0D);
??RpLog_Event_322:
        MOV       C, #0xD            ;; 1 cycle
        MOVW      AX, #0x5A8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        BR        S:??RpLog_Event_324  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 3331 	}
// 3332 	else
// 3333 	{
// 3334 		RpRegWrite(BBANTDIVCON, 0x05);
??RpLog_Event_318:
        MOV       C, #0x5            ;; 1 cycle
        MOVW      AX, #0x5A8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3335 
// 3336 		/* Recovery Antenna select assist */
// 3337 		RpAntSelAssist_RecoveryProc();
          CFI FunCall _RpAntSelAssist_RecoveryProc
        CALL      F:_RpAntSelAssist_RecoveryProc  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 3338 	}
// 3339 	RpRegWrite(BBANTDIV, antDvrCon);
??RpLog_Event_324:
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x5A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3340 	RpRegWrite(BBANTDIV2, antDvrCon2);
        MOV       A, [SP]            ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x870         ;; 1 cycle
        BR        F:?Subroutine4     ;; 3 cycles
          CFI EndBlock cfiBlock52
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 108 cycles
// 3341 }
// 3342 
// 3343 /******************************************************************************
// 3344 Function Name:       RpSetAntennaSelectTxVal
// 3345 Parameters:          none
// 3346 Return value:        none
// 3347 Description:         set antenna select Tx.
// 3348 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock53 Using cfiCommon1
          CFI Function _RpSetAntennaSelectTxVal
        CODE
// 3349 void RpSetAntennaSelectTxVal( void )
// 3350 {
_RpSetAntennaSelectTxVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 3351 	uint8_t antDvrEn = RpCb.pib.phyAntennaDiversityRxEna;
// 3352 	uint8_t antTxSel = RpCb.pib.phyAntennaSelectTx;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3429  ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 3353 	uint8_t antDvrCon;
// 3354 
// 3355 	if (antDvrEn)
        CMP0      ES:_RpRxBuf+3428   ;; 2 cycles
        BZ        ??RpLog_Event_325  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 3356 	{
// 3357 		if (antTxSel == RP_PHY_ANTENNA_UNUSE)
        INC       A                  ;; 1 cycle
        BNZ       ??RpLog_Event_326  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3358 		{
// 3359 			antTxSel = RP_PHY_ANTENNA_0;
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 3360 			RpCb.pib.phyAntennaSelectTx = antTxSel;
        CLRB      ES:_RpRxBuf+3429   ;; 2 cycles
        ; ------------------------------------- Block: 4 cycles
// 3361 		}
// 3362 		
// 3363 		// Antenna Diversity
// 3364 		antDvrCon = RpRegRead(BBANTDIV);
??RpLog_Event_326:
        MOVW      AX, #0x5A0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 3365 		// Tx Antenna Select
// 3366 		if (antTxSel)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        BZ        ??RpLog_Event_327  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 3367 		{
// 3368 			antDvrCon |= ANTSWTRNSET;
        OR        A, #0x2            ;; 1 cycle
        BR        S:??RpLog_Event_328  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3369 		}
// 3370 		else
// 3371 		{
// 3372 			antDvrCon &= ~ANTSWTRNSET;
??RpLog_Event_327:
        AND       A, #0xFD           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_328:
        MOV       C, A               ;; 1 cycle
// 3373 		}
// 3374 		RpRegWrite(BBANTDIV, antDvrCon);
        MOVW      AX, #0x5A0         ;; 1 cycle
        BR        S:??RpLog_Event_329  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3375 	}
// 3376 	else if (antTxSel != RP_PHY_ANTENNA_UNUSE)
??RpLog_Event_325:
        INC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_330  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3377 	{
// 3378 		// Port Setting(output)
// 3379 		RpCb.reg.bbGpioDir |= (GPIO1DIR | GPIO2DIR);
        MOV       A, ES:_RpRxBuf+3563  ;; 2 cycles
        OR        A, #0x6            ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3563, A  ;; 2 cycles
// 3380 		RpRegWrite(BBGPIODIR, RpCb.reg.bbGpioDir);
        MOVW      AX, #0x410         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3381 
// 3382 		// Tx Antenna Select
// 3383 		if (antTxSel)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3564)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        BZ        ??RpLog_Event_331  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
// 3384 		{
// 3385 			RpCb.reg.bbGpioData &= ~GPIO1DATA;
// 3386 			RpCb.reg.bbGpioData |= GPIO2DATA;
        CLR1      ES:[HL].1          ;; 3 cycles
        SET1      ES:[HL].2          ;; 3 cycles
        BR        S:??RpLog_Event_332  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 3387 		}
// 3388 		else
// 3389 		{
// 3390 			RpCb.reg.bbGpioData &= ~GPIO2DATA;
// 3391 			RpCb.reg.bbGpioData |= GPIO1DATA;
??RpLog_Event_331:
        CLR1      ES:[HL].2          ;; 3 cycles
        SET1      ES:[HL].1          ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3392 		}
// 3393 		RpRegWrite(BBGPIODATA, RpCb.reg.bbGpioData);
??RpLog_Event_332:
        MOV       C, ES:_RpRxBuf+3564  ;; 2 cycles
        MOVW      AX, #0x418         ;; 1 cycle
          CFI FunCall _RpRegWrite
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_329:
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3394 	}
// 3395 }
??RpLog_Event_330:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock53
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 93 cycles
// 3396 
// 3397 /******************************************************************************
// 3398 Function Name:       RpSetCcaVthVal
// 3399 Parameters:          none
// 3400 Return value:        none
// 3401 Description:         set cca vth.
// 3402 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock54 Using cfiCommon1
          CFI Function _RpSetCcaVthVal
        CODE
// 3403 void
// 3404 RpSetCcaVthVal(void)
// 3405 {
_RpSetCcaVthVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 3406 	uint16_t ccaVth;
// 3407 	uint16_t wk16;
// 3408 
// 3409 	if (RpCmpRefVthVal(RpCb.pib.phyCcaVth) == RP_TRUE)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3368  ;; 2 cycles
          CFI FunCall _RpCmpRefVthVal
        CALL      F:_RpCmpRefVthVal  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        BNZ       ??RpLog_Event_333  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
// 3410 	{
// 3411 		ccaVth = RpCb.pib.phyCcaVth;
        MOVW      AX, ES:_RpRxBuf+3368  ;; 2 cycles
        BR        S:??RpLog_Event_334  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3412 	}
// 3413 	else
// 3414 	{
// 3415 		ccaVth = RpCb.pib.refVthVal;
??RpLog_Event_333:
        MOVW      AX, ES:_RpRxBuf+3372  ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_334:
        MOVW      [SP], AX           ;; 1 cycle
// 3416 	}
// 3417 
// 3418 	switch (RpCb.pib.phyCcaBandwidth)
        MOV       A, ES:_RpRxBuf+3448  ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_335  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpLog_Event_336  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BR        S:??RpLog_Event_337  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3419 	{
// 3420 		case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
// 3421 			wk16 = RpCcaVthOffsetTblDefault[RpCb.freqIdIndex];
??RpLog_Event_335:
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpCcaVthOffsetTblDefault)  ;; 1 cycle
        BR        S:??RpLog_Event_338  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 3422 			break;
// 3423 		case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
// 3424 		case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
// 3425 			wk16 = 0x0000;
??RpLog_Event_336:
        MOVW      AX, [SP]           ;; 1 cycle
// 3426 			break;
        BR        S:??RpLog_Event_339  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3427 		case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
// 3428 		default:
// 3429 			wk16 = RpCcaVthOffsetTblNarrow[RpCb.freqIdIndex];
??RpLog_Event_337:
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpCcaVthOffsetTblDefault+40)  ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
??RpLog_Event_338:
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCcaVthOffsetTblDefault)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
// 3430 			break;
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
// 3431 	}
// 3432 	ccaVth += wk16;
// 3433 	ccaVth += RpCb.pib.phyCcaVthOffset;
// 3434 	ccaVth &= 0x01FF;
??RpLog_Event_339:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3456  ;; 2 cycles
        CLRB      B                  ;; 1 cycle
        ADDW      AX, BC             ;; 1 cycle
        AND       A, #0x1            ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 3435 
// 3436 	// BBCCAVTH
// 3437 	RpRegBlockWrite(BBCCAVTH, (uint8_t RP_FAR *)&ccaVth, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOV       X, #0x70           ;; 1 cycle
        BR        F:?Subroutine6     ;; 3 cycles
          CFI EndBlock cfiBlock54
        ; ------------------------------------- Block: 17 cycles
        ; ------------------------------------- Total: 80 cycles
// 3438 }
// 3439 
// 3440 /******************************************************************************
// 3441 Function Name:       RpCmpRefVthVal
// 3442 Parameters:          level filter vth value
// 3443 Return value:        RP_TRUE:rewrite OK
// 3444                      RP_FALSE:rewrite NG
// 3445 Description:         compare vth and refferance.
// 3446 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock55 Using cfiCommon1
          CFI Function _RpCmpRefVthVal
        CODE
// 3447 static uint8_t RpCmpRefVthVal( uint16_t vthVal )
// 3448 {
_RpCmpRefVthVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3449 	uint8_t rtn = RP_FALSE;
        MOV       D, #0x0            ;; 1 cycle
// 3450 	int16_t signedVal;
// 3451 	int16_t signedValRef;
// 3452 
// 3453 	signedVal = (int16_t)(vthVal);
        MOVW      HL, AX             ;; 1 cycle
// 3454 	signedVal = (signedVal < 0x0100) ? signedVal : (signedVal | 0xfe00);
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8100        ;; 1 cycle
        BC        ??RpLog_Event_340  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, #0xFE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 3455 	signedValRef = (int16_t)(RpCb.pib.refVthVal);
??RpLog_Event_340:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      BC, ES:_RpRxBuf+3372  ;; 2 cycles
// 3456 	signedValRef = (signedValRef < 0x0100) ? signedValRef : (signedValRef | 0xfe00);
        MOVW      AX, BC             ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8100        ;; 1 cycle
        BC        ??RpLog_Event_341  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, BC             ;; 1 cycle
        OR        A, #0xFE           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 3457 	if (signedVal >= signedValRef)
??RpLog_Event_341:
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall ?SI_CMP_L02
        CALL      N:?SI_CMP_L02      ;; 3 cycles
        SKC                          ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 3458 	{
// 3459 		rtn =  RP_TRUE;
        MOV       D, #0x1            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 3460 	}
// 3461 
// 3462 	return (rtn);
??RpCmpRefVthVal_0:
        MOV       A, D               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock55
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 37 cycles
// 3463 }
// 3464 
// 3465 /******************************************************************************
// 3466 Function Name:       RpSetLvlVthVal
// 3467 Parameters:          none
// 3468 Return value:        none
// 3469 Description:         set lvl vth.
// 3470 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock56 Using cfiCommon1
          CFI Function _RpSetLvlVthVal
        CODE
// 3471 void RpSetLvlVthVal( void )
// 3472 {
_RpSetLvlVthVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 3473 	uint16_t lvlVth;
// 3474 
// 3475 	if (RpCmpRefVthVal(RpCb.pib.phyLvlFltrVth) == RP_TRUE)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3455  ;; 2 cycles
        CLRB      A                  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3370  ;; 2 cycles
          CFI FunCall _RpCmpRefVthVal
        CALL      F:_RpCmpRefVthVal  ;; 3 cycles
        CMP       A, #0x1            ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        BNZ       ??RpLog_Event_342  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 3476 	{
// 3477 		lvlVth = RpCb.pib.phyLvlFltrVth + RpCb.pib.phyAgcWaitGainOffset;
        MOVW      AX, ES:_RpRxBuf+3370  ;; 2 cycles
        BR        S:??RpLog_Event_343  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3478 	}
// 3479 	else
// 3480 	{
// 3481 		lvlVth = RpCb.pib.refVthVal + RpCb.pib.phyAgcWaitGainOffset;
??RpLog_Event_342:
        MOVW      AX, ES:_RpRxBuf+3372  ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_343:
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 3482 	}
// 3483 
// 3484 	// BBLVLVTH
// 3485 	RpRegBlockWrite(BBLVLVTH, (uint8_t RP_FAR *)&lvlVth, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x220         ;; 1 cycle
          CFI EndBlock cfiBlock56
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 35 cycles
        REQUIRE ?Subroutine6
        ; // Fall through to label ?Subroutine6
// 3486 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock57 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+8
          CFI FunCall _RpWrEvaReg2 _RpRegBlockWrite
          CFI FunCall _RpSetCcaVthVal _RpRegBlockWrite
          CFI FunCall _RpSetLvlVthVal _RpRegBlockWrite
          CFI FunCall _RpSetAckReplyTimeVal _RpRegBlockWrite
          CFI FunCall _RpSetAntennaDiversityStartVth _RpRegBlockWrite
        CODE
?Subroutine6:
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock57
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 3487 
// 3488 /******************************************************************************
// 3489 Function Name:       RpSetCcaDurationVal
// 3490 Parameters:          pibValReq
// 3491 Return value:        none
// 3492 Description:         set cca time.
// 3493 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock58 Using cfiCommon1
          CFI Function _RpSetCcaDurationVal
        CODE
// 3494 void
// 3495 RpSetCcaDurationVal(uint8_t pibValReq)
// 3496 {
_RpSetCcaDurationVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 3497 	uint16_t ccaDuration = RpCb.pib.phyCcaDuration;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      DE, ES:_RpRxBuf+3414  ;; 2 cycles
// 3498 	uint16_t ccaRealTime;
// 3499 	uint16_t antDivTime;
// 3500 	uint16_t antSwitchingTime = RpCb.pib.phyAntennaSwitchingTime;
        MOVW      HL, ES:_RpRxBuf+3452  ;; 2 cycles
// 3501 
// 3502 	if (pibValReq == RP_TRUE)
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_344  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 3503 	{
// 3504 		ccaRealTime = ccaDuration;
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??RpLog_Event_345  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3505 		antDivTime = ccaDuration;
// 3506 	}
// 3507 	else
// 3508 	{
// 3509 		if (antSwitchingTime == RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME)
??RpLog_Event_344:
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??RpLog_Event_345  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3510 		{
// 3511 			if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_346  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_346  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3512 			{
// 3513 				ccaRealTime = (uint16_t)RpFreqBandTbl[RpCb.freqIdIndex][RP_CCA_ANTDVT_ON_ARIB];
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x7           ;; 1 cycle
        BR        S:??RpLog_Event_347  ;; 3 cycles
        ; ------------------------------------- Block: 19 cycles
// 3514 				antDivTime = (uint16_t)RpFreqBandTbl[RpCb.freqIdIndex][RP_CCA_ANTDVT_ON_ARIB];
// 3515 			}
// 3516 			else
// 3517 			{
// 3518 				ccaRealTime = (uint16_t)RpFreqBandTbl[RpCb.freqIdIndex][RP_CCA_ANTDVT_ON];
??RpLog_Event_346:
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        ; ------------------------------------- Block: 16 cycles
??RpLog_Event_347:
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 3519 				antDivTime = (uint16_t)RpFreqBandTbl[RpCb.freqIdIndex][RP_CCA_ANTDVT_ON];
// 3520 			}
// 3521 		}
// 3522 		else
// 3523 		{
// 3524 			ccaRealTime = antSwitchingTime;
??RpLog_Event_345:
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 3525 			antDivTime = antSwitchingTime;
        MOVW      [SP], AX           ;; 1 cycle
// 3526 		}
// 3527 	}
// 3528 	// BBCCATIME
// 3529 	RpRegBlockWrite(BBCCATIME, (uint8_t RP_FAR *)&ccaRealTime, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x590         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3530 	// Antenna Switching Time
// 3531 	RpRegBlockWrite(BBANTDIVTIM, (uint8_t RP_FAR *)&antDivTime, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x670         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3532 }
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock58
        ; ------------------------------------- Block: 33 cycles
        ; ------------------------------------- Total: 106 cycles
// 3533 
// 3534 /******************************************************************************
// 3535 Function Name:       RpInverseTxAnt
// 3536 Parameters:          invAntPortEna
// 3537 Return value:        none
// 3538 Description:         set cca time.
// 3539 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock59 Using cfiCommon1
          CFI Function _RpInverseTxAnt
        CODE
// 3540 void
// 3541 RpInverseTxAnt(uint8_t invAntPortEna)
// 3542 {
_RpInverseTxAnt:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 4
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
// 3543 	uint8_t antDvrEn = RpCb.pib.phyAntennaDiversityRxEna;
// 3544 	uint8_t antTxSel = RpCb.pib.phyAntennaSelectTx;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3429  ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 3545 	uint8_t antDvrCon;
// 3546 
// 3547 	if (antDvrEn)
        CMP0      ES:_RpRxBuf+3428   ;; 2 cycles
        BZ        ??RpLog_Event_348  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 3548 	{
// 3549 		antDvrCon = RpRegRead(BBANTDIV);
        MOVW      AX, #0x5A0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       X, A               ;; 1 cycle
// 3550 
// 3551 		if (antTxSel ^ invAntPortEna)
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XOR       A, B               ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        BZ        ??RpLog_Event_349  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
// 3552 		{
// 3553 			antDvrCon |= ANTSWTRNSET;
        OR        A, #0x2            ;; 1 cycle
        BR        S:??RpLog_Event_350  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3554 		}
// 3555 		else
// 3556 		{
// 3557 			antDvrCon &= ~ANTSWTRNSET;
??RpLog_Event_349:
        AND       A, #0xFD           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_350:
        MOV       C, A               ;; 1 cycle
// 3558 		}
// 3559 		RpRegWrite(BBANTDIV, antDvrCon);
        MOVW      AX, #0x5A0         ;; 1 cycle
        BR        S:??RpLog_Event_351  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3560 	}
// 3561 	else if (antTxSel != RP_PHY_ANTENNA_UNUSE)
??RpLog_Event_348:
        INC       A                  ;; 1 cycle
        BZ        ??RpLog_Event_352  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 3562 	{
// 3563 #ifdef RP_NOT_INVERT_TX_ANNTENA
// 3564 		if (antTxSel)
// 3565 #else
// 3566 		if (antTxSel ^ invAntPortEna)
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XOR       A, X               ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3564)  ;; 1 cycle
        BZ        ??RpLog_Event_353  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 3567 #endif
// 3568 		{
// 3569 			RpCb.reg.bbGpioData &= ~GPIO1DATA;
// 3570 			RpCb.reg.bbGpioData |= GPIO2DATA;
        CLR1      ES:[HL].1          ;; 3 cycles
        SET1      ES:[HL].2          ;; 3 cycles
        BR        S:??RpLog_Event_354  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 3571 		}
// 3572 		else
// 3573 		{
// 3574 			RpCb.reg.bbGpioData &= ~GPIO2DATA;
// 3575 			RpCb.reg.bbGpioData |= GPIO1DATA;
??RpLog_Event_353:
        CLR1      ES:[HL].2          ;; 3 cycles
        SET1      ES:[HL].1          ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3576 		}
// 3577 		RpRegWrite(BBGPIODATA, RpCb.reg.bbGpioData);
??RpLog_Event_354:
        MOV       C, ES:_RpRxBuf+3564  ;; 2 cycles
        MOVW      AX, #0x418         ;; 1 cycle
          CFI FunCall _RpRegWrite
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_351:
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3578 	}
// 3579 }
??RpLog_Event_352:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock59
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 78 cycles
// 3580 
// 3581 /******************************************************************************
// 3582 Function Name:       RpSetPreambleLengthVal
// 3583 Parameters:          none
// 3584 Return value:        none
// 3585 Description:         set preamble length.
// 3586 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock60 Using cfiCommon1
          CFI Function _RpSetPreambleLengthVal
          CFI NoCalls
        CODE
// 3587 void RpSetPreambleLengthVal( void )
// 3588 {
_RpSetPreambleLengthVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3589 	// BBPAMBL
// 3590 	RpRegBlockWrite(BBPAMBL, (uint8_t RP_FAR *)&RpCb.pib.phyFskPreambleLength, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3416)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x530         ;; 1 cycle
        BR        F:?Subroutine8     ;; 3 cycles
          CFI EndBlock cfiBlock60
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
// 3591 }
// 3592 
// 3593 /******************************************************************************
// 3594 Function Name:       RpSetMrFskSfdVal
// 3595 Parameters:          none
// 3596 Return value:        none
// 3597 Description:         set SFD.
// 3598 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock61 Using cfiCommon1
          CFI Function _RpSetMrFskSfdVal
        CODE
// 3599 void RpSetMrFskSfdVal( void )
// 3600 {
_RpSetMrFskSfdVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 16
        SUBW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+20
// 3601 	uint8_t sfdMode = RpCb.pib.phyMrFskSfd, opeMode = RpCb.pib.phyFskOpeMode, freqBandId = RpCb.pib.phyFreqBandId;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       B, ES:_RpRxBuf+3418  ;; 2 cycles
        MOV       X, ES:_RpRxBuf+3420  ;; 2 cycles
        MOV       A, ES:_RpRxBuf+3436  ;; 2 cycles
// 3602 	uint32_t sfdVal1, sfdVal2, sfdVal3, sfdVal4;
// 3603 
// 3604 	// BBSFD(1) - BBSFD4
// 3605 	if (((freqBandId == RP_PHY_FREQ_BAND_920MHz) && (opeMode == RP_PHY_FSK_OPEMODE_4))
// 3606 			|| ((freqBandId == RP_PHY_FREQ_BAND_863MHz) && (opeMode == RP_PHY_FSK_OPEMODE_3)))
        CMP       A, #0x9            ;; 1 cycle
        BNZ       ??RpLog_Event_355  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BZ        ??RpLog_Event_356  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3607 	{
// 3608 		sfdVal1 = RP_SFDVAL_MD0_4FSKNOFEC;
// 3609 		sfdVal2 = RP_SFDVAL_MD0_4FSKFEC;
// 3610 		sfdVal3 = RP_SFDVAL_MD1_4FSKNOFEC;
// 3611 		sfdVal4 = RP_SFDVAL_MD1_4FSKFEC;
// 3612 	}
// 3613 	else
// 3614 	{
// 3615 		sfdVal1 = RP_SFDVAL_MD0_2FSKNOFEC;
??RpSetMrFskSfdVal_0:
        MOVW      AX, #0x7209        ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
// 3616 		sfdVal2 = RP_SFDVAL_MD0_2FSKFEC;
        MOV       X, #0xF6           ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
// 3617 		sfdVal3 = RP_SFDVAL_MD1_2FSKNOFEC;
        MOVW      AX, #0x705E        ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 3618 		sfdVal4 = RP_SFDVAL_MD1_2FSKFEC;
        MOVW      AX, #0xB4C6        ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
??RpSetMrFskSfdVal_1:
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 3619 	}
// 3620 
// 3621 	if (sfdMode)
        CMP0      B                  ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3565)  ;; 1 cycle
        BZ        ??RpLog_Event_357  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 3622 	{
// 3623 		RpCb.reg.bbFecCon |= MRFSKSFD;
        SET1      ES:[HL].6          ;; 3 cycles
        BR        S:??RpLog_Event_358  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3624 	}
??RpLog_Event_355:
        CMP       A, #0x4            ;; 1 cycle
        BNZ       ??RpSetMrFskSfdVal_0  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??RpSetMrFskSfdVal_0  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpLog_Event_356:
        MOVW      AX, #0xAAEB        ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      AX, #0xBFAE        ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, #0xFFBE        ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, #0xBFAE        ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, #0xBBFE        ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, #0xBFAA        ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, #0xFABE        ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, #0xEFBA        ;; 1 cycle
        BR        S:??RpSetMrFskSfdVal_1  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
// 3625 	else
// 3626 	{
// 3627 		RpCb.reg.bbFecCon &= ~MRFSKSFD;
??RpLog_Event_357:
        CLR1      ES:[HL].6          ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3628 	}
// 3629 	RpRegWrite(BBFECCON, (uint8_t)(RpCb.reg.bbFecCon));
??RpLog_Event_358:
        MOV       C, ES:_RpRxBuf+3565  ;; 2 cycles
        MOVW      AX, #0x860         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3630 
// 3631 	RpRegBlockWrite(BBSFD, (uint8_t RP_FAR *)&sfdVal1, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x610         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3632 	RpRegBlockWrite(BBSFD2, (uint8_t RP_FAR *)&sfdVal2, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x800         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3633 	RpRegBlockWrite(BBSFD3, (uint8_t RP_FAR *)&sfdVal3, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x820         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3634 	RpRegBlockWrite(BBSFD4, (uint8_t RP_FAR *)&sfdVal4, sizeof(uint32_t));
        MOVW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x840         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3635 }
        ADDW      SP, #0x18          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock61
        ; ------------------------------------- Block: 53 cycles
        ; ------------------------------------- Total: 128 cycles
// 3636 
// 3637 /******************************************************************************
// 3638 Function Name:       RpSetFskScramblePsduVal
// 3639 Parameters:          none
// 3640 Return value:        none
// 3641 Description:         set Scramble PSDU enable.
// 3642 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock62 Using cfiCommon1
          CFI Function _RpSetFskScramblePsduVal
          CFI NoCalls
        CODE
// 3643 void RpSetFskScramblePsduVal( void )
// 3644 {
_RpSetFskScramblePsduVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3645 	if (RpCb.pib.phyFskScramblePsdu)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3419   ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3566)  ;; 1 cycle
        BZ        ??RpLog_Event_359  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 3646 	{
// 3647 		RpCb.reg.bbSubCon |= DWEN;
        SET1      ES:[HL].3          ;; 3 cycles
        BR        S:??RpLog_Event_360  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3648 	}
// 3649 	else
// 3650 	{
// 3651 		RpCb.reg.bbSubCon &= ~DWEN;
??RpLog_Event_359:
        CLR1      ES:[HL].3          ;; 3 cycles
          CFI EndBlock cfiBlock62
        ; ------------------------------------- Block: 3 cycles
// 3652 	}
// 3653 	RpRegWrite(BBSUBGCON, (uint8_t)(RpCb.reg.bbSubCon));
??RpLog_Event_360:
        REQUIRE ?Subroutine11
        ; // Fall through to label ?Subroutine11
// 3654 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock63 Using cfiCommon1
          CFI NoFunction
        CODE
?Subroutine11:
        MOV       C, ES:_RpRxBuf+3566  ;; 2 cycles
        MOVW      AX, #0x580         ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock63
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 6 cycles
// 3655 
// 3656 /******************************************************************************
// 3657 Function Name:       RpSetFcsLengthVal
// 3658 Parameters:          none
// 3659 Return value:        none
// 3660 Description:         set FCS length.
// 3661 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock64 Using cfiCommon1
          CFI Function _RpSetFcsLengthVal
          CFI NoCalls
        CODE
// 3662 void
// 3663 RpSetFcsLengthVal(void)
// 3664 {
_RpSetFcsLengthVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3665 	if (RpCb.pib.phyFcsLength == RP_FCS_LENGTH_16BIT)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3421, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_361  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 3666 	{
// 3667 		RpCb.reg.bbSubCon |= CRCBIT;
        MOVW      HL, #LWRD(_RpRxBuf+3566)  ;; 1 cycle
        SET1      ES:[HL].2          ;; 3 cycles
        BR        S:??RpLog_Event_362  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 3668 	}
// 3669 	else if (RpCb.pib.phyFcsLength == RP_FCS_LENGTH_32BIT)
??RpLog_Event_361:
        CMP       ES:_RpRxBuf+3421, #0x4  ;; 2 cycles
        BNZ       ??RpLog_Event_362  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3670 	{
// 3671 		RpCb.reg.bbSubCon &= ~CRCBIT;
        MOVW      HL, #LWRD(_RpRxBuf+3566)  ;; 1 cycle
        CLR1      ES:[HL].2          ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3672 	}
// 3673 	RpRegWrite(BBSUBGCON, (uint8_t)(RpCb.reg.bbSubCon));
??RpLog_Event_362:
        BR        F:?Subroutine11    ;; 3 cycles
          CFI EndBlock cfiBlock64
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 27 cycles
// 3674 }
// 3675 
// 3676 /******************************************************************************
// 3677 Function Name:       RpSetAckReplyTimeVal
// 3678 Parameters:          none
// 3679 Return value:        none
// 3680 Description:         set ack reply time.
// 3681 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock65 Using cfiCommon1
          CFI Function _RpSetAckReplyTimeVal
          CFI NoCalls
        CODE
// 3682 void RpSetAckReplyTimeVal( void )
// 3683 {
_RpSetAckReplyTimeVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 3684 	uint16_t ackReply = (uint16_t)(RpCb.pib.phyAckReplyTime - RP_ACKTX_WARMUP_TIME);
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3434  ;; 2 cycles
        MOVW      BC, #0x15E         ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3422  ;; 2 cycles
        SUBW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 3685 
// 3686 	// BBACKRTNTIM
// 3687 	RpRegBlockWrite(BBACKRTNTIM, (uint8_t RP_FAR *)&ackReply, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x230         ;; 1 cycle
        BR        F:?Subroutine6     ;; 3 cycles
          CFI EndBlock cfiBlock65
        ; ------------------------------------- Block: 43 cycles
        ; ------------------------------------- Total: 43 cycles
// 3688 }
// 3689 
// 3690 /******************************************************************************
// 3691 Function Name:       RpSetAckWaitDurationVal
// 3692 Parameters:          none
// 3693 Return value:        none
// 3694 Description:         set ack wait duration.
// 3695 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock66 Using cfiCommon1
          CFI Function _RpSetAckWaitDurationVal
          CFI NoCalls
        CODE
// 3696 void RpSetAckWaitDurationVal( void )
// 3697 {
_RpSetAckWaitDurationVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3698 	// BBACKRCVWIT
// 3699 	RpRegBlockWrite(BBACKRCVWIT, (uint8_t RP_FAR *)&RpCb.pib.phyAckWaitDuration, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3424)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x380         ;; 1 cycle
          CFI EndBlock cfiBlock66
        ; ------------------------------------- Block: 5 cycles
        ; ------------------------------------- Total: 5 cycles
        REQUIRE ?Subroutine8
        ; // Fall through to label ?Subroutine8
// 3700 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock67 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+6
          CFI FunCall _RpAddressFilterSetting _RpRegBlockWrite
          CFI FunCall _RpSetPreambleLengthVal _RpRegBlockWrite
          CFI FunCall _RpSetAckWaitDurationVal _RpRegBlockWrite
          CFI FunCall _RpSetCsmaBackoffPeriod _RpRegBlockWrite
          CFI FunCall _RpSetAntennaSwitchEnaTiming _RpRegBlockWrite
        CODE
?Subroutine8:
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock67
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 3701 
// 3702 /******************************************************************************
// 3703 Function Name:       RpSetCsmaBackoffPeriod
// 3704 Parameters:          seed
// 3705 Return value:        none
// 3706 Description:         set backoff period.
// 3707 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock68 Using cfiCommon1
          CFI Function _RpSetCsmaBackoffPeriod
          CFI NoCalls
        CODE
// 3708 void RpSetCsmaBackoffPeriod( void )
// 3709 {
_RpSetCsmaBackoffPeriod:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3710 	RpRegBlockWrite(BBBOFFPERIOD, (uint8_t RP_FAR *)&RpCb.pib.phyCsmaBackoffPeriod, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3442)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x250         ;; 1 cycle
        BR        F:?Subroutine8     ;; 3 cycles
          CFI EndBlock cfiBlock68
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
// 3711 }
// 3712 
// 3713 /******************************************************************************
// 3714 Function Name:       RpSetBackOffSeedVal
// 3715 Parameters:          seed
// 3716 Return value:        none
// 3717 Description:         set SFD.
// 3718 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock69 Using cfiCommon1
          CFI Function _RpSetBackOffSeedVal
        CODE
// 3719 void RpSetBackOffSeedVal( void )
// 3720 {
_RpSetBackOffSeedVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3721 	uint8_t seed = RpCb.pib.phyBackOffSeed;
// 3722 
// 3723 	// BBBOFFPROD0 and BBBOFFPROD2
// 3724 	RpRegWrite(BBBOFFPROD2, seed);									// Seed set
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3375  ;; 2 cycles
        MOVW      AX, #0x348         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3725 	RpRegWrite(BBBOFFPROD, (uint8_t)(BOFFPRODEN));	// Backoff periode Auto enaable
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x1A8         ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock69
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 12 cycles
// 3726 }
// 3727 
// 3728 /******************************************************************************
// 3729 Function Name:       RpSetRssiOffsetVal
// 3730 Parameters:          none
// 3731 Return value:        none
// 3732 Description:         set rssioffset.
// 3733 ******************************************************************************/
// 3734 static void RpSetRssiOffsetVal( void )
// 3735 {
// 3736 	// RSSIOFFSET
// 3737 	RpRegWrite((0x0483 << 3), (uint8_t)(RpCb.pib.phyRssiLoss & 0x3F));
// 3738 }
// 3739 
// 3740 /******************************************************************************
// 3741 Function Name:       RpSetFecVal
// 3742 Parameters:          none
// 3743 Return value:        none
// 3744 Description:         set FEC.
// 3745 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock70 Using cfiCommon1
          CFI Function _RpSetFecVal
        CODE
// 3746 void RpSetFecVal( void )
// 3747 {
_RpSetFecVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 3748 	uint8_t fecTxEn = RpCb.pib.phyFskFecTxEna, fecRxEn = RpCb.pib.phyFskFecRxEna, fecMode = RpCb.pib.phyFskFecScheme;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3365  ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3366  ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3367  ;; 2 cycles
        MOV       [SP+0x03], A       ;; 1 cycle
// 3749 	uint8_t rscCon;
// 3750 
// 3751 	// BBFECCON
// 3752 	RpCb.reg.bbSubCon &= ~(FECENRX | FECMODE | INTERLEAVEEN);
        MOV       A, ES:_RpRxBuf+3566  ;; 2 cycles
        AND       A, #0xBC           ;; 1 cycle
        MOV       ES:_RpRxBuf+3566, A  ;; 2 cycles
// 3753 	RpCb.reg.bbFecCon &= ~(FECAUTOEN | FECENTX | FECCONACKRTN | FECENAACKRTN | FECCONACKRCV | FECENAACKRCV);
        MOV       A, ES:_RpRxBuf+3565  ;; 2 cycles
        AND       A, #0xC0           ;; 1 cycle
        MOV       ES:_RpRxBuf+3565, A  ;; 2 cycles
// 3754 	// MDMCNT40F
// 3755 	rscCon = RpRegRead(0x040F << 3);
        MOVW      AX, #0x2078        ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
// 3756 	rscCon &= (~INTERLEAVEEN);
        AND       A, #0xBF           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 3757 	if (fecTxEn)
        MOV       A, [SP+0x02]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_363  ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
// 3758 	{
// 3759 		RpCb.reg.bbFecCon |= FECENTX;
// 3760 		RpCb.reg.bbFecCon |= FECCONACKRTN;
// 3761 		RpCb.reg.bbFecCon |= FECENAACKRTN;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3565  ;; 2 cycles
        OR        A, #0xE            ;; 1 cycle
        MOV       ES:_RpRxBuf+3565, A  ;; 2 cycles
        ; ------------------------------------- Block: 6 cycles
// 3762 	}
// 3763 	if (fecRxEn)
??RpLog_Event_363:
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_364  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3764 	{
// 3765 		RpCb.reg.bbSubCon |= FECENRX;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:_RpRxBuf+3566.0  ;; 3 cycles
// 3766 		RpCb.reg.bbFecCon |= FECENAACKRCV;
        MOVW      HL, #LWRD(_RpRxBuf+3565)  ;; 1 cycle
        SET1      ES:[HL].5          ;; 3 cycles
// 3767 		if (fecRxEn == RP_FEC_RX_MODE_AUTO_DISTINCT)
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_365  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
// 3768 		{
// 3769 			RpCb.reg.bbFecCon |= FECAUTOEN;
        SET1      ES:[HL].0          ;; 3 cycles
        BR        S:??RpLog_Event_364  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3770 		}
// 3771 		else
// 3772 		{
// 3773 			RpCb.reg.bbFecCon |= FECCONACKRCV;
??RpLog_Event_365:
        SET1      ES:[HL].4          ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3774 		}
// 3775 	}
// 3776 	if (fecMode)
??RpLog_Event_364:
        MOV       A, [SP+0x03]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_366  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3777 	{
// 3778 		RpCb.reg.bbSubCon |= FECMODE;
        MOVW      HL, #LWRD(_RpRxBuf+3566)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:[HL].1          ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 3779 	}
// 3780 	if (fecTxEn || fecRxEn)
??RpLog_Event_366:
        MOV       A, [SP+0x02]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_367  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpLog_Event_368  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 3781 	{
// 3782 		rscCon |= INTERLEAVEEN;
??RpLog_Event_367:
        MOV       A, [SP]            ;; 1 cycle
        OR        A, #0x40           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 3783 		RpCb.reg.bbSubCon |= INTERLEAVEEN;
        MOVW      HL, #LWRD(_RpRxBuf+3566)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:[HL].6          ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 3784 	}
// 3785 
// 3786 	RpRegWrite(BBSUBGCON, RpCb.reg.bbSubCon);
??RpLog_Event_368:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3566  ;; 2 cycles
        MOVW      AX, #0x580         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3787 	RpRegWrite(BBFECCON, RpCb.reg.bbFecCon);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3565  ;; 2 cycles
        MOVW      AX, #0x860         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3788 	RpRegWrite(0x040F << 3, rscCon);
        MOV       A, [SP]            ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2078        ;; 1 cycle
        BR        F:?Subroutine5     ;; 3 cycles
          CFI EndBlock cfiBlock70
        ; ------------------------------------- Block: 20 cycles
        ; ------------------------------------- Total: 118 cycles
// 3789 }
// 3790 
// 3791 /******************************************************************************
// 3792 Function Name:       RpSetMaxCsmaBackoffVal
// 3793 Parameters:          none
// 3794 Return value:        none
// 3795 Description:         set RpSetMaxCsmaBackoff
// 3796 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock71 Using cfiCommon1
          CFI Function _RpSetMaxCsmaBackoffVal
          CFI NoCalls
        CODE
// 3797 void RpSetMaxCsmaBackoffVal( void )
// 3798 {
_RpSetMaxCsmaBackoffVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3799 	// BBCSMACON1
// 3800 	RpCb.reg.bbCsmaCon1 &= (~NB);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3561  ;; 2 cycles
        AND       A, #0xF8           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3561, A  ;; 2 cycles
// 3801 	RpCb.reg.bbCsmaCon1 |= (RpCb.pib.macMaxCsmaBackOff & NB);
        MOV       A, ES:_RpRxBuf+3410  ;; 2 cycles
        AND       A, #0x7            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3561, A  ;; 2 cycles
// 3802 	RpRegWrite(BBCSMACON1, RpCb.reg.bbCsmaCon1);
        MOVW      AX, #0x90          ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock71
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 18 cycles
// 3803 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock72 Using cfiCommon1
          CFI NoFunction
        CODE
?Subroutine0:
        MOVW      AX, #0x21B0        ;; 1 cycle
          CFI FunCall _RpRegCcaBandwidth200k _RpRegWrite
          CFI FunCall _RpRegCcaBandwidth150k _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       C, #0xBE           ;; 1 cycle
        MOVW      AX, #0x21D0        ;; 1 cycle
          CFI FunCall _RpRegCcaBandwidth200k _RpRegWrite
          CFI FunCall _RpRegCcaBandwidth150k _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       C, #0x81           ;; 1 cycle
        MOVW      AX, #0x2440        ;; 1 cycle
          CFI EndBlock cfiBlock72
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 11 cycles
        REQUIRE ??Subroutine12_0
        ; // Fall through to label ??Subroutine12_0

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock73 Using cfiCommon1
          CFI NoFunction
          CFI FunCall _RpRxOnStart _RpRegWrite
          CFI FunCall _RpSetAntennaSwitchVal _RpRegWrite
          CFI FunCall _RpSetFskScramblePsduVal _RpRegWrite
          CFI FunCall _RpSetFcsLengthVal _RpRegWrite
          CFI FunCall _RpSetBackOffSeedVal _RpRegWrite
          CFI FunCall _RpSetMaxCsmaBackoffVal _RpRegWrite
          CFI FunCall _RpSetMaxBeVal _RpRegWrite
          CFI FunCall _RpSetPreamble4ByteRxMode _RpRegWrite
          CFI FunCall _RpSetGpio0Setting _RpRegWrite
          CFI FunCall _RpSetGpio3Setting _RpRegWrite
          CFI FunCall _RpRegAdcVgaModify _RpRegWrite
          CFI FunCall _RpRegTxRxDataRate100kbps _RpRegWrite
          CFI FunCall _RpRegCcaBandwidth200k _RpRegWrite
          CFI FunCall _RpRegCcaBandwidth150k _RpRegWrite
        CODE
??Subroutine12_0:
        CALL      F:_RpRegWrite      ;; 3 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock73
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
// 3804 
// 3805 /******************************************************************************
// 3806 Function Name:       RpSetMaxBeVal
// 3807 Parameters:          none
// 3808 Return value:        none
// 3809 Description:         set RpSetMaxBe
// 3810 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock74 Using cfiCommon1
          CFI Function _RpSetMaxBeVal
          CFI NoCalls
        CODE
// 3811 void RpSetMaxBeVal( void )
// 3812 {
_RpSetMaxBeVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3813 	// BBCSMACON2
// 3814 	RpCb.reg.bbCsmaCon2 &= ~(BEMAX);
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3562  ;; 2 cycles
        AND       A, #0xF0           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3562, A  ;; 2 cycles
// 3815 	RpCb.reg.bbCsmaCon2 |= (RpCb.pib.macMaxBe & BEMAX);
        MOV       A, ES:_RpRxBuf+3412  ;; 2 cycles
        AND       A, #0xF            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3562, A  ;; 2 cycles
// 3816 	RpRegWrite(BBCSMACON2, RpCb.reg.bbCsmaCon2);
        MOVW      AX, #0x98          ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock74
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 18 cycles
// 3817 }
// 3818 
// 3819 /******************************************************************************
// 3820 Function Name:       RpSetMiscellaneousIniVal
// 3821 Parameters:          none
// 3822 Return value:        none
// 3823 Description:         set miscellaneous inital value
// 3824 ******************************************************************************/
// 3825 static void RpSetMiscellaneousIniVal( void )
// 3826 {
// 3827 	// BBTXRXMODE2
// 3828 	RpCb.reg.bbTxRxMode2 = ENHACKEN;			// Enhanced Ack Enable always set
// 3829 	RpRegWrite(BBTXRXMODE2, RpCb.reg.bbTxRxMode2);	// Auto CRC,Retry=0
// 3830 
// 3831 	// BBTXRXMODE3
// 3832 	RpCb.reg.bbTxRxMode3 |= ADFGENMODE;			// General Mode Address Filter
// 3833 	RpCb.reg.bbTxRxMode3 |= RCVSTOREBANKSEL;
// 3834 	RpCb.reg.bbTxRxMode3 |= (RXEN | LVLFILEN);
// 3835 	RpRegWrite(BBTXRXMODE3, RpCb.reg.bbTxRxMode3);
// 3836 }
// 3837 
// 3838 /******************************************************************************
// 3839 Function Name:       RpSetSpecificModeVal
// 3840 Parameters:          afterReset
// 3841 Return value:        none
// 3842 Description:         set specific mode
// 3843 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock75 Using cfiCommon1
          CFI Function _RpSetSpecificModeVal
        CODE
// 3844 void RpSetSpecificModeVal( uint8_t afterReset )
// 3845 {
_RpSetSpecificModeVal:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 3846 	// BBTXRXMODE0
// 3847 	RpCb.reg.bbTxRxMode0 |= CCATYPE0;			// CCA,Auto Ack Disable,Auto Receive Disable
        MOVW      HL, #LWRD(_RpRxBuf+3555)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SET1      ES:[HL].0          ;; 3 cycles
// 3848 	if (RpCb.pib.phyProfileSpecificMode != RP_SPECIFIC_WSUN)
        CMP       ES:_RpRxBuf+3426, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_369  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 3849 	{
// 3850 		RpCb.reg.bbTxRxMode0 |= INTRAPANEN;
        SET1      ES:[HL].7          ;; 3 cycles
        BR        S:??RpLog_Event_370  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3851 	}
// 3852 	else
// 3853 	{
// 3854 		RpCb.reg.bbTxRxMode0 &= (uint8_t)(~INTRAPANEN);
??RpLog_Event_369:
        CLR1      ES:[HL].7          ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_370:
        MOV       [SP], A            ;; 1 cycle
// 3855 	}
// 3856 	RpRegWrite(BBTXRXMODE0, RpCb.reg.bbTxRxMode0);
        MOV       C, ES:_RpRxBuf+3555  ;; 2 cycles
        MOVW      AX, #0x10          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 3857 
// 3858 	// BBCSMACON2
// 3859 	if (afterReset == RP_FALSE)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_371  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
// 3860 	{
// 3861 		if (RpCb.pib.phyProfileSpecificMode == RP_SPECIFIC_WSUN)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3426, #0x1  ;; 2 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3562)  ;; 1 cycle
        BNZ       ??RpLog_Event_372  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 3862 		{
// 3863 			RpCb.reg.bbCsmaCon2 |= UNICASTFRMEN;
        SET1      ES:[HL].5          ;; 3 cycles
        BR        S:??RpLog_Event_373  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3864 		}
// 3865 		else
// 3866 		{
// 3867 			RpCb.reg.bbCsmaCon2 &= (~UNICASTFRMEN);
??RpLog_Event_372:
        CLR1      ES:[HL].5          ;; 3 cycles
          CFI FunCall _RpSetMaxBeVal
        ; ------------------------------------- Block: 3 cycles
// 3868 		}
// 3869 		RpSetMaxBeVal();
??RpLog_Event_373:
        CALL      F:_RpSetMaxBeVal   ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 3870 	}
// 3871 
// 3872 	// BBTXRXMODE1
// 3873 	RpCb.reg.bbTxRxMode1 &= (~SQCNUMSUPEN);
// 3874 	RpCb.reg.bbTxRxMode1 |= ACKRCVPOINT;
??RpLog_Event_371:
        MOVW      HL, #LWRD(_RpRxBuf+3556)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:[HL].6          ;; 3 cycles
        SET1      ES:[HL].7          ;; 3 cycles
// 3875 
// 3876 	RpRegWrite(BBTXRXMODE1, RpCb.reg.bbTxRxMode1);	// Auto Ack Receive Enable
        MOV       C, ES:_RpRxBuf+3556  ;; 2 cycles
        MOVW      AX, #0x18          ;; 1 cycle
        BR        F:?Subroutine7     ;; 3 cycles
          CFI EndBlock cfiBlock75
        ; ------------------------------------- Block: 14 cycles
        ; ------------------------------------- Total: 68 cycles
// 3877 }
// 3878 
// 3879 /******************************************************************************
// 3880 Function Name:       RpSetPreamble4ByteRxMode
// 3881 Parameters:          none
// 3882 Return value:        none
// 3883 Description:         Preamble 4Byte Rx Mode.
// 3884 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock76 Using cfiCommon1
          CFI Function _RpSetPreamble4ByteRxMode
          CFI NoCalls
        CODE
// 3885 void RpSetPreamble4ByteRxMode( void )
// 3886 {
_RpSetPreamble4ByteRxMode:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3887 	uint8_t	val0x0472;
// 3888 
// 3889 	if (RpCb.pib.phyPreamble4ByteRxMode == RP_TRUE)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3444, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_374  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 3890 	{
// 3891 		val0x0472 = 0x04;
        MOV       C, #0x4            ;; 1 cycle
        BR        S:??RpLog_Event_375  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 3892 	}
// 3893 	else
// 3894 	{
// 3895 		val0x0472 = 0x05;
??RpLog_Event_374:
        MOV       C, #0x5            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 3896 	}
// 3897 
// 3898 	RpRegWrite((0x0472 << 3), val0x0472);
??RpLog_Event_375:
        MOVW      AX, #0x2390        ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock76
        ; ------------------------------------- Block: 4 cycles
        ; ------------------------------------- Total: 16 cycles
// 3899 }
// 3900 
// 3901 /******************************************************************************
// 3902 Function Name:       RpSetAgcStartVth
// 3903 Parameters:          reset
// 3904 Return value:        none
// 3905 Description:         AGC Start Threshold Setting
// 3906 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock77 Using cfiCommon1
          CFI Function _RpSetAgcStartVth
        CODE
// 3907 void RpSetAgcStartVth( uint8_t reset )
// 3908 {
_RpSetAgcStartVth:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 3909 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
// 3910 	uint16_t reg;
// 3911 
// 3912 	if (reset || (RpCb.pib.phyAgcStartVth == RP_RF_PIB_DFLT_AGC_START_VTH))
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_376  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOVW      HL, ES:_RpRxBuf+3446  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        BNZ       ??RpLog_Event_377  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 3913 	{
// 3914 		RpCb.pib.phyAgcStartVth = RP_RF_PIB_DFLT_AGC_START_VTH;
??RpLog_Event_376:
        MOVW      HL, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      ES:_RpRxBuf+3446, AX  ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
// 3915 
// 3916 		if (RpCb.pib.phyFskFecRxEna == RP_FEC_RX_MODE_ENABLE)
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        CMP       ES:_RpRxBuf+3366, #0x1  ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BNZ       ??RpLog_Event_378  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
// 3917 		{
// 3918 			reg = ((uint16_t)(RpFreqBandTbl[index][RP_0x0458_F_OFFSET]) << 8) | (RpFreqBandTbl[index][RP_0x0457_F_OFFSET]);
        ADDW      AX, #0x23          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x25          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        BR        S:??RpLog_Event_379  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
// 3919 		}
// 3920 		else
// 3921 		{
// 3922 			reg = ((uint16_t)(RpFreqBandTbl[index][RP_0x0458_OFFSET]) << 8) | (RpFreqBandTbl[index][RP_0x0457_OFFSET]);
??RpLog_Event_378:
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x26          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        BR        S:??RpLog_Event_379  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
// 3923 		}
// 3924 	}
// 3925 	else
// 3926 	{
// 3927 		reg = RpCb.pib.phyAgcStartVth;
??RpLog_Event_377:
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_379:
        MOVW      [SP], AX           ;; 1 cycle
// 3928 	}
// 3929 	RpRegBlockWrite((0x0457 << 3), (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x22B8        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 3930 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock77
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 97 cycles
// 3931 
// 3932 /******************************************************************************
// 3933 Function Name:       RpGetAgcStartVth
// 3934 Parameters:          none
// 3935 Return value:        none
// 3936 Description:         AGC Start Threshold Setting
// 3937 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock78 Using cfiCommon1
          CFI Function _RpGetAgcStartVth
          CFI NoCalls
        CODE
// 3938 uint16_t RpGetAgcStartVth( void )
// 3939 {
_RpGetAgcStartVth:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 3940 	uint16_t reg;
// 3941 
// 3942 	RpRegBlockRead((0x0457 << 3), (uint8_t *)&reg, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x22B8        ;; 1 cycle
          CFI EndBlock cfiBlock78
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
        REQUIRE ?Subroutine2
        ; // Fall through to label ?Subroutine2
// 3943 	return reg;
// 3944 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock79 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+8
          CFI FunCall _RpGetAgcStartVth _RpRegBlockRead
          CFI FunCall _RpGetAntennaDiversityStartVth _RpRegBlockRead
        CODE
?Subroutine2:
        CALL      F:_RpRegBlockRead  ;; 3 cycles
          CFI EndBlock cfiBlock79
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 3 cycles
        REQUIRE ??Subroutine13_0
        ; // Fall through to label ??Subroutine13_0

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock80 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+8
        CODE
??Subroutine13_0:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock80
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
// 3945 
// 3946 /******************************************************************************
// 3947 Function Name:       RpSetCcaEdBandwidth
// 3948 Parameters:          none
// 3949 Return value:        none
// 3950 Description:         CCA or ED Bandwidth setting
// 3951 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock81 Using cfiCommon1
          CFI Function _RpSetCcaEdBandwidth
        CODE
// 3952 void RpSetCcaEdBandwidth( void )
// 3953 {
_RpSetCcaEdBandwidth:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 3954 	if ((RpCb.pib.phyCcaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K) && (RpCb.pib.phyEdBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_225K))	// default filter
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3448   ;; 2 cycles
        BNZ       ??RpLog_Event_380  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP0      ES:_RpRxBuf+3449   ;; 2 cycles
        BNZ       ??RpLog_Event_380  ;; 4 cycles
          CFI FunCall _RpRegCcaBandwidth225k
        ; ------------------------------------- Block: 6 cycles
// 3955 	{
// 3956 		RpRegCcaBandwidth225k();
        CALL      F:_RpRegCcaBandwidth225k  ;; 3 cycles
// 3957 		RpRegAdcVgaDefault();
          CFI FunCall _RpRegAdcVgaDefault
        CALL      F:_RpRegAdcVgaDefault  ;; 3 cycles
// 3958 		RpRegRxDataRateDefault();
          CFI FunCall _RpRegRxDataRateDefault
        CALL      F:_RpRegRxDataRateDefault  ;; 3 cycles
// 3959 		RpChangeDefaultFilter();
          CFI FunCall _RpChangeDefaultFilter
        CALL      F:_RpChangeDefaultFilter  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
// 3960 	}
// 3961 }
??RpLog_Event_380:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock81
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 31 cycles
// 3962 
// 3963 /******************************************************************************
// 3964 Function Name:       RpSetAntennaDiversityStartVth
// 3965 Parameters:          reset
// 3966 Return value:        none
// 3967 Description:         Antenna Diversity Start Threshold Setting
// 3968 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock82 Using cfiCommon1
          CFI Function _RpSetAntennaDiversityStartVth
          CFI NoCalls
        CODE
// 3969 void RpSetAntennaDiversityStartVth( uint8_t reset )
// 3970 {
_RpSetAntennaDiversityStartVth:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 3971 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
// 3972 	uint16_t reg;
// 3973 
// 3974 	if (reset || (RpCb.pib.phyAntennaDiversityStartVth == RP_RF_PIB_DFLT_ANTENNA_DIVERSITY_START_VTH))
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_381  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOVW      HL, ES:_RpRxBuf+3450  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        BNZ       ??RpLog_Event_382  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 3975 	{
// 3976 		RpCb.pib.phyAntennaDiversityStartVth = RP_RF_PIB_DFLT_ANTENNA_DIVERSITY_START_VTH;
??RpLog_Event_381:
        MOVW      HL, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      ES:_RpRxBuf+3450, AX  ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
// 3977 		reg = (uint16_t)RpFreqBandTbl[index][RP_ANTDIVVTH_OFFSET] | 0x0100;
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl+16)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        BR        S:??RpLog_Event_383  ;; 3 cycles
        ; ------------------------------------- Block: 19 cycles
// 3978 	}
// 3979 	else
// 3980 	{
// 3981 		reg = RpCb.pib.phyAntennaDiversityStartVth;
??RpLog_Event_382:
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_383:
        MOVW      [SP], AX           ;; 1 cycle
// 3982 	}
// 3983 	RpRegBlockWrite(BBANTDIVVTH, (uint8_t RP_FAR *)&reg, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x880         ;; 1 cycle
        BR        F:?Subroutine6     ;; 3 cycles
          CFI EndBlock cfiBlock82
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 49 cycles
// 3984 }
// 3985 
// 3986 /******************************************************************************
// 3987 Function Name:       RpGetAntennaDiversityStartVth
// 3988 Parameters:          none
// 3989 Return value:        none
// 3990 Description:         Antenna Diversity Start Threshold Setting
// 3991 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock83 Using cfiCommon1
          CFI Function _RpGetAntennaDiversityStartVth
          CFI NoCalls
        CODE
// 3992 uint16_t RpGetAntennaDiversityStartVth( void )
// 3993 {
_RpGetAntennaDiversityStartVth:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 3994 	uint16_t reg;
// 3995 
// 3996 	RpRegBlockRead(BBANTDIVVTH, (uint8_t *)&reg, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x880         ;; 1 cycle
        BR        F:?Subroutine2     ;; 3 cycles
          CFI EndBlock cfiBlock83
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 11 cycles
// 3997 	return reg;
// 3998 }
// 3999 
// 4000 /******************************************************************************
// 4001 Function Name:       RpGetAntennaDiversityStartVth
// 4002 Parameters:          none
// 4003 Return value:        none
// 4004 Description:         Antenna Diversity Start Threshold Setting
// 4005 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock84 Using cfiCommon1
          CFI Function _RpGetAntennaSwitchingTime
          CFI NoCalls
        CODE
// 4006 uint16_t RpGetAntennaSwitchingTime( void )
// 4007 {
_RpGetAntennaSwitchingTime:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 4008 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
// 4009 	uint16_t reg;
// 4010 
// 4011 	if (RpCb.pib.phyAntennaSwitchingTime == RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME)
        MOVW      HL, ES:_RpRxBuf+3452  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        BNZ       ??RpLog_Event_384  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 4012 	{
// 4013 		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_385  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_385  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 4014 		{
// 4015 			reg = (uint16_t)(RpFreqBandTbl[index][RP_CCA_ANTDVT_ON_ARIB]);
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl+7)  ;; 1 cycle
        BR        S:??RpLog_Event_386  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 4016 		}
// 4017 		else
// 4018 		{
// 4019 			reg = (uint16_t)(RpFreqBandTbl[index][RP_CCA_ANTDVT_ON]);
??RpLog_Event_385:
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl+6)  ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_386:
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 12 cycles
// 4020 		}
// 4021 	}
// 4022 	else
// 4023 	{
// 4024 		reg = RpCb.pib.phyAntennaSwitchingTime;
??RpLog_Event_384:
        MOVW      AX, HL             ;; 1 cycle
// 4025 	}
// 4026 	return reg;
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock84
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 56 cycles
// 4027 }
// 4028 
// 4029 /******************************************************************************
// 4030 Function Name:       RpSetSfdDetectionExtend
// 4031 Parameters:          none
// 4032 Return value:        none
// 4033 Description:         SFD Detection is Extended
// 4034 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock85 Using cfiCommon1
          CFI Function _RpSetSfdDetectionExtend
        CODE
// 4035 void RpSetSfdDetectionExtend( void )
// 4036 {
_RpSetSfdDetectionExtend:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
// 4037 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
// 4038 	uint8_t SfdDetExt = RpCb.pib.phySfdDetectionExtend;
        MOV       X, ES:_RpRxBuf+3454  ;; 2 cycles
// 4039 	uint8_t MrFskSfd = RpCb.pib.phyMrFskSfd;
        MOV       B, ES:_RpRxBuf+3418  ;; 2 cycles
        XCH       A, B               ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOV       A, B               ;; 1 cycle
// 4040 	uint8_t FecTxEna = RpCb.pib.phyFskFecTxEna;
        MOV       B, ES:_RpRxBuf+3365  ;; 2 cycles
        XCH       A, B               ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, B               ;; 1 cycle
// 4041 	uint8_t	FecRxEna = RpCb.pib.phyFskFecRxEna;
        MOV       B, ES:_RpRxBuf+3366  ;; 2 cycles
        XCH       A, B               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, B               ;; 1 cycle
// 4042 	uint16_t wk16;
// 4043 
// 4044 	RpCb.sfdExtend.writeIn = RP_FALSE;
        CLRB      ES:_RpRxBuf+3572   ;; 2 cycles
// 4045 	RpCb.sfdExtend.enableFec = RP_FALSE;
        CLRB      ES:_RpRxBuf+3587   ;; 2 cycles
// 4046 	RpCb.sfdExtend.rxFecAuto = RP_FALSE;
        CLRB      ES:_RpRxBuf+3586   ;; 2 cycles
// 4047 
// 4048 	if ((index != RP_PHY_200K_M03) && (index != RP_PHY_400K_M03))	// except for 4FSK
        CMP       A, #0x12           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_387  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
        CMP       A, #0x13           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_387  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 4049 	{
// 4050 		if (SfdDetExt == RP_TRUE)
        MOV       A, X               ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_388  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 4051 		{
// 4052 			RpCb.sfdExtend.writeIn = RP_TRUE;
        ONEB      ES:_RpRxBuf+3572   ;; 2 cycles
// 4053 
// 4054 			wk16 = (uint16_t)(RP_SFDVAL_MD0_2FSKNOFEC);
        MOVW      AX, #0x7209        ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 4055 			RpRegBlockWrite(BBSFD_2, (uint8_t RP_FAR *)&(wk16), sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x620         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4056 
// 4057 			if (MrFskSfd == RP_FALSE)
        MOV       A, [SP+0x04]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        CMP0      A                  ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_389  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
// 4058 			{					
// 4059 				if (FecTxEna == RP_FALSE)
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_390  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 4060 				{
// 4061 					RpCb.sfdExtend.txAddress0 = BBSFD;
        MOVW      AX, #0x610         ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3574, AX  ;; 2 cycles
// 4062 					RpCb.sfdExtend.txValue0 = (uint16_t)(RP_SFDVAL_MD0_2FSKNOFEC);
        MOVW      AX, #0x7209        ;; 1 cycle
        MOVW      ES:_RpRxBuf+3578, AX  ;; 2 cycles
// 4063 					RpCb.sfdExtend.txAddress1 = BBSFD2;
        MOVW      AX, #0x800         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3576, AX  ;; 2 cycles
// 4064 					RpCb.sfdExtend.txValue1 = (uint16_t)(RP_SFDVAL_MD0_2FSKFEC);
        MOVW      AX, #0x72F6        ;; 1 cycle
        BR        S:??RpLog_Event_391  ;; 3 cycles
        ; ------------------------------------- Block: 14 cycles
// 4065 				}
// 4066 				else
// 4067 				{
// 4068 					RpCb.sfdExtend.txAddress0 = BBSFD2;
??RpLog_Event_390:
        MOVW      AX, #0x800         ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3574, AX  ;; 2 cycles
// 4069 					RpCb.sfdExtend.txValue0 = (uint16_t)(RP_SFDVAL_MD0_2FSKFEC);
        MOVW      AX, #0x72F6        ;; 1 cycle
        MOVW      ES:_RpRxBuf+3578, AX  ;; 2 cycles
// 4070 					RpCb.sfdExtend.txAddress1 = BBSFD;
        MOVW      AX, #0x610         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3576, AX  ;; 2 cycles
// 4071 					RpCb.sfdExtend.txValue1 = (uint16_t)(RP_SFDVAL_MD0_2FSKNOFEC);
        MOVW      AX, #0x7209        ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
??RpLog_Event_391:
        MOVW      ES:_RpRxBuf+3580, AX  ;; 2 cycles
// 4072 				}
// 4073 
// 4074 				if (FecRxEna == RP_FEC_RX_MODE_DISABLE)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_392  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 4075 				{
// 4076 					RpCb.sfdExtend.rxAddress0 = BBSFD;
        MOVW      AX, #0x610         ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetSfdDetectionExtend_0:
        MOVW      ES:_RpRxBuf+3582, AX  ;; 2 cycles
// 4077 				}
// 4078 				else if (FecRxEna == RP_FEC_RX_MODE_ENABLE)
// 4079 				{
// 4080 					RpCb.sfdExtend.rxAddress0 = BBSFD2;
// 4081 				}
// 4082 				else // if (FecRxEna == RP_FEC_RX_MODE_AUTO_DISTINCT)
// 4083 				{
// 4084 					RpCb.sfdExtend.rxAddress0 = BBSFD;
// 4085 					RpCb.sfdExtend.rxAddress1 = BBSFD2;
// 4086 				}
// 4087 			}
// 4088 			else // if (MrFskSfd == RP_TRUE)
// 4089 			{
// 4090 				if (FecTxEna == RP_FALSE)
// 4091 				{
// 4092 					RpCb.sfdExtend.txAddress0 = BBSFD3;
// 4093 					RpCb.sfdExtend.txValue0 = (uint16_t)(RP_SFDVAL_MD1_2FSKNOFEC);
// 4094 					RpCb.sfdExtend.txAddress1 = BBSFD4;
// 4095 					RpCb.sfdExtend.txValue1 = (uint16_t)(RP_SFDVAL_MD1_2FSKFEC);
// 4096 				}
// 4097 				else
// 4098 				{
// 4099 					RpCb.sfdExtend.txAddress0 = BBSFD4;
// 4100 					RpCb.sfdExtend.txValue0 = (uint16_t)(RP_SFDVAL_MD1_2FSKFEC);
// 4101 					RpCb.sfdExtend.txAddress1 = BBSFD3;
// 4102 					RpCb.sfdExtend.txValue1 = (uint16_t)(RP_SFDVAL_MD1_2FSKNOFEC);
// 4103 				}
// 4104 
// 4105 				if (FecRxEna == RP_FEC_RX_MODE_DISABLE)
// 4106 				{
// 4107 					RpCb.sfdExtend.rxAddress0 = BBSFD3;
// 4108 				}
// 4109 				else if (FecRxEna == RP_FEC_RX_MODE_ENABLE)
// 4110 				{
// 4111 					RpCb.sfdExtend.rxAddress0 = BBSFD4;
// 4112 				}
// 4113 				else // if (FecRxEna == RP_FEC_RX_MODE_AUTO_DISTINCT)
// 4114 				{
// 4115 					RpCb.sfdExtend.rxAddress0 = BBSFD3;
// 4116 					RpCb.sfdExtend.rxAddress1 = BBSFD4;
// 4117 				}
// 4118 			}
// 4119 
// 4120 			if ((FecTxEna != RP_FALSE) || (FecRxEna != RP_FALSE))
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_393  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_394  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 4121 			{
// 4122 				RpCb.sfdExtend.enableFec = RP_TRUE;
??RpLog_Event_393:
        ONEB      ES:_RpRxBuf+3587   ;; 2 cycles
// 4123 
// 4124 				if (FecRxEna == RP_FEC_RX_MODE_AUTO_DISTINCT)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_394  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 4125 				{
// 4126 					RpCb.sfdExtend.rxFecAuto = RP_TRUE;
        ONEB      ES:_RpRxBuf+3586   ;; 2 cycles
        BR        R:??RpLog_Event_394  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 4127 				}
// 4128 			}
??RpLog_Event_392:
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_395  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, #0x800         ;; 1 cycle
        BR        S:??RpLog_Event_396  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_395:
        MOVW      AX, #0x610         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3582, AX  ;; 2 cycles
        MOVW      AX, #0x800         ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??RpSetSfdDetectionExtend_1:
        MOVW      ES:_RpRxBuf+3584, AX  ;; 2 cycles
        BR        S:??RpLog_Event_393  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_389:
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_397  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, #0x820         ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3574, AX  ;; 2 cycles
        MOVW      AX, #0x705E        ;; 1 cycle
        MOVW      ES:_RpRxBuf+3578, AX  ;; 2 cycles
        MOVW      AX, #0x840         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3576, AX  ;; 2 cycles
        MOVW      AX, #0xB4C6        ;; 1 cycle
        BR        S:??RpLog_Event_398  ;; 3 cycles
        ; ------------------------------------- Block: 14 cycles
??RpLog_Event_397:
        MOVW      AX, #0x840         ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3574, AX  ;; 2 cycles
        MOVW      AX, #0xB4C6        ;; 1 cycle
        MOVW      ES:_RpRxBuf+3578, AX  ;; 2 cycles
        MOVW      AX, #0x820         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3576, AX  ;; 2 cycles
        MOVW      AX, #0x705E        ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
??RpLog_Event_398:
        MOVW      ES:_RpRxBuf+3580, AX  ;; 2 cycles
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_399  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, #0x820         ;; 1 cycle
        BR        R:??RpSetSfdDetectionExtend_0  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_399:
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_400  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, #0x840         ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_396:
        MOVW      ES:_RpRxBuf+3582, AX  ;; 2 cycles
        ONEB      ES:_RpRxBuf+3587   ;; 2 cycles
        BR        S:??RpLog_Event_394  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_400:
        MOVW      AX, #0x820         ;; 1 cycle
        MOVW      ES:_RpRxBuf+3582, AX  ;; 2 cycles
        MOV       X, #0x40           ;; 1 cycle
        BR        S:??RpSetSfdDetectionExtend_1  ;; 3 cycles
          CFI FunCall _RpSetMrFskSfdVal
        ; ------------------------------------- Block: 7 cycles
// 4129 		}
// 4130 		else
// 4131 		{
// 4132 			RpSetMrFskSfdVal();
??RpLog_Event_388:
        CALL      F:_RpSetMrFskSfdVal  ;; 3 cycles
// 4133 			RpRegWrite(BBSHRCON, 0x02);
        MOV       C, #0x2            ;; 1 cycle
        BR        S:??RpLog_Event_401  ;; 3 cycles
          CFI FunCall _RpSetMrFskSfdVal
        ; ------------------------------------- Block: 7 cycles
// 4134 		}
// 4135 	}
// 4136 	else
// 4137 	{
// 4138 		RpSetMrFskSfdVal();
??RpLog_Event_387:
        CALL      F:_RpSetMrFskSfdVal  ;; 3 cycles
// 4139 		RpRegWrite(BBSHRCON, 0x06);
        MOV       C, #0x6            ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_401:
        MOVW      AX, #0x630         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 4140 	}
// 4141 }
??RpLog_Event_394:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock85
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 233 cycles
// 4142 
// 4143 /******************************************************************************
// 4144 Function Name:       RpSetSfdDetectionExtendWrite
// 4145 Parameters:          status
// 4146 Return value:        none
// 4147 Description:         The writing in to expand SFD detection
// 4148 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock86 Using cfiCommon1
          CFI Function _RpSetSfdDetectionExtendWrite
        CODE
// 4149 void RpSetSfdDetectionExtendWrite( uint16_t status )
// 4150 {
_RpSetSfdDetectionExtendWrite:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 16
        SUBW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+20
// 4151 	uint16_t txAddress0 = RpCb.sfdExtend.txAddress0;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      HL, ES:_RpRxBuf+3574  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
// 4152 	uint16_t txAddress1 = RpCb.sfdExtend.txAddress1;
        MOVW      HL, ES:_RpRxBuf+3576  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
// 4153 	uint16_t txValue0 = RpCb.sfdExtend.txValue0;
        MOVW      HL, ES:_RpRxBuf+3578  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
// 4154 	uint16_t txValue1 = RpCb.sfdExtend.txValue1;
        MOVW      HL, ES:_RpRxBuf+3580  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
// 4155 	uint16_t rxAddress0 = RpCb.sfdExtend.rxAddress0;
        MOVW      HL, ES:_RpRxBuf+3582  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
// 4156 	uint16_t rxAddress1 = RpCb.sfdExtend.rxAddress1;
        MOVW      HL, ES:_RpRxBuf+3584  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
// 4157 	uint8_t rxFecAuto = RpCb.sfdExtend.rxFecAuto;
        MOV       B, ES:_RpRxBuf+3586  ;; 2 cycles
        MOV       A, B               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, H               ;; 1 cycle
// 4158 	uint16_t preamblePattern = RP_SFDVAL_EXT_PREAMBLE;
        MOVW      HL, #0xAAAA        ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 4159 
// 4160 	if (RpCb.sfdExtend.writeIn == RP_TRUE)
        CMP       ES:_RpRxBuf+3572, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_402  ;; 4 cycles
        ; ------------------------------------- Block: 46 cycles
// 4161 	{
// 4162 		if (((status & RP_PHY_STAT_TX) == RP_FALSE) && (status & RP_PHY_STAT_RX) && ((RpCb.reg.bbTxRxMode0 & AUTOACKEN) == RP_FALSE))
        MOV       A, L               ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_403  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3555)  ;; 1 cycle
        BT        ES:[HL].2, ??RpLog_Event_403  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 4163 		{
// 4164 			RpRegBlockWrite(rxAddress0, (uint8_t RP_FAR *)&preamblePattern, sizeof(uint16_t));
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4165 			
// 4166 			if (rxFecAuto == RP_TRUE)
        MOV       A, [SP+0x02]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_404  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
// 4167 			{
// 4168 				RpRegBlockWrite(rxAddress1, (uint8_t RP_FAR *)&preamblePattern, sizeof(uint16_t));
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+20
        ; ------------------------------------- Block: 9 cycles
// 4169 			}
// 4170 			RpRegWrite(BBSHRCON, 0x06);
??RpLog_Event_404:
        MOV       C, #0x6            ;; 1 cycle
        BR        S:??RpLog_Event_405  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 4171 		}
// 4172 		else
// 4173 		{
// 4174 			RpRegBlockWrite(txAddress0, (uint8_t RP_FAR *)&txValue0, sizeof(uint16_t));
??RpLog_Event_403:
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4175 
// 4176 			if (RpCb.sfdExtend.enableFec == RP_TRUE)
        POP       AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3587, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_406  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
// 4177 			{
// 4178 				RpRegBlockWrite(txAddress1, (uint8_t RP_FAR *)&txValue1, sizeof(uint16_t)); 
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+20
        ; ------------------------------------- Block: 10 cycles
// 4179 			}
// 4180 			RpRegWrite(BBSHRCON, 0x02);
??RpLog_Event_406:
        MOV       C, #0x2            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_405:
        MOVW      AX, #0x630         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 4181 		}
// 4182 	}
// 4183 }
??RpLog_Event_402:
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock86
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 135 cycles
// 4184 
// 4185 /******************************************************************************
// 4186 Function Name:       RpSetAgcWaitGain
// 4187 Parameters:          none
// 4188 Return value:        none
// 4189 Description:         AGC Wait Gain Setting
// 4190 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock87 Using cfiCommon1
          CFI Function _RpSetAgcWaitGain
        CODE
// 4191 void RpSetAgcWaitGain( void )
// 4192 {
_RpSetAgcWaitGain:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 4193 	uint8_t	val0x0440;
// 4194 
// 4195 	val0x0440 = RP_DEFAULT_AGC_WAIT_GAIN - RpCb.pib.phyAgcWaitGainOffset;
// 4196 	RpRegWrite((0x0440 << 3), val0x0440);
        MOV       A, #0x44           ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        SUB       A, ES:_RpRxBuf+3455  ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2200        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4197 
// 4198 	RpSetLvlVthVal();
          CFI FunCall _RpSetLvlVthVal
        CALL      F:_RpSetLvlVthVal  ;; 3 cycles
// 4199 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock87
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 18 cycles
// 4200 
// 4201 /******************************************************************************
// 4202 Function Name:       RpSetAntennaSwitchEnaTiming
// 4203 Parameters:          none
// 4204 Return value:        none
// 4205 Description:         set antenna switch timing.
// 4206 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock88 Using cfiCommon1
          CFI Function _RpSetAntennaSwitchEnaTiming
          CFI NoCalls
        CODE
// 4207 void RpSetAntennaSwitchEnaTiming( void )
// 4208 {
_RpSetAntennaSwitchEnaTiming:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 4209 	// AntennaSwitchEnaTiming
// 4210 	RpCb.pib.phyAntennaSwitchEnaTiming &= 0x03FF;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3458  ;; 2 cycles
        AND       A, #0x3            ;; 1 cycle
        MOVW      ES:_RpRxBuf+3458, AX  ;; 2 cycles
// 4211 	RpRegBlockWrite(BBANTSWTIMG,  (uint8_t RP_FAR *)&RpCb.pib.phyAntennaSwitchEnaTiming, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3458)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x3D0         ;; 1 cycle
        BR        F:?Subroutine8     ;; 3 cycles
          CFI EndBlock cfiBlock88
        ; ------------------------------------- Block: 14 cycles
        ; ------------------------------------- Total: 14 cycles
// 4212 }
// 4213 
// 4214 /******************************************************************************
// 4215  * function Name : RpSetGpio0Setting
// 4216  * parameters    : none
// 4217  * return value  : none
// 4218  * description   : Set GPIO0
// 4219  *****************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock89 Using cfiCommon1
          CFI Function _RpSetGpio0Setting
        CODE
// 4220 void RpSetGpio0Setting( void )
// 4221 {
_RpSetGpio0Setting:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 4222 	RpCb.reg.bbGpioData &= ~GPIO0DATA;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:_RpRxBuf+3564.0  ;; 3 cycles
// 4223 	RpCb.reg.bbGpioData |= RpCb.pib.phyGpio0Setting;
// 4224 	RpCb.reg.bbGpioData &= GPIOXDATA;
        MOV       A, ES:_RpRxBuf+3564  ;; 2 cycles
        OR        A, ES:_RpRxBuf+3460  ;; 2 cycles
        AND       A, #0x1F           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3564, A  ;; 2 cycles
// 4225 	RpRegWrite(BBGPIODATA, RpCb.reg.bbGpioData);
        MOVW      AX, #0x418         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4226 
// 4227 	RpCb.reg.bbGpioDir |= GPIO0DIR;
// 4228 	RpCb.reg.bbGpioDir &= GPIOXDIR;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3563  ;; 2 cycles
        AND       A, #0x1F           ;; 1 cycle
        OR        A, #0x1            ;; 1 cycle
          CFI EndBlock cfiBlock89
        ; ------------------------------------- Block: 21 cycles
        ; ------------------------------------- Total: 21 cycles
        REQUIRE ?Subroutine10
        ; // Fall through to label ?Subroutine10
// 4229 	RpRegWrite(BBGPIODIR, RpCb.reg.bbGpioDir);
// 4230 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock90 Using cfiCommon1
          CFI NoFunction
        CODE
?Subroutine10:
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3563, A  ;; 2 cycles
        MOVW      AX, #0x410         ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock90
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 7 cycles
// 4231 
// 4232 /******************************************************************************
// 4233  * function Name : RpSetGpio3Setting
// 4234  * parameters    : none
// 4235  * return value  : none
// 4236  * description   : Set GPIO3
// 4237  *****************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock91 Using cfiCommon1
          CFI Function _RpSetGpio3Setting
        CODE
// 4238 void RpSetGpio3Setting( void )
// 4239 {
_RpSetGpio3Setting:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 4240 	RpCb.reg.bbGpioData &= ~GPIO3DATA;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLR1      ES:_RpRxBuf+3564.3  ;; 3 cycles
// 4241 	RpCb.reg.bbGpioData |= (RpCb.pib.phyGpio3Setting << 3);
// 4242 	RpCb.reg.bbGpioData &= GPIOXDATA;
        MOV       A, ES:_RpRxBuf+3461  ;; 2 cycles
        SHL       A, 0x3             ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3564  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        AND       A, #0x1F           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3564, A  ;; 2 cycles
// 4243 	RpRegWrite(BBGPIODATA, RpCb.reg.bbGpioData);
        MOVW      AX, #0x418         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4244 
// 4245 	RpCb.reg.bbGpioDir |= GPIO3DIR;
// 4246 	RpCb.reg.bbGpioDir &= GPIOXDIR;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3563  ;; 2 cycles
        AND       A, #0x1F           ;; 1 cycle
        OR        A, #0x8            ;; 1 cycle
        BR        F:?Subroutine10    ;; 3 cycles
          CFI EndBlock cfiBlock91
        ; ------------------------------------- Block: 26 cycles
        ; ------------------------------------- Total: 26 cycles
// 4247 	RpRegWrite(BBGPIODIR, RpCb.reg.bbGpioDir);
// 4248 }
// 4249 
// 4250 /******************************************************************************
// 4251 Function Name:       RpPrevSentTimeReSetting
// 4252 Parameters:          none
// 4253 Return value:        none
// 4254 Description:         Set previous timer re-setting.
// 4255 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock92 Using cfiCommon1
          CFI Function _RpPrevSentTimeReSetting
        CODE
// 4256 void RpPrevSentTimeReSetting( void )
// 4257 {
_RpPrevSentTimeReSetting:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 8
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
// 4258 	uint32_t realTime, nowTime, difTime, difTime2;
// 4259 
// 4260 	realTime = RpCb.tx.sentTime;
        MOVW      HL, #LWRD(_RpRxBuf+3284)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 4261 	if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_407  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
// 4262 	{
// 4263 		difTime = (uint32_t)(RpCb.pib.phyRmodeToffMin) * (uint32_t)(RpCb.pib.phyDataRate);
        MOVW      BC, ES:_RpRxBuf+3434  ;; 2 cycles
        MOVW      AX, ES:_RpRxBuf+3464  ;; 2 cycles
        MULHU                        ;; 2 cycles
        BR        S:??RpLog_Event_408  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 4264 	}
// 4265 	else
// 4266 	{
// 4267 		difTime = RpCalcTxInterval(RpCb.tx.sentLen, RP_FALSE);
??RpLog_Event_407:
        CLRB      C                  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3282  ;; 2 cycles
          CFI FunCall _RpCalcTxInterval
        CALL      F:_RpCalcTxInterval  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpLog_Event_408:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
// 4268 	}
// 4269 	realTime += difTime;
// 4270 	realTime &= RP_TIME_MASK;
// 4271 	nowTime = RpGetTime();
// 4272 	difTime2 = (uint32_t)((realTime - nowTime/* tx time considered interval time */) & RP_TIME_MASK);
          CFI FunCall _RpGetTime
        CALL      F:_RpGetTime       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      DE, AX             ;; 1 cycle
// 4273 	if ((difTime2 <= RP_TIME_LIMIT) && (difTime2 >= RP_RETX_TIMER_WASTE_TIME))
        SUBW      AX, #0x2           ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x7FFF        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 48 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpPrevSentTimeReSetting_0:
        BNC       ??RpLog_Event_409  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 4274 	{
// 4275 		RpCb.tx.sentTime = ((0xffffffff - (difTime - difTime2)) & RP_TIME_MASK);
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3284)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        BR        S:??RpLog_Event_410  ;; 3 cycles
        ; ------------------------------------- Block: 36 cycles
// 4276 	}
// 4277 	else
// 4278 	{
// 4279 		RpCb.tx.sentTime = 0;
??RpLog_Event_409:
        MOVW      HL, #LWRD(_RpRxBuf+3284)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_410:
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 4280 	}
// 4281 }
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock92
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 133 cycles
// 4282 
// 4283 /******************************************************************************
// 4284 Function Name:       RpInitVar
// 4285 Parameters:          refreshFlg
// 4286 Return value:        none
// 4287 Description:         set  default variables.
// 4288 ******************************************************************************/
// 4289 static void RpInitVar( uint8_t refreshFlg )
// 4290 {
// 4291 	uint32_t eddr1[2], eddr2[2];
// 4292 	uint8_t regulatoryMode;
// 4293 	uint16_t rmodeTonMax;
// 4294 	uint16_t rmodeToffMin;
// 4295 	uint16_t rmodeTcumSmpPeriod;
// 4296 	uint32_t rmodeTcumLimit;
// 4297 	uint32_t rmodeTcum;
// 4298 	uint16_t senLen;
// 4299 	uint32_t sentTime;
// 4300 	uint32_t sentElapsedTimeS;
// 4301 	uint8_t needTxWaitRegulatoryMode;
// 4302 	uint32_t regulatoryModeElapsedTimeS;
// 4303 	uint16_t ackLenForRegulatoryMode;
// 4304 	uint8_t  applyAckTotalTxTime;
// 4305 	uint8_t  applyAckToffMin;
// 4306 #ifdef R_FSB_FAN_ENABLED
// 4307 	uint8_t  isFsbDownlink;
// 4308 #endif // #ifdef R_FSB_FAN_ENABLED
// 4309 
// 4310 	if (refreshFlg == RP_FALSE)
// 4311 	{
// 4312 		RpMemset(&RpCb, 0, sizeof(RpCb));
// 4313 		RpMemset(&RpTxTime, 0, sizeof(RpTxTime));
// 4314 		RpMemset((uint8_t *)RpCb.pib.macExtendedAddress1, 0x00, sizeof(uint32_t[2]));
// 4315 		RpMemset((uint8_t *)RpCb.pib.macExtendedAddress2, 0x00, sizeof(uint32_t[2]));
// 4316 		RpCb.pib.phyRegulatoryMode = RP_RF_PIB_DFLT_REGULATORY_MODE;
// 4317 		RpCb.pib.phyRmodeTonMax = RP_RF_PIB_DFLT_RMODE_TON_MAX;
// 4318 		RpCb.pib.phyRmodeToffMin = RP_RF_PIB_DFLT_RMODE_TOFF_MIN;
// 4319 		RpCb.pib.phyRmodeTcumSmpPeriod = RP_RF_PIB_DFLT_RMODE_TCUM_SMP_PERIOD;
// 4320 		RpCb.pib.phyRmodeTcumLimit = RP_RF_PIB_DFLT_RMODE_TCUM_LIMIT;
// 4321 		RpCb.pib.phyRmodeTcum = RP_RF_PIB_DFLT_RMODE_TCUM;
// 4322 	}
// 4323 	else // if (refreshFlg == RP_TRUE)
// 4324 	{
// 4325 		RpMemcpy(eddr1, RpCb.pib.macExtendedAddress1, sizeof(uint32_t[2]));
// 4326 		RpMemcpy(eddr2, RpCb.pib.macExtendedAddress2, sizeof(uint32_t[2]));
// 4327 		regulatoryMode = RpCb.pib.phyRegulatoryMode;
// 4328 		rmodeTonMax = RpCb.pib.phyRmodeTonMax;
// 4329 		rmodeToffMin = RpCb.pib.phyRmodeToffMin;
// 4330 		rmodeTcumSmpPeriod = RpCb.pib.phyRmodeTcumSmpPeriod;
// 4331 		rmodeTcumLimit = RpCb.pib.phyRmodeTcumLimit;
// 4332 		rmodeTcum = RpCb.pib.phyRmodeTcum;
// 4333 		senLen = RpCb.tx.sentLen;
// 4334 		sentTime = RpCb.tx.sentTime;
// 4335 		sentElapsedTimeS = RpCb.tx.sentElapsedTimeS;
// 4336 		needTxWaitRegulatoryMode = RpCb.tx.needTxWaitRegulatoryMode;
// 4337 		regulatoryModeElapsedTimeS = RpCb.tx.regulatoryModeElapsedTimeS;
// 4338 		ackLenForRegulatoryMode = RpCb.tx.ackLenForRegulatoryMode;
// 4339 		applyAckTotalTxTime = RpCb.tx.applyAckTotalTxTime;
// 4340 		applyAckToffMin = RpCb.tx.applyAckToffMin;
// 4341 #ifdef R_FSB_FAN_ENABLED
// 4342 		isFsbDownlink = RpCb.tx.isFsbDownlink;
// 4343 #endif // #ifdef R_FSB_FAN_ENABLED
// 4344 		RpMemset(&RpCb.tx, 0x00, sizeof(RpCb.tx));
// 4345 		RpMemset(&RpCb.rx, 0x00, sizeof(RpCb.rx));
// 4346 		RpMemset(&RpCb.pib, 0x00, sizeof(RpCb.pib));
// 4347 		RpCb.freqIdIndex = 0x00;
// 4348 		RpCb.prohibitLowPower = RP_FALSE;
// 4349 		RpMemset(&RpCb.reg, 0x00, sizeof(RpCb.reg));
// 4350 		RpMemcpy(RpCb.pib.macExtendedAddress1, eddr1, sizeof(uint32_t[2]));
// 4351 		RpMemcpy(RpCb.pib.macExtendedAddress2, eddr2, sizeof(uint32_t[2]));
// 4352 		RpCb.pib.phyRegulatoryMode = regulatoryMode;
// 4353 		RpCb.pib.phyRmodeTonMax= rmodeTonMax;
// 4354 		RpCb.pib.phyRmodeToffMin= rmodeToffMin;
// 4355 		RpCb.pib.phyRmodeTcumSmpPeriod= rmodeTcumSmpPeriod;
// 4356 		RpCb.pib.phyRmodeTcumLimit= rmodeTcumLimit;
// 4357 		RpCb.pib.phyRmodeTcum= rmodeTcum;
// 4358 		RpCb.tx.sentLen = senLen;
// 4359 		RpCb.tx.sentTime = sentTime;
// 4360 		RpCb.tx.sentElapsedTimeS = sentElapsedTimeS;
// 4361 		RpCb.tx.needTxWaitRegulatoryMode = needTxWaitRegulatoryMode;
// 4362 		RpCb.tx.regulatoryModeElapsedTimeS = regulatoryModeElapsedTimeS;
// 4363 		RpCb.tx.ackLenForRegulatoryMode = ackLenForRegulatoryMode;
// 4364 		RpCb.tx.applyAckTotalTxTime = applyAckTotalTxTime;
// 4365 		RpCb.tx.applyAckToffMin = applyAckToffMin;
// 4366 #ifdef R_FSB_FAN_ENABLED
// 4367 		RpCb.tx.isFsbDownlink = isFsbDownlink;
// 4368 #endif // #ifdef R_FSB_FAN_ENABLED
// 4369 	}
// 4370 	RpCb.pib.phyCurrentChannel   		= RP_RF_PIB_DFLT_CURRENT_CHANNEL;
// 4371 	RpCb.pib.phyChannelsSupportedPage 	= RP_RF_PIB_DFLT_CHANNEL_SUPPORTED_INDEX;
// 4372 	RpCb.pib.phyChannelsSupported[0]	= RP_RF_PIB_DFLT_SUPPORT_CH_L;
// 4373 	RpCb.pib.phyChannelsSupported[1]	= RP_RF_PIB_DFLT_SUPPORT_CH_H;
// 4374 	RpCb.pib.phyTransmitPower	  		= RpConfig.txPowerDefault;
// 4375 	RpCb.pib.macAddressFilter1Ena		= RP_RF_PIB_DFLT_ADDRESS_FILTER1_ENA;
// 4376 	RpCb.pib.macPanId1 					= RP_RF_PIB_DFLT_PANID1;
// 4377 	RpCb.pib.macShortAddr1 				= RP_RF_PIB_DFLT_SHORTAD1;
// 4378 	RpCb.pib.macPanCoord1 				= RP_RF_PIB_DFLT_PANCOORD1;
// 4379 	RpCb.pib.macPendBit1 				= RP_RF_PIB_DFLT_PENDBIT1;
// 4380 	RpCb.pib.macAddressFilter2Ena		= RP_RF_PIB_DFLT_ADDRESS_FILTER2_ENA;
// 4381 	RpCb.pib.macPanId2 					= RP_RF_PIB_DFLT_PANID2;
// 4382 	RpCb.pib.macShortAddr2 				= RP_RF_PIB_DFLT_SHORTAD2;
// 4383 	RpCb.pib.macPanCoord2 				= RP_RF_PIB_DFLT_PANCOORD2;
// 4384 	RpCb.pib.macPendBit2				= RP_RF_PIB_DFLT_PENDBIT2;
// 4385 	RpCb.pib.macMaxCsmaBackOff 			= RP_RF_PIB_DFLT_MAXCSMABO;
// 4386 	RpCb.pib.macMinBe 					= RP_RF_PIB_DFLT_MINBE;
// 4387 	RpCb.pib.macMaxBe 					= RP_RF_PIB_DFLT_MAXBE;
// 4388 	RpCb.pib.macMaxFrameRetries 		= RP_RF_PIB_DFLT_MAX_FRAME_RETRIES;
// 4389 	RpCb.pib.phyCrcErrorUpMsg			= RP_RF_PIB_DFLT_CRCERROR_UPMSG;
// 4390 	RpCb.pib.phyBackOffSeed 			= RP_RF_PIB_DFLT_BACKOFF_SEED;
// 4391 	RpCb.pib.phyCcaVth					= RpConfig.ccaVthDefault & (~0xFE00);
// 4392 	RpCb.pib.phyCcaDuration				= RP_RF_PIB_DFLT_CCA_DURATION;
// 4393 	RpCb.pib.phyFskFecTxEna				= RP_RF_PIB_DFLT_FSK_FEC_TX_ENA;
// 4394 	RpCb.pib.phyFskFecRxEna				= RP_RF_PIB_DFLT_FSK_FEC_RX_ENA;
// 4395 	RpCb.pib.phyFskFecScheme			= RP_RF_PIB_DFLT_FSK_FEC_SCHEME;
// 4396 	RpCb.pib.phyFskPreambleLength		= RP_RF_PIB_DFLT_FSK_PREAMBLE_LENGTH;
// 4397 	RpCb.pib.phyMrFskSfd				= RP_RF_PIB_DFLT_MRFSK_SFD;
// 4398 	RpCb.pib.phyFskScramblePsdu			= RP_RF_PIB_DFLT_FSK_SCRAMBLE_PSDU;
// 4399 	RpCb.pib.phyFskOpeMode				= RP_RF_PIB_DFLT_FSK_OPE_MODE;
// 4400 	RpCb.pib.phyFcsLength				= RP_RF_PIB_DFLT_FCS_LENGTH;
// 4401 	RpCb.pib.phyProfileSpecificMode		= RpConfig.profileSpecificModeDefault;
// 4402 	RpCb.pib.phyAntennaSwitchEna		= RpConfig.antSwEnaDefault;
// 4403 	RpCb.pib.phyAntennaDiversityRxEna 	= RpConfig.antDvrEnaDefault;
// 4404 	if (RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE)
// 4405 	{
// 4406 		RpCb.pib.phyAntennaSelectTx		= RP_PHY_ANTENNA_0;
// 4407 	}
// 4408 	else
// 4409 	{
// 4410 		RpCb.pib.phyAntennaSelectTx		= RP_PHY_ANTENNA_UNUSE;
// 4411 	}
// 4412 	RpCb.pib.phyAntennaSelectAckTx		= RP_RF_PIB_DFLT_ANTENNA_SELECT_ACKTX;
// 4413 	RpCb.pib.phyAntennaSelectAckRx		= RP_RF_PIB_DFLT_ANTENNA_SELECT_ACKRX;
// 4414 	RpCb.pib.phyAckReplyTime			= RP_RF_PIB_DFLT_ACK_RESPONSE_TIME;
// 4415 	RpCb.pib.phyAckWaitDuration			= RP_RF_PIB_DFLT_ACK_WAIT_DURATION;
// 4416 	RpCb.pib.phyRxTimeoutMode			= RP_RF_PIB_DFLT_RXTIMEOUT_MODE;
// 4417 	RpCb.pib.phyFreqBandId				= RP_RF_PIB_DFLT_FREQ_BAND_ID;
// 4418 	RpCb.pib.phyLvlFltrVth				= RP_RF_PIB_DFLT_LVL_VTH;
// 4419 	RpCb.pib.refVthVal					= RP_RF_PIB_DFLT_LVL_VTH;
// 4420 	RpCb.pib.phyCsmaBackoffPeriod		= RP_RF_PIB_DFLT_BACKOFF_PERIOD;
// 4421 	RpCb.pib.phyPreamble4ByteRxMode		= RP_RF_PIB_DFLT_PREAMBLE_4BYTE_RX_MODE;
// 4422 	RpCb.pib.phyAgcStartVth				= RP_RF_PIB_DFLT_AGC_START_VTH;
// 4423 	RpCb.pib.phyCcaBandwidth			= RP_RF_PIB_DFLT_CCA_BANDWIDTH;
// 4424 	RpCb.pib.phyEdBandwidth				= RP_RF_PIB_DFLT_ED_BANDWIDTH;
// 4425 	RpCb.pib.phyAntennaDiversityStartVth	= RP_RF_PIB_DFLT_ANTENNA_DIVERSITY_START_VTH;
// 4426 	RpCb.pib.phyAntennaSwitchingTime	= RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME;
// 4427 	RpCb.pib.phySfdDetectionExtend		= RP_RF_PIB_DFLT_SFD_DETECTION_EXTEND;
// 4428 	RpCb.pib.phyAgcWaitGainOffset		= RP_RF_PIB_DFLT_AGC_WAIT_GAIN_OFFSET;
// 4429 	RpCb.pib.phyCcaVthOffset			= RP_RF_PIB_DFLT_CCA_VTH_OFFSET;
// 4430 	RpCb.pib.phyAntennaSwitchEnaTiming	= RP_RF_PIB_DFLT_ANTENNA_SWITCH_ENA_TIMING;
// 4431 	RpCb.pib.phyGpio0Setting			= RP_RF_PIB_DFLT_GPIO0_SETTING;
// 4432 	RpCb.pib.phyGpio3Setting			= RP_RF_PIB_DFLT_GPIO3_SETTING;
// 4433 	RpMemset(&RpRfStat, 0x00, sizeof(RP_PHY_ERROR));
// 4434 	RpCb.status 						= RP_PHY_STAT_TRX_OFF;
// 4435 	RpCb.statusRxTimeout 				= RP_STATUS_RX_TIMEOUT_INIT;
// 4436 	RpSetAttr_phyAckWithCca( RP_FALSE );
// 4437 	RpCb.pib.phyRssiOutputOffset		= RP_RF_PIB_DFLT_RSSI_OUTPUT_OFFSET;
// 4438 	RpCb.rx.antdvTimerValue				= RP_PHY_ANTDV_TIMERVALUE;
// 4439 #ifdef R_FREQUENCY_OFFSET_ENABLED
// 4440 	RpCb.pib.phyFrequency				= RP_RF_HW_DFLT_FREQUENCY;
// 4441 	RpSetAttr_phyFrequencyOffset( (int32_t)RP_RF_PIB_DFLT_FREQUENCY_OFFSET );
// 4442 #endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
// 4443 }
// 4444 
// 4445 /******************************************************************************
// 4446 Function Name:       RpInitRfOnly
// 4447 Parameters:          none
// 4448 Return value:        none
// 4449 Description:         set SFR for baseband.
// 4450 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock93 Using cfiCommon1
          CFI Function _RpInitRfOnly
        CODE
// 4451 static void RpInitRfOnly( void )
// 4452 {
_RpInitRfOnly:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 4453 	// set regval
// 4454 	RpCb.reg.bbTimeCon		= RP_RF_HW_DFLT_BBTIMECON;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf+3554   ;; 2 cycles
// 4455 	RpCb.reg.bbTxRxMode0	= RP_RF_HW_DFLT_BBTXRXMODE0;
        CLRB      ES:_RpRxBuf+3555   ;; 2 cycles
// 4456 	RpCb.reg.bbTxRxMode1	= RP_RF_HW_DFLT_BBTXRXMODE1;
        MOV       ES:_RpRxBuf+3556, #0xC0  ;; 2 cycles
// 4457 	RpCb.reg.bbTxRxMode2	= RP_RF_HW_DFLT_BBTXRXMODE2;
        MOV       ES:_RpRxBuf+3557, #0x30  ;; 2 cycles
// 4458 	RpCb.reg.bbTxRxMode3	= RP_RF_HW_DFLT_BBTXRXMODE3;
        CLRB      ES:_RpRxBuf+3558   ;; 2 cycles
// 4459 	RpCb.reg.bbTxRxMode4	= RP_RF_HW_DFLT_BBTXRXMODE4;
        ONEB      ES:_RpRxBuf+3559   ;; 2 cycles
// 4460 	RpCb.reg.bbCsmaCon0		= RP_RF_HW_DFLT_BBCSMACON0;
        CLRB      ES:_RpRxBuf+3560   ;; 2 cycles
// 4461 	RpCb.reg.bbCsmaCon1		= RP_RF_HW_DFLT_BBCSMACON1;
        MOV       ES:_RpRxBuf+3561, #0x20  ;; 2 cycles
// 4462 	RpCb.reg.bbCsmaCon2		= RP_RF_HW_DFLT_BBCSMACON2;
        MOV       ES:_RpRxBuf+3562, #0x15  ;; 2 cycles
// 4463 	RpCb.reg.bbGpioData		= RP_RF_HW_DFLT_BBGPIODATA;
        CLRB      ES:_RpRxBuf+3564   ;; 2 cycles
// 4464 	RpCb.reg.bbFecCon		= RP_RF_HW_DFLT_BBFECCON;
        CLRB      ES:_RpRxBuf+3565   ;; 2 cycles
// 4465 	RpCb.reg.bbSubCon		= RP_RF_HW_DFLT_BBSUBGCON;
        MOV       ES:_RpRxBuf+3566, #0xC  ;; 2 cycles
// 4466 	RpCb.reg.bbTxCon0		= RP_RF_HW_DFLT_BBTXCON0;
        MOV       ES:_RpRxBuf+3567, #0xF4  ;; 2 cycles
// 4467 	RpCb.reg.bb19Er2		= RP_RF_HW_DFLT_BB19ER2;
        MOV       ES:_RpRxBuf+3569, #0xC8  ;; 2 cycles
// 4468 	RpCb.reg.bb25Er2		= RP_RF_HW_DFLT_BB25ER2;
        MOV       ES:_RpRxBuf+3570, #0x1F  ;; 2 cycles
// 4469 	RpCb.reg.bb1AEr2		= RP_RF_HW_DFLT_BB1AER2;
        MOV       ES:_RpRxBuf+3571, #0xF0  ;; 2 cycles
// 4470 
// 4471 	// Set Regulatory Mode settings
// 4472 	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
// 4473 		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_411  ;; 4 cycles
        ; ------------------------------------- Block: 39 cycles
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_412  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        SKNZ                         ;; 1 cycle
          CFI FunCall _RpSetMacTxLimitMode
        ; ------------------------------------- Block: 3 cycles
// 4474 	{
// 4475 		RpSetMacTxLimitMode();
??RpLog_Event_411:
        CALL      F:_RpSetMacTxLimitMode  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4476 	}
// 4477 
// 4478 	// Set Misllanious registers initial settings
// 4479 	RpSetMiscellaneousIniVal();
??RpLog_Event_412:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3557, #0x8  ;; 2 cycles
        MOV       C, #0x8            ;; 1 cycle
        MOVW      AX, #0x48          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3558  ;; 2 cycles
        OR        A, #0x9C           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       ES:_RpRxBuf+3558, A  ;; 2 cycles
        MOVW      AX, #0x50          ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4480 
// 4481 	// Set Max csma backoff settings
// 4482 	if (RpCb.pib.macMaxCsmaBackOff != RP_RF_HW_DFLT_MAXCSMABO)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3410   ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetMaxCsmaBackoffVal
        ; ------------------------------------- Block: 23 cycles
// 4483 	{
// 4484 		RpSetMaxCsmaBackoffVal();
        CALL      F:_RpSetMaxCsmaBackoffVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4485 	}
// 4486 	// Specification Mode
// 4487 	RpSetSpecificModeVal(RP_TRUE);
??RpInitRfOnly_0:
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetSpecificModeVal
        CALL      F:_RpSetSpecificModeVal  ;; 3 cycles
// 4488 
// 4489 	// Address Fileters
// 4490 	if (RpCb.pib.macPanId1 != RP_RF_HW_DFLT_PANID1)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3382  ;; 2 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BZ        ??RpLog_Event_413  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 4491 	{
// 4492 		RpRegBlockWrite(BBPANID0, (uint8_t RP_FAR *)&RpCb.pib.macPanId1, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3382)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, #0xA0           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 9 cycles
// 4493 	}
// 4494 
// 4495 	if (RpCb.pib.macPanId2 != RP_RF_HW_DFLT_PANID2)
??RpLog_Event_413:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3398  ;; 2 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BZ        ??RpLog_Event_414  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 4496 	{
// 4497 		RpRegBlockWrite(BBPANID1, (uint8_t RP_FAR *)&RpCb.pib.macPanId2, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3398)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x700         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 9 cycles
// 4498 	}
// 4499 
// 4500 	if (RpCb.pib.macShortAddr1 != RP_RF_HW_DFLT_SHORTAD1)
??RpLog_Event_414:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3380  ;; 2 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BZ        ??RpLog_Event_415  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 4501 	{
// 4502 		RpRegBlockWrite(BBSHORTAD0, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr1, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3380)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, #0xB0           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 9 cycles
// 4503 	}
// 4504 
// 4505 	if (RpCb.pib.macShortAddr2 != RP_RF_HW_DFLT_SHORTAD2)
??RpLog_Event_415:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3396  ;; 2 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BZ        ??RpLog_Event_416  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 4506 	{
// 4507 		RpRegBlockWrite(BBSHORTAD1, (uint8_t RP_FAR *)&RpCb.pib.macShortAddr2, sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3396)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x710         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 9 cycles
// 4508 	}
// 4509 
// 4510 	if (RpCb.pib.macAddressFilter1Ena)
??RpLog_Event_416:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3378   ;; 2 cycles
        BZ        ??RpLog_Event_417  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 4511 	{
// 4512 		RpRegBlockWrite(BBEXTENDAD00, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress1, sizeof(uint32_t[2]));
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3384)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, #0xC0           ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 9 cycles
// 4513 	}
// 4514 
// 4515 	if (RpCb.pib.macAddressFilter2Ena)
??RpLog_Event_417:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3394   ;; 2 cycles
        BZ        ??RpLog_Event_418  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 4516 	{
// 4517 		RpRegBlockWrite(BBEXTENDAD10, (uint8_t RP_FAR *)&RpCb.pib.macExtendedAddress2, sizeof(uint32_t[2]));
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3400)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x720         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 9 cycles
// 4518 	}
// 4519 
// 4520 	// Other Setting
// 4521 	if (RpCb.pib.phyAntennaDiversityRxEna)
??RpLog_Event_418:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3428   ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetAntennaDiversityVal
        ; ------------------------------------- Block: 4 cycles
// 4522 	{
// 4523 		RpSetAntennaDiversityVal();
        CALL      F:_RpSetAntennaDiversityVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4524 	}
// 4525 
// 4526 	if (RpCb.pib.phyAntennaSelectTx != RP_PHY_ANTENNA_UNUSE)
??RpInitRfOnly_1:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3429, #0xFF  ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetAntennaSelectTxVal
        ; ------------------------------------- Block: 4 cycles
// 4527 	{
// 4528 		RpSetAntennaSelectTxVal();
        CALL      F:_RpSetAntennaSelectTxVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4529 	}
// 4530 
// 4531 	if (RpCb.pib.phyAntennaSwitchEna)
??RpInitRfOnly_2:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3427   ;; 2 cycles
        BZ        ??RpLog_Event_419  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 4532 	{
// 4533 		RpSetAntennaSwitchVal();
        MOV       C, ES:_RpRxBuf+3427  ;; 2 cycles
        MOVW      AX, #0x400         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4534 	}
        ; ------------------------------------- Block: 6 cycles
// 4535 
// 4536 	if (RpCb.pib.phyPreamble4ByteRxMode)
??RpLog_Event_419:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3444   ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetPreamble4ByteRxMode
        ; ------------------------------------- Block: 4 cycles
// 4537 	{
// 4538 		RpSetPreamble4ByteRxMode();
        CALL      F:_RpSetPreamble4ByteRxMode  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4539 	}
// 4540 
// 4541 	RpSetFskOpeModeVal(RP_TRUE);
??RpInitRfOnly_3:
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetFskOpeModeVal
        CALL      F:_RpSetFskOpeModeVal  ;; 3 cycles
// 4542 
// 4543 	// Timer
// 4544 	RpCb.reg.bbTimeCon = (TIMEEN | CNTSRCSEL | STAMPTIMSEL);	// Timer Count Start, count source datarate, Comp0 transmit Disable Stay
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3554, #0x45  ;; 2 cycles
// 4545 
// 4546 	RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
        MOV       C, #0x45           ;; 1 cycle
        MOVW      AX, #0x1A0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4547 
// 4548 	if (RpCb.pib.phyFskPreambleLength != RP_RF_HW_DFLT_FSK_PREAMBLE_LENGTH)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3416  ;; 2 cycles
        CMPW      AX, #0x4           ;; 1 cycle
        BZ        ??RpLog_Event_420  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
// 4549 	{
// 4550 		RpSetPreambleLengthVal();
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3416)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x530         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4551 	}
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 9 cycles
// 4552 
// 4553 	if (RpCb.pib.phyCsmaBackoffPeriod != RP_RF_HW_DFLT_BACKOFF_PERIOD)
??RpLog_Event_420:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3442  ;; 2 cycles
        CMPW      AX, #0x71          ;; 1 cycle
        BZ        ??RpLog_Event_421  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 4554 	{
// 4555 		RpSetCsmaBackoffPeriod();
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3442)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x250         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4556 	}
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 9 cycles
// 4557 
// 4558 	RpSetBackOffSeedVal();
??RpLog_Event_421:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       C, ES:_RpRxBuf+3375  ;; 2 cycles
        MOVW      AX, #0x348         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x1A8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4559 
// 4560 	if (RpCb.pib.phyFskScramblePsdu != RP_RF_HW_DFLT_FSK_SCRAMBLE_PSDU)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3419, #0x1  ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetFskScramblePsduVal
        ; ------------------------------------- Block: 16 cycles
// 4561 	{
// 4562 		RpSetFskScramblePsduVal();
        CALL      F:_RpSetFskScramblePsduVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4563 	}
// 4564 
// 4565 	if (RpCb.pib.phyFcsLength != RP_RF_HW_DFLT_FCS_LENGTH)
??RpInitRfOnly_4:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3421, #0x2  ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetFcsLengthVal
        ; ------------------------------------- Block: 4 cycles
// 4566 	{
// 4567 		RpSetFcsLengthVal();
        CALL      F:_RpSetFcsLengthVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4568 	}
// 4569 
// 4570 	if (RpCb.pib.phyAckWaitDuration != RP_RF_HW_DFLT_ACK_WAIT_DURATION)
??RpInitRfOnly_5:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3424  ;; 2 cycles
        CMPW      AX, #0x1F4         ;; 1 cycle
        BZ        ??RpLog_Event_422  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 4571 	{
// 4572 		RpSetAckWaitDurationVal();
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      DE, #LWRD(_RpRxBuf+3424)  ;; 1 cycle
        MOV       C, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, #0x380         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4573 	}
        POP       AX                 ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 9 cycles
// 4574 
// 4575 	if ((RpCb.pib.phyFskFecTxEna != RP_RF_HW_DFLT_FSK_FEC_TX_ENA) || (RpCb.pib.phyFskFecRxEna != RP_RF_HW_DFLT_FSK_FEC_RX_ENA))
??RpLog_Event_422:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3365   ;; 2 cycles
        BNZ       ??RpLog_Event_423  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP0      ES:_RpRxBuf+3366   ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetFecVal
        ; ------------------------------------- Block: 3 cycles
// 4576 	{
// 4577 		RpSetFecVal();
??RpLog_Event_423:
        CALL      F:_RpSetFecVal     ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4578 	}
// 4579 
// 4580 	if ((RpCb.pib.macAddressFilter1Ena) || (RpCb.pib.macAddressFilter2Ena))
??RpInitRfOnly_6:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3378   ;; 2 cycles
        BNZ       ??RpLog_Event_424  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP0      ES:_RpRxBuf+3394   ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetAckReplyTimeVal
        ; ------------------------------------- Block: 3 cycles
// 4581 	{
// 4582 		RpSetAckReplyTimeVal();
??RpLog_Event_424:
        CALL      F:_RpSetAckReplyTimeVal  ;; 3 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        ; ------------------------------------- Block: 3 cycles
// 4583 	}
// 4584 	RpSetAdrfAndAutoAckVal();
??RpInitRfOnly_7:
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
// 4585 
// 4586 	if (RpCb.pib.phyAgcStartVth != RP_RF_PIB_DFLT_AGC_START_VTH)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3446  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??RpLog_Event_425  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 4587 	{
// 4588 		RpSetAgcStartVth(RP_FALSE);
        CLRB      A                  ;; 1 cycle
        BR        S:??RpLog_Event_426  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 4589 	}
// 4590 	else
// 4591 	{
// 4592 		RpSetAgcStartVth(RP_TRUE);
??RpLog_Event_425:
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetAgcStartVth
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_426:
        CALL      F:_RpSetAgcStartVth  ;; 3 cycles
// 4593 	}
// 4594 
// 4595 	if (RpCb.pib.phyAntennaDiversityStartVth != RP_RF_PIB_DFLT_ANTENNA_DIVERSITY_START_VTH)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3450  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??RpLog_Event_427  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
// 4596 	{
// 4597 		RpSetAntennaDiversityStartVth(RP_FALSE);
        CLRB      A                  ;; 1 cycle
        BR        S:??RpLog_Event_428  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 4598 	}
// 4599 	else
// 4600 	{
// 4601 		RpSetAntennaDiversityStartVth(RP_TRUE);
??RpLog_Event_427:
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetAntennaDiversityStartVth
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_428:
        CALL      F:_RpSetAntennaDiversityStartVth  ;; 3 cycles
// 4602 	}
// 4603 
// 4604 	if (RpCb.pib.phySfdDetectionExtend != RP_RF_PIB_DFLT_SFD_DETECTION_EXTEND)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3454   ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetSfdDetectionExtend
        ; ------------------------------------- Block: 7 cycles
// 4605 	{
// 4606 		RpSetSfdDetectionExtend();
        CALL      F:_RpSetSfdDetectionExtend  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4607 	}
// 4608 
// 4609 	if (RpCb.pib.phyAgcWaitGainOffset != RP_RF_PIB_DFLT_AGC_WAIT_GAIN_OFFSET)
??RpInitRfOnly_8:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3455   ;; 2 cycles
        BZ        ??RpLog_Event_429  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 4610 	{
// 4611 		RpSetAgcWaitGain();
        MOV       A, #0x44           ;; 1 cycle
        SUB       A, ES:_RpRxBuf+3455  ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2200        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
          CFI FunCall _RpSetLvlVthVal
        CALL      F:_RpSetLvlVthVal  ;; 3 cycles
// 4612 	}
        ; ------------------------------------- Block: 11 cycles
// 4613 
// 4614 	if (RpCb.pib.phyAntennaSwitchEnaTiming != RP_RF_PIB_DFLT_ANTENNA_SWITCH_ENA_TIMING)
??RpLog_Event_429:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3458  ;; 2 cycles
        CMPW      AX, #0x12C         ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetAntennaSwitchEnaTiming
        ; ------------------------------------- Block: 5 cycles
// 4615 	{
// 4616 		RpSetAntennaSwitchEnaTiming();
        CALL      F:_RpSetAntennaSwitchEnaTiming  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4617 	}
// 4618 
// 4619 	if (RpCb.pib.phyGpio0Setting != RP_RF_PIB_DFLT_GPIO0_SETTING)
??RpInitRfOnly_9:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3460, #0xFF  ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetGpio0Setting
        ; ------------------------------------- Block: 4 cycles
// 4620 	{
// 4621 		RpSetGpio0Setting();
        CALL      F:_RpSetGpio0Setting  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4622 	}
// 4623 
// 4624 	if (RpCb.pib.phyGpio3Setting != RP_RF_PIB_DFLT_GPIO3_SETTING)
??RpInitRfOnly_10:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3461, #0xFF  ;; 2 cycles
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetGpio3Setting
        ; ------------------------------------- Block: 4 cycles
// 4625 	{
// 4626 		RpSetGpio3Setting();
        CALL      F:_RpSetGpio3Setting  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 4627 	}
// 4628 #ifdef R_FREQUENCY_OFFSET_ENABLED
// 4629 	if (RpCb.pib.phyFrequencyOffset != RP_RF_PIB_DFLT_FREQUENCY_OFFSET)
??RpInitRfOnly_11:
        MOVW      HL, #LWRD(_RpRxBuf+3486)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??RpLog_Event_430  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
// 4630 	{
// 4631 		RpSetChannelVal(RpCb.pib.phyCurrentChannel);
        MOV       A, ES:_RpRxBuf+3354  ;; 2 cycles
          CFI FunCall _RpSetChannelVal
        CALL      F:_RpSetChannelVal  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 4632 	}
// 4633 #endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
// 4634 }
??RpLog_Event_430:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock93
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 437 cycles
// 4635 
// 4636 /******************************************************************************
// 4637 Function Name:       RpSetRegBeforeIdle
// 4638 Parameters:          none
// 4639 Return value:        none
// 4640 Description:         set SFR for evalations for modem
// 4641 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock94 Using cfiCommon1
          CFI Function _RpSetRegBeforeIdle
        CODE
// 4642 static void RpSetRegBeforeIdle( void )
// 4643 {
_RpSetRegBeforeIdle:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 22
        SUBW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+26
// 4644 	uint16_t tmpWord;
// 4645 	uint8_t arrayChar[12];
// 4646 
// 4647 	RpRegWrite(BBAUTORCVCNT, 0x04);
        MOV       C, #0x4            ;; 1 cycle
        MOVW      AX, #0x240         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4648 	arrayChar[0] = 0x0E;	// 0x0052
        MOV       A, #0xE            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
// 4649 	arrayChar[1] = 0x01;	// 0x0053
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x03], A       ;; 1 cycle
// 4650 	arrayChar[2] = (uint8_t)(RP_RF_TX_WARMUP_TIME);	// 0x0054
        MOV       A, #0x5E           ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
// 4651 	arrayChar[3] = (uint8_t)((RP_RF_TX_WARMUP_TIME & 0xff00) >> 8);// 0x0055
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x05], A       ;; 1 cycle
// 4652 	arrayChar[4] = 0x00;// 0x0056(dummy)
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
// 4653 	arrayChar[5] = 0x50;// 0x0057
        MOV       A, #0x50           ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
// 4654 	arrayChar[6] = 0xE6;// 0x0058
        MOV       A, #0xE6           ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
// 4655 	arrayChar[7] = 0x01;// 0x0059(h/w initilal)
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x09], A       ;; 1 cycle
// 4656 	arrayChar[8] = (uint8_t)(RP_RF_RX_WARMUP_TIME);	// 0x005A
        MOV       A, #0xF0           ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
// 4657 	arrayChar[9] = (uint8_t)((RP_RF_RX_WARMUP_TIME & 0xff00) >> 8);// 0x005B
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x0B], A       ;; 1 cycle
// 4658 	arrayChar[10] = arrayChar[8];// 0x005C
        MOV       A, #0xF0           ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
// 4659 	arrayChar[11] = arrayChar[9];// 0x005D
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x0D], A       ;; 1 cycle
// 4660 	RpRegBlockWrite(0x0052 << 3, (uint8_t RP_FAR *)&(arrayChar[0]), 12);
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x14], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x12], AX      ;; 1 cycle
        MOVW      AX, #0xC           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        POP       DE                 ;; 1 cycle
          CFI CFA SP+28
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x290         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4661 
// 4662 	RpRegWrite(0x0078 << 3, 0xE6);
        MOV       C, #0xE6           ;; 1 cycle
        MOVW      AX, #0x3C0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4663 
// 4664 	arrayChar[0] = 0x2C;// 0x007A
        MOV       A, #0x2C           ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
// 4665 	arrayChar[1] = 0x01;// 0x007B
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x05], A       ;; 1 cycle
// 4666 	arrayChar[2] = 0x50;// 0x007C
        MOV       A, #0x50           ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
// 4667 	arrayChar[3] = 0x01;// 0x007D(h/w initial)
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
// 4668 	arrayChar[4] = 0xE6;// 0x007E
        MOV       A, #0xE6           ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
// 4669 	RpRegBlockWrite(0x007A << 3, (uint8_t RP_FAR *)&(arrayChar[0]), 5);
        MOVW      AX, #0x5           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x3D0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4670 
// 4671 	RpRegWrite(0x0086 << 3, 0x03);
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0x430         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4672 	RpRegWrite(0x008E << 3, 0x73);
        MOV       C, #0x73           ;; 1 cycle
        MOVW      AX, #0x470         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4673 
// 4674 	RpCb.reg.bbTxCon2	= 0x7B;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       ES:_RpRxBuf+3568, #0x7B  ;; 2 cycles
// 4675 
// 4676 	arrayChar[0] = 0x26;// 0x0094
        MOV       A, #0x26           ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
// 4677 	arrayChar[1] = 0x08;// 0x0095(dummy, rewrite later)
        MOV       A, #0x8            ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
// 4678 	arrayChar[2] = 0x73;// 0x0096
        MOV       A, #0x73           ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
// 4679 	RpRegBlockWrite(0x0094 << 3, (uint8_t RP_FAR *)&(arrayChar[0]), 3);
        MOVW      AX, #0x3           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x4A0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4680 
// 4681 	RpRegWrite(0x0402 << 3, 0x04);
        MOV       C, #0x4            ;; 1 cycle
        MOVW      AX, #0x2010        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4682 	tmpWord = 0x505;
        MOVW      AX, #0x505         ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 4683 	RpRegBlockWrite(0x046F << 3, (uint8_t RP_FAR *)&(tmpWord), sizeof(uint16_t));
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+26
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x10], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        POP       DE                 ;; 1 cycle
          CFI CFA SP+28
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2378        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4684 	arrayChar[0] = 0x22;// 0x0475
        MOV       A, #0x22           ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
// 4685 	arrayChar[1] = 0xE0;// 0x0476
        MOV       A, #0xE0           ;; 1 cycle
        MOV       [SP+0x05], A       ;; 1 cycle
// 4686 	arrayChar[2] = 0xE0;// 0x0477
        MOV       [SP+0x06], A       ;; 1 cycle
// 4687 	arrayChar[3] = 0x00;// 0x0478
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
// 4688 	RpRegBlockWrite(0x0475 << 3, (uint8_t RP_FAR *)&(arrayChar[0]), 4);
        MOV       X, #0x4            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x23A8        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4689 	tmpWord = 0x0160;
        MOVW      AX, #0x160         ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 4690 	RpRegBlockWrite(0x047A << 3, (uint8_t RP_FAR *)&(tmpWord), sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x23D0        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4691 	RpRegWrite(0x0481 << 3, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x2408        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4692 	RpRegWrite(0x0488 << 3, 0x82);
        MOV       C, #0x82           ;; 1 cycle
        MOVW      AX, #0x2440        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4693 	RpRegWrite(0x04EE << 3, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x2770        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4694 	RpRegWrite(0x04F9 << 3, 0x01);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x27C8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4695 	RpRegWrite(0x0501 << 3, 0x10);
        MOV       C, #0x10           ;; 1 cycle
        MOVW      AX, #0x2808        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4696 	tmpWord = 0x0160;
        MOVW      AX, #0x160         ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 4697 	RpRegBlockWrite(0x050D << 3, (uint8_t RP_FAR *)&(tmpWord), sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2868        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4698 	RpRegWrite(0x0510 << 3, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x2880        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4699 	RpRegWrite(0x0515 << 3, 0x03);
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0x28A8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4700 	RpRegWrite(0x0583 << 3, 0x7F);
        MOV       C, #0x7F           ;; 1 cycle
        MOVW      AX, #0x2C18        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4701 	RpRegWrite(0x0587 << 3, 0x7F);
        MOV       C, #0x7F           ;; 1 cycle
        MOVW      AX, #0x2C38        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4702 	tmpWord = 0x0a0a;
        MOVW      AX, #0xA0A         ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 4703 	RpRegBlockWrite(0x05a5 << 3, (uint8_t RP_FAR *)&(tmpWord), sizeof(uint16_t));
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2D28        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4704 	RpRegWrite(0x05AD << 3, 0x40);
        MOV       C, #0x40           ;; 1 cycle
        MOVW      AX, #0x2D68        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 4705 
// 4706 	RpWrEvaReg1(0x4404);
        MOVW      AX, #0x4404        ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6C0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4707 	RpWrEvaReg2(0x1F8D);
        MOVW      AX, #0x1F8D        ;; 1 cycle
        MOVW      [SP+0x1A], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4708 	RpWrEvaReg2(0x6E00);
        MOVW      AX, #0x6E00        ;; 1 cycle
        MOVW      [SP+0x1C], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4709 	RpWrEvaReg2(0x1088);
        MOVW      AX, #0x1088        ;; 1 cycle
        MOVW      [SP+0x1E], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x20          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4710 	RpWrEvaReg2(0x1108);
        MOVW      AX, #0x1108        ;; 1 cycle
        MOVW      [SP+0x20], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4711 	RpWrEvaReg2(0x1207);
        MOVW      AX, #0x1207        ;; 1 cycle
        MOVW      [SP+0x22], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+48
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x24          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4712 	RpWrEvaReg2(0x1306);
        MOVW      AX, #0x1306        ;; 1 cycle
        MOVW      [SP+0x24], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+50
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x26          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4713 	RpWrEvaReg2(0x1405);
        MOVW      AX, #0x1405        ;; 1 cycle
        MOVW      [SP+0x26], AX      ;; 1 cycle
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+52
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x6E0         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 4714 }
        ADDW      SP, #0x30          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock94
        ; ------------------------------------- Block: 324 cycles
        ; ------------------------------------- Total: 324 cycles
// 4715 
// 4716 /******************************************************************************
// 4717 Function Name:       RpSetFreqAddReg
// 4718 Parameters:          freq
// 4719 Return value:        none
// 4720 Description:         set SFR for FREQ additional registers
// 4721 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock95 Using cfiCommon2
          CFI Function _RpSetFreqAddReg
        CODE
// 4722 static void RpSetFreqAddReg( uint32_t freq, uint8_t freqBandId )
// 4723 {
_RpSetFreqAddReg:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
// 4724 #define ARRAY_SIZE(ary)  (sizeof(ary)/sizeof((ary)[0]))
// 4725 
// 4726 	uint8_t reg0x0095Val = 0x08;
        MOV       A, #0x8            ;; 1 cycle
        MOV       [SP+0x05], A       ;; 1 cycle
// 4727 	uint8_t opeMode = RpCb.pib.phyFskOpeMode;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3420  ;; 2 cycles
// 4728 
// 4729 	uint8_t i;
// 4730 	RP_DDC_TABLE RP_FAR *ptable;
// 4731 	uint8_t arraySize;
// 4732 
// 4733 	// Select table, Get arryay size
// 4734 	if (freqBandId == RP_PHY_FREQ_BAND_863MHz)
        XCH       A, E               ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        XCH       A, E               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_431  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
// 4735 	{
// 4736 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_863MHz_OPE1))
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_432  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3372        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x3E08        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_0:
        SKC                          ;; 4 cycles
        BR        R:??RpLog_Event_433  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 4737 		{
// 4738 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe1DdcTbl[0]);
// 4739 			arraySize = ARRAY_SIZE(RpFreq863MHzOpe1DdcTbl);
// 4740 		}
// 4741 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_863MHz_OPE2))
// 4742 		{
// 4743 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe2DdcTbl[0]);
// 4744 			arraySize = ARRAY_SIZE(RpFreq863MHzOpe2DdcTbl);
// 4745 		}
// 4746 		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_863MHz_OPE3))
// 4747 		{
// 4748 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe3DdcTbl[0]);
// 4749 			arraySize = ARRAY_SIZE(RpFreq863MHzOpe3DdcTbl);
// 4750 		}
// 4751 		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_863MHz_OPE4))	// Use RpFreq863MHzOpe2DdcTbl
// 4752 		{
// 4753 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe2DdcTbl[0]);
// 4754 			arraySize = ARRAY_SIZE(RpFreq863MHzOpe2DdcTbl);
// 4755 		}
// 4756 		else if ((opeMode == RP_PHY_FSK_OPEMODE_5) && (freq >= RP_FREQ_START_863MHz_OPE5))	// Use RpFreq863MHzOpe2DdcTbl
// 4757 		{
// 4758 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe2DdcTbl[0]);
// 4759 			arraySize = ARRAY_SIZE(RpFreq863MHzOpe2DdcTbl);
// 4760 		}
// 4761 		else if ((opeMode == RP_PHY_FSK_OPEMODE_6) && (freq >= RP_FREQ_START_863MHz_OPE6))	// Use RpFreq863MHzOpe3DdcTbl
// 4762 		{
// 4763 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe3DdcTbl[0]);
// 4764 			arraySize = ARRAY_SIZE(RpFreq863MHzOpe3DdcTbl);
// 4765 		}
// 4766 		else if ((opeMode == RP_PHY_FSK_OPEMODE_7) && (freq >= RP_FREQ_START_863MHz_OPE7))	// Use RpFreq863MHzOpe1DdcTbl
// 4767 		{
// 4768 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq863MHzOpe1DdcTbl[0]);
// 4769 			arraySize = ARRAY_SIZE(RpFreq863MHzOpe1DdcTbl);
// 4770 		}
// 4771 		else
// 4772 		{
// 4773 			arraySize = 0;
// 4774 		}
// 4775 	}
// 4776 	else if (freqBandId == RP_PHY_FREQ_BAND_896MHz)
// 4777 	{
// 4778 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_896MHz_OPE1))
// 4779 		{
// 4780 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq896MHzOpe1DdcTbl[0]);
// 4781 			arraySize = ARRAY_SIZE(RpFreq896MHzOpe1DdcTbl);
// 4782 		}
// 4783 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_896MHz_OPE2))
// 4784 		{
// 4785 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq896MHzOpe2DdcTbl[0]);
// 4786 			arraySize = ARRAY_SIZE(RpFreq896MHzOpe2DdcTbl);
// 4787 		}
// 4788 		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_896MHz_OPE3))
// 4789 		{
// 4790 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq896MHzOpe3DdcTbl[0]);
// 4791 			arraySize = ARRAY_SIZE(RpFreq896MHzOpe3DdcTbl);
// 4792 		}
// 4793 		else
// 4794 		{
// 4795 			arraySize = 0;
// 4796 		}
// 4797 	}
// 4798 	else if (freqBandId == RP_PHY_FREQ_BAND_901MHz)
// 4799 	{
// 4800 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_901MHz_OPE1))
// 4801 		{
// 4802 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq901MHzOpe1DdcTbl[0]);
// 4803 			arraySize = ARRAY_SIZE(RpFreq901MHzOpe1DdcTbl);
// 4804 		}
// 4805 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_901MHz_OPE2))
// 4806 		{
// 4807 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq901MHzOpe2DdcTbl[0]);
// 4808 			arraySize = ARRAY_SIZE(RpFreq901MHzOpe2DdcTbl);
// 4809 		}
// 4810 		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_901MHz_OPE3))
// 4811 		{
// 4812 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq901MHzOpe3DdcTbl[0]);
// 4813 			arraySize = ARRAY_SIZE(RpFreq901MHzOpe3DdcTbl);
// 4814 		}
// 4815 		else
// 4816 		{
// 4817 			arraySize = 0;
// 4818 		}
// 4819 	}
// 4820 	else if (freqBandId == RP_PHY_FREQ_BAND_915MHz)
// 4821 	{
// 4822 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_915MHz_OPE1))
// 4823 		{
// 4824 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe1DdcTbl[0]);
// 4825 			arraySize = ARRAY_SIZE(RpFreq915MHzOpe1DdcTbl);
// 4826 		}
// 4827 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_915MHz_OPE2))
// 4828 		{
// 4829 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe2DdcTbl[0]);
// 4830 			arraySize = ARRAY_SIZE(RpFreq915MHzOpe2DdcTbl);
// 4831 		}
// 4832 		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_915MHz_OPE3))
// 4833 		{
// 4834 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe3DdcTbl[0]);
// 4835 			arraySize = ARRAY_SIZE(RpFreq915MHzOpe3DdcTbl);
// 4836 		}
// 4837 		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_915MHz_OPE4))	// Use RpFreq915MHzOpe1DdcTbl
// 4838 		{
// 4839 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe1DdcTbl[0]);
// 4840 			arraySize = ARRAY_SIZE(RpFreq915MHzOpe1DdcTbl);
// 4841 		}
// 4842 		else
// 4843 		{
// 4844 			arraySize = 0;
// 4845 		}
// 4846 	}
// 4847 	else if (freqBandId == RP_PHY_FREQ_BAND_917MHz)
// 4848 	{
// 4849 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_917MHz_OPE1))
// 4850 		{
// 4851 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq917MHzOpe1DdcTbl[0]);
// 4852 			arraySize = ARRAY_SIZE(RpFreq917MHzOpe1DdcTbl);
// 4853 		}
// 4854 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_917MHz_OPE2))
// 4855 		{
// 4856 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq917MHzOpe2DdcTbl[0]);
// 4857 			arraySize = ARRAY_SIZE(RpFreq917MHzOpe2DdcTbl);
// 4858 		}
// 4859 		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_917MHz_OPE3))
// 4860 		{
// 4861 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq917MHzOpe3DdcTbl[0]);
// 4862 			arraySize = ARRAY_SIZE(RpFreq917MHzOpe3DdcTbl);
// 4863 		}
// 4864 		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_917MHz_OPE4))	// Use RpFreq917MHzOpe1DdcTbl
// 4865 		{
// 4866 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq917MHzOpe1DdcTbl[0]);
// 4867 			arraySize = ARRAY_SIZE(RpFreq917MHzOpe1DdcTbl);
// 4868 		}
// 4869 		else
// 4870 		{
// 4871 			arraySize = 0;
// 4872 		}
// 4873 	}
// 4874 	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz)
// 4875 	{
// 4876 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_920MHz_OPE1))
// 4877 		{
// 4878 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe1DdcTbl[0]);
// 4879 			arraySize = ARRAY_SIZE(RpFreq920MHzOpe1DdcTbl);
// 4880 		}
// 4881 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_920MHz_OPE2))
// 4882 		{
// 4883 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe2DdcTbl[0]);
// 4884 			arraySize = ARRAY_SIZE(RpFreq920MHzOpe2DdcTbl);
// 4885 		}
// 4886 		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_920MHz_OPE3))
// 4887 		{
// 4888 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe3DdcTbl[0]);
// 4889 			arraySize = ARRAY_SIZE(RpFreq920MHzOpe3DdcTbl);
// 4890 		}
// 4891 		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_920MHz_OPE4))
// 4892 		{
// 4893 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe4DdcTbl[0]);
// 4894 			arraySize = ARRAY_SIZE(RpFreq920MHzOpe4DdcTbl);
// 4895 		}
// 4896 		else if ((opeMode == RP_PHY_FSK_OPEMODE_5) && (freq >= RP_FREQ_START_920MHz_OPE5))	// Use RpFreq920MHzOpe1DdcTbl
// 4897 		{
// 4898 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe1DdcTbl[0]);
// 4899 			arraySize = ARRAY_SIZE(RpFreq920MHzOpe1DdcTbl);
// 4900 		}
// 4901 		else if ((opeMode == RP_PHY_FSK_OPEMODE_6) && (freq >= RP_FREQ_START_920MHz_OPE6))	// Use RpFreq920MHzOpe1DdcTbl
// 4902 		{
// 4903 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOpe1DdcTbl[0]);
// 4904 			arraySize = ARRAY_SIZE(RpFreq920MHzOpe1DdcTbl);
// 4905 		}
// 4906 		else
// 4907 		{
// 4908 			arraySize = 0;
// 4909 		}
// 4910 	}
// 4911 	else if (freqBandId == RP_PHY_FREQ_BAND_920MHz_Others)
// 4912 	{
// 4913 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_920MHz_Others_OPE1))
// 4914 		{
// 4915 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe1DdcTbl[0]);
// 4916 			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe1DdcTbl);
// 4917 		}
// 4918 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_920MHz_Others_OPE2))
// 4919 		{
// 4920 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe2DdcTbl[0]);
// 4921 			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe2DdcTbl);
// 4922 		}
// 4923 		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_920MHz_Others_OPE3))
// 4924 		{
// 4925 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe3DdcTbl[0]);
// 4926 			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe3DdcTbl);
// 4927 		}
// 4928 		else
// 4929 		{
// 4930 			arraySize = 0;
// 4931 		}
// 4932 	}
// 4933 	else if (freqBandId == RP_PHY_FREQ_BAND_870MHz)
// 4934 	{
// 4935 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_870MHz_OPE1))
// 4936 		{
// 4937 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq870MHzOpe1DdcTbl[0]);
// 4938 			arraySize = ARRAY_SIZE(RpFreq870MHzOpe1DdcTbl);
// 4939 		}
// 4940 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_870MHz_OPE2))	// Use RpFreq870MHzOpe1DdcTbl
// 4941 		{
// 4942 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq870MHzOpe1DdcTbl[0]);
// 4943 			arraySize = ARRAY_SIZE(RpFreq870MHzOpe1DdcTbl);
// 4944 		}
// 4945 		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_870MHz_OPE3))	// Use RpFreq870MHzOpe1DdcTbl
// 4946 		{
// 4947 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq870MHzOpe1DdcTbl[0]);
// 4948 			arraySize = ARRAY_SIZE(RpFreq870MHzOpe1DdcTbl);
// 4949 		}
// 4950 		else
// 4951 		{
// 4952 			arraySize = 0;
// 4953 		}
// 4954 	}
// 4955 	else if (freqBandId == RP_PHY_FREQ_BAND_902MHz)
// 4956 	{
// 4957 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_902MHz_OPE1))		// Use RpFreq915MHzOpe1DdcTbl
// 4958 		{
// 4959 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq915MHzOpe1DdcTbl[0]);
// 4960 			arraySize = ARRAY_SIZE(RpFreq915MHzOpe1DdcTbl);
// 4961 		}
// 4962 		else
// 4963 		{
// 4964 			arraySize = 0;
// 4965 		}
// 4966 	}
// 4967 	else if (freqBandId == RP_PHY_FREQ_BAND_921MHz)
// 4968 	{
// 4969 		if ((opeMode == RP_PHY_FSK_OPEMODE_1) && (freq >= RP_FREQ_START_921MHz_OPE1))		// Use RpFreq920MHzOthersOpe2DdcTbl
// 4970 		{
// 4971 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe2DdcTbl[0]);
// 4972 			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe2DdcTbl);
// 4973 		}
// 4974 		else if ((opeMode == RP_PHY_FSK_OPEMODE_2) && (freq >= RP_FREQ_START_921MHz_OPE2))
// 4975 		{
// 4976 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq921MHzOpe2DdcTbl[0]);
// 4977 			arraySize = ARRAY_SIZE(RpFreq921MHzOpe2DdcTbl);
// 4978 		}
// 4979 		else if ((opeMode == RP_PHY_FSK_OPEMODE_3) && (freq >= RP_FREQ_START_921MHz_OPE3))	// Use RpFreq920MHzOthersOpe2DdcTbl
// 4980 		{
// 4981 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe2DdcTbl[0]);
// 4982 			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe2DdcTbl);
// 4983 		}
// 4984 		else if ((opeMode == RP_PHY_FSK_OPEMODE_4) && (freq >= RP_FREQ_START_921MHz_OPE4))	// Use RpFreq920MHzOthersOpe2DdcTbl
// 4985 		{
// 4986 			ptable = (RP_DDC_TABLE RP_FAR *)&(RpFreq920MHzOthersOpe2DdcTbl[0]);
// 4987 			arraySize = ARRAY_SIZE(RpFreq920MHzOthersOpe2DdcTbl);
// 4988 		}
// 4989 		else
// 4990 		{
// 4991 			arraySize = 0;
// 4992 		}
// 4993 	}
// 4994 	else
// 4995 	{
// 4996 		arraySize = 0;
??RpSetFreqAddReg_1:
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 4997 	}
// 4998 
// 4999 	// Get setting value from selected table
// 5000 	for (i = 0; i < arraySize; i++)
??RpSetFreqAddReg_2:
        MOV       [SP+0x06], A       ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        BR        R:??RpLog_Event_434  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_432:
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_435  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3373        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xC4A8        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_3:
        BC        ??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        S:??RpLog_Event_436  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_435:
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??RpLog_Event_437  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3373        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xC4A8        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_4:
        BC        ??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        S:??RpLog_Event_438  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_437:
        CMP       A, #0x4            ;; 1 cycle
        BZ        ??RpLog_Event_439  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x5            ;; 1 cycle
        BNZ       ??RpLog_Event_440  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_439:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3371        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xDC60        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_5:
        BC        ??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_436:
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+84)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpSetFreqAddReg_6:
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0xC            ;; 1 cycle
        BR        S:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_440:
        CMP       A, #0x6            ;; 1 cycle
        BNZ       ??RpLog_Event_441  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3371        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xDC60        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_7:
        BC        ??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_438:
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+156)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0xD            ;; 1 cycle
        BR        S:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_441:
        CMP       A, #0x7            ;; 1 cycle
        BNZ       ??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3371        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xDC60        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_8:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_433:
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0xE            ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_431:
        XCH       A, E               ;; 1 cycle
        CMP       A, #0x5            ;; 1 cycle
        XCH       A, E               ;; 1 cycle
        BNZ       ??RpLog_Event_442  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_443  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3568        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x10D4        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_9:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+234)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpSetFreqAddReg_10:
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x9            ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_443:
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_444  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3568        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x41A8        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_11:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+288)  ;; 1 cycle
        BR        S:??RpSetFreqAddReg_10  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_444:
        CMP       A, #0x3            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x3568        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xA350        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_12:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+342)  ;; 1 cycle
        BR        S:??RpSetFreqAddReg_10  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_442:
        XCH       A, E               ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        XCH       A, E               ;; 1 cycle
        BNZ       ??RpLog_Event_445  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_446  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x35B4        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x5C14        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_13:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+396)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpSetFreqAddReg_14:
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x4            ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_446:
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_447  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x35B4        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x8CE8        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_15:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+420)  ;; 1 cycle
        BR        S:??RpSetFreqAddReg_14  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_447:
        CMP       A, #0x3            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x35B4        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xEE90        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_16:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+444)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x5            ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_445:
        XCH       A, E               ;; 1 cycle
        CMP       A, #0x7            ;; 1 cycle
        XCH       A, E               ;; 1 cycle
        BNZ       ??RpLog_Event_448  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x1            ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_449  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_450  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x35C9        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x8800        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_17:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+828)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x37           ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_450:
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??RpLog_Event_451  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x35C9        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x8800        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_18:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+1158)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x3D           ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_451:
        CMP       A, #0x4            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        R:??RpLog_Event_449  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_448:
        XCH       A, E               ;; 1 cycle
        CMP       A, #0x8            ;; 1 cycle
        XCH       A, E               ;; 1 cycle
        BNZ       ??RpLog_Event_452  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_453  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_454  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36AC        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xE320        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_19:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+1584)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpSetFreqAddReg_20:
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0xA            ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_454:
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??RpLog_Event_455  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36AC        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xE320        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_21:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+1644)  ;; 1 cycle
        BR        S:??RpSetFreqAddReg_20  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_455:
        CMP       A, #0x4            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_453:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36A9        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xD5E0        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_22:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+1524)  ;; 1 cycle
        BR        S:??RpSetFreqAddReg_20  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_452:
        XCH       A, E               ;; 1 cycle
        CMP       A, #0x9            ;; 1 cycle
        XCH       A, E               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_456  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_457  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_458  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36E0        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xC460        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_23:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+1818)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpSetFreqAddReg_24:
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x11           ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_458:
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??RpLog_Event_459  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36E2        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x4B00        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_25:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+1920)  ;; 1 cycle
        BR        S:??RpSetFreqAddReg_24  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_459:
        CMP       A, #0x4            ;; 1 cycle
        BNZ       ??RpLog_Event_460  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36E2        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x4B00        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_26:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+2022)  ;; 1 cycle
        BR        R:??RpSetFreqAddReg_6  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_460:
        CMP       A, #0x5            ;; 1 cycle
        BNZ       ??RpLog_Event_461  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36E0        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xC460        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_27:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        S:??RpLog_Event_462  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_461:
        CMP       A, #0x6            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_457:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36DF        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x3DC0        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_28:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_462:
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+1704)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x13           ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_456:
        XCH       A, E               ;; 1 cycle
        CMP       A, #0xE            ;; 1 cycle
        XCH       A, E               ;; 1 cycle
        BNZ       ??RpLog_Event_463  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_464  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36E2        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x4B00        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_29:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+2094)  ;; 1 cycle
        BR        R:??RpSetFreqAddReg_24  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_464:
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_465  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x35C6        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x7AC0        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_30:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        R:??RpLog_Event_466  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpLog_Event_465:
        CMP       A, #0x3            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x35CC        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x9540        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_31:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+2550)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x2F           ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_463:
        XCH       A, E               ;; 1 cycle
        CMP       A, #0xF            ;; 1 cycle
        XCH       A, E               ;; 1 cycle
        BNZ       ??RpLog_Event_467  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_468  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x2            ;; 1 cycle
        BZ        ??RpLog_Event_468  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x3            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_468:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x33DC        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0xAC20        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_32:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+2832)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x6            ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_467:
        XCH       A, E               ;; 1 cycle
        CMP       A, #0x10           ;; 1 cycle
        XCH       A, E               ;; 1 cycle
        BNZ       ??RpLog_Event_469  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x1            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_449:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x35C6        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x7AC0        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_33:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+474)  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpSetFreqAddReg_34:
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, #0x3B           ;; 1 cycle
        BR        R:??RpSetFreqAddReg_2  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_469:
        XCH       A, E               ;; 1 cycle
        CMP       A, #0x11           ;; 1 cycle
        MOV       A, E               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x1            ;; 1 cycle
        BZ        ??RpLog_Event_470  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RpLog_Event_471  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36DF        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x9F68        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_35:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+2868)  ;; 1 cycle
        BR        S:??RpSetFreqAddReg_34  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_471:
        CMP       A, #0x3            ;; 1 cycle
        BZ        ??RpLog_Event_470  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP       A, #0x4            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpLog_Event_470:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x36DF        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x9F68        ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpSetFreqAddReg_36:
        SKNC                         ;; 4 cycles
        BR        R:??RpSetFreqAddReg_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_466:
        MOV       [SP+0x02], #BYTE3(_RpFreq863MHzOpe1DdcTbl)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpFreq863MHzOpe1DdcTbl+2196)  ;; 1 cycle
        BR        S:??RpSetFreqAddReg_34  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 5001 	{
// 5002 		if (freq <= ptable->freq)
// 5003 		{
// 5004 			reg0x0095Val = ptable->reg;
// 5005 			break;
// 5006 		}
// 5007 		ptable++;
??RpSetFreqAddReg_37:
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        INC       A                  ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
??RpLog_Event_434:
        MOV       [SP+0x04], A       ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        CMP       X, A               ;; 1 cycle
        BNC       ??RpLog_Event_472  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??RpLog_Event_473  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??RpLog_Event_473  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??RpLog_Event_473  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_473:
        MOVW      AX, DE             ;; 1 cycle
        BC        ??RpSetFreqAddReg_37  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x05], A       ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
// 5008 	}
// 5009 	RpRegWrite(0x0095 << 3, reg0x0095Val);
??RpLog_Event_472:
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x4A8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5010 
// 5011 #undef ARRAY_SIZE
// 5012 }
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock95
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 799 cycles
// 5013 
// 5014 /******************************************************************************
// 5015 Function Name:       RpSetRfInt
// 5016 Parameters:          enable: 0:disable interrupt
// 5017                              1:enable interrupt nothing to do
// 5018 Return value:        none
// 5019 Description:         control of using RF interrput.
// 5020 ******************************************************************************/
// 5021 #ifndef RP_ST_ENV
// 5022 static void RpSetRfInt( uint8_t enable )
// 5023 #else
// 5024 void RpSetRfInt( uint8_t enable )
// 5025 #endif // #ifndef RP_ST_ENV
// 5026 {
// 5027 	RpReadIrq();	//dummy read for clearing all interrupts
// 5028 	if (enable)
// 5029 	{
// 5030 		// set RF IC interrupt
// 5031 		RpCb.reg.bbIntEn[0] = 0x00;
// 5032 		RpCb.reg.bbIntEn[1] = (RCVFININTEN | RCV0INTEN | RCV1INTEN | RCVSTINTEN | ROVRINTEN | MODESWINTEN | LVLFILINTEN);
// 5033 		RpCb.reg.bbIntEn[2] = 0x00;
// 5034 	}
// 5035 	else
// 5036 	{
// 5037 		// disable RF IC interrupt
// 5038 		RpCb.reg.bbIntEn[0] = 0x00;
// 5039 		RpCb.reg.bbIntEn[1] = 0x00;
// 5040 		RpCb.reg.bbIntEn[2] = 0x00;
// 5041 	}
// 5042 	RpRegBlockWrite(BBINTEN0, (uint8_t RP_FAR *)RpCb.reg.bbIntEn, sizeof(RpCb.reg.bbIntEn));
// 5043 }
// 5044 
// 5045 /******************************************************************************
// 5046 Function Name:       RpWaitaLittle
// 5047 Parameters:          none
// 5048 Return value:        none
// 5049 Description:         make a little time
// 5050 ******************************************************************************/
// 5051 #ifdef __CCRL__
// 5052 	void RpWaitaLittle( void )
// 5053 #else
// 5054 	static void RpWaitaLittle( void )
// 5055 #endif /* __CCRL__ */
// 5056 {
// 5057 }
// 5058 
// 5059 /******************************************************************************
// 5060 Function Name:       RpWait4us
// 5061 Parameters:          none
// 5062 Return value:        none
// 5063 Description:         make about 4uS
// 5064 ******************************************************************************/
// 5065 #ifdef __CCRL__
// 5066 #pragma inline_asm RpWait4us_asm
// 5067 void RpWait4us_asm( void )
// 5068 {
// 5069 #if RP_CPU_CLK == 32
// 5070 	call !!_RpWaitaLittle
// 5071 	call !!_RpWaitaLittle
// 5072 	call !!_RpWaitaLittle
// 5073 	call !!_RpWaitaLittle
// 5074 	call !!_RpWaitaLittle
// 5075 	call !!_RpWaitaLittle
// 5076 	call !!_RpWaitaLittle
// 5077 	call !!_RpWaitaLittle
// 5078 	call !!_RpWaitaLittle
// 5079 	call !!_RpWaitaLittle
// 5080 	call !!_RpWaitaLittle
// 5081 	call !!_RpWaitaLittle
// 5082 	call !!_RpWaitaLittle
// 5083 	nop
// 5084 	nop
// 5085 #else // if RP_CPU_CLK == 8
// 5086 	call !!_RpWaitaLittle
// 5087 	call !!_RpWaitaLittle
// 5088 	nop
// 5089 	nop
// 5090 	nop
// 5091 	nop
// 5092 	nop
// 5093 #endif
// 5094 }
// 5095 #endif /* __CCRL__ */
// 5096 
// 5097 #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
// 5098 static void RpWait4us( void )
// 5099 #else

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock96 Using cfiCommon1
          CFI Function _RpWait4us
          CFI NoCalls
        CODE
// 5100 void RpWait4us( void )
// 5101 #endif /* !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV) */
// 5102 {
_RpWait4us:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 5103 #if defined(__RL78__)
// 5104 #ifdef __CCRL__
// 5105 	RpWait4us_asm();
// 5106 
// 5107 #else /* __CCRL__ */
// 5108 #if RP_CPU_CLK == 32
// 5109 	RpWaitaLittle();
// 5110 	RpWaitaLittle();
// 5111 	RpWaitaLittle();
// 5112 	RpWaitaLittle();
// 5113 	RpWaitaLittle();
// 5114 	RpWaitaLittle();
// 5115 	RpWaitaLittle();
// 5116 	RpWaitaLittle();
// 5117 	RpWaitaLittle();
// 5118 	RpWaitaLittle();
// 5119 	RpWaitaLittle();
// 5120 	RpWaitaLittle();
// 5121 	RpWaitaLittle();
// 5122 	RpWaitaLittle();
// 5123 	RpWaitaLittle();
// 5124 	RpWaitaLittle();
// 5125 #else // if RP_CPU_CLK == 8
// 5126 	__asm("call !!_RpWaitaLittle");
// 5127 	__asm("call !!_RpWaitaLittle");
// 5128 	__asm("nop");
// 5129 	__asm("nop");
// 5130 	__asm("nop");
// 5131 	__asm("nop");
// 5132 	__asm("nop");
// 5133 #endif
// 5134 #endif /* __CCRL__ */
// 5135 
// 5136 #elif defined(__RX)
// 5137 #if RP_CPU_CLK == 80
// 5138 	uint16_t i;
// 5139 
// 5140 	for (i = 0; i < 12; ++i)
// 5141 	{
// 5142 		RpWaitaLittle();
// 5143 		RpWaitaLittle();
// 5144 		RpWaitaLittle();
// 5145 		nop();
// 5146 		nop();
// 5147 		nop();
// 5148 	}
// 5149 #endif
// 5150 #endif
// 5151 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock96
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 6 cycles
// 5152 
// 5153 /***************************************************************************************************************
// 5154  * function name  : RpRelRxBuf
// 5155  * description    : Receive Buffer Release
// 5156  * parameters     : pBuf...Starting address of the receive buffer area to be released
// 5157  * return value   : RP_SUCCESS, RP_INVALID_PARAMETER
// 5158  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock97 Using cfiCommon0
          CFI Function _RpRelRxBuf
        CODE
// 5159 int16_t RpRelRxBuf( uint8_t *pBuf )
// 5160 {
_RpRelRxBuf:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 14
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+18
// 5161 	int16_t	i;
// 5162 	int16_t	rtn = RP_INVALID_PARAMETER;
        MOVW      AX, #0x5           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 5163 
// 5164 	#if defined(__RX)
// 5165 	uint32_t bkupPsw;
// 5166 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 5167 	uint8_t  bkupPsw;
// 5168 	#elif defined(__arm)
// 5169 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 5170 	#endif
// 5171 
// 5172 	/* Disable interrupt */
// 5173 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5174 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 5175 	#else
// 5176 	RP_PHY_DI();
// 5177 	#endif
// 5178 
// 5179 	/* API function execution Log */
// 5180 	RpLog_Event( RP_LOG_API_RELBUF, RP_NULL );
// 5181 
// 5182 	for (i = 0; i < RP_RX_BUF_NUM; i++)
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 16 cycles
// 5183 	{
// 5184 		if ((RpRxBuf[i].status == RP_TRUE) && (pBuf == RpRxBuf[i].buf))
??RpRelRxBuf_0:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, #0x641         ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpRxBuf)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RpLog_Event_474  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, HL             ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??RpLog_Event_475  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_475:
        BNZ       ??RpLog_Event_474  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 5185 		{
// 5186 			RpRxBuf[i].status = RP_FALSE;
        MOV       A, E               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 5187 
// 5188 			if (RpCb.rx.waitRelRxBufWhenAutoRx == RP_TRUE)
        CMP       ES:_RpRxBuf+3346, #0x1  ;; 2 cycles
        BNZ       ??RpLog_Event_476  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
// 5189 			{
// 5190 				RpCb.rx.waitRelRxBufWhenAutoRx = RP_FALSE;
        CLRB      ES:_RpRxBuf+3346   ;; 2 cycles
// 5191 
// 5192 				if (RpCb.status & (RP_PHY_STAT_TRX_TO_RX_AUTO | RP_PHY_STAT_RXON_BACKOFF))
        MOVW      AX, ES:_RpRxBuf+3202  ;; 2 cycles
        AND       A, #0x11           ;; 1 cycle
        BZ        ??RpLog_Event_476  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 5193 				{
// 5194 					RpCb.rx.pTopRxBuf = RpRxBuf[i].buf;
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3314)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 5195 					RpRxBuf[i].status = RP_TRUE;
        DECW      HL                 ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 12 cycles
// 5196 				}
// 5197 			}
// 5198 			rtn = RP_SUCCESS;
??RpLog_Event_476:
        MOVW      AX, #0x7           ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 5199 		}
// 5200 	}
??RpLog_Event_474:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8002        ;; 1 cycle
        BC        ??RpRelRxBuf_0     ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
// 5201 
// 5202 	/* API function execution Log */
// 5203 	RpLog_Event( RP_LOG_API_RELBUF | RP_LOG_API_RET, (uint8_t)rtn );
// 5204 
// 5205 	/* Enable interrupt */
// 5206 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5207 	RP_PHY_EI(bkupPsw);
        MOV       A, [SP+0x02]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 5208 	#else
// 5209 	RP_PHY_EI();
// 5210 	#endif
// 5211 
// 5212 	return (rtn);
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock97
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 121 cycles
// 5213 }
// 5214 
// 5215 /******************************************************************************
// 5216 Function Name:       RpGetRxBuf
// 5217 Parameters:          none
// 5218 Return value:        pointer of got buffer
// 5219 Description:         Get Rx Buffer
// 5220 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock98 Using cfiCommon1
          CFI Function _RpGetRxBuf
          CFI NoCalls
        CODE
// 5221 uint8_t *RpGetRxBuf( void )
// 5222 {
_RpGetRxBuf:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 5223 	int16_t	i;
// 5224 
// 5225 	for (i = 0; i < RP_RX_BUF_NUM; i++)
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
// 5226 	{
// 5227 		if (RpRxBuf[i].status == RP_FALSE)
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf        ;; 2 cycles
        BNZ       ??RpLog_Event_477  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
// 5228 		{
// 5229 			RpRxBuf[i].status = RP_TRUE;
??RpGetRxBuf_0:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #LWRD(_RpRxBuf)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 5230 			RpCb.rx.waitRelRxBufWhenAutoRx = RP_FALSE;
        CLRB      ES:_RpRxBuf+3346   ;; 2 cycles
// 5231 
// 5232 			return (RpRxBuf[i].buf);
        INCW      DE                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        BR        S:??RpLog_Event_478  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
// 5233 		}
??RpLog_Event_477:
        MOVW      HL, #0x641         ;; 1 cycle
        CMP0      ES:_RpRxBuf+1601   ;; 2 cycles
        BZ        ??RpGetRxBuf_0     ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 5234 	}
// 5235 
// 5236 	return (RP_NULL);
        MOVW      DE, AX             ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_478:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock98
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 43 cycles
// 5237 }
// 5238 
// 5239 /******************************************************************************
// 5240 Function Name:       RpInitRxBuf
// 5241 Parameters:          none
// 5242 Return value:        none
// 5243 Description:         Initialize Rx Buffer
// 5244 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock99 Using cfiCommon1
          CFI Function _RpInitRxBuf
          CFI NoCalls
        CODE
// 5245 static void RpInitRxBuf( void )
// 5246 {
_RpInitRxBuf:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 5247 	uint8_t cnt;
// 5248 
// 5249 	for (cnt = 0; cnt < RP_RX_BUF_NUM; ++cnt)
// 5250 	{
// 5251 		RpRxBuf[cnt].status = RP_FALSE;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRB      ES:_RpRxBuf        ;; 2 cycles
        CLRB      ES:_RpRxBuf+1601   ;; 2 cycles
// 5252 	}
// 5253 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock99
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 11 cycles
// 5254 
// 5255 /******************************************************************************
// 5256 Function Name:       RpExtChkOptErrDataReq
// 5257 Parameters:          options:options of RpPdDataReq
// 5258 Return value:        0:options is available
// 5259 				 1:options is not available
// 5260 Description:         Checks if the options is available or not.
// 5261 ******************************************************************************/
// 5262 static int16_t RpExtChkOptErrDataReq( uint8_t options )
// 5263 {
// 5264 	uint8_t i;
// 5265 
// 5266 	for (i = 0; i < RP_PHYEXT_DATAREQ_NUM; ++i)
// 5267 	{
// 5268 		if ((options & RpOptionChkDataReq[i][0]) == RpOptionChkDataReq[i][1])
// 5269 		{
// 5270 			break;
// 5271 		}
// 5272 	}
// 5273 
// 5274 	if (i < RP_PHYEXT_DATAREQ_NUM)
// 5275 	{
// 5276 		return (RP_TRUE);
// 5277 	}
// 5278 	else
// 5279 	{
// 5280 		return (RP_FALSE);
// 5281 	}
// 5282 }
// 5283 
// 5284 /******************************************************************************
// 5285 Function Name:       RpExtChkOptErrRxStateReq
// 5286 Parameters:          options:options of RpPlmeTrxStateReq,RP_RX_ON
// 5287 Return value:        0:options is available
// 5288 					 1:options is not available
// 5289 Description:         Checks if the options is available or not.
// 5290 ******************************************************************************/
// 5291 static int16_t RpExtChkOptErrRxStateReq( uint8_t options )
// 5292 {
// 5293 	uint8_t i;
// 5294 
// 5295 	for (i = 0; i < RP_PHYEXT_RXSTATREQ_NUM; ++i)
// 5296 	{
// 5297 		if ((options & RpOptionChkRxStateReq[i][0]) == RpOptionChkRxStateReq[i][1])
// 5298 		{
// 5299 			break;
// 5300 		}
// 5301 	}
// 5302 
// 5303 	if (i < RP_PHYEXT_RXSTATREQ_NUM)
// 5304 	{
// 5305 		return (RP_TRUE);
// 5306 	}
// 5307 	else
// 5308 	{
// 5309 		return (RP_FALSE);
// 5310 	}
// 5311 }
// 5312 
// 5313 /******************************************************************************
// 5314 Function Name:       RpExtChkIdLenGetReq
// 5315 Parameters:          id:id of RpGetPib
// 5316 					 vallen:vallen of RpGetPib
// 5317 Return value:        0:id and vallen are available
// 5318 					 1:id is not available
// 5319 					 2:vallen is not availale
// 5320 Description:         Checks if the id and vallen are available or not.
// 5321 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock100 Using cfiCommon1
          CFI Function _RpExtChkIdLenGetReq
          CFI NoCalls
        CODE
// 5322 int16_t RpExtChkIdLenGetReq( uint8_t id, uint8_t valLen, void *pVal )
// 5323 {
_RpExtChkIdLenGetReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        MOV       B, A               ;; 1 cycle
// 5324 	uint8_t i;
// 5325 
// 5326 	if (pVal == RP_NULL)
        MOV       A, C               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_479  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_479:
        BZ        ??RpLog_Event_480  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 5327 	{
// 5328 		return (RP_INVALID_PARAMETER);
// 5329 	}
// 5330 
// 5331 	for (i = 0; i < RP_PHYEXT_GETID_NUM; ++i)
        MOV       [SP+0x02], #BYTE3(_RpIdLenChkGetReq)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpIdLenChkGetReq)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
// 5332 	{
// 5333 		if (id == RpIdLenChkGetReq[i][0])
??RpExtChkIdLenGetReq_0:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       C, A               ;; 1 cycle
        BZ        ??RpLog_Event_481  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        INC       B                  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       C, A               ;; 1 cycle
        BZ        ??RpLog_Event_481  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        INC       B                  ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+16
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x44           ;; 1 cycle
        BC        ??RpExtChkIdLenGetReq_0  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 5334 		{
// 5335 			if (valLen != RpIdLenChkGetReq[i][1])
// 5336 			{
// 5337 				i = RP_PHYEXT_GETID_NUM + 1;
// 5338 			}
// 5339 			break;
// 5340 		}
// 5341 	}
// 5342 
// 5343 	if	(i < RP_PHYEXT_GETID_NUM)
??RpExtChkIdLenGetReq_1:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x44           ;; 1 cycle
        BNC       ??RpLog_Event_482  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 5344 	{
// 5345 		return (RP_SUCCESS);
        MOVW      AX, #0x7           ;; 1 cycle
        BR        S:??RpLog_Event_483  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 5346 	}
??RpLog_Event_481:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpIdLenChkGetReq+1)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpIdLenChkGetReq)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       C, A               ;; 1 cycle
        BZ        ??RpExtChkIdLenGetReq_1  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 5347 	else if (i == RP_PHYEXT_GETID_NUM)
// 5348 	{
// 5349 		return (RP_UNSUPPORTED_ATTRIBUTE);
// 5350 	}
// 5351 	else
// 5352 	{
// 5353 		return (RP_INVALID_PARAMETER);
??RpLog_Event_480:
        MOVW      AX, #0x5           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_483:
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 7 cycles
// 5354 	}
??RpLog_Event_482:
        BNZ       ??RpLog_Event_480  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, #0xA           ;; 1 cycle
        BR        S:??RpLog_Event_483  ;; 3 cycles
          CFI EndBlock cfiBlock100
        ; ------------------------------------- Block: 4 cycles
        ; ------------------------------------- Total: 109 cycles
// 5355 }
// 5356 
// 5357 /******************************************************************************
// 5358 Function Name:       RpExtChkIdLenSetReq
// 5359 Parameters:          id:id of RpSetPib
// 5360 					 valLen:vallen of RpSetPib
// 5361 					 pVal:val of RpSetPib
// 5362 Return value:        0:id and vallen and val are available
// 5363 					 1:id is not available
// 5364 					 2:vallen or val is not availale
// 5365 Description:         Checks if the id and vallen and val are available or not.
// 5366 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock101 Using cfiCommon1
          CFI Function _RpExtChkIdLenSetReq
          CFI NoCalls
        CODE
// 5367 int16_t RpExtChkIdLenSetReq( uint8_t id, uint8_t valLen, void RP_FAR *pVal )
// 5368 {
_RpExtChkIdLenSetReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        MOV       B, A               ;; 1 cycle
// 5369 	uint8_t i;
// 5370 
// 5371 	if (pVal == RP_NULL)
        MOV       A, C               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpLog_Event_484  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_484:
        BZ        ??RpLog_Event_485  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 5372 	{
// 5373 		return (RP_INVALID_PARAMETER);
// 5374 	}
// 5375 
// 5376 	for (i = 0; i < RP_PHYEXT_SETID_NUM; ++i)
        MOV       [SP+0x02], #BYTE3(_RpIdLenChkSetReq)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpIdLenChkSetReq)  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
// 5377 	{
// 5378 		if (id == RpIdLenChkSetReq[i][0])
??RpExtChkIdLenSetReq_0:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       C, A               ;; 1 cycle
        BZ        ??RpLog_Event_486  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        INC       B                  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       C, A               ;; 1 cycle
        BZ        ??RpLog_Event_486  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        INC       B                  ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       C, A               ;; 1 cycle
        BZ        ??RpLog_Event_486  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        INC       B                  ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+16
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x3F           ;; 1 cycle
        BC        ??RpExtChkIdLenSetReq_0  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 5379 		{
// 5380 			if (valLen != RpIdLenChkSetReq[i][1])
// 5381 			{
// 5382 				i = RP_PHYEXT_SETID_NUM + 1;
// 5383 			}
// 5384 			break;
// 5385 		}
// 5386 	}
// 5387 
// 5388 	if (i < RP_PHYEXT_SETID_NUM)
??RpExtChkIdLenSetReq_1:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x3F           ;; 1 cycle
        BNC       ??RpLog_Event_487  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 5389 	{
// 5390 		return (RP_SUCCESS);
        MOVW      AX, #0x7           ;; 1 cycle
        BR        S:??RpLog_Event_488  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 5391 	}
??RpLog_Event_486:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        ADDW      AX, #LWRD(_RpIdLenChkSetReq+1)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpIdLenChkSetReq)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       C, A               ;; 1 cycle
        BZ        ??RpExtChkIdLenSetReq_1  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
// 5392 	else if (i == RP_PHYEXT_SETID_NUM)
// 5393 	{
// 5394 		return (RP_UNSUPPORTED_ATTRIBUTE);
// 5395 	}
// 5396 	else
// 5397 	{
// 5398 		return (RP_INVALID_PARAMETER);
??RpLog_Event_485:
        MOVW      AX, #0x5           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpLog_Event_488:
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 7 cycles
// 5399 	}
??RpLog_Event_487:
        BNZ       ??RpLog_Event_485  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, #0xA           ;; 1 cycle
        BR        S:??RpLog_Event_488  ;; 3 cycles
          CFI EndBlock cfiBlock101
        ; ------------------------------------- Block: 4 cycles
        ; ------------------------------------- Total: 119 cycles
// 5400 }
// 5401 
// 5402 /******************************************************************************
// 5403 Function Name:       RpMemcpy
// 5404 					 RpMemcpy(void *s1, const void *s2, uint16_t n);
// 5405 Return value:        Last dest address pointer
// 5406 Description:         Memcpy function
// 5407 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock102 Using cfiCommon0
          CFI Function _RpMemcpy
        CODE
// 5408 void *RpMemcpy( void *s1, const void RP_FAR *s2, uint16_t n )
// 5409 {
_RpMemcpy:
        ; * Stack frame (at entry) *
        ; Param size: 2
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 8
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
// 5410 	memcpy( s1, s2, (size_t)n );
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
          CFI FunCall ?MEMCPY_FAR
        CALL      N:?MEMCPY_FAR      ;; 3 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
// 5411 
// 5412 	return( s1 );
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock102
        ; ------------------------------------- Block: 40 cycles
        ; ------------------------------------- Total: 40 cycles
// 5413 }
// 5414 
// 5415 /******************************************************************************
// 5416 Function Name:      RpMemset
// 5417 Parameters:         RpMemset(void *s, int c, uint16_t n);
// 5418 Return value:       Last dest address pointer
// 5419 Description:        Memset function
// 5420 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock103 Using cfiCommon2
          CFI Function _RpMemset
        CODE
// 5421 void *RpMemset( void *s, int16_t c, uint16_t n )
// 5422 {
_RpMemset:
        ; * Stack frame (at entry) *
        ; Param size: 2
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
        MOVW      AX, [SP+0x08]      ;; 1 cycle
// 5423 #if defined(__RX)
// 5424 	memset( s, (int32_t)c, (size_t)n );
// 5425 #else
// 5426 	memset( s, c, (size_t)n );
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
// 5427 #endif
// 5428 
// 5429 	return( s );
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock103
        ; ------------------------------------- Block: 23 cycles
        ; ------------------------------------- Total: 23 cycles
// 5430 }
// 5431 
// 5432 /***************************************************************************************************************
// 5433  * function name  : RpTxLimitTimerForward
// 5434  * description    : Transmission total limit timer update
// 5435  * parameters     : calledByXmsec...Time during a low power consumption mode. [unit:ms]
// 5436  * return value   : none
// 5437  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock104 Using cfiCommon3
          CFI Function _RpTxLimitTimerForward
        CODE
// 5438 void RpTxLimitTimerForward( uint32_t calledByXmsec )
// 5439 {
_RpTxLimitTimerForward:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 14
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+18
// 5440 	uint8_t sample = 0;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 5441 	uint32_t calledByXsec;
// 5442 
// 5443 	#if defined(__RX)
// 5444 	uint32_t bkupPsw;
// 5445 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 5446 	uint8_t  bkupPsw;
// 5447 	#elif defined(__arm)
// 5448 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 5449 	#endif
// 5450 
// 5451 	if (calledByXmsec)
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_489  ;; 4 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 13 cycles
// 5452 	{
// 5453 		/* Disable interrupt */
// 5454 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5455 		RP_PHY_DI(bkupPsw);
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 5456 		#else
// 5457 		RP_PHY_DI();
// 5458 		#endif
// 5459 
// 5460 		// Check input range
// 5461 		if (calledByXmsec > (RpCb.pib.phyRmodeTcumSmpPeriod * RP_MAX_NUM_TX_LMT_BUF * 1000))
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3466  ;; 2 cycles
        MOVW      BC, #0xEE48        ;; 1 cycle
        MULHU                        ;; 2 cycles
        CLRW      BC                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??RpLog_Event_490  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??RpLog_Event_490  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??RpLog_Event_490  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_490:
        BNC       ??RpLog_Event_491  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 5462 		{
// 5463 			RpCb.txLmtTimer.msecCnt += RpCb.pib.phyRmodeTcumSmpPeriod * RP_MAX_NUM_TX_LMT_BUF * 1000;
        CLRW      AX                 ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      HL, #LWRD(_RpRxBuf+3526)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3526)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 5464 			RpCb.txLmtTimer.msecCnt += calledByXmsec % (RpCb.pib.phyRmodeTcumSmpPeriod * 1000);
        MOVW      AX, ES:_RpRxBuf+3466  ;; 2 cycles
        MOVW      BC, #0x3E8         ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BR        S:??RpLog_Event_492  ;; 3 cycles
        ; ------------------------------------- Block: 62 cycles
// 5465 		}
// 5466 		else
// 5467 		{
// 5468 			RpCb.txLmtTimer.msecCnt += calledByXmsec;
??RpLog_Event_491:
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3526)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        ; ------------------------------------- Block: 12 cycles
??RpLog_Event_492:
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3526)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 5469 		}
// 5470 
// 5471 #if defined(__RL78__)
// 5472 		if (RpCb.txLmtTimer.lackTime)
        MOVW      HL, #LWRD(_RpRxBuf+3540)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??RpLog_Event_493  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
// 5473 		{
// 5474 			RpCb.txLmtTimer.msecCnt += RpCb.txLmtTimer.lackTime / 1000;
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3526)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3526)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 5475 			RpCb.txLmtTimer.lackTime = 0;
        MOVW      HL, #LWRD(_RpRxBuf+3540)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 56 cycles
// 5476 		}
// 5477 #endif
// 5478 		calledByXsec = RpCb.txLmtTimer.msecCnt / 1000;
??RpLog_Event_493:
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      HL, #LWRD(_RpRxBuf+3526)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 5479 		RpCb.txLmtTimer.msecCnt = RpCb.txLmtTimer.msecCnt % 1000;
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      HL, #LWRD(_RpRxBuf+3526)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      HL, #LWRD(_RpRxBuf+3526)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 5480 
// 5481 		//  this counter used for arib-std send interval calculation need or not(in rf timer overflowed).
// 5482 		RpCb.tx.regulatoryModeElapsedTimeS += calledByXsec;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3296)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3296)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 5483 		if((RpCb.tx.regulatoryModeElapsedTimeS - RpCb.tx.sentElapsedTimeS) >= RP_PHY_INTERVAL_CLEAR_MARGIN)
        MOVW      HL, #LWRD(_RpRxBuf+3292)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 126 cycles
        CMPW      AX, #0xA           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpTxLimitTimerForward_0:
        SKC                          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 5484 		{
// 5485 			RpCb.tx.needTxWaitRegulatoryMode = RP_FALSE;
        CLRB      ES:_RpRxBuf+3290   ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 5486 		}
// 5487 
// 5488 		RpCb.txLmtTimer.secCnt += calledByXsec;
??RpTxLimitTimerForward_1:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3530)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3530)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 5489 		if (RpCb.txLmtTimer.secCnt >= RpCb.pib.phyRmodeTcumSmpPeriod)
        MOVW      HL, ES:_RpRxBuf+3466  ;; 2 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??RpLog_Event_494  ;; 4 cycles
        ; ------------------------------------- Block: 46 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??RpLog_Event_494  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??RpLog_Event_494  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_494:
        BC        ??RpLog_Event_495  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 5490 		{
// 5491 			sample = (uint8_t)(RpCb.txLmtTimer.secCnt / RpCb.pib.phyRmodeTcumSmpPeriod);
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 5492 			RpCb.txLmtTimer.secCnt = (RpCb.txLmtTimer.secCnt % RpCb.pib.phyRmodeTcumSmpPeriod);
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      HL, #LWRD(_RpRxBuf+3530)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 61 cycles
// 5493 		}
// 5494 
// 5495 		RpProgressSamplingTxTime(sample);
??RpLog_Event_495:
        MOV       A, [SP+0x01]       ;; 1 cycle
          CFI FunCall _RpProgressSamplingTxTime
        CALL      F:_RpProgressSamplingTxTime  ;; 3 cycles
// 5496 
// 5497 		/* Enable interrupt */
// 5498 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5499 		RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 5500 		#else
// 5501 		RP_PHY_EI();
// 5502 		#endif
// 5503 	}
// 5504 }
??RpLog_Event_489:
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock104
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 487 cycles
// 5505 
// 5506 /******************************************************************************
// 5507 Function Name:       RpProgressSamplingTxTime
// 5508 Parameters:          progressSample: Number to advance sampling position.
// 5509 Return value:        none
// 5510 Description:         Advance the sampling position for the measurement of the total Tx time at each sampling cycle and
// 5511                      remove the Tx time past the observation period from the total Tx time.
// 5512 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock105 Using cfiCommon1
          CFI Function _RpProgressSamplingTxTime
          CFI NoCalls
        CODE
// 5513 static void RpProgressSamplingTxTime( uint8_t progressSample )
// 5514 {
_RpProgressSamplingTxTime:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 14
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+18
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RpLog_Event_496  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 5515 	while (progressSample--)
// 5516 	{
// 5517 		RpCb.txLmtTimer.samplingCnt++;
??RpProgressSamplingTxTime_0:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        INC       ES:_RpRxBuf+3534   ;; 3 cycles
// 5518 		if (RpCb.txLmtTimer.samplingCnt >= RP_MAX_NUM_TX_LMT_BUF)
        MOV       A, [SP]            ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3534  ;; 2 cycles
        CMP       A, #0x3D           ;; 1 cycle
        SKC                          ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
// 5519 		{
// 5520 			RpCb.txLmtTimer.samplingCnt = 0;
        CLRB      ES:_RpRxBuf+3534   ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 5521 		}
// 5522 
// 5523 		if (RpCb.pib.phyRmodeTcum > RpTxTime[RpCb.txLmtTimer.samplingCnt])
??RpProgressSamplingTxTime_1:
        MOV       A, ES:_RpRxBuf+3534  ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_RpTxTime)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpTxTime)  ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??RpLog_Event_497  ;; 4 cycles
        ; ------------------------------------- Block: 41 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??RpLog_Event_497  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??RpLog_Event_497  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_497:
        BNC       ??RpLog_Event_498  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 5524 		{
// 5525 			RpCb.pib.phyRmodeTcum -= RpTxTime[RpCb.txLmtTimer.samplingCnt];
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        BR        S:??RpLog_Event_499  ;; 3 cycles
        ; ------------------------------------- Block: 23 cycles
// 5526 		}
// 5527 		else
// 5528 		{
// 5529 			RpCb.pib.phyRmodeTcum = 0;
??RpLog_Event_498:
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 4 cycles
??RpLog_Event_499:
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 5530 		}
// 5531 
// 5532 		RpTxTime[RpCb.txLmtTimer.samplingCnt] = 0;
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 5533 	}
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpProgressSamplingTxTime_0  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
// 5534 }
??RpLog_Event_496:
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock105
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 130 cycles
// 5535 
// 5536 /******************************************************************************
// 5537 Function Name:       RpCalcTotalTxTime
// 5538 Parameters:          none
// 5539 Return value:        none
// 5540 Description:         Calcurate total Tx time function.
// 5541 ******************************************************************************/
// 5542 static void RpCalcTotalTxTime( void )
// 5543 {
// 5544 	uint8_t	cnt = 0;
// 5545 
// 5546 	RpCb.pib.phyRmodeTcum = 0;
// 5547 	do {
// 5548 		RpCb.pib.phyRmodeTcum += RpTxTime[cnt];
// 5549 		cnt++;
// 5550 	} while ( cnt != RP_MAX_NUM_TX_LMT_BUF );
// 5551 }
// 5552 
// 5553 /******************************************************************************
// 5554 Function Name:       RpCheckLongerThanTotalTxTime
// 5555 Parameters:          txLength: frame length
// 5556 					 txOpt: ACK req?
// 5557                      useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
// 5558                                     : 1:FCS length is RpCb.rx.fcsLength.
// 5559 Return value:        RP_TRUE: Reach Tx Limit
// 5560 					 RP_FALSE:Does not reach Tx Limit
// 5561 Description:         Calcurate total Tx time function.
// 5562 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock106 Using cfiCommon3
          CFI Function _RpCheckLongerThanTotalTxTime
        CODE
// 5563 uint8_t RpCheckLongerThanTotalTxTime( uint16_t txLength, uint8_t txOpt, uint8_t useRxFcsLength )
// 5564 {
_RpCheckLongerThanTotalTxTime:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 8
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+12
        MOVW      HL, AX             ;; 1 cycle
// 5565 	uint8_t rtn = RP_FALSE;
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 5566 	uint8_t maxretries = RpCb.pib.macMaxFrameRetries;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3413  ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
// 5567 	uint32_t txThisTime;
// 5568 
// 5569 	txThisTime = RpBytesToTime(txLength, useRxFcsLength);
        MOV       A, B               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall _RpBytesToTime
        CALL      F:_RpBytesToTime   ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 5570 	if (txOpt & RP_TX_ACK)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BF        [HL].4, ??RpLog_Event_500  ;; 5 cycles
        ; ------------------------------------- Block: 26 cycles
// 5571 	{
// 5572 		txThisTime += (uint32_t)((uint32_t)txThisTime * (uint32_t)maxretries);
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
          CFI FunCall ?L_MUL_FAST_L03
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 31 cycles
// 5573 	}
// 5574 	txThisTime /= 1000;
// 5575 	txThisTime += 1;
// 5576 	if ((txThisTime + RpCb.pib.phyRmodeTcum) > RpCb.txLmtTimer.txLimitTime)
??RpLog_Event_500:
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      HL, #LWRD(_RpRxBuf+3536)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        BNC       ??RpLog_Event_501  ;; 4 cycles
        ; ------------------------------------- Block: 66 cycles
// 5577 	{
// 5578 		rtn = RP_TRUE;
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 5579 	}
// 5580 
// 5581 	return (rtn);
??RpLog_Event_501:
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock106
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 133 cycles
// 5582 }
// 5583 
// 5584 /******************************************************************************
// 5585 Function Name:       RpUpdateTxTime
// 5586 Parameters:          trntimes
// 5587                      isAck : 0:TX frame is not Ack, 1: TX frame is Ack
// 5588                      useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
// 5589                                     : 1:FCS length is RpCb.rx.fcsLength.
// 5590 Return value:        none
// 5591 Description:         Add current tx time to total Tx time function.
// 5592 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock107 Using cfiCommon3
          CFI Function _RpUpdateTxTime
        CODE
// 5593 void RpUpdateTxTime( uint8_t trndTimes, uint8_t isAck, uint8_t useRxFcsLength )
// 5594 {
_RpUpdateTxTime:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 16
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+20
        MOV       X, A               ;; 1 cycle
// 5595 	uint16_t txLength = RpCb.tx.len;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      HL, ES:_RpRxBuf+3206  ;; 2 cycles
// 5596 	uint32_t txThisTime;
// 5597 
// 5598 	#if defined(__RX)
// 5599 	uint32_t bkupPsw;
// 5600 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 5601 	uint8_t  bkupPsw;
// 5602 	#elif defined(__arm)
// 5603 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 5604 	#endif
// 5605 
// 5606 	if (isAck == RP_TRUE)
        MOV       A, [SP+0x0E]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
// 5607 	{
// 5608 		txLength = RpCb.tx.ackLenForRegulatoryMode;
        MOVW      HL, ES:_RpRxBuf+3304  ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
// 5609 	}
// 5610 
// 5611 	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
// 5612 		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz) &&
// 5613 		 (RpCb.pib.phyCurrentChannel >= RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)))
??RpUpdateTxTime_0:
        CMP       ES:_RpRxBuf+3437, #0x1  ;; 2 cycles
        BZ        ??RpLog_Event_502  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_503  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_503  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, ES:_RpRxBuf+3354  ;; 2 cycles
        CMP       A, #0x9            ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??RpLog_Event_503  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_502:
        MOV       A, X               ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
// 5614 	{
// 5615 		txThisTime = RpBytesToTime(txLength, useRxFcsLength);
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall _RpBytesToTime
        CALL      F:_RpBytesToTime   ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 5616 		txThisTime /= 1000;
// 5617 		txThisTime += 1;
// 5618 		txThisTime *= (uint32_t)trndTimes;
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        ADDW      AX, #0x1           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
          CFI FunCall ?L_MUL_FAST_L03
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
// 5619 
// 5620 		if (isAck == RP_FALSE)
        MOV       A, [SP+0x0E]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_504  ;; 4 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 58 cycles
// 5621 		{
// 5622 			/* Disable interrupt */
// 5623 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5624 			RP_PHY_DI(bkupPsw);
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 5625 			#else
// 5626 			RP_PHY_DI();
// 5627 			#endif
// 5628 		}
// 5629 
// 5630 		if ((txThisTime + RpCb.pib.phyRmodeTcum) <= RpCb.txLmtTimer.txLimitTime)
??RpLog_Event_504:
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOVW      DE, #LWRD(_RpRxBuf+3536)  ;; 1 cycle
        MOVW      AX, ES:[DE+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??RpLog_Event_505  ;; 4 cycles
        ; ------------------------------------- Block: 41 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??RpLog_Event_505  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??RpLog_Event_505  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpLog_Event_505:
        BC        ??RpLog_Event_506  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 5631 		{
// 5632 			RpTxTime[RpCb.txLmtTimer.samplingCnt] += txThisTime;
        MOV       A, ES:_RpRxBuf+3534  ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_RpTxTime)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpTxTime)  ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+26
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 5633 			RpCb.pib.phyRmodeTcum += txThisTime;
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 57 cycles
// 5634 		}
// 5635 
// 5636 		if (isAck == RP_FALSE)
??RpLog_Event_506:
        MOV       A, [SP+0x0E]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_503  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 5637 		{
// 5638 			/* Enable interrupt */
// 5639 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5640 			RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 5641 			#else
// 5642 			RP_PHY_EI();
// 5643 			#endif
// 5644 		}
// 5645 	}
// 5646 }
??RpLog_Event_503:
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock107
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 235 cycles
// 5647 
// 5648 /******************************************************************************
// 5649 Function Name:       RpBytesToTime
// 5650 Parameters:          psduLength:
// 5651                      useRxFcsLength : 0:FCS length is RpCb.pib.phyFcsLengt.
// 5652                                     : 1:FCS length is RpCb.rx.fcsLength.
// 5653 Return value:        result time
// 5654 Description:         translate Bytes to Time function.
// 5655 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock108 Using cfiCommon3
          CFI Function _RpBytesToTime
        CODE
// 5656 static uint32_t RpBytesToTime( uint16_t psduLength, uint8_t useRxFcsLength )
// 5657 {
_RpBytesToTime:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 4
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
// 5658 	uint16_t sduLen;
// 5659 	uint16_t totalTxLength = RpCalcTotalBytes(psduLength, &sduLen, useRxFcsLength);
        MOV       A, C               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall _RpCalcTotalBytes
        CALL      F:_RpCalcTotalBytes  ;; 3 cycles
// 5660 
// 5661 	return (RpCsmaBytesToTime(totalTxLength));
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      DE, ES:_RpRxBuf+3434  ;; 2 cycles
        MOVW      HL, #0x0           ;; 1 cycle
        MOVW      BC, #0x1F40        ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock108
        ; ------------------------------------- Block: 42 cycles
        ; ------------------------------------- Total: 42 cycles
// 5662 }
// 5663 
// 5664 /******************************************************************************
// 5665 Function Name:       RpSetMacTxLimitMode
// 5666 Parameters:          none
// 5667 Return value:        none
// 5668 Description:         RP_PHY_REGULATORY_MODE function.
// 5669 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock109 Using cfiCommon1
          CFI Function _RpSetMacTxLimitMode
        CODE
// 5670 void RpSetMacTxLimitMode( void )
// 5671 {
_RpSetMacTxLimitMode:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 10
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+14
// 5672 	uint8_t mode;
// 5673 
// 5674 	#if defined(__RX)
// 5675 	uint32_t bkupPsw;
// 5676 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 5677 	uint8_t  bkupPsw;
// 5678 	#elif defined(__arm)
// 5679 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 5680 	#endif
// 5681 
// 5682 	/* Disable interrupt */
// 5683 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5684 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 5685 	#else
// 5686 	RP_PHY_DI();
// 5687 	#endif
// 5688 
// 5689 	mode = RpCb.pib.phyRegulatoryMode;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3437  ;; 2 cycles
// 5690 	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_507  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_508  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 5691 	{
// 5692 		RpSetCcaDurationTimeUpdate(RpCb.pib.phyCurrentChannel);
        MOV       A, ES:_RpRxBuf+3354  ;; 2 cycles
          CFI FunCall _RpSetCcaDurationTimeUpdate
        CALL      F:_RpSetCcaDurationTimeUpdate  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 5693 	}
// 5694 
// 5695 	if (mode == RP_RMODE_DISABLE)
// 5696 	{
// 5697 		RpCb.txLmtTimer.txLimitTime = 0;
// 5698 		RpMemset((uint8_t *)RpTxTime, 0x00, (RP_MAX_NUM_TX_LMT_BUF * 4));
// 5699 		RpLimitTimerControl(RP_FALSE);
// 5700 		RpCb.pib.phyRmodeTcum = 0;
// 5701 	}
// 5702 	else
// 5703 	{
// 5704 		if (mode == RP_RMODE_ENABLE)
// 5705 		{
// 5706 			RpCb.txLmtTimer.txLimitTime = RpCb.pib.phyRmodeTcumLimit;
// 5707 		}
// 5708 		else if (mode == RP_RMODE_ENABLE_ARIB)
// 5709 		{
// 5710 			RpCb.txLmtTimer.txLimitTime = (uint32_t)(RpCb.pib.phyRmodeTcumSmpPeriod) * (RP_MAX_NUM_TX_LMT_BUF - 1) * 100;
??RpLog_Event_508:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:_RpRxBuf+3466  ;; 2 cycles
        MOVW      BC, #0x1770        ;; 1 cycle
        MULHU                        ;; 2 cycles
        ; ------------------------------------- Block: 6 cycles
??RpSetMacTxLimitMode_0:
        MOVW      HL, #LWRD(_RpRxBuf+3536)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 6 cycles
// 5711 		}
// 5712 		RpLimitTimerControl(RP_TRUE);
??RpSetMacTxLimitMode_1:
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpLimitTimerControl
        CALL      F:_RpLimitTimerControl  ;; 3 cycles
// 5713 		RpCalcTotalTxTime();
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOV       ES, #BYTE3(_RpTxTime)  ;; 1 cycle
        MOVW      AX, ES:_RpTxTime   ;; 2 cycles
        MOVW      BC, ES:_RpTxTime+2  ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       [SP+0x04], #BYTE3(_RpTxTime)  ;; 1 cycle
        MOVW      AX, #LWRD(_RpTxTime+4)  ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, #0x14           ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 36 cycles
??RpSetMacTxLimitMode_2:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      DE, AX             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetMacTxLimitMode_2  ;; 4 cycles
        ; ------------------------------------- Block: 122 cycles
// 5714 	}
// 5715 
// 5716 	if (RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE)
??RpSetMacTxLimitMode_3:
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3428, #0x1  ;; 2 cycles
        SKZ                          ;; 4 cycles
        BR        R:??RpLog_Event_509  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 5717 	{
// 5718 		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_510  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_510  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 5719 		{
// 5720 			RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(RpFreqBandTbl[RpCb.freqIdIndex][RP_ANTDVT_TIMEOUT_ARIB]), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl+10)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      AX, #0x680         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 5721 			RpRegWrite((0x0472 << 3), 0x04);
        MOV       C, #0x4            ;; 1 cycle
        MOVW      AX, #0x2390        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        BR        S:??RpLog_Event_511  ;; 3 cycles
          CFI CFA SP+14
        ; ------------------------------------- Block: 22 cycles
// 5722 		}
??RpLog_Event_507:
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpLog_Event_512  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3536)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       X, #0xF4           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_RpTxTime)  ;; 1 cycle
        MOV       A, #BYTE3(_RpTxTime)  ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpLimitTimerControl
        CALL      F:_RpLimitTimerControl  ;; 3 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3472)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        BR        S:??RpSetMacTxLimitMode_3  ;; 3 cycles
        ; ------------------------------------- Block: 29 cycles
??RpLog_Event_512:
        CMP       A, #0x1            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??RpSetMacTxLimitMode_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOVW      HL, #LWRD(_RpRxBuf+3468)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        BR        R:??RpSetMacTxLimitMode_0  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 5723 		else
// 5724 		{
// 5725 			RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(RpFreqBandTbl[RpCb.freqIdIndex][RP_ANTDVT_TIMEOUT]), 2);
??RpLog_Event_510:
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl+8)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      AX, #0x680         ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 5726 			RpSetPreamble4ByteRxMode();
          CFI FunCall _RpSetPreamble4ByteRxMode
        CALL      F:_RpSetPreamble4ByteRxMode  ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
??RpLog_Event_511:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        ; ------------------------------------- Block: 1 cycles
// 5727 		}
// 5728 	}
// 5729 
// 5730 	/* Enable interrupt */
// 5731 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5732 	RP_PHY_EI(bkupPsw);
??RpLog_Event_509:
        MOV       A, [SP+0x01]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 5733 	#else
// 5734 	RP_PHY_EI();
// 5735 	#endif
// 5736 }
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock109
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 317 cycles
// 5737 
// 5738 #ifdef RP_ENABLE_TxTotalTimeLimitControl
// 5739 /******************************************************************************
// 5740 Function Name:       RpTxTotalTimeLimitControl
// 5741 Parameters:          mode
// 5742 Return value:        time
// 5743 Description:         Total Time Special Control function.
// 5744 ******************************************************************************/
// 5745 uint32_t RpTxTotalTimeLimitControl( uint8_t mode )
// 5746 {
// 5747 	uint32_t  rtn = 0;
// 5748 	uint8_t	samplingCnter, iniSamplingCnt;
// 5749 	uint32_t realTime, difTime, willTxTime, difTime2;
// 5750 
// 5751 	#if defined(__RX)
// 5752 	uint32_t bkupPsw;
// 5753 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 5754 	uint8_t  bkupPsw;
// 5755 	#elif defined(__arm)
// 5756 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 5757 	#endif
// 5758 
// 5759 	if (RpCb.pib.phyRegulatoryMode != RP_RMODE_DISABLE)
// 5760 	{
// 5761 		/* Disable interrupt */
// 5762 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5763 		RP_PHY_DI(bkupPsw);
// 5764 		#else
// 5765 		RP_PHY_DI();
// 5766 		#endif
// 5767 
// 5768 		switch (mode)
// 5769 		{
// 5770 			case RP_PHY_TTLMT_INIT: // Initialization (Zero clears transmission total time)
// 5771 				RpMemset((uint8_t *)RpTxTime, 0x00, (RP_MAX_NUM_TX_LMT_BUF * 4));
// 5772 				RpCb.pib.phyRmodeTcum = 0;
// 5773 				break;
// 5774 			case RP_PHY_TTLMT_REF_REST_TXABLE_TIME: // The remaining transmission possible time (msec)
// 5775 				if (RpCb.txLmtTimer.txLimitTime > RpCb.pib.phyRmodeTcum)
// 5776 				{
// 5777 					rtn = (RpCb.txLmtTimer.txLimitTime - RpCb.pib.phyRmodeTcum);
// 5778 				}
// 5779 				break;
// 5780 			case RP_PHY_TTLMT_REF_REST_LIMIT_TX_TIME:	// Time until transmission restriction is released (min)
// 5781 				if (RpCb.txLmtTimer.samplingCnt < RP_MAX_NUM_TX_LMT_BUF)
// 5782 				{
// 5783 					iniSamplingCnt = (uint8_t)(RpCb.txLmtTimer.samplingCnt);
// 5784 				}
// 5785 				else
// 5786 				{
// 5787 					iniSamplingCnt = (uint8_t)(RpCb.txLmtTimer.samplingCnt - RP_MAX_NUM_TX_LMT_BUF);
// 5788 				}
// 5789 				for (samplingCnter = 0; samplingCnter < RP_MAX_NUM_TX_LMT_BUF;)
// 5790 				{
// 5791 					iniSamplingCnt++;
// 5792 					if (iniSamplingCnt >= RP_MAX_NUM_TX_LMT_BUF)
// 5793 					{
// 5794 						iniSamplingCnt = 0;
// 5795 					}
// 5796 
// 5797 					samplingCnter++;
// 5798 
// 5799 					if (RpTxTime[iniSamplingCnt] != 0)
// 5800 					{
// 5801 						break;
// 5802 					}
// 5803 				}
// 5804 				rtn = ((samplingCnter * RpCb.pib.phyRmodeTcumSmpPeriod) / 60) + 1;
// 5805 				break;
// 5806 			case RP_PHY_TTLMT_REF_REST_INTERVAL_TIME:	// Time until the downtime release (usec)
// 5807 				if (RpCb.tx.needTxWaitRegulatoryMode)	// if not need to wait, rtn is 0
// 5808 				{
// 5809 					realTime = RpCb.tx.sentTime;
// 5810 					if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
// 5811 					{
// 5812 						difTime = (uint32_t)(RpCb.pib.phyRmodeToffMin) * (uint32_t)(RpCb.pib.phyDataRate);
// 5813 					}
// 5814 					else
// 5815 					{
// 5816 						difTime = RpCalcTxInterval(RpCb.tx.sentLen, RP_FALSE);
// 5817 					}
// 5818 					realTime += (uint32_t)difTime;
// 5819 					realTime &= (uint32_t)RP_TIME_MASK;
// 5820 					willTxTime = RpGetTime();
// 5821 					difTime2 = (uint32_t)((realTime - willTxTime/* tx time considered interval time */) & RP_TIME_MASK);
// 5822 					if ((difTime2 <= RP_TIME_LIMIT) && (difTime2 <= difTime))
// 5823 					{
// 5824 						rtn = ((difTime2 * (uint32_t)RP_PHY_DATA_RATE_BPS_UNIT) / RpCb.pib.phyDataRate);
// 5825 					}
// 5826 				}
// 5827 				break;
// 5828 			default:
// 5829 				break;
// 5830 		}
// 5831 
// 5832 		/* Enable interrupt */
// 5833 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 5834 		RP_PHY_EI(bkupPsw);
// 5835 		#else
// 5836 		RP_PHY_EI();
// 5837 		#endif
// 5838 	}
// 5839 
// 5840 	return (rtn);
// 5841 }
// 5842 #endif // #ifdef RP_ENABLE_TxTotalTimeLimitControl
// 5843 
// 5844 #ifdef RP_ENABLE_LimitTimerControlSwitch
// 5845 /******************************************************************************
// 5846 Function Name:       RpLimitTimerControlSwitch
// 5847 Parameters:          timerOn: start timer
// 5848 Return value:        lack time
// 5849 Description:         Limitation Timer Control Switch
// 5850 ******************************************************************************/
// 5851 void RpLimitTimerControlSwitch( uint8_t timerOn )
// 5852 {
// 5853 	if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
// 5854 		((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
// 5855 	{
// 5856 		if (timerOn == RP_FALSE)
// 5857 		{
// 5858 			RpCb.txLmtTimer.lackTime += RpLimitTimerControl(timerOn);
// 5859 
// 5860 #if defined(__RL78__)
// 5861 			if (RpCb.txLmtTimer.lackTime > (uint32_t)1000)
// 5862 			{
// 5863 				RpCb.txLmtTimer.lackTime -= (uint32_t)1000;
// 5864 				RpTxLimitTimerForward(1);
// 5865 			}
// 5866 #endif
// 5867 		}
// 5868 		else	// (timerOn == RP_TRUE )
// 5869 		{
// 5870 			RpLimitTimerControl(timerOn);
// 5871 		}
// 5872 	}
// 5873 
// 5874 	return;
// 5875 }
// 5876 #endif // #ifdef RP_ENABLE_LimitTimerControlSwitch
// 5877 
// 5878 /******************************************************************************
// 5879 Function Name:       RpRegCcaBandwidth225k
// 5880 Parameters:          none
// 5881 Return value:        none
// 5882 Description:         Register setting for CCA filter (225k)
// 5883 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock110 Using cfiCommon1
          CFI Function _RpRegCcaBandwidth225k
        CODE
// 5884 void RpRegCcaBandwidth225k( void )
// 5885 {
_RpRegCcaBandwidth225k:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 5886 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
// 5887 
// 5888 	RpRegWrite((0x0432 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0432_OFFSET]);
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x1A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2190        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5889 	RpRegWrite((0x043A << 3), (uint8_t)RpFreqBandTbl[index][RP_0x043A_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x21D0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5890 	RpRegWrite((0x0488 << 3), RP_PHY_REG488H_DEFAULT);
        MOV       C, #0x82           ;; 1 cycle
        MOVW      AX, #0x2440        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5891 	RpRegWrite((0x0436 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0436_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x21B0        ;; 1 cycle
          CFI EndBlock cfiBlock110
        ; ------------------------------------- Block: 52 cycles
        ; ------------------------------------- Total: 52 cycles
        REQUIRE ?Subroutine5
        ; // Fall through to label ?Subroutine5
// 5892 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock111 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+8
          CFI FunCall _RpSetAdrfAndAutoAckVal _RpRegWrite
          CFI FunCall _RpSetAdrfRegConverion _RpRegWrite
          CFI FunCall _RpSetFecVal _RpRegWrite
          CFI FunCall _RpRegCcaBandwidth225k _RpRegWrite
          CFI FunCall _RpRegTxRxDataRateDefault _RpRegWrite
          CFI FunCall _RpRegRxDataRate100kbps _RpRegWrite
        CODE
?Subroutine5:
        CALL      F:_RpRegWrite      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock111
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 5893 
// 5894 /******************************************************************************
// 5895 Function Name:       RpRegCcaBandwidth200k
// 5896 Parameters:          none
// 5897 Return value:        none
// 5898 Description:         Register setting for CCA filter (200k)
// 5899 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock112 Using cfiCommon1
          CFI Function _RpRegCcaBandwidth200k
        CODE
// 5900 static void RpRegCcaBandwidth200k( void )
// 5901 {
_RpRegCcaBandwidth200k:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 5902 	RpRegWrite((0x0432 << 3), 0x06);
        MOV       C, #0x6            ;; 1 cycle
        MOVW      AX, #0x2190        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5903 	RpRegWrite((0x0436 << 3), 0x06);
        MOV       C, #0x6            ;; 1 cycle
        BR        F:?Subroutine0     ;; 3 cycles
          CFI EndBlock cfiBlock112
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
// 5904 	RpRegWrite((0x043A << 3), 0xBE);
// 5905 	RpRegWrite((0x0488 << 3), 0x81);
// 5906 }
// 5907 
// 5908 /******************************************************************************
// 5909 Function Name:       RpRegCcaBandwidth150k
// 5910 Parameters:          none
// 5911 Return value:        none
// 5912 Description:         Register setting for CCA filter (150k)
// 5913 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock113 Using cfiCommon1
          CFI Function _RpRegCcaBandwidth150k
        CODE
// 5914 static void RpRegCcaBandwidth150k( void )
// 5915 {
_RpRegCcaBandwidth150k:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 5916 	RpRegWrite((0x0432 << 3), 0x08);
        MOV       C, #0x8            ;; 1 cycle
        MOVW      AX, #0x2190        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5917 	RpRegWrite((0x0436 << 3), 0x08);
        MOV       C, #0x8            ;; 1 cycle
        BR        F:?Subroutine0     ;; 3 cycles
          CFI EndBlock cfiBlock113
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
// 5918 	RpRegWrite((0x043A << 3), 0xBE);
// 5919 	RpRegWrite((0x0488 << 3), 0x81);
// 5920 }
// 5921 
// 5922 /******************************************************************************
// 5923 Function Name:       RpRegAdcVgaDefault
// 5924 Parameters:          none
// 5925 Return value:        none
// 5926 Description:         Register setting for ADC and VGA (default)
// 5927 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock114 Using cfiCommon1
          CFI Function _RpRegAdcVgaDefault
        CODE
// 5928 void RpRegAdcVgaDefault( void )
// 5929 {
_RpRegAdcVgaDefault:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 8
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
// 5930 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
// 5931 	uint8_t fecRxEna = RpCb.pib.phyFskFecRxEna;
        MOV       A, ES:_RpRxBuf+3366  ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
// 5932 	uint8_t arrayChar[2];
// 5933 
// 5934 	RpRegWrite((0x0471 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0471_OFFSET]);
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x27          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2388        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5935 	arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0486_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0486_OFFSET];
        MOV       A, [SP+0x06]       ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BNZ       ??RpLog_Event_513  ;; 4 cycles
        ; ------------------------------------- Block: 38 cycles
        ADDW      AX, #0x2F          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x32          ;; 1 cycle
        BR        S:??RpLog_Event_514  ;; 3 cycles
        ; ------------------------------------- Block: 10 cycles
??RpLog_Event_513:
        ADDW      AX, #0x30          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x33          ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
??RpLog_Event_514:
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP+0x01], A       ;; 1 cycle
// 5936 	arrayChar[1] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0487_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0487_OFFSET];
// 5937 	RpRegBlockWrite((0x0486 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 2);
        MOVW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x2430        ;; 1 cycle
          CFI FunCall _RpRegBlockWrite
        CALL      F:_RpRegBlockWrite  ;; 3 cycles
// 5938 	RpRegWrite((0x0505 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0505_OFFSET]);
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x40          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2828        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5939 
// 5940 	if (RpCb.pib.phyAntennaDiversityRxEna)
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP0      ES:_RpRxBuf+3428   ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BZ        ??RpLog_Event_515  ;; 4 cycles
        ; ------------------------------------- Block: 37 cycles
// 5941 	{
// 5942 		RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_ANDVON_OFFSET]);
        ADDW      AX, #0x16          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2168        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5943 		
// 5944 		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        CMP       ES:_RpRxBuf+3437, #0x2  ;; 2 cycles
        BNZ       ??RpLog_Event_516  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        CMP       ES:_RpRxBuf+3436, #0x9  ;; 2 cycles
        BNZ       ??RpLog_Event_516  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 5945 		{
// 5946 			RpRegWrite((0x0472 << 3), 0x04);
        MOV       C, #0x4            ;; 1 cycle
        MOVW      AX, #0x2390        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        BR        S:??RpLog_Event_517  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 5947 		}
// 5948 		else
// 5949 		{
// 5950 			RpSetPreamble4ByteRxMode();
// 5951 		}
// 5952 	}
// 5953 	else
// 5954 	{
// 5955 		RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_OFFSET]);
??RpLog_Event_515:
        ADDW      AX, #0x15          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2168        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5956 		RpSetPreamble4ByteRxMode();
          CFI FunCall _RpSetPreamble4ByteRxMode
        ; ------------------------------------- Block: 9 cycles
??RpLog_Event_516:
        CALL      F:_RpSetPreamble4ByteRxMode  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 5957 	}
// 5958 }
??RpLog_Event_517:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock114
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 141 cycles
// 5959 
// 5960 /******************************************************************************
// 5961 Function Name:       RpRegAdcVgaModify
// 5962 Parameters:          none
// 5963 Return value:        none
// 5964 Description:         Register setting for ADC and VGA (modify)
// 5965 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock115 Using cfiCommon1
          CFI Function _RpRegAdcVgaModify
        CODE
// 5966 static void RpRegAdcVgaModify( void )
// 5967 {
_RpRegAdcVgaModify:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 5968 	RpRegWrite((0x042D << 3), 0x14);
        MOV       C, #0x14           ;; 1 cycle
        MOVW      AX, #0x2168        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5969 	RpRegWrite((0x0471 << 3), 0x05);
        MOV       C, #0x5            ;; 1 cycle
        MOVW      AX, #0x2388        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5970 	RpRegWrite((0x0472 << 3), 0x05);
        MOV       C, #0x5            ;; 1 cycle
        MOVW      AX, #0x2390        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5971 	RpRegWrite((0x0486 << 3), 0x66);
        MOV       C, #0x66           ;; 1 cycle
        MOVW      AX, #0x2430        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5972 	RpRegWrite((0x0487 << 3), 0x06);
        MOV       C, #0x6            ;; 1 cycle
        MOVW      AX, #0x2438        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5973 	RpRegWrite((0x0505 << 3), 0x41);
        MOV       C, #0x41           ;; 1 cycle
        MOVW      AX, #0x2828        ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock115
        ; ------------------------------------- Block: 30 cycles
        ; ------------------------------------- Total: 30 cycles
// 5974 }
// 5975 
// 5976 /******************************************************************************
// 5977 Function Name:       RpRegTxRxDataRateDefault
// 5978 Parameters:          none
// 5979 Return value:        none
// 5980 Description:         Register setting for TX and RX (default)
// 5981 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock116 Using cfiCommon1
          CFI Function _RpRegTxRxDataRateDefault
        CODE
// 5982 void RpRegTxRxDataRateDefault( void )
// 5983 {
_RpRegTxRxDataRateDefault:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 5984 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       X, ES:_RpRxBuf+3544  ;; 2 cycles
// 5985 
// 5986 	RpRegWrite(BBSYMBLRATE, (uint8_t)RpFreqBandTbl[index][RP_SYMBOL_OFFSET_L]);
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x560         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5987 	RpRegWrite(BBSYMBLRATE_1, (uint8_t)RpFreqBandTbl[index][RP_SYMBOL_OFFSET_H]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x568         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 5988 	RpRegWrite(BBMODSET, (uint8_t)RpFreqBandTbl[index][RP_MODESET_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x588         ;; 1 cycle
        BR        F:?Subroutine5     ;; 3 cycles
          CFI EndBlock cfiBlock116
        ; ------------------------------------- Block: 49 cycles
        ; ------------------------------------- Total: 49 cycles
// 5989 }
// 5990 
// 5991 /******************************************************************************
// 5992 Function Name:       RpRegTxRxDataRate100kbps
// 5993 Parameters:          none
// 5994 Return value:        none
// 5995 Description:         Register setting for TX and RX (100kbps)
// 5996 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock117 Using cfiCommon1
          CFI Function _RpRegTxRxDataRate100kbps
        CODE
// 5997 static void RpRegTxRxDataRate100kbps( void )
// 5998 {
_RpRegTxRxDataRate100kbps:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 5999 	RpRegWrite(BBSYMBLRATE, 0xF0);
        MOV       C, #0xF0           ;; 1 cycle
        MOVW      AX, #0x560         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6000 	RpRegWrite(BBSYMBLRATE_1, 0x00);
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x568         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6001 	RpRegWrite(BBMODSET, 0x42);
        MOV       C, #0x42           ;; 1 cycle
        MOVW      AX, #0x588         ;; 1 cycle
        BR        F:??Subroutine12_0  ;; 3 cycles
          CFI EndBlock cfiBlock117
        ; ------------------------------------- Block: 15 cycles
        ; ------------------------------------- Total: 15 cycles
// 6002 }
// 6003 
// 6004 /******************************************************************************
// 6005 Function Name:       RpRegRxDataRateDefault
// 6006 Parameters:          none
// 6007 Return value:        none
// 6008 Description:         Register setting for RX (default)
// 6009 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock118 Using cfiCommon1
          CFI Function _RpRegRxDataRateDefault
        CODE
// 6010 void RpRegRxDataRateDefault( void )
// 6011 {
_RpRegRxDataRateDefault:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 10
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+14
// 6012 	uint8_t index = RpCb.freqIdIndex;
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOV       A, ES:_RpRxBuf+3544  ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
// 6013 	const uint8_t *rssiLossPtr = (const uint8_t *)RpFreqBandTblRssiLossPtr[RpConfig.rssiLoss + 1];
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       X, ES:_RpConfig+7  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        SAR       A, 0x7             ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_RpFreqBandTblRssiLossPtr+4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpFreqBandTblRssiLossPtr)  ;; 1 cycle
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 6014 
// 6015 	RpRegWrite((0x0423 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0423_OFFSET]);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_RpFreqBandTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_RpFreqBandTbl)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2118        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6016 	RpRegWrite((0x0430 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0430_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2180        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6017 	RpRegWrite((0x0473 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0473_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x28          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2398        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6018 	RpRegWrite((0x0474 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0474_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x29          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x23A0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6019 	RpRegWrite((0x047C << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047C_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x23E0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6020 	RpRegWrite((0x047D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047D_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x23E8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6021 	RpRegWrite((0x047E << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047E_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x23F0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6022 	RpRegWrite((0x047F << 3), (uint8_t)RpFreqBandTbl[index][RP_0x047F_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2D          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x23F8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6023 	RpRegWrite((0x048D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x048D_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x37          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2468        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6024 	RpRegWrite((0x048F << 3), (uint8_t)RpFreqBandTbl[index][RP_0x048F_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2478        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6025 	RpRegWrite((0x0493 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0493_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x39          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2498        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6026 	RpRegWrite((0x0494 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0494_OFFSET]);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x3A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x24A0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6027 
// 6028 	RpCb.pib.refVthVal = 0x0100 | (uint16_t)(*(rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_LVLVTH_OFFSET)));
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3372, AX  ;; 2 cycles
// 6029 	RpSetLvlVthVal();
          CFI FunCall _RpSetLvlVthVal
        CALL      F:_RpSetLvlVthVal  ;; 3 cycles
// 6030 	RpCb.pib.phyRssiLoss = (uint16_t) * (rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_0x0483_OFFSET));
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3376, AX  ;; 2 cycles
// 6031 	RpSetRssiOffsetVal();
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3F           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2418        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6032 }
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock118
        ; ------------------------------------- Block: 227 cycles
        ; ------------------------------------- Total: 227 cycles
// 6033 
// 6034 /******************************************************************************
// 6035 Function Name:       RpRegRxDataRate100kbps
// 6036 Parameters:          none
// 6037 Return value:        none
// 6038 Description:         Register setting for RX (100kbps)
// 6039 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock119 Using cfiCommon1
          CFI Function _RpRegRxDataRate100kbps
        CODE
// 6040 static void RpRegRxDataRate100kbps( void )
// 6041 {
_RpRegRxDataRate100kbps:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
// 6042 	uint8_t index = RP_PHY_100K_M10_JP;
// 6043 	const uint8_t *rssiLossPtr = (const uint8_t *)RpFreqBandTblRssiLossPtr[RpConfig.rssiLoss + 1];
        MOV       ES, #BYTE3(_RpConfig)  ;; 1 cycle
        MOV       X, ES:_RpConfig+7  ;; 2 cycles
        MOV       A, X               ;; 1 cycle
        SAR       A, 0x7             ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_RpFreqBandTblRssiLossPtr+4)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpFreqBandTblRssiLossPtr)  ;; 1 cycle
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 6044 
// 6045 	RpRegWrite((0x0423 << 3), 0x01);
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x2118        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6046 	RpRegWrite((0x0430 << 3), 0x02);
        MOV       C, #0x2            ;; 1 cycle
        MOVW      AX, #0x2180        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6047 	RpRegWrite((0x0473 << 3), 0xFB);
        MOV       C, #0xFB           ;; 1 cycle
        MOVW      AX, #0x2398        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6048 	RpRegWrite((0x0474 << 3), 0xA6);
        MOV       C, #0xA6           ;; 1 cycle
        MOVW      AX, #0x23A0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6049 	RpRegWrite((0x047C << 3), 0x1C);
        MOV       C, #0x1C           ;; 1 cycle
        MOVW      AX, #0x23E0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6050 	RpRegWrite((0x047D << 3), 0x0F);
        MOV       C, #0xF            ;; 1 cycle
        MOVW      AX, #0x23E8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6051 	RpRegWrite((0x047E << 3), 0x03);
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0x23F0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6052 	RpRegWrite((0x047F << 3), 0x34);
        MOV       C, #0x34           ;; 1 cycle
        MOVW      AX, #0x23F8        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6053 	RpRegWrite((0x048D << 3), 0x57);
        MOV       C, #0x57           ;; 1 cycle
        MOVW      AX, #0x2468        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6054 	RpRegWrite((0x048F << 3), 0x55);
        MOV       C, #0x55           ;; 1 cycle
        MOVW      AX, #0x2478        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6055 	RpRegWrite((0x0493 << 3), 0x20);
        MOV       C, #0x20           ;; 1 cycle
        MOVW      AX, #0x2498        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6056 	RpRegWrite((0x0494 << 3), 0x55);
        MOV       C, #0x55           ;; 1 cycle
        MOVW      AX, #0x24A0        ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
// 6057 
// 6058 	RpCb.pib.refVthVal = 0x0100 | (uint16_t)(*(rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_LVLVTH_OFFSET)));
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3372, AX  ;; 2 cycles
// 6059 	RpSetLvlVthVal();
          CFI FunCall _RpSetLvlVthVal
        CALL      F:_RpSetLvlVthVal  ;; 3 cycles
// 6060 	RpCb.pib.phyRssiLoss = (uint16_t) * (rssiLossPtr + (((uint16_t)index * RP_MAXNUM_RSSI_LOSS) + RP_0x0483_OFFSET));
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x13          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES, #BYTE3(_RpRxBuf)  ;; 1 cycle
        MOVW      ES:_RpRxBuf+3376, AX  ;; 2 cycles
// 6061 	RpSetRssiOffsetVal();
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3F           ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x2418        ;; 1 cycle
        BR        F:?Subroutine5     ;; 3 cycles
          CFI EndBlock cfiBlock119
        ; ------------------------------------- Block: 110 cycles
        ; ------------------------------------- Total: 110 cycles
// 6062 }
// 6063 
// 6064 /******************************************************************************
// 6065 Function Name:		 RpLog_Event
// 6066 Parameters: 		 funtion, option
// 6067 Return value:		 none
// 6068 Description:		 Debug log output function
// 6069 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock120 Using cfiCommon1
          CFI Function _RpLog_Event
          CFI NoCalls
        CODE
// 6070 void RpLog_Event( uint8_t function, uint8_t option )
// 6071 {
_RpLog_Event:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 6072 #ifdef RP_LOG_EVENT
// 6073 	RpLogRam[RpLogRamCnt] = 0x01000000;
// 6074 	RpLogRam[RpLogRamCnt] |= (uint32_t)(((uint16_t)function << 8) | option);
// 6075 	RpLogRamCnt++;
// 6076 
// 6077 	if ( RP_LOG_NUMBER <= RpLogRamCnt )
// 6078 	{
// 6079 		RpLogRamCnt = 0;
// 6080 	}
// 6081 #endif // #ifdef RP_LOG_EVENT
// 6082 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock120
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 6 cycles

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
// 6083 
// 6084 /***********************************************************************************************************************
// 6085  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
// 6086  **********************************************************************************************************************/
// 6087 
// 
//  3 844 bytes in section .bssf
//  8 708 bytes in section .constf
//     24 bytes in section .dataf
// 20 130 bytes in section .textf
// 
//  3 868 bytes of DATA    memory
// 28 838 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
