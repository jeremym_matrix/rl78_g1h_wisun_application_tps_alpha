///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:58
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\r_flash_wrapper_rl78.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3871.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\r_flash_wrapper_rl78.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\r_flash_wrapper_rl78.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_full_locale_support", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _fdl_descriptor_str
        EXTERN ?UL_CMP_L03
        EXTERN _EEL_Close
        EXTERN _EEL_Execute
        EXTERN _EEL_Handler
        EXTERN _EEL_Init
        EXTERN _EEL_Open
        EXTERN _FDL_Close
        EXTERN _FDL_Init
        EXTERN _FDL_Open
        EXTERN _R_FRAMESEC_GetFrameCounter
        EXTERN _R_FRAMESEC_SetFrameCounter
        EXTERN _R_MemCmpZero
        EXTERN _R_memcmp
        EXTERN _R_memset
        EXTERN _ctimer_reset
        EXTERN _ctimer_set
        EXTERN _ctimer_stop
        EXTERN _r_log_append
        EXTERN _r_log_finish
        EXTERN _r_log_start
        EXTERN _vTaskSuspendAll
        EXTERN _xTaskResumeAll

        PUBLIC _R_EEL_Access
        PUBLIC _R_EEL_Read
        PUBLIC _R_EEL_Write
        PUBLIC _R_Flash_Close
        PUBLIC _R_Flash_Format
        PUBLIC _R_Flash_GetFrameCounterOffset
        PUBLIC _R_Flash_Init
        PUBLIC _R_Flash_SetFrameCounterOffset
        PUBLIC _R_Flash_SyncFrameCounters
        PUBLIC _eel_execute
        PUBLIC _handle_StoreFrameCounter_timer
        PUBLIC _p_df_nwkGlobal
        PUBLIC _r_eel_request
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
        
          CFI Common cfiCommon3 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon3
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\r_flash_wrapper_rl78.c
//    1 /******************************************************************************
//    2 * DISCLAIMER
//    3 * This software is supplied by Renesas Electronics Corporation and is only
//    4 * intended for use with Renesas products. No other uses are authorized. This
//    5 * software is owned by Renesas Electronics Corporation and is protected under
//    6 * all applicable laws, including copyright laws.
//    7 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
//    8 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
//    9 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
//   10 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//   11 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   12 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   13 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
//   14 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
//   15 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   16 * Renesas reserves the right, without notice, to make changes to this software
//   17 * and to discontinue the availability of this software. By using this software,
//   18 * you agree to the additional terms and conditions found by accessing the
//   19 * following link:
//   20 * http://www.renesas.com/disclaimer
//   21 *
//   22 * Copyright (C) 2016 Renesas Electronics Corporation. All rights reserved.
//   23 ******************************************************************************/
//   24 
//   25 /*!
//   26    \file      r_flash_wrapper_rl78.c
//   27    \brief     Handling of flash storage for RX651 devices
//   28  */
//   29 
//   30 /******************************************************************************
//   31 * Includes
//   32 ******************************************************************************/
//   33 #include "fdl.h"            /* library API                    */
//   34 #include "fdl_types.h"      /* library type definition        */
//   35 #include "fdl_descriptor.h" /* library descriptor definitions */
//   36 #include "eel.h"            /* library API                    */
//   37 #include "eel_types.h"      /* library type definition        */
//   38 #include "eel_descriptor.h" /* library descriptor definitions */
//   39 #include "intrinsics.h"     /* IAR system                     */
//   40 
//   41 #include "r_mac_ie.h"
//   42 #include "r_impl_utils.h"
//   43 #include "r_nwk_api.h"
//   44 #include "r_nwk_api_base.h"
//   45 #include "r_nwk_global.h"
//   46 #include "r_framesec.h"
//   47 #include "r_flash_wrapper.h"
//   48 #include "r_mem_tools.h"
//   49 #include "sys/ctimer.h"
//   50 
//   51 // R_LOG_PREFIX is processed by RLog code generator
//   52 #define R_LOG_PREFIX FLASH
//   53 #include "r_log_internal.h"
//   54 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
//   55 #include "r_loggen_r_flash_wrapper_rl78.h"

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _r_loggen_319
        CODE
// static __far_func __v2_call void r_loggen_319(uint8_t const, uint8_t const *, uint8_t const, uint32_t const)
_r_loggen_319:
        ; * Stack frame (at entry) *
        ; Param size: 4
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
          CFI FunCall _vTaskSuspendAll
        ; Auto size: 8
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0xE           ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0xE4          ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_0  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        MOVW      BC, #0x8           ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        MOVW      BC, #0x4           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 35 cycles
??R_Flash_GetFrameCounterOffset_0:
        REQUIRE ?Subroutine0
        ; // Fall through to label ?Subroutine0

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI NoFunction
          CFI CFA SP+12
          CFI FunCall _r_loggen_319 _xTaskResumeAll
          CFI FunCall _r_loggen_370 _xTaskResumeAll
        CODE
?Subroutine0:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon0
          CFI Function _r_loggen_370
        CODE
// static __far_func __v2_call void r_loggen_370(uint8_t const, uint8_t const *, uint8_t const, uint32_t const)
_r_loggen_370:
        ; * Stack frame (at entry) *
        ; Param size: 4
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
          CFI FunCall _vTaskSuspendAll
        ; Auto size: 8
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOVW      DE, #0xE           ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOVW      AX, #0xE7          ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_1  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        MOVW      BC, #0x8           ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        MOVW      BC, #0x4           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
        ; ------------------------------------- Block: 35 cycles
??R_Flash_GetFrameCounterOffset_1:
        BR        F:?Subroutine0     ;; 3 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 56 cycles
//   56 #endif
//   57 
//   58 /* compiler settings */
//   59 #pragma language = default
//   60 
//   61 /* defines */
//   62 typedef enum eel_descriptor_ids_e
//   63 {
//   64     HASH_1_ID          =  1,
//   65     HASH_2_ID          =  2,
//   66     HASH_3_ID          =  3,
//   67     HASH_4_ID          =  4,
//   68     HASH_5_ID          =  5,
//   69     HASH_6_ID          =  6,
//   70     HASH_7_ID          =  7,
//   71     FRAME_COUNTER_1_ID =  8,
//   72     FRAME_COUNTER_2_ID =  9,
//   73     FRAME_COUNTER_3_ID = 10,
//   74     FRAME_COUNTER_4_ID = 11,
//   75     FRAME_COUNTER_5_ID = 12,
//   76     FRAME_COUNTER_6_ID = 13,
//   77     FRAME_COUNTER_7_ID = 14
//   78 } eel_descriptor_ids_t;
//   79 
//   80 /* import list */
//   81 extern __far const fdl_descriptor_t fdl_descriptor_str;
//   82 extern __far const eel_u08 eel_descriptor[EEL_VAR_NO + 2];
//   83 
//   84 /* prototype list */
//   85 eel_status_t eel_execute(void);
//   86 
//   87 eel_status_t R_EEL_Access(eel_u08* address, eel_u08 id, eel_command_t command);
//   88 
//   89 /* vaiables */
//   90 static struct ctimer storeFrameCounterTimer;
//   91 

        SECTION `.dataf`:DATA:REORDER:NOROOT(1)
//   92 static uint16_t frame_counter_offset = 200;
_frame_counter_offset:
        DATA16
        DW 200

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   93 
//   94 r_nwk_cb_t* p_df_nwkGlobal;  // Global NWK instance used by data flash functions
_p_df_nwkGlobal:
        DS 4
        DS 20
//   95 

        SECTION `.bss`:DATA:REORDER:NOROOT(1)
//   96 __near eel_request_t r_eel_request;
_r_eel_request:
        DS 6
//   97 
//   98 /* initialization the flash */
//   99 /* ------------------------ */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon1
          CFI Function _R_Flash_Init
        CODE
//  100 void R_Flash_Init(void* vp_nwkGlobal)
//  101 {
_R_Flash_Init:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 10
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+14
//  102     uint8_t eel_dummy_buffer[4];
//  103     fdl_status_t fdl_status;
//  104     eel_status_t eel_status;
//  105 
//  106     /* Global NWK reference used by data flash functions */
//  107     p_df_nwkGlobal = (r_nwk_cb_t*)vp_nwkGlobal;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_p_df_nwkGlobal)  ;; 1 cycle
        MOV       ES, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  108     R_ASSERT_RET_VOID(p_df_nwkGlobal != NULL);
//  109 
//  110     /* initialization of the EEL request */
//  111     r_eel_request.address_pu08 =   &eel_dummy_buffer[0];
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      N:_r_eel_request, AX  ;; 1 cycle
//  112     r_eel_request.identifier_u08 =   0x00;
        CLRB      N:_r_eel_request+2  ;; 1 cycle
//  113     r_eel_request.command_enu =   EEL_CMD_UNDEFINED;
        CLRB      N:_r_eel_request+3  ;; 1 cycle
//  114     r_eel_request.status_enu =   EEL_OK;
        CLRB      N:_r_eel_request+4  ;; 1 cycle
//  115 
//  116     /* initialize and activate data flash "physically" */
//  117     fdl_status =  FDL_Init(&fdl_descriptor_str);
        MOVW      DE, #LWRD(_fdl_descriptor_str)  ;; 1 cycle
        MOV       A, #BYTE3(_fdl_descriptor_str)  ;; 1 cycle
          CFI FunCall _FDL_Init
        CALL      F:_FDL_Init        ;; 3 cycles
//  118     if (fdl_status != FDL_OK)
        CMP0      A                  ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_2  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
//  119     {
//  120 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
//  121         r_loggen_120(fdl_status);
        MOV       [SP], A            ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ONEB      C                  ;; 1 cycle
        MOV       X, #0xDC           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_3  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_4  ;; 3 cycles
          CFI FunCall _FDL_Open
        ; ------------------------------------- Block: 9 cycles
//  122 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
//  123         return;
//  124     }
//  125     FDL_Open();
??R_Flash_GetFrameCounterOffset_2:
        CALL      F:_FDL_Open        ;; 3 cycles
//  126 
//  127     /* initialize and activate EEPROM "logically" */
//  128     eel_status =  EEL_Init();
          CFI FunCall _EEL_Init
        CALL      F:_EEL_Init        ;; 3 cycles
//  129     if (eel_status != EEL_OK)
        CMP0      A                  ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_5  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
//  130     {
//  131 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
//  132         r_loggen_129(eel_status);
        MOV       [SP], A            ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ONEB      C                  ;; 1 cycle
        MOV       X, #0xDD           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_3  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_4  ;; 3 cycles
          CFI FunCall _EEL_Open
        ; ------------------------------------- Block: 9 cycles
//  133 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
//  134         return;
//  135     }
//  136     EEL_Open();
??R_Flash_GetFrameCounterOffset_5:
        CALL      F:_EEL_Open        ;; 3 cycles
//  137 
//  138     /* plausibility check and startup of the EEL data */
//  139     r_eel_request.command_enu =   EEL_CMD_STARTUP;
        ONEB      N:_r_eel_request+3  ;; 1 cycle
//  140     r_eel_request.status_enu =   EEL_OK;
        CLRB      N:_r_eel_request+4  ;; 1 cycle
//  141     eel_status = eel_execute();
          CFI FunCall _eel_execute
        CALL      F:_eel_execute     ;; 3 cycles
//  142 
//  143     if (eel_status == EEL_OK)
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_Flash_GetFrameCounterOffset_6  ;; 4 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 13 cycles
//  144     {
//  145 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  146         r_loggen_141();
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOV       X, #0xDE           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 12 cycles
??R_Flash_GetFrameCounterOffset_4:
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_3:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  147 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  148     }
        BR        S:??R_Flash_GetFrameCounterOffset_7  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  149     else if (eel_status == EEL_ERR_POOL_INCONSISTENT)
??R_Flash_GetFrameCounterOffset_6:
        CMP       A, #0x8A           ;; 1 cycle
        SKNZ                         ;; 1 cycle
          CFI FunCall _R_Flash_Format
        ; ------------------------------------- Block: 2 cycles
//  150     {
//  151         R_Flash_Format();
        CALL      F:_R_Flash_Format  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  152     }
//  153     return;
??R_Flash_GetFrameCounterOffset_7:
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 139 cycles
//  154 }
//  155 
//  156 
//  157 /* Format the flash */
//  158 /* ---------------- */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon2
          CFI Function _R_Flash_Format
        CODE
//  159 r_result_t R_Flash_Format(void)
//  160 {
_R_Flash_Format:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 16
        SUBW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+20
//  161     uint8_t eel_hash[8];
//  162     eel_status_t eel_status = EEL_OK;
//  163     eel_u32 frame_counter = 0;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  164 
//  165     r_result_t res = R_RESULT_FAILED;
        MOV       A, #0x10           ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
//  166 
//  167     r_eel_request.command_enu = EEL_CMD_SHUTDOWN;
        MOV       N:_r_eel_request+3, #0x7  ;; 1 cycle
//  168     r_eel_request.status_enu = EEL_OK;
        CLRB      N:_r_eel_request+4  ;; 1 cycle
//  169     eel_status |= eel_execute();
          CFI FunCall _eel_execute
        CALL      F:_eel_execute     ;; 3 cycles
//  170 
//  171     r_eel_request.command_enu = EEL_CMD_FORMAT;
        MOV       N:_r_eel_request+3, #0x6  ;; 1 cycle
//  172     r_eel_request.status_enu = EEL_OK;
        CLRB      N:_r_eel_request+4  ;; 1 cycle
//  173     eel_status |= eel_execute();
          CFI FunCall _eel_execute
        CALL      F:_eel_execute     ;; 3 cycles
//  174 
//  175     r_eel_request.command_enu = EEL_CMD_STARTUP;
        ONEB      N:_r_eel_request+3  ;; 1 cycle
//  176     r_eel_request.status_enu = EEL_OK;
        CLRB      N:_r_eel_request+4  ;; 1 cycle
//  177     eel_status |= eel_execute();
          CFI FunCall _eel_execute
        CALL      F:_eel_execute     ;; 3 cycles
//  178 
//  179     MEMZERO_A(eel_hash);
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       X, #0x8            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_memset
        CALL      F:_R_memset        ;; 3 cycles
//  180 
//  181     for (uint8_t i = 0; i < R_KEY_NUM; i++)
        CLRB      A                  ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 37 cycles
//  182     {
//  183         // pre-init hashes amd frame counters
//  184         eel_status =  R_EEL_Access(&eel_hash[0], HASH_1_ID + i, EEL_CMD_WRITE);
??R_Flash_Format_0:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      N:_r_eel_request, AX  ;; 1 cycle
        MOV       N:_r_eel_request+3, #0x2  ;; 1 cycle
        CLRB      N:_r_eel_request+4  ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       N:_r_eel_request+2, A  ;; 1 cycle
        MOVW      AX, #LWRD(_r_eel_request)  ;; 1 cycle
          CFI FunCall _EEL_Execute
        CALL      F:_EEL_Execute     ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_8  ;; 3 cycles
          CFI FunCall _EEL_Handler
        ; ------------------------------------- Block: 16 cycles
??R_Flash_Format_1:
        CALL      F:_EEL_Handler     ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_8:
        CMP       N:_r_eel_request+4, #0x1  ;; 1 cycle
        BZ        ??R_Flash_Format_1  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  185         eel_status =  R_EEL_Access((eel_u08*)&frame_counter, FRAME_COUNTER_1_ID + i, EEL_CMD_WRITE);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      N:_r_eel_request, AX  ;; 1 cycle
        MOV       N:_r_eel_request+3, #0x2  ;; 1 cycle
        CLRB      N:_r_eel_request+4  ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, #0x8            ;; 1 cycle
        MOV       N:_r_eel_request+2, A  ;; 1 cycle
        MOVW      AX, #LWRD(_r_eel_request)  ;; 1 cycle
          CFI FunCall _EEL_Execute
        CALL      F:_EEL_Execute     ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_9  ;; 3 cycles
          CFI FunCall _EEL_Handler
        ; ------------------------------------- Block: 15 cycles
??R_Flash_Format_2:
        CALL      F:_EEL_Handler     ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_9:
        CMP       N:_r_eel_request+4, #0x1  ;; 1 cycle
        BZ        ??R_Flash_Format_2  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       X, N:_r_eel_request+4  ;; 1 cycle
//  186     }
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BC        ??R_Flash_Format_0  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  187 
//  188     if (eel_status == EEL_OK)
        CMP0      X                  ;; 1 cycle
        BNZ       ??R_Flash_GetFrameCounterOffset_10  ;; 4 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 5 cycles
//  189     {
//  190 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  191         r_loggen_184();
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOV       X, #0xDF           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 12 cycles
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
??R_Flash_Format_3:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  192 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  193         res = R_RESULT_SUCCESS;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        BR        S:??R_Flash_GetFrameCounterOffset_11  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
//  194     }
//  195     else
//  196     {
//  197 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
//  198         r_loggen_189(eel_status);
??R_Flash_GetFrameCounterOffset_10:
        MOV       A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ONEB      C                  ;; 1 cycle
        MOV       X, #0xE0           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_12  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 9 cycles
??R_Flash_GetFrameCounterOffset_12:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  199 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
//  200     }
        ; ------------------------------------- Block: 3 cycles
//  201 
//  202     return res;
??R_Flash_GetFrameCounterOffset_11:
        MOV       A, [SP+0x06]       ;; 1 cycle
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 157 cycles
//  203 }
//  204 
//  205 /* close the flash  */
//  206 /* ---------------- */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon2
          CFI Function _R_Flash_Close
        CODE
//  207 void R_Flash_Close(void)
//  208 {
_R_Flash_Close:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  209     /* stop timer for frame counter storage */
//  210     ctimer_stop(&storeFrameCounterTimer);
        MOVW      DE, #LWRD(_p_df_nwkGlobal+4)  ;; 1 cycle
        MOV       A, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
          CFI FunCall _ctimer_stop
        CALL      F:_ctimer_stop     ;; 3 cycles
//  211 
//  212     /* deactivate EEL "logically" */
//  213     EEL_Close();
          CFI FunCall _EEL_Close
        CALL      F:_EEL_Close       ;; 3 cycles
//  214 
//  215     /* deactivate data flash "physically" */
//  216     FDL_Close();
          CFI FunCall _FDL_Close
        CALL      F:_FDL_Close       ;; 3 cycles
//  217 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 17 cycles
        ; ------------------------------------- Total: 17 cycles
//  218 
//  219 /* execution of global EEL request      */
//  220 /* ------------------------------------ */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon2
          CFI Function _eel_execute
        CODE
//  221 eel_status_t eel_execute(void)
//  222 {
_eel_execute:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  223     EEL_Execute(&r_eel_request);                                         /* initiate the command      */
        MOVW      AX, #LWRD(_r_eel_request)  ;; 1 cycle
          CFI FunCall _EEL_Execute
        CALL      F:_EEL_Execute     ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_13  ;; 3 cycles
          CFI FunCall _EEL_Handler
        ; ------------------------------------- Block: 7 cycles
//  224 
//  225     while (r_eel_request.status_enu == EEL_BUSY)
//  226     {
//  227         EEL_Handler();                                                   /* proceed execution         */
??eel_execute_0:
        CALL      F:_EEL_Handler     ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  228     }
??R_Flash_GetFrameCounterOffset_13:
        CMP       N:_r_eel_request+4, #0x1  ;; 1 cycle
        BZ        ??eel_execute_0    ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  229     return r_eel_request.status_enu;
        MOV       A, N:_r_eel_request+4  ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 22 cycles
//  230 }
//  231 
//  232 /* access EEL pool                      */
//  233 /* ------------------------------------ */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon3
          CFI Function _R_EEL_Access
        CODE
//  234 eel_status_t R_EEL_Access(eel_u08* address, eel_u08 id, eel_command_t command)
//  235 {
_R_EEL_Access:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
        MOV       B, A               ;; 1 cycle
//  236     eel_status_t eel_status = EEL_OK;
//  237 
//  238     r_eel_request.address_pu08 =   (eel_u08 __near*)address;
        MOVW      AX, DE             ;; 1 cycle
        MOVW      N:_r_eel_request, AX  ;; 1 cycle
//  239     r_eel_request.command_enu =   command;
        MOV       A, C               ;; 1 cycle
        MOV       N:_r_eel_request+3, A  ;; 1 cycle
//  240     r_eel_request.status_enu =   EEL_OK;
        CLRB      N:_r_eel_request+4  ;; 1 cycle
//  241     r_eel_request.identifier_u08 =   id;
        MOV       A, B               ;; 1 cycle
        MOV       N:_r_eel_request+2, A  ;; 1 cycle
//  242 
//  243     eel_status = eel_execute();
        MOVW      AX, #LWRD(_r_eel_request)  ;; 1 cycle
          CFI FunCall _EEL_Execute
        CALL      F:_EEL_Execute     ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_14  ;; 3 cycles
          CFI FunCall _EEL_Handler
        ; ------------------------------------- Block: 18 cycles
??R_EEL_Access_0:
        CALL      F:_EEL_Handler     ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_14:
        CMP       N:_r_eel_request+4, #0x1  ;; 1 cycle
        BZ        ??R_EEL_Access_0   ;; 4 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Total: 26 cycles
        ; ------------------------------------- Block: 5 cycles
//  244 
//  245     return eel_status;
        REQUIRE ?Subroutine1
        ; // Fall through to label ?Subroutine1
//  246 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon3
          CFI NoFunction
          CFI CFA SP+8
          CFI B SameValue
          CFI C SameValue
        CODE
?Subroutine1:
        MOV       A, N:_r_eel_request+4  ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
//  247 
//  248 /* Read data from EEL pool              */
//  249 /* ------------------------------------ */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon1
          CFI Function _R_EEL_Read
        CODE
//  250 eel_status_t R_EEL_Read(eel_u08* address, eel_u08 id)
//  251 {
_R_EEL_Read:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
        MOV       B, A               ;; 1 cycle
//  252     eel_status_t eel_status;
//  253 
//  254     eel_status = R_EEL_Access(address, id, EEL_CMD_READ);
        MOVW      AX, DE             ;; 1 cycle
        MOVW      N:_r_eel_request, AX  ;; 1 cycle
        MOV       N:_r_eel_request+3, #0x3  ;; 1 cycle
        CLRB      N:_r_eel_request+4  ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       N:_r_eel_request+2, A  ;; 1 cycle
        MOVW      AX, #LWRD(_r_eel_request)  ;; 1 cycle
          CFI FunCall _EEL_Execute
        CALL      F:_EEL_Execute     ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_15  ;; 3 cycles
          CFI FunCall _EEL_Handler
        ; ------------------------------------- Block: 17 cycles
??R_EEL_Read_0:
        CALL      F:_EEL_Handler     ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_15:
        CMP       N:_r_eel_request+4, #0x1  ;; 1 cycle
        BZ        ??R_EEL_Read_0     ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  255 
//  256     return eel_status;
        BR        F:?Subroutine1     ;; 3 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 28 cycles
//  257 }
//  258 
//  259 
//  260 /* Write Data to EEL pool               */
//  261 /* ------------------------------------ */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon1
          CFI Function _R_EEL_Write
        CODE
//  262 eel_status_t R_EEL_Write(eel_u08* address, eel_u08 id)
//  263 {
_R_EEL_Write:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
//  264     eel_status_t eel_status;
//  265 
//  266     eel_status = R_EEL_Access(address, id, EEL_CMD_WRITE);
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      N:_r_eel_request, AX  ;; 1 cycle
        MOV       N:_r_eel_request+3, #0x2  ;; 1 cycle
        CLRB      N:_r_eel_request+4  ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       N:_r_eel_request+2, A  ;; 1 cycle
        MOVW      AX, #LWRD(_r_eel_request)  ;; 1 cycle
          CFI FunCall _EEL_Execute
        CALL      F:_EEL_Execute     ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_16  ;; 3 cycles
          CFI FunCall _EEL_Handler
        ; ------------------------------------- Block: 20 cycles
??R_EEL_Write_0:
        CALL      F:_EEL_Handler     ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_16:
        CMP       N:_r_eel_request+4, #0x1  ;; 1 cycle
        BZ        ??R_EEL_Write_0    ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, N:_r_eel_request+4  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
//  267 
//  268     if (eel_status == EEL_ERR_POOL_FULL)
        CMP       A, #0x89           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_Flash_GetFrameCounterOffset_17  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  269     {
//  270         eel_status = R_EEL_Access(address, id, EEL_CMD_REFRESH);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      N:_r_eel_request, AX  ;; 1 cycle
        MOV       N:_r_eel_request+3, #0x4  ;; 1 cycle
        CLRB      N:_r_eel_request+4  ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       N:_r_eel_request+2, A  ;; 1 cycle
        MOVW      AX, #LWRD(_r_eel_request)  ;; 1 cycle
          CFI FunCall _EEL_Execute
        CALL      F:_EEL_Execute     ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_18  ;; 3 cycles
          CFI FunCall _EEL_Handler
        ; ------------------------------------- Block: 13 cycles
??R_EEL_Write_1:
        CALL      F:_EEL_Handler     ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_18:
        CMP       N:_r_eel_request+4, #0x1  ;; 1 cycle
        BZ        ??R_EEL_Write_1    ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, N:_r_eel_request+4  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
//  271 
//  272         if (eel_status == EEL_OK)
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_Flash_GetFrameCounterOffset_19  ;; 4 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 7 cycles
//  273         {
//  274 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  275             r_loggen_264();
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOV       X, #0xE1           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 12 cycles
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
??R_EEL_Write_2:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  276 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  277 
//  278             /* write latest data again */
//  279             eel_status = R_EEL_Access(address, id, EEL_CMD_WRITE);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      N:_r_eel_request, AX  ;; 1 cycle
        MOV       N:_r_eel_request+3, #0x2  ;; 1 cycle
        CLRB      N:_r_eel_request+4  ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       N:_r_eel_request+2, A  ;; 1 cycle
        MOVW      AX, #LWRD(_r_eel_request)  ;; 1 cycle
          CFI FunCall _EEL_Execute
        CALL      F:_EEL_Execute     ;; 3 cycles
        BR        S:??R_Flash_GetFrameCounterOffset_20  ;; 3 cycles
          CFI FunCall _EEL_Handler
        ; ------------------------------------- Block: 16 cycles
??R_EEL_Write_3:
        CALL      F:_EEL_Handler     ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_20:
        CMP       N:_r_eel_request+4, #0x1  ;; 1 cycle
        BZ        ??R_EEL_Write_3    ;; 4 cycles
          CFI FunCall _eel_execute
        ; ------------------------------------- Block: 5 cycles
//  280 
//  281             eel_status = eel_execute();
        CALL      F:_eel_execute     ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        BR        S:??R_Flash_GetFrameCounterOffset_17  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  282         }
//  283         else
//  284         {
//  285 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
//  286             r_loggen_273(eel_status);
??R_Flash_GetFrameCounterOffset_19:
        MOV       [SP+0x01], A       ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ONEB      C                  ;; 1 cycle
        MOV       X, #0xE2           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_21  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 10 cycles
??R_Flash_GetFrameCounterOffset_21:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  287 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
//  288         }
//  289     }
        ; ------------------------------------- Block: 3 cycles
//  290     return eel_status;
??R_Flash_GetFrameCounterOffset_17:
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 146 cycles
//  291 }
//  292 
//  293 
//  294 /* get previous stored frame counters from the flash */
//  295 /* ------------------------------------------------- */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon2
          CFI Function _R_Flash_SyncFrameCounters
        CODE
//  296 void R_Flash_SyncFrameCounters(void)
//  297 {
_R_Flash_SyncFrameCounters:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 30
        SUBW      SP, #0x1E          ;; 1 cycle
          CFI CFA SP+34
//  298     uint8_t eel_hash[8];
//  299     eel_u32 frame_counter;
//  300 
//  301     eel_status_t eel_status;
//  302 
//  303 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  304     r_loggen_289();
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOV       X, #0xE3           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 13 cycles
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
??R_Flash_SyncFrameCounters_0:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  305 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  306 
//  307     for (uint8_t i = 0; i < R_KEY_NUM; i++)
        CLRB      A                  ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x1E], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x1C], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+36
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x1A], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+34
        MOV       [SP+0x01], A       ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 59 cycles
//  308     {
//  309         frame_counter = 0;
??R_Flash_SyncFrameCounters_1:
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  310         eel_status = EEL_OK;
//  311         // Read stored hashes from EEL pool
//  312         eel_status |=  R_EEL_Read(&eel_hash[0], HASH_1_ID + i);
        MOV       A, [SP+0x01]       ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_EEL_Access
        CALL      F:_R_EEL_Access    ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
//  313 
//  314         // Check if received hashes and stored ones are identical
//  315         if ((MEMEQUAL_A(p_df_nwkGlobal->common.gtkHashesFromBr[i], &eel_hash[0])) && (eel_status == EEL_OK))
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       X, #0x8            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       ES, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
        MOVW      HL, ES:_p_df_nwkGlobal  ;; 2 cycles
        MOV       A, ES:_p_df_nwkGlobal+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+40
        POP       DE                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_memcmp
        CALL      F:_R_memcmp        ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+34
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??R_Flash_GetFrameCounterOffset_22  ;; 4 cycles
        ; ------------------------------------- Block: 54 cycles
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??R_Flash_GetFrameCounterOffset_22  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  316         {
//  317             // restore frame counter in case hash is valid
//  318             if (MEMISNOTZERO(&eel_hash[0], 8))
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       X, #0x8            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_MemCmpZero
        CALL      F:_R_MemCmpZero    ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+34
        CMP0      A                  ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_23  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
//  319             {
//  320                 // Read frame counter from EEL pool
//  321                 eel_status |=  R_EEL_Read((eel_u08*)&frame_counter, FRAME_COUNTER_1_ID + i);
        MOV       C, #0x3            ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        ADD       A, #0x8            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_EEL_Access
        CALL      F:_R_EEL_Access    ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
//  322                 // Add offset
//  323                 frame_counter += frame_counter_offset;
        MOV       ES, #BYTE3(_frame_counter_offset)  ;; 1 cycle
        MOVW      DE, ES:_frame_counter_offset  ;; 2 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        BR        S:??R_Flash_GetFrameCounterOffset_23  ;; 3 cycles
        ; ------------------------------------- Block: 31 cycles
//  324             }
//  325         }
//  326         else
//  327         {
//  328             // Store actual hashes to the EEL pool
//  329             eel_status |=  R_EEL_Write(p_df_nwkGlobal->common.gtkHashesFromBr[i], HASH_1_ID + i);
??R_Flash_GetFrameCounterOffset_22:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        MOV       ES, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
        MOVW      HL, ES:_p_df_nwkGlobal  ;; 2 cycles
        MOV       A, ES:_p_df_nwkGlobal+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_EEL_Write
        CALL      F:_R_EEL_Write     ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 30 cycles
//  330         }
//  331         // Store actual frame counter to the EEL pool
//  332         eel_status |=  R_EEL_Write((eel_u08*)&frame_counter, FRAME_COUNTER_1_ID + i);
??R_Flash_GetFrameCounterOffset_23:
        MOV       A, [SP+0x01]       ;; 1 cycle
        ADD       A, #0x8            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_EEL_Write
        CALL      F:_R_EEL_Write     ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
//  333         R_FRAMESEC_SetFrameCounter(frame_counter, i);
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       E, A               ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
          CFI FunCall _R_FRAMESEC_SetFrameCounter
        CALL      F:_R_FRAMESEC_SetFrameCounter  ;; 3 cycles
//  334 
//  335 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  336         r_loggen_319(i, p_df_nwkGlobal->common.gtkHashesFromBr[i], i, frame_counter);
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
        MOVW      DE, ES:_p_df_nwkGlobal  ;; 2 cycles
        MOV       A, ES:_p_df_nwkGlobal+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, C               ;; 1 cycle
          CFI FunCall _r_loggen_319
        CALL      F:_r_loggen_319    ;; 3 cycles
//  337 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  338 
//  339         if (eel_status != EEL_OK)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+34
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_24  ;; 4 cycles
        ; ------------------------------------- Block: 55 cycles
//  340         {
//  341 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
//  342             r_loggen_323(eel_status);
        MOV       [SP+0x01], A       ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ONEB      C                  ;; 1 cycle
        MOV       X, #0xE5           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_25  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 10 cycles
??R_Flash_GetFrameCounterOffset_25:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  343 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
//  344         }
//  345     }
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_24:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??R_Flash_SyncFrameCounters_1  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
//  346     // start frame counter timer */
//  347     clock_time_t ticks = 600;  // default value = 1 min -> 600 *100ms
//  348     ctimer_set(&storeFrameCounterTimer, ticks, handle_StoreFrameCounter_timer, NULL);
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, #0x258         ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      BC, #LWRD(_handle_StoreFrameCounter_timer)  ;; 1 cycle
        MOV       X, #BYTE3(_handle_StoreFrameCounter_timer)  ;; 1 cycle
        MOVW      DE, #LWRD(_p_df_nwkGlobal+4)  ;; 1 cycle
        MOV       A, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
          CFI FunCall _ctimer_set
        CALL      F:_ctimer_set      ;; 3 cycles
//  349 }
        ADDW      SP, #0x26          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 21 cycles
        ; ------------------------------------- Total: 328 cycles
//  350 
//  351 
//  352 /******************************************************************************
//  353    Function Name:       handle_StoreFrameCounter_timer
//  354    Parameters:          void *ptr
//  355    Return value:        none
//  356    Description:         Timer for cyclic frame counter storage to the flash
//  357 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon1
          CFI Function _handle_StoreFrameCounter_timer
        CODE
//  358 void handle_StoreFrameCounter_timer(void* unused)
//  359 {
_handle_StoreFrameCounter_timer:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 38
        SUBW      SP, #0x26          ;; 1 cycle
          CFI CFA SP+42
//  360     uint8_t eel_hash[8];
//  361     eel_u32 eel_frame_counter;
//  362     uint32_t frame_counter;
//  363 
//  364     eel_status_t eel_status;
//  365 
//  366 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  367     r_loggen_346();
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOV       X, #0xE6           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 13 cycles
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
??handle_StoreFrameCounter_timer_0:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  368 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  369 
//  370     for (uint8_t i = 0; i < R_KEY_NUM; i++)
        CLRB      A                  ;; 1 cycle
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+44
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x12], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+44
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x26], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x24], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+44
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x22], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x20], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+42
        MOVW      HL, SP             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+44
        POP       HL                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x1E], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x1C], AX      ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+42
        MOV       [SP+0x01], A       ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 77 cycles
//  371     {
//  372         eel_status = EEL_OK;
//  373         // Read stored hashes from EEL pool
//  374         eel_status |=  R_EEL_Read(&eel_hash[0], HASH_1_ID + i);
??handle_StoreFrameCounter_timer_1:
        MOV       A, [SP+0x01]       ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_EEL_Access
        CALL      F:_R_EEL_Access    ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
//  375         // Check if actual hashes and EEL pool hashes are identical
//  376         if (MEMDIFFER_A(p_df_nwkGlobal->common.gtkHashesFromBr[i], &eel_hash[0]))
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOV       X, #0x8            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOV       ES, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
        MOVW      HL, ES:_p_df_nwkGlobal  ;; 2 cycles
        MOV       A, ES:_p_df_nwkGlobal+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+48
        POP       DE                 ;; 1 cycle
          CFI CFA SP+46
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_memcmp
        CALL      F:_R_memcmp        ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+42
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_26  ;; 4 cycles
        ; ------------------------------------- Block: 51 cycles
//  377         {
//  378             // update stored hashes
//  379             eel_status |=  R_EEL_Write(p_df_nwkGlobal->common.gtkHashesFromBr[i], HASH_1_ID + i);
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        MOV       ES, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
        MOVW      HL, ES:_p_df_nwkGlobal  ;; 2 cycles
        MOV       A, ES:_p_df_nwkGlobal+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+44
        POP       DE                 ;; 1 cycle
          CFI CFA SP+42
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_EEL_Write
        CALL      F:_R_EEL_Write     ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 30 cycles
//  380         }
//  381 
//  382         // Read stored frame counter from EEL pool
//  383         eel_status |=  R_EEL_Read((eel_u08*)&eel_frame_counter, FRAME_COUNTER_1_ID + i);
??R_Flash_GetFrameCounterOffset_26:
        MOV       A, [SP+0x01]       ;; 1 cycle
        ADD       A, #0x8            ;; 1 cycle
        MOV       [SP+0x03], A       ;; 1 cycle
        MOV       C, #0x3            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_EEL_Access
        CALL      F:_R_EEL_Access    ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
//  384         frame_counter = R_FRAMESEC_GetFrameCounter(i);
        MOV       A, [SP+0x01]       ;; 1 cycle
          CFI FunCall _R_FRAMESEC_GetFrameCounter
        CALL      F:_R_FRAMESEC_GetFrameCounter  ;; 3 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  385         // Check if actual frame counters and EEL pool frame counters are identical
//  386         if (frame_counter != eel_frame_counter)
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+42
        BZ        ??R_Flash_GetFrameCounterOffset_27  ;; 4 cycles
        ; ------------------------------------- Block: 37 cycles
//  387         {
//  388             // update stored frame counter
//  389             eel_status |=  R_EEL_Write((eel_u08*)&frame_counter, FRAME_COUNTER_1_ID + i);
        MOV       A, [SP+0x03]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _R_EEL_Write
        CALL      F:_R_EEL_Write     ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
//  390         }
//  391 
//  392 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
//  393         r_loggen_370(i, p_df_nwkGlobal->common.gtkHashesFromBr[i], i, frame_counter);
??R_Flash_GetFrameCounterOffset_27:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+44
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+46
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
        MOVW      DE, ES:_p_df_nwkGlobal  ;; 2 cycles
        MOV       A, ES:_p_df_nwkGlobal+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4A          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, C               ;; 1 cycle
          CFI FunCall _r_loggen_370
        CALL      F:_r_loggen_370    ;; 3 cycles
//  394 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
//  395 
//  396         if (eel_status != EEL_OK)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+42
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_28  ;; 4 cycles
        ; ------------------------------------- Block: 32 cycles
//  397         {
//  398 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
//  399             r_loggen_374(eel_status);
        MOV       [SP+0x01], A       ;; 1 cycle
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ONEB      C                  ;; 1 cycle
        MOV       X, #0xE8           ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??R_Flash_GetFrameCounterOffset_29  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_log_append
        CALL      F:_r_log_append    ;; 3 cycles
          CFI FunCall _r_log_finish
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 10 cycles
??R_Flash_GetFrameCounterOffset_29:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  400 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
//  401         }
//  402 
//  403     }
        ; ------------------------------------- Block: 3 cycles
??R_Flash_GetFrameCounterOffset_28:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??handle_StoreFrameCounter_timer_1  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
//  404 
//  405     ctimer_reset(&storeFrameCounterTimer);
        MOVW      DE, #LWRD(_p_df_nwkGlobal+4)  ;; 1 cycle
        MOV       A, #BYTE3(_p_df_nwkGlobal)  ;; 1 cycle
          CFI FunCall _ctimer_reset
        CALL      F:_ctimer_reset    ;; 3 cycles
//  406 }
        ADDW      SP, #0x26          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 309 cycles
//  407 
//  408 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon2
          CFI Function _R_Flash_SetFrameCounterOffset
          CFI NoCalls
        CODE
//  409 void R_Flash_SetFrameCounterOffset(uint16_t offset)
//  410 {
_R_Flash_SetFrameCounterOffset:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  411     frame_counter_offset = offset;
        MOV       ES, #BYTE3(_frame_counter_offset)  ;; 1 cycle
        MOVW      ES:_frame_counter_offset, AX  ;; 2 cycles
//  412 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  413 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon2
          CFI Function _R_Flash_GetFrameCounterOffset
          CFI NoCalls
        CODE
//  414 uint16_t R_Flash_GetFrameCounterOffset()
//  415 {
_R_Flash_GetFrameCounterOffset:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  416     return frame_counter_offset;
        MOV       ES, #BYTE3(_frame_counter_offset)  ;; 1 cycle
        MOVW      AX, ES:_frame_counter_offset  ;; 2 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  417 }

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
// 
//     6 bytes in section .bss
//    24 bytes in section .bssf
//     2 bytes in section .dataf
// 1 863 bytes in section .textf
// 
//    32 bytes of DATA    memory
// 1 863 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
