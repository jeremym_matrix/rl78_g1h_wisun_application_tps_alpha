///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:58
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\r_table_config.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3D49.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\r_table_config.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\r_table_config.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1

        PUBLIC _g_nd_aro_cache
        PUBLIC _g_nd_aro_cache_length
        PUBLIC _g_nd_rpl_cache
        PUBLIC _g_nd_rpl_cache_length
        PUBLIC _g_neighbors
        PUBLIC _r_mac_neighbor_table_size_glb
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\r_table_config.c
//    1 /*******************************************************************************
//    2 * DISCLAIMER
//    3 * This software is supplied by Renesas Electronics Corporation and is only
//    4 * intended for use with Renesas products. No other uses are authorized. This
//    5 * software is owned by Renesas Electronics Corporation and is protected under
//    6 * all applicable laws, including copyright laws.
//    7 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
//    8 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
//    9 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
//   10 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//   11 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   12 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   13 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
//   14 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
//   15 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   16 * Renesas reserves the right, without notice, to make changes to this software
//   17 * and to discontinue the availability of this software. By using this software,
//   18 * you agree to the additional terms and conditions found by accessing the
//   19 * following link:
//   20 * http://www.renesas.com/disclaimer*
//   21 * Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
//   22 *******************************************************************************/
//   23 
//   24 /*******************************************************************************
//   25  * File Name   : r_table_config.c
//   26  * Version     : 1.0
//   27  * Description : This module contains configuration defines for FAN functions
//   28  ******************************************************************************/
//   29 #ifndef _R_TABLE_CONFIG_C
//   30 #define _R_TABLE_CONFIG_C
//   31 
//   32 #include <stdint.h>
//   33 #include "r_table_config.h"
//   34 #include "r_table_size.h"
//   35 #ifndef R_HYBRID_PLC_RF
//   36 #include "lib/memb.h"
//   37 #include "net/uip-ds6-source-route.h"
//   38 #include "r_nd.h"
//   39 #include "r_nd_cache.h"
//   40 #endif /* R_HYBRID_PLC_RF */
//   41 #include "mac_neighbors.h"
//   42 #include "r_impl_utils.h"
//   43 
//   44 /*** MAC Neighbor Cache ********************/
//   45 #if R_MAC_NBR_CACHE_SIZE_OFFSET < 1
//   46 #error "The number of additional entries in the MAC neighbor cache must be greater than 1."
//   47 #endif
//   48 
//   49 /* global: Substance of MAC Neighbor table */
//   50 #ifdef R_HYBRID_PLC_RF
//   51 r_mac_neighbor_t g_neighbors[R_MAC_NEIGHBOR_TABLE_SIZE];
//   52 #else /* R_HYBRID_PLC_RF */

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   53 r_mac_neighbor_t g_neighbors[R_MAX_CHILD_NODES + R_MAX_PARENT_CANDIDATES + R_MAC_NBR_CACHE_SIZE_OFFSET];
_g_neighbors:
        DS 320
//   54 #endif /* R_HYBRID_PLC_RF */
//   55 
//   56 /* global: Number of entry of mac neighbor table */

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
//   57 const uint16_t r_mac_neighbor_table_size_glb = ARRAY_SIZE(g_neighbors);
_r_mac_neighbor_table_size_glb:
        DATA16
        DW 8
//   58 
//   59 /*******************************************/
//   60 #ifndef R_HYBRID_PLC_RF
//   61 #if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
//   62 #if defined(__CCRX__)
//   63 #pragma section B expRAM
//   64 #endif
//   65 #endif
//   66 
//   67 /*** NWK Neighbor Caches ************************/
//   68 #if R_MAX_PARENT_CANDIDATES < 2
//   69 #error "At least two parent candidates must be supported to select and hold a preferred and an alternate parent."
//   70 #endif
//   71 
//   72 /** The RPL neighbor cache that contains the candidate parents (size must be at least 2 to hold both parents) */

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   73 r_nd_neighbor_cache_entry_t g_nd_rpl_cache[R_MAX_PARENT_CANDIDATES];
_g_nd_rpl_cache:
        DS 112
//   74 
//   75 #if R_MAX_CHILD_NODES < 1
//   76 #error "At least one child node must be supported."
//   77 #endif
//   78 
//   79 /** The size of the ARO neighbor cache that contains registered IP addresses (via ARO from child nodes) */

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   80 r_nd_addr_reg_entry_t g_nd_aro_cache[R_MAX_CHILD_NODES];
_g_nd_aro_cache:
        DS 36
//   81 
//   82 #if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
//   83 #if defined(__CCRX__)
//   84 #pragma section
//   85 #endif
//   86 #endif
//   87 
//   88 /** The size of the RPL neighbor cache that contains the candidate parents */

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
//   89 const uint16_t g_nd_rpl_cache_length = ARRAY_SIZE(g_nd_rpl_cache);
_g_nd_rpl_cache_length:
        DATA16
        DW 2
//   90 
//   91 /** The size of the ARO neighbor cache that contains registered IP addresses (via ARO from child nodes) */

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
//   92 const uint16_t g_nd_aro_cache_length = ARRAY_SIZE(g_nd_aro_cache);
_g_nd_aro_cache_length:
        DATA16
        DW 1

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//   93 
//   94 /*******************************************/
//   95 
//   96 #if R_BR_AUTHENTICATOR_ENABLED
//   97 #if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
//   98 #if defined(__CCRX__)
//   99 #pragma section B expRAM
//  100 #endif
//  101 #endif
//  102 
//  103 /*** Authentication supplicant table *******/
//  104 /* global: Substance of auth supplicant table */
//  105 r_auth_br_supplicant_t g_supplicants[R_AUTH_BR_SUPPLICANTS_SIZE];
//  106 
//  107 #if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
//  108 #if defined(__CCRX__)
//  109 #pragma section
//  110 #endif
//  111 #endif
//  112 
//  113 /* global: Number of entries of auth supplicant table */
//  114 const uint16_t r_auth_br_supplicants_size_glb = ARRAY_SIZE(g_supplicants);
//  115 
//  116 /*******************************************/
//  117 #endif /* R_BR_AUTHENTICATOR_ENABLED */
//  118 
//  119 #if R_BORDER_ROUTER_ENABLED
//  120 
//  121 /*** Routing table *************************/
//  122 /* Each route is represented by a uip_ds6_source_route_t structure and
//  123    memory for each route is allocated from the sourceroutememb memory
//  124    block. These routes are maintained on the routelist. */
//  125 
//  126 MEMB(sourceroutememb, uip_ds6_source_route_t, R_BR_PAN_SIZE);
//  127 /* --------------------------------------------------------------------------------------
//  128   --> This is equivalent to the following description.
//  129         static char    sourceroutememb_memb_count[R_BR_PAN_SIZE];
//  130         static uip_ds6_source_route_t  sourceroutememb_memb_mem[R_BR_PAN_SIZE];
//  131         static struct memb sourceroutememb = { sizeof(uip_ds6_source_route_t),
//  132                                                R_BR_PAN_SIZE,
//  133                                                sourceroutememb_memb_count,
//  134                                                (void *)sourceroutememb_memb_mem }
//  135 
//  136     struct memb {           <-- = static struct memb sourceroutememb
//  137       unsigned short size;  <-- = sizeof(uip_ds6_source_route_t)
//  138       unsigned short num;   <-- = R_BR_PAN_SIZE
//  139       char *count;          <-- = sourceroutememb_memb_count
//  140       void *mem;            <-- = (void *)sourceroutememb_memb_mem
//  141     };
//  142  -------------------------------------------------------------------------------------- */
//  143 
//  144 /**
//  145 / extern the pointer of routing table struct sourceroutememb
//  146 */
//  147 struct memb* g_sourceroutememb = &sourceroutememb;
//  148 
//  149 /*******************************************/
//  150 #endif /* R_BORDER_ROUTER_ENABLED */
//  151 #endif /* R_HYBRID_PLC_RF */
//  152 #endif /* _R_TABLE_CONFIG_C */
// 
// 468 bytes in section .bssf
//   6 bytes in section .constf
// 
// 468 bytes of DATA    memory
//   6 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
