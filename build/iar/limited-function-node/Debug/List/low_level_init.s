///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:55
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\low_level_init.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW2D64.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\low_level_init.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\low_level_init.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHT_IAR_NOINIT 0xabdc5467
        #define SHF_WRITE 0x1
        #define SHF_EXECINSTR 0x4

        PUBLIC __A_CKC
        PUBLIC __A_CMC
        PUBLIC __A_CSC
        PUBLIC __A_HOCODIV
        PUBLIC __A_OSMC
        PUBLIC ___low_level_init
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\low_level_init.c
//    1 /*-------------------------------------------------------------------------
//    2  *      This module contains the function `__low_level_init', a function
//    3  *      that is called before the `main' function of the program.  Normally
//    4  *      low-level initializations - such as setting the prefered interrupt
//    5  *      level or setting the watchdog - can be performed here.
//    6  *
//    7  *      Note that this function is called before the data segments are
//    8  *      initialized, this means that this function can't rely on the
//    9  *      values of global or static variables.
//   10  *
//   11  *      When this function returns zero, the startup code will inhibit the
//   12  *      initialization of the data segments.  The result is faster startup,
//   13  *      the drawback is that neither global nor static data will be
//   14  *      initialized.
//   15  *
//   16  *-------------------------------------------------------------------------
//   17  *      Copyright 2011 IAR Systems AB.
//   18  *      $Revision: 205 $
//   19  *-------------------------------------------------------------------------*/
//   20 
//   21 #ifdef __cplusplus
//   22 extern "C" {
//   23 #endif
//   24 
//   25 #include <ior5f11fll.h>

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffa0H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_CMC
// union <unnamed>#107 volatile __sfr __no_bit_access _A_CMC
__A_CMC:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffa1H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_CSC
// union <unnamed>#108 volatile __sfr _A_CSC
__A_CSC:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffa4H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_CKC
// union <unnamed>#112 volatile __sfr _A_CKC
__A_CKC:
        DS 1
//   26 #include <ior5f11fll_ext.h>

        ASEGN `.bss.noinit`:DATA:NOROOT,0f00a8H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_HOCODIV
// union <unnamed>#253 volatile __near __no_bit_access _A_HOCODIV
__A_HOCODIV:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f00f3H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_OSMC
// union <unnamed>#260 volatile __near __no_bit_access _A_OSMC
__A_OSMC:
        DS 1
//   27 

        SECTION `.textf`:FARCODE:ROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function ___low_level_init
          CFI NoCalls
        CODE
//   28 __root int __low_level_init ( void )
//   29 {
___low_level_init:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//   30   /*==================================*/
//   31   /*  Initialize hardware.            */
//   32   /*==================================*/
//   33 
//   34     // -------------------------------------------------
//   35     //   System clock 
//   36     // -------------------------------------------------
//   37     CMC		= 0x14;			// Sub Clock(XT1) and the lowest margin
        MOV       0xFFFA0, #0x14     ;; 1 cycle
//   38     CSC		= 0x80;			// X1 Stop
        MOV       0xFFFA1, #0x80     ;; 1 cycle
//   39     OSMC	= 0x00;			// 
        MOV       0xF3, #0x0         ;; 1 cycle
//   40 
//   41 #if RP_CPU_CLK == 32
//   42     HOCODIV	= 0x00;			// 32MHz(C2H is 0xE8 : FRQSEL3=1)
        MOV       0xA8, #0x0         ;; 1 cycle
//   43 #elif RP_CPU_CLK == 16
//   44     HOCODIV	= 0x01;			// 16MHz(C2H is 0xE9 : FRQSEL3=1)
//   45 #elif RP_CPU_CLK == 8
//   46     HOCODIV	= 0x02;			//  8MHz(C2H is 0xAA : FRQSEL3=1)
//   47 #elif RP_CPU_CLK == 4
//   48     HOCODIV	= 0x03;			//  4MHz(C2H is 0x2B : FRQSEL3=1)
//   49 #endif
//   50 
//   51     CKC		= 0x00;			// 
        MOV       0xFFFA4, #0x0      ;; 1 cycle
//   52     MCM0	= 0;			// OCO clock
        CLR1      0xFFFA4.4          ;; 2 cycles
//   53   /*==================================*/
//   54   /* Choose if segment initialization */
//   55   /* should be done or not.           */
//   56   /* Return: 0 to skip segment init   */
//   57   /*         1 to do segment init     */
//   58   /*==================================*/
//   59   return ( 1 );
        ONEW      AX                 ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 14 cycles
        ; ------------------------------------- Total: 14 cycles
        REQUIRE __A_CMC
        REQUIRE __A_CSC
        REQUIRE __A_OSMC
        REQUIRE __A_HOCODIV
        REQUIRE __A_CKC
//   60 }

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//   61 
//   62 #ifdef __cplusplus
//   63 }
//   64 #endif
// 
//  5 bytes in section .bss.noinit  (abs)
// 22 bytes in section .textf
// 
//  0 bytes of DATA    memory (+ 5 bytes shared)
// 22 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
