///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:55
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\hardware.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW2D62.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\hardware.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\hardware.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_file_descriptor", "0"
        RTMODEL "__dlib_full_locale_support", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHT_IAR_NOINIT 0xabdc5467
        #define SHF_WRITE 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _RmFreqHoppTimer_Handler
        EXTERN ___interrupt_tab_0x26
        EXTERN ___interrupt_tab_0x28
        EXTERN ___interrupt_tab_0x36
        EXTERN ___interrupt_tab_0x38

        PUBLIC _RdrvFREQHOPPTIMER_Initialize
        PUBLIC _RdrvFREQHOPPTIMER_Stop
        PUBLIC _RdrvITHdlStack
        PUBLIC _RdrvIT_FreqHoppInt
        PUBLIC _RdrvLED0_GetCondition
        PUBLIC _RdrvLED0_OFF
        PUBLIC _RdrvLED0_ON
        PUBLIC _RdrvLED1_GetCondition
        PUBLIC _RdrvLED1_OFF
        PUBLIC _RdrvLED1_ON
        PUBLIC _RdrvLED2_GetCondition
        PUBLIC _RdrvLED2_OFF
        PUBLIC _RdrvLED2_ON
        PUBLIC _RdrvLED_ALL_OFF
        PUBLIC _RdrvLED_ALL_ON
        PUBLIC _RdrvPeripheralInitialize
        PUBLIC _RdrvRTC_TimeCorrectionInt
        PUBLIC _RdrvSwitch0_GetCondition
        PUBLIC _RdrvUARTHdlStack
        PUBLIC _RdrvUART_GetChar
        PUBLIC _RdrvUART_GetLen
        PUBLIC _RdrvUART_PutChar
        PUBLIC _RdrvUART_RXBuf
        PUBLIC _RdrvUART_RXBuf_RdPtr
        PUBLIC _RdrvUART_RXBuf_WrPtr
        PUBLIC _RdrvUART_RxErrInt
        PUBLIC _RdrvUART_RxInt
        PUBLIC _RpIntStackBack
        PUBLIC _RpIntStackChange
        PUBLIC __A_IF1
        PUBLIC __A_ITMC
        PUBLIC __A_MIN
        PUBLIC __A_MK1
        PUBLIC __A_NFEN0
        PUBLIC __A_OSMC
        PUBLIC __A_P0
        PUBLIC __A_P14
        PUBLIC __A_P6
        PUBLIC __A_PER0
        PUBLIC __A_PM0
        PUBLIC __A_PM14
        PUBLIC __A_PM6
        PUBLIC __A_PM7
        PUBLIC __A_PMC0
        PUBLIC __A_PR01
        PUBLIC __A_PR11
        PUBLIC __A_PU14
        PUBLIC __A_PU7
        PUBLIC __A_RTCC0
        PUBLIC __A_SCR02
        PUBLIC __A_SCR03
        PUBLIC __A_SDR02
        PUBLIC __A_SDR03
        PUBLIC __A_SEC
        PUBLIC __A_SIR03
        PUBLIC __A_SMR02
        PUBLIC __A_SMR03
        PUBLIC __A_SO0
        PUBLIC __A_SOE0
        PUBLIC __A_SOL0
        PUBLIC __A_SPS0
        PUBLIC __A_SS0
        PUBLIC __A_SSR02
        PUBLIC __A_SSR03
        PUBLIC __A_ST0
        PUBLIC ___interrupt_0x26
        PUBLIC ___interrupt_0x28
        PUBLIC ___interrupt_0x36
        PUBLIC ___interrupt_0x38
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET CODE
          CFI CFA SP+4
          CFI A SameValue
          CFI X SameValue
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H SameValue
          CFI L SameValue
          CFI CS_REG SameValue
          CFI ES_REG SameValue
          CFI ?RET Frame(CFA, -4)
          CFI MACRH SameValue
          CFI MACRL SameValue
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\hardware.c
//    1 /****************************************************************************** 
//    2 * DISCLAIMER 
//    3 
//    4 * This software is supplied by Renesas Electronics Corporation and is only 
//    5 * intended for use with Renesas products. No other uses are authorized. 
//    6 
//    7 * This software is owned by Renesas Electronics Corporation and is protected under 
//    8 * all applicable laws, including copyright laws. 
//    9 
//   10 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
//   11 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
//   12 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
//   13 * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
//   14 * DISCLAIMED. 
//   15 
//   16 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
//   17 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
//   18 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
//   19 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
//   20 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
//   21 
//   22 * Renesas reserves the right, without notice, to make changes to this 
//   23 * software and to discontinue the availability of this software. 
//   24 * By using this software, you agree to the additional terms and 
//   25 * conditions found by accessing the following link: 
//   26 * http://www.renesas.com/disclaimer 
//   27 ******************************************************************************/ 
//   28 
//   29 /* Copyright (C) 2014. Renesas Electronics Corporation, All Rights Reserved. */
//   30 
//   31 /****************************************************************************** 
//   32 * File Name : hardwarer.c
//   33 * Version : 1.00 
//   34 * Device(s) : RL78/G1H
//   35 * Tool-Chain :
//   36 * OS :
//   37 * H/W Platform : 
//   38 * Description : Hardware dependency routines for RL78/G1H
//   39 * Operation : 
//   40 * Limitations : None 
//   41 ****************************************************************************** 
//   42 * History : DD.MM.YYYY Version Description 
//   43 * 
//   44 ******************************************************************************/ 
//   45 #include <ior5f11fll.h>

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff00H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P0
// union <unnamed>#3 volatile __saddr _A_P0
__A_P0:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff06H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P6
// union <unnamed>#9 volatile __saddr _A_P6
__A_P6:
        DS 1

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff0eH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_P14
// union <unnamed>#16 volatile __saddr _A_P14
__A_P14:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff20H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM0
// union <unnamed>#32 volatile __sfr _A_PM0
__A_PM0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff26H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM6
// union <unnamed>#38 volatile __sfr _A_PM6
__A_PM6:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff27H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM7
// union <unnamed>#39 volatile __sfr _A_PM7
__A_PM7:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff2eH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PM14
// union <unnamed>#44 volatile __sfr _A_PM14
__A_PM14:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff44H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SDR02
// union <unnamed>#54 volatile __sfr __no_bit_access _A_SDR02
__A_SDR02:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff46H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SDR03
// union <unnamed>#57 volatile __sfr __no_bit_access _A_SDR03
__A_SDR03:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff90H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_ITMC
// union <unnamed>#91 volatile __sfr __no_bit_access _A_ITMC
__A_ITMC:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff92H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SEC
// union <unnamed>#92 volatile __sfr __no_bit_access _A_SEC
__A_SEC:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff93H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_MIN
// union <unnamed>#93 volatile __sfr __no_bit_access _A_MIN
__A_MIN:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff9dH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_RTCC0
// union <unnamed>#103 volatile __sfr _A_RTCC0
__A_RTCC0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe2H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_IF1
// union <unnamed>#160 volatile __sfr _A_IF1
__A_IF1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe6H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_MK1
// union <unnamed>#175 volatile __sfr _A_MK1
__A_MK1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffeaH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR01
// union <unnamed>#190 volatile __sfr _A_PR01
__A_PR01:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffeeH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR11
// union <unnamed>#205 volatile __sfr _A_PR11
__A_PR11:
        DS 2
//   46 #include <ior5f11fll_ext.h>

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0037H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PU7
// union <unnamed>#226 volatile __near _A_PU7
__A_PU7:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f003eH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PU14
// union <unnamed>#230 volatile __near _A_PU14
__A_PU14:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0060H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PMC0
// union <unnamed>#238 volatile __near _A_PMC0
__A_PMC0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0070H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_NFEN0
// union <unnamed>#242 volatile __near _A_NFEN0
__A_NFEN0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f00f0H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PER0
// union <unnamed>#258 volatile __near _A_PER0
__A_PER0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f00f3H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_OSMC
// union <unnamed>#260 volatile __near __no_bit_access _A_OSMC
__A_OSMC:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0104H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SSR02
// union <unnamed>#264 const volatile __near __no_bit_access _A_SSR02
__A_SSR02:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0106H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SSR03
// union <unnamed>#267 const volatile __near __no_bit_access _A_SSR03
__A_SSR03:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f010eH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SIR03
// union <unnamed>#273 volatile __near __no_bit_access _A_SIR03
__A_SIR03:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0114H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SMR02
// union <unnamed>#276 volatile __near __no_bit_access _A_SMR02
__A_SMR02:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0116H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SMR03
// union <unnamed>#277 volatile __near __no_bit_access _A_SMR03
__A_SMR03:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f011cH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SCR02
// union <unnamed>#278 volatile __near __no_bit_access _A_SCR02
__A_SCR02:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f011eH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SCR03
// union <unnamed>#279 volatile __near __no_bit_access _A_SCR03
__A_SCR03:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0122H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SS0
// union <unnamed>#283 volatile __near _A_SS0
__A_SS0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0124H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_ST0
// union <unnamed>#286 volatile __near _A_ST0
__A_ST0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0126H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SPS0
// union <unnamed>#289 volatile __near __no_bit_access _A_SPS0
__A_SPS0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0128H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SO0
// union <unnamed>#292 volatile __near __no_bit_access _A_SO0
__A_SO0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f012aH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SOE0
// union <unnamed>#293 volatile __near _A_SOE0
__A_SOE0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0134H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_SOL0
// union <unnamed>#296 volatile __near __no_bit_access _A_SOL0
__A_SOL0:
        DS 2
//   47 #include "r_frequency_hopping.h"
//   48 
//   49 #if R_MODEM_SERVER || R_MODEM_CLIENT
//   50 #include "r_modem.h"
//   51 #endif
//   52 
//   53 //#include "iodefine.h" 
//   54 /****************************************************************************** 
//   55 Pragma Define
//   56 ******************************************************************************/ 
//   57 
//   58 #if defined (__CCRL__)
//   59 /* Stack area for interrupt handles is defined below("unsigned short RdrvUARTHdlStack[16]") */
//   60 #pragma interrupt RdrvUART_RxInt(vect=INTSR1)
//   61 #pragma interrupt RdrvUART_RxErrInt(vect=INTSRE1)
//   62 
//   63 /* Stack area for interrupt handle is defined below("unsigned short RdrvITHdlStack[16]") */
//   64 #pragma interrupt RdrvIT_FreqHoppInt(vect=INTIT)
//   65 #endif
//   66 
//   67 /****************************************************************************** 
//   68 Includes 
//   69 ******************************************************************************/ 
//   70 #include    "intrinsics.h"
//   71 #include    "hardware.h"
//   72 #include    "uart_interface.h"
//   73 #include    "phy.h"
//   74 #include    "stdio.h"
//   75 #include    "r_stdint.h"
//   76 #include    "r_nwk_api.h"
//   77 
//   78 /****************************************************************************** 
//   79 Macro definitions 
//   80 ******************************************************************************/
//   81 #if R_MODEM_SERVER || R_MODEM_CLIENT
//   82 #define RDRV_UART_BUFSIZ                (1)
//   83 #else
//   84 #define RDRV_UART_BUFSIZ                (R_UDP_MAX_UDP_DATA_LENGTH + 128)
//   85 #endif
//   86 
//   87 /* Interrupt Level */
//   88 #define RP_APL_INTLEVEL_UARTRXs         (0) // Max Level
//   89 #define RP_APL_INTLEVEL_UARTRXs_b0      (RP_APL_INTLEVEL_UARTRXs & 0x01)
//   90 #define RP_APL_INTLEVEL_UARTRXs_b1      ((RP_APL_INTLEVEL_UARTRXs & 0x02) >> 1)
//   91 
//   92 /* Clock conversion base */
//   93 #define MS2SEC     1000u 
//   94 #define SEC2MIN    60u
//   95 
//   96 /****************************************************************************** 
//   97 Private global variables and functions 
//   98 ******************************************************************************/ 
//   99 static void RdrvUART_Initialize(unsigned char brgc);
//  100 static void RdrvLED_Initialize(void);
//  101 
//  102 //static void RdrvUART_RxInt(void);
//  103 //static void RdrvUART_RxErrInt(void);
//  104 //static void RdrvTM02_FreqHoppInt(void);
//  105 
//  106 static void Rp_UART_RxInt(void);
//  107 static void Rp_UART_RxErrInt(void);
//  108 
//  109 #if 0
//  110 static void RdrvSwitch_Initialize(void);
//  111 #endif
//  112 

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//  113 volatile static unsigned short freqHoppTmIntervalMs;
_freqHoppTmIntervalMs:
        DS 2
        DS 2
//  114 volatile static short clock_correction_rtc;
//  115 
//  116 /****************************************************************************** 
//  117 Exported global variables and functions (to be accessed by other files) 
//  118 ******************************************************************************/

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//  119 unsigned char           RdrvUART_RXBuf[RDRV_UART_BUFSIZ];                   /* UART RX Buffer */
_RdrvUART_RXBuf:
        DS 1656

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//  120 unsigned short          RdrvUART_RXBuf_RdPtr;                               /* UART RX Buffer read pointer */
_RdrvUART_RXBuf_RdPtr:
        DS 2
//  121 unsigned short          RdrvUART_RXBuf_WrPtr;                               /* UART RX Buffer write pointer */
_RdrvUART_RXBuf_WrPtr:
        DS 2
//  122 
//  123 #if RTOS_USE

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//  124 unsigned short          RdrvUARTHdlStack[16];                               /* StackArea for UART interrupt handler */
_RdrvUARTHdlStack:
        DS 32

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//  125 unsigned short          RdrvITHdlStack[16];                                 /* StackArea for IT interrupt handler */
_RdrvITHdlStack:
        DS 32
//  126 #endif /* RTOS_USE */
//  127 
//  128 /****************************************************************************** 
//  129 Inline Assembler functions for special Stack Pointer handling
//  130 ******************************************************************************/ 
//  131 #if RTOS_USE
//  132 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _RpIntStackChange
          CFI NoCalls
        CODE
//  133 void RpIntStackChange(uint16_t pstk)
//  134 {
_RpIntStackChange:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  135 asm ("movw bc, sp");
        movw bc, sp
//  136 asm ("movw sp, ax"); // move SP
        movw sp, ax
//  137 asm ("push bc    "); //store original SP to the buffer
        push bc    
//  138 asm ("subw sp, #0x02");
        subw sp, #0x02
//  139 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
//  140 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _RpIntStackBack
          CFI NoCalls
        CODE
//  141 void RpIntStackBack(void)
//  142 {
_RpIntStackBack:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  143 asm ("addw sp, #0x02");
        addw sp, #0x02
//  144 asm ("pop ax");
        pop ax
//  145 asm ("movw sp, ax"); // move SP to the original
        movw sp, ax
//  146 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  147 
//  148 #endif /* RTOS_USE */
//  149 
//  150 /******************************************************************************
//  151 Function Name:       RdrvPeripheralInitialize
//  152 Parameters:          none.
//  153 Return value:        none.
//  154 Description:         Initialize peripheral settings.
//  155 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon0
          CFI Function _RdrvPeripheralInitialize
        CODE
//  156 void
//  157 RdrvPeripheralInitialize(void)
//  158 {
_RdrvPeripheralInitialize:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  159     __disable_interrupt();                  // Disable interrupt
        DI                           ;; 4 cycles
//  160 
//  161     // -------------------------------------------------
//  162     //   I/O port settings
//  163     // -------------------------------------------------
//  164 
//  165     // Port settings
//  166     PM7     = 0x38;         // P73-P75     ... Input(SW2,SW3,SW4)
        MOV       0xFFF27, #0x38     ;; 1 cycle
//  167     PU7     = 0x38;         // P73-P75     ... Pullup ON(SW2,SW3,SW4)
        MOV       0x37, #0x38        ;; 1 cycle
//  168 
//  169     PM14_bit.no0 = 1;       // P140        ... Input(SW1-1)
        SET1      0xFFF2E.0          ;; 2 cycles
//  170     PU14_bit.no0 = 1;       // PU140       ... Pullup ON(SW1-1)
        SET1      0xF003E.0          ;; 2 cycles
//  171     
//  172     // -------------------------------------------------
//  173     //   UART settings
//  174     // -------------------------------------------------
//  175     // Initialize
//  176 #if !R_MODEM_SERVER        
//  177     RdrvUART_Initialize(UART_BAUDRATE_500kBPS);
        MOV       A, #0x2            ;; 1 cycle
          CFI FunCall _RdrvUART_Initialize
        CALL      F:_RdrvUART_Initialize  ;; 3 cycles
//  178 #else
//  179     RdrvUART_Initialize(UART_BAUDRATE_230kBPS);
//  180 #endif
//  181     // -------------------------------------------------
//  182     //   LED settings
//  183     // -------------------------------------------------
//  184     // Initialize
//  185     RdrvLED_Initialize();
          CFI FunCall _RdrvLED_ALL_OFF
        CALL      F:_RdrvLED_ALL_OFF  ;; 3 cycles
        CLR1      0xFFF26.0          ;; 2 cycles
        CLR1      0xFFF26.1          ;; 2 cycles
        CLR1      0xFFF26.2          ;; 2 cycles
//  186 
//  187     // -------------------------------------------------
//  188     //   Interrupt Settings
//  189     // -------------------------------------------------
//  190     __enable_interrupt();                   // Enable Interrupt
        EI                           ;; 4 cycles
//  191 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 33 cycles
        ; ------------------------------------- Total: 33 cycles
        REQUIRE __A_PM7
        REQUIRE __A_PU7
        REQUIRE __A_PM14
        REQUIRE __A_PU14
        REQUIRE __A_PM6
//  192 
//  193 /******************************************************************************
//  194 Function Name:       RdrvUART_Initialize
//  195 Parameters:          bgrc
//  196                         Baudrate
//  197 Return value:        none.
//  198 Description:         Initialize UART settings.
//  199 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon0
          CFI Function _RdrvUART_Initialize
          CFI NoCalls
        CODE
//  200 static void
//  201 RdrvUART_Initialize(unsigned char brgc)
//  202 {
_RdrvUART_Initialize:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOV       B, A               ;; 1 cycle
//  203     SAU0EN = 1;                 // Enable Serial array unit(UART1=SAU0)
        SET1      0xF00F0.2          ;; 2 cycles
//  204 
//  205     __no_operation();           // Wait for safety
        NOP                          ;; 1 cycle
//  206     __no_operation();
        NOP                          ;; 1 cycle
//  207     __no_operation();
        NOP                          ;; 1 cycle
//  208     __no_operation();           
        NOP                          ;; 1 cycle
//  209 
//  210     /* UART1 initial setting */
//  211     ST0  |= 0x000C;             // UART1 receive & transmit disable
        MOVW      AX, 0x124          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, #0xC            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x124, AX          ;; 1 cycle
//  212     
//  213     STMK1 = 1U;    /* disable INTST1 interrupt */
        SET1      0xFFFE6.0          ;; 2 cycles
//  214     STIF1 = 0U;    /* clear INTST1 interrupt flag */
        CLR1      0xFFFE2.0          ;; 2 cycles
//  215     SRMK1 = 1U;    /* disable INTSR1 interrupt */
        SET1      0xFFFE6.1          ;; 2 cycles
//  216     SRIF1 = 0U;    /* clear INTSR1 interrupt flag */
        CLR1      0xFFFE2.1          ;; 2 cycles
//  217     SREMK1 = 1U;   /* disable INTSRE1 interrupt */
        SET1      0xFFFE6.2          ;; 2 cycles
//  218     SREIF1 = 0U;   /* clear INTSRE1 interrupt flag */
        CLR1      0xFFFE2.2          ;; 2 cycles
//  219 
//  220     /* Set INTST1 low priority */
//  221     STPR11 = 1U;
        SET1      0xFFFEE.0          ;; 2 cycles
//  222     STPR01 = 1U;
        SET1      0xFFFEA.0          ;; 2 cycles
//  223     /* Set INTSR1 low priority */
//  224     SRPR11 = RP_APL_INTLEVEL_UARTRXs_b1;
        CLR1      0xFFFEE.1          ;; 2 cycles
//  225     SRPR01 = RP_APL_INTLEVEL_UARTRXs_b0;
        CLR1      0xFFFEA.1          ;; 2 cycles
//  226     /* Set INTSRE1 low priority */
//  227     SREPR11 = 1U;
        SET1      0xFFFEE.2          ;; 2 cycles
//  228     SREPR01 = 1U;
        SET1      0xFFFEA.2          ;; 2 cycles
//  229     
//  230     SMR02 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK00 | SAU_TRIGGER_SOFTWARE | SAU_UART | SAU_BUFFER_EMPTY;
        MOVW      AX, #0x23          ;; 1 cycle
        MOVW      0x114, AX          ;; 1 cycle
//  231     SCR02 = SAU_TRANSMISSION | SAU_INTSRE_MASK | SAU_PARITY_NONE | SAU_LSB | SAU_STOP_1 | SAU_LENGTH_8;
        MOVW      AX, #0x8097        ;; 1 cycle
        MOVW      0x11C, AX          ;; 1 cycle
//  232 
//  233     NFEN0 |= 0x04;              // noise filter on(UART1)
        SET1      0xF0070.2          ;; 2 cycles
//  234     SIR03 = 0x0007;
        MOVW      AX, #0x7           ;; 1 cycle
        MOVW      0x10E, AX          ;; 1 cycle
//  235     
//  236     SMR03 = SAU_SMRMN_INITIALVALUE | SAU_CLOCK_SELECT_CK00 | SAU_TRIGGER_RXD | SAU_UART | SAU_TRANSFER_END;
        MOVW      AX, #0x122         ;; 1 cycle
        MOVW      0x116, AX          ;; 1 cycle
//  237     SCR03 = SAU_RECEPTION | SAU_INTSRE_ENABLE | SAU_PARITY_NONE | SAU_LSB | SAU_STOP_1 | SAU_LENGTH_8;
        MOVW      AX, #0x4497        ;; 1 cycle
        MOVW      0x11E, AX          ;; 1 cycle
//  238 
//  239     if(brgc == UART_BAUDRATE_500kBPS)
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNZ       ??RdrvFREQHOPPTIMER_Stop_0  ;; 4 cycles
        ; ------------------------------------- Block: 54 cycles
//  240     {
//  241         SPS0 = (UART_CLOCK_SELECT_500kBPS<<4) | (UART_CLOCK_SELECT_500kBPS);    
        CLRW      AX                 ;; 1 cycle
        MOVW      0x126, AX          ;; 1 cycle
//  242         SDR02 = UART_DIVISION_RATE_500kBPS;
        MOVW      0xFFF44, #0x3E00   ;; 1 cycle
//  243         SDR03 = UART_DIVISION_RATE_500kBPS;
        MOVW      0xFFF46, #0x3E00   ;; 1 cycle
        BR        S:??RdrvFREQHOPPTIMER_Stop_1  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  244     }
//  245     else if(brgc == UART_BAUDRATE_230kBPS)
??RdrvFREQHOPPTIMER_Stop_0:
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??RdrvFREQHOPPTIMER_Stop_2  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  246     {
//  247         SPS0 = (UART_CLOCK_SELECT_230kBPS<<4) | (UART_CLOCK_SELECT_230kBPS);    
        CLRW      AX                 ;; 1 cycle
        BR        S:??RdrvFREQHOPPTIMER_Stop_3  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  248         SDR02 = UART_DIVISION_RATE_230kBPS;
//  249         SDR03 = UART_DIVISION_RATE_230kBPS;
//  250     }
//  251     else
//  252     {   // default UART_BAUDRATE_115kBPS
//  253         SPS0 = (UART_CLOCK_SELECT_115kBPS<<4) | (UART_CLOCK_SELECT_115kBPS);    
??RdrvFREQHOPPTIMER_Stop_2:
        MOVW      AX, #0x11          ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RdrvFREQHOPPTIMER_Stop_3:
        MOVW      0x126, AX          ;; 1 cycle
//  254         SDR02 = UART_DIVISION_RATE_115kBPS;
        MOVW      0xFFF44, #0x8800   ;; 1 cycle
//  255         SDR03 = UART_DIVISION_RATE_115kBPS;
        MOVW      0xFFF46, #0x8800   ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  256     }
//  257 
//  258     SO0  |= SAU_CH2_DATA_OUTPUT_1;
??RdrvFREQHOPPTIMER_Stop_1:
        MOVW      AX, 0x128          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, #0x4            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x128, AX          ;; 1 cycle
//  259     SOL0 |= SAU_CHANNEL2_NORMAL;    /* output level normal */
        MOVW      AX, 0x134          ;; 1 cycle
        MOVW      0x134, AX          ;; 1 cycle
//  260     SOE0 |= SAU_CH2_OUTPUT_ENABLE;    /* enable UART1 output */
        SET1      0xF012A.2          ;; 2 cycles
//  261 
//  262     /* Set RxD1 pin */
//  263     PMC0 &= 0xF7U;
        MOVW      HL, #0x60          ;; 1 cycle
        CLR1      [HL].3             ;; 2 cycles
//  264     PM0 |= 0x08U;
        SET1      0xFFF20.3          ;; 2 cycles
//  265     /* Set TxD1 pin */
//  266     PMC0 &= 0xFBU;
        CLR1      [HL].2             ;; 2 cycles
//  267     P0 |= 0x04U;
        SET1      S:0xFFF00.2        ;; 2 cycles
//  268     PM0 &= 0xFBU;
        CLR1      0xFFF20.2          ;; 2 cycles
//  269 
//  270     /* TXD pin setting */
//  271     UART_TX_PORT    = PORT_HI;
        SET1      S:0xFFF00.2        ;; 2 cycles
//  272 
//  273     STIF1 = 0U;    /* clear INTST1 interrupt flag */
        CLR1      0xFFFE2.0          ;; 2 cycles
//  274     SRIF1 = 0U;    /* clear INTSR1 interrupt flag */
        CLR1      0xFFFE2.1          ;; 2 cycles
//  275     SREIF1 = 0U;   /* clear INTSRE1 interrupt flag */
        CLR1      0xFFFE2.2          ;; 2 cycles
//  276 
//  277     UART_RX_INT_MASK    = 0;        // INTSR1 enable
        CLR1      0xFFFE6.1          ;; 2 cycles
//  278     UART_ERR_INT_MASK   = 0;        // INTSRE1 enable
        CLR1      0xFFFE6.2          ;; 2 cycles
//  279     UART_TX_INT_MASK    = 1;        // INTST1 disable
        SET1      0xFFFE6.0          ;; 2 cycles
//  280 
//  281     SOE0 |= SAU_CH2_OUTPUT_ENABLE;    /* enable UART1 output */
        SET1      0xF012A.2          ;; 2 cycles
//  282     SS0 |= SAU_CH2_START_TRG_ON;    /* UART1 receive enable */
        MOVW      HL, #0x122         ;; 1 cycle
        SET1      [HL].2             ;; 2 cycles
//  283     SS0 |= SAU_CH3_START_TRG_ON;    /* UART1 transmit enable */
        SET1      [HL].3             ;; 2 cycles
//  284 
//  285     /* Init UART TX/RX Buffer pointer */
//  286     RdrvUART_RXBuf_RdPtr = 0x00;
        CLRW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_RdrvUART_RXBuf_RdPtr)  ;; 1 cycle
        MOVW      ES:_RdrvUART_RXBuf_RdPtr, AX  ;; 2 cycles
//  287     RdrvUART_RXBuf_WrPtr = 0x00;
        MOVW      ES:_RdrvUART_RXBuf_RdPtr+2, AX  ;; 2 cycles
//  288 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 53 cycles
        ; ------------------------------------- Total: 127 cycles
        REQUIRE __A_PER0
        REQUIRE __A_ST0
        REQUIRE __A_MK1
        REQUIRE __A_IF1
        REQUIRE __A_PR11
        REQUIRE __A_PR01
        REQUIRE __A_SMR02
        REQUIRE __A_SCR02
        REQUIRE __A_NFEN0
        REQUIRE __A_SIR03
        REQUIRE __A_SMR03
        REQUIRE __A_SCR03
        REQUIRE __A_SPS0
        REQUIRE __A_SDR02
        REQUIRE __A_SDR03
        REQUIRE __A_SO0
        REQUIRE __A_SOL0
        REQUIRE __A_SOE0
        REQUIRE __A_PMC0
        REQUIRE __A_PM0
        REQUIRE __A_P0
        REQUIRE __A_SS0
//  289 
//  290 
//  291 /******************************************************************************
//  292 Function Name:       RdrvUART_GetLen
//  293 Parameters:          none.
//  294 Return value:        Length of received strings.
//  295 Description:         Return Length of received strings.
//  296 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon0
          CFI Function _RdrvUART_GetLen
          CFI NoCalls
        CODE
//  297 short
//  298 RdrvUART_GetLen(void)
//  299 {
_RdrvUART_GetLen:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  300     short   len;
//  301 
//  302     len = (short)RdrvUART_RXBuf_WrPtr - (short)RdrvUART_RXBuf_RdPtr;
        MOV       ES, #BYTE3(_RdrvUART_RXBuf_RdPtr)  ;; 1 cycle
        MOVW      HL, ES:_RdrvUART_RXBuf_RdPtr  ;; 2 cycles
        MOVW      AX, ES:_RdrvUART_RXBuf_RdPtr+2  ;; 2 cycles
        SUBW      AX, HL             ;; 1 cycle
//  303 
//  304     if (len < 0)
        BF        A.7, ??RdrvFREQHOPPTIMER_Stop_4  ;; 5 cycles
        ; ------------------------------------- Block: 11 cycles
//  305     {
//  306         len += RDRV_UART_BUFSIZ;
        ADDW      AX, #0x678         ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  307     }
//  308 
//  309     return len;
??RdrvFREQHOPPTIMER_Stop_4:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 18 cycles
//  310 }
//  311 
//  312 
//  313 /******************************************************************************
//  314 Function Name:       RdrvUART_GetChar
//  315 Parameters:          none.
//  316 Return value:        Received character.
//  317 Description:         Get character from UART.
//  318 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon0
          CFI Function _RdrvUART_GetChar
          CFI NoCalls
        CODE
//  319 short
//  320 RdrvUART_GetChar(void)
//  321 {
_RdrvUART_GetChar:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  322     unsigned char   ch;
//  323 
//  324     if (RdrvUART_RXBuf_RdPtr == RdrvUART_RXBuf_WrPtr)
        MOV       ES, #BYTE3(_RdrvUART_RXBuf_RdPtr)  ;; 1 cycle
        MOVW      HL, ES:_RdrvUART_RXBuf_RdPtr+2  ;; 2 cycles
        MOVW      AX, ES:_RdrvUART_RXBuf_RdPtr  ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BNZ       ??RdrvFREQHOPPTIMER_Stop_5  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
//  325     {
//  326         return EOF;
        MOVW      AX, #0xFFFF        ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 7 cycles
//  327 
//  328     }
//  329     else
//  330     {
//  331         ch = RdrvUART_RXBuf[RdrvUART_RXBuf_RdPtr];
??RdrvFREQHOPPTIMER_Stop_5:
        ADDW      AX, #LWRD(_RdrvUART_RXBuf)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RdrvUART_RXBuf)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
//  332         RdrvUART_RXBuf_RdPtr++;
        MOV       ES, #BYTE3(_RdrvUART_RXBuf_RdPtr)  ;; 1 cycle
        INCW      ES:_RdrvUART_RXBuf_RdPtr  ;; 3 cycles
//  333         if(RdrvUART_RXBuf_RdPtr >= RDRV_UART_BUFSIZ){
        MOVW      HL, ES:_RdrvUART_RXBuf_RdPtr  ;; 2 cycles
        XCHW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x678         ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        BC        ??RdrvFREQHOPPTIMER_Stop_6  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
//  334             RdrvUART_RXBuf_RdPtr = 0;
        MOVW      HL, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      ES:_RdrvUART_RXBuf_RdPtr, AX  ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
//  335         }
//  336 
//  337         return (short)ch;
??RdrvFREQHOPPTIMER_Stop_6:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 48 cycles
//  338     }
//  339 }
//  340 
//  341 
//  342 /******************************************************************************
//  343 Function Name:       RdrvUART_PutChar
//  344 Parameters:          ch
//  345                        Character to be sent.
//  346 Return value:        none.
//  347 Description:         Put character to UART.
//  348 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon0
          CFI Function _RdrvUART_PutChar
          CFI NoCalls
        CODE
//  349 void RdrvUART_PutChar(unsigned char ch)
//  350 {
_RdrvUART_PutChar:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOV       B, A               ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  351     /* Wait while Tx register is not empty */
//  352     while((UART_TX_STATUS & (SAU_DATA_STORED))!=0);
??RdrvUART_PutChar_0:
        MOVW      AX, 0x104          ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        BT        A.5, ??RdrvUART_PutChar_0  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
//  353 
//  354     /* Send character immediately */
//  355     UART_TX = ch;
        MOV       A, B               ;; 1 cycle
        MOV       0xFFF44, A         ;; 1 cycle
//  356 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 16 cycles
        REQUIRE __A_SSR02
        REQUIRE __A_SDR02
//  357 
//  358 
//  359 /******************************************************************************
//  360 Function Name:       RdrvUART_RxInt
//  361 Parameters:          none.
//  362 Return value:        none.
//  363 Description:         UART Rx interrupt handler.
//  364 ******************************************************************************/
//  365 #if defined(RDRV_UART_RECEIVE_HANDLER)
//  366 
//  367 typedef void (*RCallbackT)(void);
//  368 extern volatile RCallbackT pFuncUartRcvHandler;
//  369 
//  370 #pragma interrupt RdrvUART_RxInt
//  371 static void RdrvUART_RxInt(void)
//  372 {
//  373     (*pFuncUartRcvHandler)();
//  374 }
//  375 
//  376 #else
//  377 
//  378 #if defined (__ICCRL78__)
//  379 #pragma vector = INTSR1_vect

        SECTION `.text`:CODE:NOROOT(0)
        CALL_GRAPH_ROOT _RdrvUART_RxInt, "interrupt"
          CFI Block cfiBlock7 Using cfiCommon1
          CFI Function _RdrvUART_RxInt
          CFI NoCalls
        CODE
//  380 __interrupt void RdrvUART_RxInt(void)
//  381 #else
//  382 void RdrvUART_RxInt(void)
//  383 #endif
//  384 {
_RdrvUART_RxInt:
___interrupt_0x26:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI X Frame(CFA, -6)
          CFI A Frame(CFA, -5)
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI E Frame(CFA, -8)
          CFI D Frame(CFA, -7)
          CFI CFA SP+8
        PUSH      HL                 ;; 1 cycle
          CFI L Frame(CFA, -10)
          CFI H Frame(CFA, -9)
          CFI CFA SP+10
        MOVW      AX, 0xFFFFC        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
//  385     #if RTOS_USE && defined(__CCRL__)
//  386     RpIntStackChange((uint16_t)(&RdrvUARTHdlStack[16]));    // (no need '-1')
//  387     #endif
//  388 
//  389     Rp_UART_RxInt();
        MOV       A, 0xFFF46         ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       ES, #BYTE3(_RdrvUART_RXBuf_RdPtr)  ;; 1 cycle
        MOVW      HL, ES:_RdrvUART_RXBuf_RdPtr  ;; 2 cycles
        MOVW      AX, ES:_RdrvUART_RXBuf_RdPtr+2  ;; 2 cycles
        SUBW      AX, HL             ;; 1 cycle
        BF        A.7, ??RdrvFREQHOPPTIMER_Stop_11  ;; 5 cycles
        ; ------------------------------------- Block: 19 cycles
        ADDW      AX, #0x678         ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RdrvFREQHOPPTIMER_Stop_11:
        CMPW      AX, #0x679         ;; 1 cycle
        BNC       ??RdrvFREQHOPPTIMER_Stop_12  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, [SP]            ;; 1 cycle
        MOVW      DE, ES:_RdrvUART_RXBuf_RdPtr+2  ;; 2 cycles
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #LWRD(_RdrvUART_RXBuf)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOV       ES, #BYTE3(_RdrvUART_RXBuf)  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RdrvUART_RXBuf_RdPtr)  ;; 1 cycle
        INCW      ES:_RdrvUART_RXBuf_RdPtr+2  ;; 3 cycles
        MOVW      AX, ES:_RdrvUART_RXBuf_RdPtr+2  ;; 2 cycles
        CMPW      AX, #0x678         ;; 1 cycle
        BC        ??RdrvFREQHOPPTIMER_Stop_12  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_RdrvUART_RXBuf_RdPtr+2, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
//  390 
//  391     #if RTOS_USE && defined(__CCRL__)
//  392     RpIntStackBack();
//  393     #endif
//  394 }
??RdrvFREQHOPPTIMER_Stop_12:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      0xFFFFC, AX        ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI L SameValue
          CFI H SameValue
          CFI CFA SP+8
        POP       DE                 ;; 1 cycle
          CFI E SameValue
          CFI D SameValue
          CFI CFA SP+6
        POP       AX                 ;; 1 cycle
          CFI X SameValue
          CFI A SameValue
          CFI CFA SP+4
        RETI                         ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 61 cycles
        REQUIRE __A_SDR03
        REQUIRE ___interrupt_tab_0x26
//  395 
//  396 static void Rp_UART_RxInt(void)
//  397 {
//  398 #if R_MODEM_SERVER || R_MODEM_CLIENT
//  399     extern r_modem_ctx g_modem_nwk_ctx;
//  400     r_modem_rx(UART_RX, &g_modem_nwk_ctx);
//  401 #else
//  402     volatile unsigned char      ch;
//  403     short                       len;
//  404 
//  405     /* Read character from UART */
//  406     ch = UART_RX;
//  407 
//  408     /* Get number of characters in buffer */
//  409     len = (short)RdrvUART_RXBuf_WrPtr - (short)RdrvUART_RXBuf_RdPtr;
//  410     if(len < 0)
//  411     {
//  412         len += RDRV_UART_BUFSIZ;
//  413     }
//  414 
//  415     /* Buffer overflow */
//  416     if(len > RDRV_UART_BUFSIZ)
//  417     {
//  418         return;
//  419     }
//  420 
//  421     /* Update poiner and Store character to buffer */
//  422     RdrvUART_RXBuf[RdrvUART_RXBuf_WrPtr] = ch;
//  423 
//  424     RdrvUART_RXBuf_WrPtr++;
//  425     if(RdrvUART_RXBuf_WrPtr >= RDRV_UART_BUFSIZ){
//  426         RdrvUART_RXBuf_WrPtr = 0;
//  427     }
//  428 #endif /* R_MODEM_SERVER || R_MODEM_CLIENT */
//  429 }
//  430 
//  431 #endif
//  432 
//  433 
//  434 /******************************************************************************
//  435 Function Name:       RdrvUART_RxErrInt
//  436 Parameters:          none.
//  437 Return value:        none.
//  438 Description:         UART RxErr interrupt handler.
//  439 ******************************************************************************/
//  440 #if defined (__ICCRL78__)
//  441 #pragma vector = INTSRE1_vect

        SECTION `.text`:CODE:NOROOT(0)
        CALL_GRAPH_ROOT _RdrvUART_RxErrInt, "interrupt"
          CFI Block cfiBlock8 Using cfiCommon1
          CFI Function _RdrvUART_RxErrInt
          CFI NoCalls
        CODE
//  442 __interrupt void RdrvUART_RxErrInt(void)
//  443 #else
//  444 void RdrvUART_RxErrInt(void)
//  445 #endif
//  446 {
_RdrvUART_RxErrInt:
___interrupt_0x28:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI X Frame(CFA, -6)
          CFI A Frame(CFA, -5)
          CFI CFA SP+6
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
//  447     #if RTOS_USE && defined(__CCRL__)
//  448     RpIntStackChange((uint16_t)(&RdrvUARTHdlStack[16]));    // (no need '-1')
//  449     #endif
//  450 
//  451     Rp_UART_RxErrInt();
        MOVW      AX, 0x106          ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        AND       A, #0x7            ;; 1 cycle
        BZ        ??RdrvFREQHOPPTIMER_Stop_13  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOV       A, 0xFFF46         ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, 0x106          ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        AND       A, #0x7            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x10E, AX          ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
//  452 
//  453     #if RTOS_USE && defined(__CCRL__)
//  454     RpIntStackBack();
//  455     #endif
//  456 }
??RdrvFREQHOPPTIMER_Stop_13:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+6
        POP       AX                 ;; 1 cycle
          CFI X SameValue
          CFI A SameValue
          CFI CFA SP+4
        RETI                         ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 27 cycles
        REQUIRE __A_SSR03
        REQUIRE __A_SDR03
        REQUIRE __A_SIR03
        REQUIRE ___interrupt_tab_0x28
//  457 
//  458 static void Rp_UART_RxErrInt(void)
//  459 {
//  460     volatile unsigned char      ch;
//  461 
//  462     /* Read UART status */
//  463     ch = (unsigned char)UART_RX_STATUS;
//  464 
//  465     if (ch & (SAU_PARITY_ERROR | SAU_FRAMING_ERROR | SAU_OVERRUN_ERROR)) {
//  466 
//  467         /* Error -> read dummy data then Exit */
//  468         ch = UART_RX;
//  469 
//  470         SIR03 = (unsigned short)(SSR03 & 0x0007);
//  471     }
//  472 }
//  473 
//  474 /******************************************************************************
//  475 Function Name:       RdrvIT_FreqHoppInt
//  476 Parameters:          none.
//  477 Return value:        none.
//  478 Description:         INTIT interrupt handler.
//  479 ******************************************************************************/
//  480 #if defined (__ICCRL78__)
//  481 #pragma vector = INTIT_vect

        SECTION `.text`:CODE:NOROOT(0)
        CALL_GRAPH_ROOT _RdrvIT_FreqHoppInt, "interrupt"
          CFI Block cfiBlock9 Using cfiCommon1
          CFI Function _RdrvIT_FreqHoppInt
        CODE
//  482 __interrupt void RdrvIT_FreqHoppInt(void)
//  483 #else
//  484 void RdrvIT_FreqHoppInt(void)
//  485 #endif
//  486 {
_RdrvIT_FreqHoppInt:
___interrupt_0x38:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI X Frame(CFA, -6)
          CFI A Frame(CFA, -5)
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI C Frame(CFA, -8)
          CFI B Frame(CFA, -7)
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI E Frame(CFA, -10)
          CFI D Frame(CFA, -9)
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI L Frame(CFA, -12)
          CFI H Frame(CFA, -11)
          CFI CFA SP+12
        MOVW      AX, 0xFFFFC        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        ; Auto size: 0
//  487     #if RTOS_USE && defined(__CCRL__)
//  488     RpIntStackChange((uint16_t)(&RdrvITHdlStack[16]));    
//  489     #endif
//  490  
//  491     unsigned short msec_inc_step = 0;
//  492     
//  493     /* Compensate deviation from RTC if realized */ 
//  494     if(clock_correction_rtc > 0)
        MOV       ES, #BYTE3(_freqHoppTmIntervalMs)  ;; 1 cycle
        MOVW      AX, ES:_freqHoppTmIntervalMs+2  ;; 2 cycles
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8001        ;; 1 cycle
        BC        ??RdrvFREQHOPPTIMER_Stop_14  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
//  495     {
//  496         /* we are behind the RTC reference -> adjust time increment accordingly */
//  497         msec_inc_step = R_FREQ_HOPP_TIMER_INTERVAL_MS + clock_correction_rtc;
        MOVW      HL, ES:_freqHoppTmIntervalMs+2  ;; 2 cycles
        INCW      HL                 ;; 1 cycle
//  498         clock_correction_rtc = 0;
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_freqHoppTmIntervalMs+2, AX  ;; 2 cycles
//  499     }
//  500     else if (clock_correction_rtc < 0)
//  501     {
//  502         /* we are before the RTC reference -> do not expand time interval, 
//  503            successively adjust frequency hopping timer interval */ 
//  504         clock_correction_rtc += R_FREQ_HOPP_TIMER_INTERVAL_MS;
//  505     }
//  506     else
//  507     {
//  508         /* we are in sync with RTC reference -> do default increment  */
//  509         msec_inc_step = R_FREQ_HOPP_TIMER_INTERVAL_MS;
//  510     }
//  511     
//  512     /* check if frequency hopping timer interval has to be expanded */
//  513     if(msec_inc_step)
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??RdrvFREQHOPPTIMER_Stop_15  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        BR        S:??RdrvFREQHOPPTIMER_Stop_16  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RdrvFREQHOPPTIMER_Stop_14:
        MOVW      AX, ES:_freqHoppTmIntervalMs+2  ;; 2 cycles
        BF        A.7, ??RdrvFREQHOPPTIMER_Stop_17  ;; 5 cycles
        ; ------------------------------------- Block: 7 cycles
        INCW      ES:_freqHoppTmIntervalMs+2  ;; 3 cycles
        BR        S:??RdrvFREQHOPPTIMER_Stop_16  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RdrvFREQHOPPTIMER_Stop_17:
        ONEW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  514     {
//  515         /* Expand frequency hopping timer interval */
//  516         freqHoppTmIntervalMs += msec_inc_step;
??RdrvFREQHOPPTIMER_Stop_15:
        MOVW      AX, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        ADDW      AX, HL             ;; 1 cycle
        MOVW      ES:_freqHoppTmIntervalMs, AX  ;; 2 cycles
//  517     
//  518         /* Compensate inaccuracy of 1ms Timer Interval: 
//  519            Interval time, 1/32.768 [kHz] x (32 + 1) = 1.00708 [ms]
//  520            To compensate 7.08ms clock delay in 1 sec, increase schedule
//  521            twice seven times a second, 1000ms / 7 = 143 ms */ 
//  522         if((freqHoppTmIntervalMs % 143u) == 0)
        MOVW      DE, #0x8F          ;; 1 cycle
        MOVW      AX, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        DIVHU                        ;; 9 cycles
        NOP                          ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??RdrvFREQHOPPTIMER_Stop_18  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
//  523         {
//  524             freqHoppTmIntervalMs++;
        INCW      ES:_freqHoppTmIntervalMs  ;; 3 cycles
//  525             msec_inc_step++;
        INCW      HL                 ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
//  526         }
//  527     
//  528         /* Track frequency hopping time interval of 1000ms */ 
//  529         if( freqHoppTmIntervalMs >= MS2SEC)
??RdrvFREQHOPPTIMER_Stop_18:
        MOVW      AX, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        CMPW      AX, #0x3E8         ;; 1 cycle
        BC        ??RdrvFREQHOPPTIMER_Stop_19  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  530         {
//  531             freqHoppTmIntervalMs -= MS2SEC;
        MOVW      AX, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        ADDW      AX, #0xFC18        ;; 1 cycle
        MOVW      ES:_freqHoppTmIntervalMs, AX  ;; 2 cycles
        ; ------------------------------------- Block: 5 cycles
//  532         }
//  533  
//  534         __enable_interrupt();           // Enable Interrupts
??RdrvFREQHOPPTIMER_Stop_19:
        EI                           ;; 4 cycles
//  535  
//  536         /* Call Frequency Hopping Timer handler */
//  537         RmFreqHoppTimer_Handler(msec_inc_step);
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall _RmFreqHoppTimer_Handler
        CALL      F:_RmFreqHoppTimer_Handler  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
//  538     }
//  539     
//  540     #if RTOS_USE && defined(__CCRL__)
//  541     RpIntStackBack();
//  542     #endif
//  543 }
??RdrvFREQHOPPTIMER_Stop_16:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      0xFFFFC, AX        ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI L SameValue
          CFI H SameValue
          CFI CFA SP+10
        POP       DE                 ;; 1 cycle
          CFI E SameValue
          CFI D SameValue
          CFI CFA SP+8
        POP       BC                 ;; 1 cycle
          CFI C SameValue
          CFI B SameValue
          CFI CFA SP+6
        POP       AX                 ;; 1 cycle
          CFI X SameValue
          CFI A SameValue
          CFI CFA SP+4
        RETI                         ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 105 cycles
        REQUIRE ___interrupt_tab_0x38
//  544 
//  545 /******************************************************************************
//  546 Function Name:       RdrvRTC_TimeCorrectionInt
//  547 Parameters:          none.
//  548 Return value:        none.
//  549 Description:         INTRTC interrupt handler.
//  550 ******************************************************************************/
//  551 #if defined (__ICCRL78__)
//  552 #pragma vector = INTRTC_vect

        SECTION `.text`:CODE:NOROOT(0)
        CALL_GRAPH_ROOT _RdrvRTC_TimeCorrectionInt, "interrupt"
          CFI Block cfiBlock10 Using cfiCommon1
          CFI Function _RdrvRTC_TimeCorrectionInt
          CFI NoCalls
        CODE
//  553 __interrupt void RdrvRTC_TimeCorrectionInt(void)
//  554 #else
//  555 void RdrvRTC_TimeCorrectionInt(void)
//  556 #endif
//  557 {
_RdrvRTC_TimeCorrectionInt:
___interrupt_0x36:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI X Frame(CFA, -6)
          CFI A Frame(CFA, -5)
          CFI CFA SP+6
        PUSH      HL                 ;; 1 cycle
          CFI L Frame(CFA, -8)
          CFI H Frame(CFA, -7)
          CFI CFA SP+8
        MOVW      AX, 0xFFFFC        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 0
//  558      __enable_interrupt();           // Enable Interrupts
        EI                           ;; 4 cycles
//  559 
//  560     /* check if frequency hopping timer is behind, before of in sync with
//  561        the RTC time reference */ 
//  562     if((freqHoppTmIntervalMs > (MS2SEC/2)) && (freqHoppTmIntervalMs < MS2SEC))
        MOV       ES, #BYTE3(_freqHoppTmIntervalMs)  ;; 1 cycle
        MOVW      AX, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        CMPW      AX, #0x1F5         ;; 1 cycle
        BC        ??RdrvFREQHOPPTIMER_Stop_20  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        CMPW      AX, #0x3E8         ;; 1 cycle
        BNC       ??RdrvFREQHOPPTIMER_Stop_20  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  563     {
//  564         /* we are behind the RTC time reference */
//  565         clock_correction_rtc = MS2SEC - freqHoppTmIntervalMs;
        MOVW      HL, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        MOVW      AX, #0x3E8         ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        MOVW      ES:_freqHoppTmIntervalMs+2, AX  ;; 2 cycles
        BR        S:??RdrvFREQHOPPTIMER_Stop_21  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  566     } 
//  567     else if ((freqHoppTmIntervalMs > 0) && (freqHoppTmIntervalMs <= (MS2SEC/2)))
??RdrvFREQHOPPTIMER_Stop_20:
        MOVW      AX, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??RdrvFREQHOPPTIMER_Stop_22  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      AX, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        CMPW      AX, #0x1F5         ;; 1 cycle
        BNC       ??RdrvFREQHOPPTIMER_Stop_22  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  568     {
//  569         /* we are before the RTC time reference */ 
//  570         clock_correction_rtc = 0 - freqHoppTmIntervalMs;
        MOVW      AX, ES:_freqHoppTmIntervalMs  ;; 2 cycles
        XOR       A, #0xFF           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        XOR       A, #0xFF           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      ES:_freqHoppTmIntervalMs+2, AX  ;; 2 cycles
        BR        S:??RdrvFREQHOPPTIMER_Stop_21  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
//  571     } 
//  572     else
//  573     { 
//  574         /* we are in sync with the RTC time reference */
//  575         clock_correction_rtc = 0;
??RdrvFREQHOPPTIMER_Stop_22:
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:_freqHoppTmIntervalMs+2, AX  ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
//  576     }
//  577 }
??RdrvFREQHOPPTIMER_Stop_21:
        POP       AX                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      0xFFFFC, AX        ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI L SameValue
          CFI H SameValue
          CFI CFA SP+6
        POP       AX                 ;; 1 cycle
          CFI X SameValue
          CFI A SameValue
          CFI CFA SP+4
        RETI                         ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 71 cycles
        REQUIRE ___interrupt_tab_0x36
//  578 
//  579 /******************************************************************************
//  580 Function Name:       RdrvLED_Initialize
//  581 Parameters:          none.
//  582 Return value:        none.
//  583 Description:         Initialize LED ports.
//  584 ******************************************************************************/
//  585 static void
//  586 RdrvLED_Initialize(void)
//  587 {
//  588     /* Turn off all LEDs */
//  589     RdrvLED_ALL_OFF();
//  590 
//  591     /* Set port mode */
//  592     PORT_LED0_PM = PORT_OUTPUT;
//  593     PORT_LED1_PM = PORT_OUTPUT;
//  594     PORT_LED2_PM = PORT_OUTPUT;
//  595 }
//  596 
//  597 
//  598 /******************************************************************************
//  599 Function Name:       RdrvLED_ALL_ON
//  600 Parameters:          none.
//  601 Return value:        none.
//  602 Description:         Turn on all LEDs.
//  603 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon0
          CFI Function _RdrvLED_ALL_ON
          CFI NoCalls
        CODE
//  604 void
//  605 RdrvLED_ALL_ON(void)
//  606 {
_RdrvLED_ALL_ON:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  607     PORT_LED0_PORT = PORT_LO;   /* LED0 on */
        CLR1      S:0xFFF06.0        ;; 2 cycles
//  608     PORT_LED1_PORT = PORT_LO;   /* LED1 on */
        CLR1      S:0xFFF06.1        ;; 2 cycles
//  609     PORT_LED2_PORT = PORT_LO;   /* LED2 on */
        CLR1      S:0xFFF06.2        ;; 2 cycles
//  610 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 12 cycles
        REQUIRE __A_P6
//  611 
//  612 
//  613 /******************************************************************************
//  614 Function Name:       RdrvLED_ALL_OFF
//  615 Parameters:          none.
//  616 Return value:        none.
//  617 Description:         Turn off all LEDs.
//  618 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon0
          CFI Function _RdrvLED_ALL_OFF
          CFI NoCalls
        CODE
//  619 void
//  620 RdrvLED_ALL_OFF(void)
//  621 {
_RdrvLED_ALL_OFF:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  622     PORT_LED0_PORT = PORT_HI;   /* LED0 off */
        SET1      S:0xFFF06.0        ;; 2 cycles
//  623     PORT_LED1_PORT = PORT_HI;   /* LED1 off */
        SET1      S:0xFFF06.1        ;; 2 cycles
//  624     PORT_LED2_PORT = PORT_HI;   /* LED2 off */
        SET1      S:0xFFF06.2        ;; 2 cycles
//  625 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 12 cycles
        REQUIRE __A_P6
//  626 
//  627 
//  628 /******************************************************************************
//  629 Function Name:       RdrvLED0_ON
//  630 Parameters:          none.
//  631 Return value:        none.
//  632 Description:         Turn on LED0.
//  633 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon0
          CFI Function _RdrvLED0_ON
          CFI NoCalls
        CODE
//  634 void
//  635 RdrvLED0_ON(void)
//  636 {
_RdrvLED0_ON:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  637     PORT_LED0_PORT = PORT_LO;   /* LED0 on */
        CLR1      S:0xFFF06.0        ;; 2 cycles
//  638 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
        REQUIRE __A_P6
//  639 
//  640 
//  641 /******************************************************************************
//  642 Function Name:       RdrvLED0_OFF
//  643 Parameters:          none.
//  644 Return value:        none.
//  645 Description:         Turn off LED0.
//  646 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon0
          CFI Function _RdrvLED0_OFF
          CFI NoCalls
        CODE
//  647 void
//  648 RdrvLED0_OFF(void)
//  649 {
_RdrvLED0_OFF:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  650     PORT_LED0_PORT = PORT_HI;   /* LED0 off */
        SET1      S:0xFFF06.0        ;; 2 cycles
//  651 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
        REQUIRE __A_P6
//  652 
//  653 
//  654 /******************************************************************************
//  655 Function Name:       RdrvLED1_ON
//  656 Parameters:          none.
//  657 Return value:        none.
//  658 Description:         Turn on LED1.
//  659 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon0
          CFI Function _RdrvLED1_ON
          CFI NoCalls
        CODE
//  660 void
//  661 RdrvLED1_ON(void)
//  662 {
_RdrvLED1_ON:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  663     PORT_LED1_PORT = PORT_LO;   /* LED1 on */
        CLR1      S:0xFFF06.1        ;; 2 cycles
//  664 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
        REQUIRE __A_P6
//  665 
//  666 
//  667 /******************************************************************************
//  668 Function Name:       RdrvLED1_OFF
//  669 Parameters:          none.
//  670 Return value:        none.
//  671 Description:         Turn off LED1.
//  672 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon0
          CFI Function _RdrvLED1_OFF
          CFI NoCalls
        CODE
//  673 void
//  674 RdrvLED1_OFF(void)
//  675 {
_RdrvLED1_OFF:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  676     PORT_LED1_PORT = PORT_HI;   /* LED1 off */
        SET1      S:0xFFF06.1        ;; 2 cycles
//  677 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
        REQUIRE __A_P6
//  678 
//  679 
//  680 /******************************************************************************
//  681 Function Name:       RdrvLED2_ON
//  682 Parameters:          none.
//  683 Return value:        none.
//  684 Description:         Turn on LED2.
//  685 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon0
          CFI Function _RdrvLED2_ON
          CFI NoCalls
        CODE
//  686 void
//  687 RdrvLED2_ON(void)
//  688 {
_RdrvLED2_ON:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  689     PORT_LED2_PORT = PORT_LO;   /* LED2 on */
        CLR1      S:0xFFF06.2        ;; 2 cycles
//  690 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
        REQUIRE __A_P6
//  691 
//  692 
//  693 /******************************************************************************
//  694 Function Name:       RdrvLED2_OFF
//  695 Parameters:          none.
//  696 Return value:        none.
//  697 Description:         Turn off LED2.
//  698 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock18 Using cfiCommon0
          CFI Function _RdrvLED2_OFF
          CFI NoCalls
        CODE
//  699 void
//  700 RdrvLED2_OFF(void)
//  701 {
_RdrvLED2_OFF:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  702     PORT_LED2_PORT = PORT_HI;   /* LED2 off */
        SET1      S:0xFFF06.2        ;; 2 cycles
//  703 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock18
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
        REQUIRE __A_P6
//  704 
//  705 
//  706 /******************************************************************************
//  707 Function Name:       RdrvLED0_GetCondition
//  708 Parameters:          none.
//  709 Return value:        1 : On
//  710                      0 : Off
//  711 Description:         Get LED0 condition.
//  712 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock19 Using cfiCommon0
          CFI Function _RdrvLED0_GetCondition
          CFI NoCalls
        CODE
//  713 unsigned char
//  714 RdrvLED0_GetCondition(void)
//  715 {
_RdrvLED0_GetCondition:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  716     unsigned char   ret;
//  717 
//  718     ret = 0;
        CLRB      A                  ;; 1 cycle
//  719 
//  720     if(PORT_LED0_PORT == PORT_LO)
        BT        S:0xFFF06.0, ??RdrvFREQHOPPTIMER_Stop_7  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  721     {
//  722         ret = 0x01;
        ONEB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  723     }
//  724 
//  725     return(ret);
??RdrvFREQHOPPTIMER_Stop_7:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock19
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 13 cycles
        REQUIRE __A_P6
//  726 }
//  727 
//  728 
//  729 /******************************************************************************
//  730 Function Name:       RdrvLED1_GetCondition
//  731 Parameters:          none.
//  732 Return value:        1 : On
//  733                      0 : Off
//  734 Description:         Get LED1 condition.
//  735 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock20 Using cfiCommon0
          CFI Function _RdrvLED1_GetCondition
          CFI NoCalls
        CODE
//  736 unsigned char
//  737 RdrvLED1_GetCondition(void)
//  738 {
_RdrvLED1_GetCondition:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  739     unsigned char   ret;
//  740 
//  741     ret = 0;
        CLRB      A                  ;; 1 cycle
//  742 
//  743     if(PORT_LED1_PORT == PORT_LO)
        BT        S:0xFFF06.1, ??RdrvFREQHOPPTIMER_Stop_8  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  744     {
//  745         ret = 0x01;
        ONEB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  746     }
//  747 
//  748     return(ret);
??RdrvFREQHOPPTIMER_Stop_8:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock20
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 13 cycles
        REQUIRE __A_P6
//  749 }
//  750 
//  751 
//  752 /******************************************************************************
//  753 Function Name:       RdrvLED2_GetCondition
//  754 Parameters:          none.
//  755 Return value:        1 : On
//  756                      0 : Off
//  757 Description:         Get LED2 condition.
//  758 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock21 Using cfiCommon0
          CFI Function _RdrvLED2_GetCondition
          CFI NoCalls
        CODE
//  759 unsigned char
//  760 RdrvLED2_GetCondition(void)
//  761 {
_RdrvLED2_GetCondition:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  762     unsigned char   ret;
//  763 
//  764     ret = 0;
        CLRB      A                  ;; 1 cycle
//  765 
//  766     if(PORT_LED2_PORT == PORT_LO)
        BT        S:0xFFF06.2, ??RdrvFREQHOPPTIMER_Stop_9  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  767     {
//  768         ret = 0x01;
        ONEB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  769     }
//  770 
//  771     return(ret);
??RdrvFREQHOPPTIMER_Stop_9:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock21
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 13 cycles
        REQUIRE __A_P6
//  772 }
//  773 
//  774 
//  775 /******************************************************************************
//  776 Function Name:       RdrvSwitch0_GetCondition
//  777 Parameters:          none.
//  778 Return value:        1 : On
//  779                      0 : Off
//  780 Description:         Get switch0 condition.
//  781 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock22 Using cfiCommon0
          CFI Function _RdrvSwitch0_GetCondition
          CFI NoCalls
        CODE
//  782 unsigned char
//  783 RdrvSwitch0_GetCondition(void)
//  784 {
_RdrvSwitch0_GetCondition:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  785     unsigned char   ret;
//  786 
//  787     ret = 0;
        CLRB      A                  ;; 1 cycle
//  788     
//  789     /* Read Switch SW1-1 of TK-RLG1H+SB2 board (Device Port P140) */
//  790     if(P14_bit.no0 == PORT_HI)
        BF        S:0xFFF0E.0, ??RdrvFREQHOPPTIMER_Stop_10  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  791     {
//  792         ret = 0x01;
        ONEB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  793     }
//  794 
//  795     return(ret);
??RdrvFREQHOPPTIMER_Stop_10:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock22
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 13 cycles
        REQUIRE __A_P14
//  796 }
//  797 
//  798 #if defined(MCU_R78G1H)

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock23 Using cfiCommon0
          CFI Function _RdrvFREQHOPPTIMER_Initialize
          CFI NoCalls
        CODE
//  799 void RdrvFREQHOPPTIMER_Initialize(void)
//  800 {
_RdrvFREQHOPPTIMER_Initialize:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  801     RTCEN = 1;         /* Enables input clock supply */
        MOVW      HL, #0xF0          ;; 1 cycle
        SET1      [HL].7             ;; 2 cycles
//  802     ITMC = 0;          /* Disable 12-bit interval timer */
        MOVW      0xFFF90, #0x0      ;; 1 cycle
//  803 
//  804     /* Timer settings(12bit intevral timer) for Frequency Hopping */
//  805     /* Mask INTIT interrupt */
//  806     ITMK = 1;       /* INTIT disabled */
        SET1      0xFFFE7.2          ;; 2 cycles
//  807     ITIF = 0;       /* INTIT interrupt flag clear */
        CLR1      0xFFFE3.2          ;; 2 cycles
//  808     
//  809     /* Set INTIT low priority */
//  810     ITPR1 = 1;      /* Level_3 */
        SET1      0xFFFEF.2          ;; 2 cycles
//  811     ITPR0 = 1;
        SET1      0xFFFEB.2          ;; 2 cycles
//  812     
//  813     RTCEN = 1;          /* Enable input clock source */
        SET1      [HL].7             ;; 2 cycles
//  814     OSMC &= (~0x10);    /* The subsystem clock is selected */
        MOV       L, #0xF3           ;; 1 cycle
        MOV       A, [HL]            ;; 1 cycle
        AND       A, #0xEF           ;; 1 cycle
        MOV       [HL], A            ;; 1 cycle
//  815 
//  816     ITMC  = 32;         /* Set interval time, 1/32.768 [kHz] x (32 + 1) = 1.00708 [ms] */
        MOVW      0xFFF90, #0x20     ;; 1 cycle
//  817 
//  818     ITMK = 0;           /* INTIT enabled */
        CLR1      0xFFFE7.2          ;; 2 cycles
//  819  
//  820     /* RTC initialisation for time correction */
//  821     /* Mask INTRTC interrupt */
//  822     RTCMK = 1;          /* INTRTC disabled */
        SET1      0xFFFE7.1          ;; 2 cycles
//  823     RTCIF = 0;          /* INTRTC interrupt flag clear */
        CLR1      0xFFFE3.1          ;; 2 cycles
//  824 
//  825     /* Set INTRTC priority */
//  826     RTCPR1 = 1;     /* Level_2 */
        SET1      0xFFFEF.1          ;; 2 cycles
//  827     RTCPR0 = 0;
        CLR1      0xFFFEB.1          ;; 2 cycles
//  828         
//  829     SEC = 0;
        MOV       0xFFF92, #0x0      ;; 1 cycle
//  830     MIN = 0;
        MOV       0xFFF93, #0x0      ;; 1 cycle
//  831     
//  832     RTCEN = 1;
        SET1      0xF00F0.7          ;; 2 cycles
//  833     RTCC0 = 0x02;       /* Periodic interrupt generation every 1sec */
        MOV       0xFFF9D, #0x2      ;; 1 cycle
//  834     
//  835     RTCMK = 0;          /* INTRTC enabled */
        CLR1      0xFFFE7.1          ;; 2 cycles
//  836    
//  837     /* reset frequency hopping timer interval */ 
//  838     freqHoppTmIntervalMs = 0;
        CLRW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_freqHoppTmIntervalMs)  ;; 1 cycle
        MOVW      ES:_freqHoppTmIntervalMs, AX  ;; 2 cycles
//  839     /* reset RTC clock correction */ 
//  840     clock_correction_rtc = 0;
        MOVW      ES:_freqHoppTmIntervalMs+2, AX  ;; 2 cycles
//  841 
//  842     /* Start timers */
//  843     ITMC  |= 0x8000;     /* Start 12-bit interval timer operation */
        MOVW      AX, 0xFFF90        ;; 1 cycle
        OR        A, #0x80           ;; 1 cycle
        MOVW      0xFFF90, AX        ;; 1 cycle
//  844     RTCC0 |= 0x80;       /* Start RTC timer operation */
        SET1      0xFFF9D.7          ;; 2 cycles
//  845 
//  846 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock23
        ; ------------------------------------- Block: 53 cycles
        ; ------------------------------------- Total: 53 cycles
        REQUIRE __A_PER0
        REQUIRE __A_ITMC
        REQUIRE __A_MK1
        REQUIRE __A_IF1
        REQUIRE __A_PR11
        REQUIRE __A_PR01
        REQUIRE __A_OSMC
        REQUIRE __A_SEC
        REQUIRE __A_MIN
        REQUIRE __A_RTCC0
//  847 #endif
//  848 
//  849 #if defined(MCU_R78G1H)

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock24 Using cfiCommon0
          CFI Function _RdrvFREQHOPPTIMER_Stop
          CFI NoCalls
        CODE
//  850 void RdrvFREQHOPPTIMER_Stop(void)
//  851 {
_RdrvFREQHOPPTIMER_Stop:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  852     ITMC  = 0;     /* Stop 12-bit interval timer operation */
        MOVW      0xFFF90, #0x0      ;; 1 cycle
//  853     RTCC0 = 0;     /* Stop RTC timer operation */
        MOV       0xFFF9D, #0x0      ;; 1 cycle
//  854 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock24
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 8 cycles
        REQUIRE __A_ITMC
        REQUIRE __A_RTCC0

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//  855 #endif
//  856 
//  857 #if R_MODEM_SERVER
//  858 #if defined(MCU_R78G1H)
//  859 void RdrvSendOSMsg_Initialize(void)
//  860 {
//  861     /* Use INTP7 interrupt to trigger OS messages from UART interrupt
//  862        at a lower interrupt priority level */
//  863       
//  864     /* Mask INTP7 interrupt */
//  865     PMK7 = 1;       /* INTP7 disabled */
//  866     PIF7 = 0;       /* INTP7 interrupt request flag clear */
//  867     
//  868     /* Set interrupt priority to Level 1 */
//  869     PPR07 = 1;
//  870     PPR17 = 0;
//  871 
//  872     /* Enable INTP7 interrupt */
//  873     PMK7 = 0;       /* INTP7 enable */
//  874 }
//  875 #endif
//  876 
//  877 #if defined(MCU_R78G1H)
//  878 void RdrvSendOSMsg_Trigger(void)
//  879 {
//  880     /* Trigger INTP7 interrupt */
//  881     PIF7 = 1;       /* INTP7 interrupt request flag set */
//  882 }
//  883 #endif
//  884 
//  885 /******************************************************************************
//  886 Function Name:       RdrvSendOSMsgTrg_Int
//  887 Parameters:          none.
//  888 Return value:        none.
//  889 Description:         Use INTP7 interrupt to trigger OS messages from UART
//  890                      interrupt at a lower interrupt priority level 
//  891 ******************************************************************************/
//  892 extern void r_modem_nwk_cb_trigger_os_msg(void);
//  893 
//  894 #if defined (__ICCRL78__)
//  895 #pragma vector = INTP7_vect
//  896 __interrupt void RdrvSendOSMsgTrg_Int(void)
//  897 #else
//  898 void RdrvSendOSMsgTrg_Int(void)
//  899 #endif
//  900 {
//  901     __enable_interrupt();           // Enable Interrupts
//  902 
//  903     r_modem_nwk_cb_trigger_os_msg();
//  904 }
//  905 
//  906 #endif /* R_MODEM_SERVER */
//  907 
//  908 
//  909 
//  910 
//  911 
// 
//    53 bytes in section .bss.noinit   (abs)
// 1 728 bytes in section .bssf
//     3 bytes in section .sbss.noinit  (abs)
//   318 bytes in section .text
//   513 bytes in section .textf
// 
//   318 bytes of CODE    memory
// 1 728 bytes of DATA    memory (+ 56 bytes shared)
//   513 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
