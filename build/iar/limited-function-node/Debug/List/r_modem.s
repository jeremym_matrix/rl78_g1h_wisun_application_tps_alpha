///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:58
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\src\r_modem.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3A66.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\src\r_modem.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\r_modem.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_file_descriptor", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN ?L_XOR_L03
        EXTERN ?UL_RSH_L03
        EXTERN _UInt32ToArr

        PUBLIC _r_modem_msg_tx
        PUBLIC _r_modem_rx
        PUBLIC _r_modem_rx_start
        PUBLIC _r_modem_tx
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\src\r_modem.c
//    1 #include "r_modem.h"
//    2 
//    3 #include "r_modem_crc_table.h"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static uint32_t const r_modem_crc_table[256]
_r_modem_crc_table:
        DATA32
        DD 0, 79764919, 159529838, 222504665, 319059676, 398814059, 445009330
        DD 507990021, 638119352, 583659535, 797628118, 726387553, 890018660
        DD 835552979, 1015980042, 944750013, 1276238704, 1221641927, 1167319070
        DD 1095957929, 1595256236, 1540665371, 1452775106, 1381403509
        DD 1780037320, 1859660671, 1671105958, 1733955601, 2031960084
        DD 2111593891, 1889500026, 1952343757, 2552477408, 2632100695
        DD 2443283854, 2506133561, 2334638140, 2414271883, 2191915858
        DD 2254759653, 3190512472, 3135915759, 3081330742, 3009969537
        DD 2905550212, 2850959411, 2762807018, 2691435357, 3560074640
        DD 3505614887, 3719321342, 3648080713, 3342211916, 3287746299
        DD 3467911202, 3396681109, 4063920168, 4143685023, 4223187782
        DD 4286162673, 3779000052, 3858754371, 3904687514, 3967668269
        DD 881225847, 809987520, 1023691545, 969234094, 662832811, 591600412
        DD 771767749, 717299826, 311336399, 374308984, 453813921, 533576470
        DD 25881363, 88864420, 134795389, 214552010, 2023205639, 2086057648
        DD 1897238633, 1976864222, 1804852699, 1867694188, 1645340341
        DD 1724971778, 1587496639, 1516133128, 1461550545, 1406951526
        DD 1302016099, 1230646740, 1142491917, 1087903418, 2896545431
        DD 2825181984, 2770861561, 2716262478, 3215044683, 3143675388
        DD 3055782693, 3001194130, 2326604591, 2389456536, 2200899649
        DD 2280525302, 2578013683, 2640855108, 2418763421, 2498394922
        DD 3769900519, 3832873040, 3912640137, 3992402750, 4088425275
        DD 4151408268, 4197601365, 4277358050, 3334271071, 3263032808
        DD 3476998961, 3422541446, 3585640067, 3514407732, 3694837229
        DD 3640369242, 1762451694, 1842216281, 1619975040, 1682949687
        DD 2047383090, 2127137669, 1938468188, 2001449195, 1325665622
        DD 1271206113, 1183200824, 1111960463, 1543535498, 1489069629
        DD 1434599652, 1363369299, 622672798, 568075817, 748617968, 677256519
        DD 907627842, 853037301, 1067152940, 995781531, 51762726, 131386257
        DD 177728840, 240578815, 269590778, 349224269, 429104020, 491947555
        DD 4046411278, 4126034873, 4172115296, 4234965207, 3794477266
        DD 3874110821, 3953728444, 4016571915, 3609705398, 3555108353
        DD 3735388376, 3664026991, 3290680682, 3236090077, 3449943556
        DD 3378572211, 3174993278, 3120533705, 3032266256, 2961025959
        DD 2923101090, 2868635157, 2813903052, 2742672763, 2604032198
        DD 2683796849, 2461293480, 2524268063, 2284983834, 2364738477
        DD 2175806836, 2238787779, 1569362073, 1498123566, 1409854455
        DD 1355396672, 1317987909, 1246755826, 1192025387, 1137557660
        DD 2072149281, 2135122070, 1912620623, 1992383480, 1753615357
        DD 1816598090, 1627664531, 1707420964, 295390185, 358241886, 404320391
        DD 483945776, 43990325, 106832002, 186451547, 266083308, 932423249
        DD 861060070, 1041341759, 986742920, 613929101, 542559546, 756411363
        DD 701822548, 3316196985, 3244833742, 3425377559, 3370778784
        DD 3601682597, 3530312978, 3744426955, 3689838204, 3819031489
        DD 3881883254, 3928223919, 4007849240, 4037393693, 4100235434
        DD 4180117107, 4259748804, 2310601993, 2373574846, 2151335527
        DD 2231098320, 2596047829, 2659030626, 2470359227, 2550115596
        DD 2947551409, 2876312838, 2788305887, 2733848168, 3165939309
        DD 3094707162, 3040238851, 2985771188
//    4 #include "r_byte_swap.h"
//    5 #include "r_impl_utils.h"
//    6 
//    7 #include <string.h>
//    8 #include <stdio.h>
//    9 
//   10 // we have no logging but this allows the user to provide a suitable function for debugging
//   11 #ifdef R_DEV_MODEM_PRINTLN
//   12 #include "r_header_utils.h"
//   13 void R_DEV_MODEM_PRINTLN(const char* fmt, ...) R_HEADER_UTILS_PRINTF_LIKE(1, 2);
//   14 #define ERR R_DEV_MODEM_PRINTLN
//   15 #else
//   16 #define ERR(...) do {} while (0);
//   17 #endif
//   18 
//   19 static uint32_t crc_update(uint32_t crc, uint8_t byte)
//   20 {
//   21     uint32_t idx = ((crc >> 24u) ^ byte) & 0xffu;
//   22     return (r_modem_crc_table[idx] ^ (crc << 8u)) & 0xffffffffu;
//   23 }
//   24 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _rx_buf_append
        CODE
//   25 static void rx_buf_append(uint8_t byte, r_modem_ctx* ctx)
//   26 {
_rx_buf_append:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        MOV       E, A               ;; 1 cycle
//   27     ctx->rx_buf[ctx->rx_buf_size++] = byte;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, E               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        INCW      BC                 ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//   28     ctx->rx_crc = crc_update(ctx->rx_crc, byte);
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        SHLW      BC, 0x8            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        SHRW      AX, 0x8            ;; 1 cycle
        ADDW      AX, BC             ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       D, #0x0            ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       E, #0x18           ;; 1 cycle
          CFI FunCall ?UL_RSH_L03
        CALL      N:?UL_RSH_L03      ;; 3 cycles
        XCH       A, X               ;; 1 cycle
        XOR       A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_r_modem_crc_table)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_r_modem_crc_table)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_XOR_L03
        CALL      N:?L_XOR_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//   29 }
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 102 cycles
        ; ------------------------------------- Total: 102 cycles
//   30 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _r_modem_rx
        CODE
//   31 void r_modem_rx(uint8_t byte, r_modem_ctx* ctx)
//   32 {
_r_modem_rx:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOV       B, A               ;; 1 cycle
//   33     if (ctx->rx_disabled)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??r_modem_msg_tx_0  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
//   34     {
//   35         return;  // still processing previous message -> drop
//   36     }
//   37 
//   38     if (ctx->rx_buf_size >= ctx->rx_buf_capacity)
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        CMPW      AX, DE             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BNH       ??r_modem_msg_tx_1  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
//   39     {
//   40         ERR("r_modem_rx: error buffer too small");
//   41         ctx->rx_error = 1;
//   42         return;
//   43     }
//   44 
//   45     if (ctx->rx_escaped)
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        BZ        ??r_modem_msg_tx_2  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
//   46     {
//   47         rx_buf_append(byte ^ ESCAPE_XOR, ctx);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XOR       A, #0x20           ;; 1 cycle
          CFI FunCall _rx_buf_append
        CALL      F:_rx_buf_append   ;; 3 cycles
//   48         ctx->rx_escaped = 0;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        BR        S:??r_modem_msg_tx_3  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
//   49         return;
//   50     }
//   51 
//   52     if (byte == ESCAPE_BYTE)
??r_modem_msg_tx_2:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x7D           ;; 1 cycle
        BNZ       ??r_modem_msg_tx_4  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//   53     {
//   54         if (ctx->rx_escaped)
//   55         {
//   56             ERR("r_modem_rx: error ESCAPE follows ESCAPE");
//   57             ctx->rx_error = 1;
//   58             return;
//   59         }
//   60         ctx->rx_escaped = 1;
        BR        S:??r_modem_msg_tx_5  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//   61         return;
//   62     }
//   63 
//   64     if (byte == RECORD_DELIMITER && ctx->rx_escaped)
//   65     {
//   66         ERR("r_modem_rx: error RECORD_DELIMITER follows ESCAPE");
//   67         ctx->rx_error = 1;
??r_modem_msg_tx_1:
        ADDW      AX, #0xF           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??r_modem_msg_tx_5:
        ONEB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??r_modem_msg_tx_3:
        MOV       ES:[HL], A         ;; 2 cycles
//   68         return;
        BR        S:??r_modem_msg_tx_0  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//   69     }
//   70 
//   71     if (byte != RECORD_DELIMITER)
??r_modem_msg_tx_4:
        CMP       A, #0x7E           ;; 1 cycle
        BZ        ??r_modem_msg_tx_6  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//   72     {
//   73         rx_buf_append(byte, ctx);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall _rx_buf_append
        CALL      F:_rx_buf_append   ;; 3 cycles
//   74         return;
        BR        S:??r_modem_msg_tx_0  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
//   75     }
//   76 
//   77     // delimiter handling
//   78     int_fast8_t error = ctx->rx_error;
??r_modem_msg_tx_6:
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
//   79     if (ctx->rx_buf_size < sizeof(r_modem_header_t) + CRC_SIZE)
        XCHW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x8           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        BC        ??r_modem_msg_tx_7  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//   80     {
//   81         if (ctx->rx_buf_size != 0)
//   82         {
//   83             ERR("r_modem_rx: error frame too small: %u", ctx->rx_buf_size);
//   84         }
//   85         error = 1;
//   86     }
//   87 
//   88     if (!error)
        CMP0      X                  ;; 1 cycle
        BNZ       ??r_modem_msg_tx_7  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//   89     {
//   90         // we've calculated the CRC over the buffer including the sent CRC -> result must be zero
//   91         if (ctx->rx_crc != 0)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??r_modem_msg_tx_8  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
//   92         {
//   93             ERR("r_modem_rx: error in CRC");
//   94             error = 1;
//   95         }
//   96     }
//   97 
//   98     if (error)
//   99     {
//  100         // drop frame
//  101         r_modem_rx_start(ctx->rx_buf, ctx->rx_buf_capacity, ctx);
??r_modem_msg_tx_7:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _r_modem_rx_start
        CALL      F:_r_modem_rx_start  ;; 3 cycles
//  102         return;
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        BR        S:??r_modem_msg_tx_0  ;; 3 cycles
        ; ------------------------------------- Block: 22 cycles
//  103     }
//  104 
//  105     // buffer is ready for processing
//  106     ctx->rx_buf_size -= CRC_SIZE;
??r_modem_msg_tx_8:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  107     ctx->rx_disabled = 1;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ONEB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  108     ctx->rx_cb(ctx->rx_buf_size);
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall
        CALL      BC                 ;; 3 cycles
//  109 }
        ; ------------------------------------- Block: 30 cycles
??r_modem_msg_tx_0:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 184 cycles
//  110 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _r_modem_rx_start
          CFI NoCalls
        CODE
//  111 void r_modem_rx_start(uint8_t* buf, uint16_t size, r_modem_ctx* ctx)
//  112 {
_r_modem_rx_start:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  113     ctx->rx_disabled = 0;
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  114     ctx->rx_error = 0;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xF           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  115     ctx->rx_escaped = 0;
        DECW      HL                 ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  116     ctx->rx_crc = 0;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  117     ctx->rx_buf_size = 0;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  118     ctx->rx_buf = buf;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+8
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  119     ctx->rx_buf_capacity = size;
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  120 }
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 67 cycles
        ; ------------------------------------- Total: 67 cycles
//  121 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon1
          CFI Function _r_modem_tx
        CODE
//  122 uint32_t r_modem_tx(const void* data, size_t size, uint32_t crc, r_modem_ctx* ctx)
//  123 {
_r_modem_tx:
        ; * Stack frame (at entry) *
        ; Param size: 8
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 10
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+14
//  124     for (size_t i = 0; i < size; i++)
        MOVW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??r_modem_msg_tx_9  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  125     {
//  126         uint8_t byte = ((const uint8_t*)data)[i];
??r_modem_tx_0:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
//  127         crc = crc_update(crc, byte);
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        SHLW      BC, 0x8            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        SHRW      AX, 0x8            ;; 1 cycle
        ADDW      AX, BC             ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOV       E, #0x18           ;; 1 cycle
          CFI FunCall ?UL_RSH_L03
        CALL      N:?UL_RSH_L03      ;; 3 cycles
        XCH       A, X               ;; 1 cycle
        XOR       A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_r_modem_crc_table)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_r_modem_crc_table)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?L_XOR_L03
        CALL      N:?L_XOR_L03       ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x10], AX      ;; 1 cycle
//  128         if (byte == RECORD_DELIMITER || byte == ESCAPE_BYTE)
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x7E           ;; 1 cycle
        BZ        ??r_modem_msg_tx_10  ;; 4 cycles
        ; ------------------------------------- Block: 56 cycles
        CMP       A, #0x7D           ;; 1 cycle
        BNZ       ??r_modem_msg_tx_11  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  129         {
//  130             ctx->tx_cb(ESCAPE_BYTE);
??r_modem_msg_tx_10:
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x7D           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
//  131             ctx->tx_cb(byte ^ ESCAPE_XOR);
        MOV       A, [SP]            ;; 1 cycle
        XOR       A, #0x20           ;; 1 cycle
        ; ------------------------------------- Block: 17 cycles
//  132         }
//  133         else
//  134         {
//  135             ctx->tx_cb(byte);
??r_modem_msg_tx_11:
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
//  136         }
//  137     }
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??r_modem_tx_0   ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
//  138     return crc;
??r_modem_msg_tx_9:
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 128 cycles
//  139 }
//  140 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon2
          CFI Function _r_modem_msg_tx
        CODE
//  141 void r_modem_msg_tx(uint8_t type, uint8_t id, uint8_t cmd, const void* data, size_t size, r_modem_ctx* ctx)
//  142 {
_r_modem_msg_tx:
        ; * Stack frame (at entry) *
        ; Param size: 6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 24
        SUBW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+28
//  143     ctx->tx_cb(RECORD_DELIMITER);
        MOV       A, [SP+0x20]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x7E           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
//  144     uint32_t crc = 0;
//  145     r_modem_header_t header = { 0, type, id, cmd};
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOV       [SP+0x05], A       ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
        MOV       A, [SP+0x13]       ;; 1 cycle
        MOV       [SP+0x05], A       ;; 1 cycle
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       [SP+0x07], A       ;; 1 cycle
//  146     crc = r_modem_tx(&header, sizeof(header), crc, ctx);
        MOV       A, [SP+0x20]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      BC, #0x4           ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_modem_tx
        CALL      F:_r_modem_tx      ;; 3 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
//  147     crc = r_modem_tx(data, size, crc, ctx);
        MOV       A, [SP+0x28]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+44
        MOVW      AX, [SP+0x2C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x26]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _r_modem_tx
        CALL      F:_r_modem_tx      ;; 3 cycles
        MOVW      [SP+0x10], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x12], AX      ;; 1 cycle
//  148     uint8_t crcbuf[CRC_SIZE];
//  149     UInt32ToArr(crc, crcbuf);
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+28
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _UInt32ToArr
        CALL      F:_UInt32ToArr     ;; 3 cycles
//  150     r_modem_tx(crcbuf, sizeof(crcbuf), crc, ctx);
        MOV       A, [SP+0x24]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      BC, #0x4           ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _r_modem_tx
        CALL      F:_r_modem_tx      ;; 3 cycles
//  151     ctx->tx_cb(RECORD_DELIMITER);
        MOV       A, [SP+0x2C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x2A]      ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, #0x7E           ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
//  152 }
        ADDW      SP, #0x24          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 138 cycles
        ; ------------------------------------- Total: 138 cycles

        SECTION `.constf`:FARCODE:REORDER:NOROOT(0)
        DATA8
        DB 0, 0, 0, 0

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
// 
// 1 028 bytes in section .constf
//   823 bytes in section .textf
// 
// 1 851 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
