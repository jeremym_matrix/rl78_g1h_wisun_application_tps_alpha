///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:56
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\phy_fan.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3394.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\phy_fan.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\phy_fan.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _RpCb
        EXTERN _RpRegRead
        EXTERN ___get_psw
        EXTERN ___set_psw

        PUBLIC _RpCalcLqiCallback
        PUBLIC _RpGetPhyStatus
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\phy_fan.c
//    1 /*******************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only
//    4  * intended for use with Renesas products. No other uses are authorized.
//    5  * This software is owned by Renesas Electronics Corporation and is protected under
//    6  * all applicable laws, including copyright laws.
//    7  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
//    8  * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
//    9  * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//   10  * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
//   11  * DISCLAIMED.
//   12  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   13  * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   14  * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
//   15  * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
//   16  * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   17  * Renesas reserves the right, without notice, to make changes to this
//   18  * software and to discontinue the availability of this software.
//   19  * By using this software, you agree to the additional terms and
//   20  * conditions found by accessing the following link:
//   21  * http://www.renesas.com/disclaimer
//   22  *******************************************************************************/
//   23 /*******************************************************************************
//   24  * file name	: phy_fan.c
//   25  * description	: 
//   26  *******************************************************************************
//   27  * Copyright (C) 2018 Renesas Electronics Corporation.
//   28  ******************************************************************************/
//   29 #include "phy.h"
//   30 #include "phy_def.h"
//   31 
//   32 extern RP_PHY_CB RpCb;
//   33 
//   34 #ifdef RP_WISUN_FAN_STACK	// Req.No8
//   35 /******************************************************************************
//   36 Function Name:       RpGetPhyStatus
//   37 Parameters:          none
//   38 Return value:        RP_RX_ON:already RX_ON
//   39                      RP_TRX_OFF:already TRX_OFF of Rxbuf full
//   40                      RP_BUSY_RX:No status setting because now RX_ON or CCA or ED
//   41                      RP_BUSY_TX:No status setting because now TX_ON
//   42                      RP_BUSY_LOWPOWER:Now Lowpower mode
//   43 Description:         Checks and returns current status of PHY driver.
//   44 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _RpGetPhyStatus
        CODE
//   45 int16_t
//   46 RpGetPhyStatus(void)
//   47 {
_RpGetPhyStatus:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
//   48 	uint16_t curStat;
//   49 	int16_t	rtnVal = RP_SUCCESS;
//   50 #if defined(__RX)
//   51 	uint32_t bkupPsw;
//   52 #elif defined(__CCRL__) || defined (__ICCRL78__)
//   53 	uint8_t  bkupPsw;
//   54 #endif
//   55 
//   56 #if defined(__RX) || defined(__CCRL__) || defined (__ICCRL78__)
//   57 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//   58 #else
//   59 	RP_PHY_DI();
//   60 #endif
//   61 
//   62 	// copy current status
//   63 	curStat = RpCb.status;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
//   64 	if (curStat & RP_PHY_STAT_LOWPOWER)
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].5, ??RpCalcLqiCallback_0  ;; 5 cycles
        ; ------------------------------------- Block: 21 cycles
//   65 	{
//   66 		rtnVal = RP_BUSY_LOWPOWER;
        MOVW      AX, #0xF3          ;; 1 cycle
        BR        S:??RpCalcLqiCallback_1  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//   67 	}
//   68 	else if (curStat & RP_PHY_STAT_TRX_OFF)
??RpCalcLqiCallback_0:
        DECW      HL                 ;; 1 cycle
        BF        [HL].4, ??RpCalcLqiCallback_2  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//   69 	{
//   70 		rtnVal = RP_TRX_OFF;
        MOVW      AX, #0x8           ;; 1 cycle
        BR        S:??RpCalcLqiCallback_1  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//   71 	}
//   72 	else if (curStat & RP_PHY_STAT_TX)
??RpCalcLqiCallback_2:
        BF        [HL].1, ??RpCalcLqiCallback_3  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//   73 	{
//   74 		rtnVal = RP_BUSY_TX;
        MOVW      AX, #0x2           ;; 1 cycle
        BR        S:??RpCalcLqiCallback_1  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//   75 	}
//   76 	else if (curStat & RP_PHY_STAT_RX)
??RpCalcLqiCallback_3:
        BF        [HL].0, ??RpCalcLqiCallback_4  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//   77 	{
//   78 		if (((RpCb.pib.macAddressFilter1Ena == RP_TRUE) || (RpCb.pib.macAddressFilter2Ena == RP_TRUE)) &&
//   79 				RP_PHY_STAT_RX_BUSY())
        CMP       ES:_RpCb+176, #0x1  ;; 2 cycles
        BZ        ??RpCalcLqiCallback_5  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       ES:_RpCb+192, #0x1  ;; 2 cycles
        BNZ       ??RpCalcLqiCallback_6  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpCalcLqiCallback_5:
        MOVW      AX, #0x330         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].3, ??RpCalcLqiCallback_4  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, #0x330         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].1, ??RpCalcLqiCallback_4  ;; 5 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CMP0      ES:_RpCb+142       ;; 2 cycles
        BNZ       ??RpCalcLqiCallback_4  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//   80 		{
//   81 			rtnVal = RP_BUSY_RX;
//   82 		}
//   83 		else
//   84 		{
//   85 			rtnVal = RP_RX_ON;
??RpCalcLqiCallback_6:
        MOVW      AX, #0x6           ;; 1 cycle
        BR        S:??RpCalcLqiCallback_1  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//   86 		}
//   87 	}
//   88 	else
//   89 	{
//   90 		// CCA or ED or RX
//   91 		rtnVal = RP_BUSY_RX;
??RpCalcLqiCallback_4:
        ONEW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpCalcLqiCallback_1:
        MOVW      [SP], AX           ;; 1 cycle
//   92 	}
//   93 
//   94 #if defined(__RX) || defined(__CCRL__) || defined (__ICCRL78__)
//   95 	RP_PHY_EI(bkupPsw);
        MOV       A, [SP+0x02]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//   96 #else
//   97 	RP_PHY_EI();
//   98 #endif
//   99 
//  100 	return (rtnVal);
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 110 cycles
//  101 }
//  102 #endif
//  103 
//  104 #ifdef RP_WISUN_FAN_STACK	// Req.No9
//  105 /******************************************************************************
//  106  * callback program
//  107  *****************************************************************************/
//  108 #define RP_EDFLOORLVL		(-75)
//  109 #define RP_EDSATURLVL		(RP_EDFLOORLVL + (256/4))
//  110 #define RP_RSSIFLOORLVL		(-174)
//  111 #define RP_RSSISATURLVL		(+80)
//  112 
//  113 /******************************************************************************
//  114  *	function Name  : RpCalcLqiCallback
//  115  *	parameters     : rssiEdValue : read value of baseband RSSI
//  116  *				   : rssiEdSelect : RP_TRUE: rssi select
//  117  *				   :                RP_FALSE:ED select
//  118  *	return value   : IEEE802.15.4 LQI or ED format
//  119  *	description    : convert rssirslt to the value which format is specifiled
//  120  *				   : in IEEE802.15.4.
//  121  *****************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon1
          CFI Function _RpCalcLqiCallback
          CFI NoCalls
        CODE
//  122 uint8_t RpCalcLqiCallback(uint16_t rssiEdValue, uint8_t rssiEdSelect)
//  123 {
_RpCalcLqiCallback:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOVW      HL, AX             ;; 1 cycle
//  124 	int16_t schg;
//  125 	uint8_t val;
//  126 
//  127 	schg = (int16_t)rssiEdValue;
//  128 	schg = (int16_t)((schg < 0x0100) ? schg : (schg | 0xfe00));
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8100        ;; 1 cycle
        BC        ??RpCalcLqiCallback_7  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, #0xFE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  129 	if (rssiEdSelect == RP_FALSE)
??RpCalcLqiCallback_7:
        CMP0      C                  ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        BNZ       ??RpCalcLqiCallback_8  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  130 	{
//  131 		if (schg < RP_EDFLOORLVL)
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x7FB5        ;; 1 cycle
        BC        ??RpCalcLqiCallback_9  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  132 		{
//  133 			val = 0x00;
//  134 		}
//  135 		else if (schg >= RP_EDSATURLVL)
        MOVW      AX, HL             ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x7FF5        ;; 1 cycle
        BNC       ??RpCalcLqiCallback_10  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  136 		{
//  137 			val = 0xff;
//  138 		}
//  139 		else // if(schg >= EDFLOORLVL && schg < EDSATURLVL)
//  140 		{
//  141 			schg = (int16_t)(schg - RP_EDFLOORLVL);
//  142 			val = (uint8_t)((int16_t)(schg * 4));
        MOV       A, L               ;; 1 cycle
        ADD       A, #0x4B           ;; 1 cycle
        SHL       A, 0x2             ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 9 cycles
//  143 		}
//  144 	}
//  145 	else // if (rssiSelect == RP_TRUE)
//  146 	{
//  147 		if	(schg <  RP_RSSIFLOORLVL)
??RpCalcLqiCallback_8:
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x7F52        ;; 1 cycle
        BNC       ??RpCalcLqiCallback_11  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  148 		{
//  149 			// floor level 		= RSSIFLOORLVL(dBm)
//  150 			val = 0;
??RpCalcLqiCallback_9:
        CLRB      A                  ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 7 cycles
//  151 		}
//  152 		else if (schg >= RP_RSSISATURLVL)
??RpCalcLqiCallback_11:
        MOVW      AX, HL             ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8050        ;; 1 cycle
        BC        ??RpCalcLqiCallback_12  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  153 		{
//  154 			// saturation level = RSSISATURLVL(dBm)
//  155 			val =  255;
??RpCalcLqiCallback_10:
        MOV       A, #0xFF           ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 7 cycles
//  156 		}
//  157 		else // if(schg >= RSSIFLOORLVL && schg < RSSISATURLVL)
//  158 		{
//  159 			schg = (int16_t)(schg - RP_RSSIFLOORLVL);		// 0 - (RSSISATURLVL - RSSIFLOORLVL)
//  160 			//			val = (uint8_t)((uint8_t)schg << 2);
//  161 			val = (uint8_t) schg;
??RpCalcLqiCallback_12:
        MOV       A, L               ;; 1 cycle
        ADD       A, #0xAE           ;; 1 cycle
//  162 		}
//  163 	}
//  164 
//  165 	return (val);							// min(-98dBm) = 0, max(-34dBm) = 255
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 73 cycles
//  166 }

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//  167 #endif
//  168 
//  169 /*******************************************************************************
//  170  * Copyright (C) 2018 Renesas Electronics Corporation.
//  171  ******************************************************************************/
//  172 
// 
// 186 bytes in section .textf
// 
// 186 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
