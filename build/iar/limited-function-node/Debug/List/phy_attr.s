///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:57
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_attr.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW32B6.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_attr.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\phy_attr.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _RpCb
        EXTERN ?C_SSWITCH_L10
        EXTERN ?SL_RSH_L03
        EXTERN ?UL_CMP_L03
        EXTERN ?UL_RSH_L03
        EXTERN _RpExtChkIdLenGetReq
        EXTERN _RpExtChkIdLenSetReq
        EXTERN _RpGetAgcStartVth
        EXTERN _RpGetAntennaDiversityStartVth
        EXTERN _RpGetAntennaSwitchingTime
        EXTERN _RpLog_Event
        EXTERN _RpMemcpy
        EXTERN _RpRegRead
        EXTERN _RpRegWrite
        EXTERN _RpSetAckReplyTimeVal
        EXTERN _RpSetAckWaitDurationVal
        EXTERN _RpSetAdrfAndAutoAckVal
        EXTERN _RpSetAgcStartVth
        EXTERN _RpSetAgcWaitGain
        EXTERN _RpSetAntennaDiversityStartVth
        EXTERN _RpSetAntennaDiversityVal
        EXTERN _RpSetAntennaSelectTxVal
        EXTERN _RpSetAntennaSwitchEnaTiming
        EXTERN _RpSetAntennaSwitchVal
        EXTERN _RpSetBackOffSeedVal
        EXTERN _RpSetCcaDurationVal
        EXTERN _RpSetCcaEdBandwidth
        EXTERN _RpSetCcaVthVal
        EXTERN _RpSetChannelVal
        EXTERN _RpSetChannelsSupportedPageAndVal
        EXTERN _RpSetCsmaBackoffPeriod
        EXTERN _RpSetFcsLengthVal
        EXTERN _RpSetFecVal
        EXTERN _RpSetFskOpeModeVal
        EXTERN _RpSetFskScramblePsduVal
        EXTERN _RpSetGpio0Setting
        EXTERN _RpSetGpio3Setting
        EXTERN _RpSetLvlVthVal
        EXTERN _RpSetMacTxLimitMode
        EXTERN _RpSetMaxBeVal
        EXTERN _RpSetMaxCsmaBackoffVal
        EXTERN _RpSetMrFskSfdVal
        EXTERN _RpSetPreamble4ByteRxMode
        EXTERN _RpSetPreambleLengthVal
        EXTERN _RpSetSfdDetectionExtend
        EXTERN _RpSetSpecificModeVal
        EXTERN _RpSetTxPowerVal
        EXTERN ___get_psw
        EXTERN ___set_psw

        PUBLIC _RpGetPibReq
        PUBLIC _RpPlmeGetReq
        PUBLIC _RpPlmeSetReq
        PUBLIC _RpSetAttr_phyAckWithCca
        PUBLIC _RpSetAttr_phyFrequencyOffset
        PUBLIC _RpSetPibReq
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_attr.c
//    1 /***********************************************************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
//    4  * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
//    5  * applicable laws, including copyright laws. 
//    6  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
//    7  * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
//    8  * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//    9  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
//   10  * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
//   11  * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
//   12  * DAMAGES.
//   13  * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
//   14  * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
//   15  * following link:
//   16  * http://www.renesas.com/disclaimer 
//   17  **********************************************************************************************************************/
//   18 /***********************************************************************************************************************
//   19  * file name	: phy_attr.c
//   20  * description	: Attribute control function
//   21  ***********************************************************************************************************************
//   22  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
//   23  **********************************************************************************************************************/
//   24 /***************************************************************************************************************
//   25  * includes
//   26  **************************************************************************************************************/
//   27 #include "phy.h"
//   28 #include "phy_def.h"
//   29 
//   30 #if defined(__arm)
//   31 	#include "cpx3.h"
//   32 	#include "phy_drv.h"
//   33 #endif
//   34 
//   35 /***************************************************************************************************************
//   36  * extern definitions
//   37  **************************************************************************************************************/
//   38 extern RP_PHY_CB RpCb;
//   39 
//   40 /***************************************************************************************************************
//   41  * macro definitions
//   42  **************************************************************************************************************/
//   43 #define RP_MinValue_phyBackOffSeed	(0x01u)
//   44 #define RP_MinValue_macMaxBe		(0x03u)	//RP_MINVAL_MAXBE
//   45 
//   46 #define RP_NUM_FREQ_BAND		(10u)
//   47 #define RP_MAX_FSK_OPEMODE		(7u)
//   48 
//   49 /***************************************************************************************************************
//   50  * typedef definitions
//   51  **************************************************************************************************************/
//   52 typedef struct {
//   53 	uint8_t maxChannel[RP_MAX_FSK_OPEMODE];
//   54 } r_maxChannelTbl_t;
//   55 
//   56 /***************************************************************************************************************
//   57  * static function prototypes
//   58  **************************************************************************************************************/
//   59 #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
//   60 	static int16_t RpPlmeSetReq( uint8_t attrId, void RP_FAR *p_attrValue );
//   61 #endif // #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
//   62 static uint8_t RpSetAttr_phyCurrentChannel( uint8_t attrValue );
//   63 #ifndef RP_WISUN_FAN_STACK
//   64 static uint8_t RpCheckChannelsSupported( uint8_t channel );
//   65 static uint8_t RpConvertChannelsSupportedPage( uint8_t channel );
//   66 #endif // ifndef RP_WISUN_FAN_STACK
//   67 static uint8_t RpSetAttr_phyChannelsSupportedPage( uint8_t attrValue );
//   68 static uint8_t RpSetAttr_phyCcaVth( uint16_t attrValue );
//   69 static uint8_t RpSetAttr_phyTransmitPower( uint8_t attrValue );
//   70 static uint8_t RpSetAttr_phyFskFecRxEna( uint8_t attrValue );
//   71 static uint8_t RpSetAttr_phyFskFecTxEna( uint8_t attrValue );
//   72 static uint8_t RpSetAttr_phyFskFecScheme( uint8_t attrValue );
//   73 static uint8_t RpSetAttr_phyLvlFltrVth( uint16_t attrValue );
//   74 static uint8_t RpSetAttr_phyBackOffSeed( uint8_t attrValue );
//   75 static uint8_t RpSetAttr_phyCrcErrorUpMsg( uint8_t attrValue );
//   76 static uint8_t RpSetAttr_macAddressFilter1Ena( uint8_t attrValue );
//   77 static uint8_t RpSetAttr_macShortAddress1( uint16_t attrValue );
//   78 static uint8_t RpSetAttr_macExtendedAddress1( void RP_FAR *p_attrValue );
//   79 static uint8_t RpSetAttr_macPanId1( uint16_t attrValue );
//   80 static uint8_t RpSetAttr_macPanCoord1( uint8_t attrValue );
//   81 static uint8_t RpSetAttr_macFramePend1( uint8_t attrValue );
//   82 static uint8_t RpSetAttr_macAddressFilter2Ena( uint8_t attrValue );
//   83 static uint8_t RpSetAttr_macShortAddress2( uint16_t attrValue );
//   84 static uint8_t RpSetAttr_macExtendedAddress2( void RP_FAR *p_attrValue );
//   85 static uint8_t RpSetAttr_macPanId2( uint16_t attrValue );
//   86 static uint8_t RpSetAttr_macPanCoord2( uint8_t attrValue );
//   87 static uint8_t RpSetAttr_macFramePend2( uint8_t attrValue );
//   88 static uint8_t RpSetAttr_macMaxCsmaBackOff( uint8_t attrValue );
//   89 static uint8_t RpSetAttr_macMinBe( uint8_t attrValue );
//   90 static uint8_t RpSetAttr_macMaxBe( uint8_t attrValue );
//   91 static uint8_t RpSetAttr_macMaxFrameRetries( uint8_t attrValue );
//   92 static uint8_t RpSetAttr_macCsmaBackoffPeriod( uint16_t attrValue );
//   93 static uint8_t RpSetAttr_phyCcaDuration( uint16_t attrValue );
//   94 static uint8_t RpSetAttr_phyMrFskSfd( uint8_t attrValue );
//   95 static uint8_t RpSetAttr_phyFskPreambleLength( uint16_t attrValue );
//   96 static uint8_t RpSetAttr_phyFskScramblePsdu( uint8_t attrValue );
//   97 static uint8_t RpSetAttr_phyFskOpeMode( uint8_t attrValue );
//   98 static uint8_t RpSetAttr_phyFcsLength( uint8_t attrValue );
//   99 static uint8_t RpSetAttr_phyAckReplyTime( uint16_t attrValue );
//  100 static uint8_t RpSetAttr_phyAckWaitDuration( uint16_t attrValue );
//  101 static uint8_t RpSetAttr_phyProfileSpecificMode( uint8_t attrValue );
//  102 static uint8_t RpSetAttr_phyAntennaSwitchEna( uint8_t attrValue );
//  103 static uint8_t RpSetAttr_phyAntennaDiversityRxEna( uint8_t attrValue );
//  104 static uint8_t RpSetAttr_phyAntennaSelectTx( uint8_t attrValue );
//  105 static uint8_t RpSetAttr_phyAntennaSelectAckTx( uint8_t attrValue );
//  106 static uint8_t RpSetAttr_phyAntennaSelectAckRx( uint8_t attrValue );
//  107 static uint8_t RpSetAttr_phyRxTimeoutMode( uint8_t attrValue );
//  108 static uint8_t RpSetAttr_phyFreqBandId( uint8_t attrValue );
//  109 static uint8_t RpSetAttr_phyRegulatoryMode( uint8_t attrValue );
//  110 static uint8_t RpSetAttr_phyPreamble4byteRxMode( uint8_t attrValue );
//  111 static uint8_t RpSetAttr_phyAgcStartVth( uint16_t attrValue );
//  112 static uint8_t RpSetAttr_phyCcaBandwidth( uint8_t attrValue );
//  113 static uint8_t RpSetAttr_phyEdBandwidth( uint8_t attrValue );
//  114 static uint8_t RpSetAttr_phyAntennaDiversityStartVth( uint16_t attrValue );
//  115 static uint8_t RpSetAttr_phyAntennaSwitchingTime( uint16_t attrValue );
//  116 static uint8_t RpSetAttr_phySfdDetectionExtend( uint8_t attrValue );
//  117 static uint8_t RpSetAttr_phyAgcWaitGainOffset( uint8_t attrValue );
//  118 static uint8_t RpSetAttr_phyCcaVthOffset( uint8_t attrValue );
//  119 static uint8_t RpSetAttr_phyAntennaSwitchEnaTiming( uint16_t attrValue );
//  120 static uint8_t RpSetAttr_phyGpio0Setting( uint8_t attrValue );
//  121 static uint8_t RpSetAttr_phyGpio3Setting( uint8_t attrValue );
//  122 static uint8_t RpSetAttr_phyRmodeTonMax( uint16_t attrValue );
//  123 static uint8_t RpSetAttr_phyRmodeToffMin( uint16_t attrValue );
//  124 static uint8_t RpSetAttr_phyRmodeTcumSmpPeriod( uint16_t attrValue );
//  125 static uint8_t RpSetAttr_phyRmodeTcumLimit( uint32_t attrValue );
//  126 static uint8_t RpSetAttr_phyRssiOutputOffset( uint8_t attrValue );
//  127 #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
//  128 	static void RpPlmeGetReq( uint8_t id, void *pVal );
//  129 #endif // #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
//  130 
//  131 /***************************************************************************************************************
//  132  * private variables
//  133  **************************************************************************************************************/

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
//  134 static const uint8_t g_MaxFskOpeModeTbl[RP_NUM_FREQ_BAND] = 
_g_MaxFskOpeModeTbl:
        DATA8
        DB 7, 3, 3, 4, 4, 6, 3, 3, 1, 4
//  135 {
//  136 	0x07,0x03,0x03,0x04,0x04,0x06,0x03,0x03,0x01,0x04
//  137 };
//  138 

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
//  139 static const r_maxChannelTbl_t g_MaxChannelTbl[RP_NUM_FREQ_BAND] =
_g_MaxChannelTbl:
        DATA8
        DB 33, 16, 16, 68, 68, 68, 68, 198, 98, 48, 0, 0, 0, 0, 38, 18, 8, 0, 0
        DB 0, 0, 128, 63, 63, 128, 0, 0, 0, 31, 15, 15, 31, 0, 0, 0, 37, 36, 35
        DB 35, 36, 37, 0, 35, 128, 41, 0, 0, 0, 0, 58, 58, 58, 0, 0, 0, 0, 128
        DB 0, 0, 0, 0, 0, 0, 17, 17, 17, 17, 0, 0, 0
//  140 {
//  141 	/* freqBanId = 0x04 */ { 0x21,0x10,0x10,0x44,0x44,0x44,0x44 },
//  142 	/* freqBanId = 0x05 */ { 0xC6,0x62,0x30,0x00,0x00,0x00,0x00 },
//  143 	/* freqBanId = 0x06 */ { 0x26,0x12,0x08,0x00,0x00,0x00,0x00 },
//  144 	/* freqBanId = 0x07 */ { 0x80,0x3F,0x3F,0x80,0x00,0x00,0x00 },
//  145 	/* freqBanId = 0x08 */ { 0x1F,0x0F,0x0F,0x1F,0x00,0x00,0x00 },
//  146 	/* freqBanId = 0x09 */ { 0x25,0x24,0x23,0x23,0x24,0x25,0x00 },
//  147 	/* freqBanId = 0x0E */ { 0x23,0x80,0x29,0x00,0x00,0x00,0x00 },
//  148 	/* freqBanId = 0x0F */ { 0x3A,0x3A,0x3A,0x00,0x00,0x00,0x00 },
//  149 	/* freqBanId = 0x10 */ { 0x80,0x00,0x00,0x00,0x00,0x00,0x00 },
//  150 	/* freqBanId = 0x11 */ { 0x11,0x11,0x11,0x11,0x00,0x00,0x00 }
//  151 };
//  152 
//  153 /***************************************************************************************************************
//  154  * program
//  155  **************************************************************************************************************/
//  156 /***************************************************************************************************************
//  157  * function name  : RpSetPibReq
//  158  * description    : Attribute Value Setting Function
//  159  * parameters     : id...Attribute ID
//  160  *                : valLen...Size of the attribute value to be set (byte length)
//  161  *                : pVal...Pointer to the beginning of the area storing the attribute value to be set
//  162  * return value   : RP_SUCCESS, RP_UNSUPPORTED_ATTRIBUTE, RP_INVALID_PARAMETER, RP_BUSY_RX, RP_BUSY_TX
//  163  *				  : RP_BUSY_LOWPOWER
//  164  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _RpSetPibReq
        CODE
//  165 int16_t RpSetPibReq( uint8_t id, uint8_t valLen, void RP_FAR *pVal )
//  166 {
_RpSetPibReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
//  167 	uint16_t status;
//  168 	int16_t rtn = RpExtChkIdLenSetReq(id, valLen, pVal);
        MOV       A, C               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
          CFI FunCall _RpExtChkIdLenSetReq
        CALL      F:_RpExtChkIdLenSetReq  ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  169 	int16_t result;
//  170 
//  171 	#if defined(__RX)
//  172 	uint32_t bkupPsw;
//  173 	#elif defined(__CCRL__) || defined(__ICCRL78__)
//  174 	uint8_t  bkupPsw;
//  175 	#elif defined(__arm)
//  176 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
//  177 	#endif
//  178 
//  179 	/* Disable interrupt */
//  180 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  181 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  182 	#else
//  183 	RP_PHY_DI();
//  184 	#endif
//  185 
//  186 	/* API function execution Log */
//  187 	RpLog_Event( RP_LOG_API_SETIB, id );
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0x80           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  188 
//  189 	if (rtn != RP_SUCCESS)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        CMPW      AX, #0x7           ;; 1 cycle
        BNZ       ??RpPlmeGetReq_1   ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
//  190 	{
//  191 		/* API function execution Log */
//  192 		RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, (uint8_t)rtn );
//  193 
//  194 		/* Enable interrupt */
//  195 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  196 		RP_PHY_EI(bkupPsw);
//  197 		#else
//  198 		RP_PHY_EI();
//  199 		#endif
//  200 
//  201 		return (rtn);
//  202 	}
//  203 
//  204 	status = RpCb.status;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  205 
//  206 	if (status & RP_PHY_STAT_LOWPOWER)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].5, ??RpPlmeGetReq_2  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
//  207 	{
//  208 		/* API function execution Log */
//  209 		RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, RP_BUSY_LOWPOWER );
        MOV       X, #0xF3           ;; 1 cycle
        MOV       A, #0x82           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  210 
//  211 		/* Enable interrupt */
//  212 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  213 		RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  214 		#else
//  215 		RP_PHY_EI();
//  216 		#endif
//  217 
//  218 		return (RP_BUSY_LOWPOWER);
        MOVW      AX, #0xF3          ;; 1 cycle
        BR        S:??RpPlmeGetReq_3  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
//  219 	}
//  220 	else if ((status & RP_PHY_STAT_TRX_OFF) == 0)
??RpPlmeGetReq_2:
        DECW      HL                 ;; 1 cycle
        BT        [HL].4, ??RpPlmeGetReq_4  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
//  221 	{
//  222 		if (status & RP_PHY_STAT_TX)
        BF        [HL].1, ??RpPlmeGetReq_5  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  223 		{
//  224 			/* API function execution Log */
//  225 			RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, RP_BUSY_TX );
        MOV       X, #0x2            ;; 1 cycle
        MOV       A, #0x82           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  226 
//  227 			/* Enable interrupt */
//  228 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  229 			RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  230 			#else
//  231 			RP_PHY_EI();
//  232 			#endif
//  233 
//  234 			return (RP_BUSY_TX);
        MOVW      AX, #0x2           ;; 1 cycle
        BR        S:??RpPlmeGetReq_3  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
//  235 		}
//  236 		else
//  237 		{
//  238 			/* API function execution Log */
//  239 			RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, RP_BUSY_RX );
??RpPlmeGetReq_5:
        ONEB      X                  ;; 1 cycle
        MOV       A, #0x82           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  240 
//  241 			/* Enable interrupt */
//  242 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  243 			RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  244 			#else
//  245 			RP_PHY_EI();
//  246 			#endif
//  247 
//  248 			return (RP_BUSY_RX);
        ONEW      AX                 ;; 1 cycle
        BR        S:??RpPlmeGetReq_3  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
//  249 		}
//  250 	}
//  251 
//  252 	result = RpPlmeSetReq( id, pVal );
??RpPlmeGetReq_4:
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
          CFI FunCall _RpPlmeSetReq
        CALL      F:_RpPlmeSetReq    ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  253 	if ( result != RP_SUCCESS )
        CMPW      AX, #0x7           ;; 1 cycle
        BZ        ??RpPlmeGetReq_6   ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
//  254 	{
//  255 		/* API function execution Log */
//  256 		RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, (uint8_t)result );
??RpPlmeGetReq_1:
        MOV       A, #0x82           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  257 
//  258 		/* Enable interrupt */
//  259 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  260 		RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  261 		#else
//  262 		RP_PHY_EI();
//  263 		#endif
//  264 
//  265 		return (result);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BR        S:??RpPlmeGetReq_3  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
//  266 	}
//  267 
//  268 	/* API function execution Log */
//  269 	RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RET, RP_SUCCESS );
??RpPlmeGetReq_6:
        MOV       X, #0x7            ;; 1 cycle
        MOV       A, #0x81           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
//  270 
//  271 	/* Enable interrupt */
//  272 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  273 	RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  274 	#else
//  275 	RP_PHY_EI();
//  276 	#endif
//  277 
//  278 	return (RP_SUCCESS);
        MOVW      AX, #0x7           ;; 1 cycle
        ; ------------------------------------- Block: 10 cycles
??RpPlmeGetReq_3:
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 141 cycles
//  279 }
//  280 
//  281 /***************************************************************************************************************
//  282  * function name  : RpPlmeSetReq
//  283  * description    : Attribute Value Setting Function
//  284  * parameters     : attrId...Attribute ID
//  285  *                : p_attrValue...
//  286  * return value   : result
//  287  **************************************************************************************************************/
//  288 #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
//  289 static int16_t RpPlmeSetReq( uint8_t attrId, void RP_FAR *p_attrValue )
//  290 #else

        SECTION `.textf_unit64kp`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _RpPlmeSetReq
        CODE
//  291 int16_t RpPlmeSetReq( uint8_t attrId, void RP_FAR *p_attrValue )
//  292 #endif // #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
//  293 {
_RpPlmeSetReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 22
        SUBW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+26
//  294 	int16_t result = RP_SUCCESS;
        MOVW      AX, #0x7           ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  295 	uint8_t RP_FAR  *p_wk8;
//  296 	uint8_t wk8;
//  297 	uint16_t wk16;
//  298 	uint32_t wk32;
//  299 
//  300 	#if defined(__RX)
//  301 	uint32_t bkupPsw;
//  302 	#elif defined(__CCRL__) || defined(__ICCRL78__)
//  303 	uint8_t bkupPsw;
//  304 	#elif defined(__arm)
//  305 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
//  306 	#endif
//  307 
//  308 	p_wk8 = (uint8_t RP_FAR *)p_attrValue;
//  309 	wk8 = *p_wk8;
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
//  310 	wk16 = RP_VAL_ARRAY_TO_UINT16( p_wk8 );
        MOV       L, A               ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  311 	wk32 = RP_VAL_ARRAY_TO_UINT32( p_wk8 );
        CLRW      BC                 ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+32
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x8            ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        ADDW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
//  312 
//  313 	/* Disable interrupt */
//  314 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  315 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  316 	#else
//  317 	RP_PHY_DI();
//  318 	#endif
//  319 
//  320 	switch ( attrId )
        MOV       A, [SP+0x11]       ;; 1 cycle
        MOVW      HL, #LWRD(??RpPlmeSetReq_0)  ;; 1 cycle
        MOV       ES, #BYTE3(??RpPlmeSetReq_0)  ;; 1 cycle
        MOV       CS, #BYTE3(_RpPlmeSetReq)  ;; 1 cycle
        BR        N:?C_SSWITCH_L10   ;; 3 cycles
        ; ------------------------------------- Block: 71 cycles
//  321 	{
//  322 		case RP_PHY_CURRENT_CHANNEL:
//  323 			result = RpSetAttr_phyCurrentChannel( wk8 );
??RpPlmeSetReq_1:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall _RpSetAttr_phyCurrentChannel
        CALL      F:_RpSetAttr_phyCurrentChannel  ;; 3 cycles
        BR        R:??RpPlmeGetReq_49  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  324 			break;
//  325 
//  326 		case RP_PHY_CHANNELS_SUPPORTED_PAGE:
//  327 			result = RpSetAttr_phyChannelsSupportedPage( wk8 );
??RpPlmeSetReq_2:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BC        ??RpPlmeGetReq_50  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_51  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_50:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+162, A    ;; 2 cycles
          CFI FunCall _RpSetChannelsSupportedPageAndVal
        CALL      F:_RpSetChannelsSupportedPageAndVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_51:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  328 			break;
//  329 
//  330 		case RP_PHY_CCA_VTH:
//  331 			result = RpSetAttr_phyCcaVth( wk16 );
??RpPlmeSetReq_3:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xFF00        ;; 1 cycle
        CMPW      AX, #0x100         ;; 1 cycle
        BC        ??RpPlmeGetReq_53  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_54  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_53:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+166, AX   ;; 2 cycles
          CFI FunCall _RpSetCcaVthVal
        CALL      F:_RpSetCcaVthVal  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpPlmeGetReq_54:
        BR        R:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  332 			break;
//  333 
//  334 		case RP_PHY_TRANSMIT_POWER:
//  335 			result = RpSetAttr_phyTransmitPower( wk8 );
??RpPlmeSetReq_4:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall _RpSetAttr_phyTransmitPower
        CALL      F:_RpSetAttr_phyTransmitPower  ;; 3 cycles
        BR        R:??RpPlmeGetReq_49  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  336 			break;
//  337 
//  338 		case RP_PHY_FSK_FEC_RX_ENA:
//  339 			result = RpSetAttr_phyFskFecRxEna( wk8 );
??RpPlmeSetReq_5:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpPlmeGetReq_56  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_57  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_56:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+164, A    ;; 2 cycles
          CFI FunCall _RpSetFecVal
        CALL      F:_RpSetFecVal     ;; 3 cycles
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetFskOpeModeVal
        CALL      F:_RpSetFskOpeModeVal  ;; 3 cycles
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetAgcStartVth
        CALL      F:_RpSetAgcStartVth  ;; 3 cycles
          CFI FunCall _RpSetSfdDetectionExtend
        CALL      F:_RpSetSfdDetectionExtend  ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
??RpPlmeGetReq_57:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  340 			break;
//  341 
//  342 		case RP_PHY_FSK_FEC_TX_ENA:
//  343 			result = RpSetAttr_phyFskFecTxEna( wk8 );
??RpPlmeSetReq_6:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_58  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_59  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_58:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+163, A    ;; 2 cycles
          CFI FunCall _RpSetFecVal
        CALL      F:_RpSetFecVal     ;; 3 cycles
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetFskOpeModeVal
        CALL      F:_RpSetFskOpeModeVal  ;; 3 cycles
          CFI FunCall _RpSetSfdDetectionExtend
        CALL      F:_RpSetSfdDetectionExtend  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
??RpPlmeGetReq_59:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  344 			break;
//  345 
//  346 		case RP_PHY_FSK_FEC_SCHEME:
//  347 			result = RpSetAttr_phyFskFecScheme( wk8 );
??RpPlmeSetReq_7:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_60  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_61  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_60:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+165, A    ;; 2 cycles
          CFI FunCall _RpSetFecVal
        CALL      F:_RpSetFecVal     ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_61:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  348 			break;
//  349 
//  350 		case RP_PHY_LVLFLTR_VTH:
//  351 			result = RpSetAttr_phyLvlFltrVth( wk16 );
??RpPlmeSetReq_8:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, #0x200         ;; 1 cycle
        BC        ??RpPlmeGetReq_62  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_63  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_62:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+168, AX   ;; 2 cycles
          CFI FunCall _RpSetLvlVthVal
        CALL      F:_RpSetLvlVthVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_63:
        BR        R:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  352 			break;
//  353 
//  354 		case RP_PHY_BACKOFF_SEED:
//  355 			result = RpSetAttr_phyBackOffSeed( wk8 );
??RpPlmeSetReq_9:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RpPlmeGetReq_64  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_65  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_64:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+173, A    ;; 2 cycles
        CLRB      C                  ;; 1 cycle
        MOVW      AX, #0x1A8         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
          CFI FunCall _RpSetBackOffSeedVal
        CALL      F:_RpSetBackOffSeedVal  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
??RpPlmeGetReq_65:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  356 			break;
//  357 
//  358 		case RP_PHY_CRCERROR_UPMSG:
//  359 			result = RpSetAttr_phyCrcErrorUpMsg( wk8 );
??RpPlmeSetReq_10:
        MOV       X, #0x7            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_66  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       X, #0x5            ;; 1 cycle
        BR        S:??RpPlmeGetReq_67  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpPlmeGetReq_66:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        CLRB      ES:_RpCb+172       ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        ONEB      ES:_RpCb+172       ;; 2 cycles
        ; ------------------------------------- Block: 2 cycles
??RpPlmeGetReq_67:
        BR        R:??RpPlmeGetReq_68  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  360 			break;
//  361 
//  362 		case RP_MAC_ADDRESS_FILTER1_ENA:
//  363 			result = RpSetAttr_macAddressFilter1Ena( wk8 );
??RpPlmeSetReq_11:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_69  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_70  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_69:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+176, A    ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetAckReplyTimeVal
        ; ------------------------------------- Block: 5 cycles
        CALL      F:_RpSetAckReplyTimeVal  ;; 3 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        ; ------------------------------------- Block: 3 cycles
??RpPlmeSetReq_12:
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpPlmeGetReq_70:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  364 			break;
//  365 
//  366 		case RP_MAC_SHORT_ADDRESS1:
//  367 			result = RpSetAttr_macShortAddress1( wk16 );
??RpPlmeSetReq_13:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+178, AX   ;; 2 cycles
        BR        R:??RpPlmeGetReq_71  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  368 			break;
//  369 
//  370 		case RP_MAC_EXTENDED_ADDRESS1:
//  371 			result = RpSetAttr_macExtendedAddress1( p_attrValue );
??RpPlmeSetReq_14:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpPlmeGetReq_72  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpPlmeGetReq_72:
        BNZ       ??RpPlmeGetReq_73  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpPlmeGetReq_73:
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_RpCb+182)  ;; 1 cycle
        MOV       A, #BYTE3(_RpCb)   ;; 1 cycle
          CFI FunCall _RpMemcpy
        CALL      F:_RpMemcpy        ;; 3 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  372 			break;
        POP       AX                 ;; 1 cycle
          CFI CFA SP+26
        BR        R:??RpPlmeGetReq_74  ;; 3 cycles
        ; ------------------------------------- Block: 22 cycles
//  373 
//  374 		case RP_MAC_PANID1:
//  375 			result = RpSetAttr_macPanId1( wk16 );
??RpPlmeSetReq_15:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+180, AX   ;; 2 cycles
        BR        R:??RpPlmeGetReq_71  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  376 			break;
//  377 
//  378 		case RP_MAC_PAN_COORD1:
//  379 			result = RpSetAttr_macPanCoord1( wk8 );
??RpPlmeSetReq_16:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_75  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_76  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_75:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+190, A    ;; 2 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_76:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  380 			break;
//  381 
//  382 		case RP_MAC_FRAME_PEND1:
//  383 			result = RpSetAttr_macFramePend1( wk8 );
??RpPlmeSetReq_17:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_77  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_78  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_77:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+191, A    ;; 2 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_78:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  384 			break;
//  385 
//  386 		case RP_MAC_ADDRESS_FILTER2_ENA:
//  387 			result = RpSetAttr_macAddressFilter2Ena( wk8 );
??RpPlmeSetReq_18:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_79  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_80  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_79:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+192, A    ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _RpSetAckReplyTimeVal
        ; ------------------------------------- Block: 5 cycles
        CALL      F:_RpSetAckReplyTimeVal  ;; 3 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        ; ------------------------------------- Block: 3 cycles
??RpPlmeSetReq_19:
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??RpPlmeGetReq_80:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  388 			break;
//  389 
//  390 		case RP_MAC_SHORT_ADDRESS2:
//  391 			result = RpSetAttr_macShortAddress2( wk16 );
??RpPlmeSetReq_20:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+194, AX   ;; 2 cycles
        BR        S:??RpPlmeGetReq_71  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  392 			break;
//  393 
//  394 		case RP_MAC_EXTENDED_ADDRESS2:
//  395 			result = RpSetAttr_macExtendedAddress2( p_attrValue );
??RpPlmeSetReq_21:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RpPlmeGetReq_81  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpPlmeGetReq_81:
        BNZ       ??RpPlmeGetReq_82  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_83  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_82:
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, #LWRD(_RpCb+198)  ;; 1 cycle
        MOV       A, #BYTE3(_RpCb)   ;; 1 cycle
          CFI FunCall _RpMemcpy
        CALL      F:_RpMemcpy        ;; 3 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+26
        ; ------------------------------------- Block: 15 cycles
??RpPlmeGetReq_83:
        BR        R:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  396 			break;
//  397 
//  398 		case RP_MAC_PANID2:
//  399 			result = RpSetAttr_macPanId2( wk16 );
??RpPlmeSetReq_22:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+196, AX   ;; 2 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        ; ------------------------------------- Block: 4 cycles
??RpPlmeGetReq_71:
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
//  400 			break;
        BR        R:??RpPlmeGetReq_74  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  401 
//  402 		case RP_MAC_PAN_COORD2:
//  403 			result = RpSetAttr_macPanCoord2( wk8 );
??RpPlmeSetReq_23:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_84  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_85  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_84:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+206, A    ;; 2 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_85:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  404 			break;
//  405 
//  406 		case RP_MAC_FRAME_PEND2:
//  407 			result = RpSetAttr_macFramePend2( wk8 );
??RpPlmeSetReq_24:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_86  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_87  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_86:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+207, A    ;; 2 cycles
          CFI FunCall _RpSetAdrfAndAutoAckVal
        CALL      F:_RpSetAdrfAndAutoAckVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_87:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  408 			break;
//  409 
//  410 		case RP_MAC_MAXCSMABACKOFF:
//  411 			result = RpSetAttr_macMaxCsmaBackOff( wk8 );
??RpPlmeSetReq_25:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BC        ??RpPlmeGetReq_88  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_89  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_88:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+208, A    ;; 2 cycles
          CFI FunCall _RpSetMaxCsmaBackoffVal
        CALL      F:_RpSetMaxCsmaBackoffVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_89:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  412 			break;
//  413 
//  414 		case RP_MAC_MINBE:
//  415 			result = RpSetAttr_macMinBe( wk8 );
??RpPlmeSetReq_26:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x9            ;; 1 cycle
        BC        ??RpPlmeGetReq_90  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_91  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_90:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+209, A    ;; 2 cycles
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegRead
        CALL      F:_RpRegRead       ;; 3 cycles
        MOV       X, A               ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+209    ;; 2 cycles
        AND       A, #0xF            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0xF0           ;; 1 cycle
        OR        A, B               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOVW      AX, #0x1F0         ;; 1 cycle
          CFI FunCall _RpRegWrite
        CALL      F:_RpRegWrite      ;; 3 cycles
        ; ------------------------------------- Block: 21 cycles
??RpPlmeGetReq_91:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  416 			break;
//  417 
//  418 		case RP_MAC_MAXBE:
//  419 			result = RpSetAttr_macMaxBe( wk8 );
??RpPlmeSetReq_27:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, #0xFD           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BC        ??RpPlmeGetReq_92  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_93  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_92:
        MOV       A, [SP]            ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+210, A    ;; 2 cycles
          CFI FunCall _RpSetMaxBeVal
        CALL      F:_RpSetMaxBeVal   ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpPlmeGetReq_93:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  420 			break;
//  421 
//  422 		case RP_MAC_MAX_FRAME_RETRIES:
//  423 			result = RpSetAttr_macMaxFrameRetries( wk8 );
??RpPlmeSetReq_28:
        MOV       X, #0x7            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x8            ;; 1 cycle
        BC        ??RpPlmeGetReq_94  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       X, #0x5            ;; 1 cycle
        BR        S:??RpPlmeGetReq_95  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpPlmeGetReq_94:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+211, A    ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??RpPlmeGetReq_95:
        BR        R:??RpPlmeGetReq_68  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  424 			break;
//  425 
//  426 		case RP_PHY_CSMA_BACKOFF_PERIOD:
//  427 			result = RpSetAttr_macCsmaBackoffPeriod( wk16 );
??RpPlmeSetReq_29:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, #0xF           ;; 1 cycle
        BNC       ??RpPlmeGetReq_96  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_97  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_96:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+240, AX   ;; 2 cycles
          CFI FunCall _RpSetCsmaBackoffPeriod
        CALL      F:_RpSetCsmaBackoffPeriod  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_97:
        BR        R:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  428 			break;
//  429 
//  430 		case RP_PHY_CCA_DURATION:
//  431 			result = RpSetAttr_phyCcaDuration( wk16 );
??RpPlmeSetReq_30:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??RpPlmeGetReq_98  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, #0x7D1         ;; 1 cycle
        BC        ??RpPlmeGetReq_99  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_98:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_100  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_99:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+212, AX   ;; 2 cycles
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetCcaDurationVal
        CALL      F:_RpSetCcaDurationVal  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpPlmeGetReq_100:
        BR        R:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  432 			break;
//  433 
//  434 		case RP_PHY_MRFSK_SFD:
//  435 			result = RpSetAttr_phyMrFskSfd( wk8 );
??RpPlmeSetReq_31:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_101  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_102  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_101:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+216, A    ;; 2 cycles
          CFI FunCall _RpSetMrFskSfdVal
        CALL      F:_RpSetMrFskSfdVal  ;; 3 cycles
          CFI FunCall _RpSetSfdDetectionExtend
        CALL      F:_RpSetSfdDetectionExtend  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
??RpPlmeGetReq_102:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  436 			break;
//  437 
//  438 		case RP_PHY_FSK_PREAMBLE_LENGTH:
//  439 			result = RpSetAttr_phyFskPreambleLength( wk16 );
??RpPlmeSetReq_32:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        CMPW      AX, #0x3E5         ;; 1 cycle
        BC        ??RpPlmeGetReq_103  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_104  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_103:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+214, AX   ;; 2 cycles
          CFI FunCall _RpSetPreambleLengthVal
        CALL      F:_RpSetPreambleLengthVal  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpPlmeGetReq_104:
        BR        R:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  440 			break;
//  441 
//  442 		case RP_PHY_FSK_SCRAMBLE_PSDU:
//  443 			result = RpSetAttr_phyFskScramblePsdu( wk8 );
??RpPlmeSetReq_33:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_105  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_106  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_105:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+217, A    ;; 2 cycles
          CFI FunCall _RpSetFskScramblePsduVal
        CALL      F:_RpSetFskScramblePsduVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_106:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  444 			break;
//  445 
//  446 		case RP_PHY_FSK_OPE_MODE:
//  447 			result = RpSetAttr_phyFskOpeMode( wk8 );
??RpPlmeSetReq_34:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall _RpSetAttr_phyFskOpeMode
        CALL      F:_RpSetAttr_phyFskOpeMode  ;; 3 cycles
        BR        R:??RpPlmeGetReq_49  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  448 			break;
//  449 
//  450 		case RP_PHY_FCS_LENGTH:
//  451 			result = RpSetAttr_phyFcsLength( wk8 );
??RpPlmeSetReq_35:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        SUB       A, #0x2            ;; 1 cycle
        BZ        ??RpPlmeGetReq_107  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        SUB       A, #0x2            ;; 1 cycle
        BZ        ??RpPlmeGetReq_107  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_108  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_107:
        MOV       A, [SP]            ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+219, A    ;; 2 cycles
          CFI FunCall _RpSetFcsLengthVal
        CALL      F:_RpSetFcsLengthVal  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpPlmeGetReq_108:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  452 			break;
//  453 
//  454 		case RP_PHY_ACK_REPLY_TIME:
//  455 			result = RpSetAttr_phyAckReplyTime( wk16 );
??RpPlmeSetReq_36:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        MOVW      BC, #0x15E         ;; 1 cycle
        MULHU                        ;; 2 cycles
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        CLRW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+26
        BNC       ??RpPlmeGetReq_109  ;; 4 cycles
        ; ------------------------------------- Block: 47 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_110  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_109:
        MOVW      ES:_RpCb+220, AX   ;; 2 cycles
          CFI FunCall _RpSetAckReplyTimeVal
        CALL      F:_RpSetAckReplyTimeVal  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_110:
        BR        R:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  456 			break;
//  457 
//  458 		case RP_PHY_ACK_WAIT_DURATION:
//  459 			result = RpSetAttr_phyAckWaitDuration( wk16 );
??RpPlmeSetReq_37:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??RpPlmeGetReq_111  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_112  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_111:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+222, AX   ;; 2 cycles
          CFI FunCall _RpSetAckWaitDurationVal
        CALL      F:_RpSetAckWaitDurationVal  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpPlmeGetReq_112:
        BR        R:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  460 			break;
//  461 
//  462 		case RP_PHY_PROFILE_SPECIFIC_MODE:
//  463 			result = RpSetAttr_phyProfileSpecificMode( wk8 );
??RpPlmeSetReq_38:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_113  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_114  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_113:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+224, A    ;; 2 cycles
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetSpecificModeVal
        CALL      F:_RpSetSpecificModeVal  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpPlmeGetReq_114:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  464 			break;
//  465 
//  466 		case RP_PHY_ANTENNA_SWITCH_ENA:
//  467 			result = RpSetAttr_phyAntennaSwitchEna( wk8 );
??RpPlmeSetReq_39:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_115  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_116  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_115:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+225, A    ;; 2 cycles
          CFI FunCall _RpSetAntennaSwitchVal
        CALL      F:_RpSetAntennaSwitchVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_116:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  468 			break;
//  469 
//  470 		case RP_PHY_ANTENNA_DIVERSITY_RX_ENA:
//  471 			result = RpSetAttr_phyAntennaDiversityRxEna( wk8 );
??RpPlmeSetReq_40:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_117  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_118  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_117:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+226, A    ;; 2 cycles
          CFI FunCall _RpSetAntennaDiversityVal
        CALL      F:_RpSetAntennaDiversityVal  ;; 3 cycles
          CFI FunCall _RpSetAntennaSelectTxVal
        CALL      F:_RpSetAntennaSelectTxVal  ;; 3 cycles
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetFskOpeModeVal
        CALL      F:_RpSetFskOpeModeVal  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
??RpPlmeGetReq_118:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  472 			break;
//  473 
//  474 		case RP_PHY_ANTENNA_SELECT_TX:
//  475 			result = RpSetAttr_phyAntennaSelectTx( wk8 );
??RpPlmeSetReq_41:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_119  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_120  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_119:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+227, A    ;; 2 cycles
          CFI FunCall _RpSetAntennaSelectTxVal
        CALL      F:_RpSetAntennaSelectTxVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_120:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  476 			break;
//  477 
//  478 		case RP_PHY_ANTENNA_SELECT_ACKTX:
//  479 			result = RpSetAttr_phyAntennaSelectAckTx( wk8 );
??RpPlmeSetReq_42:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BC        ??RpPlmeGetReq_121  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_122  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_121:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+228, A    ;; 2 cycles
          CFI FunCall _RpSetAntennaDiversityVal
        CALL      F:_RpSetAntennaDiversityVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_122:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  480 			break;
//  481 
//  482 		case RP_PHY_ANTENNA_SELECT_ACKRX:
//  483 			result = RpSetAttr_phyAntennaSelectAckRx( wk8 );
??RpPlmeSetReq_43:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpPlmeGetReq_123  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_124  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_123:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+229, A    ;; 2 cycles
          CFI FunCall _RpSetAntennaDiversityVal
        CALL      F:_RpSetAntennaDiversityVal  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_124:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  484 			break;
//  485 
//  486 		case RP_PHY_RX_TIMEOUT_MODE:
//  487 			result = RpSetAttr_phyRxTimeoutMode( wk8 );
??RpPlmeSetReq_44:
        MOV       X, #0x7            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_125  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       X, #0x5            ;; 1 cycle
        BR        S:??RpPlmeGetReq_126  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpPlmeGetReq_125:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+230, A    ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??RpPlmeGetReq_126:
        BR        R:??RpPlmeGetReq_68  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  488 			break;
//  489 
//  490 		case RP_PHY_FREQ_BAND_ID:
//  491 			result = RpSetAttr_phyFreqBandId( wk8 );
??RpPlmeSetReq_45:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        SUB       A, #0x4            ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BC        ??RpPlmeGetReq_127  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        SUB       A, #0xA            ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BC        ??RpPlmeGetReq_127  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_128  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_127:
        MOV       A, [SP]            ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+234, A    ;; 2 cycles
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetFskOpeModeVal
        CALL      F:_RpSetFskOpeModeVal  ;; 3 cycles
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetAgcStartVth
        CALL      F:_RpSetAgcStartVth  ;; 3 cycles
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetAntennaDiversityStartVth
        CALL      F:_RpSetAntennaDiversityStartVth  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+250, AX   ;; 2 cycles
          CFI FunCall _RpSetSfdDetectionExtend
        CALL      F:_RpSetSfdDetectionExtend  ;; 3 cycles
          CFI FunCall _RpSetAgcWaitGain
        CALL      F:_RpSetAgcWaitGain  ;; 3 cycles
        ; ------------------------------------- Block: 26 cycles
??RpPlmeGetReq_128:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  492 			break;
//  493 
//  494 		case RP_PHY_REGULATORY_MODE:
//  495 			result = RpSetAttr_phyRegulatoryMode( wk8 );
??RpPlmeSetReq_46:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpPlmeGetReq_129  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_130  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_129:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+235    ;; 2 cycles
        CMP       A, X               ;; 1 cycle
        BZ        ??RpPlmeGetReq_130  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       ES:_RpCb+235, A    ;; 2 cycles
          CFI FunCall _RpSetMacTxLimitMode
        CALL      F:_RpSetMacTxLimitMode  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_130:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  496 			break;
//  497 
//  498 		case RP_PHY_PREAMBLE_4BYTE_RX_MODE:
//  499 			result = RpSetAttr_phyPreamble4byteRxMode( wk8 );
??RpPlmeSetReq_47:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_131  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_132  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_131:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+242, A    ;; 2 cycles
          CFI FunCall _RpSetPreamble4ByteRxMode
        CALL      F:_RpSetPreamble4ByteRxMode  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_132:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  500 			break;
//  501 
//  502 		case RP_PHY_AGC_START_VTH:
//  503 			result = RpSetAttr_phyAgcStartVth( wk16 );
??RpPlmeSetReq_48:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, #0x387E        ;; 1 cycle
        BNC       ??RpPlmeGetReq_133  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        CMPW      AX, #0xF           ;; 1 cycle
        BNC       ??RpPlmeGetReq_134  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??RpPlmeGetReq_134  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_133:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_135  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_134:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+244, AX   ;; 2 cycles
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetAgcStartVth
        CALL      F:_RpSetAgcStartVth  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
??RpPlmeGetReq_135:
        BR        R:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  504 			break;
//  505 
//  506 		case RP_PHY_CCA_BANDWIDTH:
//  507 			result = RpSetAttr_phyCcaBandwidth( wk8 );
??RpPlmeSetReq_49:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall _RpSetAttr_phyCcaBandwidth
        CALL      F:_RpSetAttr_phyCcaBandwidth  ;; 3 cycles
        BR        R:??RpPlmeGetReq_49  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  508 			break;
//  509 
//  510 		case RP_PHY_ED_BANDWIDTH:
//  511 			result = RpSetAttr_phyEdBandwidth( wk8 );
??RpPlmeSetReq_50:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall _RpSetAttr_phyEdBandwidth
        CALL      F:_RpSetAttr_phyEdBandwidth  ;; 3 cycles
        BR        R:??RpPlmeGetReq_49  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  512 			break;
//  513 
//  514 		case RP_PHY_ANTENNA_DIVERSITY_START_VTH:
//  515 			result = RpSetAttr_phyAntennaDiversityStartVth( wk16 );
??RpPlmeSetReq_51:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, #0x200         ;; 1 cycle
        BC        ??RpPlmeGetReq_136  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_137  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_136:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+248, AX   ;; 2 cycles
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetAntennaDiversityStartVth
        CALL      F:_RpSetAntennaDiversityStartVth  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
??RpPlmeGetReq_137:
        BR        S:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  516 			break;
//  517 
//  518 		case RP_PHY_ANTENNA_SWITCHING_TIME:
//  519 			result = RpSetAttr_phyAntennaSwitchingTime( wk16 );
??RpPlmeSetReq_52:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+250, AX   ;; 2 cycles
//  520 			break;
        BR        R:??RpPlmeGetReq_74  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  521 
//  522 		case RP_PHY_SFD_DETECTION_EXTEND:
//  523 			result = RpSetAttr_phySfdDetectionExtend( wk8 );
??RpPlmeSetReq_53:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_138  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_139  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_138:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+252, A    ;; 2 cycles
          CFI FunCall _RpSetSfdDetectionExtend
        CALL      F:_RpSetSfdDetectionExtend  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_139:
        BR        R:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  524 			break;
//  525 
//  526 		case RP_PHY_AGC_WAIT_GAIN_OFFSET:
//  527 			result = RpSetAttr_phyAgcWaitGainOffset( wk8 );
??RpPlmeSetReq_54:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x20           ;; 1 cycle
        BNC       ??RpPlmeGetReq_140  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+253, A    ;; 2 cycles
          CFI FunCall _RpSetAgcWaitGain
        CALL      F:_RpSetAgcWaitGain  ;; 3 cycles
        BR        S:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  528 			break;
//  529 
//  530 		case RP_PHY_CCA_VTH_OFFSET:
//  531 			result = RpSetAttr_phyCcaVthOffset( wk8 );
??RpPlmeSetReq_55:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x20           ;; 1 cycle
        BNC       ??RpPlmeGetReq_140  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+254, A    ;; 2 cycles
          CFI FunCall _RpSetCcaVthVal
        CALL      F:_RpSetCcaVthVal  ;; 3 cycles
        BR        S:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  532 			break;
//  533 
//  534 		case RP_PHY_ANTENNA_SWITCH_ENA_TIMING:
//  535 			result = RpSetAttr_phyAntennaSwitchEnaTiming( wk16 );
??RpPlmeSetReq_56:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??RpPlmeGetReq_141  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, #0x155         ;; 1 cycle
        BC        ??RpPlmeGetReq_142  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_141:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_55  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_142:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+256, AX   ;; 2 cycles
          CFI FunCall _RpSetAntennaSwitchEnaTiming
        CALL      F:_RpSetAntennaSwitchEnaTiming  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_55:
        MOV       A, [SP]            ;; 1 cycle
        BR        R:??RpPlmeGetReq_49  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  536 			break;
//  537 
//  538 		case RP_PHY_GPIO0_SETTING:
//  539 			result = RpSetAttr_phyGpio0Setting( wk8 );
??RpPlmeSetReq_57:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_143  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
??RpPlmeGetReq_140:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_143:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+258, A    ;; 2 cycles
          CFI FunCall _RpSetGpio0Setting
        CALL      F:_RpSetGpio0Setting  ;; 3 cycles
        BR        S:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  540 			break;
//  541 
//  542 		case RP_PHY_GPIO3_SETTING:
//  543 			result = RpSetAttr_phyGpio3Setting( wk8 );
??RpPlmeSetReq_58:
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_144  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RpPlmeGetReq_52  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RpPlmeGetReq_144:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+259, A    ;; 2 cycles
          CFI FunCall _RpSetGpio3Setting
        CALL      F:_RpSetGpio3Setting  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_52:
        MOV       A, [SP+0x01]       ;; 1 cycle
        BR        S:??RpPlmeGetReq_49  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  544 			break;
//  545 
//  546 		case RP_PHY_RMODE_TON_MAX:
//  547 			result = RpSetAttr_phyRmodeTonMax( wk16 );
??RpPlmeSetReq_59:
        MOV       C, #0x7            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, #0x2711        ;; 1 cycle
        BC        ??RpPlmeGetReq_145  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       C, #0x5            ;; 1 cycle
        BR        S:??RpPlmeGetReq_146  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpPlmeGetReq_145:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+260, AX   ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??RpPlmeGetReq_146:
        CLRB      B                  ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        BR        S:??RpPlmeGetReq_147  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  548 			break;
//  549 
//  550 		case RP_PHY_RMODE_TOFF_MIN:
//  551 			result = RpSetAttr_phyRmodeToffMin( wk16 );
??RpPlmeSetReq_60:
        MOV       C, #0x7            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, #0x3E9         ;; 1 cycle
        BC        ??RpPlmeGetReq_148  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       C, #0x5            ;; 1 cycle
        BR        S:??RpPlmeGetReq_149  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpPlmeGetReq_148:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+262, AX   ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??RpPlmeGetReq_149:
        CLRB      B                  ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        BR        S:??RpPlmeGetReq_147  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  552 			break;
//  553 
//  554 		case RP_PHY_RMODE_TCUM_SMP_PERIOD:
//  555 			result = RpSetAttr_phyRmodeTcumSmpPeriod( wk16 );
??RpPlmeSetReq_61:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall _RpSetAttr_phyRmodeTcumSmpPeriod
        CALL      F:_RpSetAttr_phyRmodeTcumSmpPeriod  ;; 3 cycles
        BR        S:??RpPlmeGetReq_49  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  556 			break;
//  557 
//  558 		case RP_PHY_RMODE_TCUM_LIMIT:
//  559 			result = RpSetAttr_phyRmodeTcumLimit( wk32 );
??RpPlmeSetReq_62:
        MOVW      HL, #LWRD(_RpCb+266)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+26
        BZ        ??RpPlmeGetReq_74  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        CMP       ES:_RpCb+235, #0x1  ;; 2 cycles
        BNZ       ??RpPlmeGetReq_74  ;; 4 cycles
          CFI FunCall _RpSetMacTxLimitMode
        ; ------------------------------------- Block: 11 cycles
        CALL      F:_RpSetMacTxLimitMode  ;; 3 cycles
        BR        S:??RpPlmeGetReq_74  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  560 			break;
//  561 
//  562 		case RP_PHY_ACK_WITH_CCA:
//  563 			result = RpSetAttr_phyAckWithCca( wk8 );
??RpPlmeSetReq_63:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall _RpSetAttr_phyAckWithCca
        CALL      F:_RpSetAttr_phyAckWithCca  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpPlmeGetReq_49:
        MOV       X, A               ;; 1 cycle
        BR        S:??RpPlmeGetReq_68  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  564 			break;
//  565 
//  566 		case RP_PHY_RSSI_OUTPUT_OFFSET:
//  567 			result = RpSetAttr_phyRssiOutputOffset( wk8 );
??RpPlmeSetReq_64:
        MOV       X, #0x7            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x20           ;; 1 cycle
        BC        ??RpPlmeGetReq_150  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       X, #0x5            ;; 1 cycle
        BR        S:??RpPlmeGetReq_68  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??RpPlmeGetReq_150:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+282, A    ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??RpPlmeGetReq_68:
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpPlmeGetReq_147:
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  568 			break;
        BR        S:??RpPlmeGetReq_74  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  569 
//  570 #ifdef R_FREQUENCY_OFFSET_ENABLED
//  571 		case RP_PHY_FREQUENCY_OFFSET:
//  572 			result = RpSetAttr_phyFrequencyOffset( (int32_t)wk32 );
??RpPlmeSetReq_65:
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, #LWRD(_RpCb+284)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  573 			break;
        ; ------------------------------------- Block: 10 cycles
//  574 #endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
//  575 
//  576 		// read only
//  577 		case RP_PHY_CURRENT_PAGE:
//  578 		case RP_PHY_CHANNELS_SUPPORTED:
//  579 		case RP_PHY_DATA_RATE:
//  580 		case RP_PHY_RMODE_TCUM:
//  581 			break;
//  582 
//  583 		default:
//  584 			break;
//  585 	}
//  586 
//  587 	/* Enable interrupt */
//  588 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
//  589 	RP_PHY_EI(bkupPsw);
??RpPlmeGetReq_74:
        MOV       A, [SP+0x06]       ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  590 	#else
//  591 	RP_PHY_EI();
//  592 	#endif
//  593 
//  594 	return( result );
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 1477 cycles
//  595 }
//  596 
//  597 /* 3.1.1 */
//  598 /***************************************************************************************************************
//  599  * function name  : RpSetAttr_phyCurrentChannel
//  600  * description    : Attribute Value Setting Function
//  601  * parameters     : attrValue...The value to be set for the attribute
//  602  * return value   : result
//  603  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon0
          CFI Function _RpSetAttr_phyCurrentChannel
        CODE
//  604 static uint8_t RpSetAttr_phyCurrentChannel( uint8_t attrValue )
//  605 {
_RpSetAttr_phyCurrentChannel:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       B, A               ;; 1 cycle
//  606 	uint8_t result = RP_SUCCESS;
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
//  607 	uint8_t freqBandId = RpCb.pib.phyFreqBandId;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       X, ES:_RpCb+234    ;; 2 cycles
//  608 	uint8_t fskOpeMode = RpCb.pib.phyFskOpeMode;
        MOV       C, ES:_RpCb+218    ;; 2 cycles
//  609 	uint8_t indexFreqBandId, indexFskOpeMode;
//  610 	uint8_t res;
//  611 	uint8_t maxChannel;
//  612 
//  613 	/* get indexFreqBnadId */
//  614 	if (( RP_PHY_FREQ_BAND_863MHz <= freqBandId )
//  615 		&&( freqBandId <= RP_PHY_FREQ_BAND_920MHz ))
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xFC           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BC        ??RpPlmeGetReq_7   ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
//  616 	{
//  617 		indexFreqBandId = freqBandId - 4;
//  618 	}
//  619 	else if (( RP_PHY_FREQ_BAND_920MHz_Others <= freqBandId )
//  620 		&&( freqBandId <= RP_PHY_FREQ_BAND_921MHz ))
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xF2           ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BNC       ??RpPlmeGetReq_8   ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xF8           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpPlmeGetReq_7:
        XCH       A, B               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, B               ;; 1 cycle
//  621 	{
//  622 		indexFreqBandId = freqBandId - 8;
//  623 	}
//  624 	else
//  625 	{
//  626 		result = RP_INVALID_PARAMETER;	// Safty
//  627 	}
//  628 
//  629 	/* check parameter */
//  630 	if ( result == RP_SUCCESS )
//  631 	{
//  632 		indexFskOpeMode = fskOpeMode - 1;
//  633 		maxChannel = g_MaxChannelTbl[indexFreqBandId].maxChannel[indexFskOpeMode];
//  634 		if ( maxChannel < attrValue )
        DEC       C                  ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        POP       HL                 ;; 1 cycle
          CFI CFA SP+6
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       C, #0x7            ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_g_MaxChannelTbl)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_g_MaxChannelTbl)  ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        CMP       X, A               ;; 1 cycle
        BC        ??RpPlmeGetReq_8   ;; 4 cycles
          CFI FunCall _RpSetChannelVal
        ; ------------------------------------- Block: 27 cycles
//  635 		{
//  636 			result = RP_INVALID_PARAMETER;
//  637 		}
//  638 #ifndef RP_WISUN_FAN_STACK
//  639 		else
//  640 		{
//  641 			/* check support phyCurrentChannel */
//  642 			result = RpCheckChannelsSupported( attrValue );
//  643 		}
//  644 #endif // #ifndef RP_WISUN_FAN_STACK
//  645 	}
//  646 
//  647 	/* set(update) attribute */
//  648 	if ( result == RP_SUCCESS )
//  649 	{
//  650 		res = RpSetChannelVal( attrValue );
//  651 		if ( res != RP_ERROR )
        CALL      F:_RpSetChannelVal  ;; 3 cycles
        INC       A                  ;; 1 cycle
        BZ        ??RpPlmeGetReq_8   ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  652 		{
//  653 			RpCb.pib.phyCurrentChannel = attrValue;
        MOV       A, [SP]            ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+152, A    ;; 2 cycles
        BR        S:??RpPlmeGetReq_9  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  654 		}
//  655 		else
//  656 		{
//  657 			result = RP_INVALID_PARAMETER;	// Safty
??RpPlmeGetReq_8:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  658 		}
//  659 	}
//  660 
//  661 	return( result );
??RpPlmeGetReq_9:
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        MOV       A, H               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 77 cycles
//  662 }
//  663 
//  664 #ifndef RP_WISUN_FAN_STACK
//  665 /***************************************************************************************************************
//  666  * function name  : RpCheckChannelsSupported
//  667  * description    : 
//  668  * parameters     : channel..
//  669  *                : maskChSupported...
//  670  * return value   : retStatus
//  671  **************************************************************************************************************/
//  672 static uint8_t RpCheckChannelsSupported( uint8_t channel )
//  673 {
//  674 	uint8_t result = RP_SUCCESS;
//  675 	uint8_t channelsSupportedPage[2];
//  676 	uint8_t channel8bit;
//  677 	uint32_t channel32bit;
//  678 	uint32_t channelsSupported[2];
//  679 
//  680 	channelsSupportedPage[0] = RpCb.pib.phyChannelsSupportedPage;
//  681 
//  682 	channelsSupportedPage[1] = RpConvertChannelsSupportedPage( channel );
//  683 	RpSetAttr_phyChannelsSupportedPage( channelsSupportedPage[1] );
//  684 	RpMemcpy( &(channelsSupported[0]), RpCb.pib.phyChannelsSupported, sizeof(RpCb.pib.phyChannelsSupported) );
//  685 
//  686 	RpSetAttr_phyChannelsSupportedPage( channelsSupportedPage[0] );
//  687 
//  688 	channel8bit = channel - (channelsSupportedPage[1] * 64);
//  689 
//  690 	/* ChannelNumber bit 0-31 */
//  691 	if ( channel8bit < 32 )
//  692 	{
//  693 		channel32bit = (uint32_t)1 << channel8bit;
//  694 		channelsSupported[0] &= channel32bit;
//  695 		if ( channelsSupported[0] == 0 )
//  696 		{
//  697 			result = RP_UNSUPPORTED_CHANNEL;
//  698 		}
//  699 	}
//  700 	/* ChannelNumber bit 32-63 */
//  701 	else
//  702 	{
//  703 		channel8bit -= 32;
//  704 		channel32bit = (uint32_t)1 << channel8bit;
//  705 		channelsSupported[1] &= channel32bit;
//  706 		if ( channelsSupported[1] == 0 )
//  707 		{
//  708 			result = RP_UNSUPPORTED_CHANNEL;
//  709 		}
//  710 	}
//  711 
//  712 	return( result );
//  713 }
//  714 
//  715 /***************************************************************************************************************
//  716  * function name  : RpConvertChannelsSupportedPage
//  717  * description    : 
//  718  * parameters     : channel..
//  719  * return value   : retStatus
//  720  **************************************************************************************************************/
//  721 static uint8_t RpConvertChannelsSupportedPage( uint8_t channel )
//  722 {
//  723 	uint8_t channelsSupportedPage;
//  724 
//  725 	/* channel 0-63 */
//  726 	if ( channel < 64 )
//  727 	{
//  728 		channelsSupportedPage = 0x00;
//  729 	}
//  730 	else
//  731 	{
//  732 		/* channel 64-127 */
//  733 		if ( channel < 128 )
//  734 		{
//  735 			channelsSupportedPage = 0x01;
//  736 		}
//  737 		else
//  738 		{
//  739 			/* channel 128-191 */
//  740 			if ( channel < 192 )
//  741 			{
//  742 				channelsSupportedPage = 0x02;
//  743 			}
//  744 			/* channel 192-255 */
//  745 			else
//  746 			{
//  747 				channelsSupportedPage = 0x03;
//  748 			}
//  749 		}
//  750 	}
//  751 
//  752 	return( channelsSupportedPage );
//  753 }
//  754 #endif // #ifndef RP_WISUN_FAN_STACK
//  755 
//  756 /* 3.1.4 */
//  757 /***************************************************************************************************************
//  758  * function name  : RpSetAttr_phyChannelsSupportedPage
//  759  * description    : Attribute Value Setting Function
//  760  * parameters     : attrValue...The value to be set for the attribute
//  761  * return value   : result
//  762  **************************************************************************************************************/
//  763 static uint8_t RpSetAttr_phyChannelsSupportedPage( uint8_t attrValue )
//  764 {
//  765 	uint8_t result = RP_SUCCESS;
//  766 
//  767 	/* check parameter */
//  768 	switch ( attrValue )
//  769 	{
//  770 		case RP_PHY_CHANNELS_SUPPORTED_INDEX_0:	//0x00: ch0..ch63
//  771 		case RP_PHY_CHANNELS_SUPPORTED_INDEX_1:	//0x01: ch64..ch127
//  772 		case RP_PHY_CHANNELS_SUPPORTED_INDEX_2:	//0x02: ch128..ch191
//  773 		case RP_PHY_CHANNELS_SUPPORTED_INDEX_3:	//0x03: ch192..ch255
//  774 			break;
//  775 		default:
//  776 			result = RP_INVALID_PARAMETER;
//  777 			break;
//  778 	}
//  779 
//  780 	/* set(update) attribute */
//  781 	if ( result == RP_SUCCESS )
//  782 	{
//  783 		RpCb.pib.phyChannelsSupportedPage = attrValue;
//  784 		RpSetChannelsSupportedPageAndVal();
//  785 	}
//  786 
//  787 	return( result );
//  788 }
//  789 
//  790 /* 3.1.5 */
//  791 /***************************************************************************************************************
//  792  * function name  : RpSetAttr_phyCcaVth
//  793  * description    : Attribute Value Setting Function
//  794  * parameters     : attrValue...The value to be set for the attribute
//  795  * return value   : result
//  796  **************************************************************************************************************/
//  797 static uint8_t RpSetAttr_phyCcaVth( uint16_t attrValue )
//  798 {
//  799 	uint8_t result = RP_SUCCESS;
//  800 
//  801 	/* check parameter */
//  802 	if (( attrValue < RP_MINVAL_CCAVTH )
//  803 		||( RP_MAXVAL_CCAVTH < attrValue ))
//  804 	{
//  805 		result = RP_INVALID_PARAMETER;
//  806 	}
//  807 
//  808 	/* set(update) attribute */
//  809 	if ( result == RP_SUCCESS )
//  810 	{
//  811 		RpCb.pib.phyCcaVth = attrValue;
//  812 		RpSetCcaVthVal();
//  813 	}
//  814 
//  815 	return( result );
//  816 }
//  817 
//  818 /* 3.1.6 */
//  819 /***************************************************************************************************************
//  820  * function name  : RpSetAttr_phyTransmitPower
//  821  * description    : Attribute Value Setting Function
//  822  * parameters     : attrValue...The value to be set for the attribute
//  823  * return value   : result
//  824  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon0
          CFI Function _RpSetAttr_phyTransmitPower
        CODE
//  825 static uint8_t RpSetAttr_phyTransmitPower( uint8_t attrValue )
//  826 {
_RpSetAttr_phyTransmitPower:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       B, A               ;; 1 cycle
//  827 	uint8_t result = RP_SUCCESS;
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
//  828 	uint8_t maxGainSet;
//  829 	uint8_t res;
//  830 
//  831 	/* get maxGain */
//  832 	switch ( RpCb.pib.phyFreqBandId )
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, ES:_RpCb+234    ;; 2 cycles
        SUB       A, #0x4            ;; 1 cycle
        BZ        ??RpPlmeGetReq_10  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        DEC       A                  ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpPlmeGetReq_11  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        SUB       A, #0x3            ;; 1 cycle
        BZ        ??RpPlmeGetReq_10  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpPlmeGetReq_12  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x5            ;; 1 cycle
        BZ        ??RpPlmeGetReq_12  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpPlmeGetReq_10  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BZ        ??RpPlmeGetReq_11  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        DEC       A                  ;; 1 cycle
        BNZ       ??RpPlmeGetReq_13  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  833 	{
//  834 		case RP_PHY_FREQ_BAND_920MHz:			// (9) 920 - 928 (Japan)
//  835 		case RP_PHY_FREQ_BAND_920MHz_Others:	// (14)	920 - 928 (Japan) other settings
//  836 		case RP_PHY_FREQ_BAND_921MHz:			// (17)	921 - xxx (CH)
//  837 			maxGainSet = 101;
??RpPlmeGetReq_12:
        MOV       X, #0x65           ;; 1 cycle
//  838 			break;
        BR        S:??RpPlmeGetReq_14  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  839 		case RP_PHY_FREQ_BAND_863MHz:			// (4) 863 - 870 (Europe)
//  840 		case RP_PHY_FREQ_BAND_870MHz:			// (15)	870 - xxx (Europe2)
//  841 			maxGainSet = 102;
//  842 			break;
//  843 		case RP_PHY_FREQ_BAND_917MHz:			// (8) 917 - 923.5 (Korea)
//  844 			maxGainSet = 102;
??RpPlmeGetReq_10:
        MOV       X, #0x66           ;; 1 cycle
//  845 			break;
        BR        S:??RpPlmeGetReq_14  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  846 		case RP_PHY_FREQ_BAND_896MHz:			// (5) 896 - 901 (US FCC Part 90)
//  847 		case RP_PHY_FREQ_BAND_901MHz:			// (6) 901 - 902 (US FCC Part 24)
//  848 		case RP_PHY_FREQ_BAND_915MHz:			// (7) 902 - 928 (US)
//  849 		case RP_PHY_FREQ_BAND_902MHz:			// (16)	902 - xxx (PH/MY/AZ)
//  850 			maxGainSet = 104;
??RpPlmeGetReq_11:
        MOV       X, #0x68           ;; 1 cycle
//  851 			break;
        ; ------------------------------------- Block: 1 cycles
??RpPlmeGetReq_14:
        MOV       A, B               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
//  852 		default:
//  853 			maxGainSet = 0;	//Safty
//  854 			break;
//  855 	}
//  856 
//  857 	/* check parameter */
//  858 	if ( maxGainSet == 0 )
//  859 	{
//  860 		result = RP_INVALID_PARAMETER;	// Safty
//  861 	}
//  862 	else
//  863 	{
//  864 		if ( maxGainSet < attrValue )
        CMP       X, A               ;; 1 cycle
        BC        ??RpPlmeGetReq_13  ;; 4 cycles
          CFI FunCall _RpSetTxPowerVal
        ; ------------------------------------- Block: 7 cycles
//  865 		{
//  866 			result = RP_INVALID_PARAMETER;
//  867 		}
//  868 	}
//  869 
//  870 	/* set(update) attribute */
//  871 	if ( result == RP_SUCCESS )
//  872 	{
//  873 		res = RpSetTxPowerVal( attrValue );
//  874 		if ( res != RP_ERROR )
        CALL      F:_RpSetTxPowerVal  ;; 3 cycles
        INC       A                  ;; 1 cycle
        BZ        ??RpPlmeGetReq_13  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  875 		{
//  876 			RpCb.pib.phyTransmitPower = attrValue;
        MOV       A, [SP]            ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+153, A    ;; 2 cycles
        BR        S:??RpPlmeGetReq_15  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
//  877 		}
//  878 		else
//  879 		{
//  880 			result = RP_INVALID_PARAMETER;	// Safty
??RpPlmeGetReq_13:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  881 		}
//  882 	}
//  883 
//  884 	return( result );
??RpPlmeGetReq_15:
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        MOV       A, H               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 89 cycles
//  885 }
//  886 
//  887 /* 3.1.7 */
//  888 /***************************************************************************************************************
//  889  * function name  : RpSetAttr_phyFskFecRxEna
//  890  * description    : Attribute Value Setting Function
//  891  * parameters     : attrValue...The value to be set for the attribute
//  892  * return value   : result
//  893  **************************************************************************************************************/
//  894 static uint8_t RpSetAttr_phyFskFecRxEna( uint8_t attrValue )
//  895 {
//  896 	uint8_t result = RP_SUCCESS;
//  897 
//  898 	/* check parameter */
//  899 	switch ( attrValue )
//  900 	{
//  901 		case RP_FEC_RX_MODE_DISABLE: 		//0x00
//  902 		case RP_FEC_RX_MODE_ENABLE:			//0x01
//  903 		case RP_FEC_RX_MODE_AUTO_DISTINCT:	//0x02
//  904 			break;
//  905 		default:
//  906 			result = RP_INVALID_PARAMETER;
//  907 			break;
//  908 	}
//  909 
//  910 	/* set(update) attribute */
//  911 	if ( result == RP_SUCCESS )
//  912 	{
//  913 		RpCb.pib.phyFskFecRxEna = attrValue;
//  914 		RpSetFecVal();
//  915 		RpSetFskOpeModeVal( RP_FALSE );
//  916 		RpSetAgcStartVth( RP_TRUE );
//  917 		RpSetSfdDetectionExtend();
//  918 	}
//  919 
//  920 	return( result );
//  921 }
//  922 
//  923 /* 3.1.8 */
//  924 /***************************************************************************************************************
//  925  * function name  : RpSetAttr_phyFskFecTxEna
//  926  * description    : Attribute Value Setting Function
//  927  * parameters     : attrValue...The value to be set for the attribute
//  928  * return value   : result
//  929  **************************************************************************************************************/
//  930 static uint8_t RpSetAttr_phyFskFecTxEna( uint8_t attrValue )
//  931 {
//  932 	uint8_t result = RP_SUCCESS;
//  933 
//  934 	/* check parameter */
//  935 	switch ( attrValue )
//  936 	{
//  937 		case 0x00:
//  938 		case 0x01:
//  939 			break;
//  940 		default:
//  941 			result = RP_INVALID_PARAMETER;
//  942 			break;
//  943 	}
//  944 
//  945 	/* set(update) attribute */
//  946 	if ( result == RP_SUCCESS )
//  947 	{
//  948 		RpCb.pib.phyFskFecTxEna = attrValue;
//  949 		RpSetFecVal();
//  950 		RpSetFskOpeModeVal( RP_FALSE );
//  951 		RpSetSfdDetectionExtend();
//  952 	}
//  953 
//  954 	return( result );
//  955 }
//  956 
//  957 /* 3.1.9 */
//  958 /***************************************************************************************************************
//  959  * function name  : RpSetAttr_phyFskFecScheme
//  960  * description    : Attribute Value Setting Function
//  961  * parameters     : attrValue...The value to be set for the attribute
//  962  * return value   : result
//  963  **************************************************************************************************************/
//  964 static uint8_t RpSetAttr_phyFskFecScheme( uint8_t attrValue )
//  965 {
//  966 	uint8_t result = RP_SUCCESS;
//  967 
//  968 	/* check parameter */
//  969 	switch ( attrValue )
//  970 	{
//  971 		case 0x00:	//NRNSC
//  972 		case 0x01:	//RSC
//  973 			break;
//  974 		default:
//  975 			result = RP_INVALID_PARAMETER;
//  976 			break;
//  977 	}
//  978 
//  979 	/* set(update) attribute */
//  980 	if ( result == RP_SUCCESS )
//  981 	{
//  982 		RpCb.pib.phyFskFecScheme = attrValue;
//  983 		RpSetFecVal();
//  984 	}
//  985 
//  986 	return( result );
//  987 }
//  988 
//  989 /* 3.1.10 */
//  990 /***************************************************************************************************************
//  991  * function name  : RpSetAttr_phyLvlFltrVth
//  992  * description    : Attribute Value Setting Function
//  993  * parameters     : attrValue...The value to be set for the attribute
//  994  * return value   : result
//  995  **************************************************************************************************************/
//  996 static uint8_t RpSetAttr_phyLvlFltrVth( uint16_t attrValue )
//  997 {
//  998 	uint8_t result = RP_SUCCESS;
//  999 
// 1000 	/* check parameter */
// 1001 	if ( 0x01FF < attrValue )
// 1002 	{
// 1003 		result = RP_INVALID_PARAMETER;
// 1004 	}
// 1005 
// 1006 	/* set(update) attribute */
// 1007 	if ( result == RP_SUCCESS )
// 1008 	{
// 1009 		RpCb.pib.phyLvlFltrVth = attrValue;
// 1010 		RpSetLvlVthVal();
// 1011 	}
// 1012 
// 1013 	return( result );
// 1014 }
// 1015 
// 1016 /* 3.1.11 */
// 1017 /***************************************************************************************************************
// 1018  * function name  : RpSetAttr_phyBackOffSeed
// 1019  * description    : Attribute Value Setting Function
// 1020  * parameters     : attrValue...The value to be set for the attribute
// 1021  * return value   : result
// 1022  **************************************************************************************************************/
// 1023 static uint8_t RpSetAttr_phyBackOffSeed( uint8_t attrValue )
// 1024 {
// 1025 	uint8_t result = RP_SUCCESS;
// 1026 
// 1027 	/* check parameter */
// 1028 	if ( attrValue < RP_MinValue_phyBackOffSeed )
// 1029 	{
// 1030 		result = RP_INVALID_PARAMETER;
// 1031 	}
// 1032 
// 1033 	/* set(update) attribute */
// 1034 	if ( result == RP_SUCCESS )
// 1035 	{
// 1036 		RpCb.pib.phyBackOffSeed = attrValue;
// 1037 		RpRegWrite(BBBOFFPROD, (uint8_t)(BOFFPRODDIS)); // Backoff periode Auto disable
// 1038 		RpSetBackOffSeedVal();
// 1039 	}
// 1040 
// 1041 	return( result );
// 1042 }
// 1043 
// 1044 /* 3.1.12 */
// 1045 /***************************************************************************************************************
// 1046  * function name  : RpSetAttr_phyCrcErrorUpMsg
// 1047  * description    : Attribute Value Setting Function
// 1048  * parameters     : attrValue...The value to be set for the attribute
// 1049  * return value   : result
// 1050  **************************************************************************************************************/
// 1051 static uint8_t RpSetAttr_phyCrcErrorUpMsg( uint8_t attrValue )
// 1052 {
// 1053 	uint8_t result = RP_SUCCESS;
// 1054 
// 1055 	/* check parameter */
// 1056 	switch ( attrValue )
// 1057 	{
// 1058 		case 0x00:
// 1059 		case 0x01:
// 1060 			break;
// 1061 		default:
// 1062 			result = RP_INVALID_PARAMETER;
// 1063 			break;
// 1064 	}
// 1065 
// 1066 	/* set(update) attribute */
// 1067 	if ( result == RP_SUCCESS )
// 1068 	{
// 1069 		RpCb.pib.phyCrcErrorUpMsg = RP_RX_CRCERROR_IGNORE;
// 1070 		if ( attrValue )
// 1071 		{
// 1072 			RpCb.pib.phyCrcErrorUpMsg = RP_RX_CRCERROR_UPMSG;
// 1073 		}
// 1074 	}
// 1075 
// 1076 	return( result );
// 1077 }
// 1078 
// 1079 /* 3.1.13 */
// 1080 /***************************************************************************************************************
// 1081  * function name  : RpSetAttr_macAddressFilter1Ena
// 1082  * description    : Attribute Value Setting Function
// 1083  * parameters     : attrValue...The value to be set for the attribute
// 1084  * return value   : result
// 1085  **************************************************************************************************************/
// 1086 static uint8_t RpSetAttr_macAddressFilter1Ena( uint8_t attrValue )
// 1087 {
// 1088 	uint8_t result = RP_SUCCESS;
// 1089 
// 1090 	/* check parameter */
// 1091 	switch ( attrValue )
// 1092 	{
// 1093 		case 0x00:
// 1094 		case 0x01:
// 1095 			break;
// 1096 		default:
// 1097 			result = RP_INVALID_PARAMETER;
// 1098 			break;
// 1099 	}
// 1100 
// 1101 	/* set(update) attribute */
// 1102 	if ( result == RP_SUCCESS )
// 1103 	{
// 1104 		RpCb.pib.macAddressFilter1Ena = attrValue;
// 1105 		if ( attrValue )
// 1106 		{
// 1107 			RpSetAckReplyTimeVal();
// 1108 		}
// 1109 		RpSetAdrfAndAutoAckVal();
// 1110 	}
// 1111 
// 1112 	return( result );
// 1113 }
// 1114 
// 1115 /* 3.1.14 */
// 1116 /***************************************************************************************************************
// 1117  * function name  : RpSetAttr_macShortAddress1
// 1118  * description    : Attribute Value Setting Function
// 1119  * parameters     : attrValue...The value to be set for the attribute
// 1120  * return value   : result
// 1121  **************************************************************************************************************/
// 1122 static uint8_t RpSetAttr_macShortAddress1( uint16_t attrValue )
// 1123 {
// 1124 	uint8_t result = RP_SUCCESS;
// 1125 
// 1126 	/* set(update) attribute */
// 1127 	RpCb.pib.macShortAddr1 = attrValue;
// 1128 	RpSetAdrfAndAutoAckVal();
// 1129 
// 1130 	return( result );
// 1131 }
// 1132 
// 1133 /* 3.1.15 */
// 1134 /***************************************************************************************************************
// 1135  * function name  : RpSetAttr_macExtendedAddress1
// 1136  * description    : Attribute Value Setting Function
// 1137  * parameters     : p_attrValue...The value to be set for the attribute
// 1138  * return value   : result
// 1139  **************************************************************************************************************/
// 1140 static uint8_t RpSetAttr_macExtendedAddress1( void RP_FAR *p_attrValue )
// 1141 {
// 1142 	uint8_t result = RP_SUCCESS;
// 1143 
// 1144 	/* check parameter */
// 1145 	if ( p_attrValue == RP_NULL )
// 1146 	{
// 1147 		result = RP_INVALID_PARAMETER;
// 1148 	}
// 1149 
// 1150 	/* set(update) attribute */
// 1151 	RpMemcpy( RpCb.pib.macExtendedAddress1, p_attrValue, sizeof(RpCb.pib.macExtendedAddress1) );
// 1152 	RpSetAdrfAndAutoAckVal();
// 1153 
// 1154 	return( result );
// 1155 }
// 1156 
// 1157 /* 3.1.16 */
// 1158 /***************************************************************************************************************
// 1159  * function name  : RpSetAttr_macPanId1
// 1160  * description    : Attribute Value Setting Function
// 1161  * parameters     : attrValue...The value to be set for the attribute
// 1162  * return value   : result
// 1163  **************************************************************************************************************/
// 1164 static uint8_t RpSetAttr_macPanId1( uint16_t attrValue )
// 1165 {
// 1166 	uint8_t result = RP_SUCCESS;
// 1167 
// 1168 	/* set(update) attribute */
// 1169 	RpCb.pib.macPanId1 = attrValue;
// 1170 	RpSetAdrfAndAutoAckVal();
// 1171 
// 1172 	return( result );
// 1173 }
// 1174 
// 1175 /* 3.1.17 */
// 1176 /***************************************************************************************************************
// 1177  * function name  : RpSetAttr_macPanCoord1
// 1178  * description    : Attribute Value Setting Function
// 1179  * parameters     : attrValue...The value to be set for the attribute
// 1180  * return value   : result
// 1181  **************************************************************************************************************/
// 1182 static uint8_t RpSetAttr_macPanCoord1( uint8_t attrValue )
// 1183 {
// 1184 	uint8_t result = RP_SUCCESS;
// 1185 
// 1186 	/* check parameter */
// 1187 	switch ( attrValue )
// 1188 	{
// 1189 		case 0x00:
// 1190 		case 0x01:
// 1191 			break;
// 1192 		default:
// 1193 			result = RP_INVALID_PARAMETER;
// 1194 			break;
// 1195 	}
// 1196 
// 1197 	/* set(update) attribute */
// 1198 	if ( result == RP_SUCCESS )
// 1199 	{
// 1200 		RpCb.pib.macPanCoord1 = attrValue;
// 1201 		RpSetAdrfAndAutoAckVal();
// 1202 	}
// 1203 
// 1204 	return( result );
// 1205 }
// 1206 
// 1207 /* 3.1.18 */
// 1208 /***************************************************************************************************************
// 1209  * function name  : RpSetAttr_macFramePend1
// 1210  * description    : Attribute Value Setting Function
// 1211  * parameters     : attrValue...The value to be set for the attribute
// 1212  * return value   : result
// 1213  **************************************************************************************************************/
// 1214 static uint8_t RpSetAttr_macFramePend1( uint8_t attrValue )
// 1215 {
// 1216 	uint8_t result = RP_SUCCESS;
// 1217 
// 1218 	/* check parameter */
// 1219 	switch ( attrValue )
// 1220 	{
// 1221 		case 0x00: //ACK pending bit = 0
// 1222 		case 0x01: //ACK pending bit = 1
// 1223 			break;
// 1224 		default:
// 1225 			result = RP_INVALID_PARAMETER;
// 1226 			break;
// 1227 	}
// 1228 
// 1229 	/* set(update) attribute */
// 1230 	if ( result == RP_SUCCESS )
// 1231 	{
// 1232 		RpCb.pib.macPendBit1 = attrValue;
// 1233 		RpSetAdrfAndAutoAckVal();
// 1234 	}
// 1235 
// 1236 	return( result );
// 1237 }
// 1238 
// 1239 /* 3.1.19 */
// 1240 /***************************************************************************************************************
// 1241  * function name  : RpSetAttr_macAddressFilter2Ena
// 1242  * description    : Attribute Value Setting Function
// 1243  * parameters     : attrValue...The value to be set for the attribute
// 1244  * return value   : result
// 1245  **************************************************************************************************************/
// 1246 static uint8_t RpSetAttr_macAddressFilter2Ena( uint8_t attrValue )
// 1247 {
// 1248 	uint8_t result = RP_SUCCESS;
// 1249 
// 1250 	/* check parameter */
// 1251 	switch ( attrValue )
// 1252 	{
// 1253 		case 0x00:
// 1254 		case 0x01:
// 1255 			break;
// 1256 		default:
// 1257 			result = RP_INVALID_PARAMETER;
// 1258 			break;
// 1259 	}
// 1260 
// 1261 	/* set(update) attribute */
// 1262 	if ( result == RP_SUCCESS )
// 1263 	{
// 1264 		RpCb.pib.macAddressFilter2Ena = attrValue;
// 1265 		if ( attrValue )
// 1266 		{
// 1267 			RpSetAckReplyTimeVal();
// 1268 		}
// 1269 		RpSetAdrfAndAutoAckVal();
// 1270 	}
// 1271 
// 1272 	return( result );
// 1273 }
// 1274 
// 1275 /* 3.1.20 */
// 1276 /***************************************************************************************************************
// 1277  * function name  : RpSetAttr_macShortAddress2
// 1278  * description    : Attribute Value Setting Function
// 1279  * parameters     : attrValue...The value to be set for the attribute
// 1280  * return value   : result
// 1281  **************************************************************************************************************/
// 1282 static uint8_t RpSetAttr_macShortAddress2( uint16_t attrValue )
// 1283 {
// 1284 	uint8_t result = RP_SUCCESS;
// 1285 
// 1286 	/* set(update) attribute */
// 1287 	RpCb.pib.macShortAddr2 = attrValue;
// 1288 	RpSetAdrfAndAutoAckVal();
// 1289 
// 1290 	return( result );
// 1291 }
// 1292 
// 1293 /* 3.1.21 */
// 1294 /***************************************************************************************************************
// 1295  * function name  : RpSetAttr_macExtendedAddress2
// 1296  * description    : Attribute Value Setting Function
// 1297  * parameters     : p_attrValue...The value to be set for the attribute
// 1298  * return value   : result
// 1299  **************************************************************************************************************/
// 1300 static uint8_t RpSetAttr_macExtendedAddress2( void RP_FAR *p_attrValue )
// 1301 {
// 1302 	uint8_t result = RP_SUCCESS;
// 1303 
// 1304 	/* check parameter */
// 1305 	if ( p_attrValue == RP_NULL )
// 1306 	{
// 1307 		result = RP_INVALID_PARAMETER;
// 1308 	}
// 1309 
// 1310 	/* set(update) attribute */
// 1311 	if ( result == RP_SUCCESS )
// 1312 	{
// 1313 		RpMemcpy( RpCb.pib.macExtendedAddress2, p_attrValue, sizeof(RpCb.pib.macExtendedAddress2) );
// 1314 		RpSetAdrfAndAutoAckVal();
// 1315 	}
// 1316 
// 1317 	return( result );
// 1318 }
// 1319 
// 1320 /* 3.1.22 */
// 1321 /***************************************************************************************************************
// 1322  * function name  : RpSetAttr_macPanId2
// 1323  * description    : Attribute Value Setting Function
// 1324  * parameters     : attrValue...The value to be set for the attribute
// 1325  * return value   : result
// 1326  **************************************************************************************************************/
// 1327 static uint8_t RpSetAttr_macPanId2( uint16_t attrValue )
// 1328 {
// 1329 	uint8_t result = RP_SUCCESS;
// 1330 
// 1331 	/* set(update) attribute */
// 1332 	RpCb.pib.macPanId2 = attrValue;
// 1333 	RpSetAdrfAndAutoAckVal();
// 1334 
// 1335 	return( result );
// 1336 }
// 1337 
// 1338 /* 3.1.23 */
// 1339 /***************************************************************************************************************
// 1340  * function name  : RpSetAttr_macPanCoord2
// 1341  * description    : Attribute Value Setting Function
// 1342  * parameters     : attrValue...The value to be set for the attribute
// 1343  * return value   : result
// 1344  **************************************************************************************************************/
// 1345 static uint8_t RpSetAttr_macPanCoord2( uint8_t attrValue )
// 1346 {
// 1347 	uint8_t result = RP_SUCCESS;
// 1348 
// 1349 	/* check parameter */
// 1350 	switch ( attrValue )
// 1351 	{
// 1352 		case 0x00:
// 1353 		case 0x01:
// 1354 			break;
// 1355 		default:
// 1356 			result = RP_INVALID_PARAMETER;
// 1357 			break;
// 1358 	}
// 1359 
// 1360 	/* set(update) attribute */
// 1361 	if ( result == RP_SUCCESS )
// 1362 	{
// 1363 		RpCb.pib.macPanCoord2 = attrValue;
// 1364 		RpSetAdrfAndAutoAckVal();
// 1365 	}
// 1366 
// 1367 	return( result );
// 1368 }
// 1369 
// 1370 /* 3.1.24 */
// 1371 /***************************************************************************************************************
// 1372  * function name  : RpSetAttr_macFramePend2
// 1373  * description    : Attribute Value Setting Function
// 1374  * parameters     : attrValue...The value to be set for the attribute
// 1375  * return value   : result
// 1376  **************************************************************************************************************/
// 1377 static uint8_t RpSetAttr_macFramePend2( uint8_t attrValue )
// 1378 {
// 1379 	uint8_t result = RP_SUCCESS;
// 1380 
// 1381 	/* check parameter */
// 1382 	switch ( attrValue )
// 1383 	{
// 1384 		case 0x00: //ACK pending bit = 0
// 1385 		case 0x01: //ACK pending bit = 1
// 1386 			break;
// 1387 		default:
// 1388 			result = RP_INVALID_PARAMETER;
// 1389 			break;
// 1390 	}
// 1391 
// 1392 	/* set(update) attribute */
// 1393 	if ( result == RP_SUCCESS )
// 1394 	{
// 1395 		RpCb.pib.macPendBit2 = attrValue;
// 1396 		RpSetAdrfAndAutoAckVal();
// 1397 	}
// 1398 
// 1399 	return( result );
// 1400 }
// 1401 
// 1402 /* 3.1.25 */
// 1403 /***************************************************************************************************************
// 1404  * function name  : RpSetAttr_macMaxCsmaBackOff
// 1405  * description    : Attribute Value Setting Function
// 1406  * parameters     : attrValue...The value to be set for the attribute
// 1407  * return value   : result
// 1408  **************************************************************************************************************/
// 1409 static uint8_t RpSetAttr_macMaxCsmaBackOff( uint8_t attrValue )
// 1410 {
// 1411 	uint8_t result = RP_SUCCESS;
// 1412 
// 1413 	/* check parameter */
// 1414 	if ( RP_MAXVAL_CSMABACKOFF < attrValue )
// 1415 	{
// 1416 		result = RP_INVALID_PARAMETER;
// 1417 	}
// 1418 
// 1419 	/* set(update) attribute */
// 1420 	if ( result == RP_SUCCESS )
// 1421 	{
// 1422 		RpCb.pib.macMaxCsmaBackOff = attrValue;
// 1423 		RpSetMaxCsmaBackoffVal();
// 1424 	}
// 1425 
// 1426 	return( result );
// 1427 }
// 1428 
// 1429 /* 3.1.26 */
// 1430 /***************************************************************************************************************
// 1431  * function name  : RpSetAttr_macMinBe
// 1432  * description    : Attribute Value Setting Function
// 1433  * parameters     : attrValue...The value to be set for the attribute
// 1434  * return value   : result
// 1435  **************************************************************************************************************/
// 1436 static uint8_t RpSetAttr_macMinBe( uint8_t attrValue )
// 1437 {
// 1438 	uint8_t result = RP_SUCCESS;
// 1439 	uint8_t wk8;
// 1440 
// 1441 	/* check parameter */
// 1442 	if ( RP_MAXVAL_MINBE < attrValue )
// 1443 	{
// 1444 		result = RP_INVALID_PARAMETER;
// 1445 	}
// 1446 
// 1447 	/* set(update) attribute */
// 1448 	if ( result == RP_SUCCESS )
// 1449 	{
// 1450 		RpCb.pib.macMinBe = attrValue;
// 1451 		wk8 = RpRegRead(BBCSMACON3);
// 1452 		wk8 &= ~BEMIN;
// 1453 		wk8 |= RpCb.pib.macMinBe & BEMIN;
// 1454 		RpRegWrite( BBCSMACON3, wk8 );
// 1455 	}
// 1456 
// 1457 	return( result );
// 1458 }
// 1459 
// 1460 /* 3.1.27 */
// 1461 /***************************************************************************************************************
// 1462  * function name  : RpSetAttr_macMaxBe
// 1463  * description    : Attribute Value Setting Function
// 1464  * parameters     : attrValue...The value to be set for the attribute
// 1465  * return value   : result
// 1466  **************************************************************************************************************/
// 1467 static uint8_t RpSetAttr_macMaxBe( uint8_t attrValue )
// 1468 {
// 1469 	uint8_t result = RP_SUCCESS;
// 1470 
// 1471 	/* check parameter */
// 1472 	if (( attrValue < RP_MinValue_macMaxBe )
// 1473 		||( RP_MAXVAL_MAXBE < attrValue ))
// 1474 	{
// 1475 		result = RP_INVALID_PARAMETER;
// 1476 	}
// 1477 
// 1478 	/* set(update) attribute */
// 1479 	if ( result == RP_SUCCESS )
// 1480 	{
// 1481 		RpCb.pib.macMaxBe = attrValue;
// 1482 		RpSetMaxBeVal();
// 1483 	}
// 1484 
// 1485 	return( result );
// 1486 }
// 1487 
// 1488 /* 3.1.28 */
// 1489 /***************************************************************************************************************
// 1490  * function name  : RpSetAttr_macMaxFrameRetries
// 1491  * description    : Attribute Value Setting Function
// 1492  * parameters     : attrValue...The value to be set for the attribute
// 1493  * return value   : result
// 1494  **************************************************************************************************************/
// 1495 static uint8_t RpSetAttr_macMaxFrameRetries( uint8_t attrValue )
// 1496 {
// 1497 	uint8_t result = RP_SUCCESS;
// 1498 
// 1499 	/* check parameter */
// 1500 	if ( RP_MAXVAL_FRAME_RETRIES < attrValue )
// 1501 	{
// 1502 		result = RP_INVALID_PARAMETER;
// 1503 	}
// 1504 
// 1505 	/* set(update) attribute */
// 1506 	if ( result == RP_SUCCESS )
// 1507 	{
// 1508 		RpCb.pib.macMaxFrameRetries = attrValue;
// 1509 	}
// 1510 
// 1511 	return( result );
// 1512 }
// 1513 
// 1514 /* 3.1.29 */
// 1515 /***************************************************************************************************************
// 1516  * function name  : RpSetAttr_macCsmaBackoffPeriod
// 1517  * description    : Attribute Value Setting Function
// 1518  * parameters     : attrValue...The value to be set for the attribute
// 1519  * return value   : result
// 1520  **************************************************************************************************************/
// 1521 static uint8_t RpSetAttr_macCsmaBackoffPeriod( uint16_t attrValue )
// 1522 {
// 1523 	uint8_t result = RP_SUCCESS;
// 1524 
// 1525 	/* check parameter */
// 1526 	if ( attrValue < RP_MINVAL_BACKOFF_PERIOD )
// 1527 	{
// 1528 		result = RP_INVALID_PARAMETER;
// 1529 	}
// 1530 
// 1531 	/* set(update) attribute */
// 1532 	if ( result == RP_SUCCESS )
// 1533 	{
// 1534 		RpCb.pib.phyCsmaBackoffPeriod = attrValue;
// 1535 		RpSetCsmaBackoffPeriod();
// 1536 	}
// 1537 
// 1538 	return( result );
// 1539 }
// 1540 
// 1541 /* 3.1.30 */
// 1542 /***************************************************************************************************************
// 1543  * function name  : RpSetAttr_phyCcaDuration
// 1544  * description    : Attribute Value Setting Function
// 1545  * parameters     : attrValue...The value to be set for the attribute
// 1546  * return value   : result
// 1547  **************************************************************************************************************/
// 1548 static uint8_t RpSetAttr_phyCcaDuration( uint16_t attrValue )
// 1549 {
// 1550 	uint8_t result = RP_SUCCESS;
// 1551 
// 1552 	/* check parameter */
// 1553 	if (( attrValue < RP_MINVAL_CCADURATION ) || (RP_MAXVAL_CCADURATION < attrValue))
// 1554 	{
// 1555 		result = RP_INVALID_PARAMETER;
// 1556 	}
// 1557 
// 1558 	/* set(update) attribute */
// 1559 	if ( result == RP_SUCCESS )
// 1560 	{
// 1561 		RpCb.pib.phyCcaDuration = attrValue;
// 1562 		RpSetCcaDurationVal( RP_TRUE );
// 1563 	}
// 1564 
// 1565 	return( result );
// 1566 }
// 1567 
// 1568 /* 3.1.31 */
// 1569 /***************************************************************************************************************
// 1570  * function name  : RpSetAttr_phyMrFskSfd
// 1571  * description    : Attribute Value Setting Function
// 1572  * parameters     : attrValue...The value to be set for the attribute
// 1573  * return value   : result
// 1574  **************************************************************************************************************/
// 1575 static uint8_t RpSetAttr_phyMrFskSfd( uint8_t attrValue )
// 1576 {
// 1577 	uint8_t result = RP_SUCCESS;
// 1578 
// 1579 	/* check parameter */
// 1580 	switch ( attrValue )
// 1581 	{
// 1582 		case 0x00:
// 1583 		case 0x01:
// 1584 			break;
// 1585 		default:
// 1586 			result = RP_INVALID_PARAMETER;
// 1587 			break;
// 1588 	}
// 1589 
// 1590 	/* set(update) attribute */
// 1591 	if ( result == RP_SUCCESS )
// 1592 	{
// 1593 		RpCb.pib.phyMrFskSfd = attrValue;
// 1594 		RpSetMrFskSfdVal();
// 1595 		RpSetSfdDetectionExtend();
// 1596 	}
// 1597 
// 1598 	return( result );
// 1599 }
// 1600 
// 1601 /* 3.1.32 */
// 1602 /***************************************************************************************************************
// 1603  * function name  : RpSetAttr_phyFskPreambleLength
// 1604  * description    : Attribute Value Setting Function
// 1605  * parameters     : attrValue...The value to be set for the attribute
// 1606  * return value   : result
// 1607  **************************************************************************************************************/
// 1608 static uint8_t RpSetAttr_phyFskPreambleLength( uint16_t attrValue )
// 1609 {
// 1610 	uint8_t result = RP_SUCCESS;
// 1611 
// 1612 	/* check parameter */
// 1613 	if (( attrValue < RP_MINVAL_PREAMBLELEN )
// 1614 		||( RP_MAXVAL_PREAMBLELEN < attrValue ))
// 1615 	{
// 1616 		result = RP_INVALID_PARAMETER;
// 1617 	}
// 1618 
// 1619 	/* set(update) attribute */
// 1620 	if ( result == RP_SUCCESS )
// 1621 	{
// 1622 		RpCb.pib.phyFskPreambleLength = attrValue;
// 1623 		RpSetPreambleLengthVal();
// 1624 	}
// 1625 
// 1626 	return( result );
// 1627 }
// 1628 
// 1629 /* 3.1.33 */
// 1630 /***************************************************************************************************************
// 1631  * function name  : RpSetAttr_phyFskScramblePsdu
// 1632  * description    : Attribute Value Setting Function
// 1633  * parameters     : attrValue...The value to be set for the attribute
// 1634  * return value   : result
// 1635  **************************************************************************************************************/
// 1636 static uint8_t RpSetAttr_phyFskScramblePsdu( uint8_t attrValue )
// 1637 {
// 1638 	uint8_t result = RP_SUCCESS;
// 1639 
// 1640 	/* check parameter */
// 1641 	switch ( attrValue )
// 1642 	{
// 1643 		case 0x00:
// 1644 		case 0x01:
// 1645 			break;
// 1646 		default:
// 1647 			result = RP_INVALID_PARAMETER;
// 1648 			break;
// 1649 	}
// 1650 
// 1651 	/* set(update) attribute */
// 1652 	if ( result == RP_SUCCESS )
// 1653 	{
// 1654 		RpCb.pib.phyFskScramblePsdu = attrValue;
// 1655 		RpSetFskScramblePsduVal();
// 1656 	}
// 1657 
// 1658 	return( result );
// 1659 }
// 1660 
// 1661 /* 3.1.34 */
// 1662 /***************************************************************************************************************
// 1663  * function name  : RpSetAttr_phyFskOpeMode
// 1664  * description    : Attribute Value Setting Function
// 1665  * parameters     : attrValue...The value to be set for the attribute
// 1666  * return value   : result
// 1667  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon0
          CFI Function _RpSetAttr_phyFskOpeMode
        CODE
// 1668 static uint8_t RpSetAttr_phyFskOpeMode( uint8_t attrValue )
// 1669 {
_RpSetAttr_phyFskOpeMode:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       X, A               ;; 1 cycle
// 1670 	uint8_t result = RP_SUCCESS;
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 1671 	uint8_t freqBandId;
// 1672 	uint8_t indexFreqBandId;
// 1673 	uint8_t maxFskOpeMode;
// 1674 
// 1675 	/* get indexFreqBandId */
// 1676 	freqBandId = RpCb.pib.phyFreqBandId;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+234    ;; 2 cycles
// 1677 	switch ( freqBandId )
        MOV       A, B               ;; 1 cycle
        SUB       A, #0x4            ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BC        ??RpPlmeGetReq_16  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        SUB       A, #0xA            ;; 1 cycle
        CMP       A, #0x4            ;; 1 cycle
        BC        ??RpPlmeGetReq_17  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BR        S:??RpPlmeGetReq_18  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1678 	{
// 1679 		case RP_PHY_FREQ_BAND_863MHz:			// (4) 863 - 870 (Europe)
// 1680 		case RP_PHY_FREQ_BAND_896MHz:			// (5) 896 - 901 (US FCC Part 90)
// 1681 		case RP_PHY_FREQ_BAND_901MHz:			// (6) 901 - 902 (US FCC Part 24)
// 1682 		case RP_PHY_FREQ_BAND_915MHz:			// (7) 902 - 928 (US)
// 1683 		case RP_PHY_FREQ_BAND_917MHz:			// (8) 917 - 923.5 (Korea)
// 1684 		case RP_PHY_FREQ_BAND_920MHz:			// (9) 920 - 928 (Japan)
// 1685 			indexFreqBandId = freqBandId - 4;
??RpPlmeGetReq_16:
        MOV       A, B               ;; 1 cycle
        ADD       A, #0xFC           ;; 1 cycle
// 1686 			break;
        BR        S:??RpPlmeGetReq_19  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1687 		//
// 1688 		case RP_PHY_FREQ_BAND_920MHz_Others:	// (14)	920 - 928 (Japan) other settings
// 1689 		case RP_PHY_FREQ_BAND_870MHz:			// (15)	870 - xxx (Europe2)
// 1690 		case RP_PHY_FREQ_BAND_902MHz:			// (16)	902 - xxx (PH/MY/AZ)
// 1691 		case RP_PHY_FREQ_BAND_921MHz:			// (17)	921 - xxx (CH)
// 1692 			indexFreqBandId = freqBandId - 8;
??RpPlmeGetReq_17:
        MOV       A, B               ;; 1 cycle
        ADD       A, #0xF8           ;; 1 cycle
// 1693 			break;
        ; ------------------------------------- Block: 2 cycles
// 1694 		//
// 1695 		default:
// 1696 			result = RP_INVALID_PARAMETER;	// Safty
// 1697 			break;
// 1698 	}
// 1699 
// 1700 	/* check parameter */
// 1701 	if ( result == RP_SUCCESS )
// 1702 	{
// 1703 		if ( attrValue == 0 )
??RpPlmeGetReq_19:
        CMP0      X                  ;; 1 cycle
        BZ        ??RpPlmeGetReq_18  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1704 		{
// 1705 			result = RP_INVALID_PARAMETER;
// 1706 		}
// 1707 		else
// 1708 		{
// 1709 			maxFskOpeMode = g_MaxFskOpeModeTbl[indexFreqBandId];
// 1710 			if ( maxFskOpeMode < attrValue )
        MOV       E, A               ;; 1 cycle
        MOV       D, #0x0            ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #LWRD(_g_MaxFskOpeModeTbl)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOV       ES, #BYTE3(_g_MaxFskOpeModeTbl)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, X               ;; 1 cycle
        BNC       ??RpPlmeGetReq_20  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
// 1711 			{
// 1712 				result = RP_INVALID_PARAMETER;
??RpPlmeGetReq_18:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_21  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1713 			}
// 1714 		}
// 1715 	}
// 1716 
// 1717 	/* set(update) attribute */
// 1718 	if ( result == RP_SUCCESS )
// 1719 	{
// 1720 		RpCb.pib.phyFskOpeMode = attrValue;
??RpPlmeGetReq_20:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       A, E               ;; 1 cycle
        MOV       ES:_RpCb+218, A    ;; 2 cycles
// 1721 		RpSetFskOpeModeVal( RP_FALSE );
        CLRB      A                  ;; 1 cycle
          CFI FunCall _RpSetFskOpeModeVal
        CALL      F:_RpSetFskOpeModeVal  ;; 3 cycles
// 1722 		RpSetAgcStartVth( RP_TRUE );
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetAgcStartVth
        CALL      F:_RpSetAgcStartVth  ;; 3 cycles
// 1723 		RpSetAntennaDiversityStartVth( RP_TRUE );
        ONEB      A                  ;; 1 cycle
          CFI FunCall _RpSetAntennaDiversityStartVth
        CALL      F:_RpSetAntennaDiversityStartVth  ;; 3 cycles
// 1724 		RpCb.pib.phyAntennaSwitchingTime = RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME;
        CLRW      AX                 ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+250, AX   ;; 2 cycles
// 1725 		RpSetSfdDetectionExtend();
          CFI FunCall _RpSetSfdDetectionExtend
        CALL      F:_RpSetSfdDetectionExtend  ;; 3 cycles
// 1726 		RpSetAgcWaitGain();
          CFI FunCall _RpSetAgcWaitGain
        CALL      F:_RpSetAgcWaitGain  ;; 3 cycles
        ; ------------------------------------- Block: 26 cycles
// 1727 	}
// 1728 
// 1729 	return( result );
??RpPlmeGetReq_21:
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        MOV       A, L               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 88 cycles
// 1730 }
// 1731 
// 1732 /* 3.1.35 */
// 1733 /***************************************************************************************************************
// 1734  * function name  : RpSetAttr_phyFcsLength
// 1735  * description    : Attribute Value Setting Function
// 1736  * parameters     : attrValue...The value to be set for the attribute
// 1737  * return value   : result
// 1738  **************************************************************************************************************/
// 1739 static uint8_t RpSetAttr_phyFcsLength( uint8_t attrValue )
// 1740 {
// 1741 	uint8_t result = RP_SUCCESS;
// 1742 
// 1743 	/* check parameter */
// 1744 	switch ( attrValue )
// 1745 	{
// 1746 		case RP_FCS_LENGTH_16BIT:	//0x02
// 1747 		case RP_FCS_LENGTH_32BIT:	//0x04
// 1748 			break;
// 1749 		default:
// 1750 			result = RP_INVALID_PARAMETER;
// 1751 			break;
// 1752 	}
// 1753 
// 1754 	/* set(update) attribute */
// 1755 	if ( result == RP_SUCCESS )
// 1756 	{
// 1757 		RpCb.pib.phyFcsLength = attrValue;
// 1758 		RpSetFcsLengthVal();
// 1759 	}
// 1760 
// 1761 	return( result );
// 1762 }
// 1763 
// 1764 /* 3.1.36 */
// 1765 /***************************************************************************************************************
// 1766  * function name  : RpSetAttr_phyAckReplyTime
// 1767  * description    : Attribute Value Setting Function
// 1768  * parameters     : attrValue...The value to be set for the attribute
// 1769  * return value   : result
// 1770  **************************************************************************************************************/
// 1771 static uint8_t RpSetAttr_phyAckReplyTime( uint16_t attrValue )
// 1772 {
// 1773 	uint8_t result = RP_SUCCESS;
// 1774 
// 1775 	/* check parameter */
// 1776 	if ( attrValue < RP_MINVAL_ACKREPLY_TIME )
// 1777 	{
// 1778 		result = RP_INVALID_PARAMETER;
// 1779 	}
// 1780 
// 1781 	/* set(update) attribute */
// 1782 	if ( result == RP_SUCCESS )
// 1783 	{
// 1784 		RpCb.pib.phyAckReplyTime = attrValue;
// 1785 		RpSetAckReplyTimeVal();
// 1786 	}
// 1787 
// 1788 	return( result );
// 1789 }
// 1790 
// 1791 /* 3.1.37 */
// 1792 /***************************************************************************************************************
// 1793  * function name  : RpSetAttr_phyAckWaitDuration
// 1794  * description    : Attribute Value Setting Function
// 1795  * parameters     : attrValue...The value to be set for the attribute
// 1796  * return value   : result
// 1797  **************************************************************************************************************/
// 1798 static uint8_t RpSetAttr_phyAckWaitDuration( uint16_t attrValue )
// 1799 {
// 1800 	uint8_t result = RP_SUCCESS;
// 1801 
// 1802 	/* check parameter */
// 1803 	if ( attrValue < RP_MINVAL_ACKWAIT_DURATION )
// 1804 	{
// 1805 		result = RP_INVALID_PARAMETER;
// 1806 	}
// 1807 
// 1808 	/* set(update) attribute */
// 1809 	if ( result == RP_SUCCESS )
// 1810 	{
// 1811 		RpCb.pib.phyAckWaitDuration = attrValue;
// 1812 		RpSetAckWaitDurationVal();
// 1813 	}
// 1814 
// 1815 	return( result );
// 1816 }
// 1817 
// 1818 /* 3.1.38 */
// 1819 /***************************************************************************************************************
// 1820  * function name  : RpSetAttr_phyProfileSpecificMode
// 1821  * description    : Attribute Value Setting Function
// 1822  * parameters     : attrValue...The value to be set for the attribute
// 1823  * return value   : result
// 1824  **************************************************************************************************************/
// 1825 static uint8_t RpSetAttr_phyProfileSpecificMode( uint8_t attrValue )
// 1826 {
// 1827 	uint8_t result = RP_SUCCESS;
// 1828 
// 1829 	/* check parameter */
// 1830 	switch ( attrValue )
// 1831 	{
// 1832 		case RP_SPECIFIC_NORMAL: //0x00:
// 1833 		case RP_SPECIFIC_WSUN:	 //0x01
// 1834 			break;
// 1835 		default:
// 1836 			result = RP_INVALID_PARAMETER;
// 1837 			break;
// 1838 	}
// 1839 
// 1840 	/* set(update) attribute */
// 1841 	if ( result == RP_SUCCESS )
// 1842 	{
// 1843 		RpCb.pib.phyProfileSpecificMode = attrValue;
// 1844 		RpSetSpecificModeVal( RP_FALSE );
// 1845 	}
// 1846 
// 1847 	return( result );
// 1848 }
// 1849 
// 1850 /* 3.1.39 */
// 1851 /***************************************************************************************************************
// 1852  * function name  : RpSetAttr_phyAntennaSwitchEna
// 1853  * description    : Attribute Value Setting Function
// 1854  * parameters     : attrValue...The value to be set for the attribute
// 1855  * return value   : result
// 1856  **************************************************************************************************************/
// 1857 static uint8_t RpSetAttr_phyAntennaSwitchEna( uint8_t attrValue )
// 1858 {
// 1859 	uint8_t result = RP_SUCCESS;
// 1860 
// 1861 	/* check parameter */
// 1862 	switch ( attrValue )
// 1863 	{
// 1864 		case 0x00:
// 1865 		case 0x01:
// 1866 			break;
// 1867 		default:
// 1868 			result = RP_INVALID_PARAMETER;
// 1869 			break;
// 1870 	}
// 1871 
// 1872 	/* set(update) attribute */
// 1873 	if ( result == RP_SUCCESS )
// 1874 	{
// 1875 		RpCb.pib.phyAntennaSwitchEna = attrValue;
// 1876 		RpSetAntennaSwitchVal();
// 1877 	}
// 1878 
// 1879 	return( result );
// 1880 }
// 1881 
// 1882 /* 3.1.40 */
// 1883 /***************************************************************************************************************
// 1884  * function name  : RpSetAttr_phyAntennaDiversityRxEna
// 1885  * description    : Attribute Value Setting Function
// 1886  * parameters     : attrValue...The value to be set for the attribute
// 1887  * return value   : result
// 1888  **************************************************************************************************************/
// 1889 static uint8_t RpSetAttr_phyAntennaDiversityRxEna( uint8_t attrValue )
// 1890 {
// 1891 	uint8_t result = RP_SUCCESS;
// 1892 
// 1893 	/* check parameter */
// 1894 	switch ( attrValue )
// 1895 	{
// 1896 		case 0x00:
// 1897 		case 0x01:
// 1898 			break;
// 1899 		default:
// 1900 			result = RP_INVALID_PARAMETER;
// 1901 			break;
// 1902 	}
// 1903 
// 1904 	/* set(update) attribute */
// 1905 	if ( result == RP_SUCCESS )
// 1906 	{
// 1907 		RpCb.pib.phyAntennaDiversityRxEna = attrValue;
// 1908 		RpSetAntennaDiversityVal();
// 1909 		RpSetAntennaSelectTxVal();
// 1910 		RpSetFskOpeModeVal( RP_FALSE );
// 1911 	}
// 1912 
// 1913 	return( result );
// 1914 }
// 1915 
// 1916 /* 3.1.41 */
// 1917 /***************************************************************************************************************
// 1918  * function name  : RpSetAttr_phyAntennaSelectTx
// 1919  * description    : Attribute Value Setting Function
// 1920  * parameters     : attrValue...The value to be set for the attribute
// 1921  * return value   : result
// 1922  **************************************************************************************************************/
// 1923 static uint8_t RpSetAttr_phyAntennaSelectTx( uint8_t attrValue )
// 1924 {
// 1925 	uint8_t result = RP_SUCCESS;
// 1926 
// 1927 	/* check parameter */
// 1928 	switch ( attrValue )
// 1929 	{
// 1930 		case 0x00:
// 1931 		case 0x01:
// 1932 			break;
// 1933 		default:
// 1934 			result = RP_INVALID_PARAMETER;
// 1935 			break;
// 1936 	}
// 1937 
// 1938 	/* set(update) attribute */
// 1939 	if ( result == RP_SUCCESS )
// 1940 	{
// 1941 		RpCb.pib.phyAntennaSelectTx = attrValue;
// 1942 		RpSetAntennaSelectTxVal();
// 1943 	}
// 1944 
// 1945 	return( result );
// 1946 }
// 1947 
// 1948 /* 3.1.42 */
// 1949 /***************************************************************************************************************
// 1950  * function name  : RpSetAttr_phyAntennaSelectAckTx
// 1951  * description    : Attribute Value Setting Function
// 1952  * parameters     : attrValue...The value to be set for the attribute
// 1953  * return value   : result
// 1954  **************************************************************************************************************/
// 1955 static uint8_t RpSetAttr_phyAntennaSelectAckTx( uint8_t attrValue )
// 1956 {
// 1957 	uint8_t result = RP_SUCCESS;
// 1958 
// 1959 	/* check parameter */
// 1960 	switch ( attrValue )
// 1961 	{
// 1962 		case RP_ANT_SEL_ACK_TX_ANT0:			//0x00
// 1963 		case RP_ANT_SEL_ACK_TX_ANT1:			//0x01
// 1964 		case RP_ANT_SEL_ACK_TX_RCVD_ANT:		//0x02
// 1965 		case RP_ANT_SEL_ACK_TX_RCVD_INV_ANT:	//0x03
// 1966 			break;
// 1967 		default:
// 1968 			result = RP_INVALID_PARAMETER;
// 1969 			break;
// 1970 	}
// 1971 
// 1972 	/* set(update) attribute */
// 1973 	if ( result == RP_SUCCESS )
// 1974 	{
// 1975 		RpCb.pib.phyAntennaSelectAckTx = attrValue;
// 1976 		RpSetAntennaDiversityVal();
// 1977 	}
// 1978 
// 1979 	return( result );
// 1980 }
// 1981 
// 1982 /* 3.1.43 */
// 1983 /***************************************************************************************************************
// 1984  * function name  : RpSetAttr_phyAntennaSelectAckRx
// 1985  * description    : Attribute Value Setting Function
// 1986  * parameters     : attrValue...The value to be set for the attribute
// 1987  * return value   : result
// 1988  **************************************************************************************************************/
// 1989 static uint8_t RpSetAttr_phyAntennaSelectAckRx( uint8_t attrValue )
// 1990 {
// 1991 	uint8_t result = RP_SUCCESS;
// 1992 
// 1993 	/* check parameter */
// 1994 	switch ( attrValue )
// 1995 	{
// 1996 		case RP_ANT_SEL_ACK_RX_ANT0: //0x00:
// 1997 		case RP_ANT_SEL_ACK_RX_ANT1: //0x01:
// 1998 		case RP_ANT_SEL_ACK_RX_AUTO: //0x02:
// 1999 			break;
// 2000 		default:
// 2001 			result = RP_INVALID_PARAMETER;
// 2002 			break;
// 2003 	}
// 2004 
// 2005 	/* set(update) attribute */
// 2006 	if ( result == RP_SUCCESS )
// 2007 	{
// 2008 		RpCb.pib.phyAntennaSelectAckRx = attrValue;
// 2009 		RpSetAntennaDiversityVal();
// 2010 	}
// 2011 
// 2012 	return( result );
// 2013 }
// 2014 
// 2015 /* 3.1.44 */
// 2016 /***************************************************************************************************************
// 2017  * function name  : RpSetAttr_phyRxTimeoutMode
// 2018  * description    : Attribute Value Setting Function
// 2019  * parameters     : attrValue...The value to be set for the attribute
// 2020  * return value   : result
// 2021  **************************************************************************************************************/
// 2022 static uint8_t RpSetAttr_phyRxTimeoutMode( uint8_t attrValue )
// 2023 {
// 2024 	uint8_t result = RP_SUCCESS;
// 2025 
// 2026 	/* check parameter */
// 2027 	switch ( attrValue )
// 2028 	{
// 2029 		case 0x00:
// 2030 		case 0x01:
// 2031 			break;
// 2032 		default:
// 2033 			result = RP_INVALID_PARAMETER;
// 2034 			break;
// 2035 	}
// 2036 
// 2037 	/* set(update) attribute */
// 2038 	if ( result == RP_SUCCESS )
// 2039 	{
// 2040 		RpCb.pib.phyRxTimeoutMode = attrValue;
// 2041 	}
// 2042 
// 2043 	return( result );
// 2044 }
// 2045 
// 2046 /* 3.1.45 */
// 2047 /***************************************************************************************************************
// 2048  * function name  : RpSetAttr_phyFreqBandId
// 2049  * description    : Attribute Value Setting Function
// 2050  * parameters     : attrValue...The value to be set for the attribute
// 2051  * return value   : result
// 2052  **************************************************************************************************************/
// 2053 static uint8_t RpSetAttr_phyFreqBandId( uint8_t attrValue )
// 2054 {
// 2055 	uint8_t result = RP_SUCCESS;
// 2056 
// 2057 	/* check parameter */
// 2058 	switch ( attrValue )
// 2059 	{
// 2060 		case RP_PHY_FREQ_BAND_863MHz: //4:863-870   (Europe)
// 2061 		case RP_PHY_FREQ_BAND_896MHz: //5:896-901   (US FCC Part 90)
// 2062 		case RP_PHY_FREQ_BAND_901MHz: //6:901-902   (US FCC Part 24)
// 2063 		case RP_PHY_FREQ_BAND_915MHz: //7:902-928   (US)
// 2064 		case RP_PHY_FREQ_BAND_917MHz: //8:917-923.5 (Korea)
// 2065 		case RP_PHY_FREQ_BAND_920MHz: //9:920-928   (Japan)
// 2066 		//
// 2067 		case RP_PHY_FREQ_BAND_920MHz_Others: //14:920-928 (Japan) other settings
// 2068 		case RP_PHY_FREQ_BAND_870MHz:		 //15:870-xxx (Europe2)
// 2069 		case RP_PHY_FREQ_BAND_902MHz:		 //16:902-xxx (PH/MY/AZ)
// 2070 		case RP_PHY_FREQ_BAND_921MHz:		 //17:921-xxx (CH)
// 2071 			break;
// 2072 		default:
// 2073 			result = RP_INVALID_PARAMETER;
// 2074 			break;
// 2075 	}
// 2076 
// 2077 	/* set(update) attribute */
// 2078 	if ( result == RP_SUCCESS )
// 2079 	{
// 2080 		RpCb.pib.phyFreqBandId = attrValue;
// 2081 		RpSetFskOpeModeVal( RP_FALSE );
// 2082 		RpSetAgcStartVth( RP_TRUE );
// 2083 		RpSetAntennaDiversityStartVth( RP_TRUE );
// 2084 		RpCb.pib.phyAntennaSwitchingTime = RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME;
// 2085 		RpSetSfdDetectionExtend();
// 2086 		RpSetAgcWaitGain();
// 2087 	}
// 2088 
// 2089 	return( result );
// 2090 }
// 2091 
// 2092 /* 3.1.47 */
// 2093 /***************************************************************************************************************
// 2094  * function name  : RpSetAttr_phyRegulatoryMode
// 2095  * description    : Attribute Value Setting Function
// 2096  * parameters     : attrValue...The value to be set for the attribute
// 2097  * return value   : result
// 2098  **************************************************************************************************************/
// 2099 static uint8_t RpSetAttr_phyRegulatoryMode( uint8_t attrValue )
// 2100 {
// 2101 	uint8_t result = RP_SUCCESS;
// 2102 
// 2103 	/* check parameter */
// 2104 	switch ( attrValue )
// 2105 	{
// 2106 		case RP_RMODE_DISABLE:		//0x00:
// 2107 		case RP_RMODE_ENABLE:		//0x01:
// 2108 		case RP_RMODE_ENABLE_ARIB:	//0x02:
// 2109 			break;
// 2110 		default:
// 2111 			result = RP_INVALID_PARAMETER;
// 2112 			break;
// 2113 	}
// 2114 
// 2115 	/* set(update) attribute */
// 2116 	if ( result == RP_SUCCESS )
// 2117 	{
// 2118 		if ( attrValue != RpCb.pib.phyRegulatoryMode )
// 2119 		{
// 2120 			RpCb.pib.phyRegulatoryMode = attrValue;
// 2121 			RpSetMacTxLimitMode();
// 2122 		}
// 2123 	}
// 2124 
// 2125 	return( result );
// 2126 }
// 2127 
// 2128 /* 3.1.48 */
// 2129 /***************************************************************************************************************
// 2130  * function name  : RpSetAttr_phyPreamble4byteRxMode
// 2131  * description    : Attribute Value Setting Function
// 2132  * parameters     : attrValue...The value to be set for the attribute
// 2133  * return value   : result
// 2134  **************************************************************************************************************/
// 2135 static uint8_t RpSetAttr_phyPreamble4byteRxMode( uint8_t attrValue )
// 2136 {
// 2137 	uint8_t result = RP_SUCCESS;
// 2138 
// 2139 	/* check parameter */
// 2140 	switch ( attrValue )
// 2141 	{
// 2142 		case 0x00:
// 2143 		case 0x01:
// 2144 			break;
// 2145 		default:
// 2146 			result = RP_INVALID_PARAMETER;
// 2147 			break;
// 2148 	}
// 2149 
// 2150 	/* set(update) attribute */
// 2151 	if ( result == RP_SUCCESS )
// 2152 	{
// 2153 		RpCb.pib.phyPreamble4ByteRxMode = attrValue;
// 2154 		RpSetPreamble4ByteRxMode();
// 2155 	}
// 2156 
// 2157 	return( result );
// 2158 }
// 2159 
// 2160 /* 3.1.49 */
// 2161 /***************************************************************************************************************
// 2162  * function name  : RpSetAttr_phyAgcStartVth
// 2163  * description    : Attribute Value Setting Function
// 2164  * parameters     : attrValue...The value to be set for the attribute
// 2165  * return value   : result
// 2166  **************************************************************************************************************/
// 2167 static uint8_t RpSetAttr_phyAgcStartVth( uint16_t attrValue )
// 2168 {
// 2169 	uint8_t result = RP_SUCCESS;
// 2170 
// 2171 	/* check parameter */
// 2172 	if ( 0x387D < attrValue )
// 2173 	{
// 2174 		result = RP_INVALID_PARAMETER;
// 2175 	}
// 2176 	else
// 2177 	{
// 2178 		if ( attrValue < 0x000F )
// 2179 		{
// 2180 			if ( attrValue != 0x00 )
// 2181 			{
// 2182 				result = RP_INVALID_PARAMETER;
// 2183 			}
// 2184 		}
// 2185 	}
// 2186 
// 2187 	/* set(update) attribute */
// 2188 	if ( result == RP_SUCCESS )
// 2189 	{
// 2190 		RpCb.pib.phyAgcStartVth = attrValue;
// 2191 		RpSetAgcStartVth( RP_FALSE );
// 2192 	}
// 2193 
// 2194 	return( result );
// 2195 }
// 2196 
// 2197 /* 3.1.50 */
// 2198 /***************************************************************************************************************
// 2199  * function name  : RpSetAttr_phyCcaBandwidth
// 2200  * description    : Attribute Value Setting Function
// 2201  * parameters     : attrValue...The value to be set for the attribute
// 2202  * return value   : result
// 2203  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon0
          CFI Function _RpSetAttr_phyCcaBandwidth
        CODE
// 2204 static uint8_t RpSetAttr_phyCcaBandwidth( uint8_t attrValue )
// 2205 {
_RpSetAttr_phyCcaBandwidth:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       C, A               ;; 1 cycle
// 2206 	uint8_t result = RP_SUCCESS;
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2207 	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+246    ;; 2 cycles
// 2208 	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
        MOV       X, ES:_RpCb+247    ;; 2 cycles
// 2209 	uint8_t freqId = RpCb.freqIdIndex;
// 2210 
// 2211 	/* check parameter */
// 2212 	switch ( attrValue )
        MOV       A, C               ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpPlmeGetReq_22  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_23  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BZ        ??RpPlmeGetReq_24  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        S:??RpPlmeGetReq_25  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2213 	{
// 2214 		case 0x00:
// 2215 			break;
// 2216 		case 0x01:
// 2217 		case 0x02:
// 2218 			switch ( ccaBandwidth )
??RpPlmeGetReq_23:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??RpPlmeGetReq_26  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2219 			{
// 2220 				case 0x03:
// 2221 					result = RP_INVALID_PARAMETER;
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2222 					break;
        ; ------------------------------------- Block: 2 cycles
// 2223 				default:
// 2224 					break;
// 2225 			}
// 2226 			switch ( edBandwidth )
??RpPlmeGetReq_26:
        MOV       A, X               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BZ        ??RpPlmeGetReq_25  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2227 			{
// 2228 				case 0x03:
// 2229 					result = RP_INVALID_PARAMETER;
// 2230 					break;
// 2231 				default:
// 2232 					break;
// 2233 			}
// 2234 			break;
// 2235 		case 0x03:
// 2236 			switch ( ccaBandwidth )
// 2237 			{
// 2238 				case 0x01:
// 2239 				case 0x02:
// 2240 					result = RP_INVALID_PARAMETER;
// 2241 					break;
// 2242 				default:
// 2243 					break;
// 2244 			}
// 2245 			switch ( edBandwidth )
// 2246 			{
// 2247 				case 0x01:
// 2248 				case 0x02:
// 2249 					result = RP_INVALID_PARAMETER;
// 2250 					break;
// 2251 				default:
// 2252 					break;
// 2253 			}
// 2254 			switch ( freqId )
// 2255 			{
// 2256 				case 0:
// 2257 				case 1:
// 2258 				case 2:
// 2259 				case 18:
// 2260 				case 19:
// 2261 					result = RP_INVALID_PARAMETER;
// 2262 					break;
// 2263 				default:
// 2264 					break;		
// 2265 			}
// 2266 			break;
// 2267 		default:
// 2268 			result = RP_INVALID_PARAMETER;
// 2269 			break;
// 2270 	}
// 2271 
// 2272 	/* set(update) attribute */
// 2273 	if ( result == RP_SUCCESS )
??RpPlmeGetReq_22:
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x7            ;; 1 cycle
        BNZ       ??RpPlmeGetReq_27  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2274 	{
// 2275 		RpCb.pib.phyCcaBandwidth = attrValue;
        MOV       A, C               ;; 1 cycle
        MOV       ES:_RpCb+246, A    ;; 2 cycles
// 2276 		RpSetCcaEdBandwidth();
          CFI FunCall _RpSetCcaEdBandwidth
        CALL      F:_RpSetCcaEdBandwidth  ;; 3 cycles
// 2277 		RpSetCcaVthVal();
          CFI FunCall _RpSetCcaVthVal
        CALL      F:_RpSetCcaVthVal  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 2278 	}
// 2279 
// 2280 	return( result );
??RpPlmeGetReq_27:
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        MOV       A, L               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI CFA SP+6
        ; ------------------------------------- Block: 8 cycles
??RpPlmeGetReq_24:
        MOV       A, B               ;; 1 cycle
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNC       ??RpPlmeGetReq_28  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpPlmeGetReq_28:
        MOV       A, X               ;; 1 cycle
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNC       ??RpPlmeGetReq_29  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpPlmeGetReq_29:
        MOV       A, ES:_RpCb+342    ;; 2 cycles
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpPlmeGetReq_25  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        SUB       A, #0x12           ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNC       ??RpPlmeGetReq_22  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_25:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_27  ;; 3 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 5 cycles
        ; ------------------------------------- Total: 101 cycles
// 2281 }
// 2282 
// 2283 /* 3.1.51 */
// 2284 /***************************************************************************************************************
// 2285  * function name  : RpSetAttr_phyEdBandwidth
// 2286  * description    : Attribute Value Setting Function
// 2287  * parameters     : attrValue...The value to be set for the attribute
// 2288  * return value   : result
// 2289  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon0
          CFI Function _RpSetAttr_phyEdBandwidth
        CODE
// 2290 static uint8_t RpSetAttr_phyEdBandwidth( uint8_t attrValue )
// 2291 {
_RpSetAttr_phyEdBandwidth:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       C, A               ;; 1 cycle
// 2292 	uint8_t result = RP_SUCCESS;
        MOV       A, #0x7            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2293 	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+247    ;; 2 cycles
// 2294 	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
        MOV       X, ES:_RpCb+246    ;; 2 cycles
// 2295 	uint8_t freqId = RpCb.freqIdIndex;
// 2296 
// 2297 	/* check parameter */
// 2298 	switch ( attrValue )
        MOV       A, C               ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RpPlmeGetReq_30  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_31  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        BZ        ??RpPlmeGetReq_32  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BR        S:??RpPlmeGetReq_33  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2299 	{
// 2300 		case 0x00:
// 2301 			break;
// 2302 		case 0x01:
// 2303 		case 0x02:
// 2304 			switch ( edBandwidth )
??RpPlmeGetReq_31:
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BNZ       ??RpPlmeGetReq_34  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2305 			{
// 2306 				case 0x03:
// 2307 					result = RP_INVALID_PARAMETER;
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 2308 					break;
        ; ------------------------------------- Block: 2 cycles
// 2309 				default:
// 2310 					break;
// 2311 			}
// 2312 			switch ( ccaBandwidth )
??RpPlmeGetReq_34:
        MOV       A, X               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BZ        ??RpPlmeGetReq_33  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2313 			{
// 2314 				case 0x03:
// 2315 					result = RP_INVALID_PARAMETER;
// 2316 					break;
// 2317 				default:
// 2318 					break;
// 2319 			}
// 2320 			break;
// 2321 		case 0x03:
// 2322 			switch ( edBandwidth )
// 2323 			{
// 2324 				case 0x01:
// 2325 				case 0x02:
// 2326 					result = RP_INVALID_PARAMETER;
// 2327 					break;
// 2328 				default:
// 2329 					break;
// 2330 			}
// 2331 			switch ( ccaBandwidth )
// 2332 			{
// 2333 				case 0x01:
// 2334 				case 0x02:
// 2335 					result = RP_INVALID_PARAMETER;
// 2336 					break;
// 2337 				default:
// 2338 					break;
// 2339 			}
// 2340 			switch ( freqId )
// 2341 			{
// 2342 				case 0:
// 2343 				case 1:
// 2344 				case 2:
// 2345 				case 18:
// 2346 				case 19:
// 2347 					result = RP_INVALID_PARAMETER;
// 2348 					break;
// 2349 				default:
// 2350 					break;		
// 2351 			}
// 2352 			break;
// 2353 		default:
// 2354 			result = RP_INVALID_PARAMETER;
// 2355 			break;
// 2356 	}
// 2357 
// 2358 	/* set(update) attribute */
// 2359 	if ( result == RP_SUCCESS )
??RpPlmeGetReq_30:
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x7            ;; 1 cycle
        BNZ       ??RpPlmeGetReq_35  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2360 	{
// 2361 		RpCb.pib.phyEdBandwidth = attrValue;
        MOV       A, C               ;; 1 cycle
        MOV       ES:_RpCb+247, A    ;; 2 cycles
// 2362 		RpSetCcaEdBandwidth();
          CFI FunCall _RpSetCcaEdBandwidth
        CALL      F:_RpSetCcaEdBandwidth  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2363 	}
// 2364 
// 2365 	return( result );
??RpPlmeGetReq_35:
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        MOV       A, L               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI CFA SP+6
        ; ------------------------------------- Block: 8 cycles
??RpPlmeGetReq_32:
        MOV       A, B               ;; 1 cycle
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNC       ??RpPlmeGetReq_36  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpPlmeGetReq_36:
        MOV       A, X               ;; 1 cycle
        DEC       A                  ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNC       ??RpPlmeGetReq_37  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RpPlmeGetReq_37:
        MOV       A, ES:_RpCb+342    ;; 2 cycles
        CMP       A, #0x3            ;; 1 cycle
        BC        ??RpPlmeGetReq_33  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        SUB       A, #0x12           ;; 1 cycle
        CMP       A, #0x2            ;; 1 cycle
        BNC       ??RpPlmeGetReq_30  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_33:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_35  ;; 3 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 5 cycles
        ; ------------------------------------- Total: 98 cycles
// 2366 }
// 2367 
// 2368 /* 3.1.52 */
// 2369 /***************************************************************************************************************
// 2370  * function name  : RpSetAttr_phyAntennaDiversityStartVth
// 2371  * description    : Attribute Value Setting Function
// 2372  * parameters     : attrValue...The value to be set for the attribute
// 2373  * return value   : result
// 2374  **************************************************************************************************************/
// 2375 static uint8_t RpSetAttr_phyAntennaDiversityStartVth( uint16_t attrValue )
// 2376 {
// 2377 	uint8_t result = RP_SUCCESS;
// 2378 
// 2379 	/* check parameter */
// 2380 	if ( 0x01FF < attrValue )
// 2381 	{
// 2382 		result = RP_INVALID_PARAMETER;
// 2383 	}
// 2384 
// 2385 	/* set(update) attribute */
// 2386 	if ( result == RP_SUCCESS )
// 2387 	{
// 2388 		RpCb.pib.phyAntennaDiversityStartVth = attrValue;
// 2389 		RpSetAntennaDiversityStartVth( RP_FALSE );
// 2390 	}
// 2391 
// 2392 	return( result );
// 2393 }
// 2394 
// 2395 /* 3.1.53 */
// 2396 /***************************************************************************************************************
// 2397  * function name  : RpSetAttr_phyAntennaSwitchingTime
// 2398  * description    : Attribute Value Setting Function
// 2399  * parameters     : attrValue...The value to be set for the attribute
// 2400  * return value   : result
// 2401  **************************************************************************************************************/
// 2402 static uint8_t RpSetAttr_phyAntennaSwitchingTime( uint16_t attrValue )
// 2403 {
// 2404 	uint8_t result = RP_SUCCESS;
// 2405 
// 2406 	/* set(update) attribute */
// 2407 	RpCb.pib.phyAntennaSwitchingTime = attrValue;
// 2408 
// 2409 	return( result );
// 2410 }
// 2411 
// 2412 /* 3.1.54 */
// 2413 /***************************************************************************************************************
// 2414  * function name  : RpSetAttr_phySfdDetectionExtend
// 2415  * description    : Attribute Value Setting Function
// 2416  * parameters     : attrValue...The value to be set for the attribute
// 2417  * return value   : result
// 2418  **************************************************************************************************************/
// 2419 static uint8_t RpSetAttr_phySfdDetectionExtend( uint8_t attrValue )
// 2420 {
// 2421 	uint8_t result = RP_SUCCESS;
// 2422 
// 2423 	/* check parameter */
// 2424 	switch ( attrValue )
// 2425 	{
// 2426 		case 0x00:
// 2427 		case 0x01:
// 2428 			break;
// 2429 		default:
// 2430 			result = RP_INVALID_PARAMETER;
// 2431 			break;
// 2432 	}
// 2433 
// 2434 	/* set(update) attribute */
// 2435 	if ( result == RP_SUCCESS )
// 2436 	{
// 2437 		RpCb.pib.phySfdDetectionExtend = attrValue;
// 2438 		RpSetSfdDetectionExtend();
// 2439 	}
// 2440 
// 2441 	return( result );
// 2442 }
// 2443 
// 2444 /* 3.1.55 */
// 2445 /***************************************************************************************************************
// 2446  * function name  : RpSetAttr_phyAgcWaitGainOffset
// 2447  * description    : Attribute Value Setting Function
// 2448  * parameters     : attrValue...The value to be set for the attribute
// 2449  * return value   : result
// 2450  **************************************************************************************************************/
// 2451 static uint8_t RpSetAttr_phyAgcWaitGainOffset( uint8_t attrValue )
// 2452 {
// 2453 	uint8_t result = RP_SUCCESS;
// 2454 
// 2455 	/* check parameter */
// 2456 	if ( RP_MAXVAL_AGC_WAIT_GAIN_OFFSET < attrValue )
// 2457 	{
// 2458 		result = RP_INVALID_PARAMETER;
// 2459 	}
// 2460 	
// 2461 	/* set(update) attribute */
// 2462 	if ( result == RP_SUCCESS )
// 2463 	{
// 2464 		RpCb.pib.phyAgcWaitGainOffset = attrValue;
// 2465 		RpSetAgcWaitGain();
// 2466 	}
// 2467 
// 2468 	return( result );
// 2469 }
// 2470 
// 2471 /* 3.1.56 */
// 2472 /***************************************************************************************************************
// 2473  * function name  : RpSetAttr_phyCcaVthOffset
// 2474  * description    : Attribute Value Setting Function
// 2475  * parameters     : attrValue...The value to be set for the attribute
// 2476  * return value   : result
// 2477  **************************************************************************************************************/
// 2478 static uint8_t RpSetAttr_phyCcaVthOffset( uint8_t attrValue )
// 2479 {
// 2480 	uint8_t result = RP_SUCCESS;
// 2481 
// 2482 	/* check parameter */
// 2483 	if ( RP_MAXVAL_CCA_VTH_OFFSET < attrValue )
// 2484 	{
// 2485 		result = RP_INVALID_PARAMETER;
// 2486 	}
// 2487 
// 2488 	/* set(update) attribute */
// 2489 	if ( result == RP_SUCCESS )
// 2490 	{
// 2491 		RpCb.pib.phyCcaVthOffset = attrValue;
// 2492 		RpSetCcaVthVal();
// 2493 	}
// 2494 
// 2495 	return( result );
// 2496 }
// 2497 
// 2498 /* 3.1.57 */
// 2499 /***************************************************************************************************************
// 2500  * function name  : RpSetAttr_phyAntennaSwitchEnaTiming
// 2501  * description    : Attribute Value Setting Function
// 2502  * parameters     : attrValue...The value to be set for the attribute
// 2503  * return value   : result
// 2504  **************************************************************************************************************/
// 2505 static uint8_t RpSetAttr_phyAntennaSwitchEnaTiming( uint16_t attrValue )
// 2506 {
// 2507 	uint8_t result = RP_SUCCESS;
// 2508 
// 2509 	/* check parameter */
// 2510 	if (( attrValue < RP_MINVAL_ANTSWENA_TIMING )
// 2511 		||( RP_MAXVAL_ANTSWENA_TIMING < attrValue ))
// 2512 	{
// 2513 		result = RP_INVALID_PARAMETER;
// 2514 	}
// 2515 
// 2516 	/* set(update) attribute */
// 2517 	if ( result == RP_SUCCESS )
// 2518 	{
// 2519 		RpCb.pib.phyAntennaSwitchEnaTiming = attrValue;
// 2520 		RpSetAntennaSwitchEnaTiming();
// 2521 	}
// 2522 
// 2523 	return( result );
// 2524 }
// 2525 
// 2526 /* 3.1.58 */
// 2527 /***************************************************************************************************************
// 2528  * function name  : RpSetAttr_phyGpio0Setting
// 2529  * description    : Attribute Value Setting Function
// 2530  * parameters     : attrValue...The value to be set for the attribute
// 2531  * return value   : result
// 2532  **************************************************************************************************************/
// 2533 static uint8_t RpSetAttr_phyGpio0Setting( uint8_t attrValue )
// 2534 {
// 2535 	uint8_t result = RP_SUCCESS;
// 2536 
// 2537 	/* check parameter */
// 2538 	switch ( attrValue )
// 2539 	{
// 2540 		case 0x00:
// 2541 		case 0x01:
// 2542 			break;
// 2543 		default:
// 2544 			result = RP_INVALID_PARAMETER;
// 2545 			break;
// 2546 	}
// 2547 
// 2548 	/* set(update) attribute */
// 2549 	if ( result == RP_SUCCESS )
// 2550 	{
// 2551 		RpCb.pib.phyGpio0Setting = attrValue;
// 2552 		RpSetGpio0Setting();
// 2553 	}
// 2554 
// 2555 	return( result );
// 2556 }
// 2557 
// 2558 /* 3.1.59 */
// 2559 /***************************************************************************************************************
// 2560  * function name  : RpSetAttr_phyGpio3Setting
// 2561  * description    : Attribute Value Setting Function
// 2562  * parameters     : attrValue...The value to be set for the attribute
// 2563  * return value   : result
// 2564  **************************************************************************************************************/
// 2565 static uint8_t RpSetAttr_phyGpio3Setting( uint8_t attrValue )
// 2566 {
// 2567 	uint8_t result = RP_SUCCESS;
// 2568 
// 2569 	/* check parameter */
// 2570 	switch ( attrValue )
// 2571 	{
// 2572 		case 0x00:
// 2573 		case 0x01:
// 2574 			break;
// 2575 		default:
// 2576 			result = RP_INVALID_PARAMETER;
// 2577 			break;
// 2578 	}
// 2579 
// 2580 	/* set(update) attribute */
// 2581 	if ( result == RP_SUCCESS )
// 2582 	{
// 2583 		RpCb.pib.phyGpio3Setting = attrValue;
// 2584 		RpSetGpio3Setting();
// 2585 	}
// 2586 
// 2587 	return( result );
// 2588 }
// 2589 
// 2590 /* 3.1.60 */
// 2591 /***************************************************************************************************************
// 2592  * function name  : RpSetAttr_phyRmodeTonMax
// 2593  * description    : Attribute Value Setting Function
// 2594  * parameters     : attrValue...The value to be set for the attribute
// 2595  * return value   : result
// 2596  **************************************************************************************************************/
// 2597 static uint8_t RpSetAttr_phyRmodeTonMax( uint16_t attrValue )
// 2598 {
// 2599 	uint8_t result = RP_SUCCESS;
// 2600 
// 2601 	/* check parameter */
// 2602 	if ( RP_MAXVAL_RMODE_TON_MAX < attrValue )
// 2603 	{
// 2604 		result = RP_INVALID_PARAMETER;
// 2605 	}
// 2606 
// 2607 	/* set(update) attribute */
// 2608 	if ( result == RP_SUCCESS )
// 2609 	{
// 2610 		RpCb.pib.phyRmodeTonMax = attrValue;
// 2611 	}
// 2612 
// 2613 	return( result );
// 2614 }
// 2615 
// 2616 /* 3.1.61 */
// 2617 /***************************************************************************************************************
// 2618  * function name  : RpSetAttr_phyRmodeToffMin
// 2619  * description    : Attribute Value Setting Function
// 2620  * parameters     : attrValue...The value to be set for the attribute
// 2621  * return value   : result
// 2622  **************************************************************************************************************/
// 2623 static uint8_t RpSetAttr_phyRmodeToffMin( uint16_t attrValue )
// 2624 {
// 2625 	uint8_t result = RP_SUCCESS;
// 2626 
// 2627 	/* check parameter */
// 2628 	if ( RP_MAXVAL_RMODE_TOFF_MIN < attrValue )
// 2629 	{
// 2630 		result = RP_INVALID_PARAMETER;
// 2631 	}
// 2632 
// 2633 	/* set(update) attribute */
// 2634 	if ( result == RP_SUCCESS )
// 2635 	{
// 2636 		RpCb.pib.phyRmodeToffMin = attrValue;
// 2637 	}
// 2638 
// 2639 	return( result );
// 2640 }
// 2641 
// 2642 /* 3.1.62 */
// 2643 /***************************************************************************************************************
// 2644  * function name  : RpSetAttr_phyRmodeTcumSmpPeriod
// 2645  * description    : Attribute Value Setting Function
// 2646  * parameters     : attrValue...The value to be set for the attribute
// 2647  * return value   : result
// 2648  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon0
          CFI Function _RpSetAttr_phyRmodeTcumSmpPeriod
        CODE
// 2649 static uint8_t RpSetAttr_phyRmodeTcumSmpPeriod( uint16_t attrValue )
// 2650 {
_RpSetAttr_phyRmodeTcumSmpPeriod:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
// 2651 	uint8_t result = RP_SUCCESS;
        MOV       B, #0x7            ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, B               ;; 1 cycle
// 2652 
// 2653 	/* check parameter */
// 2654 	if (( attrValue < RP_MINVAL_RMODE_TCUM_SMP_PERIOD )
// 2655 		||( RP_MAXVAL_RMODE_TCUM_SMP_PERIOD < attrValue ))
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??RpPlmeGetReq_38  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        CMPW      AX, #0x445         ;; 1 cycle
        BC        ??RpPlmeGetReq_39  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 2656 	{
// 2657 		result = RP_INVALID_PARAMETER;
??RpPlmeGetReq_38:
        MOV       A, #0x5            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpPlmeGetReq_40  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2658 	}
// 2659 
// 2660 	/* set(update) attribute */
// 2661 	if ( result == RP_SUCCESS )
// 2662 	{
// 2663 		if ( attrValue != RpCb.pib.phyRmodeTcumSmpPeriod )
??RpPlmeGetReq_39:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      HL, ES:_RpCb+264   ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BZ        ??RpPlmeGetReq_40  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
// 2664 		{
// 2665 			RpCb.pib.phyRmodeTcumSmpPeriod = attrValue;
        MOVW      ES:_RpCb+264, AX   ;; 2 cycles
// 2666 			if ( RpCb.pib.phyRmodeTcumSmpPeriod <= RpCb.txLmtTimer.secCnt )
        CLRW      BC                 ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      HL, #LWRD(_RpCb+328)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+6
        BC        ??RpPlmeGetReq_41  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 2667 			{
// 2668 				RpCb.txLmtTimer.secCnt = RpCb.pib.phyRmodeTcumSmpPeriod - 1;
        MOVW      AX, ES:_RpCb+264   ;; 2 cycles
        DECW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2669 				RpCb.txLmtTimer.msecCnt = 0;
        MOVW      HL, #LWRD(_RpCb+324)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ; ------------------------------------- Block: 13 cycles
// 2670 			}
// 2671 			if ( RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB )
??RpPlmeGetReq_41:
        CMP       ES:_RpCb+235, #0x2  ;; 2 cycles
        SKNZ                         ;; 1 cycle
          CFI FunCall _RpSetMacTxLimitMode
        ; ------------------------------------- Block: 3 cycles
// 2672 			{
// 2673 				RpSetMacTxLimitMode();
        CALL      F:_RpSetMacTxLimitMode  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2674 			}
// 2675 		}
// 2676 	}
// 2677 
// 2678 	return( result );
??RpPlmeGetReq_40:
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        MOV       A, L               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 74 cycles
// 2679 }
// 2680 
// 2681 /* 3.1.63 */
// 2682 /***************************************************************************************************************
// 2683  * function name  : RpSetAttr_phyRmodeTcumLimit
// 2684  * description    : Attribute Value Setting Function
// 2685  * parameters     : attrValue...The value to be set for the attribute
// 2686  * return value   : result
// 2687  **************************************************************************************************************/
// 2688 static uint8_t RpSetAttr_phyRmodeTcumLimit( uint32_t attrValue )
// 2689 {
// 2690 	uint8_t result = RP_SUCCESS;
// 2691 
// 2692 	/* set(update) attribute */
// 2693 	if ( attrValue != RpCb.pib.phyRmodeTcumLimit )
// 2694 	{
// 2695 		RpCb.pib.phyRmodeTcumLimit = attrValue;
// 2696 		if ( RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE )
// 2697 		{
// 2698 			RpSetMacTxLimitMode();
// 2699 		}
// 2700 	}
// 2701 
// 2702 	return( result );
// 2703 }
// 2704 
// 2705 /* 3.1.65 */
// 2706 /***************************************************************************************************************
// 2707  * function name  : RpSetAttr_phyAckWithCca
// 2708  * description    : Attribute Value Setting Function
// 2709  * parameters     : attrValue...The value to be set for the attribute
// 2710  * return value   : result
// 2711  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon0
          CFI Function _RpSetAttr_phyAckWithCca
          CFI NoCalls
        CODE
// 2712 uint8_t RpSetAttr_phyAckWithCca( uint8_t attrValue )
// 2713 {
_RpSetAttr_phyAckWithCca:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 2714 	uint8_t result = RP_SUCCESS;
        MOV       B, #0x7            ;; 1 cycle
// 2715 
// 2716 	/* check parameter */
// 2717 	switch ( attrValue )
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RpPlmeGetReq_42  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 2718 	{
// 2719 		case 0x00:
// 2720 		case 0x01:
// 2721 			break;
// 2722 		default:
// 2723 			result = RP_INVALID_PARAMETER;
        MOV       B, #0x5            ;; 1 cycle
// 2724 			break;
        BR        S:??RpPlmeGetReq_43  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2725 	}
// 2726 
// 2727 	/* set(update) attribute */
// 2728 	if ( result == RP_SUCCESS )
// 2729 	{
// 2730 		RpCb.pib.phyAckWithCca = attrValue;
??RpPlmeGetReq_42:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       ES:_RpCb+274, A    ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
// 2731 	}
// 2732 
// 2733 	return( result );
??RpPlmeGetReq_43:
        MOV       A, B               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 20 cycles
// 2734 }
// 2735 
// 2736 /* 3.1.66 */
// 2737 /***************************************************************************************************************
// 2738  * function name  : RpSetAttr_phyRssiOutputOffset
// 2739  * description    : Attribute Value Setting Function
// 2740  * parameters     : attrValue...The value to be set for the attribute
// 2741  * return value   : result
// 2742  **************************************************************************************************************/
// 2743 static uint8_t RpSetAttr_phyRssiOutputOffset( uint8_t attrValue )
// 2744 {
// 2745 	uint8_t result = RP_SUCCESS;
// 2746 
// 2747 	/* check parameter */
// 2748 	if ( RP_MAXVAL_RSSI_OUTPUT_OFFSET < attrValue )
// 2749 	{
// 2750 		result = RP_INVALID_PARAMETER;
// 2751 	}
// 2752 
// 2753 	/* set(update) attribute */
// 2754 	if ( result == RP_SUCCESS )
// 2755 	{
// 2756 		RpCb.pib.phyRssiOutputOffset = attrValue;
// 2757 	}
// 2758 
// 2759 	return( result );
// 2760 }
// 2761 
// 2762 /* 3.1.67 */
// 2763 /***************************************************************************************************************
// 2764  * function name  : RpSetAttr_phyFrequencyOffset
// 2765  * description    : Attribute Value Setting Function
// 2766  * parameters     : attrValue...The value to be set for the attribute
// 2767  * return value   : result
// 2768  **************************************************************************************************************/
// 2769 #ifdef R_FREQUENCY_OFFSET_ENABLED

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon1
          CFI Function _RpSetAttr_phyFrequencyOffset
          CFI NoCalls
        CODE
// 2770 uint8_t RpSetAttr_phyFrequencyOffset( int32_t attrValue )
// 2771 {
_RpSetAttr_phyFrequencyOffset:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
// 2772 	uint8_t result = RP_SUCCESS;
// 2773 
// 2774 	/* set(update) attribute */
// 2775 	RpCb.pib.phyFrequencyOffset = attrValue;
        MOVW      HL, #LWRD(_RpCb+284)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
// 2776 
// 2777 	return( result );
        MOV       A, #0x7            ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 14 cycles
        ; ------------------------------------- Total: 14 cycles
// 2778 }
// 2779 #endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
// 2780 
// 2781 /***************************************************************************************************************
// 2782  * function name  : RpGetPibReq
// 2783  * description    : Attribute Value Acquisition Function
// 2784  * parameters     : id...Attribute ID
// 2785  *                : valLen...Size of the area storing the attribute value (byte length)
// 2786  *                : pVal...Pointer to the beginning of the area storing the attribute value
// 2787  * return value   : RP_SUCCESS, RP_UNSUPPORTED_ATTRIBUTE, RP_INVALID_PARAMETER, RP_BUSY_RX, RP_BUSY_TX
// 2788  *				  : RP_BUSY_LOWPOWER
// 2789  **************************************************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon0
          CFI Function _RpGetPibReq
        CODE
// 2790 int16_t RpGetPibReq( uint8_t id, uint8_t valLen, void *pVal )
// 2791 {
_RpGetPibReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
// 2792 	uint16_t status;
// 2793 	int16_t rtn = RpExtChkIdLenGetReq(id, valLen, pVal);
        MOV       A, C               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
          CFI FunCall _RpExtChkIdLenGetReq
        CALL      F:_RpExtChkIdLenGetReq  ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 2794 
// 2795 	#if defined(__RX)
// 2796 	uint32_t bkupPsw;
// 2797 	#elif defined(__CCRL__) || defined(__ICCRL78__)
// 2798 	uint8_t  bkupPsw;
// 2799 	#elif defined(__arm)
// 2800 	uint8_t bkupPsw[RP_INT_BKUP_BUF_SIZE];
// 2801 	#endif
// 2802 
// 2803 	/* Disable interrupt */
// 2804 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2805 	RP_PHY_DI(bkupPsw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 2806 	#else
// 2807 	RP_PHY_DI();
// 2808 	#endif
// 2809 
// 2810 	/* API function execution Log */
// 2811 	RpLog_Event( RP_LOG_API_GETIB, id );
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, #0x90           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2812 
// 2813 	if (rtn != RP_SUCCESS)
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        CMPW      AX, #0x7           ;; 1 cycle
        BZ        ??RpPlmeGetReq_44  ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
// 2814 	{
// 2815 		/* API function execution Log */
// 2816 		RpLog_Event( RP_LOG_API_GETIB | RP_LOG_API_RETERR, (uint8_t)rtn );
        MOV       A, #0x92           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2817 
// 2818 		/* Enable interrupt */
// 2819 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2820 		RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 2821 		#else
// 2822 		RP_PHY_EI();
// 2823 		#endif
// 2824 
// 2825 		return (rtn);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BR        S:??RpPlmeGetReq_45  ;; 3 cycles
        ; ------------------------------------- Block: 12 cycles
// 2826 	}
// 2827 
// 2828 	status = RpCb.status;
??RpPlmeGetReq_44:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb       ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 2829 
// 2830 	if (status & RP_PHY_STAT_LOWPOWER)
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].5, ??RpPlmeGetReq_46  ;; 5 cycles
        ; ------------------------------------- Block: 13 cycles
// 2831 	{
// 2832 		/* API function execution Log */
// 2833 		RpLog_Event( RP_LOG_API_SETIB | RP_LOG_API_RETERR, RP_BUSY_LOWPOWER );
        MOV       X, #0xF3           ;; 1 cycle
        MOV       A, #0x82           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2834 
// 2835 		/* Enable interrupt */
// 2836 		#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2837 		RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 2838 		#else
// 2839 		RP_PHY_EI();
// 2840 		#endif
// 2841 
// 2842 		return (RP_BUSY_LOWPOWER);
        MOVW      AX, #0xF3          ;; 1 cycle
        BR        S:??RpPlmeGetReq_45  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
// 2843 	}
// 2844 	else if ((status & RP_PHY_STAT_TRX_OFF) == 0)
??RpPlmeGetReq_46:
        DECW      HL                 ;; 1 cycle
        BT        [HL].4, ??RpPlmeGetReq_47  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
// 2845 	{
// 2846 		if (status & RP_PHY_STAT_TX)
        BF        [HL].1, ??RpPlmeGetReq_48  ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
// 2847 		{
// 2848 			/* API function execution Log */
// 2849 			RpLog_Event( RP_LOG_API_GETIB | RP_LOG_API_RETERR, RP_BUSY_TX );
        MOV       X, #0x2            ;; 1 cycle
        MOV       A, #0x92           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2850 
// 2851 			/* Enable interrupt */
// 2852 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2853 			RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 2854 			#else
// 2855 			RP_PHY_EI();
// 2856 			#endif
// 2857 
// 2858 			return (RP_BUSY_TX);
        MOVW      AX, #0x2           ;; 1 cycle
        BR        S:??RpPlmeGetReq_45  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
// 2859 		}
// 2860 		else
// 2861 		{
// 2862 			/* API function execution Log */
// 2863 			RpLog_Event( RP_LOG_API_GETIB | RP_LOG_API_RETERR, RP_BUSY_RX );
??RpPlmeGetReq_48:
        ONEB      X                  ;; 1 cycle
        MOV       A, #0x92           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2864 
// 2865 			/* Enable interrupt */
// 2866 			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2867 			RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 2868 			#else
// 2869 			RP_PHY_EI();
// 2870 			#endif
// 2871 
// 2872 			return (RP_BUSY_RX);
        ONEW      AX                 ;; 1 cycle
        BR        S:??RpPlmeGetReq_45  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
// 2873 		}
// 2874 	}
// 2875 	RpPlmeGetReq(id, pVal);
??RpPlmeGetReq_47:
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x05]       ;; 1 cycle
          CFI FunCall _RpPlmeGetReq
        CALL      F:_RpPlmeGetReq    ;; 3 cycles
// 2876 
// 2877 	/* API function execution Log */
// 2878 	RpLog_Event( RP_LOG_API_GETIB | RP_LOG_API_RET, RP_SUCCESS );
        MOV       X, #0x7            ;; 1 cycle
        MOV       A, #0x91           ;; 1 cycle
          CFI FunCall _RpLog_Event
        CALL      F:_RpLog_Event     ;; 3 cycles
// 2879 
// 2880 	/* Enable interrupt */
// 2881 	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
// 2882 	RP_PHY_EI(bkupPsw);
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
// 2883 	#else
// 2884 	RP_PHY_EI();
// 2885 	#endif
// 2886 
// 2887 	return (RP_SUCCESS);
        MOVW      AX, #0x7           ;; 1 cycle
        ; ------------------------------------- Block: 20 cycles
??RpPlmeGetReq_45:
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 135 cycles
// 2888 }
// 2889 
// 2890 /***************************************************************************************************************
// 2891  * function name  : RpPlmeGetReq
// 2892  * description    : Attribute Value Acquisition Function
// 2893  * parameters     : id...Attribute ID
// 2894  *                : pVal...Pointer to the beginning of the area storing the attribute value
// 2895  * return value   : none
// 2896  **************************************************************************************************************/
// 2897 #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
// 2898 static void RpPlmeGetReq( uint8_t id, void *pVal )
// 2899 #else

        SECTION `.textf_unit64kp`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon0
          CFI Function _RpPlmeGetReq
        CODE
// 2900 void RpPlmeGetReq( uint8_t id, void *pVal )
// 2901 #endif // #if !defined(RP_WISUN_FAN_STACK) && !defined(RP_ST_ENV)
// 2902 {
_RpPlmeGetReq:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 2903 	uint8_t *pValU8;
// 2904 
// 2905 	pValU8 = (uint8_t *)pVal;
// 2906 
// 2907 	switch (id)
        MOVW      HL, #LWRD(??RpPlmeGetReq_0)  ;; 1 cycle
        MOV       ES, #BYTE3(??RpPlmeGetReq_0)  ;; 1 cycle
        MOV       CS, #BYTE3(_RpPlmeGetReq)  ;; 1 cycle
        BR        N:?C_SSWITCH_L10   ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 2908 	{
// 2909 		case RP_PHY_CHANNELS_SUPPORTED:
// 2910 			RpMemcpy(pValU8, RpCb.pib.phyChannelsSupported, sizeof(RpCb.pib.phyChannelsSupported));
??RpPlmeGetReq_151:
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      BC, #LWRD(_RpCb+154)  ;; 1 cycle
        BR        R:??RpPlmeGetReq_152  ;; 3 cycles
          CFI CFA SP+8
        ; ------------------------------------- Block: 6 cycles
// 2911 			break;
// 2912 
// 2913 		case RP_PHY_CURRENT_CHANNEL:
// 2914 			*pValU8 = RpCb.pib.phyCurrentChannel;
??RpPlmeGetReq_153:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+152    ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        R:??RpPlmeGetReq_154  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
// 2915 			break;
// 2916 
// 2917 		case RP_PHY_TRANSMIT_POWER:
// 2918 			*pValU8 = RpCb.pib.phyTransmitPower;
??RpPlmeGetReq_155:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+153    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2919 			break;
// 2920 
// 2921 		case RP_PHY_CHANNELS_SUPPORTED_PAGE:
// 2922 			*pValU8 = RpCb.pib.phyChannelsSupportedPage;
??RpPlmeGetReq_157:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+162    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2923 			break;
// 2924 
// 2925 		case RP_PHY_CURRENT_PAGE:
// 2926 			*pValU8 = RP_RF_PIB_DFLT_CURRENT_PAGE;
??RpPlmeGetReq_158:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0x9            ;; 1 cycle
        BR        R:??RpPlmeGetReq_159  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 2927 			break;
// 2928 
// 2929 		case RP_PHY_FSK_FEC_TX_ENA:
// 2930 			*pValU8 = RpCb.pib.phyFskFecTxEna;
??RpPlmeGetReq_160:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+163    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2931 			break;
// 2932 
// 2933 		case RP_PHY_FSK_FEC_RX_ENA:
// 2934 			*pValU8 = RpCb.pib.phyFskFecRxEna;
??RpPlmeGetReq_161:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+164    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2935 			break;
// 2936 
// 2937 		case RP_PHY_FSK_FEC_SCHEME:
// 2938 			*pValU8 = RpCb.pib.phyFskFecScheme;
??RpPlmeGetReq_162:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+165    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2939 			break;
// 2940 
// 2941 		case RP_MAC_ADDRESS_FILTER1_ENA:
// 2942 			*pValU8 = RpCb.pib.macAddressFilter1Ena;
??RpPlmeGetReq_163:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+176    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2943 			break;
// 2944 
// 2945 		case RP_MAC_PANID1:
// 2946 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.macPanId1, pValU8);
??RpPlmeGetReq_164:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+180   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+180   ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BR        R:??RpPlmeGetReq_165  ;; 3 cycles
        ; ------------------------------------- Block: 20 cycles
// 2947 			break;
// 2948 
// 2949 		case RP_MAC_SHORT_ADDRESS1:
// 2950 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.macShortAddr1, pValU8);
??RpPlmeGetReq_166:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+178   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+178   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 2951 			break;
// 2952 
// 2953 		case RP_MAC_EXTENDED_ADDRESS1:
// 2954 			RpMemcpy(pValU8, RpCb.pib.macExtendedAddress1, sizeof(RpCb.pib.macExtendedAddress1));
??RpPlmeGetReq_168:
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      BC, #LWRD(_RpCb+182)  ;; 1 cycle
        BR        S:??RpPlmeGetReq_152  ;; 3 cycles
          CFI CFA SP+8
        ; ------------------------------------- Block: 6 cycles
// 2955 			break;
// 2956 
// 2957 		case RP_MAC_PAN_COORD1:
// 2958 			*pValU8 = RpCb.pib.macPanCoord1;
??RpPlmeGetReq_169:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+190    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2959 			break;
// 2960 
// 2961 		case RP_MAC_FRAME_PEND1:
// 2962 			*pValU8 = RpCb.pib.macPendBit1;
??RpPlmeGetReq_170:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+191    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2963 			break;
// 2964 
// 2965 		case RP_MAC_ADDRESS_FILTER2_ENA:
// 2966 			*pValU8 = RpCb.pib.macAddressFilter2Ena;
??RpPlmeGetReq_171:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+192    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2967 			break;
// 2968 
// 2969 		case RP_MAC_PANID2:
// 2970 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.macPanId2, pValU8);
??RpPlmeGetReq_172:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+196   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+196   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 2971 			break;
// 2972 
// 2973 		case RP_MAC_SHORT_ADDRESS2:
// 2974 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.macShortAddr2, pValU8);
??RpPlmeGetReq_173:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+194   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+194   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 2975 			break;
// 2976 
// 2977 		case RP_MAC_EXTENDED_ADDRESS2:
// 2978 			RpMemcpy(pValU8, RpCb.pib.macExtendedAddress2, sizeof(RpCb.pib.macExtendedAddress2));
??RpPlmeGetReq_174:
        MOVW      AX, #0x8           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      BC, #LWRD(_RpCb+198)  ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??RpPlmeGetReq_152:
        MOV       X, #BYTE3(_RpCb)   ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _RpMemcpy
        CALL      F:_RpMemcpy        ;; 3 cycles
// 2979 			break;
        POP       AX                 ;; 1 cycle
          CFI CFA SP+8
        BR        R:??RpPlmeGetReq_175  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
// 2980 
// 2981 		case RP_MAC_PAN_COORD2:
// 2982 			*pValU8 = RpCb.pib.macPanCoord2;
??RpPlmeGetReq_176:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+206    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2983 			break;
// 2984 
// 2985 		case RP_MAC_FRAME_PEND2:
// 2986 			*pValU8 = RpCb.pib.macPendBit2;
??RpPlmeGetReq_177:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+207    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2987 			break;
// 2988 
// 2989 		case RP_MAC_MAXCSMABACKOFF:
// 2990 			*pValU8 = RpCb.pib.macMaxCsmaBackOff;
??RpPlmeGetReq_178:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+208    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2991 			break;
// 2992 
// 2993 		case RP_MAC_MINBE:
// 2994 			*pValU8 = RpCb.pib.macMinBe;
??RpPlmeGetReq_179:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+209    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2995 			break;
// 2996 
// 2997 		case RP_MAC_MAXBE:
// 2998 			*pValU8 = RpCb.pib.macMaxBe;
??RpPlmeGetReq_180:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+210    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2999 			break;
// 3000 
// 3001 		case RP_MAC_MAX_FRAME_RETRIES:
// 3002 			*pValU8 = RpCb.pib.macMaxFrameRetries;
??RpPlmeGetReq_181:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+211    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3003 			break;
// 3004 
// 3005 		case RP_PHY_CRCERROR_UPMSG:
// 3006 			*pValU8 = RpCb.pib.phyCrcErrorUpMsg;
??RpPlmeGetReq_182:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+172    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3007 			break;
// 3008 
// 3009 		case RP_PHY_CCA_VTH:
// 3010 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyCcaVth, pValU8);
??RpPlmeGetReq_183:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+166   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+166   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3011 			break;
// 3012 
// 3013 		case RP_PHY_LVLFLTR_VTH:
// 3014 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyLvlFltrVth, pValU8);
??RpPlmeGetReq_184:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+168   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+168   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3015 			break;
// 3016 
// 3017 		case RP_PHY_BACKOFF_SEED:
// 3018 			*pValU8 = RpCb.pib.phyBackOffSeed;
??RpPlmeGetReq_185:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+173    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3019 			break;
// 3020 
// 3021 		case RP_PHY_CCA_DURATION:
// 3022 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyCcaDuration, pValU8);
??RpPlmeGetReq_186:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+212   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+212   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3023 			break;
// 3024 
// 3025 		case RP_PHY_FSK_PREAMBLE_LENGTH:
// 3026 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyFskPreambleLength, pValU8);
??RpPlmeGetReq_187:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+214   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+214   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3027 			break;
// 3028 
// 3029 		case RP_PHY_MRFSK_SFD:
// 3030 			*pValU8 = RpCb.pib.phyMrFskSfd;
??RpPlmeGetReq_188:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+216    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3031 			break;
// 3032 
// 3033 		case RP_PHY_FSK_SCRAMBLE_PSDU:
// 3034 			*pValU8 = RpCb.pib.phyFskScramblePsdu;
??RpPlmeGetReq_189:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+217    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3035 			break;
// 3036 
// 3037 		case RP_PHY_FSK_OPE_MODE:
// 3038 			*pValU8 = RpCb.pib.phyFskOpeMode;
??RpPlmeGetReq_190:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+218    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3039 			break;
// 3040 
// 3041 		case RP_PHY_FCS_LENGTH:
// 3042 			*pValU8 = RpCb.pib.phyFcsLength;
??RpPlmeGetReq_191:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+219    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3043 			break;
// 3044 
// 3045 		case RP_PHY_ACK_REPLY_TIME:
// 3046 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAckReplyTime, pValU8);
??RpPlmeGetReq_192:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+220   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+220   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3047 			break;
// 3048 
// 3049 		case RP_PHY_ACK_WAIT_DURATION:
// 3050 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAckWaitDuration, pValU8);
??RpPlmeGetReq_193:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+222   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+222   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3051 			break;
// 3052 
// 3053 		case RP_PHY_PROFILE_SPECIFIC_MODE:
// 3054 			*pValU8 = RpCb.pib.phyProfileSpecificMode;
??RpPlmeGetReq_194:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+224    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3055 			break;
// 3056 
// 3057 		case RP_PHY_ANTENNA_SWITCH_ENA:
// 3058 			*pValU8 = RpCb.pib.phyAntennaSwitchEna;
??RpPlmeGetReq_195:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+225    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3059 			break;
// 3060 
// 3061 		case RP_PHY_ANTENNA_DIVERSITY_RX_ENA:
// 3062 			*pValU8 = RpCb.pib.phyAntennaDiversityRxEna;
??RpPlmeGetReq_196:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+226    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3063 			break;
// 3064 
// 3065 		case RP_PHY_ANTENNA_SELECT_TX:
// 3066 			*pValU8 = RpCb.pib.phyAntennaSelectTx;
??RpPlmeGetReq_197:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+227    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3067 			break;
// 3068 
// 3069 		case RP_PHY_ANTENNA_SELECT_ACKTX:
// 3070 			*pValU8 = RpCb.pib.phyAntennaSelectAckTx;
??RpPlmeGetReq_198:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+228    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3071 			break;
// 3072 
// 3073 		case RP_PHY_ANTENNA_SELECT_ACKRX:
// 3074 			*pValU8 = RpCb.pib.phyAntennaSelectAckRx;
??RpPlmeGetReq_199:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+229    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3075 			break;
// 3076 
// 3077 		case RP_PHY_RX_TIMEOUT_MODE:
// 3078 			*pValU8 = RpCb.pib.phyRxTimeoutMode;
??RpPlmeGetReq_200:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+230    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3079 			break;
// 3080 
// 3081 		case RP_PHY_FREQ_BAND_ID:
// 3082 			*pValU8 = RpCb.pib.phyFreqBandId;
??RpPlmeGetReq_201:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+234    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3083 			break;
// 3084 
// 3085 		case RP_PHY_DATA_RATE:
// 3086 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyDataRate, pValU8);
??RpPlmeGetReq_202:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+232   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+232   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3087 			break;
// 3088 
// 3089 		case RP_PHY_REGULATORY_MODE:
// 3090 			*pValU8 = RpCb.pib.phyRegulatoryMode;
??RpPlmeGetReq_203:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+235    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3091 			break;
// 3092 
// 3093 		case RP_PHY_CSMA_BACKOFF_PERIOD:
// 3094 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyCsmaBackoffPeriod, pValU8);
??RpPlmeGetReq_204:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+240   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+240   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3095 			break;
// 3096 
// 3097 		case RP_PHY_PREAMBLE_4BYTE_RX_MODE:
// 3098 			*pValU8 = RpCb.pib.phyPreamble4ByteRxMode;
??RpPlmeGetReq_205:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+242    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
          CFI FunCall _RpGetAgcStartVth
        ; ------------------------------------- Block: 6 cycles
// 3099 			break;
// 3100 
// 3101 		case RP_PHY_AGC_START_VTH:
// 3102 			RpCb.pib.phyAgcStartVth = RpGetAgcStartVth();
??RpPlmeGetReq_206:
        CALL      F:_RpGetAgcStartVth  ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+244, AX   ;; 2 cycles
// 3103 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAgcStartVth, pValU8);
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+244   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 20 cycles
// 3104 			break;
// 3105 
// 3106 		case RP_PHY_CCA_BANDWIDTH:
// 3107 			*pValU8 = RpCb.pib.phyCcaBandwidth;
??RpPlmeGetReq_207:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+246    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3108 			break;
// 3109 
// 3110 		case RP_PHY_ED_BANDWIDTH:
// 3111 			*pValU8 = RpCb.pib.phyEdBandwidth;
??RpPlmeGetReq_208:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+247    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
          CFI FunCall _RpGetAntennaDiversityStartVth
        ; ------------------------------------- Block: 6 cycles
// 3112 			break;
// 3113 
// 3114 		case RP_PHY_ANTENNA_DIVERSITY_START_VTH:
// 3115 			RpCb.pib.phyAntennaDiversityStartVth = RpGetAntennaDiversityStartVth();
??RpPlmeGetReq_209:
        CALL      F:_RpGetAntennaDiversityStartVth  ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+248, AX   ;; 2 cycles
// 3116 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAntennaDiversityStartVth, pValU8);
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+248   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
          CFI FunCall _RpGetAntennaSwitchingTime
        ; ------------------------------------- Block: 20 cycles
// 3117 			break;
// 3118 
// 3119 		case RP_PHY_ANTENNA_SWITCHING_TIME:
// 3120 			RpCb.pib.phyAntennaSwitchingTime = RpGetAntennaSwitchingTime();
??RpPlmeGetReq_210:
        CALL      F:_RpGetAntennaSwitchingTime  ;; 3 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      ES:_RpCb+250, AX   ;; 2 cycles
// 3121 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAntennaSwitchingTime, pValU8);
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+250   ;; 2 cycles
        BR        R:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 20 cycles
// 3122 			break;
// 3123 
// 3124 		case RP_PHY_SFD_DETECTION_EXTEND:
// 3125 			*pValU8 = RpCb.pib.phySfdDetectionExtend;
??RpPlmeGetReq_211:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+252    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3126 			break;
// 3127 
// 3128 		case RP_PHY_AGC_WAIT_GAIN_OFFSET:
// 3129 			*pValU8 = RpCb.pib.phyAgcWaitGainOffset;
??RpPlmeGetReq_212:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+253    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3130 			break;
// 3131 
// 3132 		case RP_PHY_CCA_VTH_OFFSET:
// 3133 			*pValU8 = RpCb.pib.phyCcaVthOffset;
??RpPlmeGetReq_213:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+254    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3134 			break;
// 3135 
// 3136 		case RP_PHY_ANTENNA_SWITCH_ENA_TIMING:
// 3137 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyAntennaSwitchEnaTiming, pValU8);
??RpPlmeGetReq_214:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+256   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+256   ;; 2 cycles
        BR        S:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3138 			break;
// 3139 
// 3140 		case RP_PHY_GPIO0_SETTING:
// 3141 			*pValU8 = RpCb.pib.phyGpio0Setting;
??RpPlmeGetReq_215:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+258    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3142 			break;
// 3143 
// 3144 		case RP_PHY_GPIO3_SETTING:
// 3145 			*pValU8 = RpCb.pib.phyGpio3Setting;
??RpPlmeGetReq_216:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+259    ;; 2 cycles
        BR        R:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3146 			break;
// 3147 
// 3148 		case RP_PHY_RMODE_TON_MAX:
// 3149 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyRmodeTonMax, pValU8);
??RpPlmeGetReq_217:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+260   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+260   ;; 2 cycles
        BR        S:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3150 			break;
// 3151 
// 3152 		case RP_PHY_RMODE_TOFF_MIN:
// 3153 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyRmodeToffMin, pValU8);
??RpPlmeGetReq_218:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+262   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+262   ;; 2 cycles
        BR        S:??RpPlmeGetReq_167  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
// 3154 			break;
// 3155 
// 3156 		case RP_PHY_RMODE_TCUM_SMP_PERIOD:
// 3157 			RP_VAL_UINT16_TO_ARRAY(RpCb.pib.phyRmodeTcumSmpPeriod, pValU8);
??RpPlmeGetReq_219:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      BC, ES:_RpCb+264   ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:_RpCb+264   ;; 2 cycles
        ; ------------------------------------- Block: 13 cycles
??RpPlmeGetReq_167:
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BR        R:??RpPlmeGetReq_165  ;; 3 cycles
        ; ------------------------------------- Block: 7 cycles
// 3158 			break;
// 3159 
// 3160 		case RP_PHY_RMODE_TCUM_LIMIT:
// 3161 			RP_VAL_UINT32_TO_ARRAY(RpCb.pib.phyRmodeTcumLimit, pValU8);
??RpPlmeGetReq_220:
        MOVW      HL, #LWRD(_RpCb+266)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+266)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        INCW      HL                 ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+266)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+266)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       E, #0x18           ;; 1 cycle
          CFI FunCall ?UL_RSH_L03
        CALL      N:?UL_RSH_L03      ;; 3 cycles
        BR        R:??RpPlmeGetReq_221  ;; 3 cycles
        ; ------------------------------------- Block: 51 cycles
// 3162 			break;
// 3163 
// 3164 		case RP_PHY_RMODE_TCUM:
// 3165 			RP_VAL_UINT32_TO_ARRAY(RpCb.pib.phyRmodeTcum, pValU8);
??RpPlmeGetReq_222:
        MOVW      HL, #LWRD(_RpCb+270)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+270)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        INCW      HL                 ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+270)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+270)  ;; 1 cycle
        BR        S:??RpPlmeGetReq_223  ;; 3 cycles
        ; ------------------------------------- Block: 41 cycles
// 3166 			break;
// 3167 
// 3168 		case RP_PHY_ACK_WITH_CCA:
// 3169 			*pValU8 = RpCb.pib.phyAckWithCca;
??RpPlmeGetReq_224:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+274    ;; 2 cycles
        BR        S:??RpPlmeGetReq_156  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3170 			break;
// 3171 		case RP_PHY_RSSI_OUTPUT_OFFSET:
// 3172 			*pValU8 = RpCb.pib.phyRssiOutputOffset;
??RpPlmeGetReq_225:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOV       B, ES:_RpCb+282    ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??RpPlmeGetReq_156:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        R:??RpPlmeGetReq_154  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 3173 			break;
// 3174 
// 3175 #ifdef R_FREQUENCY_OFFSET_ENABLED
// 3176 		case RP_PHY_FREQUENCY:
// 3177 			RP_VAL_UINT32_TO_ARRAY( RpCb.pib.phyFrequency, pValU8 );
??RpPlmeGetReq_226:
        MOVW      HL, #LWRD(_RpCb+236)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+236)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        INCW      HL                 ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+236)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+236)  ;; 1 cycle
        ; ------------------------------------- Block: 38 cycles
??RpPlmeGetReq_223:
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       E, #0x18           ;; 1 cycle
          CFI FunCall ?UL_RSH_L03
        CALL      N:?UL_RSH_L03      ;; 3 cycles
        BR        S:??RpPlmeGetReq_221  ;; 3 cycles
        ; ------------------------------------- Block: 13 cycles
// 3178 			break;
// 3179 
// 3180 		case RP_PHY_FREQUENCY_OFFSET:
// 3181 			RP_VAL_UINT32_TO_ARRAY( RpCb.pib.phyFrequencyOffset, pValU8 );
??RpPlmeGetReq_227:
        MOVW      HL, #LWRD(_RpCb+284)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+284)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        SARW      AX, 0x8            ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        INCW      HL                 ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+284)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      HL, #LWRD(_RpCb+284)  ;; 1 cycle
        MOV       ES, #BYTE3(_RpCb)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOV       E, #0x18           ;; 1 cycle
          CFI FunCall ?SL_RSH_L03
        CALL      N:?SL_RSH_L03      ;; 3 cycles
        ; ------------------------------------- Block: 48 cycles
??RpPlmeGetReq_221:
        MOV       A, X               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x3           ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
??RpPlmeGetReq_154:
        MOVW      HL, AX             ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpPlmeGetReq_165:
        MOV       A, B               ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RpPlmeGetReq_159:
        MOV       ES:[HL], A         ;; 2 cycles
// 3182 			break;
        ; ------------------------------------- Block: 2 cycles
// 3183 #endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
// 3184 
// 3185 		default:
// 3186 			break;
// 3187 	}
// 3188 }
??RpPlmeGetReq_175:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 833 cycles

        SECTION `.switchf`:FARCONST:REORDER:NOROOT(1)
??RpPlmeSetReq_0:
        DATA32
        DW        0
        DATA16
        DW        68
        DW        LWRD(??RpPlmeGetReq_74)
        DW        LWRD(??RpPlmeSetReq_1)
        DW        LWRD(??RpPlmeGetReq_74)
        DW        LWRD(??RpPlmeSetReq_4)
        DW        LWRD(??RpPlmeGetReq_74)
        DW        LWRD(??RpPlmeSetReq_6)
        DW        LWRD(??RpPlmeSetReq_5)
        DW        LWRD(??RpPlmeSetReq_7)
        DW        LWRD(??RpPlmeSetReq_3)
        DW        LWRD(??RpPlmeSetReq_8)
        DW        LWRD(??RpPlmeSetReq_9)
        DW        LWRD(??RpPlmeSetReq_10)
        DW        LWRD(??RpPlmeSetReq_11)
        DW        LWRD(??RpPlmeSetReq_13)
        DW        LWRD(??RpPlmeSetReq_15)
        DW        LWRD(??RpPlmeSetReq_14)
        DW        LWRD(??RpPlmeSetReq_16)
        DW        LWRD(??RpPlmeSetReq_17)
        DW        LWRD(??RpPlmeSetReq_18)
        DW        LWRD(??RpPlmeSetReq_20)
        DW        LWRD(??RpPlmeSetReq_22)
        DW        LWRD(??RpPlmeSetReq_21)
        DW        LWRD(??RpPlmeSetReq_23)
        DW        LWRD(??RpPlmeSetReq_24)
        DW        LWRD(??RpPlmeSetReq_25)
        DW        LWRD(??RpPlmeSetReq_26)
        DW        LWRD(??RpPlmeSetReq_27)
        DW        LWRD(??RpPlmeSetReq_28)
        DW        LWRD(??RpPlmeSetReq_30)
        DW        LWRD(??RpPlmeSetReq_32)
        DW        LWRD(??RpPlmeSetReq_31)
        DW        LWRD(??RpPlmeSetReq_33)
        DW        LWRD(??RpPlmeSetReq_34)
        DW        LWRD(??RpPlmeSetReq_35)
        DW        LWRD(??RpPlmeSetReq_36)
        DW        LWRD(??RpPlmeSetReq_37)
        DW        LWRD(??RpPlmeSetReq_38)
        DW        LWRD(??RpPlmeSetReq_39)
        DW        LWRD(??RpPlmeSetReq_40)
        DW        LWRD(??RpPlmeSetReq_41)
        DW        LWRD(??RpPlmeSetReq_42)
        DW        LWRD(??RpPlmeSetReq_43)
        DW        LWRD(??RpPlmeSetReq_44)
        DW        LWRD(??RpPlmeSetReq_45)
        DW        LWRD(??RpPlmeGetReq_74)
        DW        LWRD(??RpPlmeSetReq_46)
        DW        LWRD(??RpPlmeSetReq_2)
        DW        LWRD(??RpPlmeSetReq_29)
        DW        LWRD(??RpPlmeSetReq_47)
        DW        LWRD(??RpPlmeSetReq_48)
        DW        LWRD(??RpPlmeSetReq_49)
        DW        LWRD(??RpPlmeSetReq_50)
        DW        LWRD(??RpPlmeSetReq_51)
        DW        LWRD(??RpPlmeSetReq_52)
        DW        LWRD(??RpPlmeSetReq_53)
        DW        LWRD(??RpPlmeSetReq_54)
        DW        LWRD(??RpPlmeSetReq_55)
        DW        LWRD(??RpPlmeSetReq_56)
        DW        LWRD(??RpPlmeSetReq_57)
        DW        LWRD(??RpPlmeSetReq_58)
        DW        LWRD(??RpPlmeSetReq_59)
        DW        LWRD(??RpPlmeSetReq_60)
        DW        LWRD(??RpPlmeSetReq_61)
        DW        LWRD(??RpPlmeSetReq_62)
        DW        LWRD(??RpPlmeGetReq_74)
        DW        LWRD(??RpPlmeSetReq_63)
        DW        LWRD(??RpPlmeSetReq_64)
        DW        LWRD(??RpPlmeGetReq_74)
        DW        LWRD(??RpPlmeSetReq_65)

        SECTION `.switchf`:FARCONST:REORDER:NOROOT(1)
??RpPlmeGetReq_0:
        DATA32
        DW        0
        DATA16
        DW        68
        DW        LWRD(??RpPlmeGetReq_175)
        DW        LWRD(??RpPlmeGetReq_153)
        DW        LWRD(??RpPlmeGetReq_151)
        DW        LWRD(??RpPlmeGetReq_155)
        DW        LWRD(??RpPlmeGetReq_158)
        DW        LWRD(??RpPlmeGetReq_160)
        DW        LWRD(??RpPlmeGetReq_161)
        DW        LWRD(??RpPlmeGetReq_162)
        DW        LWRD(??RpPlmeGetReq_183)
        DW        LWRD(??RpPlmeGetReq_184)
        DW        LWRD(??RpPlmeGetReq_185)
        DW        LWRD(??RpPlmeGetReq_182)
        DW        LWRD(??RpPlmeGetReq_163)
        DW        LWRD(??RpPlmeGetReq_166)
        DW        LWRD(??RpPlmeGetReq_164)
        DW        LWRD(??RpPlmeGetReq_168)
        DW        LWRD(??RpPlmeGetReq_169)
        DW        LWRD(??RpPlmeGetReq_170)
        DW        LWRD(??RpPlmeGetReq_171)
        DW        LWRD(??RpPlmeGetReq_173)
        DW        LWRD(??RpPlmeGetReq_172)
        DW        LWRD(??RpPlmeGetReq_174)
        DW        LWRD(??RpPlmeGetReq_176)
        DW        LWRD(??RpPlmeGetReq_177)
        DW        LWRD(??RpPlmeGetReq_178)
        DW        LWRD(??RpPlmeGetReq_179)
        DW        LWRD(??RpPlmeGetReq_180)
        DW        LWRD(??RpPlmeGetReq_181)
        DW        LWRD(??RpPlmeGetReq_186)
        DW        LWRD(??RpPlmeGetReq_187)
        DW        LWRD(??RpPlmeGetReq_188)
        DW        LWRD(??RpPlmeGetReq_189)
        DW        LWRD(??RpPlmeGetReq_190)
        DW        LWRD(??RpPlmeGetReq_191)
        DW        LWRD(??RpPlmeGetReq_192)
        DW        LWRD(??RpPlmeGetReq_193)
        DW        LWRD(??RpPlmeGetReq_194)
        DW        LWRD(??RpPlmeGetReq_195)
        DW        LWRD(??RpPlmeGetReq_196)
        DW        LWRD(??RpPlmeGetReq_197)
        DW        LWRD(??RpPlmeGetReq_198)
        DW        LWRD(??RpPlmeGetReq_199)
        DW        LWRD(??RpPlmeGetReq_200)
        DW        LWRD(??RpPlmeGetReq_201)
        DW        LWRD(??RpPlmeGetReq_202)
        DW        LWRD(??RpPlmeGetReq_203)
        DW        LWRD(??RpPlmeGetReq_157)
        DW        LWRD(??RpPlmeGetReq_204)
        DW        LWRD(??RpPlmeGetReq_205)
        DW        LWRD(??RpPlmeGetReq_206)
        DW        LWRD(??RpPlmeGetReq_207)
        DW        LWRD(??RpPlmeGetReq_208)
        DW        LWRD(??RpPlmeGetReq_209)
        DW        LWRD(??RpPlmeGetReq_210)
        DW        LWRD(??RpPlmeGetReq_211)
        DW        LWRD(??RpPlmeGetReq_212)
        DW        LWRD(??RpPlmeGetReq_213)
        DW        LWRD(??RpPlmeGetReq_214)
        DW        LWRD(??RpPlmeGetReq_215)
        DW        LWRD(??RpPlmeGetReq_216)
        DW        LWRD(??RpPlmeGetReq_217)
        DW        LWRD(??RpPlmeGetReq_218)
        DW        LWRD(??RpPlmeGetReq_219)
        DW        LWRD(??RpPlmeGetReq_220)
        DW        LWRD(??RpPlmeGetReq_222)
        DW        LWRD(??RpPlmeGetReq_224)
        DW        LWRD(??RpPlmeGetReq_225)
        DW        LWRD(??RpPlmeGetReq_226)
        DW        LWRD(??RpPlmeGetReq_227)

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
// 3189 
// 3190 /***********************************************************************************************************************
// 3191  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
// 3192  **********************************************************************************************************************/
// 
//    80 bytes in section .constf
//   284 bytes in section .switchf
//   964 bytes in section .textf
// 3 165 bytes in section .textf_unit64kp
// 
// 4 209 bytes of FARCODE  memory
//   284 bytes of FARCONST memory
//
//Errors: none
//Warnings: none
