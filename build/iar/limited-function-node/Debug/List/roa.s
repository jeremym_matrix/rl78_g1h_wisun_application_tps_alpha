///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:59
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\src\roa.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3DC7.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\src\roa.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\roa.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN ?SI_CMP_L02
        EXTERN _R_OS_GetTaskId
        EXTERN _R_OS_MsgAlloc
        EXTERN _R_OS_MsgFree
        EXTERN _R_OS_PostSemaphore
        EXTERN _R_OS_SetFlag
        EXTERN _R_OS_SetFlagFromISR
        EXTERN _R_OS_WaitForFlag
        EXTERN _R_OS_WaitSemaphore
        EXTERN ___get_psw
        EXTERN ___set_psw
        EXTERN _r_log_finish
        EXTERN _r_log_start
        EXTERN _vTaskSuspendAll
        EXTERN _xTaskResumeAll

        PUBLIC _RoaAllocBlf
        PUBLIC _RoaGetMsg
        PUBLIC _RoaGetQ
        PUBLIC _RoaGetSem
        PUBLIC _RoaGetTskId
        PUBLIC _RoaISetFlg
        PUBLIC _RoaInit
        PUBLIC _RoaInitMsgQ
        PUBLIC _RoaPutBackQ
        PUBLIC _RoaPutQ
        PUBLIC _RoaRelBlf
        PUBLIC _RoaReleaseSem
        PUBLIC _RoaSetFlg
        PUBLIC _RoaSndMsg
        PUBLIC _RoaSndMsgFromISR
        PUBLIC _RoaWaiFlg
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
        
          CFI Common cfiCommon3 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon3
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\src\roa.c
//    1 /*******************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only
//    4  * intended for use with Renesas products. No other uses are authorized.
//    5  * This software is owned by Renesas Electronics Corporation and is protected under
//    6  * all applicable laws, including copyright laws.
//    7  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
//    8  * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
//    9  * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//   10  * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
//   11  * DISCLAIMED.
//   12  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   13  * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   14  * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
//   15  * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
//   16  * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   17  * Renesas reserves the right, without notice, to make changes to this
//   18  * software and to discontinue the availability of this software.
//   19  * By using this software, you agree to the additional terms and
//   20  * conditions found by accessing the following link:
//   21  * http://www.renesas.com/disclaimer
//   22  *******************************************************************************/
//   23 /*******************************************************************************
//   24  * file name    : roa.c
//   25  * description  : The OS Abstraction module for MAC sub layer.
//   26  *******************************************************************************
//   27  * Copyright (C) 2014-2016 Renesas Electronics Corporation.
//   28  ******************************************************************************/
//   29 
//   30 /* --- --- */
//   31 #ifdef RM_NON_STATIC
//   32 #define RM_STATIC
//   33 #else
//   34 #define RM_STATIC static
//   35 #endif
//   36 
//   37 /* --- --- */
//   38 #include "r_stdint.h"
//   39 #include "mac_api.h"
//   40 #include "roa.h"
//   41 #include "roa_config.h"
//   42 #include "roa_intr.h"
//   43 
//   44 /* --- --- */
//   45 // R_LOG_PREFIX is processed by RLog code generator
//   46 #define R_LOG_PREFIX RTOS
//   47 #include "r_log_internal.h"
//   48 #if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
//   49 #include "r_loggen_roa.h"
//   50 #endif
//   51 
//   52 /* --- --- */
//   53 typedef struct
//   54 {
//   55     RoaQEntry* freeQ;
//   56     RoaMsgQ*   pEventMsgQ;
//   57 } RoaQEntryFreeT;
//   58 

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   59 RM_STATIC RoaQEntryFreeT RoaQEntryFree;
_RoaQEntryFree:
        DS 8
//   60 
//   61 /***********************************************************************
//   62  * program start
//   63  **********************************************************************/
//   64 
//   65 /***********************************************************************
//   66  *  function name  : roaInitQueue
//   67  *  -------------------------------------------------------------------
//   68  *  parameters     : RoaResourcesT *pResources
//   69  *  -------------------------------------------------------------------
//   70  *  return value   : none
//   71  *  -------------------------------------------------------------------
//   72  *  description    :
//   73  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _roaInitQueue
        CODE
//   74 RM_STATIC void roaInitQueue(RoaResourcesT* pRoaResources)
//   75 {
_roaInitQueue:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 14
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+18
//   76     int16_t i;
//   77 
//   78     RoaQEntryFree.freeQ = pRoaResources->pQBlk;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RoaQEntryFree)  ;; 1 cycle
        MOV       ES, #BYTE3(_RoaQEntryFree)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//   79 
//   80     for (i = 0; i < (pRoaResources->numQBlk - 1); i++)
        CLRW      AX                 ;; 1 cycle
        BR        S:??RoaSndMsgFromISR_0  ;; 3 cycles
        ; ------------------------------------- Block: 31 cycles
//   81     {
//   82         pRoaResources->pQBlk[i].next = (void*)&pRoaResources->pQBlk[i + 1];
??roaInitQueue_0:
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//   83         pRoaResources->pQBlk[i].data = NULL;
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//   84     }
        MOVW      AX, [SP]           ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 27 cycles
??RoaSndMsgFromISR_0:
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        SHLW      BC, 0x3            ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, BC             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        DECW      BC                 ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
          CFI FunCall ?SI_CMP_L02
        CALL      N:?SI_CMP_L02      ;; 3 cycles
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BC        ??roaInitQueue_0   ;; 4 cycles
        ; ------------------------------------- Block: 35 cycles
//   85     pRoaResources->pQBlk[i].next = NULL;
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//   86     pRoaResources->pQBlk[i].data = NULL;
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//   87 }
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 23 cycles
        ; ------------------------------------- Total: 116 cycles
//   88 
//   89 /***********************************************************************
//   90  *  function name  : RoaInitMsgQ
//   91  *  -------------------------------------------------------------------
//   92  *  parameters     : RoaMsgQ *queue
//   93  *  -------------------------------------------------------------------
//   94  *  return value   : none
//   95  *  -------------------------------------------------------------------
//   96  *  description    :
//   97  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _RoaInitMsgQ
          CFI NoCalls
        CODE
//   98 void RoaInitMsgQ(RoaMsgQ* queue)
//   99 {
_RoaInitMsgQ:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  100     if (queue == NULL)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_1  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_1:
        BZ        ??RoaSndMsgFromISR_2  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  101     {
//  102         return;
//  103     }
//  104     queue->first = NULL;
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  105     queue->last = NULL;
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  106 }
        ; ------------------------------------- Block: 15 cycles
??RoaSndMsgFromISR_2:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 41 cycles
//  107 
//  108 /***********************************************************************
//  109  *  function name  : roaq_get_free_blk
//  110  *  -------------------------------------------------------------------
//  111  *  parameters     : none
//  112  *  -------------------------------------------------------------------
//  113  *  return value   : RoaQEntry *free_blk
//  114  *  -------------------------------------------------------------------
//  115  *  description    :
//  116  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _roaq_get_free_blk
        CODE
//  117 RM_STATIC RoaQEntry* roaq_get_free_blk(void)
//  118 {
_roaq_get_free_blk:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
//  119     RoaQEntry* free_blk;
//  120 
//  121     roa_psw_t bkup_psw;
//  122 
//  123     ROA_ALL_DI(bkup_psw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
//  124 
//  125     free_blk = (RoaQEntry*)RoaQEntryFree.freeQ;
        MOV       ES, #BYTE3(_RoaQEntryFree)  ;; 1 cycle
        MOVW      DE, ES:_RoaQEntryFree  ;; 2 cycles
        MOV       A, ES:_RoaQEntryFree+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  126     if (free_blk != NULL)
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_3  ;; 4 cycles
        ; ------------------------------------- Block: 34 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_3:
        BZ        ??RoaSndMsgFromISR_4  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  127     {
//  128         RoaQEntryFree.freeQ = free_blk->next;
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RoaQEntryFree)  ;; 1 cycle
        MOV       ES, #BYTE3(_RoaQEntryFree)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        ; ------------------------------------- Block: 18 cycles
//  129     }
//  130 
//  131     ROA_ALL_EI(bkup_psw);
??RoaSndMsgFromISR_4:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  132 
//  133     return free_blk;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 16 cycles
        ; ------------------------------------- Total: 74 cycles
//  134 }
//  135 
//  136 /***********************************************************************
//  137  *  function name  : roaq_rel_free_blk
//  138  *  -------------------------------------------------------------------
//  139  *  parameters     : RoaQEntry *free_blk
//  140  *  -------------------------------------------------------------------
//  141  *  return value   : none
//  142  *  -------------------------------------------------------------------
//  143  *  description    :
//  144  **********************************************************************/
//  145 RM_STATIC void roaq_rel_free_blk(RoaQEntry* free_blk)
//  146 {
//  147     roa_psw_t bkup_psw;
//  148 
//  149     if (free_blk == NULL)
//  150     {
//  151         return;
//  152     }
//  153 
//  154     ROA_ALL_DI(bkup_psw);
//  155 
//  156     /* Add at the beginning of the free queue */
//  157     free_blk->data = NULL;
//  158     free_blk->next = RoaQEntryFree.freeQ;
//  159 
//  160     RoaQEntryFree.freeQ = free_blk;
//  161 
//  162     ROA_ALL_EI(bkup_psw);
//  163 
//  164 }
//  165 
//  166 /***********************************************************************
//  167  *  function name  : RoaPutQ
//  168  *  -------------------------------------------------------------------
//  169  *  parameters     : void *pMsg
//  170  *                 : RoaMsgQ *queue
//  171  *  -------------------------------------------------------------------
//  172  *  return value   : none
//  173  *  -------------------------------------------------------------------
//  174  *  description    :
//  175  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon0
          CFI Function _RoaPutQ
        CODE
//  176 void RoaPutQ(void* pMsg, RoaMsgQ* queue)
//  177 {
_RoaPutQ:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 22
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+26
//  178     RoaQEntry* pEntry;
//  179     roa_psw_t bkup_psw;
//  180 
//  181     if (queue == NULL || pMsg == NULL)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_5  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_5:
        SKNZ                         ;; 4 cycles
        BR        R:??RoaSndMsgFromISR_6  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_7  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_7:
        SKNZ                         ;; 4 cycles
        BR        R:??RoaSndMsgFromISR_6  ;; 4 cycles
          CFI FunCall _roaq_get_free_blk
        ; ------------------------------------- Block: 4 cycles
//  182     {
//  183         return;
//  184     }
//  185 
//  186     pEntry = roaq_get_free_blk();
        CALL      F:_roaq_get_free_blk  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  187     if (pEntry == NULL)
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_8  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_8:
        BNZ       ??RoaSndMsgFromISR_9  ;; 4 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 4 cycles
//  188     {
//  189 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
//  190         r_loggen_189();
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x10B         ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 12 cycles
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
??RoaPutQ_0:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  191 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
//  192         return;
        BR        R:??RoaSndMsgFromISR_6  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  193     }
//  194 
//  195     /* Add the message at the end of the queue */
//  196     pEntry->data = (void*)pMsg;
??RoaSndMsgFromISR_9:
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  197     pEntry->next = NULL;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  198 
//  199     ROA_ALL_DI(bkup_psw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
//  200 
//  201     if (queue->last != NULL)
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_10  ;; 4 cycles
        ; ------------------------------------- Block: 66 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_10:
        BZ        ??RoaSndMsgFromISR_11  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  202     {
//  203         ((RoaQEntry*)(queue->last))->next = (void*)pEntry;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 20 cycles
//  204     }
//  205 
//  206     queue->last = (void*)pEntry;
??RoaSndMsgFromISR_11:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  207 
//  208     if (queue->first == NULL)
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_12  ;; 4 cycles
        ; ------------------------------------- Block: 35 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_12:
        BNZ       ??RoaSndMsgFromISR_13  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  209     {
//  210         queue->first = queue->last;
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 13 cycles
//  211     }
//  212 
//  213     ROA_ALL_EI(bkup_psw);
??RoaSndMsgFromISR_13:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  214 
//  215 }
        ; ------------------------------------- Block: 4 cycles
??RoaSndMsgFromISR_6:
        ADDW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 244 cycles
//  216 
//  217 /***********************************************************************
//  218  *  function name  : RoaPutBackQ
//  219  *  -------------------------------------------------------------------
//  220  *  parameters     : void *pMsg
//  221  *                 : RoaMsgQ *queue
//  222  *  -------------------------------------------------------------------
//  223  *  return value   : none
//  224  *  -------------------------------------------------------------------
//  225  *  description    :
//  226  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon0
          CFI Function _RoaPutBackQ
        CODE
//  227 void RoaPutBackQ(void* pMsg, RoaMsgQ* queue)
//  228 {
_RoaPutBackQ:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 18
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+22
//  229     RoaQEntry* pEntry;
//  230     roa_psw_t bkup_psw;
//  231 
//  232     if (queue == NULL || pMsg == NULL)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_14  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_14:
        SKNZ                         ;; 4 cycles
        BR        R:??RoaSndMsgFromISR_15  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_16  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_16:
        SKNZ                         ;; 4 cycles
        BR        R:??RoaSndMsgFromISR_15  ;; 4 cycles
          CFI FunCall _roaq_get_free_blk
        ; ------------------------------------- Block: 4 cycles
//  233     {
//  234         return;
//  235     }
//  236 
//  237     pEntry = roaq_get_free_blk();
        CALL      F:_roaq_get_free_blk  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  238     if (pEntry == NULL)
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_17  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_17:
        BNZ       ??RoaSndMsgFromISR_18  ;; 4 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 4 cycles
//  239     {
//  240 #if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
//  241         r_loggen_238();
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        ONEB      C                  ;; 1 cycle
        MOVW      AX, #0x10C         ;; 1 cycle
          CFI FunCall _r_log_start
        CALL      F:_r_log_start     ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _r_log_finish
        ; ------------------------------------- Block: 12 cycles
        CALL      F:_r_log_finish    ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 3 cycles
??RoaPutBackQ_0:
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  242 #endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
//  243         return;
        BR        R:??RoaSndMsgFromISR_15  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  244     }
//  245 
//  246     pEntry->data = (void*)pMsg;
??RoaSndMsgFromISR_18:
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  247 
//  248     ROA_ALL_DI(bkup_psw);
          CFI FunCall ___get_psw
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
//  249 
//  250     /* Add the message at the end of the queue */
//  251     pEntry->next = queue->first;
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  252     queue->first = (void*)pEntry;
        MOVW      AX, HL             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  253 
//  254     if (queue->last == NULL)
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_19  ;; 4 cycles
        ; ------------------------------------- Block: 95 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_19:
        BNZ       ??RoaSndMsgFromISR_20  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  255     {
//  256         queue->last = queue->first;
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 13 cycles
//  257     }
//  258 
//  259     ROA_ALL_EI(bkup_psw);
??RoaSndMsgFromISR_20:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  260 
//  261 }
        ; ------------------------------------- Block: 4 cycles
??RoaSndMsgFromISR_15:
        ADDW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 212 cycles
//  262 
//  263 /***********************************************************************
//  264  *  function name  : RoaGetQ
//  265  *  -------------------------------------------------------------------
//  266  *  parameters     : RoaMsgQ *queue
//  267  *  -------------------------------------------------------------------
//  268  *  return value   : void *pMsg
//  269  *  -------------------------------------------------------------------
//  270  *  description    :
//  271  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon0
          CFI Function _RoaGetQ
        CODE
//  272 void* RoaGetQ(RoaMsgQ* queue)
//  273 {
_RoaGetQ:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 14
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+18
//  274     /* Function to pick up the queue */
//  275     void* pMsg;
//  276     RoaQEntry* pEntry;
//  277     roa_psw_t bkup_psw;
//  278 
//  279     if (queue == NULL || queue->first == NULL)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_21  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_21:
        BZ        ??RoaSndMsgFromISR_22  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_23  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_23:
        BNZ       ??RoaSndMsgFromISR_24  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  280     {
//  281         return NULL;
??RoaSndMsgFromISR_22:
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        BR        R:??RoaSndMsgFromISR_25  ;; 3 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 5 cycles
//  282     }
//  283 
//  284     ROA_ALL_DI(bkup_psw);
??RoaSndMsgFromISR_24:
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
//  285 
//  286     pEntry = (RoaQEntry*)queue->first;
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  287     pMsg = (RoaMsgT*)pEntry->data;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  288 
//  289     /* De-queue the message */
//  290     queue->first = (void*)(pEntry->next);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  291 
//  292     /* When the queue become empty */
//  293     if (queue->first == NULL)
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_26  ;; 4 cycles
        ; ------------------------------------- Block: 70 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_26:
        BNZ       ??RoaSndMsgFromISR_27  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  294     {
//  295         queue->last = NULL;
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 11 cycles
//  296     }
//  297 
//  298     ROA_ALL_EI(bkup_psw);
??RoaSndMsgFromISR_27:
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
//  299 
//  300     roaq_rel_free_blk(pEntry);
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_28  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_28:
        BZ        ??RoaSndMsgFromISR_29  ;; 4 cycles
          CFI FunCall ___get_psw
        ; ------------------------------------- Block: 4 cycles
        CALL      F:___get_psw       ;; 3 cycles
        MOV       [SP], A            ;; 1 cycle
        DI                           ;; 4 cycles
        AND       A, #0xF9           ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        EI                           ;; 4 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOV       ES, #BYTE3(_RoaQEntryFree)  ;; 1 cycle
        MOVW      DE, ES:_RoaQEntryFree  ;; 2 cycles
        MOV       A, ES:_RoaQEntryFree+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RoaQEntryFree)  ;; 1 cycle
        MOV       ES, #BYTE3(_RoaQEntryFree)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOV       A, [SP]            ;; 1 cycle
          CFI FunCall ___set_psw
        CALL      F:___set_psw       ;; 3 cycles
        ; ------------------------------------- Block: 66 cycles
//  301 
//  302     return pMsg;
??RoaSndMsgFromISR_29:
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RoaSndMsgFromISR_25:
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 228 cycles
//  303 }
//  304 
//  305 /***********************************************************************
//  306  *  function name  : RoaInit
//  307  *  -------------------------------------------------------------------
//  308  *  parameters     : RoaResourcesT *pResources
//  309  *  -------------------------------------------------------------------
//  310  *  return value   : none
//  311  *  -------------------------------------------------------------------
//  312  *  description    :
//  313  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon0
          CFI Function _RoaInit
        CODE
//  314 void RoaInit(RoaResourcesT* pResources)
//  315 {
_RoaInit:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 10
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+14
//  316 #if defined(__RX)
//  317     uint8_t i;
//  318 #else
//  319     int8_t i;
//  320 #endif
//  321 
//  322     roaInitQueue(pResources);
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _roaInitQueue
        CALL      F:_roaInitQueue    ;; 3 cycles
//  323 
//  324     for (i = 0; i < pResources->numEvent; i++)
        CLRB      A                  ;; 1 cycle
        BR        S:??RoaSndMsgFromISR_30  ;; 3 cycles
        ; ------------------------------------- Block: 14 cycles
//  325     {
//  326         RoaInitMsgQ(&pResources->pEventMsgQ[i]);
??RoaInit_0:
        SHLW      AX, 0x3            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_31  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_31:
        BZ        ??RoaSndMsgFromISR_32  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 17 cycles
//  327     }
??RoaSndMsgFromISR_32:
        MOV       A, [SP]            ;; 1 cycle
        INC       A                  ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RoaSndMsgFromISR_30:
        MOV       [SP], A            ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        MOV       L, A               ;; 1 cycle
        SAR       A, 0x7             ;; 1 cycle
        MOV       H, A               ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       C, A               ;; 1 cycle
        CLRB      B                  ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall ?SI_CMP_L02
        CALL      N:?SI_CMP_L02      ;; 3 cycles
        BC        ??RoaInit_0        ;; 4 cycles
        ; ------------------------------------- Block: 31 cycles
//  328 
//  329     RoaQEntryFree.pEventMsgQ = pResources->pEventMsgQ;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_RoaQEntryFree+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_RoaQEntryFree)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  330 }
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 19 cycles
        ; ------------------------------------- Total: 107 cycles
//  331 
//  332 /***********************************************************************
//  333  *  function name  : RoaSetFlg
//  334  *  -------------------------------------------------------------------
//  335  *  parameters     : uint8_t eventid
//  336  *                 : uint16_t flgptn
//  337  *  -------------------------------------------------------------------
//  338  *  return value   : none
//  339  *  -------------------------------------------------------------------
//  340  *  description    :
//  341  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon2
          CFI Function _RoaSetFlg
          CFI FunCall _R_OS_SetFlag
        CODE
//  342 void RoaSetFlg(uint8_t eventid, uint16_t flgptn)
//  343 {
_RoaSetFlg:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  344     // This distinction is from the uItron version although also RoaISetFlg exists
//  345     if (RoaIsInInterrupt())
//  346     {
//  347         R_OS_SetFlagFromISR(eventid, flgptn);
//  348     }
//  349     else
//  350     {
//  351         R_OS_SetFlag(eventid, flgptn);
        CALL      F:_R_OS_SetFlag    ;; 3 cycles
//  352     }
//  353 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  354 
//  355 /***********************************************************************
//  356  *  function name  : RoaISetFlg
//  357  *  -------------------------------------------------------------------
//  358  *  parameters     : uint8_t eventid
//  359  *                 : uint16_t flgptn
//  360  *  -------------------------------------------------------------------
//  361  *  return value   : none
//  362  *  -------------------------------------------------------------------
//  363  *  description    :
//  364  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon2
          CFI Function _RoaISetFlg
          CFI FunCall _R_OS_SetFlagFromISR
        CODE
//  365 void RoaISetFlg(uint8_t eventid, uint16_t flgptn)
//  366 {
_RoaISetFlg:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  367     R_OS_SetFlagFromISR(eventid, flgptn);
        CALL      F:_R_OS_SetFlagFromISR  ;; 3 cycles
//  368 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  369 
//  370 /***********************************************************************
//  371  *  function name  : RoaWaiFlg
//  372  *  -------------------------------------------------------------------
//  373  *  parameters     : uint16_t *pFlgptn
//  374  *                 : uint8_t eventid
//  375  *                 : uint16_t flgptn
//  376  *                 : uint16_t tmo
//  377  *  -------------------------------------------------------------------
//  378  *  return value   : ROA_SUCCESS,ROA_ERR_TIMEOUT,ROA_ERR_FATAL
//  379  *  -------------------------------------------------------------------
//  380  *  description    : This function should be called from the task.
//  381  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon3
          CFI Function _RoaWaiFlg
        CODE
//  382 uint8_t RoaWaiFlg(uint16_t* pFlgptn, uint8_t eventid, uint16_t flgptn, uint16_t tmo)
//  383 {
_RoaWaiFlg:
        ; * Stack frame (at entry) *
        ; Param size: 2
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 12
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+16
        MOV       D, A               ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
//  384     // uItron: "wait for any" and cleaning flags is part of configuration
//  385     // PRQA S 2740 2
//  386     while (1)
//  387     {
//  388         r_os_result_t res = R_OS_WaitForFlag(eventid, flgptn, 1, 0, tmo, pFlgptn);
??RoaWaiFlg_0:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        CLRW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+26
        ONEW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
          CFI FunCall _R_OS_WaitForFlag
        CALL      F:_R_OS_WaitForFlag  ;; 3 cycles
        MOV       B, A               ;; 1 cycle
//  389         if (tmo == ROA_TMO_FEVR && res == R_OS_RESULT_TIMEOUT)
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BNZ       ??RoaSndMsgFromISR_33  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x3            ;; 1 cycle
        BZ        ??RoaWaiFlg_0      ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  390         {
//  391             continue;
//  392         }
//  393         switch (res)
??RoaSndMsgFromISR_33:
        MOV       A, B               ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RoaSndMsgFromISR_34  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        SUB       A, #0x3            ;; 1 cycle
        BZ        ??RoaSndMsgFromISR_35  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??RoaSndMsgFromISR_36  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  394         {
//  395             case R_OS_RESULT_SUCCESS:
//  396                 return ROA_SUCCESS;
??RoaSndMsgFromISR_34:
        CLRB      A                  ;; 1 cycle
        BR        S:??RoaSndMsgFromISR_37  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  397 
//  398             case R_OS_RESULT_TIMEOUT:
//  399                 return ROA_ERR_TIMEOUT;
??RoaSndMsgFromISR_35:
        ONEB      A                  ;; 1 cycle
        BR        S:??RoaSndMsgFromISR_37  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  400 
//  401             default:
//  402                 return ROA_ERR_FATAL;
??RoaSndMsgFromISR_36:
        MOV       A, #0x80           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RoaSndMsgFromISR_37:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 77 cycles
//  403         }
//  404     }
//  405 }
//  406 
//  407 /***********************************************************************
//  408  *  function name  : RoaGetMsg
//  409  *  -------------------------------------------------------------------
//  410  *  parameters     : uint8_t eventid
//  411  *  -------------------------------------------------------------------
//  412  *  return value   : void *p_msg
//  413  *  -------------------------------------------------------------------
//  414  *  description    : This function should be called from the task.
//  415  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon1
          CFI Function _RoaGetMsg
        CODE
//  416 void* RoaGetMsg(uint8_t eventid)
//  417 {
_RoaGetMsg:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  418     return RoaGetQ(RoaQEntryFree.pEventMsgQ);
        MOVW      HL, #LWRD(_RoaQEntryFree+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_RoaQEntryFree)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _RoaGetQ
        CALL      F:_RoaGetQ         ;; 3 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 18 cycles
//  419 }
//  420 
//  421 /***********************************************************************
//  422  *  function name  : RoaGetSem
//  423  *  -------------------------------------------------------------------
//  424  *  parameters     : uint8_t semid
//  425  *  -------------------------------------------------------------------
//  426  *  return value   : none
//  427  *  -------------------------------------------------------------------
//  428  *  description    :
//  429  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon1
          CFI Function _RoaGetSem
          CFI FunCall _R_OS_WaitSemaphore
        CODE
//  430 void RoaGetSem(uint8_t semid)
//  431 {
_RoaGetSem:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  432     R_OS_WaitSemaphore(semid);
        CALL      F:_R_OS_WaitSemaphore  ;; 3 cycles
//  433 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  434 
//  435 /***********************************************************************
//  436  *  function name  : RoaReleaseSem
//  437  *  -------------------------------------------------------------------
//  438  *  parameters     : uint8_t semid
//  439  *  -------------------------------------------------------------------
//  440  *  return value   : none
//  441  *  -------------------------------------------------------------------
//  442  *  description    :
//  443  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon1
          CFI Function _RoaReleaseSem
          CFI FunCall _R_OS_PostSemaphore
        CODE
//  444 void RoaReleaseSem(uint8_t semid)
//  445 {
_RoaReleaseSem:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  446     R_OS_PostSemaphore(semid);
        CALL      F:_R_OS_PostSemaphore  ;; 3 cycles
//  447 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  448 
//  449 /******************************************************************************
//  450  *****************************************************************************/
//  451 
//  452 /***********************************************************************
//  453  *  function name  : RoaAllocBlf
//  454  *  -------------------------------------------------------------------
//  455  *  parameters     : uint8_t mplid
//  456  *  -------------------------------------------------------------------
//  457  *  return value   : void *pMsg
//  458  *  -------------------------------------------------------------------
//  459  *  description    : This function should be called from the task.
//  460  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon2
          CFI Function _RoaAllocBlf
          CFI FunCall _R_OS_MsgAlloc
        CODE
//  461 void* RoaAllocBlf(uint8_t mplid, size_t size)
//  462 {
_RoaAllocBlf:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  463     return R_OS_MsgAlloc(mplid, size);
        CALL      F:_R_OS_MsgAlloc   ;; 3 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  464 }
//  465 
//  466 /***********************************************************************
//  467  *  function name  : RoaRelBlf
//  468  *  -------------------------------------------------------------------
//  469  *  parameters     : uint8_t mpfid
//  470  *                 : void *pMsg
//  471  *  -------------------------------------------------------------------
//  472  *  return value   : ROA_SUCCESS, ROA_ERR_FATAL
//  473  *  -------------------------------------------------------------------
//  474  *  description    : This function should be called from the task.
//  475  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon0
          CFI Function _RoaRelBlf
        CODE
//  476 uint8_t RoaRelBlf(void* pMsg)
//  477 {
_RoaRelBlf:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  478     R_OS_MsgFree(pMsg);
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_OS_MsgFree
        CALL      F:_R_OS_MsgFree    ;; 3 cycles
//  479     return R_OS_RESULT_SUCCESS;
        CLRB      A                  ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 17 cycles
        ; ------------------------------------- Total: 17 cycles
//  480 }
//  481 
//  482 /***********************************************************************
//  483  *  function name  : RoaGetTskId
//  484  *  -------------------------------------------------------------------
//  485  *  parameters     : none
//  486  *  -------------------------------------------------------------------
//  487  *  return value   : task Id
//  488  *  -------------------------------------------------------------------
//  489  *  description    : This function should be called from the task.
//  490  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon1
          CFI Function _RoaGetTskId
          CFI FunCall _R_OS_GetTaskId
        CODE
//  491 uint8_t RoaGetTskId(void)
//  492 {
_RoaGetTskId:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  493     return R_OS_GetTaskId();
        CALL      F:_R_OS_GetTaskId  ;; 3 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  494 }
//  495 
//  496 /***********************************************************************
//  497  *  function name  : RoaSndMsg
//  498  *  -------------------------------------------------------------------
//  499  *  parameters     : void *pMsg
//  500  *  -------------------------------------------------------------------
//  501  *  return value   : none
//  502  *  -------------------------------------------------------------------
//  503  *  description    : This function should be called from the task.
//  504  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon0
          CFI Function _RoaSndMsg
        CODE
//  505 void RoaSndMsg(void* pMsg)
//  506 {
_RoaSndMsg:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _R_OS_GetTaskId
        ; Auto size: 4
//  507     uint8_t eventid = ID_mac_flg;
//  508 
//  509     ((RoaMsgHdrT*)(pMsg))->osMsgHeader.task_id = RoaGetTskId();
        CALL      F:_R_OS_GetTaskId  ;; 3 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  510 
//  511     RoaPutQ(pMsg, RoaQEntryFree.pEventMsgQ);
        MOVW      HL, #LWRD(_RoaQEntryFree+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_RoaQEntryFree)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _RoaPutQ
        CALL      F:_RoaPutQ         ;; 3 cycles
//  512     RoaSetFlg(eventid, ROA_FLG_MSG);
        ONEW      BC                 ;; 1 cycle
        CLRB      A                  ;; 1 cycle
          CFI FunCall _R_OS_SetFlag
        CALL      F:_R_OS_SetFlag    ;; 3 cycles
//  513 }
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 53 cycles
        ; ------------------------------------- Total: 53 cycles
//  514 
//  515 /***********************************************************************
//  516  *  function name  : RoaSndMsgFromISR
//  517  *  -------------------------------------------------------------------
//  518  *  parameters     : void *pMsg
//  519  *  -------------------------------------------------------------------
//  520  *  return value   : none
//  521  *  -------------------------------------------------------------------
//  522  *  description    : This function should be called from ISRs.
//  523  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon0
          CFI Function _RoaSndMsgFromISR
        CODE
//  524 void RoaSndMsgFromISR(void* pMsg)
//  525 {
_RoaSndMsgFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _R_OS_GetTaskId
        ; Auto size: 4
//  526     uint8_t eventid = ID_mac_flg;
//  527 
//  528     ((RoaMsgHdrT*)(pMsg))->osMsgHeader.task_id = RoaGetTskId();
        CALL      F:_R_OS_GetTaskId  ;; 3 cycles
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x5           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  529 
//  530     RoaPutQ(pMsg, RoaQEntryFree.pEventMsgQ);
        MOVW      HL, #LWRD(_RoaQEntryFree+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_RoaQEntryFree)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
          CFI FunCall _RoaPutQ
        CALL      F:_RoaPutQ         ;; 3 cycles
//  531     RoaISetFlg(eventid, ROA_FLG_MSG);
        ONEW      BC                 ;; 1 cycle
        CLRB      A                  ;; 1 cycle
          CFI FunCall _R_OS_SetFlagFromISR
        CALL      F:_R_OS_SetFlagFromISR  ;; 3 cycles
//  532 }
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 53 cycles
        ; ------------------------------------- Total: 53 cycles

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//  533 
//  534 /*******************************************************************************
//  535  * Copyright (C) 2014-2016 Renesas Electronics Corporation.
//  536  ******************************************************************************/
// 
//     8 bytes in section .bssf
// 1 579 bytes in section .textf
// 
//     8 bytes of DATA    memory
// 1 579 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
