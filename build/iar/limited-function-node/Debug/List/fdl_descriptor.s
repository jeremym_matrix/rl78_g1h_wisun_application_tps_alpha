///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:55
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\fdl_descriptor.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW2D51.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\fdl_descriptor.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\fdl_descriptor.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1

        PUBLIC _fdl_descriptor_str
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\fdl_descriptor.c
//    1 /*******************************************************************************
//    2 * Library       : Data Flash Access Library T02
//    3 *
//    4 * File Name     : fdl_descriptor.c
//    5 * Device(s)     : RL78
//    6 * Lib. Version  : V1.01 (for IAR V2)
//    7 * Description   : Physical allocation of the FDL descriptor.
//    8 *                 Please do NOT change this file !!!!
//    9 *******************************************************************************
//   10 * DISCLAIMER
//   11 * This software is supplied by Renesas Electronics Corporation and is only
//   12 * intended for use with Renesas products. No other uses are authorized. This
//   13 * software is owned by Renesas Electronics Corporation and is protected under
//   14 * all applicable laws, including copyright laws.
//   15 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
//   16 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
//   17 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
//   18 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//   19 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   20 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   21 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
//   22 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
//   23 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   24 * Renesas reserves the right, without notice, to make changes to this software
//   25 * and to discontinue the availability of this software. By using this software,
//   26 * you agree to the additional terms and conditions found by accessing the
//   27 * following link:
//   28 * http://www.renesas.com/disclaimer
//   29 *
//   30 * Copyright (C) 2015, 2016 Renesas Electronics Corporation. All rights reserved.
//   31 *******************************************************************************/
//   32 
//   33 
//   34 
//   35 
//   36 /*==============================================================================================*/
//   37 /* compiler settings                                                                            */
//   38 /*==============================================================================================*/
//   39 #pragma language = extended
//   40 
//   41 /*==============================================================================================*/
//   42 /* include files list                                                                           */
//   43 /*==============================================================================================*/
//   44 #include "fdl_types.h"
//   45 #include "fdl_descriptor.h"
//   46 
//   47 /* ----------------------------------------------------------------------------------------------------- */
//   48 /* ------                                                                                          ----- */
//   49 /* ------     B E G I N    O F    U N T O U C H A B L E     D E C L A R A T I O N    A R E A       ----- */
//   50 /* ------                                                                                          ----- */
//   51 /* ----------------------------------------------------------------------------------------------------- */
//   52 
//   53 /* constant segment definition */
//   54 #pragma location="FDL_CNST"
//   55 #pragma data_alignment=2
//   56 
//   57 
//   58 /* physical data flash specification: start address and block-size in bytes */
//   59 #define     DATA_FLASH_BLOCK_SIZE         (1*1024)
//   60 
//   61 
//   62 #if (EEL_POOL_BLOCKS>0)
//   63     #define   EEL_POOL_BYTES                (DATA_FLASH_BLOCK_SIZE*EEL_POOL_BLOCKS)
//   64 #else
//   65     #define   EEL_POOL_BYTES                0
//   66 #endif
//   67 
//   68 #if (FDL_POOL_BLOCKS>0)
//   69     #define   FDL_POOL_BYTES                (DATA_FLASH_BLOCK_SIZE*FDL_POOL_BLOCKS)
//   70 #else
//   71     #define   FDL_POOL_BYTES                0
//   72 #endif
//   73 
//   74 
//   75 /* calculate f_MHz = round-up(FDL_SYSTEM_FREQUENCY)   */
//   76 #define FDL_FX_MHZ                      ((FDL_SYSTEM_FREQUENCY+999999)/1000000)
//   77 
//   78 /* calculate 10us delay as a function f(fx)           */
//   79 #define FDL_DELAY                       ((10*FDL_FX_MHZ)/6)
//   80 
//   81 #ifdef FDL_WIDE_VOLTAGE_MODE
//   82     #define FDL_VOLTAGE_MODE  0x01
//   83 #else
//   84     #define FDL_VOLTAGE_MODE  0x00
//   85 #endif
//   86 

        SECTION FDL_CNST:FARCODE:REORDER:NOROOT(1)
//   87 __far const fdl_descriptor_t  fdl_descriptor_str =  {                                   \ 
_fdl_descriptor_str:
        DATA16
        DW 7168, 1024, 53
        DATA8
        DB 7, 1, 32, 0

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//   88                                                       (fdl_u16)EEL_POOL_BYTES,          \ 
//   89                                                       (fdl_u16)FDL_POOL_BYTES,          \ 
//   90 
//   91                                                       (fdl_u16)FDL_DELAY,               \ 
//   92 
//   93                                                       (fdl_u08)EEL_POOL_BLOCKS,         \ 
//   94                                                       (fdl_u08)FDL_POOL_BLOCKS,         \ 
//   95 
//   96                                                       (fdl_u08)FDL_FX_MHZ,              \ 
//   97                                                       (fdl_u08)FDL_VOLTAGE_MODE         \ 
//   98                                                     };
//   99 
//  100 /* ----------------------------------------------------------------------------------------------------- */
//  101 /* ------                                                                                          ----- */
//  102 /* ------     E N D    O F    U N T O U C H A B L E     D E C L A R A T I O N    A R E A           ----- */
//  103 /* ------                                                                                          ----- */
//  104 /* ----------------------------------------------------------------------------------------------------- */
// 
// 10 bytes in section FDL_CNST
// 
// 10 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
