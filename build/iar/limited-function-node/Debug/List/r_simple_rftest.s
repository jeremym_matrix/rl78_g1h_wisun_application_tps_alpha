///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:58
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\r_simple_rftest.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3D19.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\r_simple_rftest.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\r_simple_rftest.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1


        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\r_simple_rftest.c
//    1 /*******************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only
//    4  * intended for use with Renesas products. No other uses are authorized.
//    5  * This software is owned by Renesas Electronics Corporation and is protected under
//    6  * all applicable laws, including copyright laws.
//    7  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
//    8  * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
//    9  * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//   10  * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
//   11  * DISCLAIMED.
//   12  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   13  * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   14  * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
//   15  * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
//   16  * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   17  * Renesas reserves the right, without notice, to make changes to this
//   18  * software and to discontinue the availability of this software.
//   19  * By using this software, you agree to the additional terms and
//   20  * conditions found by accessing the following link:
//   21  * http://www.renesas.com/disclaimer
//   22  *******************************************************************************/
//   23 /*******************************************************************************
//   24  * file name	: r_simple_rftest.c
//   25  * version		: V.1.02
//   26  * description	: Inspection program at the time of shipment of user product.
//   27  *******************************************************************************
//   28  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
//   29  ******************************************************************************/
//   30 #ifdef R_SIMPLE_RFTEST_ENABLED
//   31 /***********************************************************************
//   32  * includes
//   33  **********************************************************************/
//   34 #include "r_simple_rftest.h"
//   35 
//   36 #include <stdio.h>
//   37 #include <string.h>	// for memcpy,memset,memcmp
//   38 //
//   39 #include "phy.h"
//   40 #include "phy_def.h"
//   41 //
//   42 #include "r_app_main.h"
//   43 #include "r_nwk_api.h"
//   44 #include "hardware.h"
//   45 
//   46 /***********************************************************************
//   47  * macro definitions
//   48  **********************************************************************/
//   49 /* log output */ 
//   50 // #define R_RFDRVIF_LOG_REASON
//   51 
//   52 /* for test */
//   53 #define R_TEST_DATA_LENGTH		(10)	/* test data length */
//   54 
//   55 /* command execution processing */
//   56 #define PRINTF	printf
//   57 
//   58 /* status */ 
//   59 #define R_STATUS_SUCCESS					(0x00)
//   60 /* --- rfdriver --- */
//   61 #define	R_STATUS_ILLEGAL_REQUEST			(0x10)
//   62 #define R_STATUS_INVALID_PARAMETER			(0x11)
//   63 #define R_STATUS_BUSY_LOWPOWER				(0x12)
//   64 #define R_STATUS_BUSY_RX					(0x13)
//   65 #define R_STATUS_BUSY_TX					(0x14)
//   66 #define R_STATUS_TRX_OFF					(0x15)
//   67 #define R_STATUS_INVALID_API_OPTION			(0x16)
//   68 #define R_STATUS_UNSUPPORTED_ATTRIBUTE		(0x17)
//   69 #define R_STATUS_NO_ACK						(0x18)
//   70 #define R_STATUS_CHANNEL_BUSY				(0x19)
//   71 #define R_STATUS_TRANSMIT_TIME_OVERFLOW		(0x1A)
//   72 #define R_STATUS_TX_UNDERFLOW				(0x1B)
//   73 #define R_STATUS_ERROR						(0x1C)
//   74 /* --- serial command --- */
//   75 #define R_STATUS_UNSUPPORTED_COMMAND		(0x20)
//   76 #define R_STATUS_INVALID_COMMAND			(0x21)
//   77 #define R_STATUS_INVALID_COMMAND_PARAMETER	(0x22)
//   78 #define	R_STATUS_UART_RXERROR				(0x23)
//   79 
//   80 /* rf state */
//   81 #define R_RFSTATE_INIT						(0x00)
//   82 #define R_RFSTATE_IDLE						(0x01)
//   83 #define R_RFSTATE_RXAUTO					(0x02)
//   84 #define R_RFSTATE_CONTINUOUS				(0x03)
//   85 
//   86 /* prameter Id */
//   87 #define R_ParamId_channel					(0x00)
//   88 #define R_ParamId_transmitPower				(0x01)
//   89 #define R_ParamId_preambleLength			(0x02)
//   90 #define R_ParamId_agcWaitGainOffset			(0x03)
//   91 #define R_ParamId_antennaSwitchEna			(0x04)
//   92 #define R_ParamId_antennaSwitchEnaTiming	(0x05)
//   93 #define R_ParamId_gpio0Setting				(0x06)
//   94 #define R_ParamId_gpio3Setting				(0x07)
//   95 #define R_ParamId_antennaSelectTx			(0x08)
//   96 
//   97 /* prameter range */
//   98 #define R_MIN_preambleLength				(RP_MINVAL_PREAMBLELEN)		// 4
//   99 #define R_MAX_preambleLength				(RP_MAXVAL_PREAMBLELEN)		// 1000(0x03E8)
//  100 #define R_MAX_agcWaitGainOffset				(0x1F)
//  101 #define R_MIN_antennaSwitchEnaTiming		(0x0001)
//  102 #define R_MAX_antennaSwitchEnaTiming		(0x0154)
//  103 
//  104 unsigned char AppHexStrToNum( unsigned char **ppBuf, void *pData, short dataSize, unsigned char isOctetStr );
//  105 
//  106 /***********************************************************************
//  107  * typedef definitions
//  108  **********************************************************************/
//  109 /* command execution processing */
//  110 typedef struct {
//  111 	const uint8_t *pCmd;
//  112 	void (*pFunc)(uint8_t *pBuff);
//  113 } r_simple_cmd_func_t;
//  114 
//  115 typedef struct {
//  116 	uint8_t paramId;
//  117 	uint8_t (*pFunc)(uint8_t *pBuff);
//  118 } r_setr_func_t;
//  119 
//  120 typedef struct {
//  121 	uint8_t paramId;
//  122 	void (*pFunc)(uint8_t handle);
//  123 } r_getr_func_t;
//  124 
//  125 /* rfdriver API wrapping function */
//  126 typedef struct {
//  127 	boolean_t	eventFlag;
//  128 	uint8_t		status;
//  129 } r_rfdrvIf_msg_pddatacfm_t;
//  130 
//  131 typedef struct {
//  132 	uint16_t	rssi;
//  133 	uint16_t	psduLength;
//  134 	uint8_t		psdu[R_TEST_DATA_LENGTH];
//  135 } r_rfdrvIf_msg_pddataind_t;
//  136 #define R_MSG_SIZE_DATAI	(sizeof(r_rfdrvIf_msg_pddataind_t))
//  137 
//  138 typedef union {
//  139 	uint8_t		phyCurrentChannel;
//  140 	uint8_t		phyCurrentPage;
//  141 	uint32_t	phyChannelsSupported[2];
//  142 	uint8_t		phyChannelsSupportedPage;
//  143 	uint16_t 	phyCcaVth;
//  144 	uint8_t		phyTransmitPower;
//  145 	uint8_t		phyFSKFECRxEna;
//  146 	uint8_t		phyFSKFECTxEna;
//  147 	uint8_t		phyFSKFECScheme;
//  148 	uint16_t 	phyLvlFltrVth;
//  149 	uint8_t		phyBackOffSeed;
//  150 	uint8_t		phyCrcErrorUpMsg;
//  151 	uint8_t		macAddressFilter1Ena;
//  152 	uint16_t 	macShortAddr1;
//  153 	uint32_t 	macExtendedAddress1[2];
//  154 	uint16_t 	macPanId1;
//  155 	uint8_t 	macPanCoord1;
//  156 	uint8_t 	macFramePend1;
//  157 	uint8_t 	macAddressFilter2Ena;
//  158 	uint16_t 	macShortAddr2;
//  159 	uint32_t 	macExtendedAddress2[2];
//  160 	uint16_t 	macPanId2;
//  161 	uint8_t 	macPanCoord2;
//  162 	uint8_t 	macFramePend2;
//  163 	uint8_t 	macMaxCsmaBackOff;
//  164 	uint8_t 	macMinBe;
//  165 	uint8_t 	macMaxBe;
//  166 	uint8_t 	macMaxFrameRetries;
//  167 	uint16_t	phyCsmaBackoffPeriod;
//  168 	uint16_t	phyCcaDuration;
//  169 	uint8_t		phyMRFSKSFD;
//  170 	uint16_t	phyFSKPreambleLength;
//  171 	uint8_t		phyFSKScramblePSDU;
//  172 	uint8_t		phyFSKOpeMode;
//  173 	uint8_t		phyFCSLength;
//  174 	uint16_t	phyAckReplyTime;
//  175 	uint16_t	phyAckWaitDuration;
//  176 	uint8_t		phyProfileSpecificMode;
//  177 	uint8_t		phyAntennaSwitchEna;
//  178 	uint8_t		phyAntennaDiversityRxEna;
//  179 	uint8_t		phyAntennaSelectTx;
//  180 	uint8_t		phyAntennaSelectAckTx;
//  181 	uint8_t		phyAntennaSelectAckRx;
//  182 	uint8_t		phyRxTimeoutMode;
//  183 	uint8_t		phyFreqBandId;
//  184 	uint16_t	phyDataRate;
//  185 	uint8_t		phyRegulatoryMode;	// V3.08
//  186 	uint8_t		phyPreamble4ByteRxMode;
//  187 	uint16_t	phyAgcStartVth;
//  188 	uint8_t		phyCcaBandwidth;
//  189 	uint8_t		phyEdBandwidth;
//  190 	uint16_t	phyAntennaDiversityStartVth;
//  191 	uint16_t	phyAntennaSwitchingTime;
//  192 	uint8_t		phySfdDetectionExtend;
//  193 	uint8_t		phyAgcWaitGainOffset;
//  194 	uint8_t		phyCcaVthOffset;
//  195 	uint16_t	phyAntennaSwitchEnaTiming;
//  196 	uint8_t		phyGpio0Setting;
//  197 	uint8_t		phyGpio3Setting;
//  198 	uint16_t	phyRmodeTonMax;				// V3.08
//  199 	uint16_t	phyRmodeToffMin;			// V3.08
//  200 	uint16_t	phyRmodeTcumSmpPeriod;		// V3.08
//  201 	uint32_t	phyRmodeTcumLimit;			// V3.08
//  202 	uint32_t	phyRmodeTcum;				// V3.08
//  203 	uint8_t		phyAckWithCca;				// V3.08
//  204 	uint8_t		phyRssiOutputOffset;		// V3.08
//  205 	uint32_t	phyFrequency;
//  206 	int32_t		phyFrequencyOffset;
//  207 } r_rfdrvIf_attr_t;
//  208 
//  209 #define R_NUM_FREQ_BAND		(10)
//  210 #define R_MAX_FSK_OPEMODE	(7)
//  211 typedef struct {
//  212 	uint8_t maxChannel[R_MAX_FSK_OPEMODE];
//  213 } r_maxChannelTbl_t;
//  214 
//  215 /* rfdrvIf message buff */
//  216 #define R_MSG_MAXSIZE		(R_MSG_SIZE_DATAI)
//  217 #define	R_MSG_BUFF_NUM		(2)
//  218 
//  219 typedef struct {
//  220 	uint8_t		cont[R_MSG_MAXSIZE];
//  221 } r_rfdrvIf_msgcont_t;
//  222 
//  223 typedef struct {
//  224 	uint8_t		readBufNum;
//  225 	uint8_t		writBufNum 	:7;
//  226 	uint8_t		bufFullBit 	:1;
//  227 	r_rfdrvIf_msgcont_t buff[R_MSG_BUFF_NUM];
//  228 } r_rfdrvIf_msgbuff_t;
//  229 
//  230 static const uint8_t g_MaxFskOpeModeTbl[R_NUM_FREQ_BAND] = 
//  231 {
//  232 	0x07,0x03,0x03,0x04,0x04,0x06,0x03,0x03,0x01,0x04
//  233 };
//  234 static const r_maxChannelTbl_t g_MaxChannelTbl[R_NUM_FREQ_BAND] =
//  235 {
//  236 	/* freqBanId = 0x04 */ { 0x21,0x10,0x10,0x44,0x44,0x44,0x44 },
//  237 	/* freqBanId = 0x05 */ { 0xC6,0x62,0x30,0x00,0x00,0x00,0x00 },
//  238 	/* freqBanId = 0x06 */ { 0x26,0x12,0x08,0x00,0x00,0x00,0x00 },
//  239 	/* freqBanId = 0x07 */ { 0x80,0x3F,0x3F,0x80,0x00,0x00,0x00 },
//  240 	/* freqBanId = 0x08 */ { 0x1F,0x0F,0x0F,0x1F,0x00,0x00,0x00 },
//  241 	/* freqBanId = 0x09 */ { 0x25,0x24,0x23,0x23,0x24,0x25,0x00 },
//  242 	/* freqBanId = 0x0E */ { 0x23,0x80,0x29,0x00,0x00,0x00,0x00 },
//  243 	/* freqBanId = 0x0F */ { 0x3A,0x3A,0x3A,0x00,0x00,0x00,0x00 },
//  244 	/* freqBanId = 0x10 */ { 0x80,0x00,0x00,0x00,0x00,0x00,0x00 },
//  245 	/* freqBanId = 0x11 */ { 0x11,0x11,0x11,0x11,0x00,0x00,0x00 }
//  246 };
//  247 
//  248 
//  249 /***********************************************************************
//  250  * global function prototypes
//  251  **********************************************************************/
//  252 void R_Simple_RFTEST_ProcessCommand( uint8_t *pBuff );
//  253 
//  254 /***********************************************************************
//  255  * private function prototypes
//  256  **********************************************************************/
//  257 /* command execution function */
//  258 static void R_SSCMD_ProcessCmd_RSTR( uint8_t *pCmd );
//  259 static void R_SSCMD_ProcessCmd_RXOFFR( uint8_t *pCmd );
//  260 static void R_SSCMD_ProcessCmd_RXONR( uint8_t *pCmd );
//  261 static void R_SSCMD_ProcessCmd_RCVI( void *pMsg );
//  262 static void R_SSCMD_ProcessCmd_TXR( uint8_t *pCmd );
//  263 static void R_SSCMD_ProcessCmd_CTXR( uint8_t *pCmd );
//  264 static void R_SSCMD_ProcessCmd_PTXR( uint8_t *pCmd );
//  265 static void R_SSCMD_ProcessCmd_CTSR( uint8_t *pCmd );
//  266 static void R_SSCMD_ProcessCmd_SETR( uint8_t *pCmd );
//  267 static uint8_t R_SetR_channel( uint8_t *pCmd );
//  268 static uint8_t R_SetR_transmitPower( uint8_t *pCmd );
//  269 static uint8_t R_SetR_preambleLength( uint8_t *pCmd );
//  270 static uint8_t R_SetR_agcWaitGainOffset( uint8_t *pCmd );
//  271 static uint8_t R_SetR_antennaSwitchEna( uint8_t *pCmd );
//  272 static uint8_t R_SetR_antennaSwitchEnaTiming( uint8_t *pCmd );
//  273 static uint8_t R_SetR_gpio0Setting( uint8_t *pCmd );
//  274 static uint8_t R_SetR_gpio3Setting( uint8_t *pCmd );
//  275 static uint8_t R_SetR_antennaSelectTx( uint8_t *pCmd );
//  276 static void R_SSCMD_ProcessCmd_GETR( uint8_t *pCmd );
//  277 static void R_GetR_channel( uint8_t handle );
//  278 static void R_printf_32data( uint8_t *p_data );
//  279 static void R_GetR_transmitPower( uint8_t handle );
//  280 static void R_GetR_preambleLength( uint8_t handle );
//  281 static void R_GetR_agcWaitGainOffset( uint8_t handle );
//  282 static void R_GetR_antennaSwitchEna( uint8_t handle );
//  283 static void R_GetR_antennaSwitchEnaTiming( uint8_t handle );
//  284 static void R_GetR_gpio0Setting( uint8_t handle );
//  285 static void R_GetR_gpio3Setting( uint8_t handle );
//  286 static void R_GetR_antennaSelectTx( uint8_t handle );
//  287 static void R_SSCMD_ProcessCmd_SIBR( uint8_t *pCmd );
//  288 static void R_SSCMD_ProcessCmd_GIBR( uint8_t *pCmd );
//  289 static void R_SSCMD_ProcessCmd_CERRC( uint8_t status );
//  290 
//  291 /* rfdriver API wrapping function */
//  292 static uint8_t RfdrvIf_Start( void );
//  293 static void RfdrvIf_Reset( void );
//  294 static uint8_t RfdrvIf_SetRxOff( void );
//  295 static uint8_t RfdrvIf_SetRxOn( void );
//  296 static uint8_t RfdrvIf_SendFrame( void );
//  297 static uint8_t RfdrvIf_SetAttr( uint8_t attrId, uint8_t attrLength, uint8_t RP_FAR *p_attrValue );
//  298 static uint8_t RfdrvIf_GetAttr( uint8_t attrId, uint8_t attrValueLength, uint8_t *p_attrLength,
//  299 								uint8_t *p_attrValue );
//  300 static uint8_t RfdrvIf_GetAttrLength( uint8_t attrId );
//  301 static uint8_t RfdrvIf_SetAttrChannel( uint8_t freqBandId, uint8_t fskOpeMode, uint8_t setChannel, int32_t frequencyOffset );
//  302 static uint8_t RfdrvIf_CheckChannelsSupported( uint8_t channel, uint8_t maskChSupported );
//  303 static uint8_t RfdrvIf_ConvertChannelsSupportedPage( uint8_t channel );
//  304 static uint8_t RfdrvIf_SetAttrTxPower( uint8_t freqBandId, uint8_t transmitPower );
//  305 static uint8_t RfdrvIf_ContUnmoduTx( void );
//  306 static uint8_t RfdrvIf_Pn9ContModuTx( void );
//  307 static uint8_t RfdrvIf_ContTxStop( void );
//  308 
//  309 /* rfdriver callback function */
//  310 static void RfdrvIf_PdDataIndCallback( uint8_t *pData, uint16_t dataLength, uint32_t time,
//  311 									uint8_t linkQuality, uint16_t rssi, uint8_t selectedAntenna,
//  312 									uint16_t rssi_0, uint16_t rssi_1, uint8_t status,
//  313 									uint8_t filteredAddr, uint8_t phr );
//  314 static void RfdrvIf_PdDataCfmCallback( uint8_t status, uint8_t framePend, uint8_t numBackoffs );
//  315 static void RfdrvIf_PlmeCcaCfmCallback( uint8_t status );
//  316 static void RfdrvIf_PlmeEdCfmCallback( uint8_t phyState, uint8_t edValue, uint16_t rssi );
//  317 static void RfdrvIf_RxOffIndCallback( void );
//  318 static void RfdrvIf_FatalErrorIndCallback( uint8_t status );
//  319 static void RfdrvIf_WarningIndCallback( uint8_t status );
//  320 
//  321 /* rfdrvIf message buff */
//  322 static void RfdrvIf_MessageProcess( void );
//  323 static void RfdrvIf_InitMsgBuff( void );
//  324 static void RfdrvIf_PutMsgBuff( void *pSrcBuff, uint16_t length );
//  325 static uint8_t *RfdrvIf_GetMsgBuff( void );
//  326 static void RfdrvIf_RelMsgBuff( void );
//  327 
//  328 /***********************************************************************
//  329  * command table
//  330  **********************************************************************/
//  331 static const uint8_t CmdStr_RSTR[]	 = "RSTR";		// "RF Reset" Request Command
//  332 static const uint8_t CmdStr_RXOFFR[] = "RXOFFR";	// "RF Idle(RX Off)" Request Command
//  333 static const uint8_t CmdStr_RXONR[]  = "RXONR";		// "RF RX ON" Request Command
//  334 static const uint8_t CmdStr_TXR[]	 = "TXR";		// "Frame Transmission" Request Command
//  335 static const uint8_t CmdStr_CTXR[]	 = "CTXR";		// "Continuous Unmodulated Transmission" Request Command
//  336 static const uint8_t CmdStr_PTXR[]	 = "PTXR";		// "PN9 Continuous Modulated Transmission" Request Command
//  337 static const uint8_t CmdStr_CTSR[]	 = "CTSR";		// "Continuous Transmisson Stop" Request Command
//  338 static const uint8_t CmdStr_SETR[]	 = "SETR";		// "Set Parameter" Request Command
//  339 static const uint8_t CmdStr_GETR[]	 = "GETR";		// "Get Parameter" Request Command
//  340 static const uint8_t CmdStr_SIBR[]	 = "SIBR";		// "Set IB Value" Request Command
//  341 static const uint8_t CmdStr_GIBR[]	 = "GIBR";		// "Get IB value" Request Command
//  342 
//  343 static const r_simple_cmd_func_t CmdFunc_RFTest[] = {
//  344 	{ CmdStr_RSTR, 	 	&R_SSCMD_ProcessCmd_RSTR	},
//  345 	{ CmdStr_RXOFFR, 	&R_SSCMD_ProcessCmd_RXOFFR	},
//  346 	{ CmdStr_RXONR,  	&R_SSCMD_ProcessCmd_RXONR	},
//  347 	{ CmdStr_TXR, 	 	&R_SSCMD_ProcessCmd_TXR		},
//  348 	{ CmdStr_CTXR,	 	&R_SSCMD_ProcessCmd_CTXR	},
//  349 	{ CmdStr_PTXR,	 	&R_SSCMD_ProcessCmd_PTXR	},
//  350 	{ CmdStr_CTSR,	 	&R_SSCMD_ProcessCmd_CTSR	},
//  351 	{ CmdStr_SETR,	 	&R_SSCMD_ProcessCmd_SETR	},
//  352 	{ CmdStr_GETR,	 	&R_SSCMD_ProcessCmd_GETR	},
//  353 	{ CmdStr_SIBR,	 	&R_SSCMD_ProcessCmd_SIBR	},
//  354 	{ CmdStr_GIBR,	 	&R_SSCMD_ProcessCmd_GIBR	},
//  355 };
//  356 
//  357 static const r_setr_func_t gc_SetrFunc[] = {
//  358 	{ R_ParamId_channel,				&R_SetR_channel 				},
//  359 	{ R_ParamId_transmitPower,			&R_SetR_transmitPower			},
//  360 	{ R_ParamId_preambleLength,			&R_SetR_preambleLength			},
//  361 	{ R_ParamId_agcWaitGainOffset,		&R_SetR_agcWaitGainOffset		},
//  362 	{ R_ParamId_antennaSwitchEna,		&R_SetR_antennaSwitchEna		},
//  363 	{ R_ParamId_antennaSwitchEnaTiming,	&R_SetR_antennaSwitchEnaTiming	},
//  364 	{ R_ParamId_gpio0Setting,			&R_SetR_gpio0Setting			},
//  365 	{ R_ParamId_gpio3Setting,			&R_SetR_gpio3Setting			},
//  366 	{ R_ParamId_antennaSelectTx,		&R_SetR_antennaSelectTx			},
//  367 };
//  368 
//  369 static const r_getr_func_t gc_GetrFunc[] = {
//  370 	{ R_ParamId_channel,				&R_GetR_channel 				},
//  371 	{ R_ParamId_transmitPower,			&R_GetR_transmitPower			},
//  372 	{ R_ParamId_preambleLength,			&R_GetR_preambleLength			},
//  373 	{ R_ParamId_agcWaitGainOffset,		&R_GetR_agcWaitGainOffset		},
//  374 	{ R_ParamId_antennaSwitchEna,		&R_GetR_antennaSwitchEna		},
//  375 	{ R_ParamId_antennaSwitchEnaTiming,	&R_GetR_antennaSwitchEnaTiming	},
//  376 	{ R_ParamId_gpio0Setting,			&R_GetR_gpio0Setting			},
//  377 	{ R_ParamId_gpio3Setting,			&R_GetR_gpio3Setting			},
//  378 	{ R_ParamId_antennaSelectTx,		&R_GetR_antennaSelectTx			},
//  379 };
//  380 
//  381 /***********************************************************************
//  382  * private variables
//  383  **********************************************************************/
//  384 static uint8_t gRfState;
//  385 static uint8_t gNtfHandle;
//  386 static r_rfdrvIf_msgbuff_t gMsgBuff;
//  387 static r_rfdrvIf_msg_pddatacfm_t gCbMsgPdDataCfm;
//  388 static r_rfdrvIf_msg_pddataind_t gCbMsgPdDataInd;
//  389 
//  390 /***********************************************************************
//  391  * Simple serial command is valid function
//  392  **********************************************************************/
//  393 /***********************************************************************
//  394  * function name  : R_Simple_RFTest_IsValid
//  395  * description    : Check if simple RF Test program is enabled 
//  396  * parameters     : none
//  397  * return value   : R_TRUE in case of RF Test program execution 
//  398                     R_FALSE in case of Wi-SUN FAN stack execution 
//  399  **********************************************************************/
//  400 r_boolean_t R_Simple_RFTest_IsValid()
//  401 {
//  402 	/* For RX651: Read Switch SW3-1 of MB-RX604S-02 board (ON = RF Test Mode) */
//  403 	/* For RL78G1H: Read Switch SW1-1 of TK-RLG1H+SB2 board (ON = RF TEST Mode) */
//  404 
//  405   if(RdrvSwitch0_GetCondition() == 0)
//  406 	{
//  407 		return R_TRUE;
//  408 	}
//  409 	return R_FALSE;
//  410 }
//  411 
//  412 
//  413 /********************************************************************************
//  414 * Function Name     : AppCommandInput
//  415 * Description       : Read command from UART
//  416 *                   : Process command and message
//  417 * Arguments         : None
//  418 * Return Value      : None
//  419 ********************************************************************************/
//  420 void AppCommandInput(void)
//  421 {
//  422     short ch;
//  423     unsigned char cmdReady = 0;
//  424 
//  425     /*--------------------------------------------------*/
//  426     /* Read input command from UART                     */
//  427     /*--------------------------------------------------*/
//  428     do
//  429     {
//  430         ch = RdrvUART_GetChar();
//  431         switch (ch)
//  432         {
//  433             case 0x0a:
//  434                 if ((AppCmdSize > 0) && (AppCmdSize < sizeof(AppCmdBuf)))
//  435                 {
//  436                     AppCmdBuf[AppCmdSize++] = 0;
//  437                     cmdReady = 1;
//  438                 }
//  439                 else
//  440                 {
//  441                     AppCmdSize = 0;
//  442                 }
//  443                 break;
//  444 
//  445             case 0x0d:
//  446 
//  447                 /* No Action */
//  448                 break;
//  449 
//  450             default:
//  451                 if (ch >= 0)
//  452                 {
//  453                     if (AppCmdSize < sizeof(AppCmdBuf))
//  454                     {
//  455                         AppCmdBuf[AppCmdSize++] = (unsigned char)ch;  /* put character to rcv buffer */
//  456                     }
//  457                 }
//  458                 break;
//  459         }
//  460     }
//  461     while ((ch > 0) && (cmdReady == 0));
//  462 
//  463     /*--------------------------------------------------*/
//  464     /* Process command                                  */
//  465     /*--------------------------------------------------*/
//  466     if (cmdReady == 1)
//  467     {
//  468         R_Simple_RFTest_ProcessCommand(AppCmdBuf);
//  469         AppCmdSize = 0;
//  470     }
//  471 }
//  472 
//  473 /***********************************************************************
//  474  * Simple serial command main function
//  475  **********************************************************************/
//  476 /***********************************************************************
//  477  * function name  : R_SimpleSerial_RFTest
//  478  * description    : Simple serial RF Test command program main processing
//  479  * parameters     : none
//  480  * return value   : none
//  481  **********************************************************************/
//  482 void R_Simple_RFTest_Main( void )
//  483 {
//  484 	RfdrvIf_Start();
//  485 	while (1)
//  486 	{
//  487 		AppCommandInput();
//  488 		RfdrvIf_MessageProcess();
//  489 	}
//  490 }
//  491 
//  492 /***********************************************************************
//  493  * command execution function
//  494  **********************************************************************/
//  495 /***********************************************************************
//  496  * function name  : R_Simple_RFTest_ProcessCommand
//  497  * description    : command reception processing
//  498  * parameters     : pBuff...pointer to the uart data
//  499  * return value   : none
//  500  **********************************************************************/
//  501 void R_Simple_RFTest_ProcessCommand( uint8_t *pBuff )
//  502 {
//  503 	uint8_t i, size;
//  504 	uint8_t status = R_STATUS_UNSUPPORTED_COMMAND;
//  505 
//  506 	size = sizeof(CmdFunc_RFTest)/sizeof(CmdFunc_RFTest[0]);
//  507 	for ( i=0; i<size; i++ )
//  508 	{
//  509 		if ( !AppStricmp( (void __far *)CmdFunc_RFTest[i].pCmd, (void*)&pBuff ))
//  510 		{
//  511 			CmdFunc_RFTest[i].pFunc( pBuff );
//  512 			status = R_STATUS_SUCCESS;
//  513 			break;
//  514 		}
//  515 	}
//  516 
//  517 	if ( status != R_STATUS_SUCCESS )
//  518 	{
//  519 		R_SSCMD_ProcessCmd_CERRC( status );
//  520 	}
//  521 }
//  522 
//  523 /***********************************************************************
//  524  * function name  : R_SSCMD_ProcessCmd_RSTR
//  525  * description	  : "RF Reset Request"(RSTR) Command Processing
//  526  * parameters	  : pCmd...pointer to the uart data
//  527  *				  :   RSTR (handle)
//  528  * return value   : none
//  529  * output		  : Confirm command
//  530  *				  :   RSTC (handle) (status) (rfstate)
//  531  **********************************************************************/
//  532 static void R_SSCMD_ProcessCmd_RSTR( uint8_t *pCmd )
//  533 {
//  534 	uint8_t status = R_STATUS_SUCCESS;
//  535 	uint8_t res;
//  536 	uint8_t handle;
//  537 
//  538 	/* get command parameters */
//  539 	/* (handle) */
//  540 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
//  541 	if ( res != 0 )
//  542 	{
//  543 		status = R_STATUS_INVALID_COMMAND;
//  544 	}
//  545 
//  546 	/* command execution */
//  547 	if ( status == R_STATUS_SUCCESS )
//  548 	{
//  549 		/* rfdriver reset */
//  550 		RfdrvIf_Reset();
//  551 	}
//  552 
//  553 	/* "RTSC" command transmission */
//  554 	PRINTF( "RSTC " );
//  555 	PRINTF( "%02X ", handle );
//  556 	PRINTF( "%02X ", status );
//  557 	PRINTF( "%02X\n", gRfState );
//  558 }
//  559 
//  560 /***********************************************************************
//  561  * function name  : R_SSCMD_ProcessCmd_RXOFFR
//  562  * description	  : "RF Idle(RX Off) Request(RXOFFR)" Command Processing
//  563  * parameters	  : pCmd...pointer to the uart data
//  564  *				  :   RXOFFR (handle)
//  565  * return value   : none
//  566  * output		  : Confirm command
//  567  *				  :   RXOFFC (handle) (status) (rfstate)
//  568  **********************************************************************/
//  569 static void R_SSCMD_ProcessCmd_RXOFFR( uint8_t *pCmd )
//  570 {
//  571 	uint8_t status = R_STATUS_SUCCESS;
//  572 	uint8_t res;
//  573 	uint8_t handle;
//  574 
//  575 	/* get command parameters */
//  576 	/* (handle) */
//  577 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
//  578 	if ( res != 0 )
//  579 	{
//  580 		status = R_STATUS_INVALID_COMMAND;
//  581 	}
//  582 
//  583 	/* command execution */
//  584 	if ( status == R_STATUS_SUCCESS )
//  585 	{
//  586 		/* rfdriver Idle Setting Request */
//  587 		status = RfdrvIf_SetRxOff();
//  588 	}
//  589 
//  590 	/* "RXOFFC" command transmission */
//  591 	PRINTF( "RXOFFC " );
//  592 	PRINTF( "%02X ", handle );
//  593 	PRINTF( "%02X ", status );
//  594 	PRINTF( "%02X\n", gRfState );
//  595 }
//  596 
//  597 /***********************************************************************
//  598  * function name  : R_SSCMD_ProcessCmd_RXONR
//  599  * description	  : "RF RX ON Request(RXONR)" Command Processing
//  600  * parameters	  : pCmd...pointer to the uart data
//  601  *				  :   RXONR (handle)
//  602  * return value   : none
//  603  * output		  : Confirm command
//  604  *				  :   RXONC (handle) (status) (rfState)
//  605  **********************************************************************/
//  606 static void R_SSCMD_ProcessCmd_RXONR( uint8_t *pCmd )
//  607 {
//  608 	uint8_t status = R_STATUS_SUCCESS;
//  609 	uint8_t res;
//  610 	uint8_t handle;
//  611 
//  612 	/* get command parameters */
//  613 	/* (handle) */
//  614 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
//  615 	if ( res != 0 )
//  616 	{
//  617 		status = R_STATUS_INVALID_COMMAND;
//  618 	}
//  619 
//  620 	/* command execution */
//  621 	if ( status == R_STATUS_SUCCESS )
//  622 	{
//  623 		/* rfdriver Reception ON Setting Request */
//  624 		status = RfdrvIf_SetRxOn();
//  625 	}
//  626 
//  627 	/* "RXONC" command transmission */
//  628 	PRINTF( "RXONC " );
//  629 	PRINTF( "%02X ", handle );
//  630 	PRINTF( "%02X ", status );
//  631 	PRINTF( "%02X\n", gRfState );
//  632 }
//  633 
//  634 /***********************************************************************
//  635  * function name  : R_SSCMD_ProcessCmd_RCVI
//  636  * description	  : Process "RCVI" command
//  637  * parameters	  : pMsg...pointer to message
//  638  * return value   : none
//  639  * output		  : Indication command
//  640  *				  :   RCVI (handle) (dataLength) (data)
//  641  **********************************************************************/
//  642 static void R_SSCMD_ProcessCmd_RCVI( void *pMsg )
//  643 {
//  644 	uint16_t i;
//  645 	uint16_t psduLength;	// for CC-RL
//  646 	uint16_t rssi;
//  647 	r_rfdrvIf_msg_pddataind_t *pInd = (r_rfdrvIf_msg_pddataind_t *)pMsg;
//  648 
//  649 	/* "RCVI" command transmission */
//  650 	PRINTF( "RCVI " );
//  651 	PRINTF( "%02X ", gNtfHandle++ );
//  652 	rssi = RP_VAL_ARRAY_TO_UINT16( (uint8_t *)&pInd->rssi );	// for CC-RL
//  653 	PRINTF( "%04X ", rssi );
//  654 	psduLength = RP_VAL_ARRAY_TO_UINT16( (uint8_t *)&pInd->psduLength );	// for CC-RL
//  655 	PRINTF( "%04X ", psduLength );
//  656 
//  657 	for ( i=0; i<psduLength; i++ )
//  658 	{
//  659 		PRINTF( "%02X", pInd->psdu[i] );
//  660 	}
//  661 	PRINTF( "\n" );
//  662 }
//  663 
//  664 /***********************************************************************
//  665  * function name  : R_SSCMD_ProcessCmd_TXR
//  666  * description	  : "Frame Transmission Request(TXR)" Command Processing
//  667  * parameters	  : pCmd...pointer to the uart data
//  668  *				  :   TXR (handle)
//  669  * return value   : none
//  670  * output		  : Confirm command
//  671  *				  :   TXC (handle) (status) (rfstate)
//  672  **********************************************************************/
//  673 static void R_SSCMD_ProcessCmd_TXR( uint8_t *pCmd )
//  674 {
//  675 	uint8_t status = R_STATUS_SUCCESS;
//  676 	uint8_t res;
//  677 	uint8_t handle;
//  678 
//  679 	/* get command parameters */
//  680 	/* (handle) */
//  681 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
//  682 	if ( res != 0 )
//  683 	{
//  684 		status = R_STATUS_INVALID_COMMAND;
//  685 	}
//  686 
//  687 	/* command execution */
//  688 	if ( status == R_STATUS_SUCCESS )
//  689 	{
//  690 		/* frame transmission processing */
//  691 		status = RfdrvIf_SendFrame();
//  692 	}
//  693 
//  694 	/* "TXC" command transmission */
//  695 	PRINTF( "TXC " );
//  696 	PRINTF( "%02X ", handle );
//  697 	PRINTF( "%02X ", status );
//  698 	PRINTF( "%02X\n", gRfState );
//  699 }
//  700 
//  701 /***********************************************************************
//  702  * function name  : R_SSCMD_ProcessCmd_CTXR
//  703  * description	  : "Continuous Unmodulated Transmission Request(CTXR)"
//  704  *                : Command Processing
//  705  * parameters	  : pCmd...pointer to the uart data
//  706  *				  :   CTXR (handle)
//  707  * return value   : none
//  708  * output		  : Confirm command
//  709  *				  :   CTXC (handle) (status) (rfstate)
//  710  **********************************************************************/
//  711 static void R_SSCMD_ProcessCmd_CTXR( uint8_t *pCmd )
//  712 {
//  713 	uint8_t status = R_STATUS_SUCCESS;
//  714 	uint8_t res;
//  715 	uint8_t handle;
//  716 
//  717 	/* get command parameters */
//  718 	/* (handle) */
//  719 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
//  720 	if ( res != 0 )
//  721 	{
//  722 		status = R_STATUS_INVALID_COMMAND;
//  723 	}
//  724 
//  725 	/* command execution */
//  726 	if ( status == R_STATUS_SUCCESS )
//  727 	{
//  728 		status = RfdrvIf_ContUnmoduTx();
//  729 	}
//  730 
//  731 	/* "CTXC" command transmission */
//  732 	PRINTF( "CTXC " );
//  733 	PRINTF( "%02X ", handle );
//  734 	PRINTF( "%02X ", status );
//  735 	PRINTF( "%02X\n", gRfState );
//  736 }
//  737 
//  738 /***********************************************************************
//  739  * function name  : R_SSCMD_ProcessCmd_PTXR
//  740  * description	  : "PN9 Continuous Modulated Transmission Request(PTXR)"
//  741  *				  : Command Processing
//  742  * parameters	  : pCmd...pointer to the uart data
//  743  *				  :   PTXR (handle)
//  744  * return value   : none
//  745  * output		  : Confirm command
//  746  *				  :   PTXC (handle) (status) (rfstate)
//  747  **********************************************************************/
//  748 static void R_SSCMD_ProcessCmd_PTXR( uint8_t *pCmd )
//  749 {
//  750 	uint8_t status = R_STATUS_SUCCESS;
//  751 	uint8_t res;
//  752 	uint8_t handle;
//  753 
//  754 	/* get command parameters */
//  755 	/* (handle) */
//  756 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
//  757 	if ( res != 0 )
//  758 	{
//  759 		status = R_STATUS_INVALID_COMMAND;
//  760 	}
//  761 
//  762 	/* command execution */
//  763 	if ( status == R_STATUS_SUCCESS )
//  764 	{
//  765 		status = RfdrvIf_Pn9ContModuTx();
//  766 	}
//  767 
//  768 	/* "PTXC" command transmission */
//  769 	PRINTF( "PTXC " );
//  770 	PRINTF( "%02X ", handle );
//  771 	PRINTF( "%02X ", status );
//  772 	PRINTF( "%02X\n", gRfState );
//  773 }
//  774 
//  775 /***********************************************************************
//  776  * function name  : R_SSCMD_ProcessCmd_CTSR
//  777  * description	  : "Continuous Transmisson Stop Request(CTSR)" Command Processing
//  778  * parameters	  : pCmd...pointer to the uart data
//  779  *				  :   CTSR (handle)
//  780  * return value   : none
//  781  * output		  : Confirm command
//  782  *				  :   CTSC (handle) (status) (rfstate)
//  783  **********************************************************************/
//  784 static void R_SSCMD_ProcessCmd_CTSR( uint8_t *pCmd )
//  785 {
//  786 	uint8_t status = R_STATUS_SUCCESS;
//  787 	uint8_t res;
//  788 	uint8_t handle;
//  789 
//  790 	/* get command parameters */
//  791 	/* (handle) */
//  792 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
//  793 	if ( res != 0 )
//  794 	{
//  795 		status = R_STATUS_INVALID_COMMAND;
//  796 	}
//  797 
//  798 	/* command execution */
//  799 	if ( status == R_STATUS_SUCCESS )
//  800 	{
//  801 		status = RfdrvIf_ContTxStop();
//  802 	}
//  803 
//  804 	/* "CTSC" command transmission */
//  805 	PRINTF( "CTSC " );
//  806 	PRINTF( "%02X ", handle );
//  807 	PRINTF( "%02X ", status );
//  808 	PRINTF( "%02X\n", gRfState );
//  809 }
//  810 
//  811 /***********************************************************************
//  812  * function name  : R_SSCMD_ProcessCmd_SETR
//  813  * description	  : "Set Parameter Request(SETR)" Command Processing
//  814  * parameters	  : pCmd...pointer to the uart data
//  815  *				  :   SETR (handle) (paramId) (parameter)
//  816  * return value   : none
//  817  * output		  : Confirm command
//  818  *				  :   SETC (handle) (status) (rfstate) (paramId)
//  819  **********************************************************************/
//  820 static void R_SSCMD_ProcessCmd_SETR( uint8_t *pCmd )
//  821 {
//  822 	uint8_t status = R_STATUS_SUCCESS;
//  823 	uint8_t res;
//  824 	uint8_t handle;
//  825 	uint8_t paramId;
//  826 	uint8_t i, size;
//  827 
//  828 	/* get command parameters */
//  829 	/* (handle) */
//  830 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
//  831 	/* (paramId) */
//  832 	res |= AppHexStrToNum( &pCmd, &paramId, sizeof(paramId), R_FALSE );
//  833 	if ( res != 0 )
//  834 	{
//  835 		status = R_STATUS_INVALID_COMMAND;
//  836 	}
//  837 
//  838 	/* command execution */
//  839 	if ( status == R_STATUS_SUCCESS )
//  840 	{
//  841 		status = R_STATUS_INVALID_PARAMETER;
//  842 
//  843 		size = sizeof(gc_SetrFunc)/sizeof(gc_SetrFunc[0]);
//  844 		for ( i=0; i<size; i++ )
//  845 		{
//  846 			if ( gc_SetrFunc[i].paramId == paramId )
//  847 			{
//  848 				status = gc_SetrFunc[i].pFunc( pCmd );
//  849 				break;
//  850 			}
//  851 		}
//  852 	}
//  853 
//  854 	/* "SETC" command transmission */
//  855 	PRINTF( "SETC " );
//  856 	PRINTF( "%02X ", handle );
//  857 	PRINTF( "%02X ", status );
//  858 	PRINTF( "%02X", gRfState );
//  859 	if ( status == R_STATUS_SUCCESS )
//  860 	{
//  861 		PRINTF( " %02X", paramId );
//  862 	}
//  863 	PRINTF( "\n" );
//  864 }
//  865 
//  866 /***********************************************************************
//  867  * function name  : R_SetR_channel
//  868  * description	  : "Set Parameter Request(SETR)" Command Processing
//  869  * parameters	  : pCmd...pointer to the uart data
//  870  * return value   : status
//  871  **********************************************************************/
//  872 static uint8_t R_SetR_channel( uint8_t *pCmd )
//  873 {
//  874 	uint8_t status = R_STATUS_SUCCESS;
//  875 	uint8_t res;
//  876 	uint8_t freqBandId, fskOpeMode, channel;
//  877 	int32_t frequencyOffset;
//  878 
//  879 	/* get command parameters */
//  880 	/* (freqBandId) */
//  881 	res = AppHexStrToNum( &pCmd, &freqBandId, sizeof(freqBandId), R_FALSE );
//  882 	/* (fskOpeMode) */
//  883 	res |= AppHexStrToNum( &pCmd, &fskOpeMode, sizeof(fskOpeMode), R_FALSE );
//  884 	/* (channel) */
//  885 	res |= AppHexStrToNum( &pCmd, &channel, sizeof(channel), R_FALSE );
//  886 	/* (frequencyOffset) */
//  887 	res |= AppHexStrToNum( &pCmd, &frequencyOffset, sizeof(frequencyOffset), R_FALSE );
//  888 	if ( res != 0 )
//  889 	{
//  890 		status = R_STATUS_INVALID_COMMAND;
//  891 	}
//  892 
//  893 	/* command execution */
//  894 	if ( status == R_STATUS_SUCCESS )
//  895 	{
//  896 		/* Set Attribute Value */
//  897 		status = RfdrvIf_SetAttrChannel( freqBandId, fskOpeMode, channel, frequencyOffset );
//  898 	}
//  899 
//  900 	return( status );
//  901 }
//  902 
//  903 /***********************************************************************
//  904  * function name  : R_SetR_transmitPower
//  905  * description	  : "Set Parameter Request(SETR)" Command Processing
//  906  * parameters	  : pCmd...pointer to the uart data
//  907  * return value   : status
//  908  **********************************************************************/
//  909 static uint8_t R_SetR_transmitPower( uint8_t *pCmd )
//  910 {
//  911 	uint8_t status = R_STATUS_SUCCESS;
//  912 	uint8_t res;
//  913 	uint8_t freqBandId, transmitPower;
//  914 
//  915 	/* get command parameters */
//  916 	/* (freqBandId) */
//  917 	res = AppHexStrToNum( &pCmd, &freqBandId, sizeof(freqBandId), R_FALSE );
//  918 	/* (transmitPower) */
//  919 	res |= AppHexStrToNum( &pCmd, &transmitPower, sizeof(transmitPower), R_FALSE );
//  920 	if ( res != 0 )
//  921 	{
//  922 		status = R_STATUS_INVALID_COMMAND;
//  923 	}
//  924 
//  925 	/* command execution */
//  926 	if ( status == R_STATUS_SUCCESS )
//  927 	{
//  928 		/* Set Attribute Value */
//  929 		status = RfdrvIf_SetAttrTxPower( freqBandId, transmitPower );
//  930 	}
//  931 
//  932 	return( status );
//  933 }
//  934 
//  935 /***********************************************************************
//  936  * function name  : R_SetR_preambleLength
//  937  * description	  : "Set Parameter Request(SETR)" Command Processing
//  938  * parameters	  : pCmd...pointer to the uart data
//  939  * return value   : status
//  940  **********************************************************************/
//  941 static uint8_t R_SetR_preambleLength( uint8_t *pCmd )
//  942 {
//  943 	uint8_t status = R_STATUS_SUCCESS;
//  944 	uint8_t res;
//  945 	uint16_t preambleLength;
//  946 
//  947 	/* get command parameters */
//  948 	/* (preambleLength) */
//  949 	res = AppHexStrToNum( &pCmd, &preambleLength, sizeof(preambleLength), R_FALSE );
//  950 	if ( res != 0 )
//  951 	{
//  952 		status = R_STATUS_INVALID_COMMAND;
//  953 	}
//  954 
//  955 	/* check parameter */
//  956 	if ( status == R_STATUS_SUCCESS )
//  957 	{
//  958 		if (( preambleLength < R_MIN_preambleLength )
//  959 			||( R_MAX_preambleLength < preambleLength ))
//  960 		{
//  961 			status = R_STATUS_INVALID_PARAMETER;
//  962 		}
//  963 	}
//  964 
//  965 	/* command execution */
//  966 	if ( status == R_STATUS_SUCCESS )
//  967 	{
//  968 		/* Set Attribute Value */
//  969 		status = RfdrvIf_SetAttr( RP_PHY_FSK_PREAMBLE_LENGTH, sizeof(preambleLength), (uint8_t *)&preambleLength );
//  970 	}
//  971 
//  972 	return( status );
//  973 }
//  974 
//  975 /***********************************************************************
//  976  * function name  : R_SetR_agcWaitGainOffset
//  977  * description	  : "Set Parameter Request(SETR)" Command Processing
//  978  * parameters	  : pCmd...pointer to the uart data
//  979  * return value   : status
//  980  **********************************************************************/
//  981 static uint8_t R_SetR_agcWaitGainOffset( uint8_t *pCmd )
//  982 {
//  983 	uint8_t status = R_STATUS_SUCCESS;
//  984 	uint8_t res;
//  985 	uint8_t agcWaitGainOffset;
//  986 
//  987 	/* get command parameters */
//  988 	/* (agcWaitGainOffset) */
//  989 	res = AppHexStrToNum( &pCmd, &agcWaitGainOffset, sizeof(agcWaitGainOffset), R_FALSE );
//  990 	if ( res != 0 )
//  991 	{
//  992 		status = R_STATUS_INVALID_COMMAND;
//  993 	}
//  994 
//  995 	/* check parameter */
//  996 	if ( status == R_STATUS_SUCCESS )
//  997 	{
//  998 		if ( R_MAX_agcWaitGainOffset < agcWaitGainOffset )
//  999 		{
// 1000 			status = R_STATUS_INVALID_PARAMETER;
// 1001 		}
// 1002 	}
// 1003 
// 1004 	/* command execution */
// 1005 	if ( status == R_STATUS_SUCCESS )
// 1006 	{
// 1007 		/* Set Attribute Value */
// 1008 		status = RfdrvIf_SetAttr( RP_PHY_AGC_WAIT_GAIN_OFFSET, sizeof(agcWaitGainOffset), (uint8_t *)&agcWaitGainOffset );
// 1009 	}
// 1010 
// 1011 	return( status );
// 1012 }
// 1013 
// 1014 /***********************************************************************
// 1015  * function name  : R_SetR_antennaSwitchEna
// 1016  * description	  : "Set Parameter Request(SETR)" Command Processing
// 1017  * parameters	  : pCmd...pointer to the uart data
// 1018  * return value   : status
// 1019  **********************************************************************/
// 1020 static uint8_t R_SetR_antennaSwitchEna( uint8_t *pCmd )
// 1021 {
// 1022 	uint8_t status = R_STATUS_SUCCESS;
// 1023 	uint8_t res;
// 1024 	uint8_t antennaSwitchEna;
// 1025 
// 1026 	/* get command parameters */
// 1027 	/* (antennaSwitchEna) */
// 1028 	res = AppHexStrToNum( &pCmd, &antennaSwitchEna, sizeof(antennaSwitchEna), R_FALSE );
// 1029 	if ( res != 0 )
// 1030 	{
// 1031 		status = R_STATUS_INVALID_COMMAND;
// 1032 	}
// 1033 
// 1034 	/* check parameter */
// 1035 	if ( status == R_STATUS_SUCCESS )
// 1036 	{
// 1037 		switch (antennaSwitchEna)
// 1038 		{
// 1039 			case 0x00:
// 1040 			case 0x01:
// 1041 				break;
// 1042 			default:
// 1043 				status = R_STATUS_INVALID_PARAMETER;
// 1044 				break;
// 1045 		}
// 1046 	}
// 1047 
// 1048 	/* command execution */
// 1049 	if ( status == R_STATUS_SUCCESS )
// 1050 	{
// 1051 		/* Set Attribute Value */
// 1052 		status = RfdrvIf_SetAttr( RP_PHY_ANTENNA_SWITCH_ENA, sizeof(antennaSwitchEna), (uint8_t *)&antennaSwitchEna );
// 1053 	}
// 1054 
// 1055 	return( status );
// 1056 }
// 1057 
// 1058 /***********************************************************************
// 1059  * function name  : R_SetR_antennaSwitchEnaTiming
// 1060  * description	  : "Set Parameter Request(SETR)" Command Processing
// 1061  * parameters	  : pCmd...pointer to the uart data
// 1062  * return value   : status
// 1063  **********************************************************************/
// 1064 static uint8_t R_SetR_antennaSwitchEnaTiming( uint8_t *pCmd )
// 1065 {
// 1066 	uint8_t status = R_STATUS_SUCCESS;
// 1067 	uint8_t res;
// 1068 	uint16_t antennaSwitchEnaTiming;
// 1069 
// 1070 	/* get command parameters */
// 1071 	/* (antennaSwitchEnaTiming) */
// 1072 	res = AppHexStrToNum( &pCmd, &antennaSwitchEnaTiming, sizeof(antennaSwitchEnaTiming), R_FALSE );
// 1073 	if ( res != 0 )
// 1074 	{
// 1075 		status = R_STATUS_INVALID_COMMAND;
// 1076 	}
// 1077 
// 1078 	/* check parameter */
// 1079 	if ( status == R_STATUS_SUCCESS )
// 1080 	{
// 1081 		if (( antennaSwitchEnaTiming < R_MIN_antennaSwitchEnaTiming )
// 1082 			||( R_MAX_antennaSwitchEnaTiming < antennaSwitchEnaTiming ))
// 1083 		{
// 1084 			status = R_STATUS_INVALID_PARAMETER;
// 1085 		}
// 1086 	}
// 1087 
// 1088 	/* command execution */
// 1089 	if ( status == R_STATUS_SUCCESS )
// 1090 	{
// 1091 		/* Set Attribute Value */
// 1092 		status = RfdrvIf_SetAttr( RP_PHY_ANTENNA_SWITCH_ENA_TIMING, sizeof(antennaSwitchEnaTiming), (uint8_t *)&antennaSwitchEnaTiming );
// 1093 	}
// 1094 
// 1095 	return( status );
// 1096 }
// 1097 
// 1098 /***********************************************************************
// 1099  * function name  : R_SetR_gpio0Setting
// 1100  * description	  : "Set Parameter Request(SETR)" Command Processing
// 1101  * parameters	  : pCmd...pointer to the uart data
// 1102  * return value   : status
// 1103  **********************************************************************/
// 1104 static uint8_t R_SetR_gpio0Setting( uint8_t *pCmd )
// 1105 {
// 1106 	uint8_t status = R_STATUS_SUCCESS;
// 1107 	uint8_t res;
// 1108 	uint8_t gpio0Setting;
// 1109 
// 1110 	/* get command parameters */
// 1111 	/* (gpio0Setting) */
// 1112 	res = AppHexStrToNum( &pCmd, &gpio0Setting, sizeof(gpio0Setting), R_FALSE );
// 1113 	if ( res != 0 )
// 1114 	{
// 1115 		status = R_STATUS_INVALID_COMMAND;
// 1116 	}
// 1117 
// 1118 	/* check parameter */
// 1119 	if ( status == R_STATUS_SUCCESS )
// 1120 	{
// 1121 		switch (gpio0Setting)
// 1122 		{
// 1123 			case 0x00:
// 1124 			case 0x01:
// 1125 				break;
// 1126 			default:
// 1127 				status = R_STATUS_INVALID_PARAMETER;
// 1128 				break;
// 1129 		}
// 1130 	}
// 1131 
// 1132 	/* command execution */
// 1133 	if ( status == R_STATUS_SUCCESS )
// 1134 	{
// 1135 		/* Set Attribute Value */
// 1136 		status = RfdrvIf_SetAttr( RP_PHY_GPIO0_SETTING, sizeof(gpio0Setting), (uint8_t *)&gpio0Setting );
// 1137 	}
// 1138 
// 1139 	return( status );
// 1140 }
// 1141 
// 1142 /***********************************************************************
// 1143  * function name  : R_SetR_gpio3Setting
// 1144  * description	  : "Set Parameter Request(SETR)" Command Processing
// 1145  * parameters	  : pCmd...pointer to the uart data
// 1146  * return value   : status
// 1147  **********************************************************************/
// 1148 static uint8_t R_SetR_gpio3Setting( uint8_t *pCmd )
// 1149 {
// 1150 	uint8_t status = R_STATUS_SUCCESS;
// 1151 	uint8_t res;
// 1152 	uint8_t gpio3Setting;
// 1153 
// 1154 	/* get command parameters */
// 1155 	/* (gpio1Setting) */
// 1156 	res = AppHexStrToNum( &pCmd, &gpio3Setting, sizeof(gpio3Setting), R_FALSE );
// 1157 	if ( res != 0 )
// 1158 	{
// 1159 		status = R_STATUS_INVALID_COMMAND;
// 1160 	}
// 1161 
// 1162 	/* check parameter */
// 1163 	if ( status == R_STATUS_SUCCESS )
// 1164 	{
// 1165 		switch (gpio3Setting)
// 1166 		{
// 1167 			case 0x00:
// 1168 			case 0x01:
// 1169 				break;
// 1170 			default:
// 1171 				status = R_STATUS_INVALID_PARAMETER;
// 1172 				break;
// 1173 		}
// 1174 	}
// 1175 
// 1176 	/* command execution */
// 1177 	if ( status == R_STATUS_SUCCESS )
// 1178 	{
// 1179 		/* Set Attribute Value */
// 1180 		status = RfdrvIf_SetAttr( RP_PHY_GPIO3_SETTING, sizeof(gpio3Setting), (uint8_t *)&gpio3Setting );
// 1181 	}
// 1182 
// 1183 	return( status );
// 1184 }
// 1185 
// 1186 /***********************************************************************
// 1187  * function name  : R_SetR_antennaSelectTx
// 1188  * description	  : "Set Parameter Request(SETR)" Command Processing
// 1189  * parameters	  : pCmd...pointer to the uart data
// 1190  * return value   : status
// 1191  **********************************************************************/
// 1192 static uint8_t R_SetR_antennaSelectTx( uint8_t *pCmd )
// 1193 {
// 1194 	uint8_t status = R_STATUS_SUCCESS;
// 1195 	uint8_t res;
// 1196 	uint8_t antennaSelectTx;
// 1197 
// 1198 	/* get command parameters */
// 1199 	/* (antennaSelectTx) */
// 1200 	res = AppHexStrToNum( &pCmd, &antennaSelectTx, sizeof(antennaSelectTx), R_FALSE );
// 1201 	if ( res != 0 )
// 1202 	{
// 1203 		status = R_STATUS_INVALID_COMMAND;
// 1204 	}
// 1205 
// 1206 	/* check parameter */
// 1207 	if ( status == R_STATUS_SUCCESS )
// 1208 	{
// 1209 		switch (antennaSelectTx)
// 1210 		{
// 1211 			case 0x00:
// 1212 			case 0x01:
// 1213 				break;
// 1214 			default:
// 1215 				status = R_STATUS_INVALID_PARAMETER;
// 1216 				break;
// 1217 		}
// 1218 	}
// 1219 
// 1220 	/* command execution */
// 1221 	if ( status == R_STATUS_SUCCESS )
// 1222 	{
// 1223 		/* Set Attribute Value */
// 1224 		status = RfdrvIf_SetAttr( RP_PHY_ANTENNA_SELECT_TX, sizeof(antennaSelectTx), (uint8_t *)&antennaSelectTx );
// 1225 	}
// 1226 
// 1227 	return( status );
// 1228 }
// 1229 
// 1230 /***********************************************************************
// 1231  * function name  : R_SSCMD_ProcessCmd_GETR
// 1232  * description	  : "Set Parameter Request(GETR)" Command Processing
// 1233  * parameters	  : pCmd...pointer to the uart data
// 1234  *				  :   GETR (handle) (paramId)
// 1235  * return value   : none
// 1236  * output		  : Confirm command
// 1237  *				  :   GETC (handle) (status) (rfstate) (paramId) (parameter)
// 1238  **********************************************************************/
// 1239 static void R_SSCMD_ProcessCmd_GETR( uint8_t *pCmd )
// 1240 {
// 1241 	uint8_t status = R_STATUS_SUCCESS;
// 1242 	uint8_t res;
// 1243 	uint8_t handle;
// 1244 	uint8_t paramId;
// 1245 	uint8_t i, size;
// 1246 
// 1247 	/* get command parameters */
// 1248 	/* (handle) */
// 1249 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
// 1250 	/* (paramId) */
// 1251 	res |= AppHexStrToNum( &pCmd, &paramId, sizeof(paramId), R_FALSE );
// 1252 	if ( res != 0 )
// 1253 	{
// 1254 		status = R_STATUS_INVALID_COMMAND;
// 1255 	}
// 1256 
// 1257 	/* command execution */
// 1258 	if ( status == R_STATUS_SUCCESS )
// 1259 	{
// 1260 		status = R_STATUS_INVALID_PARAMETER;
// 1261 
// 1262 		size = sizeof(gc_GetrFunc)/sizeof(gc_GetrFunc[0]);
// 1263 		for ( i=0; i<size; i++ )
// 1264 		{
// 1265 			if ( gc_GetrFunc[i].paramId == paramId )
// 1266 			{
// 1267 				gc_GetrFunc[i].pFunc( handle );
// 1268 				status = R_STATUS_SUCCESS;
// 1269 				break;
// 1270 			}
// 1271 		}
// 1272 	}
// 1273 
// 1274 	/* "GETC" command transmission (error) */
// 1275 	if ( status != R_STATUS_SUCCESS )
// 1276 	{
// 1277 		PRINTF( "GETC " );
// 1278 		PRINTF( "%02X ", handle );
// 1279 		PRINTF( "%02X ", status );
// 1280 		PRINTF( "%02X\n", gRfState );
// 1281 	}
// 1282 }
// 1283 
// 1284 /***********************************************************************
// 1285  * function name  : R_GetR_channel
// 1286  * description	  : "Get Parameter Request(GETR)" Command Processing
// 1287  * parameters	  : handle...handle value
// 1288  * return value   : none
// 1289  * output         : Confirm command
// 1290  *                :   GETC (handle) (status) (paramId) (freqBandId) (fskOpeMode) (channel)
// 1291  **********************************************************************/
// 1292 static void R_GetR_channel( uint8_t handle )
// 1293 {
// 1294 	uint8_t status;
// 1295 	uint8_t attrLength = 0;
// 1296 	uint8_t freqBandId, fskOpeMode, channel;
// 1297 	uint32_t frequencyOffset;
// 1298 	uint32_t frequency;
// 1299 
// 1300 	/* command execution */
// 1301 	/* Get Attribute Value */
// 1302 	status = RfdrvIf_GetAttr( RP_PHY_FREQ_BAND_ID, sizeof(freqBandId), &attrLength, (uint8_t *)&freqBandId );
// 1303 	if ( status == R_STATUS_SUCCESS )
// 1304 	{
// 1305 		status = RfdrvIf_GetAttr( RP_PHY_FSK_OPE_MODE, sizeof(fskOpeMode), &attrLength, (uint8_t *)&fskOpeMode );
// 1306 	}
// 1307 	if ( status == R_STATUS_SUCCESS )
// 1308 	{
// 1309 		status = RfdrvIf_GetAttr( RP_PHY_CURRENT_CHANNEL, sizeof(channel), &attrLength, (uint8_t *)&channel );
// 1310 	}
// 1311 	if ( status == R_STATUS_SUCCESS )
// 1312 	{
// 1313 		status = RfdrvIf_GetAttr( RP_PHY_FREQUENCY_OFFSET, sizeof(frequencyOffset), &attrLength, (uint8_t *)&frequencyOffset );
// 1314 	}
// 1315 	if ( status == R_STATUS_SUCCESS )
// 1316 	{
// 1317 		status = RfdrvIf_GetAttr( RP_PHY_FREQUENCY, sizeof(frequency), &attrLength, (uint8_t *)&frequency );
// 1318 	}
// 1319 
// 1320 	/* "GETC" command transmission */
// 1321 	PRINTF( "GETC " );
// 1322 	PRINTF( "%02X ", handle );
// 1323 	PRINTF( "%02X ", status );
// 1324 	PRINTF( "%02X", gRfState );
// 1325 	if ( status == R_STATUS_SUCCESS )
// 1326 	{
// 1327 		PRINTF( " " );
// 1328 		PRINTF( "%02X ", R_ParamId_channel );
// 1329 		PRINTF( "%02X ", freqBandId );
// 1330 		PRINTF( "%02X ", fskOpeMode );
// 1331 		PRINTF( "%02X", channel );
// 1332 		PRINTF( " " );
// 1333 		R_printf_32data( (uint8_t *)&frequencyOffset );
// 1334 		PRINTF( " " );
// 1335 		R_printf_32data( (uint8_t *)&frequency );
// 1336 	}
// 1337 	PRINTF( "\n" );
// 1338 }
// 1339 
// 1340 /***************************************************************************************************************
// 1341  * function name  : R_printf_32data
// 1342  * description	  : display 32bit data
// 1343  * parameters	  : uint8_t *p_data...pointer to data
// 1344  * return value   : none
// 1345  **************************************************************************************************************/
// 1346 static void R_printf_32data( uint8_t *p_data )
// 1347 {
// 1348 	uint16_t i;
// 1349 	uint16_t size = sizeof(uint32_t);
// 1350 
// 1351 	p_data += size;
// 1352 	for ( i=0; i<size; i++ )
// 1353 	{
// 1354 		PRINTF( "%02X", *(--p_data) );
// 1355 	}
// 1356 }
// 1357 
// 1358 /***********************************************************************
// 1359  * function name  : R_GetR_transmitPower
// 1360  * description	  : "Get Parameter Request(GETR)" Command Processing
// 1361  * parameters	  : handle...handle value
// 1362  * return value   : none
// 1363  * output         : Confirm command
// 1364  *                :   GETC (handle) (status) (paramId) (freqBandId) (transmitPower)
// 1365  **********************************************************************/
// 1366 static void R_GetR_transmitPower( uint8_t handle )
// 1367 {
// 1368 	uint8_t status;
// 1369 	uint8_t attrLength = 0;
// 1370 	uint8_t freqBandId, transmitPower;
// 1371 
// 1372 	/* command execution */
// 1373 	/* Get Attribute Value */
// 1374 	status = RfdrvIf_GetAttr( RP_PHY_FREQ_BAND_ID, sizeof(freqBandId), &attrLength, (uint8_t *)&freqBandId );
// 1375 	if ( status == R_STATUS_SUCCESS )
// 1376 	{
// 1377 		status = RfdrvIf_GetAttr( RP_PHY_TRANSMIT_POWER, sizeof(transmitPower), &attrLength, (uint8_t *)&transmitPower );
// 1378 	}
// 1379 
// 1380 	/* "GETC" command transmission */
// 1381 	PRINTF( "GETC " );
// 1382 	PRINTF( "%02X ", handle );
// 1383 	PRINTF( "%02X ", status );
// 1384 	PRINTF( "%02X", gRfState );
// 1385 	if ( status == R_STATUS_SUCCESS )
// 1386 	{
// 1387 		PRINTF( " " );
// 1388 		PRINTF( "%02X ", R_ParamId_transmitPower );
// 1389 		PRINTF( "%02X ", freqBandId );
// 1390 		PRINTF( "%02X", transmitPower );
// 1391 	}
// 1392 	PRINTF( "\n" );
// 1393 }
// 1394 
// 1395 /***********************************************************************
// 1396  * function name  : R_GetR_preambleLength
// 1397  * description	  : "Get Parameter Request(GETR)" Command Processing
// 1398  * parameters	  : handle...handle value
// 1399  * return value   : none
// 1400  * output         : Confirm command
// 1401  *                :   GETC (handle) (status) (paramId) (preambleLength)
// 1402  **********************************************************************/
// 1403 static void R_GetR_preambleLength( uint8_t handle )
// 1404 {
// 1405 	uint8_t status;
// 1406 	uint8_t attrLength = 0;
// 1407 	uint16_t preambleLength;
// 1408 
// 1409 	/* command execution */
// 1410 	/* Get Attribute Value */
// 1411 	status = RfdrvIf_GetAttr( RP_PHY_FSK_PREAMBLE_LENGTH, sizeof(preambleLength), &attrLength, (uint8_t *)&preambleLength );
// 1412 
// 1413 	/* "GETC" command transmission */
// 1414 	PRINTF( "GETC " );
// 1415 	PRINTF( "%02X ", handle );
// 1416 	PRINTF( "%02X ", status );
// 1417 	PRINTF( "%02X", gRfState );
// 1418 	if ( status == R_STATUS_SUCCESS )
// 1419 	{
// 1420 		PRINTF( " " );
// 1421 		PRINTF( "%02X ", R_ParamId_preambleLength );
// 1422 		PRINTF( "%04X", preambleLength );
// 1423 	}
// 1424 	PRINTF( "\n" );
// 1425 }
// 1426 
// 1427 /***********************************************************************
// 1428  * function name  : R_GetR_agcWaitGainOffset
// 1429  * description	  : "Get Parameter Request(GETR)" Command Processing
// 1430  * parameters	  : handle...handle value
// 1431  * return value   : none
// 1432  * output         : Confirm command
// 1433  *                :   GETC (handle) (status) (paramId) (agcWaitGainOffset)
// 1434  **********************************************************************/
// 1435 static void R_GetR_agcWaitGainOffset( uint8_t handle )
// 1436 {
// 1437 	uint8_t status;
// 1438 	uint8_t attrLength = 0;
// 1439 	uint8_t agcWaitGainOffset;
// 1440 
// 1441 	/* command execution */
// 1442 	/* Get Attribute Value */
// 1443 	status = RfdrvIf_GetAttr( RP_PHY_AGC_WAIT_GAIN_OFFSET, sizeof(agcWaitGainOffset), &attrLength, (uint8_t *)&agcWaitGainOffset );
// 1444 
// 1445 	/* "GETC" command transmission */
// 1446 	PRINTF( "GETC " );
// 1447 	PRINTF( "%02X ", handle );
// 1448 	PRINTF( "%02X ", status );
// 1449 	PRINTF( "%02X", gRfState );
// 1450 	if ( status == R_STATUS_SUCCESS )
// 1451 	{
// 1452 		PRINTF( " " );
// 1453 		PRINTF( "%02X ", R_ParamId_agcWaitGainOffset );
// 1454 		PRINTF( "%02X", agcWaitGainOffset );
// 1455 	}
// 1456 	PRINTF( "\n" );
// 1457 }
// 1458 
// 1459 /***********************************************************************
// 1460  * function name  : R_GetR_antennaSwitchEna
// 1461  * description	  : "Get Parameter Request(GETR)" Command Processing
// 1462  * parameters	  : handle...handle value
// 1463  * return value   : none
// 1464  * output         : Confirm command
// 1465  *                :   GETC (handle) (status) (paramId) (antennaSwitchEna)
// 1466  **********************************************************************/
// 1467 static void R_GetR_antennaSwitchEna( uint8_t handle )
// 1468 {
// 1469 	uint8_t status;
// 1470 	uint8_t attrLength = 0;
// 1471 	uint8_t antennaSwitchEna;
// 1472 
// 1473 	/* command execution */
// 1474 	/* Get Attribute Value */
// 1475 	status = RfdrvIf_GetAttr( RP_PHY_ANTENNA_SWITCH_ENA, sizeof(antennaSwitchEna), &attrLength, (uint8_t *)&antennaSwitchEna );
// 1476 
// 1477 	/* "GETC" command transmission */
// 1478 	PRINTF( "GETC " );
// 1479 	PRINTF( "%02X ", handle );
// 1480 	PRINTF( "%02X ", status );
// 1481 	PRINTF( "%02X", gRfState );
// 1482 	if ( status == R_STATUS_SUCCESS )
// 1483 	{
// 1484 		PRINTF( " " );
// 1485 		PRINTF( "%02X ", R_ParamId_antennaSwitchEna );
// 1486 		PRINTF( "%02X", antennaSwitchEna );
// 1487 	}
// 1488 	PRINTF( "\n" );
// 1489 }
// 1490 
// 1491 /***********************************************************************
// 1492  * function name  : R_GetR_antennaSwitchEnaTiming
// 1493  * description	  : "Get Parameter Request(GETR)" Command Processing
// 1494  * parameters	  : handle...handle value
// 1495  * return value   : none
// 1496  * output         : Confirm command
// 1497  *                :   GETC (handle) (status) (paramId) (antennaSwitchEnaTiming)
// 1498  **********************************************************************/
// 1499 static void R_GetR_antennaSwitchEnaTiming( uint8_t handle )
// 1500 {
// 1501 	uint8_t status;
// 1502 	uint8_t attrLength = 0;
// 1503 	uint16_t antennaSwitchEnaTiming;
// 1504 
// 1505 	/* command execution */
// 1506 	/* Get Attribute Value */
// 1507 	status = RfdrvIf_GetAttr( RP_PHY_ANTENNA_SWITCH_ENA_TIMING, sizeof(antennaSwitchEnaTiming), &attrLength, (uint8_t *)&antennaSwitchEnaTiming );
// 1508 
// 1509 	/* "GETC" command transmission */
// 1510 	PRINTF( "GETC " );
// 1511 	PRINTF( "%02X ", handle );
// 1512 	PRINTF( "%02X ", status );
// 1513 	PRINTF( "%02X", gRfState );
// 1514 	if ( status == R_STATUS_SUCCESS )
// 1515 	{
// 1516 		PRINTF( " " );
// 1517 		PRINTF( "%02X ", R_ParamId_antennaSwitchEnaTiming );
// 1518 		PRINTF( "%04X", antennaSwitchEnaTiming );
// 1519 	}
// 1520 	PRINTF( "\n" );
// 1521 }
// 1522 
// 1523 /***********************************************************************
// 1524  * function name  : R_GetR_gpio0Setting
// 1525  * description	  : "Get Parameter Request(GETR)" Command Processing
// 1526  * parameters	  : handle...handle value
// 1527  * return value   : none
// 1528  * output         : Confirm command
// 1529  *                :   GETC (handle) (status) (paramId) (gpio0Setting)
// 1530  **********************************************************************/
// 1531 static void R_GetR_gpio0Setting( uint8_t handle )
// 1532 {
// 1533 	uint8_t status;
// 1534 	uint8_t attrLength = 0;
// 1535 	uint8_t gpio0Setting;
// 1536 
// 1537 	/* command execution */
// 1538 	/* Get Attribute Value */
// 1539 	status = RfdrvIf_GetAttr( RP_PHY_GPIO0_SETTING, sizeof(gpio0Setting), &attrLength, (uint8_t *)&gpio0Setting );
// 1540 
// 1541 	/* "GETC" command transmission */
// 1542 	PRINTF( "GETC " );
// 1543 	PRINTF( "%02X ", handle );
// 1544 	PRINTF( "%02X ", status );
// 1545 	PRINTF( "%02X", gRfState );
// 1546 	if ( status == R_STATUS_SUCCESS )
// 1547 	{
// 1548 		PRINTF( " " );
// 1549 		PRINTF( "%02X ", R_ParamId_gpio0Setting );
// 1550 		PRINTF( "%02X", gpio0Setting );
// 1551 	}
// 1552 	PRINTF( "\n" );
// 1553 }
// 1554 
// 1555 /***********************************************************************
// 1556  * function name  : R_GetR_gpio3Setting
// 1557  * description	  : "Get Parameter Request(GETR)" Command Processing
// 1558  * parameters	  : handle...handle value
// 1559  * return value   : none
// 1560  * output         : Confirm command
// 1561  *                :   GETC (handle) (status) (paramId) (gpio3Setting)
// 1562  **********************************************************************/
// 1563 static void R_GetR_gpio3Setting( uint8_t handle )
// 1564 {
// 1565 	uint8_t status;
// 1566 	uint8_t attrLength = 0;
// 1567 	uint8_t gpio3Setting;
// 1568 
// 1569 	/* command execution */
// 1570 	/* Get Attribute Value */
// 1571 	status = RfdrvIf_GetAttr( RP_PHY_GPIO3_SETTING, sizeof(gpio3Setting), &attrLength, (uint8_t *)&gpio3Setting );
// 1572 
// 1573 	/* "GETC" command transmission */
// 1574 	PRINTF( "GETC " );
// 1575 	PRINTF( "%02X ", handle );
// 1576 	PRINTF( "%02X ", status );
// 1577 	PRINTF( "%02X", gRfState );
// 1578 	if ( status == R_STATUS_SUCCESS )
// 1579 	{
// 1580 		PRINTF( " " );
// 1581 		PRINTF( "%02X ", R_ParamId_gpio3Setting );
// 1582 		PRINTF( "%02X", gpio3Setting );
// 1583 	}
// 1584 	PRINTF( "\n" );
// 1585 }
// 1586 
// 1587 /***********************************************************************
// 1588  * function name  : R_GetR_antennaSelectTx
// 1589  * description	  : "Get Parameter Request(GETR)" Command Processing
// 1590  * parameters	  : handle...handle value
// 1591  * return value   : none
// 1592  * output         : Confirm command
// 1593  *                :   GETC (handle) (status) (paramId) (antennaSelectTx)
// 1594  **********************************************************************/
// 1595 static void R_GetR_antennaSelectTx( uint8_t handle )
// 1596 {
// 1597 	uint8_t status;
// 1598 	uint8_t attrLength = 0;
// 1599 	uint8_t antennaSelectTx;
// 1600 
// 1601 	/* command execution */
// 1602 	/* Get Attribute Value */
// 1603 	status = RfdrvIf_GetAttr( RP_PHY_ANTENNA_SELECT_TX, sizeof(antennaSelectTx), &attrLength, (uint8_t *)&antennaSelectTx );
// 1604 
// 1605 	/* "GETC" command transmission */
// 1606 	PRINTF( "GETC " );
// 1607 	PRINTF( "%02X ", handle );
// 1608 	PRINTF( "%02X ", status );
// 1609 	PRINTF( "%02X", gRfState );
// 1610 	if ( status == R_STATUS_SUCCESS )
// 1611 	{
// 1612 		PRINTF( " " );
// 1613 		PRINTF( "%02X ", R_ParamId_antennaSelectTx );
// 1614 		PRINTF( "%02X", antennaSelectTx );
// 1615 	}
// 1616 	PRINTF( "\n" );
// 1617 }
// 1618 
// 1619 /***********************************************************************
// 1620  * function name  : R_SSCMD_ProcessCmd_SIBR
// 1621  * description	  : "Set IB Value Request(SIBR)" Command Processing
// 1622  * parameters	  : pCmd...pointer to the uart data
// 1623  *				  :   SIBR (handle) (attrId) (attrLength) (attrValue)
// 1624  * return value   : none
// 1625  * output		  : Confirm command
// 1626  *				  :   SIBC (handle) (status) (rfstate) (attrId)
// 1627  **********************************************************************/
// 1628 static void R_SSCMD_ProcessCmd_SIBR( uint8_t *pCmd )
// 1629 {
// 1630 	uint8_t status = R_STATUS_SUCCESS;
// 1631 	uint8_t res;
// 1632 	uint8_t handle;
// 1633 	uint8_t attrId;
// 1634 	uint8_t attrLength;
// 1635 	r_rfdrvIf_attr_t attrValue;
// 1636 
// 1637 	/* get command parameters */
// 1638 	/* (handle) */
// 1639 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
// 1640 	/* (attrId) */
// 1641 	res |= AppHexStrToNum( &pCmd, &attrId, sizeof(attrId), R_FALSE );
// 1642 	/* (attrLength) */
// 1643 	res |= AppHexStrToNum( &pCmd, &attrLength, sizeof(attrLength), R_FALSE );
// 1644 	if ( res == 0 )
// 1645 	{
// 1646 		if ( attrLength > sizeof(attrValue) )
// 1647 		{
// 1648 			status = R_STATUS_INVALID_COMMAND_PARAMETER;
// 1649 		}
// 1650 		else
// 1651 		{
// 1652 			/* (attrValue) */
// 1653 			res = AppHexStrToNum( &pCmd, &attrValue, attrLength, R_FALSE );
// 1654 			if ( res != 0 )
// 1655 			{
// 1656 				status = R_STATUS_INVALID_COMMAND;
// 1657 			}
// 1658 		}
// 1659 	}
// 1660 	else
// 1661 	{
// 1662 		status = R_STATUS_INVALID_COMMAND;
// 1663 	}
// 1664 
// 1665 	/* command execution */
// 1666 	if ( status == R_STATUS_SUCCESS )
// 1667 	{
// 1668 		/* Set Attribute Value */
// 1669 		status = RfdrvIf_SetAttr( attrId, attrLength, (uint8_t *)&attrValue );
// 1670 	}
// 1671 
// 1672 	/* "SIBC" command transmission */
// 1673 	PRINTF( "SIBC " );
// 1674 	PRINTF( "%02X ", handle );
// 1675 	PRINTF( "%02X ", status );
// 1676 	PRINTF( "%02X", gRfState );
// 1677 	if ( status == R_STATUS_SUCCESS )
// 1678 	{
// 1679 		PRINTF( " " );
// 1680 		PRINTF( "%02X", attrId );
// 1681 	}
// 1682 	PRINTF( "\n" );
// 1683 }
// 1684 
// 1685 /***********************************************************************
// 1686  * function name  : R_SSCMD_ProcessCmd_GIBR
// 1687  * description	  : "Get IB Value Request(GIBR)" Command Processing
// 1688  * parameters	  : pCmd...pointer to the uart data
// 1689  *				  :   GIBR (handle) (attrId)
// 1690  * return value   : none
// 1691  * output		  : Confirm command
// 1692  *				  :   GIBC (handle) (status) (rfstate)
// 1693  *				  : 	    (attrId) (attrLength) (attrValue)
// 1694  **********************************************************************/
// 1695 static void R_SSCMD_ProcessCmd_GIBR( uint8_t *pCmd )
// 1696 {
// 1697 	uint8_t status = R_STATUS_SUCCESS;
// 1698 	uint8_t res;
// 1699 	uint8_t handle;
// 1700 	uint8_t attrId;
// 1701 	uint8_t attrLength = 0;
// 1702 	r_rfdrvIf_attr_t attrValue;
// 1703 	uint8_t *pOffset;
// 1704 	uint16_t i;
// 1705 
// 1706 	/* get command parameters */
// 1707 	/* (handle) */
// 1708 	res = AppHexStrToNum( &pCmd, &handle, sizeof(handle), R_FALSE );
// 1709 	/* (attrId) */
// 1710 	res |= AppHexStrToNum( &pCmd, &attrId, sizeof(attrId), R_FALSE );
// 1711 	if ( res != 0 )
// 1712 	{
// 1713 		status = R_STATUS_INVALID_COMMAND;
// 1714 	}
// 1715 
// 1716 	/* command execution */
// 1717 	if ( status == R_STATUS_SUCCESS )
// 1718 	{
// 1719 		/* Get Attribute Value */
// 1720 		status = RfdrvIf_GetAttr( attrId, sizeof(attrValue), &attrLength, (uint8_t *)&attrValue );
// 1721 	}
// 1722 
// 1723 	/* "GIBC" command transmission */
// 1724 	PRINTF( "GIBC " );
// 1725 	PRINTF( "%02X ", handle );
// 1726 	PRINTF( "%02X ", status );
// 1727 	PRINTF( "%02X", gRfState );
// 1728 	if ( status == R_STATUS_SUCCESS )
// 1729 	{
// 1730 		PRINTF( " " );
// 1731 		PRINTF( "%02X ", attrId );
// 1732 		PRINTF( "%02X ", attrLength );
// 1733 		pOffset = &attrValue.phyCurrentChannel + attrLength - 1;
// 1734 		for ( i=0; i<attrLength; i++ )
// 1735 		{
// 1736 			PRINTF( "%02X", *pOffset-- );
// 1737 		}
// 1738 	}
// 1739 	PRINTF( "\n" );
// 1740 }
// 1741 
// 1742 /***********************************************************************
// 1743  * function name  : R_SSCMD_ProcessCmd_CERRC
// 1744  * description    : Process "CEERC" command
// 1745  * parameters     : status...command status
// 1746  * return value   : none
// 1747  * output         : Confirm command
// 1748  *                :   CEERC (handle) (status) (rfstate)
// 1749  **********************************************************************/
// 1750 static void R_SSCMD_ProcessCmd_CERRC( uint8_t status )
// 1751 {
// 1752 	uint8_t handle = 0;
// 1753 
// 1754 	/* "CERRC" command transmission */
// 1755 	PRINTF( "CERRC " );
// 1756 	PRINTF( "%02X ", handle );
// 1757 	PRINTF( "%02X ", status );
// 1758 	PRINTF( "%02X\n", gRfState );
// 1759 }
// 1760 
// 1761 /*******************************************************************************
// 1762  * "Rfdriver API wrapping function"
// 1763  ******************************************************************************/
// 1764 /***********************************************************************
// 1765  * function name  : RfdrvIf_Start
// 1766  * description    : RF driver initialization
// 1767  * parameters     : none
// 1768  * return value   : retStatus
// 1769  **********************************************************************/
// 1770 uint8_t RfdrvIf_Start( void )
// 1771 {
// 1772 	uint8_t retStatus;
// 1773 	int16_t phyStatus;
// 1774 	uint8_t phySfdDetectionExtend;
// 1775 
// 1776 	/* rfdriver API */
// 1777 	phyStatus = RpInit( RfdrvIf_PdDataIndCallback, RfdrvIf_PdDataCfmCallback,
// 1778 						RfdrvIf_PlmeCcaCfmCallback, RfdrvIf_PlmeEdCfmCallback,
// 1779 						RfdrvIf_RxOffIndCallback, RfdrvIf_FatalErrorIndCallback,
// 1780 						RfdrvIf_WarningIndCallback, RpCalcLqiCallback, RP_NULL );
// 1781 
// 1782 	/* convert phyStatus */
// 1783 	switch ( phyStatus )
// 1784 	{
// 1785 		case RP_SUCCESS:
// 1786 			retStatus = R_STATUS_SUCCESS;
// 1787 			break;
// 1788 		case RP_INVALID_PARAMETER:
// 1789 			retStatus = R_STATUS_INVALID_PARAMETER;
// 1790 			break;
// 1791 		default:
// 1792 			retStatus = R_STATUS_ERROR;
// 1793 			break;
// 1794 	}
// 1795 
// 1796 	/* The pattern length of SFD is setting in the 2FSK/2GFSK reception. */
// 1797 	if ( retStatus == R_STATUS_SUCCESS )
// 1798 	{
// 1799 		phySfdDetectionExtend = 0x01;	// 0x01:Received using SFD of 4 bytes.
// 1800 		RpSetPibReq(RP_PHY_SFD_DETECTION_EXTEND, sizeof(phySfdDetectionExtend), &phySfdDetectionExtend);
// 1801 	}
// 1802 
// 1803 	/* init message buffer */
// 1804 	RfdrvIf_InitMsgBuff();
// 1805 
// 1806 	gNtfHandle = 0;
// 1807 
// 1808 	/* init state */
// 1809 	gRfState = R_RFSTATE_IDLE;
// 1810 
// 1811 	return( retStatus );
// 1812 }
// 1813 
// 1814 /***********************************************************************
// 1815  * function name  : RfdrvIf_Reset
// 1816  * description    : RF driver reset
// 1817  * parameters     : none
// 1818  * return value   : none
// 1819  **********************************************************************/
// 1820 static void RfdrvIf_Reset( void )
// 1821 {
// 1822 	uint8_t phySfdDetectionExtend;
// 1823 
// 1824 	/* rfdriver API */
// 1825 	RpResetReq();
// 1826 
// 1827 	/* The pattern length of SFD is setting in the 2FSK/2GFSK reception. */
// 1828 	phySfdDetectionExtend = 0x01;	// 0x01:Received using SFD of 4 bytes.
// 1829 	RpSetPibReq(RP_PHY_SFD_DETECTION_EXTEND, sizeof(phySfdDetectionExtend), &phySfdDetectionExtend);
// 1830 
// 1831 	/* init message buffer */
// 1832 	RfdrvIf_InitMsgBuff();
// 1833 
// 1834 	gNtfHandle = 0;
// 1835 
// 1836 	/* change current state */
// 1837 	gRfState = R_RFSTATE_IDLE;
// 1838 }
// 1839 
// 1840 /***********************************************************************
// 1841  * function name  : RfdrvIf_SetRxOff
// 1842  * description    : Idle setting request
// 1843  * parameters     : none
// 1844  * return value   : retStatus
// 1845  **********************************************************************/
// 1846 static uint8_t RfdrvIf_SetRxOff( void )
// 1847 {
// 1848 	uint8_t retStatus;
// 1849 	int16_t phyStatus;
// 1850 
// 1851 	/* check current state */
// 1852 	if ( gRfState == R_RFSTATE_CONTINUOUS )
// 1853 	{
// 1854 		retStatus = R_STATUS_ILLEGAL_REQUEST;
// 1855 		return( retStatus );
// 1856 	}
// 1857 
// 1858 	/* rfdriver API */
// 1859 	phyStatus = RpSetRxOffReq();
// 1860 
// 1861 	/* convert phyStatus */
// 1862 	switch ( phyStatus )
// 1863 	{
// 1864 		case RP_SUCCESS:
// 1865 			retStatus = R_STATUS_SUCCESS;
// 1866 			break;
// 1867 		case RP_BUSY_LOWPOWER:
// 1868 			retStatus = R_STATUS_BUSY_LOWPOWER;
// 1869 			break;
// 1870 		default:
// 1871 			retStatus = R_STATUS_ERROR;
// 1872 			break;
// 1873 	}
// 1874 
// 1875 	/* change current state */
// 1876 	if ( retStatus == R_STATUS_SUCCESS )
// 1877 	{
// 1878 		gRfState = R_RFSTATE_IDLE;
// 1879 	}
// 1880 
// 1881 	return( retStatus );
// 1882 }
// 1883 
// 1884 /***********************************************************************
// 1885  * function name  : RfdrvIf_SetRxOn
// 1886  * description    : Reception on setting request
// 1887  * parameters     : none
// 1888  * return value   : retStatus
// 1889  **********************************************************************/
// 1890 static uint8_t RfdrvIf_SetRxOn( void )
// 1891 {
// 1892 	uint8_t retStatus;
// 1893 	int16_t phyStatus;
// 1894 
// 1895 	/* check current state */
// 1896 	if ( gRfState == R_RFSTATE_CONTINUOUS )
// 1897 	{
// 1898 		retStatus = R_STATUS_ILLEGAL_REQUEST;
// 1899 		return( retStatus );
// 1900 	}
// 1901 
// 1902 	/* rfdriver API */
// 1903 	RpSetRxOffReq();
// 1904 
// 1905 	/* rfdriver API */
// 1906 	phyStatus = RpSetRxOnReq( (uint8_t)RP_RX_AUTO_RCV, (uint32_t)0 );
// 1907 
// 1908 	/* convert phyStatus */
// 1909 	switch ( phyStatus )
// 1910 	{
// 1911 		case RP_SUCCESS:
// 1912 		case RP_RX_ON:
// 1913 			retStatus = R_STATUS_SUCCESS;
// 1914 			break;
// 1915 		case RP_INVALID_PARAMETER:
// 1916 			retStatus = R_STATUS_INVALID_PARAMETER;
// 1917 			break;
// 1918 		case RP_BUSY_RX:
// 1919 			retStatus = R_STATUS_BUSY_RX;
// 1920 			break;
// 1921 		case RP_BUSY_TX:
// 1922 			retStatus = R_STATUS_BUSY_TX;
// 1923 			break;
// 1924 		case RP_BUSY_LOWPOWER:
// 1925 			retStatus = R_STATUS_BUSY_LOWPOWER;
// 1926 			break;
// 1927 		case RP_TRX_OFF:
// 1928 			retStatus = R_STATUS_TRX_OFF;
// 1929 			break;
// 1930 		default:
// 1931 			retStatus = R_STATUS_ERROR;
// 1932 			break;
// 1933 	}
// 1934 
// 1935 	/* change current state */
// 1936 	if ( retStatus == R_STATUS_SUCCESS )
// 1937 	{
// 1938 		gRfState = R_RFSTATE_RXAUTO;
// 1939 	}
// 1940 
// 1941 	return( retStatus );
// 1942 }
// 1943 
// 1944 /***********************************************************************
// 1945  * function name  : RfdrvIf_SendFrame
// 1946  * description    : Frame transmission request
// 1947  * parameters     : none
// 1948  * return value   : retStatus
// 1949  **********************************************************************/
// 1950 static uint8_t RfdrvIf_SendFrame( void )
// 1951 {
// 1952 	uint8_t retStatus;
// 1953 	int16_t phyStatus;
// 1954 	uint16_t psduLength = (uint16_t)R_TEST_DATA_LENGTH;
// 1955 	uint8_t psdu[R_TEST_DATA_LENGTH];
// 1956 	uint16_t i;
// 1957 	r_rfdrvIf_msg_pddatacfm_t *pCfm = &gCbMsgPdDataCfm;
// 1958 
// 1959 	/* check current state */
// 1960 	if ( gRfState == R_RFSTATE_CONTINUOUS )
// 1961 	{
// 1962 		retStatus = R_STATUS_ILLEGAL_REQUEST;
// 1963 		return( retStatus );
// 1964 	}
// 1965 
// 1966 	/* rfdriver API */
// 1967 	RpSetRxOffReq();
// 1968 
// 1969 	/* clear event flag */
// 1970 	pCfm->eventFlag = R_FALSE;
// 1971 
// 1972 	/* set test data */
// 1973 	for( i=0; i<psduLength; i++ )
// 1974 	{
// 1975 		psdu[i] = i;
// 1976 	}
// 1977 
// 1978 	/* rfdriver API */
// 1979 	phyStatus = RpPdDataReq( &(psdu[0]), psduLength, (uint8_t)0, (uint32_t)0 );
// 1980 
// 1981 	/* convert phyStatus */
// 1982 	switch ( phyStatus )
// 1983 	{
// 1984 		case RP_PENDING:
// 1985 			retStatus = R_STATUS_SUCCESS;
// 1986 			break;
// 1987 		case RP_INVALID_PARAMETER:
// 1988 			retStatus = R_STATUS_INVALID_PARAMETER;
// 1989 			break;
// 1990 		case RP_BUSY_RX:
// 1991 			retStatus = R_STATUS_BUSY_RX;
// 1992 			break;
// 1993 		case RP_BUSY_TX:
// 1994 			retStatus = R_STATUS_BUSY_TX;
// 1995 			break;
// 1996 		case RP_BUSY_LOWPOWER:
// 1997 			retStatus = R_STATUS_BUSY_LOWPOWER;
// 1998 			break;
// 1999 		case RP_INVALID_API_OPTION:
// 2000 			retStatus = R_STATUS_INVALID_API_OPTION;
// 2001 			break;
// 2002 		default:
// 2003 			retStatus = R_STATUS_ERROR;
// 2004 			break;
// 2005 	}
// 2006 
// 2007 	if ( retStatus == R_STATUS_SUCCESS )
// 2008 	{
// 2009 		/* wait for processing completion */
// 2010 		do {
// 2011 		} while ( pCfm->eventFlag == R_FALSE );
// 2012 
// 2013 		/* frame transmission complete status */
// 2014 		retStatus = pCfm->status;
// 2015 	}
// 2016 
// 2017 	/* return to reception on */
// 2018 	if ( gRfState == R_RFSTATE_RXAUTO )
// 2019 	{
// 2020 		/* rfdriver API */
// 2021 		RpSetRxOnReq( (uint8_t)RP_RX_AUTO_RCV, (uint32_t)0 );
// 2022 	}
// 2023 
// 2024 	return( retStatus );
// 2025 }
// 2026 
// 2027 /***********************************************************************
// 2028  * function name  : RfdrvIf_SetAttr
// 2029  * description    : Attribute value setting function
// 2030  * parameters     : attrId...Attribute ID
// 2031  *                : attrLength...Size of the attribute value to be set
// 2032  *                : p_attrValue...Pointer to the beginning of the area 
// 2033  *                :               storing the attribute value to be set
// 2034  * return value   : retStatus
// 2035  **********************************************************************/
// 2036 static uint8_t RfdrvIf_SetAttr( uint8_t attrId, uint8_t attrLength,
// 2037 								uint8_t RP_FAR *p_attrValue )
// 2038 {
// 2039 	uint8_t retStatus;
// 2040 	int16_t phyStatus;
// 2041 
// 2042 	/* check current state */
// 2043 	if ( gRfState == R_RFSTATE_CONTINUOUS )
// 2044 	{
// 2045 		retStatus = R_STATUS_ILLEGAL_REQUEST;
// 2046 		return( retStatus );
// 2047 	}
// 2048 
// 2049 	/* null check */
// 2050 	if ( p_attrValue == NULL )
// 2051 	{
// 2052 		retStatus = R_STATUS_INVALID_PARAMETER;
// 2053 		return( retStatus );
// 2054 	}
// 2055 
// 2056 	/* rfdriver API */
// 2057 	RpSetRxOffReq();
// 2058 
// 2059 	/* rfdriver API */
// 2060 	phyStatus = RpSetPibReq( attrId, attrLength, p_attrValue );
// 2061 
// 2062 	/* convert phyStatus */
// 2063 	switch ( phyStatus )
// 2064 	{
// 2065 		case RP_SUCCESS:
// 2066 			retStatus = R_STATUS_SUCCESS;
// 2067 			break;
// 2068 		case RP_UNSUPPORTED_ATTRIBUTE:
// 2069 			retStatus = R_STATUS_UNSUPPORTED_ATTRIBUTE;
// 2070 			break;
// 2071 		case RP_INVALID_PARAMETER:
// 2072 			retStatus = R_STATUS_INVALID_PARAMETER;
// 2073 			break;
// 2074 		case RP_BUSY_RX:
// 2075 			retStatus = R_STATUS_BUSY_RX;
// 2076 			break;
// 2077 		case RP_BUSY_TX:
// 2078 			retStatus = R_STATUS_BUSY_TX;
// 2079 			break;
// 2080 		case RP_BUSY_LOWPOWER:
// 2081 			retStatus = R_STATUS_BUSY_LOWPOWER;
// 2082 			break;
// 2083 		default:
// 2084 			retStatus = R_STATUS_ERROR;
// 2085 			break;
// 2086 	}
// 2087 
// 2088 	/* return to reception on */
// 2089 	if ( gRfState == R_RFSTATE_RXAUTO )
// 2090 	{
// 2091 		/* rfdriver API */
// 2092 		RpSetRxOnReq( (uint8_t)RP_RX_AUTO_RCV, (uint32_t)0 );
// 2093 	}
// 2094 
// 2095 	return( retStatus );
// 2096 }
// 2097 
// 2098 /***********************************************************************
// 2099  * function name  : RfdrvIf_GetAttr
// 2100  * description    : Attribute value getting function
// 2101  * parameters     : attrId...Attribute ID
// 2102  *                : attrValueLength...Attribute value Length
// 2103  *                : p_attrLength...Attribute value Length(out)
// 2104  *                : p_attrValue...Attribute value(out)
// 2105  * return value   : retStatus
// 2106  **********************************************************************/
// 2107 static uint8_t RfdrvIf_GetAttr( uint8_t attrId, uint8_t attrValueLength,
// 2108 						uint8_t *p_attrLength, uint8_t *p_attrValue )
// 2109 {
// 2110 	uint8_t retStatus;
// 2111 	int16_t phyStatus;
// 2112 
// 2113 	/* check current state */
// 2114 	if ( gRfState == R_RFSTATE_CONTINUOUS )
// 2115 	{
// 2116 		retStatus = R_STATUS_ILLEGAL_REQUEST;
// 2117 		return( retStatus );
// 2118 	}
// 2119 
// 2120 	/* null check */
// 2121 	if (( p_attrLength == NULL )
// 2122 		||( p_attrValue == NULL ))
// 2123 	{
// 2124 		retStatus = R_STATUS_INVALID_PARAMETER;
// 2125 		return( retStatus );
// 2126 	}
// 2127 
// 2128 	/* get attribute value length */
// 2129 	*p_attrLength = RfdrvIf_GetAttrLength( attrId );
// 2130 
// 2131 	/* length check of the area storing attribute value */
// 2132 	if ( attrValueLength < *p_attrLength )
// 2133 	{
// 2134 		retStatus = R_STATUS_INVALID_PARAMETER;
// 2135 		return( retStatus );
// 2136 	}
// 2137 
// 2138 	/* rfdriver API */
// 2139 	RpSetRxOffReq();
// 2140 
// 2141 	/* rfdriver API */
// 2142 	phyStatus = RpGetPibReq( attrId, *p_attrLength, p_attrValue );
// 2143 
// 2144 	/* convert phyStatus */
// 2145 	switch ( phyStatus )
// 2146 	{
// 2147 		case RP_SUCCESS:
// 2148 			retStatus = R_STATUS_SUCCESS;
// 2149 			break;
// 2150 		case RP_UNSUPPORTED_ATTRIBUTE:
// 2151 			retStatus = R_STATUS_UNSUPPORTED_ATTRIBUTE;
// 2152 			break;
// 2153 		case RP_INVALID_PARAMETER:
// 2154 			retStatus = R_STATUS_INVALID_PARAMETER;
// 2155 			break;
// 2156 		case RP_BUSY_RX:
// 2157 			retStatus = R_STATUS_BUSY_RX;
// 2158 			break;
// 2159 		case RP_BUSY_TX:
// 2160 			retStatus = R_STATUS_BUSY_TX;
// 2161 			break;
// 2162 		case RP_BUSY_LOWPOWER:
// 2163 			retStatus = R_STATUS_BUSY_LOWPOWER;
// 2164 			break;
// 2165 		default:
// 2166 			retStatus = R_STATUS_ERROR;
// 2167 			break;
// 2168 	}
// 2169 
// 2170 	/* clear attribute value length */
// 2171 	if ( retStatus != R_STATUS_SUCCESS )
// 2172 	{
// 2173 		*p_attrLength = 0;
// 2174 	}
// 2175 
// 2176 	/* return to reception on */
// 2177 	if ( gRfState == R_RFSTATE_RXAUTO )
// 2178 	{
// 2179 		/* rfdriver API */
// 2180 		RpSetRxOnReq( (uint8_t)RP_RX_AUTO_RCV, (uint32_t)0 );
// 2181 	}
// 2182 
// 2183 	return( retStatus );
// 2184 }
// 2185 
// 2186 /***********************************************************************
// 2187  * function name  : RfdrvIf_GetAttrLength
// 2188  * description    : get attribute length
// 2189  * parameters     : attrId...attribute ID
// 2190  * return value   : attribute length
// 2191  **********************************************************************/
// 2192 static uint8_t RfdrvIf_GetAttrLength( uint8_t attrId )
// 2193 {
// 2194 	uint8_t attrLength;
// 2195 
// 2196 	switch ( attrId )
// 2197 	{
// 2198 		case RP_PHY_RMODE_TCUM_LIMIT:
// 2199 		case RP_PHY_RMODE_TCUM:
// 2200 		case RP_PHY_FREQUENCY:
// 2201 		case RP_PHY_FREQUENCY_OFFSET:
// 2202 			attrLength = sizeof(uint32_t);
// 2203 			break;
// 2204 		case RP_PHY_CCA_VTH:
// 2205 		case RP_PHY_LVLFLTR_VTH:
// 2206 		case RP_MAC_SHORT_ADDRESS1:
// 2207 		case RP_MAC_PANID1:
// 2208 		case RP_MAC_SHORT_ADDRESS2:
// 2209 		case RP_MAC_PANID2:
// 2210 		case RP_PHY_CSMA_BACKOFF_PERIOD:
// 2211 		case RP_PHY_CCA_DURATION:
// 2212 		case RP_PHY_FSK_PREAMBLE_LENGTH:
// 2213 		case RP_PHY_ACK_REPLY_TIME:
// 2214 		case RP_PHY_ACK_WAIT_DURATION:
// 2215 		case RP_PHY_DATA_RATE:
// 2216 		case RP_PHY_AGC_START_VTH:
// 2217 		case RP_PHY_ANTENNA_DIVERSITY_START_VTH:
// 2218 		case RP_PHY_ANTENNA_SWITCHING_TIME:
// 2219 		case RP_PHY_ANTENNA_SWITCH_ENA_TIMING:
// 2220 		case RP_PHY_RMODE_TON_MAX:
// 2221 		case RP_PHY_RMODE_TOFF_MIN:
// 2222 		case RP_PHY_RMODE_TCUM_SMP_PERIOD:
// 2223 			attrLength = sizeof(uint16_t);
// 2224 			break;
// 2225 		case RP_PHY_CHANNELS_SUPPORTED:
// 2226 		case RP_MAC_EXTENDED_ADDRESS1:
// 2227 		case RP_MAC_EXTENDED_ADDRESS2:
// 2228 			attrLength = 8;
// 2229 			break;
// 2230 		default:
// 2231 			attrLength = sizeof(uint8_t);
// 2232 			break;
// 2233 	}
// 2234 
// 2235 	return( attrLength );
// 2236 }
// 2237 
// 2238 /***********************************************************************
// 2239  * function name  : RfdrvIf_SetAttrChannel
// 2240  * description    : 
// 2241  * parameters     : freqBandId...
// 2242  *				  : fskOpeMode...
// 2243  *				  : setChannel...
// 2244  * return value   : retStatus
// 2245  **********************************************************************/
// 2246 static uint8_t RfdrvIf_SetAttrChannel( uint8_t freqBandId, uint8_t fskOpeMode,
// 2247 										uint8_t setChannel, int32_t frequencyOffset )
// 2248 {
// 2249 	uint8_t retStatus = R_STATUS_SUCCESS;
// 2250 	uint8_t indexFreqBandId, indexFskOpeMode;
// 2251 	uint8_t maxFskOpeMode, maxChannel;
// 2252 	uint8_t maskChSupported;
// 2253 
// 2254 	/* range check phyFreqBandId */
// 2255 	if (( RP_PHY_FREQ_BAND_863MHz <= freqBandId )
// 2256 		&&( freqBandId <= RP_PHY_FREQ_BAND_920MHz ))
// 2257 	{
// 2258 		indexFreqBandId = freqBandId - 4;
// 2259 	}
// 2260 	else if (( RP_PHY_FREQ_BAND_920MHz_Others <= freqBandId )
// 2261 		&&( freqBandId <= RP_PHY_FREQ_BAND_921MHz ))
// 2262 	{
// 2263 		indexFreqBandId = freqBandId - 8;
// 2264 	}
// 2265 	else
// 2266 	{
// 2267 		#ifdef R_RFDRVIF_LOG_REASON
// 2268 		PRINTF( "RangeError FreqBandId\n" );
// 2269 		#endif // #ifdef R_RFDRVIF_LOG_REASON
// 2270 		retStatus = R_STATUS_INVALID_PARAMETER;
// 2271 		return( retStatus );
// 2272 	}
// 2273 
// 2274 	/* set Attribute phyFreqBandId Value */
// 2275 	retStatus = RfdrvIf_SetAttr( RP_PHY_FREQ_BAND_ID, sizeof(freqBandId), (uint8_t *)&freqBandId );
// 2276 	if ( retStatus != R_STATUS_SUCCESS )
// 2277 	{
// 2278 		#ifdef R_RFDRVIF_LOG_REASON
// 2279 		PRINTF( "SetAttrError FreqBandId\n" );
// 2280 		#endif // #ifdef R_RFDRVIF_LOG_REASON
// 2281 		return( retStatus );
// 2282 	}
// 2283 
// 2284 	/* range check phyFskOpeMode */
// 2285 	maxFskOpeMode = g_MaxFskOpeModeTbl[indexFreqBandId];
// 2286 	if (( fskOpeMode == 0 )
// 2287 		||( maxFskOpeMode < fskOpeMode ))
// 2288 	{
// 2289 		#ifdef R_RFDRVIF_LOG_REASON
// 2290 		PRINTF( "RangeError FskOpeMode\n" );
// 2291 		#endif // #ifdef R_RFDRVIF_LOG_REASON
// 2292 		retStatus = R_STATUS_INVALID_PARAMETER;
// 2293 		return( retStatus );
// 2294 	}
// 2295 
// 2296 	/* Set Attribute phyFskOpeMode Value */
// 2297 	retStatus = RfdrvIf_SetAttr( RP_PHY_FSK_OPE_MODE, sizeof(fskOpeMode), (uint8_t *)&fskOpeMode );
// 2298 	if ( retStatus != R_STATUS_SUCCESS )
// 2299 	{
// 2300 		#ifdef R_RFDRVIF_LOG_REASON
// 2301 		PRINTF( "SetAttrError FskOpeMode\n" );
// 2302 		#endif // #ifdef R_RFDRVIF_LOG_REASON
// 2303 		return( retStatus );
// 2304 	}
// 2305 
// 2306 	/* range check phyCurrentChannel */
// 2307 	indexFskOpeMode = fskOpeMode - 1;
// 2308 	maxChannel = g_MaxChannelTbl[indexFreqBandId].maxChannel[indexFskOpeMode];
// 2309 	if ( maxChannel < setChannel )
// 2310 	{
// 2311 		#ifdef R_RFDRVIF_LOG_REASON
// 2312 		PRINTF( "RangeError maxChannel\n" );
// 2313 		#endif // #ifdef R_RFDRVIF_LOG_REASON
// 2314 		retStatus = R_STATUS_INVALID_PARAMETER;
// 2315 		return( retStatus );
// 2316 	}
// 2317 
// 2318 	/* check support phyCurrentChannel */
// 2319 	maskChSupported = R_FALSE;
// 2320 	if (( freqBandId == RP_PHY_FREQ_BAND_920MHz_Others )
// 2321 		&&( fskOpeMode == 1 ))
// 2322 	{
// 2323 		maskChSupported = R_TRUE;
// 2324 	}
// 2325 	retStatus = RfdrvIf_CheckChannelsSupported( setChannel, maskChSupported );
// 2326 	if ( retStatus != R_STATUS_SUCCESS )
// 2327 	{
// 2328 		#ifdef R_RFDRVIF_LOG_REASON
// 2329 		PRINTF( "Non-Support Channel\n" );
// 2330 		#endif // #ifdef R_RFDRVIF_LOG_REASON
// 2331 		return( retStatus );
// 2332 	}
// 2333 
// 2334 	/* set Attribute phyFrequencyOffset Value */
// 2335 	retStatus = RfdrvIf_SetAttr( RP_PHY_FREQUENCY_OFFSET, sizeof(frequencyOffset), (uint8_t *)&frequencyOffset );
// 2336 	if ( retStatus != R_STATUS_SUCCESS )
// 2337 	{
// 2338 		return( retStatus );
// 2339 	}
// 2340 
// 2341 	/* set Attribute phyCurrentChannel Value */
// 2342 	retStatus = RfdrvIf_SetAttr( RP_PHY_CURRENT_CHANNEL, sizeof(setChannel), (uint8_t *)&setChannel );
// 2343 	if ( retStatus != R_STATUS_SUCCESS )
// 2344 	{
// 2345 		#ifdef R_RFDRVIF_LOG_REASON
// 2346 		PRINTF( "SetAttrError Channel\n" );
// 2347 		#endif // #ifdef R_RFDRVIF_LOG_REASON
// 2348 		return( retStatus );
// 2349 	}
// 2350 
// 2351 	return( retStatus );
// 2352 }
// 2353 
// 2354 /***********************************************************************
// 2355  * function name  : RfdrvIf_CheckChannelsSupported
// 2356  * description    : 
// 2357  * parameters     : channel..
// 2358  * return value   : retStatus
// 2359  **********************************************************************/
// 2360 static uint8_t RfdrvIf_CheckChannelsSupported( uint8_t channel, uint8_t maskChSupported )
// 2361 {
// 2362 	uint8_t retStatus;
// 2363 	uint8_t attrLength, channel8bit;
// 2364 	uint32_t channelsSupported[2];
// 2365 	uint32_t channel32bit;
// 2366 	uint8_t channelsSupportedPage;
// 2367 
// 2368 	/* set Attribute phyChannelsSupportedPage */
// 2369 	channelsSupportedPage = RfdrvIf_ConvertChannelsSupportedPage( channel );
// 2370 	retStatus = RfdrvIf_SetAttr( RP_PHY_CHANNELS_SUPPORTED_PAGE, sizeof(channelsSupportedPage), (uint8_t *)&channelsSupportedPage );
// 2371 	if ( retStatus != R_STATUS_SUCCESS )
// 2372 	{
// 2373 		return( retStatus );
// 2374 	}
// 2375 
// 2376 	/* get Attribute phyChannelsSupported */
// 2377 	retStatus = RfdrvIf_GetAttr( RP_PHY_CHANNELS_SUPPORTED, 8, &attrLength, (uint8_t *)&(channelsSupported[0]) );
// 2378 	if (( maskChSupported )&&( channelsSupportedPage == 0 ))
// 2379 	{
// 2380 		channelsSupported[0] |= 0x00000100;
// 2381 	}
// 2382 	if ( retStatus != R_STATUS_SUCCESS )
// 2383 	{
// 2384 		return( retStatus );
// 2385 	}
// 2386 
// 2387 	channel8bit = channel - (channelsSupportedPage * 64);
// 2388 
// 2389 	/* ChannelNumber bit 0-31 */
// 2390 	if ( channel8bit < 32 )
// 2391 	{
// 2392 		channel32bit = (uint32_t)1 << channel8bit;
// 2393 		channelsSupported[0] &= channel32bit;
// 2394 		if ( channelsSupported[0] == 0 )
// 2395 		{
// 2396 			retStatus = R_STATUS_INVALID_PARAMETER;
// 2397 		}
// 2398 	}
// 2399 	/* ChannelNumber bit 32-63 */
// 2400 	else
// 2401 	{
// 2402 		channel8bit -= 32;
// 2403 		channel32bit = (uint32_t)1 << channel8bit;
// 2404 		channelsSupported[1] &= channel32bit;
// 2405 		if ( channelsSupported[1] == 0 )
// 2406 		{
// 2407 			retStatus = R_STATUS_INVALID_PARAMETER;
// 2408 		}
// 2409 	}
// 2410 
// 2411 	return( retStatus );
// 2412 }
// 2413 
// 2414 /***********************************************************************
// 2415  * function name  : RfdrvIf_ConvertChannelsSupportedPage
// 2416  * description    : 
// 2417  * parameters     : channel..
// 2418  * return value   : retStatus
// 2419  **********************************************************************/
// 2420 static uint8_t RfdrvIf_ConvertChannelsSupportedPage( uint8_t channel )
// 2421 {
// 2422 	uint8_t channelsSupportedPage;
// 2423 
// 2424 	/* channel 0-63 */
// 2425 	if ( channel < 64 )
// 2426 	{
// 2427 		channelsSupportedPage = 0x00;
// 2428 	}
// 2429 	else
// 2430 	{
// 2431 		/* channel 64-127 */
// 2432 		if ( channel < 128 )
// 2433 		{
// 2434 			channelsSupportedPage = 0x01;
// 2435 		}
// 2436 		else
// 2437 		{
// 2438 			/* channel 128-191 */
// 2439 			if ( channel < 192 )
// 2440 			{
// 2441 				channelsSupportedPage = 0x02;
// 2442 			}
// 2443 			/* channel 192-255 */
// 2444 			else
// 2445 			{
// 2446 				channelsSupportedPage = 0x03;
// 2447 			}
// 2448 		}
// 2449 	}
// 2450 
// 2451 	return( channelsSupportedPage );
// 2452 }
// 2453 
// 2454 /***********************************************************************
// 2455  * function name  : RfdrvIf_SetAttrTxPower
// 2456  * description    : 
// 2457  * parameters     : freqBandId...
// 2458  *				  : txPower...
// 2459  * return value   : retStatus
// 2460  **********************************************************************/
// 2461 static uint8_t RfdrvIf_SetAttrTxPower( uint8_t freqBandId, uint8_t transmitPower )
// 2462 {
// 2463 	uint8_t retStatus = R_STATUS_SUCCESS;
// 2464 	uint8_t maxGainSet;
// 2465 
// 2466 	/* range check phyFreqBandId */
// 2467 	switch ( freqBandId )
// 2468 	{
// 2469 		case RP_PHY_FREQ_BAND_920MHz:			// (9) 920 - 928 (Japan)
// 2470 		case RP_PHY_FREQ_BAND_920MHz_Others:	// (14)	920 - 928 (Japan) other settings
// 2471 		case RP_PHY_FREQ_BAND_921MHz:			// (17)	921 - xxx (CH)
// 2472 			maxGainSet = 101;
// 2473 			break;
// 2474 		case RP_PHY_FREQ_BAND_863MHz:			// (4) 863 - 870 (Europe)
// 2475 		case RP_PHY_FREQ_BAND_870MHz:			// (15)	870 - xxx (Europe2)
// 2476 			maxGainSet = 102;
// 2477 			break;
// 2478 		case RP_PHY_FREQ_BAND_917MHz:			// (8) 917 - 923.5 (Korea)
// 2479 			maxGainSet = 102;
// 2480 			break;
// 2481 		case RP_PHY_FREQ_BAND_896MHz:			// (5) 896 - 901 (US FCC Part 90)
// 2482 		case RP_PHY_FREQ_BAND_901MHz:			// (6) 901 - 902 (US FCC Part 24)
// 2483 		case RP_PHY_FREQ_BAND_915MHz:			// (7) 902 - 928 (US)
// 2484 		case RP_PHY_FREQ_BAND_902MHz:			// (16)	902 - xxx (PH/MY/AZ)
// 2485 			maxGainSet = 104;
// 2486 			break;
// 2487 		default:
// 2488 			retStatus = R_STATUS_INVALID_PARAMETER;
// 2489 			break;
// 2490 	}
// 2491 
// 2492 	/* range check phyTransmitPower */
// 2493 	if ( retStatus == R_STATUS_SUCCESS )
// 2494 	{
// 2495 		if ( maxGainSet < transmitPower )
// 2496 		{
// 2497 			retStatus = R_STATUS_INVALID_PARAMETER;
// 2498 		}
// 2499 	}
// 2500 
// 2501 	/* set Attribute phyFreqBandId Value */
// 2502 	if ( retStatus == R_STATUS_SUCCESS )
// 2503 	{
// 2504 		retStatus = RfdrvIf_SetAttr( RP_PHY_FREQ_BAND_ID, sizeof(freqBandId), (uint8_t *)&freqBandId );
// 2505 	}
// 2506 
// 2507 	/* set Attribute phyTransmitPower Value */
// 2508 	if ( retStatus == R_STATUS_SUCCESS )
// 2509 	{
// 2510 		retStatus = RfdrvIf_SetAttr( RP_PHY_TRANSMIT_POWER, sizeof(transmitPower), (uint8_t *)&transmitPower );
// 2511 	}
// 2512 
// 2513 	return( retStatus );
// 2514 }
// 2515 
// 2516 /***********************************************************************
// 2517  * function name  : RfdrvIf_ContUnmoduTx
// 2518  * description    : Start continuous unmodulated Transmisson
// 2519  * parameters     : none
// 2520  * return value   : retStatus
// 2521  **********************************************************************/
// 2522 static uint8_t RfdrvIf_ContUnmoduTx( void )
// 2523 {
// 2524 	uint8_t retStatus = R_STATUS_SUCCESS;
// 2525 
// 2526 	/* check current state */
// 2527 	if ( gRfState != R_RFSTATE_IDLE )
// 2528 	{
// 2529 		retStatus = R_STATUS_ILLEGAL_REQUEST;
// 2530 		return( retStatus );
// 2531 	}
// 2532 
// 2533 	/* rfdriver API */
// 2534 	RptConTxNoModu();
// 2535 
// 2536 	/* change current state */
// 2537 	gRfState = R_RFSTATE_CONTINUOUS;
// 2538 
// 2539 	return( retStatus );
// 2540 }
// 2541 
// 2542 /***********************************************************************
// 2543  * function name  : RfdrvIf_Pn9ContModuTx
// 2544  * description    : Start PN9 continuous modulated Transmisson
// 2545  * parameters     : none
// 2546  * return value   : retStatus
// 2547  **********************************************************************/
// 2548 static uint8_t RfdrvIf_Pn9ContModuTx( void )
// 2549 {
// 2550 	uint8_t retStatus = R_STATUS_SUCCESS;
// 2551 
// 2552 	/* check current state */
// 2553 	if ( gRfState != R_RFSTATE_IDLE )
// 2554 	{
// 2555 		retStatus = R_STATUS_ILLEGAL_REQUEST;
// 2556 		return( retStatus );
// 2557 	}
// 2558 
// 2559 	/* rfdriver API */
// 2560 	RptPulseTxPrbs9();
// 2561 
// 2562 	/* change current state */
// 2563 	gRfState = R_RFSTATE_CONTINUOUS;
// 2564 
// 2565 	return( retStatus );
// 2566 }
// 2567 
// 2568 /***********************************************************************
// 2569  * function name  : RfdrvIf_ContTxStop
// 2570  * description    : Continuous Transmisson Stop
// 2571  * parameters     : none
// 2572  * return value   : retStatus
// 2573  **********************************************************************/
// 2574 static uint8_t RfdrvIf_ContTxStop( void )
// 2575 {
// 2576 	uint8_t retStatus = R_STATUS_SUCCESS;
// 2577 
// 2578 	/* check current state */
// 2579 	if ( gRfState != R_RFSTATE_CONTINUOUS )
// 2580 	{
// 2581 		retStatus = R_STATUS_ILLEGAL_REQUEST;
// 2582 		return( retStatus );
// 2583 	}
// 2584 
// 2585 	/* rfdriver API */
// 2586 	RptConTrxStop();
// 2587 
// 2588 	/* change current state */
// 2589 	gRfState = R_RFSTATE_IDLE;
// 2590 
// 2591 	return( retStatus );
// 2592 }
// 2593 
// 2594 /*******************************************************************************
// 2595  * "Rfdriver Callback function" (User-Defined Function)
// 2596  ******************************************************************************/
// 2597 /***********************************************************************
// 2598  * function name  : RfdrvIf_PdDataIndCallback
// 2599  * description    : Frame Reception Complete Callback Function
// 2600  * parameters     : pData...Pointer to the beginning of the area storing the reception data
// 2601  *                : dataLength...Reception data size
// 2602  *                : time...SFD reception time
// 2603  *                : linkQuality...LQI value
// 2604  *                : rssi...RSSI value
// 2605  *                : selectedAntenna...Selected antenna
// 2606  *                : rssi_0...RSSI value
// 2607  *                : rssi_1...RSSI value
// 2608  *                : status...Reception status
// 2609  *                : filteredAddr...Number of the function filtered by the address filter function.
// 2610  *                :                1(filter1), 2(filter2), 3(filter1 & 2)
// 2611  *                : phr...Received PHR information
// 2612  * return value   : none
// 2613  **********************************************************************/
// 2614 static void RfdrvIf_PdDataIndCallback( uint8_t *pData, uint16_t dataLength, uint32_t time,
// 2615 									uint8_t linkQuality, uint16_t rssi, uint8_t selectedAntenna,
// 2616 									uint16_t rssi_0, uint16_t rssi_1, uint8_t status,
// 2617 									uint8_t filteredAddr, uint8_t phr )
// 2618 {
// 2619 	uint8_t retStatus;
// 2620 	r_rfdrvIf_msg_pddataind_t *pMsg = &gCbMsgPdDataInd;
// 2621 
// 2622 	/* convert phyStatus */
// 2623 	switch ( status )
// 2624 	{
// 2625 		case RP_NO_FRMPENBIT_ACK:
// 2626 		case RP_FRMPENBIT_ACK:
// 2627 			retStatus = R_STATUS_SUCCESS;
// 2628 			break;
// 2629 		default:
// 2630 			retStatus = R_STATUS_ERROR;
// 2631 			break;
// 2632 	}
// 2633 
// 2634 	if ( retStatus == R_STATUS_SUCCESS )
// 2635 	{
// 2636 		if ( dataLength <= R_TEST_DATA_LENGTH )
// 2637 		{
// 2638 			/* store message in ring buffer */
// 2639 			memset( pMsg, 0, sizeof(r_rfdrvIf_msg_pddataind_t) );
// 2640 			pMsg->rssi = rssi;
// 2641 			pMsg->psduLength = dataLength;
// 2642 			memcpy( &(pMsg->psdu[0]), pData, pMsg->psduLength );
// 2643 			RfdrvIf_PutMsgBuff( pMsg, R_MSG_SIZE_DATAI );
// 2644 		}
// 2645 	}
// 2646 
// 2647 	/* rfdriver API "Receive Buffer Release" */
// 2648 	RpRelRxBuf( pData );
// 2649 }
// 2650 
// 2651 /***********************************************************************
// 2652  * function name  : RfdrvIf_PdDataCfmCallback
// 2653  * description    : Frame Transmission Complete Callback Function
// 2654  * parameters     : status...Frame transmission complete status
// 2655  *                : framepend...Pending bit of the reply ACK frame
// 2656  *                : numBackoffs...Number of times CCA has been executed
// 2657  * return value   : none
// 2658  **********************************************************************/
// 2659 static void RfdrvIf_PdDataCfmCallback( uint8_t status, uint8_t framePend, uint8_t numBackoffs )
// 2660 {
// 2661 	uint8_t retStatus;
// 2662 	r_rfdrvIf_msg_pddatacfm_t *pCfm = &gCbMsgPdDataCfm;
// 2663 
// 2664 	/* convert phyStatus */
// 2665 	switch ( status )
// 2666 	{
// 2667 		case RP_SUCCESS:
// 2668 			retStatus = R_STATUS_SUCCESS;
// 2669 			break;
// 2670 		case RP_NO_ACK:
// 2671 			retStatus = R_STATUS_NO_ACK;
// 2672 			break;
// 2673 		case RP_BUSY:
// 2674 			retStatus = R_STATUS_CHANNEL_BUSY;
// 2675 			break;
// 2676 		case RP_TRANSMIT_TIME_OVERFLOW:
// 2677 			retStatus = R_STATUS_TRANSMIT_TIME_OVERFLOW;
// 2678 			break;
// 2679 		case RP_TX_UNDERFLOW:
// 2680 			retStatus = R_STATUS_TX_UNDERFLOW;
// 2681 			break;
// 2682 		default:
// 2683 			retStatus = R_STATUS_ERROR;
// 2684 			break;
// 2685 	}
// 2686 
// 2687 	/* set message content */
// 2688 	pCfm->status = retStatus;
// 2689 
// 2690 	/* set event flag */
// 2691 	pCfm->eventFlag = R_TRUE;
// 2692 }
// 2693 
// 2694 /***********************************************************************
// 2695  * function name  : RfdrvIf_PlmeCcaCfmCallback
// 2696  * description    : CCA Complete Callback Function
// 2697  * parameters     : status...CCA status
// 2698  * return value   : none
// 2699  **********************************************************************/
// 2700 static void RfdrvIf_PlmeCcaCfmCallback( uint8_t status )
// 2701 {
// 2702 }
// 2703 
// 2704 /***********************************************************************
// 2705  * function name  : RfdrvIf_PlmeEdCfmCallback
// 2706  * description    : ED (Energy Detection) Complete Callback Function
// 2707  * parameters     : phyState...ED complete status
// 2708  *                : edValue...ED result
// 2709  *                : rssi...RSSI value
// 2710  * return value   : none
// 2711  **********************************************************************/
// 2712 static void RfdrvIf_PlmeEdCfmCallback( uint8_t phyState, uint8_t edValue, uint16_t rssi )
// 2713 {
// 2714 }
// 2715 
// 2716 /***********************************************************************
// 2717  * function name  : RfdrvIf_RxOffIndCallback
// 2718  * description    : Reception OFF Notification Callback Function
// 2719  * parameters     : none
// 2720  * return value   : none
// 2721  **********************************************************************/
// 2722 static void RfdrvIf_RxOffIndCallback( void )
// 2723 {
// 2724 }
// 2725 
// 2726 /***********************************************************************
// 2727  * function name  : RfdrvIf_FatalErrorIndCallback
// 2728  * description    : Error Notification Callback Function
// 2729  * parameters     : status...Fatal error reason
// 2730  * return value   : none
// 2731  **********************************************************************/
// 2732 static void RfdrvIf_FatalErrorIndCallback( uint8_t status )
// 2733 {
// 2734 }
// 2735 
// 2736 /***********************************************************************
// 2737  * function name  : RfdrvIf_WarningIndCallback
// 2738  * description    : Warning Notification Callback Function
// 2739  * parameters     : status...Cause of the warning
// 2740  * return value   : none
// 2741  **********************************************************************/
// 2742 static void RfdrvIf_WarningIndCallback( uint8_t status )
// 2743 {
// 2744 }
// 2745 
// 2746 /*******************************************************************************
// 2747  * "RfdrvIf Message Buff"
// 2748  ******************************************************************************/
// 2749 /***********************************************************************
// 2750  * function name  : RfdrvIf_MessageProcess
// 2751  * description	  : message reception processing
// 2752  * parameters     : none
// 2753  * return value   : none
// 2754  **********************************************************************/
// 2755 void RfdrvIf_MessageProcess( void )
// 2756 {
// 2757 	uint8_t *pMsg;
// 2758 
// 2759 	pMsg = RfdrvIf_GetMsgBuff();
// 2760 	if ( pMsg != NULL )
// 2761 	{
// 2762 		R_SSCMD_ProcessCmd_RCVI( pMsg );
// 2763 		RfdrvIf_RelMsgBuff();
// 2764 	}
// 2765 }
// 2766 
// 2767 /***********************************************************************
// 2768  * function name  : RfdrvIf_InitMsgBuff
// 2769  * description    : ring buffer initialization processing
// 2770  * parameters     : none
// 2771  * return value   : none
// 2772  **********************************************************************/
// 2773 static void RfdrvIf_InitMsgBuff( void )
// 2774 {
// 2775 	r_rfdrvIf_msgbuff_t *pBuff;
// 2776 
// 2777 	pBuff = (r_rfdrvIf_msgbuff_t *)&gMsgBuff;
// 2778 
// 2779 	pBuff->readBufNum = 0;
// 2780 	pBuff->writBufNum = 0;
// 2781 	pBuff->bufFullBit = R_FALSE;
// 2782 }
// 2783 
// 2784 /***********************************************************************
// 2785  * function name  : RfdrvIf_PutMsgBuff
// 2786  * description    : message storage processing to the ring buffer
// 2787  * parameters     : pSrcBuff...
// 2788  *                : length...
// 2789  * return value   : none
// 2790  **********************************************************************/
// 2791 static void RfdrvIf_PutMsgBuff( void *pSrcBuff, uint16_t length )
// 2792 {
// 2793 	r_rfdrvIf_msgbuff_t *pBuff;
// 2794 
// 2795 	pBuff = (r_rfdrvIf_msgbuff_t *)&gMsgBuff;
// 2796 
// 2797 	if ( pBuff->bufFullBit == R_FALSE )
// 2798 	{
// 2799 		memcpy( (void *)&(pBuff->buff[pBuff->writBufNum].cont[0]), (void *)pSrcBuff, length );
// 2800 
// 2801 		pBuff->writBufNum++;
// 2802 		if ( pBuff->writBufNum >= R_MSG_BUFF_NUM )
// 2803 		{
// 2804 			pBuff->writBufNum = 0;
// 2805 		}
// 2806 		if ( pBuff->writBufNum == pBuff->readBufNum )
// 2807 		{
// 2808 			pBuff->bufFullBit = R_TRUE;
// 2809 		}
// 2810 	}
// 2811 }
// 2812 
// 2813 /***********************************************************************
// 2814  * function name  : RfdrvIf_GetMsgBuff
// 2815  * description    : message acquisition process from the ring buffer
// 2816  * parameters     : none
// 2817  * return value   : pointer of the acquired message
// 2818  **********************************************************************/
// 2819 static uint8_t *RfdrvIf_GetMsgBuff( void )
// 2820 {
// 2821 	uint8_t *pSrc = NULL;
// 2822 	r_rfdrvIf_msgbuff_t *pBuff = NULL;
// 2823 
// 2824 	pBuff = (r_rfdrvIf_msgbuff_t *)&gMsgBuff;
// 2825 
// 2826 	if (( pBuff->readBufNum != (uint8_t)(pBuff->writBufNum ))
// 2827 		|| ( pBuff->bufFullBit == R_TRUE ))
// 2828 	{
// 2829 		pSrc = (uint8_t *)&(pBuff->buff[pBuff->readBufNum].cont[0]);
// 2830 	}
// 2831 	return( pSrc );
// 2832 }
// 2833 
// 2834 /***********************************************************************
// 2835  * function name  : RfdrvIf_RelMsgBuff
// 2836  * description    : release of the buffer has stored the message
// 2837  * parameters     : none
// 2838  * return value   : none
// 2839  **********************************************************************/
// 2840 static void RfdrvIf_RelMsgBuff( void )
// 2841 {
// 2842 	r_rfdrvIf_msgbuff_t *pBuff = NULL;
// 2843 
// 2844 	pBuff = (r_rfdrvIf_msgbuff_t *)&gMsgBuff;
// 2845 
// 2846 	pBuff->readBufNum++;
// 2847 	if ( pBuff->readBufNum >= R_MSG_BUFF_NUM )
// 2848 	{
// 2849 		pBuff->readBufNum = 0;
// 2850 	}
// 2851 	pBuff->bufFullBit = R_FALSE;
// 2852 }
// 2853 
// 2854 /*******************************************************************************
// 2855  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
// 2856  ******************************************************************************/
// 2857 #endif /* R_SIMPLE_RFTEST_ENABLED */
// 
// 
// 0 bytes of memory
//
//Errors: none
//Warnings: none
