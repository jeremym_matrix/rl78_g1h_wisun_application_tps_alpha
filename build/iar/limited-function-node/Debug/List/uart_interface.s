///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:31:00
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\uart_interface.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW42EC.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\uart_interface.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\uart_interface.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_file_descriptor", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN ?L_IOR_L03
        EXTERN ?L_MUL_FAST_L03
        EXTERN ?L_NEG_L03
        EXTERN ?SL_DIV_L03
        EXTERN ?SL_MOD_L03
        EXTERN ?UL_RSH_L03
        EXTERN _RdrvUART_GetChar
        EXTERN _RdrvUART_GetLen
        EXTERN _RdrvUART_PutChar

        PUBLIC _RdrvGetcw
        PUBLIC _RdrvInputHex
        PUBLIC _RdrvPrint
        PUBLIC _RdrvPrintDec
        PUBLIC _RdrvPrintHex
        PUBLIC __COM_gets_f
        PUBLIC _getchar
        PUBLIC _putchar
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\uart_interface.c
//    1 /****************************************************************************** 
//    2 * DISCLAIMER 
//    3 
//    4 * This software is supplied by Renesas Electronics Corporation and is only 
//    5 * intended for use with Renesas products. No other uses are authorized. 
//    6 
//    7 * This software is owned by Renesas Electronics Corporation and is protected under 
//    8 * all applicable laws, including copyright laws. 
//    9 
//   10 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
//   11 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
//   12 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
//   13 * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
//   14 * DISCLAIMED. 
//   15 
//   16 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
//   17 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
//   18 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
//   19 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
//   20 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 
//   21 
//   22 * Renesas reserves the right, without notice, to make changes to this 
//   23 * software and to discontinue the availability of this software. 
//   24 * By using this software, you agree to the additional terms and 
//   25 * conditions found by accessing the following link: 
//   26 * http://www.renesas.com/disclaimer 
//   27 ******************************************************************************/ 
//   28 
//   29 /* Copyright (C) 2014. Renesas Electronics Corporation, All Rights Reserved. */
//   30 
//   31 /****************************************************************************** 
//   32 * File Name : uart_interface.h
//   33 * Version : 0.10 
//   34 * Device(s) : RL78/G13 
//   35 * Tool-Chain : CubeSuite+ 
//   36 * OS : RI78V4 
//   37 * H/W Platform : 
//   38 * Description : UART interface for RL78/G13 / cc78k0r.
//   39 * Operation : 
//   40 * Limitations : None 
//   41 ****************************************************************************** 
//   42 * History : DD.MM.YYYY Version Description 
//   43 * 
//   44 ******************************************************************************/ 
//   45 
//   46 
//   47 /****************************************************************************** 
//   48 Includes 
//   49 ******************************************************************************/ 
//   50 #include	"uart_interface.h"
//   51 #include	"stdio.h"
//   52 
//   53 
//   54 /******************************************************************************
//   55 Function Name:       getchar
//   56 Parameters:          none.
//   57 Return value:        Received character.
//   58 Description:         Get character from UART.
//   59 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _getchar
          CFI FunCall _RdrvUART_GetChar
        CODE
//   60 int getchar(void)
//   61 {
_getchar:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//   62     int		data = 0;
//   63 
//   64 	data = RdrvUART_GetChar();
//   65 
//   66     return data;
        CALL      F:_RdrvUART_GetChar  ;; 3 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//   67 }
//   68 
//   69 
//   70 /******************************************************************************
//   71 Function Name:       putchar
//   72 Parameters:          Character to be sent.
//   73 Return value:        none.
//   74 Description:         Put character to UART.
//   75 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _putchar
        CODE
//   76 int putchar( int data )
//   77 {
_putchar:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 2
//   78 #ifdef R_HYBRID_PLC_RF
//   79 	/* Add carriage return code */
//   80 	if(data == '\n')
//   81 	{
//   82 		RdrvUART_PutChar('\r');
//   83 	}
//   84 #endif
//   85 	RdrvUART_PutChar((unsigned char)data);
        MOV       A, X               ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
//   86 	
//   87     return data;
        MOVW      AX, [SP]           ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 13 cycles
//   88 }
//   89 
//   90 
//   91 /******************************************************************************
//   92 Function Name:       gets
//   93 Parameters:          none.
//   94 Return value:        Received strings.
//   95 Description:         Get strings from UART.
//   96 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function __COM_gets_f
        CODE
//   97 char __far * _COM_gets_f(char __far *buffer )
//   98 {
__COM_gets_f:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
//   99 	short			data = 0;
//  100 	short			index = 0;
        CLRW      AX                 ;; 1 cycle
//  101 	unsigned char	Continueflag = 1;
        BR        S:??RdrvInputHex_0  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
//  102 
//  103 	do{
//  104 		data = getchar();
//  105 
//  106 		switch(data){
//  107 			/* wait CR code */
//  108 			case	'\n':
//  109 			case	'\r':
//  110 				Continueflag = 0;
//  111 				break;
//  112 
//  113 			/* End of strings */
//  114 			case	EOF:
//  115 				if(index == 0)
//  116 				{
//  117 					return ( char * )NULL;
//  118 				}
//  119 				Continueflag = 0;
//  120 				break;
//  121 
//  122 	        /* Back space */
//  123 			case	'\b':
//  124 				if(index > 0)
//  125 				{
//  126 					/* Delete character */
//  127 					index--;
//  128 				}
//  129 				break;
//  130 
//  131 			default:
//  132 				/* Get character */
//  133 				buffer[index] = (char)data;
??_COM_gets_f_0:
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, BC             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, L               ;; 1 cycle
        MOV       ES:[DE], A         ;; 2 cycles
//  134 				index++;
        MOVW      AX, BC             ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
??RdrvInputHex_0:
        MOVW      [SP], AX           ;; 1 cycle
//  135 				break;
          CFI FunCall _RdrvUART_GetChar
        ; ------------------------------------- Block: 1 cycles
??_COM_gets_f_1:
        CALL      F:_RdrvUART_GetChar  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        SUBW      AX, #0x8           ;; 1 cycle
        BZ        ??RdrvInputHex_1   ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        SUBW      AX, #0x2           ;; 1 cycle
        BZ        ??RdrvInputHex_2   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0x3           ;; 1 cycle
        BZ        ??RdrvInputHex_2   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0xFFF2        ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        BNZ       ??_COM_gets_f_0    ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??RdrvInputHex_3   ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        BR        S:??RdrvInputHex_4  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RdrvInputHex_1:
        MOVW      AX, [SP]           ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8001        ;; 1 cycle
        BC        ??_COM_gets_f_1    ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        DECW      AX                 ;; 1 cycle
        BR        S:??RdrvInputHex_0  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  136 		}
//  137 	}while(Continueflag == 1);
??RdrvInputHex_2:
        MOVW      AX, [SP]           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  138 
//  139 	/* Add null code */
//  140 	buffer[index] = '\0';
??RdrvInputHex_3:
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[DE], A         ;; 2 cycles
//  141 
//  142 	return buffer;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
??RdrvInputHex_4:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 87 cycles
//  143 }
//  144 
//  145 
//  146 /******************************************************************************
//  147 Function Name:       RdrvGetcw
//  148 Parameters:          none.
//  149 Return value:        Received character. 
//  150 Description:         Get character from UART. 
//  151 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon0
          CFI Function _RdrvGetcw
          CFI FunCall _RdrvUART_GetLen
        CODE
//  152 unsigned char RdrvGetcw(void)
//  153 {
_RdrvGetcw:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  154 	/* Wait untill character input */
//  155 	while(RdrvUART_GetLen() == 0);
??RdrvGetcw_0:
        CALL      F:_RdrvUART_GetLen  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??RdrvGetcw_0      ;; 4 cycles
          CFI FunCall _RdrvUART_GetChar
        ; ------------------------------------- Block: 8 cycles
//  156 
//  157 	return (unsigned char)RdrvUART_GetChar();
        CALL      F:_RdrvUART_GetChar  ;; 3 cycles
        MOV       A, X               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 18 cycles
//  158 }
//  159 
//  160 
//  161 /******************************************************************************
//  162 Function Name:       RdrvPrint
//  163 Parameters:          str
//  164                        Pointer to string. 
//  165 Return value:        none.
//  166 Description:         Put strings to UART. 
//  167 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon1
          CFI Function _RdrvPrint
        CODE
//  168 void RdrvPrint(char *str)
//  169 {
_RdrvPrint:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
        BR        S:??RdrvInputHex_5  ;; 3 cycles
          CFI FunCall _RdrvUART_PutChar
        ; ------------------------------------- Block: 6 cycles
//  170 	/* Loop till end of strings */
//  171 	while (*str != '\0')
//  172 	{
//  173 
//  174 #if defined(RDRV_UART_NEWLINE_CR_LF)
//  175 		if(*str == '\n')
//  176 		{
//  177 			RdrvUART_PutChar('\r');
//  178 		}
//  179 #endif
//  180 
//  181 		/* Put character to UART */
//  182 		RdrvUART_PutChar((unsigned char)*str++);
??RdrvPrint_0:
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 12 cycles
//  183 	}
??RdrvInputHex_5:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        BNZ       ??RdrvPrint_0      ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
//  184 }
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 36 cycles
//  185 
//  186 
//  187 /******************************************************************************
//  188 Function Name:       RdrvPrintHex
//  189 Parameters:          Num
//  190                        Number(HEX).
//  191                      Len
//  192                        Length of strings.
//  193 Return value:        none.
//  194 Description:         Put numerical strings to UART. 
//  195 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon2
          CFI Function _RdrvPrintHex
        CODE
//  196 void RdrvPrintHex(unsigned long Num, unsigned char Len)
//  197 {
_RdrvPrintHex:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, E               ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RdrvInputHex_6   ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        SHL       A, 0x2             ;; 1 cycle
        ADD       A, #0xFC           ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, E               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
//  198 	unsigned char	tmp;
//  199 
//  200 	/* Loop till end of strings */
//  201 	while(Len > 0)
//  202 	{
//  203 		/* Get digit */
//  204 		tmp = (unsigned char)((Num >> ((Len - 1) * 4)) & 0x0F);
??RdrvPrintHex_0:
        MOV       A, [SP+0x01]       ;; 1 cycle
        MOV       E, A               ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
          CFI FunCall ?UL_RSH_L03
        CALL      N:?UL_RSH_L03      ;; 3 cycles
        MOV       A, X               ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
//  205 
//  206 		/* numerical -> HEX character and put to UART */
//  207 		if(tmp <= 0x09)
        CMP       A, #0xA            ;; 1 cycle
        BNC       ??RdrvInputHex_7   ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
//  208 		{
//  209 			RdrvUART_PutChar('0' + tmp);
        ADD       A, #0x30           ;; 1 cycle
        BR        S:??RdrvInputHex_8  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  210 		}
//  211 		else
//  212 		{
//  213 			RdrvUART_PutChar('A' + (tmp - 0x0A));
??RdrvInputHex_7:
        ADD       A, #0x37           ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        ; ------------------------------------- Block: 1 cycles
??RdrvInputHex_8:
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
//  214 		}
//  215 
//  216 		/* Decrement length */
//  217 		Len--;
        MOV       A, [SP+0x01]       ;; 1 cycle
        ADD       A, #0xFC           ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
//  218 	}
        BNZ       ??RdrvPrintHex_0   ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
//  219 }
??RdrvInputHex_6:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 54 cycles
//  220 
//  221 
//  222 /******************************************************************************
//  223 Function Name:       RdrvPrintDec
//  224 Parameters:          DecNum
//  225                        Number(DEC).
//  226                      Len
//  227                        Length of strings.
//  228 Return value:        none.
//  229 Description:         Put numerical strings to UART. 
//  230 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon2
          CFI Function _RdrvPrintDec
        CODE
//  231 void RdrvPrintDec(long DecNum, unsigned char Len, unsigned char SupCh)
//  232 {
_RdrvPrintDec:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 12
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+16
//  233 	long			digit = 1;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  234 	unsigned char	SupressFlg = 1;
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
//  235 	unsigned char	Num;
//  236 	unsigned char	i;
//  237 
//  238 	/* minus number? */
//  239 	if(DecNum<0)
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 18 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RdrvPrintDec_0:
        BNC       ??RdrvInputHex_9   ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  240 	{
//  241 		/* Put "-" character */
//  242 		RdrvUART_PutChar('-');
        MOV       A, #0x2D           ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
//  243 		DecNum = -DecNum;
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall ?L_NEG_L03
        CALL      N:?L_NEG_L03       ;; 3 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        ; ------------------------------------- Block: 13 cycles
??RdrvInputHex_9:
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       D, A               ;; 1 cycle
//  244 	}
//  245 
//  246 	/* Count digit */
//  247 	for(i=1; i<Len; i++)
        CMP       A, #0x2            ;; 1 cycle
        BC        ??RdrvInputHex_10  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        DEC       D                  ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        ONEW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  248 	{
//  249 		digit *= 10;
??RdrvPrintDec_1:
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+18
        MOV       L, #0xA            ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall ?L_MUL_FAST_L03
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
//  250 	}
        DEC       D                  ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        BNZ       ??RdrvPrintDec_1   ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
??RdrvPrintDec_2:
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  251 
//  252 	while(digit > 0)
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        CMPW      AX, #0x8000        ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 13 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RdrvPrintDec_3:
        SKNC                         ;; 4 cycles
        BR        R:??RdrvInputHex_11  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  253 	{
//  254 		if(digit == 1)
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??RdrvPrintDec_4:
        BNZ       ??RdrvInputHex_12  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  255 		{
//  256 			/* Suppression off (Lowest digit) */
//  257 			SupressFlg = 0;
??RdrvInputHex_10:
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  258 		}
//  259 
//  260 		/* Get top digit number */
//  261 		Num = (unsigned char)((DecNum / digit) % 10);
??RdrvInputHex_12:
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       X, #0xA            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
          CFI FunCall ?SL_DIV_L03
        CALL      N:?SL_DIV_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall ?SL_MOD_L03
        CALL      N:?SL_MOD_L03      ;; 3 cycles
        MOV       A, X               ;; 1 cycle
//  262 
//  263 		if(SupressFlg == 1)
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        BF        [HL].0, ??RdrvInputHex_13  ;; 5 cycles
        ; ------------------------------------- Block: 29 cycles
//  264 		{
//  265 			if(Num == 0)
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??RdrvInputHex_14  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  266 			{
//  267 				if(SupCh != '\0')
        MOV       A, [SP+0x07]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??RdrvInputHex_15  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  268 				{
//  269 					RdrvUART_PutChar(SupCh);
        BR        S:??RdrvInputHex_16  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  270 				}
//  271 			}
//  272 			else
//  273 			{
//  274 				/* Put a character(top digit number) */
//  275 				putc((unsigned char)('0' + Num));
??RdrvInputHex_14:
        ADD       A, #0x30           ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
//  276 
//  277 				/* Supression off */
//  278 				SupressFlg = 0;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x01], A       ;; 1 cycle
        BR        S:??RdrvInputHex_15  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
//  279 			}
//  280 		}
//  281 		else
//  282 		{
//  283 			/* Put a character(top digit number) */
//  284 			putc((unsigned char)('0' + Num));
??RdrvInputHex_13:
        MOV       A, [SP]            ;; 1 cycle
        ADD       A, #0x30           ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        ; ------------------------------------- Block: 2 cycles
??RdrvInputHex_16:
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  285 		}
//  286 
//  287 		/* remove top digit number and move a figure one place to the right */
//  288 		DecNum = DecNum - (Num * digit);
??RdrvInputHex_15:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
          CFI FunCall ?L_MUL_FAST_L03
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
//  289 		digit = digit / 10;
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       X, #0xA            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
          CFI FunCall ?SL_DIV_L03
        CALL      N:?SL_DIV_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        BR        R:??RdrvPrintDec_2  ;; 3 cycles
        ; ------------------------------------- Block: 44 cycles
//  290 	}
//  291 }
??RdrvInputHex_11:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 200 cycles
//  292 
//  293 
//  294 /******************************************************************************
//  295 Function Name:       RdrvInputHex
//  296 Parameters:          l
//  297                        Pinter to number(HEX).
//  298                      len
//  299                        Length of strings.
//  300 Return value:        none.
//  301 Description:         Get numerical strings from UART. 
//  302 ******************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon2
          CFI Function _RdrvInputHex
        CODE
//  303 unsigned long RdrvInputHex(unsigned long *Num, unsigned char Len)
//  304 {
_RdrvInputHex:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
//  305 	unsigned char	ch;
//  306 	unsigned char	index = 0;
        MOV       A, X               ;; 1 cycle
//  307 	unsigned char	ContinueFlag = 1;
        MOV       [SP+0x01], A       ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
          CFI FunCall _RdrvUART_GetLen
        ; ------------------------------------- Block: 9 cycles
??RdrvInputHex_17:
        CALL      F:_RdrvUART_GetLen  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??RdrvInputHex_17  ;; 4 cycles
          CFI FunCall _RdrvUART_GetChar
        ; ------------------------------------- Block: 8 cycles
        CALL      F:_RdrvUART_GetChar  ;; 3 cycles
//  308 
//  309 	while(ContinueFlag == 1)
//  310 	{
//  311 		/* Get character from UART */
//  312 		ch = getcw();
//  313 
//  314 		switch(ch)
        MOV       A, X               ;; 1 cycle
        SUB       A, #0x8            ;; 1 cycle
        BZ        ??RdrvInputHex_18  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        SUB       A, #0x2            ;; 1 cycle
        BZ        ??RdrvInputHex_19  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x3            ;; 1 cycle
        BZ        ??RdrvInputHex_19  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0xE            ;; 1 cycle
        BZ        ??RdrvInputHex_20  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUB       A, #0x64           ;; 1 cycle
        BZ        ??RdrvInputHex_18  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  315 		{
//  316 			case	0x1B:	/* ESC */
//  317 				index = 0;
//  318 				ContinueFlag = 0;
//  319 				break;
//  320 
//  321 			case	0x0A:	/* Line feed */
//  322 			case	0x0D:	/* Carriage return */
//  323 				ContinueFlag = 0;
//  324 				break;
//  325 
//  326 			case	0x08:	/* BS */
//  327 			case	0x7F:	/* DEL */
//  328 				/* Delete character */
//  329 				if(index > 0)
//  330 				{
//  331 					(*Num) = (*Num) >> 4;
//  332 					index--;
//  333 
//  334 					RdrvUART_PutChar(0x08);
//  335 					RdrvUART_PutChar(0x20);		/* Space */
//  336 					RdrvUART_PutChar(0x08);
//  337 				}
//  338 				break;
//  339 
//  340 			default:
//  341 				if((ch >= 0x30) && (ch <= 0x39))			/* '0'-'9' */
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xD0           ;; 1 cycle
        CMP       A, #0xA            ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        BNC       ??RdrvInputHex_21  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  342 				{
//  343 					*Num = (*Num) << 4;
//  344 					*Num |= (ch - 0x30) & 0x0F;
        CLRB      X                  ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        BR        S:??RdrvInputHex_22  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  345 					index++;
//  346 				}else
??RdrvInputHex_20:
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
//  347 				if((ch >= 0x41) && (ch <= 0x5A))			/* 'A'-'Z' */
//  348 				{
//  349 					*Num = (*Num) << 4;
//  350 					*Num |= ((ch - 0x41) & 0x0F) + 0x0A;
//  351 					index++;
//  352 				}else
//  353 				if((ch >= 0x61) && (ch <= 0x7A))			/* 'a'-'z' */
//  354 				{
//  355 					*Num = (*Num) << 4;
//  356 					*Num |= ((ch - 0x61) & 0x0F) + 0x0A;
//  357 					index++;
//  358 				}
//  359 				break;
//  360 		}
//  361 
//  362 		if(index >= Len)
//  363 		{
//  364 			ContinueFlag = 0;
//  365 		}
//  366 	}
//  367 
//  368 	/* add "CR" and "LF" */
//  369 	RdrvUART_PutChar(0x0D);
??RdrvInputHex_19:
        MOV       A, #0xD            ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
//  370 	RdrvUART_PutChar(0x0A);
        MOV       A, #0xA            ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
//  371 
//  372 	return index;
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+10
        ; ------------------------------------- Block: 19 cycles
??RdrvInputHex_18:
        MOV       A, [SP]            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??RdrvInputHex_23  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        SHRW      AX, 0x4            ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        SHRW      AX, 0x4            ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        SHLW      AX, 0xC            ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       A, [SP]            ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, #0x8            ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
        MOV       A, #0x20           ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
        MOV       A, #0x8            ;; 1 cycle
          CFI FunCall _RdrvUART_PutChar
        CALL      F:_RdrvUART_PutChar  ;; 3 cycles
        BR        S:??RdrvInputHex_23  ;; 3 cycles
        ; ------------------------------------- Block: 46 cycles
??RdrvInputHex_21:
        ADD       A, #0xBF           ;; 1 cycle
        CMP       A, #0x1A           ;; 1 cycle
        BNC       ??RdrvInputHex_24  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CLRB      A                  ;; 1 cycle
        ADDW      AX, #0xFFBF        ;; 1 cycle
        BR        S:??RdrvInputHex_25  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??RdrvInputHex_24:
        MOV       A, X               ;; 1 cycle
        ADD       A, #0x9F           ;; 1 cycle
        CMP       A, #0x1A           ;; 1 cycle
        BNC       ??RdrvInputHex_23  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CLRB      A                  ;; 1 cycle
        ADDW      AX, #0xFF9F        ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??RdrvInputHex_25:
        MOV       A, X               ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        AND       A, #0xF            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
??RdrvInputHex_22:
        MOVW      BC, AX             ;; 1 cycle
        SARW      AX, 0xF            ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        SHLW      BC, 0x4            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        SHRW      AX, 0xC            ;; 1 cycle
        ADDW      AX, BC             ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        SHLW      AX, 0x4            ;; 1 cycle
          CFI FunCall ?L_IOR_L03
        CALL      N:?L_IOR_L03       ;; 3 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        INC       A                  ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 39 cycles
??RdrvInputHex_23:
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP+0x01]       ;; 1 cycle
        CMP       X, A               ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??RdrvInputHex_17  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BR        R:??RdrvInputHex_19  ;; 3 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 208 cycles
//  373 }

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//  374 
// 
// 724 bytes in section .textf
// 
// 724 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
