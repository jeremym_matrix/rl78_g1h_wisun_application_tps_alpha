///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:55
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\eel_descriptor.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW2D4F.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\eel_descriptor.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\eel_descriptor.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1

        PUBLIC _eel_descriptor
        PUBLIC _eel_internal_cfg_cau08
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\eel_descriptor.c
//    1 /*******************************************************************************
//    2 * Library       : EEPROM Emulation Library (T02)
//    3 *
//    4 * File Name     : eel_descriptor.c
//    5 * Device(s)     : RL78
//    6 * Lib. Version  : V1.00 (for IAR V2)
//    7 * Description   : contains the user defined EEL-variable descriptor
//    8 *******************************************************************************
//    9 * DISCLAIMER
//   10 * This software is supplied by Renesas Electronics Corporation and is only
//   11 * intended for use with Renesas products. No other uses are authorized. This
//   12 * software is owned by Renesas Electronics Corporation and is protected under
//   13 * all applicable laws, including copyright laws.
//   14 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
//   15 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
//   16 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
//   17 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//   18 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   19 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   20 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
//   21 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
//   22 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   23 * Renesas reserves the right, without notice, to make changes to this software
//   24 * and to discontinue the availability of this software. By using this software,
//   25 * you agree to the additional terms and conditions found by accessing the
//   26 * following link:
//   27 * http://www.renesas.com/disclaimer
//   28 *
//   29 * Copyright (C) 2015, 2016 Renesas Electronics Corporation. All rights reserved.
//   30 *******************************************************************************/
//   31 
//   32 
//   33 
//   34 /*==============================================================================================*/
//   35 /* compiler settings                                                                            */
//   36 /*==============================================================================================*/
//   37 #pragma language = extended
//   38 
//   39 
//   40 /*==============================================================================================*/
//   41 /* include files list                                                                           */
//   42 /*==============================================================================================*/
//   43 #include  "eel_types.h"
//   44 #include  "eel_descriptor.h"
//   45 #include  "fdl_types.h"
//   46 
//   47 
//   48 /* definition of variable types registered at EEL */
//   49 #include  "EEL_user_types.h"
//   50 
//   51 
//   52 /*==============================================================================================*/
//   53 /* import list                                                                                  */
//   54 /*==============================================================================================*/
//   55 /* empty */
//   56 
//   57 
//   58 /*==============================================================================================*/
//   59 /* data definition                                                                              */
//   60 /*==============================================================================================*/
//   61 /* empty */
//   62   
//   63 /*********************************************************************************************************/
//   64 /*******                                                                                           *******/
//   65 /*******      B E G I N    O F    C U S T O M I Z A B L E    D E C L A R A T I O N    A R E A      *******/
//   66 /*******                                                                                           *******/
//   67 /*********************************************************************************************************/
//   68 
//   69 #pragma location="EEL_CNST"
//   70 #pragma data_alignment=2
//   71 

        SECTION EEL_CNST:FARCODE:REORDER:NOROOT(1)
//   72 __far const eel_u08 eel_descriptor[EEL_VAR_NO+2] =
_eel_descriptor:
        DATA8
        DB 14, 8, 8, 8, 8, 8, 8, 8, 4, 4, 4, 4, 4, 4, 4, 0
//   73 {
//   74   (eel_u08)(EEL_VAR_NO),                   /* variable count            */  \ 
//   75   (eel_u08)(sizeof(eel_u08[8])),           /* id=1 -> hash_1            */  \ 
//   76   (eel_u08)(sizeof(eel_u08[8])),           /* id=2 -> hash_2            */  \ 
//   77   (eel_u08)(sizeof(eel_u08[8])),           /* id=3 -> hash_3            */  \ 
//   78   (eel_u08)(sizeof(eel_u08[8])),           /* id=4 -> hash_4            */  \ 
//   79   (eel_u08)(sizeof(eel_u08[8])),           /* id=5 -> hash_5            */  \ 
//   80   (eel_u08)(sizeof(eel_u08[8])),           /* id=6 -> hash_6            */  \ 
//   81   (eel_u08)(sizeof(eel_u08[8])),           /* id=7 -> hash_7            */  \ 
//   82   (eel_u08)(sizeof(eel_u32)),              /* id=8 -> frame_counter_1   */  \ 
//   83   (eel_u08)(sizeof(eel_u32)),              /* id=9 -> frame_counter_2   */  \ 
//   84   (eel_u08)(sizeof(eel_u32)),              /* id=10 -> frame_counter_3   */  \ 
//   85   (eel_u08)(sizeof(eel_u32)),              /* id=11 -> frame_counter_4   */  \ 
//   86   (eel_u08)(sizeof(eel_u32)),              /* id=12 -> frame_counter_5   */  \ 
//   87   (eel_u08)(sizeof(eel_u32)),              /* id=13 -> frame_counter_6   */  \ 
//   88   (eel_u08)(sizeof(eel_u32)),              /* id=14 -> frame_counter_7   */  \ 
//   89   (eel_u08)(0x00),                         /* zero terminator  */  \ 
//   90 };
//   91 
//   92 /*********************************************************************************************************/
//   93 /*******                                                                                           *******/
//   94 /*******      E N D    O F    C U S T O M I Z A B L E    D E C L A R A T I O N    A R E A          *******/
//   95 /*******                                                                                           *******/
//   96 /*********************************************************************************************************/
//   97 
//   98 
//   99 
//  100 
//  101 /*********************************************************************************************************/
//  102 /*******                                                                                           *******/
//  103 /*******      B E G I N    O F    U N T O U C H A B L E     D E C L A R A T I O N    A R E A       *******/
//  104 /*******                                                                                           *******/
//  105 /*********************************************************************************************************/
//  106 
//  107 #pragma location="EEL_CNST"
//  108 #pragma data_alignment=2
//  109 

        SECTION EEL_CNST:FARCODE:REORDER:NOROOT(1)
//  110 __far const eel_u08   eel_internal_cfg_cau08[]     = {0x40};
_eel_internal_cfg_cau08:
        DATA8
        DB 64, 0

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//  111 
//  112 /*********************************************************************************************************/
//  113 /*******                                                                                           *******/
//  114 /*******      E N D    O F    U N T O U C H A B L E     D E C L A R A T I O N    A R E A           *******/
//  115 /*******                                                                                           *******/
//  116 /*********************************************************************************************************/
// 
// 18 bytes in section EEL_CNST
// 
// 18 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
