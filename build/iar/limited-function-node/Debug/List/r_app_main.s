///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:57
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\r_app_main.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3803.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\r_app_main.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\r_app_main.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_file_descriptor", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _AppCmd_Init
        EXTERN _AppIpProcessCmd
        EXTERN _AppIpReceiveMessage
        EXTERN _R_Modem_Demo_Init
        EXTERN _R_Modem_Demo_ReadCommand
        EXTERN _R_Modem_print
        EXTERN _R_OS_MsgFree
        EXTERN _R_OS_ReceiveMsg
        EXTERN _RdrvPeripheralInitialize
        EXTERN _memset

        PUBLIC _AppAton
        PUBLIC _AppCmdBuf
        PUBLIC _AppCmdSize
        PUBLIC _AppCmd_ProcessCmd
        PUBLIC _AppCmd_SkipWhiteSpace
        PUBLIC _AppHexStrToNum
        PUBLIC _AppStrToLower
        PUBLIC _AppStricmp
        PUBLIC _apl_task
        PUBLIC _reset
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\r_app_main.c
//    1 /**********************************************************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
//    4  * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
//    5  * applicable laws, including copyright laws.
//    6  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
//    7  * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
//    8  * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
//    9  * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
//   10  * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
//   11  * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   12  * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
//   13  * this software. By using this software, you agree to the additional terms and conditions found by accessing the
//   14  * following link:
//   15  * http://www.renesas.com/disclaimer
//   16  *
//   17  * Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
//   18  *********************************************************************************************************************/
//   19 
//   20 /**
//   21  * @file r_app_main.c
//   22  * @brief Generic application layer functionality (including task definition)
//   23  */
//   24 
//   25 /******************************************************************************
//   26    Includes   <System Includes> , "Project Includes"
//   27 ******************************************************************************/
//   28 #include "r_app_main.h"
//   29 
//   30 #include <stdio.h>
//   31 #include <string.h>
//   32 
//   33 #include "hardware.h"
//   34 #include "r_os_wrapper.h"
//   35 #include "r_nwk_api_base.h"
//   36 #include "r_rpl_api.h"
//   37 #include "r_modem_demo_msg.h"
//   38 
//   39 #ifdef R_SIMPLE_RFTEST_ENABLED
//   40 #include "r_simple_rftest.h"
//   41 #endif
//   42 
//   43 #if R_DEV_TBU_ENABLED && __RX
//   44 #include "iodefine.h"
//   45 #endif
//   46 
//   47 /******************************************************************************
//   48    Macro definitions
//   49 ******************************************************************************/
//   50 
//   51 
//   52 /******************************************************************************
//   53    Typedef definitions
//   54 ******************************************************************************/
//   55 
//   56 
//   57 /******************************************************************************
//   58    Imported global variables and functions (from other files)
//   59 ******************************************************************************/
//   60 extern void AppCmd_Init();
//   61 
//   62 #ifdef R_DEV_AUTO_START
//   63 extern void AppCmd_AutoStart();
//   64 #endif
//   65 
//   66 extern unsigned char AppIpProcessCmd(unsigned char* pCmd);
//   67 extern unsigned char AppIpReceiveMessage(unsigned char* pErase, r_os_msg_t p_msg);
//   68 
//   69 /******************************************************************************
//   70    Exported global variables and functions (to be accessed by other files)
//   71 ******************************************************************************/
//   72 unsigned char AppStrToLower(unsigned char c);
//   73 unsigned char AppAton(unsigned char c);
//   74 

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   75 unsigned char AppCmdBuf[APP_CMD_BUFSIZE];
_AppCmdBuf:
        DS 3164

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   76 unsigned short AppCmdSize;
_AppCmdSize:
        DS 2
//   77 
//   78 #ifdef R_SIMPLE_RFTEST_ENABLED
//   79 static r_boolean_t rfTestModeEnable = R_FALSE;
//   80 #endif
//   81 
//   82 /******************************************************************************
//   83    Map dummy variable to external SRAM section to avoid linker warning
//   84 ******************************************************************************/
//   85 #if defined(__CCRX__)
//   86 #pragma section B expRAM
//   87 uint8_t dummy_u8_var_in_expRAM;
//   88 uint32_t dummy_u32_var_in_expRAM;
//   89 #pragma section
//   90 #endif
//   91 
//   92 /******************************************************************************
//   93    Map rstr_handle variable to none initialized RAM section
//   94 ******************************************************************************/
//   95 #if R_DEV_TBU_ENABLED && __RX
//   96 #pragma section B noINIT
//   97 uint8_t rstr_handle;
//   98 #pragma section
//   99 #endif
//  100 
//  101 /******************************************************************************
//  102    Private variables
//  103 ******************************************************************************/
//  104 
//  105 /******************************************************************************
//  106    Private function prototypes
//  107 ******************************************************************************/
//  108 static void AppReceiveMessage(void);
//  109 
//  110 /******************************************************************************
//  111    Public function bodies
//  112 ******************************************************************************/
//  113 
//  114 /********************************************************************************
//  115 * Function Name     : reset
//  116 * Description       : Reset(dummy)
//  117 * Arguments         : None
//  118 * Return Value      : None
//  119 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _reset
          CFI NoCalls
        CODE
//  120 void reset(void)
//  121 {
_reset:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  122 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 6 cycles
//  123 
//  124 
//  125 /********************************************************************************
//  126 * Function Name     : apl_task
//  127 * Description       : Application task to process messages from IPv6 stack
//  128 * Arguments         : None
//  129 * Return Value      : None
//  130 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _apl_task
        CODE
//  131 void apl_task()
//  132 {
_apl_task:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 10
        SUBW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+14
//  133     /* Initialization */
//  134     RdrvPeripheralInitialize();
          CFI FunCall _RdrvPeripheralInitialize
        CALL      F:_RdrvPeripheralInitialize  ;; 3 cycles
//  135 
//  136 #ifdef R_SIMPLE_RFTEST_ENABLED
//  137 
//  138     /* Check if RF Test mode is enabled */
//  139     rfTestModeEnable = R_Simple_RFTest_IsValid();
//  140 
//  141     if (rfTestModeEnable == R_TRUE)
//  142     {
//  143         /* Execute simple RF Test program */
//  144         R_Simple_RFTest_Main();
//  145     }
//  146 #endif /* R_SIMPLE_RFTEST_ENABLED */
//  147 
//  148     R_Modem_Demo_Init(AppCmdBuf, sizeof(AppCmdBuf));  // Initialize HDLC module for UART
        MOVW      BC, #0xC5B         ;; 1 cycle
        MOVW      DE, #LWRD(_AppCmdBuf)  ;; 1 cycle
        MOV       A, #BYTE3(_AppCmdBuf)  ;; 1 cycle
          CFI FunCall _R_Modem_Demo_Init
        CALL      F:_R_Modem_Demo_Init  ;; 3 cycles
//  149 
//  150     AppCmd_Init();
          CFI FunCall _AppCmd_Init
        CALL      F:_AppCmd_Init     ;; 3 cycles
        MOVW      HL, SP             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
          CFI FunCall _R_Modem_Demo_ReadCommand
        ; ------------------------------------- Block: 18 cycles
//  151 
//  152 #ifdef R_DEV_AUTO_START
//  153     AppCmd_AutoStart();
//  154 #endif
//  155 
//  156 #if R_DEV_TBU_ENABLED && __RX
//  157 
//  158     /* Check if a software reset was detected  */
//  159     if (SYSTEM.RSTSR2.BIT.SWRF == R_TRUE)
//  160     {
//  161         /* Send the pending confirm for the RSTR reset request */
//  162         R_Modem_print("RSTC %02X %02X\n", rstr_handle, R_RESULT_SUCCESS);
//  163     }
//  164 #endif
//  165 
//  166     while (1)
//  167     {
//  168         R_Modem_Demo_ReadCommand();
??apl_task_0:
        CALL      F:_R_Modem_Demo_ReadCommand  ;; 3 cycles
//  169         AppReceiveMessage();
        CLRW      AX                 ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #0x3            ;; 1 cycle
          CFI FunCall _R_OS_ReceiveMsg
        CALL      F:_R_OS_ReceiveMsg  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??AppCmd_SkipWhiteSpace_0  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??AppCmd_SkipWhiteSpace_0:
        BZ        ??apl_task_0       ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       X, #0xF            ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
          CFI FunCall _AppIpReceiveMessage
        CALL      F:_AppIpReceiveMessage  ;; 3 cycles
        MOV       A, [SP]            ;; 1 cycle
        CMP       A, #0x1            ;; 1 cycle
        BNZ       ??apl_task_0       ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _R_OS_MsgFree
        CALL      F:_R_OS_MsgFree    ;; 3 cycles
        BR        S:??apl_task_0     ;; 3 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 11 cycles
        ; ------------------------------------- Total: 81 cycles
//  170     }
//  171 }
//  172 
//  173 /******************************************************************************
//  174    Private function bodies
//  175 ******************************************************************************/
//  176 
//  177 /********************************************************************************
//  178 * Function Name     : AppReceiveMessage
//  179 * Description       : Receive Message from NWK task
//  180 * Arguments         : None
//  181 * Return Value      : None
//  182 ********************************************************************************/
//  183 static void AppReceiveMessage(void)
//  184 {
//  185     r_os_msg_header_t* msg = R_OS_ReceiveMsg(ID_apl_mbx, R_OS_TIMEOUT_POLL);
//  186     if (msg != NULL)
//  187     {
//  188         unsigned char erase;
//  189         AppIpReceiveMessage(&erase, msg);
//  190         if (R_TRUE == erase)
//  191         {
//  192             R_OS_MsgFree(msg);
//  193         }
//  194     }
//  195 }
//  196 
//  197 /********************************************************************************
//  198 * Function Name     : AppCmd_ProcessCmd
//  199 * Description       : Process a test command
//  200 * Arguments         : pCmd ... Pointer to command
//  201 * Return Value      : None
//  202 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _AppCmd_ProcessCmd
        CODE
//  203 void AppCmd_ProcessCmd(unsigned char* pCmd, uint16_t size)
//  204 {
_AppCmd_ProcessCmd:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  205     if (pCmd[size - 1] != '\n')
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0xA            ;; 1 cycle
        BNZ       ??AppCmd_SkipWhiteSpace_1  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
//  206     {
//  207         return;  // Discard command if it does not end with a newline character
//  208     }
//  209 
//  210     if (AppIpProcessCmd(pCmd) == 0)
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _AppIpProcessCmd
        CALL      F:_AppIpProcessCmd  ;; 3 cycles
        CMP0      A                  ;; 1 cycle
        BNZ       ??AppCmd_SkipWhiteSpace_1  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
//  211     {
//  212         R_Modem_print("Command not found!!\n");
        MOVW      DE, #LWRD(?_0)     ;; 1 cycle
        MOV       A, #BYTE3(?_0)     ;; 1 cycle
          CFI FunCall _R_Modem_print
        CALL      F:_R_Modem_print   ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  213     }
//  214 }
??AppCmd_SkipWhiteSpace_1:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 37 cycles
//  215 
//  216 /********************************************************************************
//  217 * Function Name     : AppStricmp
//  218 * Description       : Compare string
//  219 * Arguments         : pStr1 ... Pointer to string #1
//  220 *                   : pStr2 ... Double pointer to string #2
//  221 * Return Value      : int8_t
//  222 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon2
          CFI Function _AppStricmp
          CFI NoCalls
        CODE
//  223 char AppStricmp(void __far* pStr1, void** ppStr2)
//  224 {
_AppStricmp:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 16
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+20
//  225     unsigned char __far* ptr1 = (unsigned char __far*)pStr1;
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  226     unsigned char* ptr2 = (unsigned char*)*ppStr2;
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
//  227     unsigned char c1 = 0, c2;
//  228 
//  229     /* first remove leading white spaces */
//  230     c2 = *ptr2;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BR        S:??AppCmd_SkipWhiteSpace_2  ;; 3 cycles
        ; ------------------------------------- Block: 31 cycles
//  231     while ((c2 == ' ') || (c2 == '\t'))
//  232     {
//  233         c2 = *(++ptr2);
??AppStricmp_0:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
??AppCmd_SkipWhiteSpace_2:
        MOV       A, ES:[HL]         ;; 2 cycles
//  234     }
        CMP       A, #0x20           ;; 1 cycle
        BZ        ??AppStricmp_0     ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        CMP       A, #0x9            ;; 1 cycle
        BZ        ??AppStricmp_0     ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  235 
//  236     while (1)
//  237     {
//  238         c1 = *ptr1;
//  239         c2 = *ptr2;
??AppStricmp_1:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
//  240 
//  241         /* upper case -> lower case */
//  242         c1 = AppStrToLower(c1);
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        ADD       A, #0xBF           ;; 1 cycle
        CMP       A, #0x1A           ;; 1 cycle
        BNC       ??AppCmd_SkipWhiteSpace_3  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        MOV       A, X               ;; 1 cycle
        ADD       A, #0x20           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  243         c2 = AppStrToLower(c2);
??AppCmd_SkipWhiteSpace_3:
        MOV       A, B               ;; 1 cycle
        ADD       A, #0xBF           ;; 1 cycle
        CMP       A, #0x1A           ;; 1 cycle
        BNC       ??AppCmd_SkipWhiteSpace_4  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, B               ;; 1 cycle
        ADD       A, #0x20           ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  244 
//  245         /* null terminator or no match ? */
//  246         if ((!c1 || !c2) || (c1 != c2))
??AppCmd_SkipWhiteSpace_4:
        CMP0      X                  ;; 1 cycle
        BZ        ??AppCmd_SkipWhiteSpace_5  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMP0      B                  ;; 1 cycle
        BZ        ??AppCmd_SkipWhiteSpace_6  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, B               ;; 1 cycle
        BNZ       ??AppCmd_SkipWhiteSpace_6  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  247         {
//  248             break;
//  249         }
//  250         else  /* if ( c1 == c2 ) */
//  251         {
//  252             ptr1++;
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  253             ptr2++;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        S:??AppStricmp_1   ;; 3 cycles
        ; ------------------------------------- Block: 17 cycles
//  254         }
//  255     }
??AppCmd_SkipWhiteSpace_5:
        CMP0      B                  ;; 1 cycle
        BZ        ??AppCmd_SkipWhiteSpace_7  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, #0x20           ;; 1 cycle
        BZ        ??AppCmd_SkipWhiteSpace_7  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        CMP       A, #0x9            ;; 1 cycle
        BNZ       ??AppCmd_SkipWhiteSpace_6  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  256 
//  257     /* re-position the pointer to skip the parsed part (only if match was a success) */
//  258     if (!c1 && (!c2 || c2 == ' ' || c2 == '\t'))
//  259     {
//  260         *ppStr2 = ptr2;
??AppCmd_SkipWhiteSpace_7:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  261         return 0;
        CLRB      A                  ;; 1 cycle
        BR        S:??AppCmd_SkipWhiteSpace_8  ;; 3 cycles
        ; ------------------------------------- Block: 24 cycles
//  262     }
//  263     else
//  264     {
//  265         return c1 - c2;
??AppCmd_SkipWhiteSpace_6:
        MOV       A, X               ;; 1 cycle
        SUB       A, B               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??AppCmd_SkipWhiteSpace_8:
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 167 cycles
//  266     }
//  267 }
//  268 
//  269 /********************************************************************************
//  270 * Function Name     : AppAton
//  271 * Description       : Convert ascii character to number
//  272 * Arguments         : c ... Ascii character ('0'...'9','a'...'f','A'...'F')
//  273 * Return Value      : Converted number (0 ... 15)
//  274 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon0
          CFI Function _AppAton
          CFI NoCalls
        CODE
//  275 unsigned char AppAton(unsigned char c)
//  276 {
_AppAton:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOV       X, A               ;; 1 cycle
//  277     c = AppStrToLower(c);
        ADD       A, #0xBF           ;; 1 cycle
        CMP       A, #0x1A           ;; 1 cycle
        BNC       ??AppCmd_SkipWhiteSpace_9  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, X               ;; 1 cycle
        ADD       A, #0x20           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  278     if (c >= '0' && c <= '9')
??AppCmd_SkipWhiteSpace_9:
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xD0           ;; 1 cycle
        CMP       A, #0xA            ;; 1 cycle
        SKNC                         ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
//  279     {
//  280         return c - '0';
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 6 cycles
//  281     }
//  282     else if (c >= 'a' && c <= 'f')
??AppAton_0:
        MOV       A, X               ;; 1 cycle
        ADD       A, #0x9F           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BNC       ??AppCmd_SkipWhiteSpace_10  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  283     {
//  284         return (c - 'a') + 10;
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xA9           ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 8 cycles
//  285     }
//  286     return 0;
??AppCmd_SkipWhiteSpace_10:
        CLRB      A                  ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 42 cycles
//  287 }
//  288 
//  289 /********************************************************************************
//  290 * Function Name     : AppStrToLower
//  291 * Description       : Compare string
//  292 * Arguments         : c ... data
//  293 * Return Value      : uint8_t
//  294 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon0
          CFI Function _AppStrToLower
          CFI NoCalls
        CODE
//  295 unsigned char AppStrToLower(unsigned char c)
//  296 {
_AppStrToLower:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOV       X, A               ;; 1 cycle
//  297     if ((c >= 'A') && (c <= 'Z'))
        ADD       A, #0xBF           ;; 1 cycle
        CMP       A, #0x1A           ;; 1 cycle
        BNC       ??AppCmd_SkipWhiteSpace_11  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  298     {
//  299         c = (c - 'A') + 'a';
        MOV       A, X               ;; 1 cycle
        ADD       A, #0x20           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  300     }
//  301     return c;
??AppCmd_SkipWhiteSpace_11:
        MOV       A, X               ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 17 cycles
//  302 }
//  303 
//  304 /********************************************************************************
//  305 * Function Name     : AppCmd_HexStrToNum
//  306 * Description       : Convert hex format string to number
//  307 * Arguments         : ppBuf ... Double pointer buffer
//  308 *                   : pData ... Ponter to data
//  309 *                   : size ... Size of string
//  310 *                   : isOctetStr ...
//  311 *                   :   R_TRUE: Octtet string
//  312 *                   :   R_TRUE: Big endian format
//  313 * Return Value      : r_result_t
//  314 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon2
          CFI Function _AppHexStrToNum
        CODE
//  315 unsigned char AppHexStrToNum(unsigned char** ppBuf, void* pData, short dataSize, unsigned char isOctetStr)
//  316 {
_AppHexStrToNum:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 24
        SUBW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+28
//  317     unsigned char* ptr1, * ptr2, byte;
//  318     unsigned short count, i;
//  319     unsigned char ret = 0;
        CLRB      A                  ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
//  320 
//  321     /* initialize with 0. */
//  322     if (0 == isOctetStr)
        MOV       A, [SP+0x1E]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??AppCmd_SkipWhiteSpace_12  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
//  323     {
//  324         memset(pData, 0, dataSize);
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        CLRW      BC                 ;; 1 cycle
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+28
        ; ------------------------------------- Block: 12 cycles
//  325     }
//  326 
//  327     ptr1 = *ppBuf;
//  328     ptr2 = (unsigned char*)pData;
??AppCmd_SkipWhiteSpace_12:
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
//  329 
//  330     /* skip the non-numeric characters */
//  331     AppCmd_SkipWhiteSpace(&ptr1);
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        BR        S:??AppCmd_SkipWhiteSpace_13  ;; 3 cycles
        ; ------------------------------------- Block: 20 cycles
??AppHexStrToNum_0:
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??AppCmd_SkipWhiteSpace_13:
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0x20           ;; 1 cycle
        BZ        ??AppHexStrToNum_0  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        CMP       A, #0x9            ;; 1 cycle
        BZ        ??AppHexStrToNum_0  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  332 
//  333     /* Count numeric characters */
//  334     count = 0;
        CLRW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        ADDW      AX, AX             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
//  335     while ((count != (dataSize * 2)) && ((*ptr1 >= '0' && *ptr1 <= '9')
//  336                                          || (*ptr1 >= 'A' && *ptr1 <= 'F')
//  337                                          || (*ptr1 >= 'a' && *ptr1 <= 'f')))
??AppHexStrToNum_1:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??AppCmd_SkipWhiteSpace_14  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        ADD       A, #0xD0           ;; 1 cycle
        CMP       A, #0xA            ;; 1 cycle
        BC        ??AppCmd_SkipWhiteSpace_15  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       A, ES:[HL]         ;; 2 cycles
        ADD       A, #0xBF           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BC        ??AppCmd_SkipWhiteSpace_15  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, ES:[HL]         ;; 2 cycles
        ADD       A, #0x9F           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        SKC                          ;; 4 cycles
        BR        R:??AppCmd_SkipWhiteSpace_14  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  338     {
//  339         count++;
??AppCmd_SkipWhiteSpace_15:
        INCW      BC                 ;; 1 cycle
//  340         ptr1++;
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        POP       DE                 ;; 1 cycle
          CFI CFA SP+28
        INCW      DE                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        S:??AppHexStrToNum_1  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
//  341     }
//  342 
//  343     if (!count || (count % 2) != 0)
//  344     {
//  345         ret = 1;
//  346     }
//  347     else
//  348     {
//  349         *ppBuf = ptr1;
??AppHexStrToNum_2:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, [SP+0x1A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+30
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  350 
//  351         if (isOctetStr)
        MOV       A, [SP+0x1E]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??AppCmd_SkipWhiteSpace_16  ;; 4 cycles
        ; ------------------------------------- Block: 26 cycles
//  352         {
//  353             ptr1 = ptr1 - count;
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        XOR       A, #0xFF           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        XOR       A, #0xFF           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        BR        S:??AppCmd_SkipWhiteSpace_17  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
//  354         }
//  355         else
//  356         {
//  357             ptr1 -= 2;
??AppCmd_SkipWhiteSpace_16:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xFFFE        ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
??AppCmd_SkipWhiteSpace_17:
        MOVW      [SP], AX           ;; 1 cycle
//  358         }
//  359 
//  360         i = count;
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  361         do
//  362         {
//  363             /* Convert character to number */
//  364             byte = AppAton(*ptr1++);
??AppHexStrToNum_3:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        ADD       A, #0xBF           ;; 1 cycle
        CMP       A, #0x1A           ;; 1 cycle
        BNC       ??AppCmd_SkipWhiteSpace_18  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, X               ;; 1 cycle
        ADD       A, #0x20           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??AppCmd_SkipWhiteSpace_18:
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xD0           ;; 1 cycle
        CMP       A, #0xA            ;; 1 cycle
        BC        ??AppCmd_SkipWhiteSpace_19  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, X               ;; 1 cycle
        ADD       A, #0x9F           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BNC       ??AppCmd_SkipWhiteSpace_20  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, X               ;; 1 cycle
        ADD       A, #0xA9           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??AppCmd_SkipWhiteSpace_19:
        MOV       C, A               ;; 1 cycle
        BR        S:??AppCmd_SkipWhiteSpace_21  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??AppCmd_SkipWhiteSpace_20:
        CLRB      C                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  365             byte <<= 4;
//  366             byte |= AppAton(*ptr1++);
??AppCmd_SkipWhiteSpace_21:
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+30
        POP       AX                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        ADD       A, #0xBF           ;; 1 cycle
        CMP       A, #0x1A           ;; 1 cycle
        BNC       ??AppCmd_SkipWhiteSpace_22  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        MOV       A, B               ;; 1 cycle
        ADD       A, #0x20           ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??AppCmd_SkipWhiteSpace_22:
        MOV       A, B               ;; 1 cycle
        ADD       A, #0xD0           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CMP       A, #0xA            ;; 1 cycle
        BC        ??AppCmd_SkipWhiteSpace_23  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, B               ;; 1 cycle
        ADD       A, #0x9F           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BNC       ??AppCmd_SkipWhiteSpace_24  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOV       A, B               ;; 1 cycle
        ADD       A, #0xA9           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        BR        S:??AppCmd_SkipWhiteSpace_23  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??AppCmd_SkipWhiteSpace_24:
        CLRB      X                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  367 
//  368             *ptr2++ = byte;
??AppCmd_SkipWhiteSpace_23:
        MOV       A, C               ;; 1 cycle
        SHL       A, 0x4             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
//  369             if (!isOctetStr)
        MOV       A, [SP+0x1E]       ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BNZ       ??AppCmd_SkipWhiteSpace_25  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
//  370             {
//  371                 ptr1 -= 4;
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
//  372             }
//  373             i -= 2;
??AppCmd_SkipWhiteSpace_25:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xFFFE        ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  374         }
//  375         while (i != 0);
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??AppHexStrToNum_3  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, [SP+0x1E]       ;; 1 cycle
//  376 
//  377         if (isOctetStr)
        CMP0      A                  ;; 1 cycle
        BZ        ??AppCmd_SkipWhiteSpace_26  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  378         {
//  379             memset(ptr2, 0, dataSize - (count / 2));
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        SHRW      AX, 0x1            ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        CLRW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _memset
        CALL      F:_memset          ;; 3 cycles
        POP       AX                 ;; 1 cycle
          CFI CFA SP+28
        ; ------------------------------------- Block: 14 cycles
//  380         }
//  381     }
//  382 
//  383     return ret;
??AppCmd_SkipWhiteSpace_26:
        MOV       A, [SP+0x06]       ;; 1 cycle
        ADDW      SP, #0x18          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+28
        ; ------------------------------------- Block: 8 cycles
??AppCmd_SkipWhiteSpace_14:
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??AppCmd_SkipWhiteSpace_27  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BT        [HL].0, $+7        ;; 5 cycles
        BR        F:??AppHexStrToNum_2  ;; 5 cycles
        ; ------------------------------------- Block: 8 cycles
??AppCmd_SkipWhiteSpace_27:
        ONEB      A                  ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        BR        S:??AppCmd_SkipWhiteSpace_26  ;; 3 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 5 cycles
        ; ------------------------------------- Total: 342 cycles
//  384 }
//  385 
//  386 /********************************************************************************
//  387 * Function Name     : AppSkipWhiteSpace
//  388 * Description       : Skip white space
//  389 * Arguments         : ppBuf ... Double pointer to buffer
//  390 * Return Value      : None
//  391 ********************************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon2
          CFI Function _AppCmd_SkipWhiteSpace
          CFI NoCalls
        CODE
//  392 void AppCmd_SkipWhiteSpace(unsigned char** ppBuf)
//  393 {
_AppCmd_SkipWhiteSpace:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
//  394     unsigned char* ptr = *ppBuf;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        BR        S:??AppCmd_SkipWhiteSpace_28  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
//  395 
//  396     /* skip the non-numeric characters */
//  397     while ((*ptr == ' ') || (*ptr == '\t'))
//  398     {
//  399         ptr++;
??AppCmd_SkipWhiteSpace_29:
        INCW      HL                 ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??AppCmd_SkipWhiteSpace_28:
        MOVW      [SP], AX           ;; 1 cycle
//  400     }
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP       A, #0x20           ;; 1 cycle
        BZ        ??AppCmd_SkipWhiteSpace_29  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        CMP       A, #0x9            ;; 1 cycle
        BZ        ??AppCmd_SkipWhiteSpace_29  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  401     *ppBuf = ptr;
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, X               ;; 1 cycle
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+12
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  402 }
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 23 cycles
        ; ------------------------------------- Total: 60 cycles

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_0:
        DB "Command not found!!\012"
        DATA8
        DB 0

        END
// 
// 3 166 bytes in section .bssf
//    22 bytes in section .constf
//   890 bytes in section .textf
// 
// 3 166 bytes of DATA    memory
//   912 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
