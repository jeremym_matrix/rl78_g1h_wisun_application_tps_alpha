///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:59
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\src\r_os_wrapper.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW3C3D.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\src\r_os_wrapper.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\r_os_wrapper.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _usCriticalNesting
        EXTERN _R_TIMER_Handle
        EXTERN ?L_AND_FAST_L03
        EXTERN ?L_MUL_FAST_L03
        EXTERN ?UL_CMP_L03
        EXTERN _apl_tsk
        EXTERN _mac_tsk
        EXTERN _nwk_tsk
        EXTERN _umm_free
        EXTERN _umm_malloc
        EXTERN _vTaskDelay
        EXTERN _vTaskPrioritySet
        EXTERN _vTaskResume
        EXTERN _vTaskSuspend
        EXTERN _xEventGroupCreateStatic
        EXTERN _xEventGroupSetBits
        EXTERN _xEventGroupSetBitsFromISR
        EXTERN _xEventGroupWaitBits
        EXTERN _xQueueCreateCountingSemaphoreStatic
        EXTERN _xQueueGenericReceive
        EXTERN _xQueueGenericSend
        EXTERN _xQueueGiveFromISR
        EXTERN _xTaskCreateStatic
        EXTERN _xTaskGetCurrentTaskHandle
        EXTERN _xTaskGetTickCount
        EXTERN _xTimerCreateStatic
        EXTERN _xTimerGenericCommand

        PUBLIC _R_OS_ChangeOwnTaskPriority
        PUBLIC _R_OS_DelayTaskMs
        PUBLIC _R_OS_GetTaskHandle
        PUBLIC _R_OS_GetTaskId
        PUBLIC _R_OS_Init
        PUBLIC _R_OS_MsgAlloc
        PUBLIC _R_OS_MsgFree
        PUBLIC _R_OS_MsgMarkIgnoreFree
        PUBLIC _R_OS_PostSemaphore
        PUBLIC _R_OS_ReceiveMsg
        PUBLIC _R_OS_SendMsg
        PUBLIC _R_OS_SendMsgFromISR
        PUBLIC _R_OS_SetFlag
        PUBLIC _R_OS_SetFlagFromISR
        PUBLIC _R_OS_Sleep
        PUBLIC _R_OS_SleepMaxTicks
        PUBLIC _R_OS_StartCyclic
        PUBLIC _R_OS_StartTask
        PUBLIC _R_OS_StopCyclic
        PUBLIC _R_OS_WaitForFlag
        PUBLIC _R_OS_WaitSemaphore
        PUBLIC _R_OS_WakeupTask
        PUBLIC _r_os_wrapper_internal_heap_addr
        PUBLIC _r_os_wrapper_internal_heap_size
        PUBLIC _vApplicationGetIdleTaskMemory
        PUBLIC _vApplicationGetTimerTaskMemory
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
        
          CFI Common cfiCommon3 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon3
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\src\r_os_wrapper.c
//    1 /******************************************************************************
//    2 * DISCLAIMER
//    3 * This software is supplied by Renesas Electronics Corporation and is only
//    4 * intended for use with Renesas products. No other uses are authorized. This
//    5 * software is owned by Renesas Electronics Corporation and is protected under
//    6 * all applicable laws, including copyright laws.
//    7 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
//    8 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
//    9 * LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
//   10 * AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//   11 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   12 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   13 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
//   14 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
//   15 * BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   16 * Renesas reserves the right, without notice, to make changes to this software
//   17 * and to discontinue the availability of this software. By using this software,
//   18 * you agree to the additional terms and conditions found by accessing the
//   19 * following link:
//   20 * http://www.renesas.com/disclaimer
//   21 *
//   22 * Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
//   23 ******************************************************************************/
//   24 
//   25 /*!
//   26    \file      r_os_wrapper.c
//   27    \version   1.00
//   28    \brief     Wrapper for FreeRTOS API
//   29  */
//   30 
//   31 // Note: all header files that #include r_os_wrapper_config.h must be included later
//   32 #include "FreeRTOS.h"
//   33 #include "queue.h"
//   34 #include "event_groups.h"
//   35 #include "semphr.h"
//   36 #include "task.h"
//   37 #include "r_impl_utils.h"
//   38 
//   39 
//   40 /*
//   41  * We need 32 bits ticks because
//   42  * "an event group is 8 if configUSE_16_BIT_TICKS is set to 1, or 24 if configUSE_16_BIT_TICKS is set to 0"
//   43  * [https://www.freertos.org/xEventGroupCreate.html] and roa expects 16 bits for event groups.
//   44  * Additionally, we use 32 bits for timeouts
//   45  */
//   46 #if configUSE_16_BIT_TICKS != 0
//   47 #error configUSE_16_BIT_TICKS!=0 not supported
//   48 #endif
//   49 
//   50 /******************************************************************************
//   51 * Support for the r_os_wrapper_id_config file
//   52 ******************************************************************************/
//   53 #define SIZE_TO_NSTACKTYPES(size) ELEMENTS(size, StackType_t)
//   54 
//   55 struct r_os_msg_header_s;
//   56 
//   57 // first round: allocate dynamically sized buffers
//   58 // align stacks on StackType_t, heaps on uint32_t
//   59 #include "r_os_wrapper_def.h"
//   60 #undef R_OS_TASK_LIST_ENTRY
//   61 #define R_OS_TASK_LIST_ENTRY(id, name, func, stack, priority, suspended) \ 
//   62     static StackType_t task_stack_storage_##id[SIZE_TO_NSTACKTYPES(stack)];
//   63 
//   64 // "All memory used by the manager is allocated at link time, it is aligned on a 32 bit boundary"
//   65 // "Each memory block holds 8 bytes, and there are up to 32767 blocks available"
//   66 #undef R_OS_HEAP_LIST_ENTRY
//   67 #define R_OS_HEAP_LIST_ENTRY(id, size) \ 
//   68     static uint32_t heap_storage_##id[ELEMENTS(size, uint32_t)]; \ 
//   69     STATIC_ASSERT(size < (uint32_t)8 * 32767, Heap_size_too_big_for_##id);
//   70 
//   71 // include the configuration file to make the declarations
//   72 #include "r_os_wrapper_config.h"

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static uint32_t heap_storage_R_HEAP_ID_CYCTICK_NTF[20]
_heap_storage_R_HEAP_ID_CYCTICK_NTF:
        DS 80

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static uint32_t heap_storage_R_HEAP_ID_WARN_IND[12]
_heap_storage_R_HEAP_ID_WARN_IND:
        DS 48

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static uint32_t heap_storage_R_HEAP_ID_ERR_IND[12]
_heap_storage_R_HEAP_ID_ERR_IND:
        DS 48

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static uint32_t heap_storage_R_HEAP_ID_APL_REQ[406]
_heap_storage_R_HEAP_ID_APL_REQ:
        DS 1624

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static uint32_t heap_storage_R_HEAP_ID_MAC_NEIGHBORS[200]
_heap_storage_R_HEAP_ID_MAC_NEIGHBORS:
        DS 800

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static uint32_t heap_storage_R_HEAP_ID_NWK[4352]
_heap_storage_R_HEAP_ID_NWK:
        DS 17408

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static uint16_t task_stack_storage_ID_mac_tsk[754]
_task_stack_storage_ID_mac_tsk:
        DS 1508

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static uint16_t task_stack_storage_ID_nwk_tsk[1054]
_task_stack_storage_ID_nwk_tsk:
        DS 2108

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static uint16_t task_stack_storage_ID_apl_tsk[1354]
_task_stack_storage_ID_apl_tsk:
        DS 2708
//   73 
//   74 // some static checks
//   75 #ifndef R_HEAP_COUNT
//   76 #error R_HEAP_COUNT must be set (#define) to the total number of heaps
//   77 #endif
//   78 #if R_OS_HEAP_BASEID == 0
//   79 #error R_OS_HEAP_BASEID must be greater than 0
//   80 #endif
//   81 
//   82 
//   83 
//   84 // clean #include guard and macros for second round
//   85 #undef R_OS_WRAPPER_CONFIG_H
//   86 #undef R_OS_MAILBOX_LIST_BEGIN
//   87 #undef R_OS_MAILBOX_LIST_ENTRY
//   88 #undef R_OS_MAILBOX_LIST_END
//   89 #undef R_OS_FLAG_LIST_BEGIN
//   90 #undef R_OS_FLAG_LIST_ENTRY
//   91 #undef R_OS_FLAG_LIST_END
//   92 #undef R_OS_SEMAPHORE_LIST_BEGIN
//   93 #undef R_OS_SEMAPHORE_LIST_ENTRY
//   94 #undef R_OS_SEMAPHORE_LIST_END
//   95 #undef R_OS_TIMER_LIST_BEGIN
//   96 #undef R_OS_TIMER_LIST_ENTRY
//   97 #undef R_OS_TIMER_LIST_END
//   98 #undef R_OS_TASK_LIST_BEGIN
//   99 #undef R_OS_TASK_LIST_ENTRY
//  100 #undef R_OS_TASK_LIST_END
//  101 #undef R_OS_HEAP_LIST_BEGIN
//  102 #undef R_OS_HEAP_LIST_ENTRY
//  103 #undef R_OS_HEAP_LIST_END
//  104 
//  105 
//  106 // second round
//  107 
//  108 typedef struct
//  109 {
//  110     size_t    capacity;
//  111     uint32_t* address;
//  112 } heap_def_t;
//  113 
//  114 #define R_OS_HEAP_LIST_BEGIN static heap_def_t heaps[] = {
//  115 #define R_OS_HEAP_LIST_ENTRY(id, size) {size, heap_storage_##id},
//  116 #define R_OS_HEAP_LIST_END   };
//  117 
//  118 typedef struct
//  119 {
//  120     struct r_os_msg_header_s* head;
//  121     struct r_os_msg_header_s* tail;
//  122     SemaphoreHandle_t         sem_count;
//  123     StaticSemaphore_t         semaphore_buffer_count;
//  124 } mailbox_t;
//  125 
//  126 #define R_OS_MAILBOX_LIST_BEGIN static mailbox_t mailboxes[] = {
//  127 #define R_OS_MAILBOX_LIST_ENTRY(id) {NULL},
//  128 #define R_OS_MAILBOX_LIST_END   };
//  129 
//  130 
//  131 #define R_OS_FLAG_LIST_BEGIN    static EventGroupHandle_t events[] = {
//  132 #define R_OS_FLAG_LIST_ENTRY(id) NULL,
//  133 #define R_OS_FLAG_LIST_END      }; \ 
//  134     static StaticEventGroup_t eventBuffers[ARRAY_SIZE(events)];
//  135 
//  136 
//  137 typedef struct
//  138 {
//  139     uint8_t max;
//  140     uint8_t initial;
//  141 } semaphore_def_t;
//  142 
//  143 #define R_OS_SEMAPHORE_LIST_BEGIN static const semaphore_def_t semaphore_defs[] = {
//  144 #define R_OS_SEMAPHORE_LIST_ENTRY(id, max, initial) {max, initial},
//  145 #define R_OS_SEMAPHORE_LIST_END   }; \ 
//  146     static SemaphoreHandle_t semaphores[ARRAY_SIZE(semaphore_defs)]; \ 
//  147     static StaticSemaphore_t semaphore_buffers[ARRAY_SIZE(semaphore_defs)];
//  148 
//  149 
//  150 typedef struct
//  151 {
//  152     TimerCallbackFunction_t func;
//  153     uint32_t                period_millis;
//  154     uint8_t                 oneshot;
//  155 } timer_def_t;
//  156 
//  157 #define R_OS_TIMER_LIST_BEGIN static const timer_def_t timer_defs[] = {
//  158 #define R_OS_TIMER_LIST_ENTRY(id, func, period, oneshot) {func, period, oneshot},
//  159 #define R_OS_TIMER_LIST_END   }; \ 
//  160     static TimerHandle_t timers[ARRAY_SIZE(timer_defs)]; \ 
//  161     static StaticTimer_t timer_buffers[ARRAY_SIZE(timer_defs)];
//  162 
//  163 
//  164 // Implementation note: uItron slp_tsk/wup_tsk is counting. The FreeRTOS counterpart is not -> race condition
//  165 // Instead of suspend/resume, use wait/post a dedicated semaphore
//  166 typedef struct
//  167 {
//  168     char* const  name;
//  169     void (* func)(void*);
//  170     StackType_t* stack_storage;
//  171     uint16_t     stack;
//  172     uint8_t      priority;
//  173     uint8_t      suspended;
//  174 } task_def_t;
//  175 
//  176 typedef struct
//  177 {
//  178     TaskHandle_t      task;
//  179     SemaphoreHandle_t sleep;
//  180     StaticSemaphore_t semaphore_buffer_sleep;
//  181 } task_info_t;
//  182 
//  183 // FreeRTOS expects the stack depth in terms of StackType_t
//  184 #define R_OS_TASK_LIST_BEGIN static const task_def_t task_defs[] = {
//  185 #define R_OS_TASK_LIST_ENTRY(id, name, func, stack, priority, suspended) \ 
//  186     {name, func, task_stack_storage_##id, SIZE_TO_NSTACKTYPES(stack), priority, suspended},
//  187 #define R_OS_TASK_LIST_END   }; \ 
//  188     static task_info_t tasks[ARRAY_SIZE(task_defs)]; \ 
//  189     static StaticTask_t task_buffers[ARRAY_SIZE(task_defs)];
//  190 
//  191 
//  192 // Use our implementation macros instead of the 'prototypes' in R_OS_WRAPPER_DEF_H
//  193 #define R_OS_WRAPPER_DEF_H
//  194 #include "r_os_wrapper.h"

        SECTION `.dataf`:DATA:REORDER:NOROOT(1)
// static heap_def_t heaps[6]
_heaps:
        DATA16
        DW 80
        DATA32
        DD _heap_storage_R_HEAP_ID_CYCTICK_NTF
        DATA16
        DW 48
        DATA32
        DD _heap_storage_R_HEAP_ID_WARN_IND
        DATA16
        DW 48
        DATA32
        DD _heap_storage_R_HEAP_ID_ERR_IND
        DATA16
        DW 1624
        DATA32
        DD _heap_storage_R_HEAP_ID_APL_REQ
        DATA16
        DW 800
        DATA32
        DD _heap_storage_R_HEAP_ID_MAC_NEIGHBORS
        DATA16
        DW 17408
        DATA32
        DD _heap_storage_R_HEAP_ID_NWK

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static mailbox_t mailboxes[4]
_mailboxes:
        DS 320

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static void * events[1]
_events:
        DS 4

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
// static task_def_t const task_defs[3]
_task_defs:
        DATA32
        DD ?_0, _mac_tsk, _task_stack_storage_ID_mac_tsk
        DATA16
        DW 754
        DATA8
        DB 3, 1
        DATA32
        DD ?_1, _nwk_tsk, _task_stack_storage_ID_nwk_tsk
        DATA16
        DW 1054
        DATA8
        DB 1, 1
        DATA32
        DD ?_2, _apl_tsk, _task_stack_storage_ID_apl_tsk
        DATA16
        DW 1354
        DATA8
        DB 0, 0
        DB ""

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static void * semaphores[1]
_semaphores:
        DS 4
        DS 4
        DS 24
        DS 68
        DS 40
        DS 228

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
// static task_info_t tasks[3]
_tasks:
        DS 228
//  195 
//  196 /******************************************************************************
//  197 * Stack includes
//  198 ******************************************************************************/
//  199 
//  200 #include "r_heap.h"
//  201 #include "r_os_wrapper_internal.h"
//  202 #include "r_contract_checks.h"
//  203 
//  204 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _R_OS_MsgAlloc
        CODE
//  205 r_os_msg_header_t* R_OS_MsgAlloc(r_os_id_t memId, size_t size)
//  206 {
_R_OS_MsgAlloc:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 6
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
//  207     CCHKP(BETWEEN(memId, R_OS_HEAP_BASEID, R_OS_HEAP_BASEID + ARRAY_SIZE(heaps)));
        ADD       A, #0xE0           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BC        ??vApplicationGetTimerTaskMemory_0  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        BR        S:??vApplicationGetTimerTaskMemory_1  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  208     r_os_msg_header_t* msg = r_alloc(memId, size);
??vApplicationGetTimerTaskMemory_0:
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, #0xFFE0        ;; 1 cycle
          CFI FunCall _umm_malloc
        CALL      F:_umm_malloc      ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  209     if (msg)
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_2  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_2:
        BZ        ??vApplicationGetTimerTaskMemory_3  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  210     {
//  211         msg->mem_id = memId;
        MOV       A, [SP+0x05]       ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 8 cycles
//  212     }
//  213     return msg;
??vApplicationGetTimerTaskMemory_3:
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??vApplicationGetTimerTaskMemory_1:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 57 cycles
//  214 }
//  215 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon1
          CFI Function _R_OS_MsgFree
        CODE
//  216 void R_OS_MsgFree(r_os_msg_header_t* msg)
//  217 {
_R_OS_MsgFree:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  218     if (!msg || msg->mem_id == 0 || msg->mem_id == R_OS_INVALID_ID)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_4  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_4:
        BZ        ??vApplicationGetTimerTaskMemory_5  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       L, A               ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_5  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        INC       A                  ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_5  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  219     {
//  220         return;  // NULL or not from us
//  221     }
//  222 
//  223     CCHKV(BETWEEN(msg->mem_id, R_OS_HEAP_BASEID, R_OS_HEAP_BASEID + ARRAY_SIZE(heaps)));
        MOV       A, L               ;; 1 cycle
        ADD       A, #0xE0           ;; 1 cycle
        CMP       A, #0x6            ;; 1 cycle
        BNC       ??vApplicationGetTimerTaskMemory_5  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
//  224     r_free(msg->mem_id, msg);
        MOV       A, ES              ;; 1 cycle
        MOV       C, A               ;; 1 cycle
        MOV       H, #0x0            ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xFFE0        ;; 1 cycle
          CFI FunCall _umm_free
        CALL      F:_umm_free        ;; 3 cycles
//  225 }
        ; ------------------------------------- Block: 8 cycles
??vApplicationGetTimerTaskMemory_5:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 57 cycles
//  226 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _R_OS_MsgMarkIgnoreFree
          CFI NoCalls
        CODE
//  227 void R_OS_MsgMarkIgnoreFree(r_os_msg_header_t* msg)
//  228 {
_R_OS_MsgMarkIgnoreFree:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  229     if (msg)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_6  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_6:
        BZ        ??vApplicationGetTimerTaskMemory_7  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  230     {
//  231         msg->mem_id = 0;  // use zero, so statically allocated messages just work
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 6 cycles
//  232     }
//  233 }
??vApplicationGetTimerTaskMemory_7:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 32 cycles
//  234 
//  235 
//  236 /******************************************************************************
//  237 * Heap
//  238 ******************************************************************************/
//  239 
//  240 STATIC_ASSERT(R_HEAP_COUNT == ARRAY_SIZE(heaps), Number_of_heaps_must_equal_R_HEAP_COUNT);
//  241 
//  242 // functions for r_heap (id is zero based!)

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon2
          CFI Function _r_os_wrapper_internal_heap_addr
          CFI NoCalls
        CODE
//  243 void* r_os_wrapper_internal_heap_addr(unsigned id)
//  244 {
_r_os_wrapper_internal_heap_addr:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  245     CCHKP(id < R_HEAP_COUNT);
        CMPW      AX, #0x6           ;; 1 cycle
        BC        ??vApplicationGetTimerTaskMemory_8  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 8 cycles
//  246     return heaps[id].address;
??vApplicationGetTimerTaskMemory_8:
        MOVW      BC, #0x6           ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_heaps+2)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_heaps)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 19 cycles
        ; ------------------------------------- Total: 32 cycles
//  247 }
//  248 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon2
          CFI Function _r_os_wrapper_internal_heap_size
          CFI NoCalls
        CODE
//  249 size_t r_os_wrapper_internal_heap_size(unsigned id)
//  250 {
_r_os_wrapper_internal_heap_size:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  251     CCHK0(id < R_HEAP_COUNT);
        CMPW      AX, #0x6           ;; 1 cycle
        BC        ??vApplicationGetTimerTaskMemory_9  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CLRW      AX                 ;; 1 cycle
        RET                          ;; 6 cycles
        ; ------------------------------------- Block: 7 cycles
//  252     return heaps[id].capacity;
??vApplicationGetTimerTaskMemory_9:
        MOVW      BC, #0x6           ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_heaps)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_heaps)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 14 cycles
        ; ------------------------------------- Total: 26 cycles
//  253 }
//  254 
//  255 
//  256 /******************************************************************************
//  257 * Mailboxes
//  258 ******************************************************************************/
//  259 
//  260 /* FreeRTOS Queues are external: they require specifying their maximum length
//  261  * but do not impose requirements on the queued items. It is not possible
//  262  * to specify the mailbox length for uItron and they seemingly use the
//  263  * 'kernel_msg_head' for an internal list implementation which requires all
//  264  * mailbox items to start with this special header. Therefore, the send_msg
//  265  * function of uItron cannot fail and the code does not check success. Since
//  266  * the code assumes the uItron style we provide a mailbox implementation with
//  267  * the same interface and requirements.
//  268  *
//  269  * The semaphore holds the number of messages and allows waiting on new entries.
//  270  * Head and tail are used to implement O(1) enqueue and dequeue primitives. The
//  271  * messages form a linked list based on r_os_msg_header_t#kernel_msghead
//  272  */
//  273 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon2
          CFI Function _R_OS_ReceiveMsg
        CODE
//  274 r_os_msg_header_t* R_OS_ReceiveMsg(r_os_id_t mbxId, uint32_t timeOutMs)
//  275 {
_R_OS_ReceiveMsg:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 16
        SUBW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+20
//  276     BaseType_t res = xSemaphoreTake(mailboxes[mbxId].sem_count, pdMS_TO_TICKS(timeOutMs));
//  277     if (res != pdTRUE)
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_mailboxes)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_mailboxes)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      DE, #0x3E8         ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
          CFI FunCall ?L_MUL_FAST_L03
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+26
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        CLRW      BC                 ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
          CFI FunCall _xQueueGenericReceive
        CALL      F:_xQueueGenericReceive  ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+20
        CMPW      AX, #0x1           ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_10  ;; 4 cycles
        ; ------------------------------------- Block: 80 cycles
//  278     {
//  279         return NULL;
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        BR        R:??vApplicationGetTimerTaskMemory_11  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  280     }
//  281     taskENTER_CRITICAL();
??vApplicationGetTimerTaskMemory_10:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_12  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??vApplicationGetTimerTaskMemory_12:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
//  282     r_os_msg_header_t* msg = mailboxes[mbxId].head;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  283     mailboxes[mbxId].head = msg->kernel_msghead;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  284     if (mailboxes[mbxId].head == NULL)
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_13  ;; 4 cycles
        ; ------------------------------------- Block: 54 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_13:
        BNZ       ??vApplicationGetTimerTaskMemory_14  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  285     {
//  286         mailboxes[mbxId].tail = NULL;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 11 cycles
//  287     }
//  288     taskEXIT_CRITICAL();
??vApplicationGetTimerTaskMemory_14:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_15  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_15  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  289     msg->kernel_msghead = NULL;  // not necessary (for debugging)
??vApplicationGetTimerTaskMemory_15:
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  290     return msg;
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        ; ------------------------------------- Block: 13 cycles
??vApplicationGetTimerTaskMemory_11:
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 213 cycles
//  291 }
//  292 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon1
          CFI Function _R_OS_SendMsg
        CODE
//  293 void R_OS_SendMsg(r_os_msg_header_t* msg, r_os_id_t mbxId)
//  294 {
_R_OS_SendMsg:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
        XCH       A, X               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
//  295     if (msg == NULL)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_16  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_16:
        SKNZ                         ;; 4 cycles
        BR        R:??vApplicationGetTimerTaskMemory_17  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  296     {
//  297         return;
//  298     }
//  299     msg->kernel_msghead = NULL;
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  300     taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_18  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??vApplicationGetTimerTaskMemory_18:
        MOV       A, B               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
//  301     if (mailboxes[mbxId].tail)
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_mailboxes)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_mailboxes)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_19  ;; 4 cycles
        ; ------------------------------------- Block: 37 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_19:
        BZ        ??vApplicationGetTimerTaskMemory_20  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  302     {
//  303         mailboxes[mbxId].tail->kernel_msghead = msg;
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 20 cycles
//  304     }
//  305     mailboxes[mbxId].tail = msg;
??vApplicationGetTimerTaskMemory_20:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  306     if (mailboxes[mbxId].head == NULL)
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_21  ;; 4 cycles
        ; ------------------------------------- Block: 35 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_21:
        BNZ       ??vApplicationGetTimerTaskMemory_22  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  307     {
//  308         mailboxes[mbxId].head = msg;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 13 cycles
//  309     }
//  310     taskEXIT_CRITICAL();
??vApplicationGetTimerTaskMemory_22:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_23  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_23  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  311     xSemaphoreGive(mailboxes[mbxId].sem_count);
??vApplicationGetTimerTaskMemory_23:
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+24
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
//  312 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+16
        ; ------------------------------------- Block: 27 cycles
??vApplicationGetTimerTaskMemory_17:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 223 cycles
//  313 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon1
          CFI Function _R_OS_SendMsgFromISR
        CODE
//  314 void R_OS_SendMsgFromISR(r_os_msg_header_t* msg, r_os_id_t mbxId)
//  315 {
_R_OS_SendMsgFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
        XCH       A, X               ;; 1 cycle
        MOV       C, A               ;; 1 cycle
//  316     if (msg == NULL)
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_24  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_24:
        SKNZ                         ;; 4 cycles
        BR        R:??vApplicationGetTimerTaskMemory_25  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  317     {
//  318         return;
//  319     }
//  320     msg->kernel_msghead = NULL;
        CLRB      A                  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  321     if (mailboxes[mbxId].tail)
        MOVW      AX, BC             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x50          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_mailboxes)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_mailboxes)  ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_26  ;; 4 cycles
        ; ------------------------------------- Block: 38 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_26:
        BZ        ??vApplicationGetTimerTaskMemory_27  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  322     {
//  323         mailboxes[mbxId].tail->kernel_msghead = msg;
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 20 cycles
//  324     }
//  325     mailboxes[mbxId].tail = msg;
??vApplicationGetTimerTaskMemory_27:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  326     if (mailboxes[mbxId].head == NULL)
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_28  ;; 4 cycles
        ; ------------------------------------- Block: 35 cycles
        XCHW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??vApplicationGetTimerTaskMemory_28:
        BNZ       ??vApplicationGetTimerTaskMemory_29  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  327     {
//  328         mailboxes[mbxId].head = msg;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        XCHW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        ; ------------------------------------- Block: 15 cycles
//  329     }
//  330     xSemaphoreGiveFromISR(mailboxes[mbxId].sem_count, NULL);
??vApplicationGetTimerTaskMemory_29:
        CLRW      BC                 ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
          CFI FunCall _xQueueGiveFromISR
        CALL      F:_xQueueGiveFromISR  ;; 3 cycles
//  331 }
        ; ------------------------------------- Block: 24 cycles
??vApplicationGetTimerTaskMemory_25:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 175 cycles
//  332 
//  333 
//  334 /******************************************************************************
//  335 * Task Control
//  336 ******************************************************************************/
//  337 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon0
          CFI Function _R_OS_DelayTaskMs
        CODE
//  338 void R_OS_DelayTaskMs(uint32_t delayMs)
//  339 {
_R_OS_DelayTaskMs:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  340     vTaskDelay(pdMS_TO_TICKS(delayMs));
        MOVW      DE, #0x3E8         ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall ?L_MUL_FAST_L03
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        POP       HL                 ;; 1 cycle
          CFI CFA SP+8
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
          CFI FunCall _vTaskDelay
        CALL      F:_vTaskDelay      ;; 3 cycles
//  341 }
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 43 cycles
        ; ------------------------------------- Total: 43 cycles
//  342 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon2
          CFI Function _R_OS_GetTaskId
        CODE
//  343 r_os_id_t R_OS_GetTaskId(void)
//  344 {
_R_OS_GetTaskId:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 6
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+10
//  345     TaskHandle_t current = xTaskGetCurrentTaskHandle();
          CFI FunCall _xTaskGetCurrentTaskHandle
        CALL      F:_xTaskGetCurrentTaskHandle  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  346     for (r_os_id_t i = 0; i < ARRAY_SIZE(tasks); i++)
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
//  347     {
//  348         if (tasks[i].task == current)
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       ES, #BYTE3(_tasks)  ;; 1 cycle
        MOVW      HL, ES:_tasks      ;; 2 cycles
        MOV       A, ES:_tasks+2     ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_30  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_30:
        BNZ       ??vApplicationGetTimerTaskMemory_31  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  349         {
//  350             return i;
??R_OS_GetTaskId_0:
        MOV       A, [SP]            ;; 1 cycle
        BR        S:??vApplicationGetTimerTaskMemory_32  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  351         }
??vApplicationGetTimerTaskMemory_31:
        ONEB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, #LWRD(_tasks+76)  ;; 1 cycle
        MOV       ES, #BYTE3(_tasks)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_33  ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_33:
        BZ        ??R_OS_GetTaskId_0  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, #0x2            ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, #LWRD(_tasks+152)  ;; 1 cycle
        MOV       ES, #BYTE3(_tasks)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_34  ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_34:
        BZ        ??R_OS_GetTaskId_0  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  352     }
//  353     // Note: this is also called from timer handler, which has no task id from us
//  354     return R_OS_INVALID_ID;
        MOV       A, #0xFF           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??vApplicationGetTimerTaskMemory_32:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 108 cycles
//  355 }
//  356 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon2
          CFI Function _R_OS_StartTask
        CODE
//  357 void R_OS_StartTask(r_os_id_t tskId)
//  358 {
_R_OS_StartTask:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  359     vTaskResume(tasks[tskId].task);
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x4C          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_tasks)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_tasks)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vTaskResume
        CALL      F:_vTaskResume     ;; 3 cycles
//  360 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 24 cycles
        ; ------------------------------------- Total: 24 cycles
//  361 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon2
          CFI Function _R_OS_Sleep
          CFI FunCall _R_OS_GetTaskId
        CODE
//  362 r_os_result_t R_OS_Sleep(void)
//  363 {
_R_OS_Sleep:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  364     xSemaphoreTake(tasks[R_OS_GetTaskId()].sleep, portMAX_DELAY);
        CALL      F:_R_OS_GetTaskId  ;; 3 cycles
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+6
        DECW      HL                 ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        CLRW      BC                 ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       C, #0x4C           ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_tasks+4)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_tasks)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _xQueueGenericReceive
        CALL      F:_xQueueGenericReceive  ;; 3 cycles
//  365     return R_OS_RESULT_SUCCESS;
        CLRB      A                  ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 43 cycles
        ; ------------------------------------- Total: 43 cycles
//  366 }
//  367 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon2
          CFI Function _R_OS_SleepMaxTicks
        CODE
//  368 r_os_result_t R_OS_SleepMaxTicks(unsigned ticks)
//  369 {
_R_OS_SleepMaxTicks:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
          CFI FunCall _R_OS_GetTaskId
        ; Auto size: 2
//  370     if (xSemaphoreTake(tasks[R_OS_GetTaskId()].sleep, ticks) == pdTRUE)
        CALL      F:_R_OS_GetTaskId  ;; 3 cycles
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+12
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x4C          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_tasks+4)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_tasks)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
          CFI FunCall _xQueueGenericReceive
        CALL      F:_xQueueGenericReceive  ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+6
        CMPW      AX, #0x1           ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_35  ;; 4 cycles
        ; ------------------------------------- Block: 43 cycles
//  371     {
//  372         return R_OS_RESULT_SUCCESS;
        CLRB      A                  ;; 1 cycle
        BR        S:??vApplicationGetTimerTaskMemory_36  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  373     }
//  374     return R_OS_RESULT_FAIL;
??vApplicationGetTimerTaskMemory_35:
        MOV       A, #0x2            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??vApplicationGetTimerTaskMemory_36:
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 55 cycles
//  375 }
//  376 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon2
          CFI Function _R_OS_WakeupTask
        CODE
//  377 r_os_result_t R_OS_WakeupTask(r_os_id_t tskId)
//  378 {
_R_OS_WakeupTask:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  379     xSemaphoreGive(tasks[tskId].sleep);
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x4C          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_tasks+4)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_tasks)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
//  380     return R_OS_RESULT_SUCCESS;
        CLRB      A                  ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 38 cycles
        ; ------------------------------------- Total: 38 cycles
//  381 }
//  382 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon2
          CFI Function _R_OS_GetTaskHandle
          CFI NoCalls
        CODE
//  383 void* R_OS_GetTaskHandle(r_os_id_t tskId)
//  384 {
_R_OS_GetTaskHandle:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 4
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
//  385     return tskId < ARRAY_SIZE(tasks) ? tasks[tskId].task : NULL;
        CMP       A, #0x3            ;; 1 cycle
        BNC       ??vApplicationGetTimerTaskMemory_37  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      BC, #0x4C          ;; 1 cycle
        MULHU                        ;; 2 cycles
        ADDW      AX, #LWRD(_tasks)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_tasks)  ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        BR        S:??vApplicationGetTimerTaskMemory_38  ;; 3 cycles
        ; ------------------------------------- Block: 16 cycles
??vApplicationGetTimerTaskMemory_37:
        MOV       [SP+0x02], #0x0    ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_38:
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 37 cycles
//  386 }
//  387 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon2
          CFI Function _R_OS_ChangeOwnTaskPriority
        CODE
//  388 void R_OS_ChangeOwnTaskPriority(uint16_t priority)
//  389 {
_R_OS_ChangeOwnTaskPriority:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOVW      BC, AX             ;; 1 cycle
//  390     vTaskPrioritySet(NULL, priority);
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
          CFI FunCall _vTaskPrioritySet
        CALL      F:_vTaskPrioritySet  ;; 3 cycles
//  391 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 12 cycles
//  392 
//  393 /******************************************************************************
//  394 * Timers
//  395 ******************************************************************************/
//  396 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon2
          CFI Function _R_OS_StartCyclic
        CODE
//  397 void R_OS_StartCyclic(r_os_id_t cycId)
//  398 {
_R_OS_StartCyclic:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
          CFI FunCall _xTaskGetTickCount
        ; Auto size: 2
//  399     xTimerStart(timers[cycId], 0);
        CALL      F:_xTaskGetTickCount  ;; 3 cycles
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        ONEW      BC                 ;; 1 cycle
        MOV       A, [SP+0x0D]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_semaphores+4)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_semaphores)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTimerGenericCommand
        CALL      F:_xTimerGenericCommand  ;; 3 cycles
//  400 }
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 36 cycles
        ; ------------------------------------- Total: 36 cycles
//  401 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon2
          CFI Function _R_OS_StopCyclic
        CODE
//  402 void R_OS_StopCyclic(r_os_id_t cycId)
//  403 {
_R_OS_StopCyclic:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  404     xTimerStop(timers[cycId], 0);
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      BC, #0x3           ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_semaphores+4)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_semaphores)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTimerGenericCommand
        CALL      F:_xTimerGenericCommand  ;; 3 cycles
//  405 }
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 31 cycles
        ; ------------------------------------- Total: 31 cycles
//  406 
//  407 
//  408 /******************************************************************************
//  409 * Event Flags
//  410 ******************************************************************************/
//  411 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock18 Using cfiCommon0
          CFI Function _R_OS_SetFlag
        CODE
//  412 void R_OS_SetFlag(r_os_id_t id, uint16_t bits)
//  413 {
_R_OS_SetFlag:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  414     xEventGroupSetBits(events[id], bits);
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        POP       DE                 ;; 1 cycle
          CFI CFA SP+4
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_events)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_events)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xEventGroupSetBits
        CALL      F:_xEventGroupSetBits  ;; 3 cycles
//  415 }
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock18
        ; ------------------------------------- Block: 28 cycles
        ; ------------------------------------- Total: 28 cycles
//  416 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock19 Using cfiCommon0
          CFI Function _R_OS_SetFlagFromISR
        CODE
//  417 void R_OS_SetFlagFromISR(r_os_id_t id, uint16_t bits)
//  418 {
_R_OS_SetFlagFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOV       D, A               ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
//  419     /* xHigherPriorityTaskWoken must be initialised to pdFALSE. */
//  420     BaseType_t xHigherPriorityTaskWoken = pdFALSE;
        MOVW      HL, #0x0           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
//  421     xEventGroupSetBitsFromISR(events[id], bits, &xHigherPriorityTaskWoken);
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, D               ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_events)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_events)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _xEventGroupSetBitsFromISR
        CALL      F:_xEventGroupSetBitsFromISR  ;; 3 cycles
//  422 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock19
        ; ------------------------------------- Block: 47 cycles
        ; ------------------------------------- Total: 47 cycles
//  423 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock20 Using cfiCommon3
          CFI Function _R_OS_WaitForFlag
        CODE
//  424 r_os_result_t R_OS_WaitForFlag(r_os_id_t id, uint16_t bits, int clearOnExit, int waitForAll, uint32_t timeOutMs, uint16_t* resultBits)
//  425 {
_R_OS_WaitForFlag:
        ; * Stack frame (at entry) *
        ; Param size: 10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
//  426     EventBits_t res = xEventGroupWaitBits(events[id], bits, clearOnExit ? pdTRUE : pdFALSE, waitForAll ? pdTRUE : pdFALSE, pdMS_TO_TICKS(timeOutMs));
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ONEW      AX                 ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 7 cycles
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??R_OS_WaitForFlag_0:
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ONEW      AX                 ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 5 cycles
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??R_OS_WaitForFlag_1:
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      DE, #0x3E8         ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
          CFI FunCall ?L_MUL_FAST_L03
        CALL      N:?L_MUL_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        DIVWU                        ;; 17 cycles
        NOP                          ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        CLRW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x13]       ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_events)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_events)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xEventGroupWaitBits
        CALL      F:_xEventGroupWaitBits  ;; 3 cycles
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
//  427     *resultBits = (uint16_t)res;
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  428     if (waitForAll)
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_39  ;; 4 cycles
        ; ------------------------------------- Block: 84 cycles
//  429     {
//  430         return (res & bits) == bits ? R_OS_RESULT_SUCCESS : R_OS_RESULT_TIMEOUT;
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
          CFI FunCall ?L_AND_FAST_L03
        CALL      N:?L_AND_FAST_L03  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
        BNZ       ??vApplicationGetTimerTaskMemory_40  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        BR        S:??vApplicationGetTimerTaskMemory_41  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  431     }
//  432     return (res & bits) ? R_OS_RESULT_SUCCESS : R_OS_RESULT_TIMEOUT;
??vApplicationGetTimerTaskMemory_39:
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        AND       A, H               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, L               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_40  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
??vApplicationGetTimerTaskMemory_41:
        CLRB      A                  ;; 1 cycle
        BR        S:??vApplicationGetTimerTaskMemory_42  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??vApplicationGetTimerTaskMemory_40:
        MOV       A, #0x3            ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??vApplicationGetTimerTaskMemory_42:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock20
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 148 cycles
//  433 }
//  434 
//  435 
//  436 /******************************************************************************
//  437 * Semaphores
//  438 ******************************************************************************/
//  439 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock21 Using cfiCommon2
          CFI Function _R_OS_PostSemaphore
        CODE
//  440 void R_OS_PostSemaphore(r_os_id_t id)
//  441 {
_R_OS_PostSemaphore:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  442     xSemaphoreGive(semaphores[id]);
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_semaphores)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_semaphores)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
//  443 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock21
        ; ------------------------------------- Block: 35 cycles
        ; ------------------------------------- Total: 35 cycles

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock22 Using cfiCommon2
          CFI Function _R_OS_WaitSemaphore
        CODE
//  444 void R_OS_WaitSemaphore(r_os_id_t id)
//  445 {
_R_OS_WaitSemaphore:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  446     xSemaphoreTake(semaphores[id], portMAX_DELAY);
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+6
        DECW      HL                 ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        CLRW      BC                 ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        SHLW      AX, 0x2            ;; 1 cycle
        ADDW      AX, #LWRD(_semaphores)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_semaphores)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+10
          CFI FunCall _xQueueGenericReceive
        CALL      F:_xQueueGenericReceive  ;; 3 cycles
//  447 }
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock22
        ; ------------------------------------- Block: 37 cycles
        ; ------------------------------------- Total: 37 cycles
//  448 
//  449 
//  450 /******************************************************************************
//  451 * Initialization
//  452 ******************************************************************************/
//  453 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock23 Using cfiCommon2
          CFI Function _R_OS_Init
        CODE
//  454 r_os_result_t R_OS_Init()
//  455 {
_R_OS_Init:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 18
        SUBW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+22
//  456     // mailboxes
//  457     for (size_t i = 0; i < ARRAY_SIZE(mailboxes); i++)
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
??R_OS_Init_0:
        MOVW      AX, [SP]           ;; 1 cycle
        INCW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x50          ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        CMPW      AX, #0x4           ;; 1 cycle
        BNC       ??vApplicationGetTimerTaskMemory_43  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  458     {
//  459 #if !defined R_HYBRID_PLC_RF || (defined R_HYBRID_PLC_RF & !defined R_MODEM_CLIENT)
//  460         if (i != ID_mac_mbx)  // MAC mailbox uses ROA implementation
        OR        A, X               ;; 1 cycle
        BZ        ??R_OS_Init_0      ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  461 #endif
//  462         {
//  463             mailboxes[i].sem_count = xSemaphoreCreateCountingStatic(0xfff0, 0, &mailboxes[i].semaphore_buffer_count);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #LWRD(_mailboxes)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_mailboxes)  ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        CLRW      BC                 ;; 1 cycle
        MOVW      AX, #0xFFF0        ;; 1 cycle
          CFI FunCall _xQueueCreateCountingSemaphoreStatic
        CALL      F:_xQueueCreateCountingSemaphoreStatic  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+22
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  464             if (mailboxes[i].sem_count == NULL)
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_44  ;; 4 cycles
        ; ------------------------------------- Block: 55 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_44:
        BNZ       ??R_OS_Init_0      ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  465             {
//  466                 return R_OS_RESULT_FAIL;
//  467             }
//  468         }
//  469     }
//  470 
//  471     // events
//  472     // The number of events is configured to be 1 for WiSUN
//  473     // PRQA S 2877 2
//  474     for (size_t i = 0; i < ARRAY_SIZE(events); i++)
//  475     {
//  476         events[i] = xEventGroupCreateStatic(&eventBuffers[i]);
//  477         if (events[i] == NULL)
//  478         {
//  479             return R_OS_RESULT_FAIL;
//  480         }
//  481     }
//  482 
//  483     // semaphores
//  484     // The number of semaphores is configured to be 1 for WiSUN
//  485     // PRQA S 2877 2
//  486     for (size_t i = 0; i < ARRAY_SIZE(semaphore_defs); i++)
//  487     {
//  488         semaphores[i] = xSemaphoreCreateCountingStatic(semaphore_defs[i].max, semaphore_defs[i].initial, &semaphore_buffers[i]);
//  489         if (semaphores[i] == NULL)
//  490         {
//  491             return R_OS_RESULT_FAIL;
//  492         }
//  493     }
//  494 
//  495     // timers
//  496     // The number of timers is configured to be 1 for WiSUN
//  497     // PRQA S 2877 2
//  498     for (size_t i = 0; i < ARRAY_SIZE(timer_defs); i++)
//  499     {
//  500         timers[i] = xTimerCreateStatic("",
//  501                                        pdMS_TO_TICKS(timer_defs[i].period_millis),
//  502                                        timer_defs[i].oneshot ? pdFALSE : pdTRUE,
//  503                                        (void*)i,
//  504                                        timer_defs[i].func,
//  505                                        &timer_buffers[i]);
//  506         if (timers[i] == NULL)
//  507         {
//  508             return R_OS_RESULT_FAIL;
//  509         }
//  510     }
//  511 
//  512     // tasks
//  513     for (size_t i = 0; i < ARRAY_SIZE(task_defs); i++)
//  514     {
//  515         tasks[i].task = xTaskCreateStatic(task_defs[i].func,
//  516                                           task_defs[i].name,
//  517                                           task_defs[i].stack,
//  518                                           NULL,
//  519                                           task_defs[i].priority,
//  520                                           task_defs[i].stack_storage,
//  521                                           &task_buffers[i]);
//  522         if (tasks[i].task == NULL)
//  523         {
//  524             return R_OS_RESULT_FAIL;
//  525         }
//  526         tasks[i].sleep = xSemaphoreCreateCountingStatic(10, 0, &tasks[i].semaphore_buffer_sleep);
//  527         if (tasks[i].sleep == NULL)
//  528         {
//  529             return R_OS_RESULT_FAIL;
??R_OS_Init_1:
        MOV       A, #0x2            ;; 1 cycle
        BR        R:??vApplicationGetTimerTaskMemory_45  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  530         }
??vApplicationGetTimerTaskMemory_43:
        MOVW      DE, #LWRD(_semaphores+8)  ;; 1 cycle
        MOV       A, #BYTE3(_semaphores)  ;; 1 cycle
          CFI FunCall _xEventGroupCreateStatic
        CALL      F:_xEventGroupCreateStatic  ;; 3 cycles
        MOVW      HL, #LWRD(_events)  ;; 1 cycle
        MOV       ES, #BYTE3(_events)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        XCHW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, ES:_events     ;; 2 cycles
        MOV       A, ES:_events+2    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_46  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
        XCHW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??vApplicationGetTimerTaskMemory_46:
        BZ        ??R_OS_Init_1      ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       X, #BYTE3(_semaphores)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, #LWRD(_semaphores+32)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        ONEW      BC                 ;; 1 cycle
        ONEW      AX                 ;; 1 cycle
          CFI FunCall _xQueueCreateCountingSemaphoreStatic
        CALL      F:_xQueueCreateCountingSemaphoreStatic  ;; 3 cycles
        MOVW      HL, #LWRD(_semaphores)  ;; 1 cycle
        MOV       ES, #BYTE3(_semaphores)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        XCHW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, ES:_semaphores  ;; 2 cycles
        MOV       A, ES:_semaphores+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_47  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
        XCHW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??vApplicationGetTimerTaskMemory_47:
        BZ        ??R_OS_Init_1      ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       X, #BYTE3(_semaphores)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, #LWRD(_semaphores+100)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       X, #BYTE3(_R_TIMER_Handle)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, #LWRD(_R_TIMER_Handle)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       X, #0x64           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        ONEW      BC                 ;; 1 cycle
        MOVW      DE, #LWRD(_task_defs+48)  ;; 1 cycle
        MOV       A, #BYTE3(_task_defs)  ;; 1 cycle
          CFI FunCall _xTimerCreateStatic
        CALL      F:_xTimerCreateStatic  ;; 3 cycles
        MOVW      HL, #LWRD(_semaphores+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_semaphores)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_48  ;; 4 cycles
        ; ------------------------------------- Block: 38 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_48:
        SKNZ                         ;; 4 cycles
        BR        R:??R_OS_Init_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      DE, #0x0           ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, #0x3            ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
??R_OS_Init_2:
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #LWRD(_task_defs)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_task_defs)  ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #LWRD(_semaphores+140)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #BYTE3(_semaphores)  ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xE           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CLRW      BC                 ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+38
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+40
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+42
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+42
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+44
        MOV       A, [SP+0x1E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+42
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+40
          CFI FunCall _xTaskCreateStatic
        CALL      F:_xTaskCreateStatic  ;; 3 cycles
        ADDW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+22
        MOV       [SP+0x10], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #LWRD(_tasks)  ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, #BYTE3(_tasks)  ;; 1 cycle
        MOV       [SP+0x0C], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, X               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_49  ;; 4 cycles
        ; ------------------------------------- Block: 124 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_49:
        SKNZ                         ;; 4 cycles
        BR        R:??R_OS_Init_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+26
        CLRW      BC                 ;; 1 cycle
        MOVW      AX, #0xA           ;; 1 cycle
          CFI FunCall _xQueueCreateCountingSemaphoreStatic
        CALL      F:_xQueueCreateCountingSemaphoreStatic  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+22
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x10], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+24
        POP       BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??vApplicationGetTimerTaskMemory_50  ;; 4 cycles
        ; ------------------------------------- Block: 48 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??vApplicationGetTimerTaskMemory_50:
        SKNZ                         ;; 4 cycles
        BR        R:??R_OS_Init_1    ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  531         if (task_defs[i].suspended)
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0xF           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        CMP0      A                  ;; 1 cycle
        BZ        ??vApplicationGetTimerTaskMemory_51  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  532         {
//  533             vTaskSuspend(tasks[i].task);  // will be started with R_OS_StartTask, not with R_OS_WakeupTask
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vTaskSuspend
        CALL      F:_vTaskSuspend    ;; 3 cycles
        ; ------------------------------------- Block: 14 cycles
//  534         }
//  535     }
??vApplicationGetTimerTaskMemory_51:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4C          ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??R_OS_Init_2    ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
//  536     return R_OS_RESULT_SUCCESS;
        CLRB      A                  ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??vApplicationGetTimerTaskMemory_45:
        ADDW      SP, #0x12          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock23
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 439 cycles
//  537 }
//  538 
//  539 // from https://www.freertos.org/a00110.html (description of configSUPPORT_STATIC_ALLOCATION)

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock24 Using cfiCommon1
          CFI Function _vApplicationGetIdleTaskMemory
          CFI NoCalls
        CODE
//  540 void vApplicationGetIdleTaskMemory(StaticTask_t** ppxIdleTaskTCBBuffer,
//  541                                    StackType_t**  ppxIdleTaskStackBuffer,
//  542                                    uint32_t*      pulIdleTaskStackSize)
//  543 {
_vApplicationGetIdleTaskMemory:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 8
//  544     static StaticTask_t xIdleTaskTCB;
//  545     static StackType_t uxIdleTaskStack[configMINIMAL_STACK_SIZE];
//  546     *ppxIdleTaskTCBBuffer = &xIdleTaskTCB;
        MOVW      HL, #LWRD(`vApplicationGetIdleTaskMemory::xIdleTaskTCB`)  ;; 1 cycle
        MOV       A, #BYTE3(`vApplicationGetIdleTaskMemory::xIdleTaskTCB`)  ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        XCHW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
//  547     *ppxIdleTaskStackBuffer = uxIdleTaskStack;
        MOVW      HL, #LWRD(`vApplicationGetIdleTaskMemory::xIdleTaskTCB`+76)  ;; 1 cycle
        MOV       A, #BYTE3(`vApplicationGetIdleTaskMemory::xIdleTaskTCB`)  ;; 1 cycle
          CFI EndBlock cfiBlock24
        ; ------------------------------------- Block: 26 cycles
        ; ------------------------------------- Total: 26 cycles
        REQUIRE ?Subroutine0
        ; // Fall through to label ?Subroutine0
//  548     *pulIdleTaskStackSize = configMINIMAL_STACK_SIZE;
//  549 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock25 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+12
        CODE
?Subroutine0:
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, B               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, #0x230         ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock25
        ; ------------------------------------- Block: 32 cycles
        ; ------------------------------------- Total: 32 cycles

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
`vApplicationGetIdleTaskMemory::xIdleTaskTCB`:
        DS 76
        DS 1120
//  550 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock26 Using cfiCommon1
          CFI Function _vApplicationGetTimerTaskMemory
          CFI NoCalls
        CODE
//  551 void vApplicationGetTimerTaskMemory(StaticTask_t** ppxTimerTaskTCBBuffer,
//  552                                     StackType_t**  ppxTimerTaskStackBuffer,
//  553                                     uint32_t*      pulTimerTaskStackSize)
//  554 {
_vApplicationGetTimerTaskMemory:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 8
//  555     static StaticTask_t xTimerTaskTCB;
//  556     static StackType_t uxTimerTaskStack[configTIMER_TASK_STACK_DEPTH];
//  557     *ppxTimerTaskTCBBuffer = &xTimerTaskTCB;
        MOVW      HL, #LWRD(`vApplicationGetTimerTaskMemory::xTimerTaskTCB`)  ;; 1 cycle
        MOV       A, #BYTE3(`vApplicationGetTimerTaskMemory::xTimerTaskTCB`)  ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        XCHW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
//  558     *ppxTimerTaskStackBuffer = uxTimerTaskStack;
        MOVW      HL, #LWRD(`vApplicationGetTimerTaskMemory::xTimerTaskTCB`+76)  ;; 1 cycle
        MOV       A, #BYTE3(`vApplicationGetTimerTaskMemory::xTimerTaskTCB`)  ;; 1 cycle
        BR        F:?Subroutine0     ;; 3 cycles
          CFI EndBlock cfiBlock26
        ; ------------------------------------- Block: 29 cycles
        ; ------------------------------------- Total: 29 cycles
//  559     *pulTimerTaskStackSize = configTIMER_TASK_STACK_DEPTH;
//  560 }

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
`vApplicationGetTimerTaskMemory::xTimerTaskTCB`:
        DS 76
        DS 1120

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_0:
        DB "MAC_TSK"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_1:
        DB "NWK_TSK"

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_2:
        DB "APL_TSK"

        END
// 
// 29 644 bytes in section .bssf
//     73 bytes in section .constf
//     36 bytes in section .dataf
//  2 543 bytes in section .textf
// 
// 29 680 bytes of DATA    memory
//  2 616 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
