///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:56
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\port.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW35F9.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\port.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\port.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHT_IAR_NOINIT 0xabdc5467
        #define SHF_WRITE 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _vPortStartFirstTask

        PUBLIC __A_IF1
        PUBLIC __A_MK1
        PUBLIC __A_PER0
        PUBLIC __A_PR01
        PUBLIC __A_PR11
        PUBLIC __A_TDR03
        PUBLIC __A_TMR03
        PUBLIC __A_TOE0
        PUBLIC __A_TOL0
        PUBLIC __A_TOM0
        PUBLIC __A_TPS0
        PUBLIC __A_TS0
        PUBLIC __A_TT0
        PUBLIC _pxPortInitialiseStack
        PUBLIC _usCriticalNesting
        PUBLIC _vPortEndScheduler
        PUBLIC _xPortStartScheduler
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\port.c
//    1 /*
//    2  * FreeRTOS Kernel V10.0.0
//    3  * Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
//    4  *
//    5  * Permission is hereby granted, free of charge, to any person obtaining a copy of
//    6  * this software and associated documentation files (the "Software"), to deal in
//    7  * the Software without restriction, including without limitation the rights to
//    8  * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
//    9  * the Software, and to permit persons to whom the Software is furnished to do so,
//   10  * subject to the following conditions:
//   11  *
//   12  * The above copyright notice and this permission notice shall be included in all
//   13  * copies or substantial portions of the Software. If you wish to use our Amazon
//   14  * FreeRTOS name, please do so in a fair use way that does not cause confusion.
//   15  *
//   16  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
//   17  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
//   18  * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
//   19  * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
//   20  * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
//   21  * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//   22  *
//   23  * http://www.FreeRTOS.org
//   24  * http://aws.amazon.com/freertos
//   25  *
//   26  * 1 tab == 4 spaces!
//   27  */
//   28 
//   29 /* Scheduler includes. */
//   30 #include "FreeRTOS.h"
//   31 #include "task.h"
//   32 
//   33 #include <ior5f11fll.h>

        ASEGN `.bss.noinit`:DATA:NOROOT,0fff66H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TDR03
// union <unnamed>#83 volatile __sfr __no_bit_access _A_TDR03
__A_TDR03:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe2H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_IF1
// union <unnamed>#166 volatile __sfr _A_IF1
__A_IF1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe6H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_MK1
// union <unnamed>#181 volatile __sfr _A_MK1
__A_MK1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffeaH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR01
// union <unnamed>#196 volatile __sfr _A_PR01
__A_PR01:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffeeH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR11
// union <unnamed>#211 volatile __sfr _A_PR11
__A_PR11:
        DS 2
//   34 #include <ior5f11fll_ext.h>

        ASEGN `.bss.noinit`:DATA:NOROOT,0f00f0H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PER0
// union <unnamed>#264 volatile __near _A_PER0
__A_PER0:
        DS 1

        ASEGN `.bss.noinit`:DATA:NOROOT,0f0196H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TMR03
// union <unnamed>#363 volatile __near __no_bit_access _A_TMR03
__A_TMR03:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01b2H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TS0
// union <unnamed>#379 volatile __near _A_TS0
__A_TS0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01b4H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TT0
// union <unnamed>#382 volatile __near _A_TT0
__A_TT0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01b6H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TPS0
// union <unnamed>#385 volatile __near __no_bit_access _A_TPS0
__A_TPS0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01baH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TOE0
// union <unnamed>#389 volatile __near _A_TOE0
__A_TOE0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01bcH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TOL0
// union <unnamed>#392 volatile __near __no_bit_access _A_TOL0
__A_TOL0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01beH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TOM0
// union <unnamed>#395 volatile __near __no_bit_access _A_TOM0
__A_TOM0:
        DS 2
//   35 #include "hardware.h"
//   36 
//   37 /* RM_OSTICKmS macro defines the number of ms per OS tick */
//   38 #define RM_OSTICKmS                     (1)
//   39 
//   40 /* The critical nesting value is initialised to a non zero value to ensure
//   41 interrupts don't accidentally become enabled before the scheduler is started. */
//   42 #define portINITIAL_CRITICAL_NESTING  ( ( uint16_t ) 10 )
//   43 
//   44 /* Initial PSW value allocated to a newly created task.
//   45  *   1100011000000000
//   46  *   ||||||||-------------- Fill byte
//   47  *   |||||||--------------- Carry Flag cleared
//   48  *   |||||----------------- In-service priority Flags set to low level
//   49  *   ||||------------------ Register bank Select 0 Flag cleared
//   50  *   |||------------------- Auxiliary Carry Flag cleared
//   51  *   ||-------------------- Register bank Select 1 Flag cleared
//   52  *   |--------------------- Zero Flag set
//   53  *   ---------------------- Global Interrupt Flag set (enabled)
//   54  */
//   55 #define portPSW		  ( 0xc6UL )
//   56 
//   57 /* The address of the pxCurrentTCB variable, but don't know or need to know its
//   58 type. */
//   59 typedef void TCB_t;
//   60 extern volatile TCB_t * volatile pxCurrentTCB;
//   61 
//   62 /* Each task maintains a count of the critical section nesting depth.  Each time
//   63 a critical section is entered the count is incremented.  Each time a critical
//   64 section is exited the count is decremented - with interrupts only being
//   65 re-enabled if the count is zero.
//   66 
//   67 usCriticalNesting will get set to zero when the scheduler starts, but must
//   68 not be initialised to zero as that could cause problems during the startup
//   69 sequence. */

        SECTION `.dataf`:DATA:REORDER:NOROOT(1)
//   70 volatile uint16_t usCriticalNesting = portINITIAL_CRITICAL_NESTING;
_usCriticalNesting:
        DATA16
        DW 10
//   71 
//   72 /*-----------------------------------------------------------*/
//   73 
//   74 /*
//   75  * Sets up the periodic ISR used for the RTOS tick using the interval timer.
//   76  * The application writer can define configSETUP_TICK_INTERRUPT() (in
//   77  * FreeRTOSConfig.h) such that their own tick interrupt configuration is used
//   78  * in place of prvSetupTimerInterrupt().
//   79  */
//   80 static void prvSetupTimerInterrupt( void );
//   81 #ifndef configSETUP_TICK_INTERRUPT
//   82 	/* The user has not provided their own tick interrupt configuration so use
//   83     the definition in this file (which uses the interval timer). */
//   84 	#define configSETUP_TICK_INTERRUPT() prvSetupTimerInterrupt()
//   85 #endif /* configSETUP_TICK_INTERRUPT */
//   86 
//   87 /*
//   88  * Defined in portasm.s87, this function starts the scheduler by loading the
//   89  * context of the first task to run.
//   90  */
//   91 extern void vPortStartFirstTask( void );
//   92 
//   93 /*
//   94  * Used to catch tasks that attempt to return from their implementing function.
//   95  */
//   96 static void prvTaskExitError( void );
//   97 
//   98 /*-----------------------------------------------------------*/
//   99 
//  100 /*
//  101  * Initialise the stack of a task to look exactly as if a call to
//  102  * portSAVE_CONTEXT had been called.
//  103  *
//  104  * See the header file portable.h.
//  105  */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _pxPortInitialiseStack
          CFI NoCalls
        CODE
//  106 StackType_t *pxPortInitialiseStack( StackType_t *pxTopOfStack, TaskFunction_t pxCode, void *pvParameters )
//  107 {
_pxPortInitialiseStack:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 12
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
//  108 uint32_t *pulLocal;
//  109 uint32_t  addrParameters = (uint32_t) pvParameters;
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        CLRB      X                  ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  110 
//  111 	/* With large code and large data sizeof( StackType_t ) == 2, and
//  112 	sizeof( StackType_t * ) == 4.  With small code and small data
//  113 	sizeof( StackType_t ) == 2 and sizeof( StackType_t * ) == 2. */
//  114 
//  115         /*!!! IAR V2 calling conventions                !!!*/
//  116         /*!!! far pointer is parsed via X:BC register   !!!*/
//  117 
//  118         #if __DATA_MODEL__ == __DATA_MODEL_FAR__
//  119 	{
//  120 		/* The return address, leaving space for the first two bytes of	the
//  121 		32-bit value.  See the comments above the prvTaskExitError() prototype
//  122 		at the top of this file. */
//  123 		pxTopOfStack--;
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xFFFE        ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  124 		pulLocal = ( uint32_t * ) pxTopOfStack;
//  125 		*pulLocal = ( uint32_t ) prvTaskExitError;
        MOVW      AX, #LWRD(_prvTaskExitError)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, #HWRD(_prvTaskExitError)  ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  126 		pxTopOfStack--;
//  127 
//  128 		/* The start address / PSW value is also written in as a 32-bit value,
//  129 		so leave a space for the second two bytes. */
//  130 		pxTopOfStack--;
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCHW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  131 
//  132 		/* Task function start address combined with the PSW. */
//  133 		pulLocal = ( uint32_t * ) pxTopOfStack;
//  134 		*pulLocal = ( ( ( uint32_t ) pxCode ) | ( portPSW << 24UL ) );
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        CLRB      X                  ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+16
        MOV       B, #0xC6           ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  135 		pxTopOfStack--;
//  136 
//  137 		/* An initial value for the AX register. */
//  138                 *pxTopOfStack = ( StackType_t ) ((addrParameters >> 8) & 0xff00);
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        CLRB      X                  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xFFFE        ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  139 		pxTopOfStack--;
//  140 
//  141                 /* An initial value for the HL register. */
//  142                 *pxTopOfStack = ( StackType_t ) 0x2222;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xFFFC        ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, #0x2222        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  143                 pxTopOfStack--;
//  144 
//  145                 /* CS and ES registers. */
//  146                 *pxTopOfStack = ( StackType_t ) 0x0F00;
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      AX, #0xF00         ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  147                 pxTopOfStack--;
//  148 
//  149                 /* The remaining general purpose registers DE and BC */
//  150                 *pxTopOfStack = ( StackType_t ) (addrParameters & 0xffff) ;
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xFFF8        ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  151                 pxTopOfStack--;
//  152                 *pxTopOfStack = ( StackType_t ) 0xBCBC;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xFFF6        ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, #0xBCBC        ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  153                 pxTopOfStack--;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xFFF4        ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  154 	}
//  155 	#else
//  156 	{
//  157 		/* The return address, leaving space for the first two bytes of	the
//  158 		32-bit value.  See the comments above the prvTaskExitError() prototype
//  159 		at the top of this file. */
//  160 		pxTopOfStack--;
//  161 		pulLocal = ( uint32_t * ) pxTopOfStack;
//  162 		*pulLocal = ( uint32_t ) prvTaskExitError;
//  163 		pxTopOfStack--;
//  164 
//  165 		/* Task function.  Again as it is written as a 32-bit value a space is
//  166 		left on the stack for the second two bytes. */
//  167 		pxTopOfStack--;
//  168 
//  169 		/* Task function start address combined with the PSW. */
//  170 		pulLocal = ( uint32_t * ) pxTopOfStack;
//  171 		*pulLocal = ( ( ( uint32_t ) pxCode ) | ( portPSW << 24UL ) );
//  172 		pxTopOfStack--;
//  173 
//  174 		/* The parameter is passed in AX. */
//  175 		*pxTopOfStack = ( StackType_t ) pvParameters;
//  176 		pxTopOfStack--;
//  177 
//  178                 /* An initial value for the HL register. */
//  179                 *pxTopOfStack = ( StackType_t ) 0x2222;
//  180                 pxTopOfStack--;
//  181 
//  182                 /* CS and ES registers. */
//  183                 *pxTopOfStack = ( StackType_t ) 0x0F00;
//  184                 pxTopOfStack--;
//  185 
//  186                 /* The remaining general purpose registers DE and BC */
//  187                 *pxTopOfStack = ( StackType_t ) 0xDEDE;
//  188                 pxTopOfStack--;
//  189                 *pxTopOfStack = ( StackType_t ) 0xBCBC;
//  190                 pxTopOfStack--;
//  191 	}
//  192 	#endif
//  193 
//  194 	/* Finally the critical section nesting count is set to zero when the task
//  195 	first starts. */
//  196 	*pxTopOfStack = ( StackType_t ) portNO_CRITICAL_SECTION_NESTING;
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  197 
//  198 	/* Return a pointer to the top of the stack that has been generated so it
//  199 	can	be stored in the task control block for the task. */
//  200 	return pxTopOfStack;
        MOV       A, [SP+0x0A]       ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+18
        POP       DE                 ;; 1 cycle
          CFI CFA SP+16
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 118 cycles
        ; ------------------------------------- Total: 118 cycles
//  201 }
//  202 /*-----------------------------------------------------------*/
//  203 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon1
          CFI Function _prvTaskExitError
          CFI NoCalls
        CODE
//  204 static void prvTaskExitError( void )
//  205 {
_prvTaskExitError:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  206 	/* A function that implements a task must not exit or attempt to return to
//  207 	its caller as there is nothing to return to.  If a task wants to exit it
//  208 	should instead call vTaskDelete( NULL ).
//  209 
//  210 	Artificially force an assert() to be triggered if configASSERT() is
//  211 	defined, then stop here so application writers can catch the error. */
//  212 	configASSERT( usCriticalNesting == ~0U );
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0xFFFF        ;; 1 cycle
        BZ        ??prvSetupTimerInterrupt_0  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvSetupTimerInterrupt_1  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvSetupTimerInterrupt_1:
        BR        S:??prvSetupTimerInterrupt_1  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  213 	portDISABLE_INTERRUPTS();
??prvSetupTimerInterrupt_0:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvSetupTimerInterrupt_2  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  214 	for( ;; );
??prvSetupTimerInterrupt_2:
        BR        S:??prvSetupTimerInterrupt_2  ;; 3 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 45 cycles
//  215 }
//  216 /*-----------------------------------------------------------*/
//  217 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _xPortStartScheduler
          CFI FunCall _prvSetupTimerInterrupt
        CODE
//  218 BaseType_t xPortStartScheduler( void )
//  219 {
_xPortStartScheduler:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  220 	/* Setup the hardware to generate the tick.  Interrupts are disabled when
//  221 	this function is called. */
//  222 	configSETUP_TICK_INTERRUPT();
        CALL      F:_prvSetupTimerInterrupt  ;; 3 cycles
//  223 
//  224 	/* Restore the context of the first task that is going to run. */
//  225 	vPortStartFirstTask();
          CFI FunCall _vPortStartFirstTask
        CALL      F:_vPortStartFirstTask  ;; 3 cycles
//  226 
//  227 	/* Execution should not reach here as the tasks are now running!
//  228 	prvSetupTimerInterrupt() is called here to prevent the compiler outputting
//  229 	a warning about a statically declared function not being referenced in the
//  230 	case that the application writer has provided their own tick interrupt
//  231 	configuration routine (and defined configSETUP_TICK_INTERRUPT() such that
//  232 	their own routine will be called in place of prvSetupTimerInterrupt()). */
//  233 	prvSetupTimerInterrupt();
          CFI FunCall _prvSetupTimerInterrupt
        CALL      F:_prvSetupTimerInterrupt  ;; 3 cycles
//  234 	return pdTRUE;
        ONEW      AX                 ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 16 cycles
        ; ------------------------------------- Total: 16 cycles
//  235 }
//  236 /*-----------------------------------------------------------*/
//  237 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon1
          CFI Function _vPortEndScheduler
          CFI NoCalls
        CODE
//  238 void vPortEndScheduler( void )
//  239 {
_vPortEndScheduler:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  240 	/* It is unlikely that the RL78 port will get stopped. */
//  241 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 6 cycles
//  242 /*-----------------------------------------------------------*/
//  243 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon1
          CFI Function _prvSetupTimerInterrupt
          CFI NoCalls
        CODE
//  244 static void prvSetupTimerInterrupt( void )
//  245 {
_prvSetupTimerInterrupt:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  246 	TAU0EN = 1;			/* supplies input clock */
        SET1      0xF00F0.0          ;; 2 cycles
//  247 
//  248 	TPS0 = TAU_TPS0_INITIALVALUE;
        CLRW      AX                 ;; 1 cycle
        MOVW      0x1B6, AX          ;; 1 cycle
//  249 #if RP_CPU_CLK == 32
//  250 	TPS0 |= TAU_CK00_FCLK_3;
        MOVW      AX, 0x1B6          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        OR        A, #0x3            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x1B6, AX          ;; 1 cycle
//  251 #elif RP_CPU_CLK == 16
//  252 	TPS0 |= TAU_CK00_FCLK_2;
//  253 #elif RP_CPU_CLK == 8
//  254 	TPS0 |= TAU_CK00_FCLK_1;
//  255 #elif RP_CPU_CLK == 4
//  256 	TPS0 |= TAU_CK00_FCLK_0;
//  257 #endif
//  258 
//  259 	/* Stop all channels */
//  260 	TT0 = TAU_CH0_STOP_TRG_ON | TAU_CH1_STOP_TRG_ON | TAU_CH2_STOP_TRG_ON | TAU_CH3_STOP_TRG_ON;
        MOVW      AX, #0xF           ;; 1 cycle
        MOVW      0x1B4, AX          ;; 1 cycle
//  261 
//  262 /*   Timer settings(16bit Timer03) for uItron */
//  263 	/* Mask channel 3 interrupt */
//  264 	TMMK03 = 1;		/* INTTM03 disabled */
        SET1      0xFFFE6.7          ;; 2 cycles
//  265 	TMIF03 = 0;		/* INTTM03 interrupt flag clear */
        CLR1      0xFFFE2.7          ;; 2 cycles
//  266 
//  267 	/* Set INTTM03 low priority */
//  268 	TMPR003 = 1;
        SET1      0xFFFEA.7          ;; 2 cycles
//  269 	TMPR103 = 1;// Level_3
        SET1      0xFFFEE.7          ;; 2 cycles
//  270 
//  271 	/* Channel 3 used as interval timer */
//  272 	TMR03 = TAU_TMR0_INITIALVALUE | TAU_CLOCK_SELECT_CK00 | TAU_CLOCK_MODE_CKS | TAU_MODE_INTERVAL_TIMER | TAU_START_INT_UNUSED;
        CLRW      AX                 ;; 1 cycle
        MOVW      0x196, AX          ;; 1 cycle
//  273 	TDR03 = (unsigned short) (((TAU_TDR00_VALUE + 1) * RM_OSTICKmS) - 1);
        MOVW      0xFFF66, #0xF9F    ;; 1 cycle
//  274 	TOM0 &= ~TAU_CH3_OUTPUT_COMBIN ;
        MOVW      AX, 0x1BE          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xF7           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x1BE, AX          ;; 1 cycle
//  275 	TOL0 &= ~TAU_CH3_OUTPUT_LEVEL_L;
        MOVW      AX, 0x1BC          ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        AND       A, #0xF7           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      0x1BC, AX          ;; 1 cycle
//  276 	TOE0 &= ~TAU_CH3_OUTPUT_ENABLE;
        CLR1      0xF01BA.3          ;; 2 cycles
//  277 
//  278 	TMMK03 = 0;		/* INTTM03 enabled */
        CLR1      0xFFFE6.7          ;; 2 cycles
//  279 	TS0 |= TAU_CH3_START_TRG_ON;
        SET1      0xF01B2.3          ;; 2 cycles
//  280  }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 44 cycles
        ; ------------------------------------- Total: 44 cycles
        REQUIRE __A_PER0
        REQUIRE __A_TPS0
        REQUIRE __A_TT0
        REQUIRE __A_MK1
        REQUIRE __A_IF1
        REQUIRE __A_PR01
        REQUIRE __A_PR11
        REQUIRE __A_TMR03
        REQUIRE __A_TDR03
        REQUIRE __A_TOM0
        REQUIRE __A_TOL0
        REQUIRE __A_TOE0
        REQUIRE __A_TS0

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//  281 /*-----------------------------------------------------------*/
//  282 
// 
//  25 bytes in section .bss.noinit  (abs)
//   2 bytes in section .dataf
// 311 bytes in section .textf
// 
//   2 bytes of DATA    memory (+ 25 bytes shared)
// 311 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
