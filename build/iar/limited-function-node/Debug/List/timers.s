///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:31:01
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\timers.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW423F.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\timers.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\timers.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_full_locale_support", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _usCriticalNesting
        EXTERN ?UL_CMP_L03
        EXTERN _uxListRemove
        EXTERN _vApplicationGetTimerTaskMemory
        EXTERN _vListInitialise
        EXTERN _vListInitialiseItem
        EXTERN _vListInsert
        EXTERN _vQueueWaitForMessageRestricted
        EXTERN _vTaskSuspendAll
        EXTERN _xQueueGenericCreateStatic
        EXTERN _xQueueGenericReceive
        EXTERN _xQueueGenericSend
        EXTERN _xQueueGenericSendFromISR
        EXTERN _xTaskCreateStatic
        EXTERN _xTaskGetSchedulerState
        EXTERN _xTaskGetTickCount
        EXTERN _xTaskResumeAll

        PUBLIC _pcTimerGetName
        PUBLIC _pvTimerGetTimerID
        PUBLIC _vTimerSetTimerID
        PUBLIC _xTimerCreateStatic
        PUBLIC _xTimerCreateTimerTask
        PUBLIC _xTimerGenericCommand
        PUBLIC _xTimerGetExpiryTime
        PUBLIC _xTimerGetPeriod
        PUBLIC _xTimerGetTimerDaemonTaskHandle
        PUBLIC _xTimerIsTimerActive
        PUBLIC _xTimerPendFunctionCall
        PUBLIC _xTimerPendFunctionCallFromISR
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
        
          CFI Common cfiCommon3 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon3
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\timers.c
//    1 /*
//    2     FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
//    3     All rights reserved
//    4 
//    5     VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
//    6 
//    7     This file is part of the FreeRTOS distribution.
//    8 
//    9     FreeRTOS is free software; you can redistribute it and/or modify it under
//   10     the terms of the GNU General Public License (version 2) as published by the
//   11     Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.
//   12 
//   13     ***************************************************************************
//   14     >>!   NOTE: The modification to the GPL is included to allow you to     !<<
//   15     >>!   distribute a combined work that includes FreeRTOS without being   !<<
//   16     >>!   obliged to provide the source code for proprietary components     !<<
//   17     >>!   outside of the FreeRTOS kernel.                                   !<<
//   18     ***************************************************************************
//   19 
//   20     FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
//   21     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//   22     FOR A PARTICULAR PURPOSE.  Full license text is available on the following
//   23     link: http://www.freertos.org/a00114.html
//   24 
//   25     ***************************************************************************
//   26      *                                                                       *
//   27      *    FreeRTOS provides completely free yet professionally developed,    *
//   28      *    robust, strictly quality controlled, supported, and cross          *
//   29      *    platform software that is more than just the market leader, it     *
//   30      *    is the industry's de facto standard.                               *
//   31      *                                                                       *
//   32      *    Help yourself get started quickly while simultaneously helping     *
//   33      *    to support the FreeRTOS project by purchasing a FreeRTOS           *
//   34      *    tutorial book, reference manual, or both:                          *
//   35      *    http://www.FreeRTOS.org/Documentation                              *
//   36      *                                                                       *
//   37     ***************************************************************************
//   38 
//   39     http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
//   40     the FAQ page "My application does not run, what could be wrong?".  Have you
//   41     defined configASSERT()?
//   42 
//   43     http://www.FreeRTOS.org/support - In return for receiving this top quality
//   44     embedded software for free we request you assist our global community by
//   45     participating in the support forum.
//   46 
//   47     http://www.FreeRTOS.org/training - Investing in training allows your team to
//   48     be as productive as possible as early as possible.  Now you can receive
//   49     FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
//   50     Ltd, and the world's leading authority on the world's leading RTOS.
//   51 
//   52     http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
//   53     including FreeRTOS+Trace - an indispensable productivity tool, a DOS
//   54     compatible FAT file system, and our tiny thread aware UDP/IP stack.
//   55 
//   56     http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
//   57     Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.
//   58 
//   59     http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
//   60     Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
//   61     licenses offer ticketed support, indemnification and commercial middleware.
//   62 
//   63     http://www.SafeRTOS.com - High Integrity Systems also provide a safety
//   64     engineered and independently SIL3 certified version for use in safety and
//   65     mission critical applications that require provable dependability.
//   66 
//   67     1 tab == 4 spaces!
//   68 */
//   69 
//   70 /* Standard includes. */
//   71 #include <stdlib.h>
//   72 
//   73 /* Defining MPU_WRAPPERS_INCLUDED_FROM_API_FILE prevents task.h from redefining
//   74 all the API functions to use the MPU wrappers.  That should only be done when
//   75 task.h is included from an application file. */
//   76 #define MPU_WRAPPERS_INCLUDED_FROM_API_FILE
//   77 
//   78 #include "FreeRTOS.h"
//   79 #include "task.h"
//   80 #include "queue.h"
//   81 #include "timers.h"
//   82 
//   83 #if ( INCLUDE_xTimerPendFunctionCall == 1 ) && ( configUSE_TIMERS == 0 )
//   84 	#error configUSE_TIMERS must be set to 1 to make the xTimerPendFunctionCall() function available.
//   85 #endif
//   86 
//   87 /* Lint e961 and e750 are suppressed as a MISRA exception justified because the
//   88 MPU ports require MPU_WRAPPERS_INCLUDED_FROM_API_FILE to be defined for the
//   89 header files above, but not in this file, in order to generate the correct
//   90 privileged Vs unprivileged linkage and placement. */
//   91 #undef MPU_WRAPPERS_INCLUDED_FROM_API_FILE /*lint !e961 !e750. */
//   92 
//   93 
//   94 /* This entire source file will be skipped if the application is not configured
//   95 to include software timer functionality.  This #if is closed at the very bottom
//   96 of this file.  If you want to include software timer functionality then ensure
//   97 configUSE_TIMERS is set to 1 in FreeRTOSConfig.h. */
//   98 #if ( configUSE_TIMERS == 1 )
//   99 
//  100 /* Misc definitions. */
//  101 #define tmrNO_DELAY		( TickType_t ) 0U
//  102 
//  103 /* The definition of the timers themselves. */
//  104 typedef struct tmrTimerControl
//  105 {
//  106 	const char				*pcTimerName;		/*<< Text name.  This is not used by the kernel, it is included simply to make debugging easier. */ /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  107 	ListItem_t				xTimerListItem;		/*<< Standard linked list item as used by all kernel features for event management. */
//  108 	TickType_t				xTimerPeriodInTicks;/*<< How quickly and often the timer expires. */
//  109 	UBaseType_t				uxAutoReload;		/*<< Set to pdTRUE if the timer should be automatically restarted once expired.  Set to pdFALSE if the timer is, in effect, a one-shot timer. */
//  110 	void 					*pvTimerID;			/*<< An ID to identify the timer.  This allows the timer to be identified when the same callback is used for multiple timers. */
//  111 	TimerCallbackFunction_t	pxCallbackFunction;	/*<< The function that will be called when the timer expires. */
//  112 	#if( configUSE_TRACE_FACILITY == 1 )
//  113 		UBaseType_t			uxTimerNumber;		/*<< An ID assigned by trace tools such as FreeRTOS+Trace */
//  114 	#endif
//  115 
//  116 	#if( ( configSUPPORT_STATIC_ALLOCATION == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) )
//  117 		uint8_t 			ucStaticallyAllocated; /*<< Set to pdTRUE if the timer was created statically so no attempt is made to free the memory again if the timer is later deleted. */
//  118 	#endif
//  119 } xTIMER;
//  120 
//  121 /* The old xTIMER name is maintained above then typedefed to the new Timer_t
//  122 name below to enable the use of older kernel aware debuggers. */
//  123 typedef xTIMER Timer_t;
//  124 
//  125 /* The definition of messages that can be sent and received on the timer queue.
//  126 Two types of message can be queued - messages that manipulate a software timer,
//  127 and messages that request the execution of a non-timer related callback.  The
//  128 two message types are defined in two separate structures, xTimerParametersType
//  129 and xCallbackParametersType respectively. */
//  130 typedef struct tmrTimerParameters
//  131 {
//  132 	TickType_t			xMessageValue;		/*<< An optional value used by a subset of commands, for example, when changing the period of a timer. */
//  133 	Timer_t *			pxTimer;			/*<< The timer to which the command will be applied. */
//  134 } TimerParameter_t;
//  135 
//  136 
//  137 typedef struct tmrCallbackParameters
//  138 {
//  139 	PendedFunction_t	pxCallbackFunction;	/* << The callback function to execute. */
//  140 	void *pvParameter1;						/* << The value that will be used as the callback functions first parameter. */
//  141 	uint32_t ulParameter2;					/* << The value that will be used as the callback functions second parameter. */
//  142 } CallbackParameters_t;
//  143 
//  144 /* The structure that contains the two message types, along with an identifier
//  145 that is used to determine which message type is valid. */
//  146 typedef struct tmrTimerQueueMessage
//  147 {
//  148 	BaseType_t			xMessageID;			/*<< The command being sent to the timer service task. */
//  149 	union
//  150 	{
//  151 		TimerParameter_t xTimerParameters;
//  152 
//  153 		/* Don't include xCallbackParameters if it is not going to be used as
//  154 		it makes the structure (and therefore the timer queue) larger. */
//  155 		#if ( INCLUDE_xTimerPendFunctionCall == 1 )
//  156 			CallbackParameters_t xCallbackParameters;
//  157 		#endif /* INCLUDE_xTimerPendFunctionCall */
//  158 	} u;
//  159 } DaemonTaskMessage_t;
//  160 
//  161 /*lint -e956 A manual analysis and inspection has been used to determine which
//  162 static variables must be declared volatile. */
//  163 
//  164 /* The list in which active timers are stored.  Timers are referenced in expire
//  165 time order, with the nearest expiry time at the front of the list.  Only the
//  166 timer service task is allowed to access these lists. */
//  167 PRIVILEGED_DATA static List_t xActiveTimerList1;
//  168 PRIVILEGED_DATA static List_t xActiveTimerList2;
//  169 PRIVILEGED_DATA static List_t *pxCurrentTimerList;
//  170 PRIVILEGED_DATA static List_t *pxOverflowTimerList;
//  171 
//  172 /* A queue that is used to send commands to the timer service task. */
//  173 PRIVILEGED_DATA static QueueHandle_t xTimerQueue = NULL;
//  174 PRIVILEGED_DATA static TaskHandle_t xTimerTaskHandle = NULL;
//  175 
//  176 /*lint +e956 */
//  177 
//  178 /*-----------------------------------------------------------*/
//  179 
//  180 #if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  181 
//  182 	/* If static allocation is supported then the application must provide the
//  183 	following callback function - which enables the application to optionally
//  184 	provide the memory that will be used by the timer task as the task's stack
//  185 	and TCB. */
//  186 	extern void vApplicationGetTimerTaskMemory( StaticTask_t **ppxTimerTaskTCBBuffer, StackType_t **ppxTimerTaskStackBuffer, uint32_t *pulTimerTaskStackSize );
//  187 
//  188 #endif
//  189 
//  190 /*
//  191  * Initialise the infrastructure used by the timer service task if it has not
//  192  * been initialised already.
//  193  */
//  194 static void prvCheckForValidListAndQueue( void ) PRIVILEGED_FUNCTION;
//  195 
//  196 /*
//  197  * The timer service task (daemon).  Timer functionality is controlled by this
//  198  * task.  Other tasks communicate with the timer service task using the
//  199  * xTimerQueue queue.
//  200  */
//  201 static void prvTimerTask( void *pvParameters ) PRIVILEGED_FUNCTION;
//  202 
//  203 /*
//  204  * Called by the timer service task to interpret and process a command it
//  205  * received on the timer queue.
//  206  */
//  207 static void prvProcessReceivedCommands( void ) PRIVILEGED_FUNCTION;
//  208 
//  209 /*
//  210  * Insert the timer into either xActiveTimerList1, or xActiveTimerList2,
//  211  * depending on if the expire time causes a timer counter overflow.
//  212  */
//  213 static BaseType_t prvInsertTimerInActiveList( Timer_t * const pxTimer, const TickType_t xNextExpiryTime, const TickType_t xTimeNow, const TickType_t xCommandTime ) PRIVILEGED_FUNCTION;
//  214 
//  215 /*
//  216  * An active timer has reached its expire time.  Reload the timer if it is an
//  217  * auto reload timer, then call its callback.
//  218  */
//  219 static void prvProcessExpiredTimer( const TickType_t xNextExpireTime, const TickType_t xTimeNow ) PRIVILEGED_FUNCTION;
//  220 
//  221 /*
//  222  * The tick count has overflowed.  Switch the timer lists after ensuring the
//  223  * current timer list does not still reference some timers.
//  224  */
//  225 static void prvSwitchTimerLists( void ) PRIVILEGED_FUNCTION;
//  226 
//  227 /*
//  228  * Obtain the current tick count, setting *pxTimerListsWereSwitched to pdTRUE
//  229  * if a tick count overflow occurred since prvSampleTimeNow() was last called.
//  230  */
//  231 static TickType_t prvSampleTimeNow( BaseType_t * const pxTimerListsWereSwitched ) PRIVILEGED_FUNCTION;
//  232 
//  233 /*
//  234  * If the timer list contains any active timers then return the expire time of
//  235  * the timer that will expire first and set *pxListWasEmpty to false.  If the
//  236  * timer list does not contain any timers then return 0 and set *pxListWasEmpty
//  237  * to pdTRUE.
//  238  */
//  239 static TickType_t prvGetNextExpireTime( BaseType_t * const pxListWasEmpty ) PRIVILEGED_FUNCTION;
//  240 
//  241 /*
//  242  * If a timer has expired, process it.  Otherwise, block the timer service task
//  243  * until either a timer does expire or a command is received.
//  244  */
//  245 static void prvProcessTimerOrBlockTask( const TickType_t xNextExpireTime, BaseType_t xListWasEmpty ) PRIVILEGED_FUNCTION;
//  246 
//  247 /*
//  248  * Called after a Timer_t structure has been allocated either statically or
//  249  * dynamically to fill in the structure's members.
//  250  */
//  251 static void prvInitialiseNewTimer(	const char * const pcTimerName,
//  252 									const TickType_t xTimerPeriodInTicks,
//  253 									const UBaseType_t uxAutoReload,
//  254 									void * const pvTimerID,
//  255 									TimerCallbackFunction_t pxCallbackFunction,
//  256 									Timer_t *pxNewTimer ) PRIVILEGED_FUNCTION; /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  257 /*-----------------------------------------------------------*/
//  258 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _xTimerCreateTimerTask
        CODE
//  259 BaseType_t xTimerCreateTimerTask( void )
//  260 {
_xTimerCreateTimerTask:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 12
        SUBW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+16
//  261 BaseType_t xReturn = pdFAIL;
//  262 
//  263 	/* This function is called when the scheduler is started if
//  264 	configUSE_TIMERS is set to 1.  Check that the infrastructure used by the
//  265 	timer service task has been created/initialised.  If timers have already
//  266 	been created then the initialisation will already have been performed. */
//  267 	prvCheckForValidListAndQueue();
          CFI FunCall _prvCheckForValidListAndQueue
        CALL      F:_prvCheckForValidListAndQueue  ;; 3 cycles
//  268 
//  269 	if( xTimerQueue != NULL )
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_0  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_0:
        BZ        ??xTimerPendFunctionCall_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  270 	{
//  271 		#if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  272 		{
//  273 			StaticTask_t *pxTimerTaskTCBBuffer = NULL;
        MOV       [SP+0x0A], #0x0    ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
//  274 			StackType_t *pxTimerTaskStackBuffer = NULL;
        MOV       [SP+0x06], #0x0    ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
//  275 			uint32_t ulTimerTaskStackSize;
//  276 
//  277 			vApplicationGetTimerTaskMemory( &pxTimerTaskTCBBuffer, &pxTimerTaskStackBuffer, &ulTimerTaskStackSize );
        MOV       X, #0xF            ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x2           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _vApplicationGetTimerTaskMemory
        CALL      F:_vApplicationGetTimerTaskMemory  ;; 3 cycles
//  278 			xTimerTaskHandle = xTaskCreateStatic(	prvTimerTask,
//  279 													"Tmr Svc",
//  280 													ulTimerTaskStackSize,
//  281 													NULL,
//  282 													( ( UBaseType_t ) configTIMER_TASK_PRIORITY ) | portPRIVILEGE_BIT,
//  283 													pxTimerTaskStackBuffer,
//  284 													pxTimerTaskTCBBuffer );
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, #0x6           ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+36
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+38
        MOVW      BC, #LWRD(?_0)     ;; 1 cycle
        MOV       X, #BYTE3(?_0)     ;; 1 cycle
        MOVW      DE, #LWRD(_prvTimerTask)  ;; 1 cycle
        MOV       A, #BYTE3(_prvTimerTask)  ;; 1 cycle
          CFI FunCall _xTaskCreateStatic
        CALL      F:_xTaskCreateStatic  ;; 3 cycles
        MOVW      HL, #LWRD(_xTimerQueue+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  285 
//  286 			if( xTimerTaskHandle != NULL )
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        ADDW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_2  ;; 4 cycles
        ; ------------------------------------- Block: 71 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_2:
        BZ        ??xTimerPendFunctionCall_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  287 			{
//  288 				xReturn = pdPASS;
//  289 			}
//  290 		}
//  291 		#else
//  292 		{
//  293 			xReturn = xTaskCreate(	prvTimerTask,
//  294 									"Tmr Svc",
//  295 									configTIMER_TASK_STACK_DEPTH,
//  296 									NULL,
//  297 									( ( UBaseType_t ) configTIMER_TASK_PRIORITY ) | portPRIVILEGE_BIT,
//  298 									&xTimerTaskHandle );
//  299 		}
//  300 		#endif /* configSUPPORT_STATIC_ALLOCATION */
//  301 	}
//  302 	else
//  303 	{
//  304 		mtCOVERAGE_TEST_MARKER();
//  305 	}
//  306 
//  307 	configASSERT( xReturn );
//  308 	return xReturn;
        ONEW      AX                 ;; 1 cycle
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI CFA SP+16
        ; ------------------------------------- Block: 8 cycles
??xTimerPendFunctionCall_1:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_3  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_3:
        BR        S:??xTimerPendFunctionCall_3  ;; 3 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 128 cycles
//  309 }
//  310 /*-----------------------------------------------------------*/
//  311 
//  312 #if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
//  313 
//  314 	TimerHandle_t xTimerCreate(	const char * const pcTimerName,
//  315 								const TickType_t xTimerPeriodInTicks,
//  316 								const UBaseType_t uxAutoReload,
//  317 								void * const pvTimerID,
//  318 								TimerCallbackFunction_t pxCallbackFunction ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  319 	{
//  320 	Timer_t *pxNewTimer;
//  321 
//  322 		pxNewTimer = ( Timer_t * ) pvPortMalloc( sizeof( Timer_t ) );
//  323 
//  324 		if( pxNewTimer != NULL )
//  325 		{
//  326 			prvInitialiseNewTimer( pcTimerName, xTimerPeriodInTicks, uxAutoReload, pvTimerID, pxCallbackFunction, pxNewTimer );
//  327 
//  328 			#if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  329 			{
//  330 				/* Timers can be created statically or dynamically, so note this
//  331 				timer was created dynamically in case the timer is later
//  332 				deleted. */
//  333 				pxNewTimer->ucStaticallyAllocated = pdFALSE;
//  334 			}
//  335 			#endif /* configSUPPORT_STATIC_ALLOCATION */
//  336 		}
//  337 
//  338 		return pxNewTimer;
//  339 	}
//  340 
//  341 #endif /* configSUPPORT_STATIC_ALLOCATION */
//  342 /*-----------------------------------------------------------*/
//  343 
//  344 #if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  345 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon1
          CFI Function _xTimerCreateStatic
        CODE
//  346 	TimerHandle_t xTimerCreateStatic(	const char * const pcTimerName,
//  347 										const TickType_t xTimerPeriodInTicks,
//  348 										const UBaseType_t uxAutoReload,
//  349 										void * const pvTimerID,
//  350 										TimerCallbackFunction_t pxCallbackFunction,
//  351 										StaticTimer_t *pxTimerBuffer ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  352 	{
_xTimerCreateStatic:
        ; * Stack frame (at entry) *
        ; Param size: 16
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
//  353 	Timer_t *pxNewTimer;
//  354 
//  355 		#if( configASSERT_DEFINED == 1 )
//  356 		{
//  357 			/* Sanity check that the size of the structure used to declare a
//  358 			variable of type StaticTimer_t equals the size of the real timer
//  359 			structures. */
//  360 			volatile size_t xSize = sizeof( StaticTimer_t );
        MOVW      AX, #0x28          ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  361 			configASSERT( xSize == sizeof( Timer_t ) );
        MOVW      AX, [SP]           ;; 1 cycle
        CMPW      AX, #0x28          ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_4  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_5  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_5:
        BR        S:??xTimerPendFunctionCall_5  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  362 		}
//  363 		#endif /* configASSERT_DEFINED */
//  364 
//  365 		/* A pointer to a StaticTimer_t structure MUST be provided, use it. */
//  366 		configASSERT( pxTimerBuffer );
??xTimerPendFunctionCall_4:
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_6  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_6:
        BNZ       ??xTimerPendFunctionCall_7  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_8  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_8:
        BR        S:??xTimerPendFunctionCall_8  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  367 		pxNewTimer = ( Timer_t * ) pxTimerBuffer; /*lint !e740 Unusual cast is ok as the structures are designed to have the same alignment, and the size is checked by an assert. */
//  368 
//  369 		if( pxNewTimer != NULL )
//  370 		{
//  371 			prvInitialiseNewTimer( pcTimerName, xTimerPeriodInTicks, uxAutoReload, pvTimerID, pxCallbackFunction, pxNewTimer );
??xTimerPendFunctionCall_7:
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvInitialiseNewTimer
        CALL      F:_prvInitialiseNewTimer  ;; 3 cycles
//  372 
//  373 			#if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
//  374 			{
//  375 				/* Timers can be created statically or dynamically so note this
//  376 				timer was created statically in case it is later deleted. */
//  377 				pxNewTimer->ucStaticallyAllocated = pdTRUE;
//  378 			}
//  379 			#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
//  380 		}
//  381 
//  382 		return pxNewTimer;
        MOV       A, [SP+0x28]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x16          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 43 cycles
        ; ------------------------------------- Total: 113 cycles
//  383 	}
//  384 
//  385 #endif /* configSUPPORT_STATIC_ALLOCATION */
//  386 /*-----------------------------------------------------------*/
//  387 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _prvInitialiseNewTimer
        CODE
//  388 static void prvInitialiseNewTimer(	const char * const pcTimerName,
//  389 									const TickType_t xTimerPeriodInTicks,
//  390 									const UBaseType_t uxAutoReload,
//  391 									void * const pvTimerID,
//  392 									TimerCallbackFunction_t pxCallbackFunction,
//  393 									Timer_t *pxNewTimer ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  394 {
_prvInitialiseNewTimer:
        ; * Stack frame (at entry) *
        ; Param size: 16
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
//  395 	/* 0 is not a valid value for xTimerPeriodInTicks. */
//  396 	configASSERT( ( xTimerPeriodInTicks > 0 ) );
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        OR        A, E               ;; 1 cycle
        OR        A, L               ;; 1 cycle
        OR        A, H               ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_9  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_10  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_10:
        BR        S:??xTimerPendFunctionCall_10  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  397 
//  398 	if( pxNewTimer != NULL )
??xTimerPendFunctionCall_9:
        MOV       A, [SP+0x18]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_11  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_11:
        SKNZ                         ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_12  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  399 	{
//  400 		/* Ensure the infrastructure used by the timer service task has been
//  401 		created/initialised. */
//  402 		prvCheckForValidListAndQueue();
          CFI FunCall _prvCheckForValidListAndQueue
        CALL      F:_prvCheckForValidListAndQueue  ;; 3 cycles
//  403 
//  404 		/* Initialise the timer structure members using the function
//  405 		parameters. */
//  406 		pxNewTimer->pcTimerName = pcTimerName;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  407 		pxNewTimer->xTimerPeriodInTicks = xTimerPeriodInTicks;
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  408 		pxNewTimer->uxAutoReload = uxAutoReload;
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  409 		pxNewTimer->pvTimerID = pvTimerID;
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  410 		pxNewTimer->pxCallbackFunction = pxCallbackFunction;
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  411 		vListInitialiseItem( &( pxNewTimer->xTimerListItem ) );
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInitialiseItem
        CALL      F:_vListInitialiseItem  ;; 3 cycles
        ; ------------------------------------- Block: 97 cycles
//  412 		traceTIMER_CREATE( pxNewTimer );
//  413 	}
//  414 }
??xTimerPendFunctionCall_12:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 156 cycles
//  415 /*-----------------------------------------------------------*/
//  416 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon1
          CFI Function _xTimerGenericCommand
        CODE
//  417 BaseType_t xTimerGenericCommand( TimerHandle_t xTimer, const BaseType_t xCommandID, const TickType_t xOptionalValue, BaseType_t * const pxHigherPriorityTaskWoken, const TickType_t xTicksToWait )
//  418 {
_xTimerGenericCommand:
        ; * Stack frame (at entry) *
        ; Param size: 12
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 24
        SUBW      SP, #0x14          ;; 1 cycle
          CFI CFA SP+28
//  419 BaseType_t xReturn = pdFAIL;
        CLRW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
//  420 DaemonTaskMessage_t xMessage;
//  421 
//  422 	configASSERT( xTimer );
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_13  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_13:
        BNZ       ??xTimerPendFunctionCall_14  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_15  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_15:
        BR        S:??xTimerPendFunctionCall_15  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  423 
//  424 	/* Send a message to the timer service task to perform a particular action
//  425 	on a particular timer definition. */
//  426 	if( xTimerQueue != NULL )
??xTimerPendFunctionCall_14:
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      DE, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_16  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_16:
        SKNZ                         ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_17  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  427 	{
//  428 		/* Send a command to the timer service task to start the xTimer timer. */
//  429 		xMessage.xMessageID = xCommandID;
        MOVW      [SP+0x06], AX      ;; 1 cycle
//  430 		xMessage.u.xTimerParameters.xMessageValue = xOptionalValue;
        MOVW      AX, DE             ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        POP       BC                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
//  431 		xMessage.u.xTimerParameters.pxTimer = ( Timer_t * ) xTimer;
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x0E], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
//  432 
//  433 		if( xCommandID < tmrFIRST_FROM_ISR_COMMAND )
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        CMPW      AX, #0x8006        ;; 1 cycle
        BNC       ??xTimerPendFunctionCall_18  ;; 4 cycles
          CFI FunCall _xTaskGetSchedulerState
        ; ------------------------------------- Block: 35 cycles
//  434 		{
//  435 			if( xTaskGetSchedulerState() == taskSCHEDULER_RUNNING )
        CALL      F:_xTaskGetSchedulerState  ;; 3 cycles
        CMPW      AX, #0x2           ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_19  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x26]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x24]      ;; 1 cycle
//  436 			{
//  437 				xReturn = xQueueSendToBack( xTimerQueue, &xMessage, xTicksToWait );
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
        BR        S:??xTimerPendFunctionCall_20  ;; 3 cycles
          CFI CFA SP+28
        ; ------------------------------------- Block: 25 cycles
//  438 			}
//  439 			else
//  440 			{
//  441 				xReturn = xQueueSendToBack( xTimerQueue, &xMessage, tmrNO_DELAY );
??xTimerPendFunctionCall_19:
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
        BR        S:??xTimerPendFunctionCall_20  ;; 3 cycles
          CFI CFA SP+28
        ; ------------------------------------- Block: 22 cycles
//  442 			}
//  443 		}
//  444 		else
//  445 		{
//  446 			xReturn = xQueueSendToBackFromISR( xTimerQueue, &xMessage, pxHigherPriorityTaskWoken );
??xTimerPendFunctionCall_18:
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, [SP+0x24]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+36
        POP       DE                 ;; 1 cycle
          CFI CFA SP+34
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xQueueGenericSendFromISR
        CALL      F:_xQueueGenericSendFromISR  ;; 3 cycles
        ; ------------------------------------- Block: 25 cycles
??xTimerPendFunctionCall_20:
        MOVW      HL, AX             ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+28
        ; ------------------------------------- Block: 2 cycles
//  447 		}
//  448 
//  449 		traceTIMER_COMMAND_SEND( xTimer, xCommandID, xOptionalValue, xReturn );
//  450 	}
//  451 	else
//  452 	{
//  453 		mtCOVERAGE_TEST_MARKER();
//  454 	}
//  455 
//  456 	return xReturn;
??xTimerPendFunctionCall_17:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0x18          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 184 cycles
//  457 }
//  458 /*-----------------------------------------------------------*/
//  459 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon0
          CFI Function _xTimerGetTimerDaemonTaskHandle
          CFI NoCalls
        CODE
//  460 TaskHandle_t xTimerGetTimerDaemonTaskHandle( void )
//  461 {
_xTimerGetTimerDaemonTaskHandle:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  462 	/* If xTimerGetTimerDaemonTaskHandle() is called before the scheduler has been
//  463 	started, then xTimerTaskHandle will be NULL. */
//  464 	configASSERT( ( xTimerTaskHandle != NULL ) );
        MOVW      HL, #LWRD(_xTimerQueue+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_21  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_21:
        BNZ       ??xTimerPendFunctionCall_22  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_23  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_23:
        BR        S:??xTimerPendFunctionCall_23  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  465 	return xTimerTaskHandle;
??xTimerPendFunctionCall_22:
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+6
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+4
        MOV       A, ES              ;; 1 cycle
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 54 cycles
//  466 }
//  467 /*-----------------------------------------------------------*/
//  468 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon1
          CFI Function _xTimerGetPeriod
          CFI NoCalls
        CODE
//  469 TickType_t xTimerGetPeriod( TimerHandle_t xTimer )
//  470 {
_xTimerGetPeriod:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  471 Timer_t *pxTimer = ( Timer_t * ) xTimer;
//  472 
//  473 	configASSERT( xTimer );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_24  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_24:
        BNZ       ??xTimerPendFunctionCall_25  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_26  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_26:
        BR        S:??xTimerPendFunctionCall_26  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  474 	return pxTimer->xTimerPeriodInTicks;
??xTimerPendFunctionCall_25:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 2 cycles
        ; ------------------------------------- Total: 42 cycles
        REQUIRE ?Subroutine0
        ; // Fall through to label ?Subroutine0
//  475 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+8
        CODE
?Subroutine0:
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 13 cycles
        ; ------------------------------------- Total: 13 cycles
//  476 /*-----------------------------------------------------------*/
//  477 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon1
          CFI Function _xTimerGetExpiryTime
          CFI NoCalls
        CODE
//  478 TickType_t xTimerGetExpiryTime( TimerHandle_t xTimer )
//  479 {
_xTimerGetExpiryTime:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  480 Timer_t * pxTimer = ( Timer_t * ) xTimer;
//  481 TickType_t xReturn;
//  482 
//  483 	configASSERT( xTimer );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_27  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_27:
        BNZ       ??xTimerPendFunctionCall_28  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_29  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_29:
        BR        S:??xTimerPendFunctionCall_29  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  484 	xReturn = listGET_LIST_ITEM_VALUE( &( pxTimer->xTimerListItem ) );
//  485 	return xReturn;
??xTimerPendFunctionCall_28:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        BR        F:?Subroutine0     ;; 3 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 5 cycles
        ; ------------------------------------- Total: 45 cycles
//  486 }
//  487 /*-----------------------------------------------------------*/
//  488 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon2
          CFI Function _pcTimerGetName
          CFI NoCalls
        CODE
//  489 const char * pcTimerGetName( TimerHandle_t xTimer ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  490 {
_pcTimerGetName:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  491 Timer_t *pxTimer = ( Timer_t * ) xTimer;
//  492 
//  493 	configASSERT( xTimer );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_30  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_30:
        BNZ       ??xTimerPendFunctionCall_31  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_32  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_32:
        BR        S:??xTimerPendFunctionCall_32  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  494 	return pxTimer->pcTimerName;
??xTimerPendFunctionCall_31:
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 14 cycles
        ; ------------------------------------- Total: 54 cycles
//  495 }
//  496 /*-----------------------------------------------------------*/
//  497 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon3
          CFI Function _prvProcessExpiredTimer
        CODE
//  498 static void prvProcessExpiredTimer( const TickType_t xNextExpireTime, const TickType_t xTimeNow )
//  499 {
_prvProcessExpiredTimer:
        ; * Stack frame (at entry) *
        ; Param size: 4
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
//  500 BaseType_t xResult;
//  501 Timer_t * const pxTimer = ( Timer_t * ) listGET_OWNER_OF_HEAD_ENTRY( pxCurrentTimerList );
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTimerList  ;; 2 cycles
        MOV       A, ES:_pxCurrentTimerList+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x0A]   ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x0C]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[DE+0x0E]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[DE+0x0C]   ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
//  502 
//  503 	/* Remove the timer from the list of active timers.  A check has already
//  504 	been performed to ensure the list is not empty. */
//  505 	( void ) uxListRemove( &( pxTimer->xTimerListItem ) );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
//  506 	traceTIMER_EXPIRED( pxTimer );
//  507 
//  508 	/* If the timer is an auto reload timer then calculate the next
//  509 	expiry time and re-insert the timer in the list of active timers. */
//  510 	if( pxTimer->uxAutoReload == ( UBaseType_t ) pdTRUE )
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_33  ;; 4 cycles
        ; ------------------------------------- Block: 42 cycles
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
//  511 	{
//  512 		/* The timer is inserted into a list using a time relative to anything
//  513 		other than the current time.  It will therefore be inserted into the
//  514 		correct list relative to the time this task thinks it is now. */
//  515 		if( prvInsertTimerInActiveList( pxTimer, ( xNextExpireTime + pxTimer->xTimerPeriodInTicks ), xTimeNow, xNextExpireTime ) != pdFALSE )
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvInsertTimerInActiveList
        CALL      F:_prvInsertTimerInActiveList  ;; 3 cycles
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+12
        OR        A, X               ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_33  ;; 4 cycles
        ; ------------------------------------- Block: 48 cycles
//  516 		{
//  517 			/* The timer expired before it was added to the active timer
//  518 			list.  Reload it now.  */
//  519 			xResult = xTimerGenericCommand( pxTimer, tmrCOMMAND_START_DONT_TRACE, xNextExpireTime, NULL, tmrNO_DELAY );
//  520 			configASSERT( xResult );
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        CLRW      BC                 ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTimerGenericCommand
        CALL      F:_xTimerGenericCommand  ;; 3 cycles
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+12
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_33  ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_34  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_34:
        BR        S:??xTimerPendFunctionCall_34  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  521 			( void ) xResult;
//  522 		}
//  523 		else
//  524 		{
//  525 			mtCOVERAGE_TEST_MARKER();
//  526 		}
//  527 	}
//  528 	else
//  529 	{
//  530 		mtCOVERAGE_TEST_MARKER();
//  531 	}
//  532 
//  533 	/* Call the timer callback. */
//  534 	pxTimer->pxCallbackFunction( ( TimerHandle_t ) pxTimer );
??xTimerPendFunctionCall_33:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, C               ;; 1 cycle
          CFI FunCall
        CALL      HL                 ;; 3 cycles
//  535 }
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 31 cycles
        ; ------------------------------------- Total: 167 cycles
//  536 /*-----------------------------------------------------------*/
//  537 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon2
          CFI Function _prvTimerTask
        CODE
//  538 static void prvTimerTask( void *pvParameters )
//  539 {
_prvTimerTask:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
//  540 TickType_t xNextExpireTime;
//  541 BaseType_t xListWasEmpty;
//  542 
//  543 	/* Just to avoid compiler warnings. */
//  544 	( void ) pvParameters;
        ; ------------------------------------- Block: 1 cycles
//  545 
//  546 	#if( configUSE_DAEMON_TASK_STARTUP_HOOK == 1 )
//  547 	{
//  548 		extern void vApplicationDaemonTaskStartupHook( void );
//  549 
//  550 		/* Allow the application writer to execute some code in the context of
//  551 		this task at the point the task starts executing.  This is useful if the
//  552 		application includes initialisation code that would benefit from
//  553 		executing after the scheduler has been started. */
//  554 		vApplicationDaemonTaskStartupHook();
//  555 	}
//  556 	#endif /* configUSE_DAEMON_TASK_STARTUP_HOOK */
//  557 
//  558 	for( ;; )
//  559 	{
//  560 		/* Query the timers list to see if it contains any timers, and if so,
//  561 		obtain the time at which the next timer will expire. */
//  562 		xNextExpireTime = prvGetNextExpireTime( &xListWasEmpty );
??prvTimerTask_0:
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTimerList  ;; 2 cycles
        MOV       A, ES:_pxCurrentTimerList+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_35  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOV       A, ES:_pxCurrentTimerList+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL+0x0A]   ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOV       A, ES:[HL+0x0C]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        BR        S:??xTimerPendFunctionCall_36  ;; 3 cycles
        ; ------------------------------------- Block: 21 cycles
??xTimerPendFunctionCall_35:
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        ; ------------------------------------- Block: 4 cycles
//  563 
//  564 		/* If a timer has expired, process it.  Otherwise, block this task
//  565 		until either a timer does expire, or a command is received. */
//  566 		prvProcessTimerOrBlockTask( xNextExpireTime, xListWasEmpty );
??xTimerPendFunctionCall_36:
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
          CFI FunCall _prvProcessTimerOrBlockTask
        CALL      F:_prvProcessTimerOrBlockTask  ;; 3 cycles
//  567 
//  568 		/* Empty the command queue. */
//  569 		prvProcessReceivedCommands();
          CFI FunCall _prvProcessReceivedCommands
        CALL      F:_prvProcessReceivedCommands  ;; 3 cycles
        BR        S:??prvTimerTask_0  ;; 3 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 51 cycles
//  570 	}
//  571 }
//  572 /*-----------------------------------------------------------*/
//  573 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon1
          CFI Function _prvProcessTimerOrBlockTask
        CODE
//  574 static void prvProcessTimerOrBlockTask( const TickType_t xNextExpireTime, BaseType_t xListWasEmpty )
//  575 {
_prvProcessTimerOrBlockTask:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+10
        ; Auto size: 10
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
//  576 TickType_t xTimeNow;
//  577 BaseType_t xTimerListsWereSwitched;
//  578 
//  579 	vTaskSuspendAll();
          CFI FunCall _vTaskSuspendAll
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
//  580 	{
//  581 		/* Obtain the time now to make an assessment as to whether the timer
//  582 		has expired or not.  If obtaining the time causes the lists to switch
//  583 		then don't process this timer as any timers that remained in the list
//  584 		when the lists were switched will have been processed within the
//  585 		prvSampleTimeNow() function. */
//  586 		xTimeNow = prvSampleTimeNow( &xTimerListsWereSwitched );
          CFI FunCall _xTaskGetTickCount
        CALL      F:_xTaskGetTickCount  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      HL, #LWRD(_xTimerQueue+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        BNC       ??xTimerPendFunctionCall_37  ;; 4 cycles
          CFI FunCall _prvSwitchTimerLists
        ; ------------------------------------- Block: 33 cycles
        CALL      F:_prvSwitchTimerLists  ;; 3 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, #LWRD(_xTimerQueue+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  587 		if( xTimerListsWereSwitched == pdFALSE )
//  588 		{
//  589 			/* The tick count has not overflowed, has the timer expired? */
//  590 			if( ( xListWasEmpty == pdFALSE ) && ( xNextExpireTime <= xTimeNow ) )
//  591 			{
//  592 				( void ) xTaskResumeAll();
//  593 				prvProcessExpiredTimer( xNextExpireTime, xTimeNow );
//  594 			}
//  595 			else
//  596 			{
//  597 				/* The tick count has not overflowed, and the next expire
//  598 				time has not been reached yet.  This task should therefore
//  599 				block to wait for the next expire time or a command to be
//  600 				received - whichever comes first.  The following line cannot
//  601 				be reached unless xNextExpireTime > xTimeNow, except in the
//  602 				case when the current timer list is empty. */
//  603 				if( xListWasEmpty != pdFALSE )
//  604 				{
//  605 					/* The current timer list is empty - is the overflow list
//  606 					also empty? */
//  607 					xListWasEmpty = listLIST_IS_EMPTY( pxOverflowTimerList );
//  608 				}
//  609 
//  610 				vQueueWaitForMessageRestricted( xTimerQueue, ( xNextExpireTime - xTimeNow ), xListWasEmpty );
//  611 
//  612 				if( xTaskResumeAll() == pdFALSE )
//  613 				{
//  614 					/* Yield to wait for either a command to arrive, or the
//  615 					block time to expire.  If a command arrived between the
//  616 					critical section being exited and this yield then the yield
//  617 					will not cause the task to block. */
//  618 					portYIELD_WITHIN_API();
//  619 				}
//  620 				else
//  621 				{
//  622 					mtCOVERAGE_TEST_MARKER();
//  623 				}
//  624 			}
//  625 		}
//  626 		else
//  627 		{
//  628 			( void ) xTaskResumeAll();
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        BR        R:??xTimerPendFunctionCall_38  ;; 3 cycles
        ; ------------------------------------- Block: 19 cycles
//  629 		}
??xTimerPendFunctionCall_37:
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_39  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_40  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_40  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_40  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_40:
        BC        ??xTimerPendFunctionCall_41  ;; 4 cycles
          CFI FunCall _xTaskResumeAll
        ; ------------------------------------- Block: 4 cycles
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
          CFI FunCall _prvProcessExpiredTimer
        CALL      F:_prvProcessExpiredTimer  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        BR        S:??xTimerPendFunctionCall_38  ;; 3 cycles
        ; ------------------------------------- Block: 18 cycles
??xTimerPendFunctionCall_39:
        MOVW      HL, #LWRD(_pxCurrentTimerList+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        ONEW      AX                 ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 13 cycles
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvProcessTimerOrBlockTask_0:
        MOVW      [SP+0x04], AX      ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??xTimerPendFunctionCall_41:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vQueueWaitForMessageRestricted
        CALL      F:_vQueueWaitForMessageRestricted  ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+14
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 38 cycles
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  630 	}
//  631 }
??xTimerPendFunctionCall_38:
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 174 cycles
//  632 /*-----------------------------------------------------------*/
//  633 
//  634 static TickType_t prvGetNextExpireTime( BaseType_t * const pxListWasEmpty )
//  635 {
//  636 TickType_t xNextExpireTime;
//  637 
//  638 	/* Timers are listed in expiry time order, with the head of the list
//  639 	referencing the task that will expire first.  Obtain the time at which
//  640 	the timer with the nearest expiry time will expire.  If there are no
//  641 	active timers then just set the next expire time to 0.  That will cause
//  642 	this task to unblock when the tick count overflows, at which point the
//  643 	timer lists will be switched and the next expiry time can be
//  644 	re-assessed.  */
//  645 	*pxListWasEmpty = listLIST_IS_EMPTY( pxCurrentTimerList );
//  646 	if( *pxListWasEmpty == pdFALSE )
//  647 	{
//  648 		xNextExpireTime = listGET_ITEM_VALUE_OF_HEAD_ENTRY( pxCurrentTimerList );
//  649 	}
//  650 	else
//  651 	{
//  652 		/* Ensure the task unblocks when the tick count rolls over. */
//  653 		xNextExpireTime = ( TickType_t ) 0U;
//  654 	}
//  655 
//  656 	return xNextExpireTime;
//  657 }
//  658 /*-----------------------------------------------------------*/
//  659 
//  660 static TickType_t prvSampleTimeNow( BaseType_t * const pxTimerListsWereSwitched )
//  661 {
//  662 TickType_t xTimeNow;
//  663 PRIVILEGED_DATA static TickType_t xLastTime = ( TickType_t ) 0U; /*lint !e956 Variable is only accessible to one task. */
//  664 
//  665 	xTimeNow = xTaskGetTickCount();
//  666 
//  667 	if( xTimeNow < xLastTime )
//  668 	{
//  669 		prvSwitchTimerLists();
//  670 		*pxTimerListsWereSwitched = pdTRUE;
//  671 	}
//  672 	else
//  673 	{
//  674 		*pxTimerListsWereSwitched = pdFALSE;
//  675 	}
//  676 
//  677 	xLastTime = xTimeNow;
//  678 
//  679 	return xTimeNow;
//  680 }
//  681 /*-----------------------------------------------------------*/
//  682 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon2
          CFI Function _prvInsertTimerInActiveList
        CODE
//  683 static BaseType_t prvInsertTimerInActiveList( Timer_t * const pxTimer, const TickType_t xNextExpiryTime, const TickType_t xTimeNow, const TickType_t xCommandTime )
//  684 {
_prvInsertTimerInActiveList:
        ; * Stack frame (at entry) *
        ; Param size: 12
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
//  685 BaseType_t xProcessTimerNow = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  686 
//  687 	listSET_LIST_ITEM_VALUE( &( pxTimer->xTimerListItem ), xNextExpiryTime );
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  688 	listSET_LIST_ITEM_OWNER( &( pxTimer->xTimerListItem ), pxTimer );
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  689 
//  690 	if( xNextExpiryTime <= xTimeNow )
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_42  ;; 4 cycles
        ; ------------------------------------- Block: 52 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_42  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_42  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_42:
        BC        ??xTimerPendFunctionCall_43  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  691 	{
//  692 		/* Has the expiry time elapsed between the command to start/reset a
//  693 		timer was issued, and the time the command was processed? */
//  694 		if( ( ( TickType_t ) ( xTimeNow - xCommandTime ) ) >= pxTimer->xTimerPeriodInTicks ) /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        SUBW      AX, DE             ;; 1 cycle
        SKNC
        DECW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        SUBW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+10
        BNC       ??xTimerPendFunctionCall_44  ;; 4 cycles
        ; ------------------------------------- Block: 34 cycles
//  695 		{
//  696 			/* The time between a command being issued and the command being
//  697 			processed actually exceeds the timers period.  */
//  698 			xProcessTimerNow = pdTRUE;
//  699 		}
//  700 		else
//  701 		{
//  702 			vListInsert( pxOverflowTimerList, &( pxTimer->xTimerListItem ) );
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      HL, #LWRD(_pxCurrentTimerList+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       AX                 ;; 1 cycle
          CFI CFA SP+10
        BR        S:??xTimerPendFunctionCall_45  ;; 3 cycles
        ; ------------------------------------- Block: 20 cycles
//  703 		}
//  704 	}
//  705 	else
//  706 	{
//  707 		if( ( xTimeNow < xCommandTime ) && ( xNextExpiryTime >= xCommandTime ) )
??xTimerPendFunctionCall_43:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x12          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_46  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_46  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_46  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_46:
        BNC       ??xTimerPendFunctionCall_47  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        XCH       A, B               ;; 1 cycle
        CMP       A, [HL+0x03]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_48  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOV       A, C               ;; 1 cycle
        CMP       A, [HL+0x02]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_48  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, B               ;; 1 cycle
        CMP       A, [HL+0x01]       ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_48  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, X               ;; 1 cycle
        CMP       A, [HL]            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_48:
        BC        ??xTimerPendFunctionCall_47  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  708 		{
//  709 			/* If, since the command was issued, the tick count has overflowed
//  710 			but the expiry time has not, then the timer must have already passed
//  711 			its expiry time and should be processed immediately. */
//  712 			xProcessTimerNow = pdTRUE;
??xTimerPendFunctionCall_44:
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        BR        S:??xTimerPendFunctionCall_49  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
//  713 		}
//  714 		else
//  715 		{
//  716 			vListInsert( pxCurrentTimerList, &( pxTimer->xTimerListItem ) );
??xTimerPendFunctionCall_47:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTimerList  ;; 2 cycles
        MOV       A, ES:_pxCurrentTimerList+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
??xTimerPendFunctionCall_45:
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+12
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInsert
        CALL      F:_vListInsert     ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  717 		}
//  718 	}
//  719 
//  720 	return xProcessTimerNow;
??xTimerPendFunctionCall_49:
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 211 cycles
//  721 }
//  722 /*-----------------------------------------------------------*/
//  723 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon0
          CFI Function _prvProcessReceivedCommands
        CODE
//  724 static void	prvProcessReceivedCommands( void )
//  725 {
_prvProcessReceivedCommands:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 52
        SUBW      SP, #0x34          ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #0xF           ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 6 cycles
        MOV       ES, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvProcessReceivedCommands_0:
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x1C], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x1A], AX      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x32], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x30], AX      ;; 1 cycle
        ADDW      AX, #0xFFE6        ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x20], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x1E], AX      ;; 1 cycle
        BR        S:??xTimerPendFunctionCall_50  ;; 3 cycles
        ; ------------------------------------- Block: 19 cycles
//  726 DaemonTaskMessage_t xMessage;
//  727 Timer_t *pxTimer;
//  728 BaseType_t xTimerListsWereSwitched, xResult;
//  729 TickType_t xTimeNow;
//  730 
//  731 	while( xQueueReceive( xTimerQueue, &xMessage, tmrNO_DELAY ) != pdFAIL ) /*lint !e603 xMessage does not have to be initialised as it is passed out, not in, and it is not used unless xQueueReceive() returns pdTRUE. */
//  732 	{
//  733 		#if ( INCLUDE_xTimerPendFunctionCall == 1 )
//  734 		{
//  735 			/* Negative commands are pended function calls rather than timer
//  736 			commands. */
//  737 			if( xMessage.xMessageID < ( BaseType_t ) 0 )
//  738 			{
//  739 				const CallbackParameters_t * const pxCallback = &( xMessage.u.xCallbackParameters );
//  740 
//  741 				/* The timer uses the xCallbackParameters member to request a
//  742 				callback be executed.  Check the callback is not NULL. */
//  743 				configASSERT( pxCallback );
//  744 
//  745 				/* Call the function. */
//  746 				pxCallback->pxCallbackFunction( pxCallback->pvParameter1, pxCallback->ulParameter2 );
//  747 			}
//  748 			else
//  749 			{
//  750 				mtCOVERAGE_TEST_MARKER();
//  751 			}
//  752 		}
//  753 		#endif /* INCLUDE_xTimerPendFunctionCall */
//  754 
//  755 		/* Commands that are positive are timer commands rather than pended
//  756 		function calls. */
//  757 		if( xMessage.xMessageID >= ( BaseType_t ) 0 )
//  758 		{
//  759 			/* The messages uses the xTimerParameters member to work on a
//  760 			software timer. */
//  761 			pxTimer = xMessage.u.xTimerParameters.pxTimer;
//  762 
//  763 			if( listIS_CONTAINED_WITHIN( NULL, &( pxTimer->xTimerListItem ) ) == pdFALSE )
//  764 			{
//  765 				/* The timer is in a list, remove it. */
//  766 				( void ) uxListRemove( &( pxTimer->xTimerListItem ) );
//  767 			}
//  768 			else
//  769 			{
//  770 				mtCOVERAGE_TEST_MARKER();
//  771 			}
//  772 
//  773 			traceTIMER_COMMAND_RECEIVED( pxTimer, xMessage.xMessageID, xMessage.u.xTimerParameters.xMessageValue );
//  774 
//  775 			/* In this case the xTimerListsWereSwitched parameter is not used, but
//  776 			it must be present in the function call.  prvSampleTimeNow() must be
//  777 			called after the message is received from xTimerQueue so there is no
//  778 			possibility of a higher priority task adding a message to the message
//  779 			queue with a time that is ahead of the timer daemon task (because it
//  780 			pre-empted the timer daemon task after the xTimeNow value was set). */
//  781 			xTimeNow = prvSampleTimeNow( &xTimerListsWereSwitched );
//  782 
//  783 			switch( xMessage.xMessageID )
//  784 			{
//  785 				case tmrCOMMAND_START :
//  786 			    case tmrCOMMAND_START_FROM_ISR :
//  787 			    case tmrCOMMAND_RESET :
//  788 			    case tmrCOMMAND_RESET_FROM_ISR :
//  789 				case tmrCOMMAND_START_DONT_TRACE :
//  790 					/* Start or restart a timer. */
//  791 					if( prvInsertTimerInActiveList( pxTimer,  xMessage.u.xTimerParameters.xMessageValue + pxTimer->xTimerPeriodInTicks, xTimeNow, xMessage.u.xTimerParameters.xMessageValue ) != pdFALSE )
//  792 					{
//  793 						/* The timer expired before it was added to the active
//  794 						timer list.  Process it now. */
//  795 						pxTimer->pxCallbackFunction( ( TimerHandle_t ) pxTimer );
//  796 						traceTIMER_EXPIRED( pxTimer );
//  797 
//  798 						if( pxTimer->uxAutoReload == ( UBaseType_t ) pdTRUE )
//  799 						{
//  800 							xResult = xTimerGenericCommand( pxTimer, tmrCOMMAND_START_DONT_TRACE, xMessage.u.xTimerParameters.xMessageValue + pxTimer->xTimerPeriodInTicks, NULL, tmrNO_DELAY );
//  801 							configASSERT( xResult );
//  802 							( void ) xResult;
//  803 						}
//  804 						else
//  805 						{
//  806 							mtCOVERAGE_TEST_MARKER();
//  807 						}
//  808 					}
//  809 					else
//  810 					{
//  811 						mtCOVERAGE_TEST_MARKER();
//  812 					}
//  813 					break;
//  814 
//  815 				case tmrCOMMAND_STOP :
//  816 				case tmrCOMMAND_STOP_FROM_ISR :
//  817 					/* The timer has already been removed from the active list.
//  818 					There is nothing to do here. */
//  819 					break;
//  820 
//  821 				case tmrCOMMAND_CHANGE_PERIOD :
//  822 				case tmrCOMMAND_CHANGE_PERIOD_FROM_ISR :
//  823 					pxTimer->xTimerPeriodInTicks = xMessage.u.xTimerParameters.xMessageValue;
//  824 					configASSERT( ( pxTimer->xTimerPeriodInTicks > 0 ) );
//  825 
//  826 					/* The new period does not really have a reference, and can
//  827 					be longer or shorter than the old one.  The command time is
//  828 					therefore set to the current time, and as the period cannot
//  829 					be zero the next expiry time can only be in the future,
//  830 					meaning (unlike for the xTimerStart() case above) there is
//  831 					no fail case that needs to be handled here. */
//  832 					( void ) prvInsertTimerInActiveList( pxTimer, ( xTimeNow + pxTimer->xTimerPeriodInTicks ), xTimeNow, xTimeNow );
??prvProcessReceivedCommands_1:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+62
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+64
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+66
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvInsertTimerInActiveList
        CALL      F:_prvInsertTimerInActiveList  ;; 3 cycles
//  833 					break;
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+56
        ; ------------------------------------- Block: 30 cycles
??xTimerPendFunctionCall_50:
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        MOVW      AX, [SP+0x24]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+64
        POP       DE                 ;; 1 cycle
          CFI CFA SP+62
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xQueueGenericReceive
        CALL      F:_xQueueGenericReceive  ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+56
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_51  ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        BF        A.7, ??xTimerPendFunctionCall_52  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x1C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      AX, [SP+0x1E]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+60
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+64
        MOV       A, [SP+0x24]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+66
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+64
        POP       DE                 ;; 1 cycle
          CFI CFA SP+62
        POP       BC                 ;; 1 cycle
          CFI CFA SP+60
        MOV       A, C               ;; 1 cycle
          CFI FunCall
        CALL      HL                 ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+56
        ; ------------------------------------- Block: 44 cycles
??xTimerPendFunctionCall_52:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        BT        A.7, ??xTimerPendFunctionCall_50  ;; 5 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_53  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_53:
        BZ        ??xTimerPendFunctionCall_54  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
          CFI FunCall _xTaskGetTickCount
        ; ------------------------------------- Block: 9 cycles
??xTimerPendFunctionCall_54:
        CALL      F:_xTaskGetTickCount  ;; 3 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      HL, #LWRD(_xTimerQueue+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+56
        SKNC                         ;; 1 cycle
          CFI FunCall _prvSwitchTimerLists
        ; ------------------------------------- Block: 23 cycles
        CALL      F:_prvSwitchTimerLists  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvProcessReceivedCommands_2:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, #LWRD(_xTimerQueue+8)  ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        CMPW      AX, #0x3           ;; 1 cycle
        BC        ??xTimerPendFunctionCall_55  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        SUBW      AX, #0x4           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_56  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        SUBW      AX, #0x2           ;; 1 cycle
        CMPW      AX, #0x2           ;; 1 cycle
        BC        ??xTimerPendFunctionCall_55  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        SUBW      AX, #0x3           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_56  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        R:??xTimerPendFunctionCall_50  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??xTimerPendFunctionCall_55:
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+62
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+64
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+66
        POP       HL                 ;; 1 cycle
          CFI CFA SP+64
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+66
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+68
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvInsertTimerInActiveList
        CALL      F:_prvInsertTimerInActiveList  ;; 3 cycles
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+56
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_50  ;; 4 cycles
        ; ------------------------------------- Block: 49 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+60
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+60
        POP       DE                 ;; 1 cycle
          CFI CFA SP+58
        POP       BC                 ;; 1 cycle
          CFI CFA SP+56
        MOV       A, C               ;; 1 cycle
          CFI FunCall
        CALL      HL                 ;; 3 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_50  ;; 4 cycles
        ; ------------------------------------- Block: 36 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+58
        POP       HL                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_57  ;; 4 cycles
        ; ------------------------------------- Block: 34 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_57:
        BZ        ??xTimerPendFunctionCall_58  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_59  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_59:
        BZ        ??xTimerPendFunctionCall_60  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x22], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x24], AX      ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      [SP+0x26], AX      ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x2A], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x28], AX      ;; 1 cycle
          CFI FunCall _xTaskGetSchedulerState
        CALL      F:_xTaskGetSchedulerState  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+62
        MOVW      AX, [SP+0x36]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+64
        POP       DE                 ;; 1 cycle
          CFI CFA SP+62
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+56
        CMPW      AX, #0x0           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_50  ;; 4 cycles
        ; ------------------------------------- Block: 42 cycles
??xTimerPendFunctionCall_60:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_61  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_61:
        BR        S:??xTimerPendFunctionCall_61  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??xTimerPendFunctionCall_58:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_62  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_62:
        BR        S:??xTimerPendFunctionCall_62  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??xTimerPendFunctionCall_56:
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      [SP+0x16], AX      ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      [SP+0x18], AX      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+58
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+60
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+58
        POP       BC                 ;; 1 cycle
          CFI CFA SP+56
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvProcessReceivedCommands_1  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_63  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_63:
        BR        S:??xTimerPendFunctionCall_63  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  834 
//  835 				case tmrCOMMAND_DELETE :
//  836 					/* The timer has already been removed from the active list,
//  837 					just free up the memory if the memory was dynamically
//  838 					allocated. */
//  839 					#if( ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 0 ) )
//  840 					{
//  841 						/* The timer can only have been allocated dynamically -
//  842 						free it again. */
//  843 						vPortFree( pxTimer );
//  844 					}
//  845 					#elif( ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 1 ) )
//  846 					{
//  847 						/* The timer could have been allocated statically or
//  848 						dynamically, so check before attempting to free the
//  849 						memory. */
//  850 						if( pxTimer->ucStaticallyAllocated == ( uint8_t ) pdFALSE )
//  851 						{
//  852 							vPortFree( pxTimer );
//  853 						}
//  854 						else
//  855 						{
//  856 							mtCOVERAGE_TEST_MARKER();
//  857 						}
//  858 					}
//  859 					#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
//  860 					break;
//  861 
//  862 				default	:
//  863 					/* Don't expect to get here. */
//  864 					break;
//  865 			}
//  866 		}
//  867 	}
//  868 }
??xTimerPendFunctionCall_51:
        ADDW      SP, #0x34          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 518 cycles
//  869 /*-----------------------------------------------------------*/
//  870 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon0
          CFI Function _prvSwitchTimerLists
        CODE
//  871 static void prvSwitchTimerLists( void )
//  872 {
_prvSwitchTimerLists:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 26
        SUBW      SP, #0x1A          ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       [SP+0x18], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x16], AX      ;; 1 cycle
        BR        S:??xTimerPendFunctionCall_64  ;; 3 cycles
        ; ------------------------------------- Block: 11 cycles
//  873 TickType_t xNextExpireTime, xReloadTime;
//  874 List_t *pxTemp;
//  875 Timer_t *pxTimer;
//  876 BaseType_t xResult;
//  877 
//  878 	/* The tick count has overflowed.  The timer lists must be switched.
//  879 	If there are any timers still referenced from the current timer list
//  880 	then they must have expired and should be processed before the lists
//  881 	are switched. */
//  882 	while( listLIST_IS_EMPTY( pxCurrentTimerList ) == pdFALSE )
//  883 	{
//  884 		xNextExpireTime = listGET_ITEM_VALUE_OF_HEAD_ENTRY( pxCurrentTimerList );
//  885 
//  886 		/* Remove the timer from the list. */
//  887 		pxTimer = ( Timer_t * ) listGET_OWNER_OF_HEAD_ENTRY( pxCurrentTimerList );
//  888 		( void ) uxListRemove( &( pxTimer->xTimerListItem ) );
//  889 		traceTIMER_EXPIRED( pxTimer );
//  890 
//  891 		/* Execute its callback, then send a command to restart the timer if
//  892 		it is an auto-reload timer.  It cannot be restarted here as the lists
//  893 		have not yet been switched. */
//  894 		pxTimer->pxCallbackFunction( ( TimerHandle_t ) pxTimer );
//  895 
//  896 		if( pxTimer->uxAutoReload == ( UBaseType_t ) pdTRUE )
//  897 		{
//  898 			/* Calculate the reload value, and if the reload value results in
//  899 			the timer going into the same timer list then it has already expired
//  900 			and the timer should be re-inserted into the current list so it is
//  901 			processed again within this loop.  Otherwise a command should be sent
//  902 			to restart the timer to ensure it is only inserted into a list after
//  903 			the lists have been swapped. */
//  904 			xReloadTime = ( xNextExpireTime + pxTimer->xTimerPeriodInTicks );
//  905 			if( xReloadTime > xNextExpireTime )
//  906 			{
//  907 				listSET_LIST_ITEM_VALUE( &( pxTimer->xTimerListItem ), xReloadTime );
??prvSwitchTimerLists_0:
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       AX                 ;; 1 cycle
          CFI CFA SP+32
        POP       BC                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL+0x02], AX   ;; 2 cycles
//  908 				listSET_LIST_ITEM_OWNER( &( pxTimer->xTimerListItem ), pxTimer );
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        POP       BC                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  909 				vListInsert( pxCurrentTimerList, &( pxTimer->xTimerListItem ) );
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTimerList  ;; 2 cycles
        MOV       A, ES:_pxCurrentTimerList+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        POP       DE                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInsert
        CALL      F:_vListInsert     ;; 3 cycles
        ; ------------------------------------- Block: 45 cycles
//  910 			}
??xTimerPendFunctionCall_64:
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOVW      HL, ES:_pxCurrentTimerList  ;; 2 cycles
        MOV       A, ES:_pxCurrentTimerList+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_65  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOV       A, ES:_pxCurrentTimerList+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES:[HL+0x0C]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL+0x0A]   ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      [SP+0x06], AX      ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _uxListRemove
        CALL      F:_uxListRemove    ;; 3 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+34
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        POP       BC                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, C               ;; 1 cycle
          CFI FunCall
        CALL      HL                 ;; 3 cycles
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x1C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_64  ;; 4 cycles
        ; ------------------------------------- Block: 73 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL+0x02]   ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+32
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        SKNC
        INCW      BC                 ;; 5 cycles
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, HL             ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+32
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
        MOVW      DE, AX             ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
          CFI FunCall ?UL_CMP_L03
        CALL      N:?UL_CMP_L03      ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+30
        SKNC                         ;; 4 cycles
        BR        R:??prvSwitchTimerLists_0  ;; 4 cycles
        ; ------------------------------------- Block: 39 cycles
//  911 			else
//  912 			{
//  913 				xResult = xTimerGenericCommand( pxTimer, tmrCOMMAND_START_DONT_TRACE, xNextExpireTime, NULL, tmrNO_DELAY );
//  914 				configASSERT( xResult );
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_66  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_66:
        BZ        ??xTimerPendFunctionCall_67  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_68  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_68:
        BZ        ??xTimerPendFunctionCall_69  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x10], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x0E], AX      ;; 1 cycle
          CFI FunCall _xTaskGetSchedulerState
        CALL      F:_xTaskGetSchedulerState  ;; 3 cycles
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+34
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+36
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+38
        POP       DE                 ;; 1 cycle
          CFI CFA SP+36
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+30
        CMPW      AX, #0x0           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??xTimerPendFunctionCall_64  ;; 4 cycles
        ; ------------------------------------- Block: 42 cycles
??xTimerPendFunctionCall_69:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_70  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_70:
        BR        S:??xTimerPendFunctionCall_70  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  915 				( void ) xResult;
//  916 			}
//  917 		}
//  918 		else
//  919 		{
//  920 			mtCOVERAGE_TEST_MARKER();
//  921 		}
//  922 	}
??xTimerPendFunctionCall_67:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_71  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_71:
        BR        S:??xTimerPendFunctionCall_71  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  923 
//  924 	pxTemp = pxCurrentTimerList;
??xTimerPendFunctionCall_65:
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        POP       DE                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, ES:_pxCurrentTimerList+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  925 	pxCurrentTimerList = pxOverflowTimerList;
        MOVW      HL, #LWRD(_pxCurrentTimerList+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+32
        POP       HL                 ;; 1 cycle
          CFI CFA SP+30
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_pxCurrentTimerList)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  926 	pxOverflowTimerList = pxTemp;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOVW      DE, #LWRD(_pxCurrentTimerList+4)  ;; 1 cycle
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOV       ES:[DE+0x02], A    ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  927 }
        ADDW      SP, #0x1A          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 46 cycles
        ; ------------------------------------- Total: 344 cycles
//  928 /*-----------------------------------------------------------*/
//  929 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon0
          CFI Function _prvCheckForValidListAndQueue
        CODE
//  930 static void prvCheckForValidListAndQueue( void )
//  931 {
_prvCheckForValidListAndQueue:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//  932 	/* Check that the list from which active timers are referenced, and the
//  933 	queue used to communicate with the timer service, have been
//  934 	initialised. */
//  935 	taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_72  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_72:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
//  936 	{
//  937 		if( xTimerQueue == NULL )
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_73  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_73:
        BNZ       ??xTimerPendFunctionCall_74  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  938 		{
//  939 			vListInitialise( &xActiveTimerList1 );
        MOVW      DE, #LWRD(_pxCurrentTimerList+8)  ;; 1 cycle
        MOV       A, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
//  940 			vListInitialise( &xActiveTimerList2 );
        MOVW      DE, #LWRD(_pxCurrentTimerList+26)  ;; 1 cycle
        MOV       A, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
//  941 			pxCurrentTimerList = &xActiveTimerList1;
        MOV       ES, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        MOV       ES:_pxCurrentTimerList+2, #BYTE3(_pxCurrentTimerList)  ;; 2 cycles
        MOVW      AX, #LWRD(_pxCurrentTimerList+8)  ;; 1 cycle
        MOVW      ES:_pxCurrentTimerList, AX  ;; 2 cycles
//  942 			pxOverflowTimerList = &xActiveTimerList2;
        MOV       ES:_pxCurrentTimerList+6, #BYTE3(_pxCurrentTimerList)  ;; 2 cycles
        MOVW      AX, #LWRD(_pxCurrentTimerList+26)  ;; 1 cycle
        MOVW      ES:_pxCurrentTimerList+4, AX  ;; 2 cycles
//  943 
//  944 			#if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  945 			{
//  946 				/* The timer queue is allocated statically in case
//  947 				configSUPPORT_DYNAMIC_ALLOCATION is 0. */
//  948 				static StaticQueue_t xStaticTimerQueue;
//  949 				static uint8_t ucStaticTimerQueueStorage[ configTIMER_QUEUE_LENGTH * sizeof( DaemonTaskMessage_t ) ];
//  950 
//  951 				xTimerQueue = xQueueCreateStatic( ( UBaseType_t ) configTIMER_QUEUE_LENGTH, sizeof( DaemonTaskMessage_t ), &( ucStaticTimerQueueStorage[ 0 ] ), &xStaticTimerQueue );
        MOV       X, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        MOVW      AX, #LWRD(_pxCurrentTimerList+44)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+8
        MOV       X, #BYTE3(_pxCurrentTimerList)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOVW      AX, #LWRD(_pxCurrentTimerList+112)  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       E, #0x0            ;; 1 cycle
        MOVW      BC, #0xE           ;; 1 cycle
        MOVW      AX, #0x5           ;; 1 cycle
          CFI FunCall _xQueueGenericCreateStatic
        CALL      F:_xQueueGenericCreateStatic  ;; 3 cycles
        MOVW      HL, #LWRD(_xTimerQueue)  ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        ; ------------------------------------- Block: 43 cycles
//  952 			}
//  953 			#else
//  954 			{
//  955 				xTimerQueue = xQueueCreate( ( UBaseType_t ) configTIMER_QUEUE_LENGTH, sizeof( DaemonTaskMessage_t ) );
//  956 			}
//  957 			#endif
//  958 
//  959 			#if ( configQUEUE_REGISTRY_SIZE > 0 )
//  960 			{
//  961 				if( xTimerQueue != NULL )
//  962 				{
//  963 					vQueueAddToRegistry( xTimerQueue, "TmrQ" );
//  964 				}
//  965 				else
//  966 				{
//  967 					mtCOVERAGE_TEST_MARKER();
//  968 				}
//  969 			}
//  970 			#endif /* configQUEUE_REGISTRY_SIZE */
//  971 		}
//  972 		else
//  973 		{
//  974 			mtCOVERAGE_TEST_MARKER();
//  975 		}
//  976 	}
//  977 	taskEXIT_CRITICAL();
??xTimerPendFunctionCall_74:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_75  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_75  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  978 }
??xTimerPendFunctionCall_75:
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 6 cycles
        ; ------------------------------------- Total: 108 cycles

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
_pxCurrentTimerList:
        DS 4
        DS 4
        DS 18
        DS 18
        DS 68
        DS 70

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
_xTimerQueue:
        DS 4
        DS 4
        DS 4
//  979 /*-----------------------------------------------------------*/
//  980 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon2
          CFI Function _xTimerIsTimerActive
          CFI NoCalls
        CODE
//  981 BaseType_t xTimerIsTimerActive( TimerHandle_t xTimer )
//  982 {
_xTimerIsTimerActive:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
//  983 BaseType_t xTimerIsInActiveList;
//  984 Timer_t *pxTimer = ( Timer_t * ) xTimer;
//  985 
//  986 	configASSERT( xTimer );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_76  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_76:
        BNZ       ??xTimerPendFunctionCall_77  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_78  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_78:
        BR        S:??xTimerPendFunctionCall_78  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  987 
//  988 	/* Is the timer in the list of active timers? */
//  989 	taskENTER_CRITICAL();
??xTimerPendFunctionCall_77:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_79  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_79:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
//  990 	{
//  991 		/* Checking to see if it is in the NULL list in effect checks to see if
//  992 		it is referenced from either the current or the overflow timer lists in
//  993 		one go, but the logic has to be reversed, hence the '!'. */
//  994 		xTimerIsInActiveList = ( BaseType_t ) !( listIS_CONTAINED_WITHIN( NULL, &( pxTimer->xTimerListItem ) ) );
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_80  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_80:
        CLRW      BC                 ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
        ONEW      BC                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  995 	}
//  996 	taskEXIT_CRITICAL();
??xTimerIsTimerActive_0:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_81  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_81  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  997 
//  998 	return xTimerIsInActiveList;
??xTimerPendFunctionCall_81:
        MOVW      AX, BC             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 111 cycles
//  999 } /*lint !e818 Can't be pointer to const due to the typedef. */
// 1000 /*-----------------------------------------------------------*/
// 1001 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon2
          CFI Function _pvTimerGetTimerID
          CFI NoCalls
        CODE
// 1002 void *pvTimerGetTimerID( const TimerHandle_t xTimer )
// 1003 {
_pvTimerGetTimerID:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 1004 Timer_t * const pxTimer = ( Timer_t * ) xTimer;
// 1005 void *pvReturn;
// 1006 
// 1007 	configASSERT( xTimer );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_82  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_82:
        BNZ       ??xTimerPendFunctionCall_83  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_84  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_84:
        BR        S:??xTimerPendFunctionCall_84  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1008 
// 1009 	taskENTER_CRITICAL();
??xTimerPendFunctionCall_83:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_85  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_85:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1010 	{
// 1011 		pvReturn = pxTimer->pvTimerID;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 1012 	}
// 1013 	taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_86  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_86  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1014 
// 1015 	return pvReturn;
??xTimerPendFunctionCall_86:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 12 cycles
        ; ------------------------------------- Total: 105 cycles
// 1016 }
// 1017 /*-----------------------------------------------------------*/
// 1018 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock18 Using cfiCommon2
          CFI Function _vTimerSetTimerID
          CFI NoCalls
        CODE
// 1019 void vTimerSetTimerID( TimerHandle_t xTimer, void *pvNewID )
// 1020 {
_vTimerSetTimerID:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 8
// 1021 Timer_t * const pxTimer = ( Timer_t * ) xTimer;
// 1022 
// 1023 	configASSERT( xTimer );
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_87  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_87:
        BNZ       ??xTimerPendFunctionCall_88  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_89  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_89:
        BR        S:??xTimerPendFunctionCall_89  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1024 
// 1025 	taskENTER_CRITICAL();
??xTimerPendFunctionCall_88:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_90  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_90:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1026 	{
// 1027 		pxTimer->pvTimerID = pvNewID;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x1E          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1028 	}
// 1029 	taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_91  ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_91  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1030 }
??xTimerPendFunctionCall_91:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock18
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 111 cycles
// 1031 /*-----------------------------------------------------------*/
// 1032 
// 1033 #if( INCLUDE_xTimerPendFunctionCall == 1 )
// 1034 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock19 Using cfiCommon2
          CFI Function _xTimerPendFunctionCallFromISR
        CODE
// 1035 	BaseType_t xTimerPendFunctionCallFromISR( PendedFunction_t xFunctionToPend, void *pvParameter1, uint32_t ulParameter2, BaseType_t *pxHigherPriorityTaskWoken )
// 1036 	{
_xTimerPendFunctionCallFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 8
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 22
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+26
// 1037 	DaemonTaskMessage_t xMessage;
// 1038 	BaseType_t xReturn;
// 1039 
// 1040 		/* Complete the message with the function parameters and post it to the
// 1041 		daemon task. */
// 1042 		xMessage.xMessageID = tmrCOMMAND_EXECUTE_CALLBACK_FROM_ISR;
        MOVW      AX, #0xFFFE        ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1043 		xMessage.u.xCallbackParameters.pxCallbackFunction = xFunctionToPend;
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1044 		xMessage.u.xCallbackParameters.pvParameter1 = pvParameter1;
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 1045 		xMessage.u.xCallbackParameters.ulParameter2 = ulParameter2;
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
// 1046 
// 1047 		xReturn = xQueueSendFromISR( xTimerQueue, &xMessage, pxHigherPriorityTaskWoken );
// 1048 
// 1049 		tracePEND_FUNC_CALL_FROM_ISR( xFunctionToPend, pvParameter1, ulParameter2, xReturn );
// 1050 
// 1051 		return xReturn;
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, [SP+0x22]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xQueueGenericSendFromISR
        CALL      F:_xQueueGenericSendFromISR  ;; 3 cycles
        ADDW      SP, #0x1C          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock19
        ; ------------------------------------- Block: 62 cycles
        ; ------------------------------------- Total: 62 cycles
// 1052 	}
// 1053 
// 1054 #endif /* INCLUDE_xTimerPendFunctionCall */
// 1055 /*-----------------------------------------------------------*/
// 1056 
// 1057 #if( INCLUDE_xTimerPendFunctionCall == 1 )
// 1058 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock20 Using cfiCommon2
          CFI Function _xTimerPendFunctionCall
        CODE
// 1059 	BaseType_t xTimerPendFunctionCall( PendedFunction_t xFunctionToPend, void *pvParameter1, uint32_t ulParameter2, TickType_t xTicksToWait )
// 1060 	{
_xTimerPendFunctionCall:
        ; * Stack frame (at entry) *
        ; Param size: 8
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 22
        SUBW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+26
// 1061 	DaemonTaskMessage_t xMessage;
// 1062 	BaseType_t xReturn;
// 1063 
// 1064 		/* This function can only be called after a timer has been created or
// 1065 		after the scheduler has been started because, until then, the timer
// 1066 		queue does not exist. */
// 1067 		configASSERT( xTimerQueue );
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??xTimerPendFunctionCall_92  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??xTimerPendFunctionCall_92:
        BNZ       ??xTimerPendFunctionCall_93  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??xTimerPendFunctionCall_94  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??xTimerPendFunctionCall_94:
        BR        S:??xTimerPendFunctionCall_94  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1068 
// 1069 		/* Complete the message with the function parameters and post it to the
// 1070 		daemon task. */
// 1071 		xMessage.xMessageID = tmrCOMMAND_EXECUTE_CALLBACK;
??xTimerPendFunctionCall_93:
        MOVW      AX, #0xFFFF        ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1072 		xMessage.u.xCallbackParameters.pxCallbackFunction = xFunctionToPend;
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1073 		xMessage.u.xCallbackParameters.pvParameter1 = pvParameter1;
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        POP       HL                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x08], A       ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 1074 		xMessage.u.xCallbackParameters.ulParameter2 = ulParameter2;
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      [SP+0x0A], AX      ;; 1 cycle
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        MOVW      [SP+0x0C], AX      ;; 1 cycle
// 1075 
// 1076 		xReturn = xQueueSendToBack( xTimerQueue, &xMessage, xTicksToWait );
// 1077 
// 1078 		tracePEND_FUNC_CALL( xFunctionToPend, pvParameter1, ulParameter2, xReturn );
// 1079 
// 1080 		return xReturn;
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOVW      AX, [SP+0x22]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x20]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+30
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+32
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOV       ES, #BYTE3(_xTimerQueue)  ;; 1 cycle
        MOVW      HL, ES:_xTimerQueue  ;; 2 cycles
        MOV       A, ES:_xTimerQueue+2  ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+34
        POP       DE                 ;; 1 cycle
          CFI CFA SP+32
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
        ADDW      SP, #0x1C          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock20
        ; ------------------------------------- Block: 52 cycles
        ; ------------------------------------- Total: 98 cycles
// 1081 	}

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
?_0:
        DB "Tmr Svc"

        END
// 1082 
// 1083 #endif /* INCLUDE_xTimerPendFunctionCall */
// 1084 /*-----------------------------------------------------------*/
// 1085 
// 1086 /* This entire source file will be skipped if the application is not configured
// 1087 to include software timer functionality.  If you want to include software timer
// 1088 functionality then ensure configUSE_TIMERS is set to 1 in FreeRTOSConfig.h. */
// 1089 #endif /* configUSE_TIMERS == 1 */
// 1090 
// 1091 
// 1092 
// 
//   194 bytes in section .bssf
//     8 bytes in section .constf
// 3 618 bytes in section .textf
// 
//   194 bytes of DATA    memory
// 3 626 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
