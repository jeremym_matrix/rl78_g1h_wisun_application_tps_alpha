///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:56
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\phy_mac.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW350E.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\phy_mac.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\phy_mac.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHT_IAR_NOINIT 0xabdc5467
        #define SHF_WRITE 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN ___interrupt_tab_0x2C

        PUBLIC _RmWupTskEventId
        PUBLIC _RmWupTskEventIdNum
        PUBLIC _RpInitWupTsk
        PUBLIC _RpWupTsk
        PUBLIC _RpWupTskHdr
        PUBLIC __A_IF1
        PUBLIC __A_MK1
        PUBLIC __A_PR01
        PUBLIC __A_PR11
        PUBLIC __A_TDR00
        PUBLIC __A_TS0
        PUBLIC __A_TT0
        PUBLIC ___interrupt_0x2C
        PUBLIC _pRmMacOSCback
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET CODE
          CFI CFA SP+4
          CFI A SameValue
          CFI X SameValue
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H SameValue
          CFI L SameValue
          CFI CS_REG SameValue
          CFI ES_REG SameValue
          CFI ?RET Frame(CFA, -4)
          CFI MACRH SameValue
          CFI MACRL SameValue
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\phy_mac.c
//    1 /*******************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only
//    4  * intended for use with Renesas products. No other uses are authorized.
//    5  * This software is owned by Renesas Electronics Corporation and is protected under
//    6  * all applicable laws, including copyright laws.
//    7  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
//    8  * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
//    9  * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
//   10  * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
//   11  * DISCLAIMED.
//   12  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
//   13  * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
//   14  * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
//   15  * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
//   16  * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
//   17  * Renesas reserves the right, without notice, to make changes to this
//   18  * software and to discontinue the availability of this software.
//   19  * By using this software, you agree to the additional terms and
//   20  * conditions found by accessing the following link:
//   21  * http://www.renesas.com/disclaimer
//   22  *******************************************************************************/
//   23 /*******************************************************************************
//   24  * file name	: phy_mac.c
//   25  * description	: Mac Callback Operation
//   26  *******************************************************************************
//   27  * Copyright (C) 2014-2016 Renesas Electronics Corporation.
//   28  ******************************************************************************/
//   29 #include <intrinsics.h>
//   30 
//   31 #include <ior5f11fll.h>

        ASEGN `.sbss.noinit`:DATA:NOROOT,0fff18H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TDR00
// union <unnamed>#24 volatile __saddr __no_bit_access _A_TDR00
__A_TDR00:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe2H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_IF1
// union <unnamed>#160 volatile __sfr _A_IF1
__A_IF1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffe6H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_MK1
// union <unnamed>#175 volatile __sfr _A_MK1
__A_MK1:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffeaH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR01
// union <unnamed>#190 volatile __sfr _A_PR01
__A_PR01:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0fffeeH
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_PR11
// union <unnamed>#205 volatile __sfr _A_PR11
__A_PR11:
        DS 2
//   32 #include <ior5f11fll_ext.h>

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01b2H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TS0
// union <unnamed>#373 volatile __near _A_TS0
__A_TS0:
        DS 2

        ASEGN `.bss.noinit`:DATA:NOROOT,0f01b4H
        SECTION_TYPE SHT_IAR_NOINIT, SHF_WRITE
        SECTION_GROUP __A_TT0
// union <unnamed>#376 volatile __near _A_TT0
__A_TT0:
        DS 2
//   33 
//   34 /***********************************************************
//   35  * include
//   36  **********************************************************/
//   37 
//   38 #include "r_stdint.h"
//   39 #include "RL78G1H.h"
//   40 
//   41 /***********************************************************
//   42  * Interrupt Level
//   43  **********************************************************/
//   44 #define RP_PHY_INTLEVEL_SYSCALLTimer	(2)	// OS system call
//   45 #define RP_PHY_INTLEVEL_SYSCALLTimer_b0	(RP_PHY_INTLEVEL_SYSCALLTimer & 0x01)
//   46 #define RP_PHY_INTLEVEL_SYSCALLTimer_b1	((RP_PHY_INTLEVEL_SYSCALLTimer & 0x02) >> 1)
//   47 
//   48 /***********************************************************
//   49  * typedef
//   50  **********************************************************/
//   51 typedef void (*RmMacOSCallbackT)(uint8_t);

        SECTION `.bssf`:DATA:REORDER:NOROOT(1)
//   52 volatile RmMacOSCallbackT pRmMacOSCback;
//   53 volatile uint8_t RmWupTskEventId[2];
_RmWupTskEventId:
        DS 2
_pRmMacOSCback:
        DS 4

        SECTION `.bssf`:DATA:REORDER:NOROOT(0)
//   54 volatile uint8_t RmWupTskEventIdNum = 0;
_RmWupTskEventIdNum:
        DS 1
//   55 
//   56 /***********************************************************************
//   57  * program start
//   58  **********************************************************************/
//   59 
//   60 /***********************************************************************
//   61  *	function Name  : RpInitWupTsk
//   62  *	parameters     : none
//   63  *	return value   : none
//   64  *	description    : initial wakeup-task ram.
//   65  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _RpInitWupTsk
          CFI NoCalls
        CODE
//   66 void RpInitWupTsk( void )
//   67 {
_RpInitWupTsk:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
//   68 	uint8_t macnum;
//   69 
//   70 	for ( macnum=0; macnum < 2 ; macnum++ )
//   71 	{
//   72 		RmWupTskEventId[macnum] = 0;
        MOV       ES, #BYTE3(_RmWupTskEventId)  ;; 1 cycle
        CLRB      ES:_RmWupTskEventId  ;; 2 cycles
        CLRB      ES:_RmWupTskEventId+1  ;; 2 cycles
//   73 	}
//   74 	RmWupTskEventIdNum = 0;
        MOV       ES, #BYTE3(_RmWupTskEventIdNum)  ;; 1 cycle
        CLRB      ES:_RmWupTskEventIdNum  ;; 2 cycles
//   75 	return;
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 14 cycles
        ; ------------------------------------- Total: 14 cycles
//   76 }
//   77 
//   78 /***********************************************************************
//   79  *	function Name  : RpWupTsk
//   80  *	parameters     : eventId
//   81  *	return value   : none
//   82  *	description    : set wakeup-task handler.
//   83  **********************************************************************/

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _RpWupTsk
          CFI NoCalls
        CODE
//   84 void RpWupTsk( uint8_t eventId )
//   85 {
_RpWupTsk:
        ; * Stack frame (at entry) *
        ; Param size: 0
        ; Auto size: 0
        MOV       X, A               ;; 1 cycle
//   86 	if ( RmWupTskEventIdNum < 2 )
        MOV       ES, #BYTE3(_RmWupTskEventIdNum)  ;; 1 cycle
        MOV       A, ES:_RmWupTskEventIdNum  ;; 2 cycles
        CMP       A, #0x2            ;; 1 cycle
        BNC       ??RpWupTskHdr_0    ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
//   87 	{
//   88 		RmWupTskEventId[RmWupTskEventIdNum++] = eventId;
        MOV       C, ES:_RmWupTskEventIdNum  ;; 2 cycles
        MOV       A, C               ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       ES:_RmWupTskEventIdNum, A  ;; 2 cycles
        CLRB      B                  ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, #LWRD(_RmWupTskEventId)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RmWupTskEventId)  ;; 1 cycle
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 14 cycles
//   89 	}
//   90 
//   91 	/* generate timer interrupt */
//   92 	TMIF00 = 0;		// INTTM00 interrupt flag clear
??RpWupTskHdr_0:
        CLR1      0xFFFE2.4          ;; 2 cycles
//   93 	TDR00 = 1;
        MOVW      S:0xFFF18, #0x1    ;; 1 cycle
//   94 	TMPR100 = RP_PHY_INTLEVEL_SYSCALLTimer_b1;	//
        SET1      0xFFFEE.4          ;; 2 cycles
//   95 	TMPR000 = RP_PHY_INTLEVEL_SYSCALLTimer_b0;	// Level_2
        CLR1      0xFFFEA.4          ;; 2 cycles
//   96 	TMMK00 = 0;		// INTTM00 enabled
        CLR1      0xFFFE6.4          ;; 2 cycles
//   97 	TS0 |= TAU_CH0_START_TRG_ON;
        SET1      0xF01B2.0          ;; 2 cycles
//   98 }
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 17 cycles
        ; ------------------------------------- Total: 40 cycles
        REQUIRE __A_IF1
        REQUIRE __A_TDR00
        REQUIRE __A_PR11
        REQUIRE __A_PR01
        REQUIRE __A_MK1
        REQUIRE __A_TS0
//   99 
//  100 /***********************************************************************
//  101  *	function Name  : RpWupTskHdr
//  102  *	parameters     : none
//  103  *	return value   : none
//  104  *	description    : This handler is OS depending interrupt for "iset_flg".
//  105  **********************************************************************/
//  106 #if defined (__ICCRL78__)
//  107 #pragma vector = INTTM00_vect

        SECTION `.text`:CODE:NOROOT(0)
        CALL_GRAPH_ROOT _RpWupTskHdr, "interrupt"
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _RpWupTskHdr
        CODE
//  108 __interrupt void RpWupTskHdr(void)
//  109 #else
//  110 void RpWupTskHdr(void)
//  111 #endif
//  112 {
_RpWupTskHdr:
___interrupt_0x2C:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI X Frame(CFA, -6)
          CFI A Frame(CFA, -5)
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI C Frame(CFA, -8)
          CFI B Frame(CFA, -7)
          CFI CFA SP+8
        PUSH      DE                 ;; 1 cycle
          CFI E Frame(CFA, -10)
          CFI D Frame(CFA, -9)
          CFI CFA SP+10
        PUSH      HL                 ;; 1 cycle
          CFI L Frame(CFA, -12)
          CFI H Frame(CFA, -11)
          CFI CFA SP+12
        MOVW      AX, 0xFFFFC        ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        ; Auto size: 2
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
//  113 	uint8_t   j;
//  114 
//  115 	__enable_interrupt();
        EI                           ;; 4 cycles
//  116 
//  117 	// stop timer
//  118 	TMMK00 = 1;		// INTTM00 disabled
        SET1      0xFFFE6.4          ;; 2 cycles
//  119 	TT0 |= TAU_CH0_STOP_TRG_ON;
        MOVW      HL, #0x1B4         ;; 1 cycle
        SET1      [HL].0             ;; 2 cycles
//  120 	// Call MAC OS Callback
//  121 	for (j = 0; j < RmWupTskEventIdNum; j++)
        CLRB      A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        BR        S:??RpWupTskHdr_1  ;; 3 cycles
        ; ------------------------------------- Block: 21 cycles
//  122 	{
//  123 		(*pRmMacOSCback)(RmWupTskEventId[j]);
??RpWupTskHdr_2:
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        ADDW      AX, #LWRD(_RmWupTskEventId)  ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       ES, #BYTE3(_RmWupTskEventId)  ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
        MOVW      HL, #LWRD(_RmWupTskEventId+2)  ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       CS, A              ;; 1 cycle
        MOV       A, B               ;; 1 cycle
          CFI FunCall
        CALL      DE                 ;; 3 cycles
//  124 	}
        MOV       A, [SP]            ;; 1 cycle
        INC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 22 cycles
??RpWupTskHdr_1:
        MOV       A, [SP]            ;; 1 cycle
        MOV       ES, #BYTE3(_RmWupTskEventIdNum)  ;; 1 cycle
        MOV       X, ES:_RmWupTskEventIdNum  ;; 2 cycles
        CMP       A, X               ;; 1 cycle
        BC        ??RpWupTskHdr_2    ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
//  125 	RmWupTskEventIdNum = 0;
        CLRB      ES:_RmWupTskEventIdNum  ;; 2 cycles
//  126 }
        POP       AX                 ;; 1 cycle
          CFI CFA SP+14
        POP       AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      0xFFFFC, AX        ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI L SameValue
          CFI H SameValue
          CFI CFA SP+10
        POP       DE                 ;; 1 cycle
          CFI E SameValue
          CFI D SameValue
          CFI CFA SP+8
        POP       BC                 ;; 1 cycle
          CFI C SameValue
          CFI B SameValue
          CFI CFA SP+6
        POP       AX                 ;; 1 cycle
          CFI X SameValue
          CFI A SameValue
          CFI CFA SP+4
        RETI                         ;; 6 cycles
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 15 cycles
        ; ------------------------------------- Total: 67 cycles
        REQUIRE __A_MK1
        REQUIRE __A_TT0
        REQUIRE ___interrupt_tab_0x2C

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//  127 
//  128 /*******************************************************************************
//  129  * Copyright (C) 2014-2016 Renesas Electronics Corporation.
//  130  ******************************************************************************/
//  131 
// 
// 12 bytes in section .bss.noinit   (abs)
//  7 bytes in section .bssf
//  2 bytes in section .sbss.noinit  (abs)
// 80 bytes in section .text
// 70 bytes in section .textf
// 
// 80 bytes of CODE    memory
//  7 bytes of DATA    memory (+ 14 bytes shared)
// 70 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
