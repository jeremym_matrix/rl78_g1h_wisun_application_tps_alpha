///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:59
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\queue.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW360A.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\queue.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\queue.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_full_locale_support", "0"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1
        #define SHF_EXECINSTR 0x4

        EXTERN _usCriticalNesting
        EXTERN ?MEMCPY_FAR
        EXTERN _pvTaskIncrementMutexHeldCount
        EXTERN _vListInitialise
        EXTERN _vTaskMissedYield
        EXTERN _vTaskPlaceOnEventList
        EXTERN _vTaskPlaceOnEventListRestricted
        EXTERN _vTaskPriorityInherit
        EXTERN _vTaskSetTimeOutState
        EXTERN _vTaskSuspendAll
        EXTERN _xTaskCheckForTimeOut
        EXTERN _xTaskGetCurrentTaskHandle
        EXTERN _xTaskGetSchedulerState
        EXTERN _xTaskPriorityDisinherit
        EXTERN _xTaskRemoveFromEventList
        EXTERN _xTaskResumeAll

        PUBLIC _ucQueueGetQueueType
        PUBLIC _uxQueueGetQueueNumber
        PUBLIC _uxQueueMessagesWaiting
        PUBLIC _uxQueueMessagesWaitingFromISR
        PUBLIC _uxQueueSpacesAvailable
        PUBLIC _vQueueDelete
        PUBLIC _vQueueSetQueueNumber
        PUBLIC _vQueueWaitForMessageRestricted
        PUBLIC _xQueueAddToSet
        PUBLIC _xQueueCreateCountingSemaphoreStatic
        PUBLIC _xQueueCreateMutexStatic
        PUBLIC _xQueueGenericCreateStatic
        PUBLIC _xQueueGenericReceive
        PUBLIC _xQueueGenericReset
        PUBLIC _xQueueGenericSend
        PUBLIC _xQueueGenericSendFromISR
        PUBLIC _xQueueGiveFromISR
        PUBLIC _xQueueGiveMutexRecursive
        PUBLIC _xQueueIsQueueEmptyFromISR
        PUBLIC _xQueueIsQueueFullFromISR
        PUBLIC _xQueuePeekFromISR
        PUBLIC _xQueueReceiveFromISR
        PUBLIC _xQueueRemoveFromSet
        PUBLIC _xQueueSelectFromSet
        PUBLIC _xQueueSelectFromSetFromISR
        PUBLIC _xQueueTakeMutexRecursive
        
          CFI Names cfiNames0
          CFI StackFrame CFA SP NEARDATA
          CFI Resource A:8, X:8, B:8, C:8, D:8, E:8, H:8, L:8, CS_REG:4, ES_REG:4
          CFI VirtualResource ?RET:20
          CFI Resource MACRH:16, MACRL:16, W0:8, W1:8, W2:8, W3:8, W4:8, W5:8
          CFI Resource W6:8, W7:8, W8:8, W9:8, W10:8, W11:8, W12:8, W13:8, W14:8
          CFI Resource W15:8, W16:8, W17:8, W18:8, W19:8, W20:8, W21:8, W22:8
          CFI Resource W23:8, W24:8, W25:8, W26:8, W27:8, W28:8, W29:8, W30:8
          CFI Resource W31:8, W32:8, W33:8, W34:8, W35:8, W36:8, W37:8, W38:8
          CFI Resource W39:8, W40:8, W41:8, W42:8, W43:8, W44:8, W45:8, W46:8
          CFI Resource W47:8, W48:8, W49:8, W50:8, W51:8, W52:8, W53:8, W54:8
          CFI Resource W55:8, W56:8, W57:8, W58:8, W59:8, W60:8, W61:8, W62:8
          CFI Resource W63:8, W64:8, W65:8, W66:8, W67:8, W68:8, W69:8, W70:8
          CFI Resource W71:8, W72:8, W73:8, W74:8, W75:8, W76:8, W77:8, W78:8
          CFI Resource W79:8, W80:8, W81:8, W82:8, W83:8, W84:8, W85:8, W86:8
          CFI Resource W87:8, W88:8, W89:8, W90:8, W91:8, W92:8, W93:8, W94:8
          CFI Resource W95:8, W96:8, W97:8, W98:8, W99:8, W100:8, W101:8, W102:8
          CFI Resource W103:8, W104:8, W105:8, W106:8, W107:8, W108:8, W109:8
          CFI Resource W110:8, W111:8, W112:8, W113:8, W114:8, W115:8, W116:8
          CFI Resource W117:8, W118:8, W119:8, W120:8, W121:8, W122:8, W123:8
          CFI Resource W124:8, W125:8, W126:8, W127:8, SP:16, ?SPH:4
          CFI EndNames cfiNames0
        
          CFI Common cfiCommon0 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon0
        
        
          CFI Common cfiCommon1 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon1
        
        
          CFI Common cfiCommon2 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B SameValue
          CFI C SameValue
          CFI D Undefined
          CFI E Undefined
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon2
        
        
          CFI Common cfiCommon3 Using cfiNames0
          CFI CodeAlign 1
          CFI DataAlign 1
          CFI ReturnAddress ?RET FARCODE
          CFI CFA SP+4
          CFI A Undefined
          CFI X Undefined
          CFI B Undefined
          CFI C Undefined
          CFI D SameValue
          CFI E SameValue
          CFI H Undefined
          CFI L Undefined
          CFI CS_REG Undefined
          CFI ES_REG Undefined
          CFI ?RET Frame(CFA, -4)
          CFI MACRH Undefined
          CFI MACRL Undefined
          CFI W0 SameValue
          CFI W1 SameValue
          CFI W2 SameValue
          CFI W3 SameValue
          CFI W4 SameValue
          CFI W5 SameValue
          CFI W6 SameValue
          CFI W7 SameValue
          CFI W8 SameValue
          CFI W9 SameValue
          CFI W10 SameValue
          CFI W11 SameValue
          CFI W12 SameValue
          CFI W13 SameValue
          CFI W14 SameValue
          CFI W15 SameValue
          CFI W16 SameValue
          CFI W17 SameValue
          CFI W18 SameValue
          CFI W19 SameValue
          CFI W20 SameValue
          CFI W21 SameValue
          CFI W22 SameValue
          CFI W23 SameValue
          CFI W24 SameValue
          CFI W25 SameValue
          CFI W26 SameValue
          CFI W27 SameValue
          CFI W28 SameValue
          CFI W29 SameValue
          CFI W30 SameValue
          CFI W31 SameValue
          CFI W32 SameValue
          CFI W33 SameValue
          CFI W34 SameValue
          CFI W35 SameValue
          CFI W36 SameValue
          CFI W37 SameValue
          CFI W38 SameValue
          CFI W39 SameValue
          CFI W40 SameValue
          CFI W41 SameValue
          CFI W42 SameValue
          CFI W43 SameValue
          CFI W44 SameValue
          CFI W45 SameValue
          CFI W46 SameValue
          CFI W47 SameValue
          CFI W48 SameValue
          CFI W49 SameValue
          CFI W50 SameValue
          CFI W51 SameValue
          CFI W52 SameValue
          CFI W53 SameValue
          CFI W54 SameValue
          CFI W55 SameValue
          CFI W56 SameValue
          CFI W57 SameValue
          CFI W58 SameValue
          CFI W59 SameValue
          CFI W60 SameValue
          CFI W61 SameValue
          CFI W62 SameValue
          CFI W63 SameValue
          CFI W64 SameValue
          CFI W65 SameValue
          CFI W66 SameValue
          CFI W67 SameValue
          CFI W68 SameValue
          CFI W69 SameValue
          CFI W70 SameValue
          CFI W71 SameValue
          CFI W72 SameValue
          CFI W73 SameValue
          CFI W74 SameValue
          CFI W75 SameValue
          CFI W76 SameValue
          CFI W77 SameValue
          CFI W78 SameValue
          CFI W79 SameValue
          CFI W80 SameValue
          CFI W81 SameValue
          CFI W82 SameValue
          CFI W83 SameValue
          CFI W84 SameValue
          CFI W85 SameValue
          CFI W86 SameValue
          CFI W87 SameValue
          CFI W88 SameValue
          CFI W89 SameValue
          CFI W90 SameValue
          CFI W91 SameValue
          CFI W92 SameValue
          CFI W93 SameValue
          CFI W94 SameValue
          CFI W95 SameValue
          CFI W96 SameValue
          CFI W97 SameValue
          CFI W98 SameValue
          CFI W99 SameValue
          CFI W100 SameValue
          CFI W101 SameValue
          CFI W102 SameValue
          CFI W103 SameValue
          CFI W104 SameValue
          CFI W105 SameValue
          CFI W106 SameValue
          CFI W107 SameValue
          CFI W108 SameValue
          CFI W109 SameValue
          CFI W110 SameValue
          CFI W111 SameValue
          CFI W112 SameValue
          CFI W113 SameValue
          CFI W114 SameValue
          CFI W115 SameValue
          CFI W116 SameValue
          CFI W117 SameValue
          CFI W118 SameValue
          CFI W119 SameValue
          CFI W120 SameValue
          CFI W121 SameValue
          CFI W122 SameValue
          CFI W123 SameValue
          CFI W124 SameValue
          CFI W125 SameValue
          CFI W126 SameValue
          CFI W127 SameValue
          CFI ?SPH Undefined
          CFI EndCommon cfiCommon3
        
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\queue.c
//    1 /*
//    2     FreeRTOS V9.0.0 - Copyright (C) 2016 Real Time Engineers Ltd.
//    3     All rights reserved
//    4 
//    5     VISIT http://www.FreeRTOS.org TO ENSURE YOU ARE USING THE LATEST VERSION.
//    6 
//    7     This file is part of the FreeRTOS distribution.
//    8 
//    9     FreeRTOS is free software; you can redistribute it and/or modify it under
//   10     the terms of the GNU General Public License (version 2) as published by the
//   11     Free Software Foundation >>>> AND MODIFIED BY <<<< the FreeRTOS exception.
//   12 
//   13     ***************************************************************************
//   14     >>!   NOTE: The modification to the GPL is included to allow you to     !<<
//   15     >>!   distribute a combined work that includes FreeRTOS without being   !<<
//   16     >>!   obliged to provide the source code for proprietary components     !<<
//   17     >>!   outside of the FreeRTOS kernel.                                   !<<
//   18     ***************************************************************************
//   19 
//   20     FreeRTOS is distributed in the hope that it will be useful, but WITHOUT ANY
//   21     WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
//   22     FOR A PARTICULAR PURPOSE.  Full license text is available on the following
//   23     link: http://www.freertos.org/a00114.html
//   24 
//   25     ***************************************************************************
//   26      *                                                                       *
//   27      *    FreeRTOS provides completely free yet professionally developed,    *
//   28      *    robust, strictly quality controlled, supported, and cross          *
//   29      *    platform software that is more than just the market leader, it     *
//   30      *    is the industry's de facto standard.                               *
//   31      *                                                                       *
//   32      *    Help yourself get started quickly while simultaneously helping     *
//   33      *    to support the FreeRTOS project by purchasing a FreeRTOS           *
//   34      *    tutorial book, reference manual, or both:                          *
//   35      *    http://www.FreeRTOS.org/Documentation                              *
//   36      *                                                                       *
//   37     ***************************************************************************
//   38 
//   39     http://www.FreeRTOS.org/FAQHelp.html - Having a problem?  Start by reading
//   40     the FAQ page "My application does not run, what could be wrong?".  Have you
//   41     defined configASSERT()?
//   42 
//   43     http://www.FreeRTOS.org/support - In return for receiving this top quality
//   44     embedded software for free we request you assist our global community by
//   45     participating in the support forum.
//   46 
//   47     http://www.FreeRTOS.org/training - Investing in training allows your team to
//   48     be as productive as possible as early as possible.  Now you can receive
//   49     FreeRTOS training directly from Richard Barry, CEO of Real Time Engineers
//   50     Ltd, and the world's leading authority on the world's leading RTOS.
//   51 
//   52     http://www.FreeRTOS.org/plus - A selection of FreeRTOS ecosystem products,
//   53     including FreeRTOS+Trace - an indispensable productivity tool, a DOS
//   54     compatible FAT file system, and our tiny thread aware UDP/IP stack.
//   55 
//   56     http://www.FreeRTOS.org/labs - Where new FreeRTOS products go to incubate.
//   57     Come and try FreeRTOS+TCP, our new open source TCP/IP stack for FreeRTOS.
//   58 
//   59     http://www.OpenRTOS.com - Real Time Engineers ltd. license FreeRTOS to High
//   60     Integrity Systems ltd. to sell under the OpenRTOS brand.  Low cost OpenRTOS
//   61     licenses offer ticketed support, indemnification and commercial middleware.
//   62 
//   63     http://www.SafeRTOS.com - High Integrity Systems also provide a safety
//   64     engineered and independently SIL3 certified version for use in safety and
//   65     mission critical applications that require provable dependability.
//   66 
//   67     1 tab == 4 spaces!
//   68 */
//   69 
//   70 #include <stdlib.h>
//   71 #include <string.h>
//   72 
//   73 /* Defining MPU_WRAPPERS_INCLUDED_FROM_API_FILE prevents task.h from redefining
//   74 all the API functions to use the MPU wrappers.  That should only be done when
//   75 task.h is included from an application file. */
//   76 #define MPU_WRAPPERS_INCLUDED_FROM_API_FILE
//   77 
//   78 #include "FreeRTOS.h"
//   79 #include "task.h"
//   80 #include "queue.h"
//   81 
//   82 #if ( configUSE_CO_ROUTINES == 1 )
//   83 	#include "croutine.h"
//   84 #endif
//   85 
//   86 /* Lint e961 and e750 are suppressed as a MISRA exception justified because the
//   87 MPU ports require MPU_WRAPPERS_INCLUDED_FROM_API_FILE to be defined for the
//   88 header files above, but not in this file, in order to generate the correct
//   89 privileged Vs unprivileged linkage and placement. */
//   90 #undef MPU_WRAPPERS_INCLUDED_FROM_API_FILE /*lint !e961 !e750. */
//   91 
//   92 
//   93 /* Constants used with the cRxLock and cTxLock structure members. */
//   94 #define queueUNLOCKED					( ( int8_t ) -1 )
//   95 #define queueLOCKED_UNMODIFIED			( ( int8_t ) 0 )
//   96 
//   97 /* When the Queue_t structure is used to represent a base queue its pcHead and
//   98 pcTail members are used as pointers into the queue storage area.  When the
//   99 Queue_t structure is used to represent a mutex pcHead and pcTail pointers are
//  100 not necessary, and the pcHead pointer is set to NULL to indicate that the
//  101 pcTail pointer actually points to the mutex holder (if any).  Map alternative
//  102 names to the pcHead and pcTail structure members to ensure the readability of
//  103 the code is maintained despite this dual use of two structure members.  An
//  104 alternative implementation would be to use a union, but use of a union is
//  105 against the coding standard (although an exception to the standard has been
//  106 permitted where the dual use also significantly changes the type of the
//  107 structure member). */
//  108 #define pxMutexHolder					pcTail
//  109 #define uxQueueType						pcHead
//  110 #define queueQUEUE_IS_MUTEX				NULL
//  111 
//  112 /* Semaphores do not actually store or copy data, so have an item size of
//  113 zero. */
//  114 #define queueSEMAPHORE_QUEUE_ITEM_LENGTH ( ( UBaseType_t ) 0 )
//  115 #define queueMUTEX_GIVE_BLOCK_TIME		 ( ( TickType_t ) 0U )
//  116 
//  117 #if( configUSE_PREEMPTION == 0 )
//  118 	/* If the cooperative scheduler is being used then a yield should not be
//  119 	performed just because a higher priority task has been woken. */
//  120 	#define queueYIELD_IF_USING_PREEMPTION()
//  121 #else
//  122 	#define queueYIELD_IF_USING_PREEMPTION() portYIELD_WITHIN_API()
//  123 #endif
//  124 
//  125 /*
//  126  * Definition of the queue used by the scheduler.
//  127  * Items are queued by copy, not reference.  See the following link for the
//  128  * rationale: http://www.freertos.org/Embedded-RTOS-Queues.html
//  129  */
//  130 typedef struct QueueDefinition
//  131 {
//  132 	int8_t *pcHead;					/*< Points to the beginning of the queue storage area. */
//  133 	int8_t *pcTail;					/*< Points to the byte at the end of the queue storage area.  Once more byte is allocated than necessary to store the queue items, this is used as a marker. */
//  134 	int8_t *pcWriteTo;				/*< Points to the free next place in the storage area. */
//  135 
//  136 	union							/* Use of a union is an exception to the coding standard to ensure two mutually exclusive structure members don't appear simultaneously (wasting RAM). */
//  137 	{
//  138 		int8_t *pcReadFrom;			/*< Points to the last place that a queued item was read from when the structure is used as a queue. */
//  139 		UBaseType_t uxRecursiveCallCount;/*< Maintains a count of the number of times a recursive mutex has been recursively 'taken' when the structure is used as a mutex. */
//  140 	} u;
//  141 
//  142 	List_t xTasksWaitingToSend;		/*< List of tasks that are blocked waiting to post onto this queue.  Stored in priority order. */
//  143 	List_t xTasksWaitingToReceive;	/*< List of tasks that are blocked waiting to read from this queue.  Stored in priority order. */
//  144 
//  145 	volatile UBaseType_t uxMessagesWaiting;/*< The number of items currently in the queue. */
//  146 	UBaseType_t uxLength;			/*< The length of the queue defined as the number of items it will hold, not the number of bytes. */
//  147 	UBaseType_t uxItemSize;			/*< The size of each items that the queue will hold. */
//  148 
//  149 	volatile int8_t cRxLock;		/*< Stores the number of items received from the queue (removed from the queue) while the queue was locked.  Set to queueUNLOCKED when the queue is not locked. */
//  150 	volatile int8_t cTxLock;		/*< Stores the number of items transmitted to the queue (added to the queue) while the queue was locked.  Set to queueUNLOCKED when the queue is not locked. */
//  151 
//  152 	#if( ( configSUPPORT_STATIC_ALLOCATION == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) )
//  153 		uint8_t ucStaticallyAllocated;	/*< Set to pdTRUE if the memory used by the queue was statically allocated to ensure no attempt is made to free the memory. */
//  154 	#endif
//  155 
//  156 	#if ( configUSE_QUEUE_SETS == 1 )
//  157 		struct QueueDefinition *pxQueueSetContainer;
//  158 	#endif
//  159 
//  160 	#if ( configUSE_TRACE_FACILITY == 1 )
//  161 		UBaseType_t uxQueueNumber;
//  162 		uint8_t ucQueueType;
//  163 	#endif
//  164 
//  165 } xQUEUE;
//  166 
//  167 /* The old xQUEUE name is maintained above then typedefed to the new Queue_t
//  168 name below to enable the use of older kernel aware debuggers. */
//  169 typedef xQUEUE Queue_t;
//  170 
//  171 /*-----------------------------------------------------------*/
//  172 
//  173 /*
//  174  * The queue registry is just a means for kernel aware debuggers to locate
//  175  * queue structures.  It has no other purpose so is an optional component.
//  176  */
//  177 #if ( configQUEUE_REGISTRY_SIZE > 0 )
//  178 
//  179 	/* The type stored within the queue registry array.  This allows a name
//  180 	to be assigned to each queue making kernel aware debugging a little
//  181 	more user friendly. */
//  182 	typedef struct QUEUE_REGISTRY_ITEM
//  183 	{
//  184 		const char *pcQueueName; /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
//  185 		QueueHandle_t xHandle;
//  186 	} xQueueRegistryItem;
//  187 
//  188 	/* The old xQueueRegistryItem name is maintained above then typedefed to the
//  189 	new xQueueRegistryItem name below to enable the use of older kernel aware
//  190 	debuggers. */
//  191 	typedef xQueueRegistryItem QueueRegistryItem_t;
//  192 
//  193 	/* The queue registry is simply an array of QueueRegistryItem_t structures.
//  194 	The pcQueueName member of a structure being NULL is indicative of the
//  195 	array position being vacant. */
//  196 	PRIVILEGED_DATA QueueRegistryItem_t xQueueRegistry[ configQUEUE_REGISTRY_SIZE ];
//  197 
//  198 #endif /* configQUEUE_REGISTRY_SIZE */
//  199 
//  200 /*
//  201  * Unlocks a queue locked by a call to prvLockQueue.  Locking a queue does not
//  202  * prevent an ISR from adding or removing items to the queue, but does prevent
//  203  * an ISR from removing tasks from the queue event lists.  If an ISR finds a
//  204  * queue is locked it will instead increment the appropriate queue lock count
//  205  * to indicate that a task may require unblocking.  When the queue in unlocked
//  206  * these lock counts are inspected, and the appropriate action taken.
//  207  */
//  208 static void prvUnlockQueue( Queue_t * const pxQueue ) PRIVILEGED_FUNCTION;
//  209 
//  210 /*
//  211  * Uses a critical section to determine if there is any data in a queue.
//  212  *
//  213  * @return pdTRUE if the queue contains no items, otherwise pdFALSE.
//  214  */
//  215 static BaseType_t prvIsQueueEmpty( const Queue_t *pxQueue ) PRIVILEGED_FUNCTION;
//  216 
//  217 /*
//  218  * Uses a critical section to determine if there is any space in a queue.
//  219  *
//  220  * @return pdTRUE if there is no space, otherwise pdFALSE;
//  221  */
//  222 static BaseType_t prvIsQueueFull( const Queue_t *pxQueue ) PRIVILEGED_FUNCTION;
//  223 
//  224 /*
//  225  * Copies an item into the queue, either at the front of the queue or the
//  226  * back of the queue.
//  227  */
//  228 static BaseType_t prvCopyDataToQueue( Queue_t * const pxQueue, const void *pvItemToQueue, const BaseType_t xPosition ) PRIVILEGED_FUNCTION;
//  229 
//  230 /*
//  231  * Copies an item out of a queue.
//  232  */
//  233 static void prvCopyDataFromQueue( Queue_t * const pxQueue, void * const pvBuffer ) PRIVILEGED_FUNCTION;
//  234 
//  235 #if ( configUSE_QUEUE_SETS == 1 )
//  236 	/*
//  237 	 * Checks to see if a queue is a member of a queue set, and if so, notifies
//  238 	 * the queue set that the queue contains data.
//  239 	 */
//  240 	static BaseType_t prvNotifyQueueSetContainer( const Queue_t * const pxQueue, const BaseType_t xCopyPosition ) PRIVILEGED_FUNCTION;
//  241 #endif
//  242 
//  243 /*
//  244  * Called after a Queue_t structure has been allocated either statically or
//  245  * dynamically to fill in the structure's members.
//  246  */
//  247 static void prvInitialiseNewQueue( const UBaseType_t uxQueueLength, const UBaseType_t uxItemSize, uint8_t *pucQueueStorage, const uint8_t ucQueueType, Queue_t *pxNewQueue ) PRIVILEGED_FUNCTION;
//  248 
//  249 /*
//  250  * Mutexes are a special type of queue.  When a mutex is created, first the
//  251  * queue is created, then prvInitialiseMutex() is called to configure the queue
//  252  * as a mutex.
//  253  */
//  254 #if( configUSE_MUTEXES == 1 )
//  255 	static void prvInitialiseMutex( Queue_t *pxNewQueue ) PRIVILEGED_FUNCTION;
//  256 #endif
//  257 
//  258 /*-----------------------------------------------------------*/
//  259 
//  260 /*
//  261  * Macro to mark a queue as locked.  Locking a queue prevents an ISR from
//  262  * accessing the queue event lists.
//  263  */
//  264 #define prvLockQueue( pxQueue )								\ 
//  265 	taskENTER_CRITICAL();									\ 
//  266 	{														\ 
//  267 		if( ( pxQueue )->cRxLock == queueUNLOCKED )			\ 
//  268 		{													\ 
//  269 			( pxQueue )->cRxLock = queueLOCKED_UNMODIFIED;	\ 
//  270 		}													\ 
//  271 		if( ( pxQueue )->cTxLock == queueUNLOCKED )			\ 
//  272 		{													\ 
//  273 			( pxQueue )->cTxLock = queueLOCKED_UNMODIFIED;	\ 
//  274 		}													\ 
//  275 	}														\ 
//  276 	taskEXIT_CRITICAL()
//  277 /*-----------------------------------------------------------*/
//  278 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock0 Using cfiCommon0
          CFI Function _xQueueGenericReset
        CODE
//  279 BaseType_t xQueueGenericReset( QueueHandle_t xQueue, BaseType_t xNewQueue )
//  280 {
_xQueueGenericReset:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
//  281 Queue_t * const pxQueue = ( Queue_t * ) xQueue;
//  282 
//  283 	configASSERT( pxQueue );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_0  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_0:
        BNZ       ??prvNotifyQueueSetContainer_1  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_2  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_2:
        BR        S:??prvNotifyQueueSetContainer_2  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  284 
//  285 	taskENTER_CRITICAL();
??prvNotifyQueueSetContainer_1:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_3  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_3:
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
//  286 	{
//  287 		pxQueue->pcTail = pxQueue->pcHead + ( pxQueue->uxLength * pxQueue->uxItemSize );
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MULHU                        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  288 		pxQueue->uxMessagesWaiting = ( UBaseType_t ) 0U;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  289 		pxQueue->pcWriteTo = pxQueue->pcHead;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  290 		pxQueue->u.pcReadFrom = pxQueue->pcHead + ( ( pxQueue->uxLength - ( UBaseType_t ) 1U ) * pxQueue->uxItemSize );
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        DECW      AX                 ;; 1 cycle
        MULHU                        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+10
        XCHW      AX, HL             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        POP       BC                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  291 		pxQueue->cRxLock = queueUNLOCKED;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x3A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xFF           ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  292 		pxQueue->cTxLock = queueUNLOCKED;
        INCW      HL                 ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
//  293 
//  294 		if( xNewQueue == pdFALSE )
        MOVW      AX, [SP]           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_4  ;; 4 cycles
        ; ------------------------------------- Block: 140 cycles
//  295 		{
//  296 			/* If there are tasks blocked waiting to read from the queue, then
//  297 			the tasks will remain blocked as after this function exits the queue
//  298 			will still be empty.  If there are tasks blocked waiting to write to
//  299 			the queue, then one should be unblocked as after this function exits
//  300 			it will be possible to write to it. */
//  301 			if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToSend ) ) == pdFALSE )
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_5  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
//  302 			{
//  303 				if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToSend ) ) != pdFALSE )
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_5  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  304 				{
//  305 					queueYIELD_IF_USING_PREEMPTION();
        BRK                          ;; 5 cycles
        BR        S:??prvNotifyQueueSetContainer_5  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
//  306 				}
//  307 				else
//  308 				{
//  309 					mtCOVERAGE_TEST_MARKER();
//  310 				}
//  311 			}
//  312 			else
//  313 			{
//  314 				mtCOVERAGE_TEST_MARKER();
//  315 			}
//  316 		}
//  317 		else
//  318 		{
//  319 			/* Ensure the event queues start in the correct state. */
//  320 			vListInitialise( &( pxQueue->xTasksWaitingToSend ) );
??prvNotifyQueueSetContainer_4:
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
//  321 			vListInitialise( &( pxQueue->xTasksWaitingToReceive ) );
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vListInitialise
        CALL      F:_vListInitialise  ;; 3 cycles
        ; ------------------------------------- Block: 15 cycles
//  322 		}
//  323 	}
//  324 	taskEXIT_CRITICAL();
??prvNotifyQueueSetContainer_5:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_6  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_6  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
//  325 
//  326 	/* A value is returned for calling semantic consistency with previous
//  327 	versions. */
//  328 	return pdPASS;
??prvNotifyQueueSetContainer_6:
        ONEW      AX                 ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock0
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 270 cycles
//  329 }
//  330 /*-----------------------------------------------------------*/
//  331 
//  332 #if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  333 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock1 Using cfiCommon0
          CFI Function _xQueueGenericCreateStatic
        CODE
//  334 	QueueHandle_t xQueueGenericCreateStatic( const UBaseType_t uxQueueLength, const UBaseType_t uxItemSize, uint8_t *pucQueueStorage, StaticQueue_t *pxStaticQueue, const uint8_t ucQueueType )
//  335 	{
_xQueueGenericCreateStatic:
        ; * Stack frame (at entry) *
        ; Param size: 8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        MOV       A, E               ;; 1 cycle
        MOV       B, A               ;; 1 cycle
//  336 	Queue_t *pxNewQueue;
//  337 
//  338 		configASSERT( uxQueueLength > ( UBaseType_t ) 0 );
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_7  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_8  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_8:
        BR        S:??prvNotifyQueueSetContainer_8  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  339 
//  340 		/* The StaticQueue_t structure and the queue storage area must be
//  341 		supplied. */
//  342 		configASSERT( pxStaticQueue != NULL );
??prvNotifyQueueSetContainer_7:
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_9  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_9:
        BNZ       ??prvNotifyQueueSetContainer_10  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_11  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_11:
        BR        S:??prvNotifyQueueSetContainer_11  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  343 
//  344 		/* A queue storage area should be provided if the item size is not 0, and
//  345 		should not be provided if the item size is 0. */
//  346 		configASSERT( !( ( pucQueueStorage != NULL ) && ( uxItemSize == 0 ) ) );
??prvNotifyQueueSetContainer_10:
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_12  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_12:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_13  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_14  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_15  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_15:
        BR        S:??prvNotifyQueueSetContainer_15  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_13:
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_16  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
//  347 		configASSERT( !( ( pucQueueStorage == NULL ) && ( uxItemSize != 0 ) ) );
//  348 
//  349 		#if( configASSERT_DEFINED == 1 )
//  350 		{
//  351 			/* Sanity check that the size of the structure used to declare a
//  352 			variable of type StaticQueue_t or StaticSemaphore_t equals the size of
//  353 			the real queue and semaphore structures. */
//  354 			volatile size_t xSize = sizeof( StaticQueue_t );
??prvNotifyQueueSetContainer_14:
        MOVW      AX, #0x44          ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  355 			configASSERT( xSize == sizeof( Queue_t ) );
        MOVW      AX, [SP]           ;; 1 cycle
        CMPW      AX, #0x44          ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_17  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  356 		}
//  357 		#endif /* configASSERT_DEFINED */
//  358 
//  359 		/* The address of a statically allocated queue was passed in, use it.
//  360 		The address of a statically allocated storage area was also passed in
//  361 		but is already set. */
//  362 		pxNewQueue = ( Queue_t * ) pxStaticQueue; /*lint !e740 Unusual cast is ok as the structures are designed to have the same alignment, and the size is checked by an assert. */
//  363 
//  364 		if( pxNewQueue != NULL )
//  365 		{
//  366 			#if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
//  367 			{
//  368 				/* Queues can be allocated wither statically or dynamically, so
//  369 				note this queue was allocated statically in case the queue is
//  370 				later deleted. */
//  371 				pxNewQueue->ucStaticallyAllocated = pdTRUE;
//  372 			}
//  373 			#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
//  374 
//  375 			prvInitialiseNewQueue( uxQueueLength, uxItemSize, pucQueueStorage, ucQueueType, pxNewQueue );
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_18  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, X               ;; 1 cycle
        BR        S:??prvNotifyQueueSetContainer_19  ;; 3 cycles
          CFI CFA SP+10
        ; ------------------------------------- Block: 13 cycles
??prvNotifyQueueSetContainer_16:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_20  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_20:
        BR        S:??prvNotifyQueueSetContainer_20  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_17:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_21  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_21:
        BR        S:??prvNotifyQueueSetContainer_21  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_18:
        MOVW      AX, HL             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, [SP]            ;; 1 cycle
        ; ------------------------------------- Block: 11 cycles
??prvNotifyQueueSetContainer_19:
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+10
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOV       A, B               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        ONEW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xQueueGenericReset
        CALL      F:_xQueueGenericReset  ;; 3 cycles
        MOV       A, [SP]            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
//  376 		}
//  377 
//  378 		return pxNewQueue;
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock1
        ; ------------------------------------- Block: 57 cycles
        ; ------------------------------------- Total: 254 cycles
//  379 	}
//  380 
//  381 #endif /* configSUPPORT_STATIC_ALLOCATION */
//  382 /*-----------------------------------------------------------*/
//  383 
//  384 #if( configSUPPORT_DYNAMIC_ALLOCATION == 1 )
//  385 
//  386 	QueueHandle_t xQueueGenericCreate( const UBaseType_t uxQueueLength, const UBaseType_t uxItemSize, const uint8_t ucQueueType )
//  387 	{
//  388 	Queue_t *pxNewQueue;
//  389 	size_t xQueueSizeInBytes;
//  390 	uint8_t *pucQueueStorage;
//  391 
//  392 		configASSERT( uxQueueLength > ( UBaseType_t ) 0 );
//  393 
//  394 		if( uxItemSize == ( UBaseType_t ) 0 )
//  395 		{
//  396 			/* There is not going to be a queue storage area. */
//  397 			xQueueSizeInBytes = ( size_t ) 0;
//  398 		}
//  399 		else
//  400 		{
//  401 			/* Allocate enough space to hold the maximum number of items that
//  402 			can be in the queue at any time. */
//  403 			xQueueSizeInBytes = ( size_t ) ( uxQueueLength * uxItemSize ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
//  404 		}
//  405 
//  406 		pxNewQueue = ( Queue_t * ) pvPortMalloc( sizeof( Queue_t ) + xQueueSizeInBytes );
//  407 
//  408 		if( pxNewQueue != NULL )
//  409 		{
//  410 			/* Jump past the queue structure to find the location of the queue
//  411 			storage area. */
//  412 			pucQueueStorage = ( ( uint8_t * ) pxNewQueue ) + sizeof( Queue_t );
//  413 
//  414 			#if( configSUPPORT_STATIC_ALLOCATION == 1 )
//  415 			{
//  416 				/* Queues can be created either statically or dynamically, so
//  417 				note this task was created dynamically in case it is later
//  418 				deleted. */
//  419 				pxNewQueue->ucStaticallyAllocated = pdFALSE;
//  420 			}
//  421 			#endif /* configSUPPORT_STATIC_ALLOCATION */
//  422 
//  423 			prvInitialiseNewQueue( uxQueueLength, uxItemSize, pucQueueStorage, ucQueueType, pxNewQueue );
//  424 		}
//  425 
//  426 		return pxNewQueue;
//  427 	}
//  428 
//  429 #endif /* configSUPPORT_STATIC_ALLOCATION */
//  430 /*-----------------------------------------------------------*/
//  431 
//  432 static void prvInitialiseNewQueue( const UBaseType_t uxQueueLength, const UBaseType_t uxItemSize, uint8_t *pucQueueStorage, const uint8_t ucQueueType, Queue_t *pxNewQueue )
//  433 {
//  434 	/* Remove compiler warnings about unused parameters should
//  435 	configUSE_TRACE_FACILITY not be set to 1. */
//  436 	( void ) ucQueueType;
//  437 
//  438 	if( uxItemSize == ( UBaseType_t ) 0 )
//  439 	{
//  440 		/* No RAM was allocated for the queue storage area, but PC head cannot
//  441 		be set to NULL because NULL is used as a key to say the queue is used as
//  442 		a mutex.  Therefore just set pcHead to point to the queue as a benign
//  443 		value that is known to be within the memory map. */
//  444 		pxNewQueue->pcHead = ( int8_t * ) pxNewQueue;
//  445 	}
//  446 	else
//  447 	{
//  448 		/* Set the head to the start of the queue storage area. */
//  449 		pxNewQueue->pcHead = ( int8_t * ) pucQueueStorage;
//  450 	}
//  451 
//  452 	/* Initialise the queue members as described where the queue type is
//  453 	defined. */
//  454 	pxNewQueue->uxLength = uxQueueLength;
//  455 	pxNewQueue->uxItemSize = uxItemSize;
//  456 	( void ) xQueueGenericReset( pxNewQueue, pdTRUE );
//  457 
//  458 	#if ( configUSE_TRACE_FACILITY == 1 )
//  459 	{
//  460 		pxNewQueue->ucQueueType = ucQueueType;
//  461 	}
//  462 	#endif /* configUSE_TRACE_FACILITY */
//  463 
//  464 	#if( configUSE_QUEUE_SETS == 1 )
//  465 	{
//  466 		pxNewQueue->pxQueueSetContainer = NULL;
//  467 	}
//  468 	#endif /* configUSE_QUEUE_SETS */
//  469 
//  470 	traceQUEUE_CREATE( pxNewQueue );
//  471 }
//  472 /*-----------------------------------------------------------*/
//  473 
//  474 #if( configUSE_MUTEXES == 1 )
//  475 
//  476 	static void prvInitialiseMutex( Queue_t *pxNewQueue )
//  477 	{
//  478 		if( pxNewQueue != NULL )
//  479 		{
//  480 			/* The queue create function will set all the queue structure members
//  481 			correctly for a generic queue, but this function is creating a
//  482 			mutex.  Overwrite those members that need to be set differently -
//  483 			in particular the information required for priority inheritance. */
//  484 			pxNewQueue->pxMutexHolder = NULL;
//  485 			pxNewQueue->uxQueueType = queueQUEUE_IS_MUTEX;
//  486 
//  487 			/* In case this is a recursive mutex. */
//  488 			pxNewQueue->u.uxRecursiveCallCount = 0;
//  489 
//  490 			traceCREATE_MUTEX( pxNewQueue );
//  491 
//  492 			/* Start with the semaphore in the expected state. */
//  493 			( void ) xQueueGenericSend( pxNewQueue, NULL, ( TickType_t ) 0U, queueSEND_TO_BACK );
//  494 		}
//  495 		else
//  496 		{
//  497 			traceCREATE_MUTEX_FAILED();
//  498 		}
//  499 	}
//  500 
//  501 #endif /* configUSE_MUTEXES */
//  502 /*-----------------------------------------------------------*/
//  503 
//  504 #if( ( configUSE_MUTEXES == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) )
//  505 
//  506 	QueueHandle_t xQueueCreateMutex( const uint8_t ucQueueType )
//  507 	{
//  508 	Queue_t *pxNewQueue;
//  509 	const UBaseType_t uxMutexLength = ( UBaseType_t ) 1, uxMutexSize = ( UBaseType_t ) 0;
//  510 
//  511 		pxNewQueue = ( Queue_t * ) xQueueGenericCreate( uxMutexLength, uxMutexSize, ucQueueType );
//  512 		prvInitialiseMutex( pxNewQueue );
//  513 
//  514 		return pxNewQueue;
//  515 	}
//  516 
//  517 #endif /* configUSE_MUTEXES */
//  518 /*-----------------------------------------------------------*/
//  519 
//  520 #if( ( configUSE_MUTEXES == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 1 ) )
//  521 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock2 Using cfiCommon1
          CFI Function _xQueueCreateMutexStatic
        CODE
//  522 	QueueHandle_t xQueueCreateMutexStatic( const uint8_t ucQueueType, StaticQueue_t *pxStaticQueue )
//  523 	{
_xQueueCreateMutexStatic:
        ; * Stack frame (at entry) *
        ; Param size: 0
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
        MOV       E, A               ;; 1 cycle
//  524 	Queue_t *pxNewQueue;
//  525 	const UBaseType_t uxMutexLength = ( UBaseType_t ) 1, uxMutexSize = ( UBaseType_t ) 0;
//  526 
//  527 		/* Prevent compiler warnings about unused parameters if
//  528 		configUSE_TRACE_FACILITY does not equal 1. */
//  529 		( void ) ucQueueType;
//  530 
//  531 		pxNewQueue = ( Queue_t * ) xQueueGenericCreateStatic( uxMutexLength, uxMutexSize, NULL, pxStaticQueue, ucQueueType );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        CLRW      BC                 ;; 1 cycle
        ONEW      AX                 ;; 1 cycle
          CFI FunCall _xQueueGenericCreateStatic
        CALL      F:_xQueueGenericCreateStatic  ;; 3 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
        MOV       [SP+0x02], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  532 		prvInitialiseMutex( pxNewQueue );
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_22  ;; 4 cycles
        ; ------------------------------------- Block: 35 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_22:
        BZ        ??prvNotifyQueueSetContainer_23  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+12
        ; ------------------------------------- Block: 40 cycles
//  533 
//  534 		return pxNewQueue;
??prvNotifyQueueSetContainer_23:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
          CFI EndBlock cfiBlock2
        ; ------------------------------------- Block: 3 cycles
        ; ------------------------------------- Total: 84 cycles
        REQUIRE ?Subroutine1
        ; // Fall through to label ?Subroutine1
//  535 	}

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock3 Using cfiCommon1
          CFI NoFunction
          CFI CFA SP+12
        CODE
?Subroutine1:
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock3
        ; ------------------------------------- Block: 9 cycles
        ; ------------------------------------- Total: 9 cycles
//  536 
//  537 #endif /* configUSE_MUTEXES */
//  538 /*-----------------------------------------------------------*/
//  539 
//  540 #if ( ( configUSE_MUTEXES == 1 ) && ( INCLUDE_xSemaphoreGetMutexHolder == 1 ) )
//  541 
//  542 	void* xQueueGetMutexHolder( QueueHandle_t xSemaphore )
//  543 	{
//  544 	void *pxReturn;
//  545 
//  546 		/* This function is called by xSemaphoreGetMutexHolder(), and should not
//  547 		be called directly.  Note:  This is a good way of determining if the
//  548 		calling task is the mutex holder, but not a good way of determining the
//  549 		identity of the mutex holder, as the holder may change between the
//  550 		following critical section exiting and the function returning. */
//  551 		taskENTER_CRITICAL();
//  552 		{
//  553 			if( ( ( Queue_t * ) xSemaphore )->uxQueueType == queueQUEUE_IS_MUTEX )
//  554 			{
//  555 				pxReturn = ( void * ) ( ( Queue_t * ) xSemaphore )->pxMutexHolder;
//  556 			}
//  557 			else
//  558 			{
//  559 				pxReturn = NULL;
//  560 			}
//  561 		}
//  562 		taskEXIT_CRITICAL();
//  563 
//  564 		return pxReturn;
//  565 	} /*lint !e818 xSemaphore cannot be a pointer to const because it is a typedef. */
//  566 
//  567 #endif
//  568 /*-----------------------------------------------------------*/
//  569 
//  570 #if ( configUSE_RECURSIVE_MUTEXES == 1 )
//  571 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock4 Using cfiCommon2
          CFI Function _xQueueGiveMutexRecursive
        CODE
//  572 	BaseType_t xQueueGiveMutexRecursive( QueueHandle_t xMutex )
//  573 	{
_xQueueGiveMutexRecursive:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
//  574 	BaseType_t xReturn;
//  575 	Queue_t * const pxMutex = ( Queue_t * ) xMutex;
//  576 
//  577 		configASSERT( pxMutex );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_24  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_24:
        BNZ       ??prvNotifyQueueSetContainer_25  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_26  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_26:
        BR        S:??prvNotifyQueueSetContainer_26  ;; 3 cycles
          CFI FunCall _xTaskGetCurrentTaskHandle
        ; ------------------------------------- Block: 3 cycles
//  578 
//  579 		/* If this is the task that holds the mutex then pxMutexHolder will not
//  580 		change outside of this task.  If this task does not hold the mutex then
//  581 		pxMutexHolder can never coincidentally equal the tasks handle, and as
//  582 		this is the only condition we are interested in it does not matter if
//  583 		pxMutexHolder is accessed simultaneously by another task.  Therefore no
//  584 		mutual exclusion is required to test the pxMutexHolder variable. */
//  585 		if( pxMutex->pxMutexHolder == ( void * ) xTaskGetCurrentTaskHandle() ) /*lint !e961 Not a redundant cast as TaskHandle_t is a typedef. */
??prvNotifyQueueSetContainer_25:
        CALL      F:_xTaskGetCurrentTaskHandle  ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_27  ;; 4 cycles
        ; ------------------------------------- Block: 36 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_27:
        BNZ       ??prvNotifyQueueSetContainer_28  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  586 		{
//  587 			traceGIVE_MUTEX_RECURSIVE( pxMutex );
//  588 
//  589 			/* uxRecursiveCallCount cannot be zero if pxMutexHolder is equal to
//  590 			the task handle, therefore no underflow check is required.  Also,
//  591 			uxRecursiveCallCount is only modified by the mutex holder, and as
//  592 			there can only be one, no mutual exclusion is required to modify the
//  593 			uxRecursiveCallCount member. */
//  594 			( pxMutex->u.uxRecursiveCallCount )--;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  595 
//  596 			/* Has the recursive call count unwound to 0? */
//  597 			if( pxMutex->u.uxRecursiveCallCount == ( UBaseType_t ) 0 )
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_29  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
//  598 			{
//  599 				/* Return the mutex.  This will automatically unblock any other
//  600 				task that might be waiting to access the mutex. */
//  601 				( void ) xQueueGenericSend( pxMutex, NULL, queueMUTEX_GIVE_BLOCK_TIME, queueSEND_TO_BACK );
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       X, A               ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _xQueueGenericSend
        CALL      F:_xQueueGenericSend  ;; 3 cycles
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+12
        ; ------------------------------------- Block: 18 cycles
//  602 			}
//  603 			else
//  604 			{
//  605 				mtCOVERAGE_TEST_MARKER();
//  606 			}
//  607 
//  608 			xReturn = pdPASS;
??prvNotifyQueueSetContainer_29:
        ONEW      AX                 ;; 1 cycle
        BR        S:??prvNotifyQueueSetContainer_30  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
//  609 		}
//  610 		else
//  611 		{
//  612 			/* The mutex cannot be given because the calling task is not the
//  613 			holder. */
//  614 			xReturn = pdFAIL;
??prvNotifyQueueSetContainer_28:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
//  615 
//  616 			traceGIVE_MUTEX_RECURSIVE_FAILED( pxMutex );
//  617 		}
//  618 
//  619 		return xReturn;
??prvNotifyQueueSetContainer_30:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock4
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 133 cycles
//  620 	}
//  621 
//  622 #endif /* configUSE_RECURSIVE_MUTEXES */
//  623 /*-----------------------------------------------------------*/
//  624 
//  625 #if ( configUSE_RECURSIVE_MUTEXES == 1 )
//  626 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock5 Using cfiCommon2
          CFI Function _xQueueTakeMutexRecursive
        CODE
//  627 	BaseType_t xQueueTakeMutexRecursive( QueueHandle_t xMutex, TickType_t xTicksToWait )
//  628 	{
_xQueueTakeMutexRecursive:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
//  629 	BaseType_t xReturn;
//  630 	Queue_t * const pxMutex = ( Queue_t * ) xMutex;
//  631 
//  632 		configASSERT( pxMutex );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_31  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_31:
        BNZ       ??prvNotifyQueueSetContainer_32  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_33  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_33:
        BR        S:??prvNotifyQueueSetContainer_33  ;; 3 cycles
          CFI FunCall _xTaskGetCurrentTaskHandle
        ; ------------------------------------- Block: 3 cycles
//  633 
//  634 		/* Comments regarding mutual exclusion as per those within
//  635 		xQueueGiveMutexRecursive(). */
//  636 
//  637 		traceTAKE_MUTEX_RECURSIVE( pxMutex );
//  638 
//  639 		if( pxMutex->pxMutexHolder == ( void * ) xTaskGetCurrentTaskHandle() ) /*lint !e961 Cast is not redundant as TaskHandle_t is a typedef. */
??prvNotifyQueueSetContainer_32:
        CALL      F:_xTaskGetCurrentTaskHandle  ;; 3 cycles
        MOV       [SP+0x02], A       ;; 1 cycle
        XCHW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, D               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_34  ;; 4 cycles
        ; ------------------------------------- Block: 36 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_34:
        BNZ       ??prvNotifyQueueSetContainer_35  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  640 		{
//  641 			( pxMutex->u.uxRecursiveCallCount )++;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
//  642 			xReturn = pdPASS;
        ONEW      AX                 ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        BR        S:??prvNotifyQueueSetContainer_36  ;; 3 cycles
        ; ------------------------------------- Block: 20 cycles
//  643 		}
??prvNotifyQueueSetContainer_35:
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
//  644 		else
//  645 		{
//  646 			xReturn = xQueueGenericReceive( pxMutex, NULL, xTicksToWait, pdFALSE );
        MOVW      HL, #0x0           ;; 1 cycle
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        CLRB      X                  ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _xQueueGenericReceive
        CALL      F:_xQueueGenericReceive  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
//  647 
//  648 			/* pdPASS will only be returned if the mutex was successfully
//  649 			obtained.  The calling task may have entered the Blocked state
//  650 			before reaching here. */
//  651 			if( xReturn != pdFAIL )
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+12
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_36  ;; 4 cycles
        ; ------------------------------------- Block: 28 cycles
//  652 			{
//  653 				( pxMutex->u.uxRecursiveCallCount )++;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        INCW      AX                 ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        ; ------------------------------------- Block: 10 cycles
//  654 			}
//  655 			else
//  656 			{
//  657 				traceTAKE_MUTEX_RECURSIVE_FAILED( pxMutex );
//  658 			}
//  659 		}
//  660 
//  661 		return xReturn;
??prvNotifyQueueSetContainer_36:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock5
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 149 cycles
//  662 	}
//  663 
//  664 #endif /* configUSE_RECURSIVE_MUTEXES */
//  665 /*-----------------------------------------------------------*/
//  666 
//  667 #if( ( configUSE_COUNTING_SEMAPHORES == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 1 ) )
//  668 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock6 Using cfiCommon3
          CFI Function _xQueueCreateCountingSemaphoreStatic
        CODE
//  669 	QueueHandle_t xQueueCreateCountingSemaphoreStatic( const UBaseType_t uxMaxCount, const UBaseType_t uxInitialCount, StaticQueue_t *pxStaticQueue )
//  670 	{
_xQueueCreateCountingSemaphoreStatic:
        ; * Stack frame (at entry) *
        ; Param size: 4
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        ; Auto size: 8
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+12
//  671 	QueueHandle_t xHandle;
//  672 
//  673 		configASSERT( uxMaxCount != 0 );
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_37  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_38  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_38:
        BR        S:??prvNotifyQueueSetContainer_38  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_37:
        MOVW      AX, BC             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  674 		configASSERT( uxInitialCount <= uxMaxCount );
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        CMPW      AX, HL             ;; 1 cycle
        BNC       ??prvNotifyQueueSetContainer_39  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_40  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_40:
        BR        S:??prvNotifyQueueSetContainer_40  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  675 
//  676 		xHandle = xQueueGenericCreateStatic( uxMaxCount, queueSEMAPHORE_QUEUE_ITEM_LENGTH, NULL, pxStaticQueue, queueQUEUE_TYPE_COUNTING_SEMAPHORE );
??prvNotifyQueueSetContainer_39:
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        CLRB      X                  ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        CLRW      AX                 ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        MOV       E, #0x2            ;; 1 cycle
        CLRW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
          CFI FunCall _xQueueGenericCreateStatic
        CALL      F:_xQueueGenericCreateStatic  ;; 3 cycles
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+12
        MOVW      HL, AX             ;; 1 cycle
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
//  677 
//  678 		if( xHandle != NULL )
        MOV       A, H               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_41  ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_41:
        BZ        ??prvNotifyQueueSetContainer_42  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  679 		{
//  680 			( ( Queue_t * ) xHandle )->uxMessagesWaiting = uxInitialCount;
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        ; ------------------------------------- Block: 8 cycles
//  681 
//  682 			traceCREATE_COUNTING_SEMAPHORE();
//  683 		}
//  684 		else
//  685 		{
//  686 			traceCREATE_COUNTING_SEMAPHORE_FAILED();
//  687 		}
//  688 
//  689 		return xHandle;
??prvNotifyQueueSetContainer_42:
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        BR        F:?Subroutine1     ;; 3 cycles
          CFI EndBlock cfiBlock6
        ; ------------------------------------- Block: 4 cycles
        ; ------------------------------------- Total: 109 cycles
//  690 	}
//  691 
//  692 #endif /* ( ( configUSE_COUNTING_SEMAPHORES == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) ) */
//  693 /*-----------------------------------------------------------*/
//  694 
//  695 #if( ( configUSE_COUNTING_SEMAPHORES == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) )
//  696 
//  697 	QueueHandle_t xQueueCreateCountingSemaphore( const UBaseType_t uxMaxCount, const UBaseType_t uxInitialCount )
//  698 	{
//  699 	QueueHandle_t xHandle;
//  700 
//  701 		configASSERT( uxMaxCount != 0 );
//  702 		configASSERT( uxInitialCount <= uxMaxCount );
//  703 
//  704 		xHandle = xQueueGenericCreate( uxMaxCount, queueSEMAPHORE_QUEUE_ITEM_LENGTH, queueQUEUE_TYPE_COUNTING_SEMAPHORE );
//  705 
//  706 		if( xHandle != NULL )
//  707 		{
//  708 			( ( Queue_t * ) xHandle )->uxMessagesWaiting = uxInitialCount;
//  709 
//  710 			traceCREATE_COUNTING_SEMAPHORE();
//  711 		}
//  712 		else
//  713 		{
//  714 			traceCREATE_COUNTING_SEMAPHORE_FAILED();
//  715 		}
//  716 
//  717 		return xHandle;
//  718 	}
//  719 
//  720 #endif /* ( ( configUSE_COUNTING_SEMAPHORES == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) ) */
//  721 /*-----------------------------------------------------------*/
//  722 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock7 Using cfiCommon2
          CFI Function _xQueueGenericSend
        CODE
//  723 BaseType_t xQueueGenericSend( QueueHandle_t xQueue, const void * const pvItemToQueue, TickType_t xTicksToWait, const BaseType_t xCopyPosition )
//  724 {
_xQueueGenericSend:
        ; * Stack frame (at entry) *
        ; Param size: 6
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 16
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+20
//  725 BaseType_t xEntryTimeSet = pdFALSE, xYieldRequired;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
//  726 TimeOut_t xTimeOut;
//  727 Queue_t * const pxQueue = ( Queue_t * ) xQueue;
//  728 
//  729 	configASSERT( pxQueue );
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_43  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_43:
        BNZ       ??prvNotifyQueueSetContainer_44  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_45  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_45:
        BR        S:??prvNotifyQueueSetContainer_45  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  730 	configASSERT( !( ( pvItemToQueue == NULL ) && ( pxQueue->uxItemSize != ( UBaseType_t ) 0U ) ) );
??prvNotifyQueueSetContainer_44:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_46  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_46:
        BNZ       ??prvNotifyQueueSetContainer_47  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_48  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  731 	configASSERT( !( ( xCopyPosition == queueOVERWRITE ) && ( pxQueue->uxLength != 1 ) ) );
??prvNotifyQueueSetContainer_47:
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        CMPW      AX, #0x2           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_49  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_50  ;; 4 cycles
          CFI FunCall _xTaskGetSchedulerState
        ; ------------------------------------- Block: 12 cycles
//  732 	#if ( ( INCLUDE_xTaskGetSchedulerState == 1 ) || ( configUSE_TIMERS == 1 ) )
//  733 	{
//  734 		configASSERT( !( ( xTaskGetSchedulerState() == taskSCHEDULER_SUSPENDED ) && ( xTicksToWait != 0 ) ) );
??prvNotifyQueueSetContainer_49:
        CALL      F:_xTaskGetSchedulerState  ;; 3 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_51  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_51  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_52  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_52:
        BR        S:??prvNotifyQueueSetContainer_52  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  735 	}
??prvNotifyQueueSetContainer_48:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_53  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_53:
        BR        S:??prvNotifyQueueSetContainer_53  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_50:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_54  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_54:
        BR        S:??prvNotifyQueueSetContainer_54  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  736 	#endif
//  737 
//  738 
//  739 	/* This function relaxes the coding standard somewhat to allow return
//  740 	statements within the function itself.  This is done in the interest
//  741 	of execution time efficiency. */
//  742 	for( ;; )
//  743 	{
//  744 		taskENTER_CRITICAL();
//  745 		{
//  746 			/* Is there room on the queue now?  The running task must be the
//  747 			highest priority task wanting to access the queue.  If the head item
//  748 			in the queue is to be overwritten then it does not matter if the
//  749 			queue is full. */
//  750 			if( ( pxQueue->uxMessagesWaiting < pxQueue->uxLength ) || ( xCopyPosition == queueOVERWRITE ) )
//  751 			{
//  752 				traceQUEUE_SEND( pxQueue );
//  753 				xYieldRequired = prvCopyDataToQueue( pxQueue, pvItemToQueue, xCopyPosition );
//  754 
//  755 				#if ( configUSE_QUEUE_SETS == 1 )
//  756 				{
//  757 					if( pxQueue->pxQueueSetContainer != NULL )
//  758 					{
//  759 						if( prvNotifyQueueSetContainer( pxQueue, xCopyPosition ) != pdFALSE )
//  760 						{
//  761 							/* The queue is a member of a queue set, and posting
//  762 							to the queue set caused a higher priority task to
//  763 							unblock. A context switch is required. */
//  764 							queueYIELD_IF_USING_PREEMPTION();
//  765 						}
//  766 						else
//  767 						{
//  768 							mtCOVERAGE_TEST_MARKER();
//  769 						}
//  770 					}
//  771 					else
//  772 					{
//  773 						/* If there was a task waiting for data to arrive on the
//  774 						queue then unblock it now. */
//  775 						if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
//  776 						{
//  777 							if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
//  778 							{
//  779 								/* The unblocked task has a priority higher than
//  780 								our own so yield immediately.  Yes it is ok to
//  781 								do this from within the critical section - the
//  782 								kernel takes care of that. */
//  783 								queueYIELD_IF_USING_PREEMPTION();
//  784 							}
//  785 							else
//  786 							{
//  787 								mtCOVERAGE_TEST_MARKER();
//  788 							}
//  789 						}
//  790 						else if( xYieldRequired != pdFALSE )
//  791 						{
//  792 							/* This path is a special case that will only get
//  793 							executed if the task was holding multiple mutexes
//  794 							and the mutexes were given back in an order that is
//  795 							different to that in which they were taken. */
//  796 							queueYIELD_IF_USING_PREEMPTION();
//  797 						}
//  798 						else
//  799 						{
//  800 							mtCOVERAGE_TEST_MARKER();
//  801 						}
//  802 					}
//  803 				}
//  804 				#else /* configUSE_QUEUE_SETS */
//  805 				{
//  806 					/* If there was a task waiting for data to arrive on the
//  807 					queue then unblock it now. */
//  808 					if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
//  809 					{
//  810 						if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
//  811 						{
//  812 							/* The unblocked task has a priority higher than
//  813 							our own so yield immediately.  Yes it is ok to do
//  814 							this from within the critical section - the kernel
//  815 							takes care of that. */
//  816 							queueYIELD_IF_USING_PREEMPTION();
//  817 						}
//  818 						else
//  819 						{
//  820 							mtCOVERAGE_TEST_MARKER();
//  821 						}
//  822 					}
//  823 					else if( xYieldRequired != pdFALSE )
//  824 					{
//  825 						/* This path is a special case that will only get
//  826 						executed if the task was holding multiple mutexes and
//  827 						the mutexes were given back in an order that is
//  828 						different to that in which they were taken. */
//  829 						queueYIELD_IF_USING_PREEMPTION();
//  830 					}
//  831 					else
//  832 					{
//  833 						mtCOVERAGE_TEST_MARKER();
//  834 					}
//  835 				}
//  836 				#endif /* configUSE_QUEUE_SETS */
//  837 
//  838 				taskEXIT_CRITICAL();
//  839 				return pdPASS;
//  840 			}
//  841 			else
//  842 			{
//  843 				if( xTicksToWait == ( TickType_t ) 0 )
//  844 				{
//  845 					/* The queue was full and no block time is specified (or
//  846 					the block time has expired) so leave now. */
//  847 					taskEXIT_CRITICAL();
//  848 
//  849 					/* Return to the original privilege level before exiting
//  850 					the function. */
//  851 					traceQUEUE_SEND_FAILED( pxQueue );
//  852 					return errQUEUE_FULL;
//  853 				}
//  854 				else if( xEntryTimeSet == pdFALSE )
//  855 				{
//  856 					/* The queue was full and a block time was specified so
//  857 					configure the timeout structure. */
//  858 					vTaskSetTimeOutState( &xTimeOut );
//  859 					xEntryTimeSet = pdTRUE;
//  860 				}
//  861 				else
//  862 				{
//  863 					/* Entry time was already set. */
//  864 					mtCOVERAGE_TEST_MARKER();
//  865 				}
//  866 			}
//  867 		}
//  868 		taskEXIT_CRITICAL();
//  869 
//  870 		/* Interrupts and other tasks can send to and receive from the queue
//  871 		now the critical section has been exited. */
//  872 
//  873 		vTaskSuspendAll();
//  874 		prvLockQueue( pxQueue );
//  875 
//  876 		/* Update the timeout state to see if it has expired yet. */
//  877 		if( xTaskCheckForTimeOut( &xTimeOut, &xTicksToWait ) == pdFALSE )
//  878 		{
//  879 			if( prvIsQueueFull( pxQueue ) != pdFALSE )
//  880 			{
//  881 				traceBLOCKING_ON_QUEUE_SEND( pxQueue );
//  882 				vTaskPlaceOnEventList( &( pxQueue->xTasksWaitingToSend ), xTicksToWait );
??xQueueGenericSend_0:
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vTaskPlaceOnEventList
        CALL      F:_vTaskPlaceOnEventList  ;; 3 cycles
//  883 
//  884 				/* Unlocking the queue means queue events can effect the
//  885 				event list.  It is possible	that interrupts occurring now
//  886 				remove this task from the event	list again - but as the
//  887 				scheduler is suspended the task will go onto the pending
//  888 				ready last instead of the actual ready list. */
//  889 				prvUnlockQueue( pxQueue );
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvUnlockQueue
        CALL      F:_prvUnlockQueue  ;; 3 cycles
//  890 
//  891 				/* Resuming the scheduler will move tasks from the pending
//  892 				ready list into the ready list - so it is feasible that this
//  893 				task is already in a ready list before it yields - in which
//  894 				case the yield will not cause a context switch unless there
//  895 				is also a higher priority task in the pending ready list. */
//  896 				if( xTaskResumeAll() == pdFALSE )
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+20
        CMPW      AX, #0x0           ;; 1 cycle
        SKNZ                         ;; 1 cycle
        ; ------------------------------------- Block: 28 cycles
//  897 				{
//  898 					portYIELD_WITHIN_API();
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
//  899 				}
//  900 			}
??prvNotifyQueueSetContainer_51:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_55  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_55:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_56  ;; 4 cycles
        ; ------------------------------------- Block: 22 cycles
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        CMPW      AX, #0x2           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_56  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      AX, [SP+0x16]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_57  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_58  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _vTaskSetTimeOutState
        CALL      F:_vTaskSetTimeOutState  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
??prvNotifyQueueSetContainer_58:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_59  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_59  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 6 cycles
??prvNotifyQueueSetContainer_59:
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_60  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_60:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x3A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        INC       A                  ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_61  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_61:
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        INC       A                  ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_62  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_62:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_63  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_63  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??prvNotifyQueueSetContainer_63:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x14          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
          CFI FunCall _xTaskCheckForTimeOut
        CALL      F:_xTaskCheckForTimeOut  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_64  ;; 4 cycles
          CFI FunCall _prvIsQueueFull
        ; ------------------------------------- Block: 21 cycles
        CALL      F:_prvIsQueueFull  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??xQueueGenericSend_0  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
//  901 			else
//  902 			{
//  903 				/* Try again. */
//  904 				prvUnlockQueue( pxQueue );
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvUnlockQueue
        CALL      F:_prvUnlockQueue  ;; 3 cycles
//  905 				( void ) xTaskResumeAll();
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        BR        R:??prvNotifyQueueSetContainer_51  ;; 3 cycles
        ; ------------------------------------- Block: 14 cycles
//  906 			}
??xQueueGenericSend_1:
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_65  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??xQueueGenericSend_2:
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_66  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        BR        S:??prvNotifyQueueSetContainer_67  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_65:
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_66:
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_67:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_68  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_68  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??prvNotifyQueueSetContainer_68:
        ONEW      AX                 ;; 1 cycle
        BR        R:??prvNotifyQueueSetContainer_69  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??prvNotifyQueueSetContainer_57:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_70  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_70  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        BR        S:??prvNotifyQueueSetContainer_70  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
??prvNotifyQueueSetContainer_56:
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+24
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+22
          CFI FunCall _prvCopyDataToQueue
        CALL      F:_prvCopyDataToQueue  ;; 3 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_71  ;; 4 cycles
        ; ------------------------------------- Block: 44 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_71:
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??xQueueGenericSend_1  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvNotifyQueueSetContainer
        CALL      F:_prvNotifyQueueSetContainer  ;; 3 cycles
        BR        R:??xQueueGenericSend_2  ;; 3 cycles
          CFI FunCall _prvUnlockQueue
        ; ------------------------------------- Block: 8 cycles
//  907 		}
//  908 		else
//  909 		{
//  910 			/* The timeout has expired. */
//  911 			prvUnlockQueue( pxQueue );
??prvNotifyQueueSetContainer_64:
        CALL      F:_prvUnlockQueue  ;; 3 cycles
//  912 			( void ) xTaskResumeAll();
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
//  913 
//  914 			traceQUEUE_SEND_FAILED( pxQueue );
//  915 			return errQUEUE_FULL;
        ; ------------------------------------- Block: 6 cycles
??prvNotifyQueueSetContainer_70:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvNotifyQueueSetContainer_69:
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock7
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 568 cycles
//  916 		}
//  917 	}
//  918 }
//  919 /*-----------------------------------------------------------*/
//  920 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock8 Using cfiCommon2
          CFI Function _xQueueGenericSendFromISR
        CODE
//  921 BaseType_t xQueueGenericSendFromISR( QueueHandle_t xQueue, const void * const pvItemToQueue, BaseType_t * const pxHigherPriorityTaskWoken, const BaseType_t xCopyPosition )
//  922 {
_xQueueGenericSendFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 6
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 12
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
//  923 BaseType_t xReturn;
//  924 UBaseType_t uxSavedInterruptStatus;
//  925 Queue_t * const pxQueue = ( Queue_t * ) xQueue;
//  926 
//  927 	configASSERT( pxQueue );
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_72  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_72:
        BNZ       ??prvNotifyQueueSetContainer_73  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_74  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_74:
        BR        S:??prvNotifyQueueSetContainer_74  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  928 	configASSERT( !( ( pvItemToQueue == NULL ) && ( pxQueue->uxItemSize != ( UBaseType_t ) 0U ) ) );
??prvNotifyQueueSetContainer_73:
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_75  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_75:
        BNZ       ??prvNotifyQueueSetContainer_76  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_77  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  929 	configASSERT( !( ( xCopyPosition == queueOVERWRITE ) && ( pxQueue->uxLength != 1 ) ) );
??prvNotifyQueueSetContainer_76:
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        CMPW      AX, #0x2           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_78  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x1           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_79  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  930 
//  931 	/* RTOS ports that support interrupt nesting have the concept of a maximum
//  932 	system call (or maximum API call) interrupt priority.  Interrupts that are
//  933 	above the maximum system call priority are kept permanently enabled, even
//  934 	when the RTOS kernel is in a critical section, but cannot make any calls to
//  935 	FreeRTOS API functions.  If configASSERT() is defined in FreeRTOSConfig.h
//  936 	then portASSERT_IF_INTERRUPT_PRIORITY_INVALID() will result in an assertion
//  937 	failure if a FreeRTOS API function is called from an interrupt that has been
//  938 	assigned a priority above the configured maximum system call priority.
//  939 	Only FreeRTOS functions that end in FromISR can be called from interrupts
//  940 	that have been assigned a priority at or (logically) below the maximum
//  941 	system call	interrupt priority.  FreeRTOS maintains a separate interrupt
//  942 	safe API to ensure interrupt entry is as fast and as simple as possible.
//  943 	More information (albeit Cortex-M specific) is provided on the following
//  944 	link: http://www.freertos.org/RTOS-Cortex-M3-M4.html */
//  945 	portASSERT_IF_INTERRUPT_PRIORITY_INVALID();
//  946 
//  947 	/* Similar to xQueueGenericSend, except without blocking if there is no room
//  948 	in the queue.  Also don't directly wake a task that was blocked on a queue
//  949 	read, instead return a flag to say whether a context switch is required or
//  950 	not (i.e. has a task with a higher priority than us been woken by this
//  951 	post). */
//  952 	uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
??prvNotifyQueueSetContainer_78:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
//  953 	{
//  954 		if( ( pxQueue->uxMessagesWaiting < pxQueue->uxLength ) || ( xCopyPosition == queueOVERWRITE ) )
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        BC        ??prvNotifyQueueSetContainer_80  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        CMPW      AX, #0x2           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_81  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
//  955 		{
//  956 			const int8_t cTxLock = pxQueue->cTxLock;
??prvNotifyQueueSetContainer_80:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x3B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
//  957 
//  958 			traceQUEUE_SEND_FROM_ISR( pxQueue );
//  959 
//  960 			/* Semaphores use xQueueGiveFromISR(), so pxQueue will not be a
//  961 			semaphore or mutex.  That means prvCopyDataToQueue() cannot result
//  962 			in a task disinheriting a priority and prvCopyDataToQueue() can be
//  963 			called here even though the disinherit function does not check if
//  964 			the scheduler is suspended before accessing the ready lists. */
//  965 			( void ) prvCopyDataToQueue( pxQueue, pvItemToQueue, xCopyPosition );
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _prvCopyDataToQueue
        CALL      F:_prvCopyDataToQueue  ;; 3 cycles
//  966 
//  967 			/* The event list is not altered if the queue is locked.  This will
//  968 			be done when the queue is unlocked later. */
//  969 			if( cTxLock == queueUNLOCKED )
        MOV       A, [SP+0x02]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        INC       A                  ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_82  ;; 4 cycles
        ; ------------------------------------- Block: 36 cycles
//  970 			{
//  971 				#if ( configUSE_QUEUE_SETS == 1 )
//  972 				{
//  973 					if( pxQueue->pxQueueSetContainer != NULL )
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_83  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_83:
        BZ        ??prvNotifyQueueSetContainer_84  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
//  974 					{
//  975 						if( prvNotifyQueueSetContainer( pxQueue, xCopyPosition ) != pdFALSE )
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvNotifyQueueSetContainer
        CALL      F:_prvNotifyQueueSetContainer  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_85  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        BR        S:??prvNotifyQueueSetContainer_86  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  976 						{
//  977 							/* The queue is a member of a queue set, and posting
//  978 							to the queue set caused a higher priority task to
//  979 							unblock.  A context switch is required. */
//  980 							if( pxHigherPriorityTaskWoken != NULL )
//  981 							{
//  982 								*pxHigherPriorityTaskWoken = pdTRUE;
//  983 							}
//  984 							else
//  985 							{
//  986 								mtCOVERAGE_TEST_MARKER();
//  987 							}
//  988 						}
//  989 						else
//  990 						{
//  991 							mtCOVERAGE_TEST_MARKER();
//  992 						}
//  993 					}
??prvNotifyQueueSetContainer_77:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_87  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_87:
        BR        S:??prvNotifyQueueSetContainer_87  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_79:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_88  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_88:
        BR        S:??prvNotifyQueueSetContainer_88  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
//  994 					else
//  995 					{
//  996 						if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
??prvNotifyQueueSetContainer_84:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_86  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  997 						{
//  998 							if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_86  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
//  999 							{
// 1000 								/* The task waiting has a higher priority so
// 1001 								record that a context switch is required. */
// 1002 								if( pxHigherPriorityTaskWoken != NULL )
??prvNotifyQueueSetContainer_85:
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_89  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_89:
        BZ        ??prvNotifyQueueSetContainer_86  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1003 								{
// 1004 									*pxHigherPriorityTaskWoken = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        BR        S:??prvNotifyQueueSetContainer_86  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1005 								}
// 1006 								else
// 1007 								{
// 1008 									mtCOVERAGE_TEST_MARKER();
// 1009 								}
// 1010 							}
// 1011 							else
// 1012 							{
// 1013 								mtCOVERAGE_TEST_MARKER();
// 1014 							}
// 1015 						}
// 1016 						else
// 1017 						{
// 1018 							mtCOVERAGE_TEST_MARKER();
// 1019 						}
// 1020 					}
// 1021 				}
// 1022 				#else /* configUSE_QUEUE_SETS */
// 1023 				{
// 1024 					if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
// 1025 					{
// 1026 						if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
// 1027 						{
// 1028 							/* The task waiting has a higher priority so record that a
// 1029 							context	switch is required. */
// 1030 							if( pxHigherPriorityTaskWoken != NULL )
// 1031 							{
// 1032 								*pxHigherPriorityTaskWoken = pdTRUE;
// 1033 							}
// 1034 							else
// 1035 							{
// 1036 								mtCOVERAGE_TEST_MARKER();
// 1037 							}
// 1038 						}
// 1039 						else
// 1040 						{
// 1041 							mtCOVERAGE_TEST_MARKER();
// 1042 						}
// 1043 					}
// 1044 					else
// 1045 					{
// 1046 						mtCOVERAGE_TEST_MARKER();
// 1047 					}
// 1048 				}
// 1049 				#endif /* configUSE_QUEUE_SETS */
// 1050 			}
// 1051 			else
// 1052 			{
// 1053 				/* Increment the lock count so the task that unlocks the queue
// 1054 				knows that data was posted while it was locked. */
// 1055 				pxQueue->cTxLock = ( int8_t ) ( cTxLock + 1 );
??prvNotifyQueueSetContainer_82:
        MOV       A, [SP]            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        INC       B                  ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x3B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 11 cycles
// 1056 			}
// 1057 
// 1058 			xReturn = pdPASS;
??prvNotifyQueueSetContainer_86:
        ONEW      AX                 ;; 1 cycle
        BR        S:??prvNotifyQueueSetContainer_90  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1059 		}
// 1060 		else
// 1061 		{
// 1062 			traceQUEUE_SEND_FROM_ISR_FAILED( pxQueue );
// 1063 			xReturn = errQUEUE_FULL;
??prvNotifyQueueSetContainer_81:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvNotifyQueueSetContainer_90:
        MOVW      HL, AX             ;; 1 cycle
// 1064 		}
// 1065 	}
// 1066 	portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        ROL       A, 0x1             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 1067 
// 1068 	return xReturn;
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock8
        ; ------------------------------------- Block: 20 cycles
        ; ------------------------------------- Total: 325 cycles
// 1069 }
// 1070 /*-----------------------------------------------------------*/
// 1071 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock9 Using cfiCommon2
          CFI Function _xQueueGiveFromISR
        CODE
// 1072 BaseType_t xQueueGiveFromISR( QueueHandle_t xQueue, BaseType_t * const pxHigherPriorityTaskWoken )
// 1073 {
_xQueueGiveFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
// 1074 BaseType_t xReturn;
// 1075 UBaseType_t uxSavedInterruptStatus;
// 1076 Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 1077 
// 1078 	/* Similar to xQueueGenericSendFromISR() but used with semaphores where the
// 1079 	item size is 0.  Don't directly wake a task that was blocked on a queue
// 1080 	read, instead return a flag to say whether a context switch is required or
// 1081 	not (i.e. has a task with a higher priority than us been woken by this
// 1082 	post). */
// 1083 
// 1084 	configASSERT( pxQueue );
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_91  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_91:
        BNZ       ??prvNotifyQueueSetContainer_92  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_93  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_93:
        BR        S:??prvNotifyQueueSetContainer_93  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1085 
// 1086 	/* xQueueGenericSendFromISR() should be used instead of xQueueGiveFromISR()
// 1087 	if the item size is not 0. */
// 1088 	configASSERT( pxQueue->uxItemSize == 0 );
??prvNotifyQueueSetContainer_92:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_94  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_95  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_95:
        BR        S:??prvNotifyQueueSetContainer_95  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1089 
// 1090 	/* Normally a mutex would not be given from an interrupt, especially if
// 1091 	there is a mutex holder, as priority inheritance makes no sense for an
// 1092 	interrupts, only tasks. */
// 1093 	configASSERT( !( ( pxQueue->uxQueueType == queueQUEUE_IS_MUTEX ) && ( pxQueue->pxMutexHolder != NULL ) ) );
??prvNotifyQueueSetContainer_94:
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        POP       HL                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_96  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_96:
        BNZ       ??prvNotifyQueueSetContainer_97  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_98  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_98:
        BNZ       ??prvNotifyQueueSetContainer_99  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1094 
// 1095 	/* RTOS ports that support interrupt nesting have the concept of a maximum
// 1096 	system call (or maximum API call) interrupt priority.  Interrupts that are
// 1097 	above the maximum system call priority are kept permanently enabled, even
// 1098 	when the RTOS kernel is in a critical section, but cannot make any calls to
// 1099 	FreeRTOS API functions.  If configASSERT() is defined in FreeRTOSConfig.h
// 1100 	then portASSERT_IF_INTERRUPT_PRIORITY_INVALID() will result in an assertion
// 1101 	failure if a FreeRTOS API function is called from an interrupt that has been
// 1102 	assigned a priority above the configured maximum system call priority.
// 1103 	Only FreeRTOS functions that end in FromISR can be called from interrupts
// 1104 	that have been assigned a priority at or (logically) below the maximum
// 1105 	system call	interrupt priority.  FreeRTOS maintains a separate interrupt
// 1106 	safe API to ensure interrupt entry is as fast and as simple as possible.
// 1107 	More information (albeit Cortex-M specific) is provided on the following
// 1108 	link: http://www.freertos.org/RTOS-Cortex-M3-M4.html */
// 1109 	portASSERT_IF_INTERRUPT_PRIORITY_INVALID();
// 1110 
// 1111 	uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
??prvNotifyQueueSetContainer_97:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 1112 	{
// 1113 		const UBaseType_t uxMessagesWaiting = pxQueue->uxMessagesWaiting;
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
// 1114 
// 1115 		/* When the queue is used to implement a semaphore no data is ever
// 1116 		moved through the queue but it is still valid to see if the queue 'has
// 1117 		space'. */
// 1118 		if( uxMessagesWaiting < pxQueue->uxLength )
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        SKH                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_100  ;; 4 cycles
        ; ------------------------------------- Block: 29 cycles
// 1119 		{
// 1120 			const int8_t cTxLock = pxQueue->cTxLock;
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x3B          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE]         ;; 2 cycles
        MOV       B, A               ;; 1 cycle
// 1121 
// 1122 			traceQUEUE_SEND_FROM_ISR( pxQueue );
// 1123 
// 1124 			/* A task can only have an inherited priority if it is a mutex
// 1125 			holder - and if there is a mutex holder then the mutex cannot be
// 1126 			given from an ISR.  As this is the ISR version of the function it
// 1127 			can be assumed there is no mutex holder and no need to determine if
// 1128 			priority disinheritance is needed.  Simply increase the count of
// 1129 			messages (semaphores) available. */
// 1130 			pxQueue->uxMessagesWaiting = uxMessagesWaiting + 1;
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        INCW      DE                 ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1131 
// 1132 			/* The event list is not altered if the queue is locked.  This will
// 1133 			be done when the queue is unlocked later. */
// 1134 			if( cTxLock == queueUNLOCKED )
        MOV       A, B               ;; 1 cycle
        INC       A                  ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_101  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
// 1135 			{
// 1136 				#if ( configUSE_QUEUE_SETS == 1 )
// 1137 				{
// 1138 					if( pxQueue->pxQueueSetContainer != NULL )
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_102  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_102:
        BZ        ??prvNotifyQueueSetContainer_103  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1139 					{
// 1140 						if( prvNotifyQueueSetContainer( pxQueue, queueSEND_TO_BACK ) != pdFALSE )
        CLRW      BC                 ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvNotifyQueueSetContainer
        CALL      F:_prvNotifyQueueSetContainer  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_104  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        BR        S:??prvNotifyQueueSetContainer_105  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1141 						{
// 1142 							/* The semaphore is a member of a queue set, and
// 1143 							posting	to the queue set caused a higher priority
// 1144 							task to	unblock.  A context switch is required. */
// 1145 							if( pxHigherPriorityTaskWoken != NULL )
// 1146 							{
// 1147 								*pxHigherPriorityTaskWoken = pdTRUE;
// 1148 							}
// 1149 							else
// 1150 							{
// 1151 								mtCOVERAGE_TEST_MARKER();
// 1152 							}
// 1153 						}
// 1154 						else
// 1155 						{
// 1156 							mtCOVERAGE_TEST_MARKER();
// 1157 						}
// 1158 					}
??prvNotifyQueueSetContainer_99:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_106  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_106:
        BR        S:??prvNotifyQueueSetContainer_106  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1159 					else
// 1160 					{
// 1161 						if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
??prvNotifyQueueSetContainer_103:
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_105  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 1162 						{
// 1163 							if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_105  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 1164 							{
// 1165 								/* The task waiting has a higher priority so
// 1166 								record that a context switch is required. */
// 1167 								if( pxHigherPriorityTaskWoken != NULL )
??prvNotifyQueueSetContainer_104:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_107  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_107:
        BZ        ??prvNotifyQueueSetContainer_105  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1168 								{
// 1169 									*pxHigherPriorityTaskWoken = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        BR        S:??prvNotifyQueueSetContainer_105  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1170 								}
// 1171 								else
// 1172 								{
// 1173 									mtCOVERAGE_TEST_MARKER();
// 1174 								}
// 1175 							}
// 1176 							else
// 1177 							{
// 1178 								mtCOVERAGE_TEST_MARKER();
// 1179 							}
// 1180 						}
// 1181 						else
// 1182 						{
// 1183 							mtCOVERAGE_TEST_MARKER();
// 1184 						}
// 1185 					}
// 1186 				}
// 1187 				#else /* configUSE_QUEUE_SETS */
// 1188 				{
// 1189 					if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
// 1190 					{
// 1191 						if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
// 1192 						{
// 1193 							/* The task waiting has a higher priority so record that a
// 1194 							context	switch is required. */
// 1195 							if( pxHigherPriorityTaskWoken != NULL )
// 1196 							{
// 1197 								*pxHigherPriorityTaskWoken = pdTRUE;
// 1198 							}
// 1199 							else
// 1200 							{
// 1201 								mtCOVERAGE_TEST_MARKER();
// 1202 							}
// 1203 						}
// 1204 						else
// 1205 						{
// 1206 							mtCOVERAGE_TEST_MARKER();
// 1207 						}
// 1208 					}
// 1209 					else
// 1210 					{
// 1211 						mtCOVERAGE_TEST_MARKER();
// 1212 					}
// 1213 				}
// 1214 				#endif /* configUSE_QUEUE_SETS */
// 1215 			}
// 1216 			else
// 1217 			{
// 1218 				/* Increment the lock count so the task that unlocks the queue
// 1219 				knows that data was posted while it was locked. */
// 1220 				pxQueue->cTxLock = ( int8_t ) ( cTxLock + 1 );
??prvNotifyQueueSetContainer_101:
        INC       B                  ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      AX, #0x3B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 7 cycles
// 1221 			}
// 1222 
// 1223 			xReturn = pdPASS;
??prvNotifyQueueSetContainer_105:
        ONEW      AX                 ;; 1 cycle
        BR        S:??prvNotifyQueueSetContainer_108  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 1224 		}
// 1225 		else
// 1226 		{
// 1227 			traceQUEUE_SEND_FROM_ISR_FAILED( pxQueue );
// 1228 			xReturn = errQUEUE_FULL;
??prvNotifyQueueSetContainer_100:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvNotifyQueueSetContainer_108:
        MOVW      HL, AX             ;; 1 cycle
// 1229 		}
// 1230 	}
// 1231 	portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
        MOVW      AX, [SP]           ;; 1 cycle
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        ROL       A, 0x1             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 1232 
// 1233 	return xReturn;
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0xA           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock9
        ; ------------------------------------- Block: 20 cycles
        ; ------------------------------------- Total: 304 cycles
// 1234 }
// 1235 /*-----------------------------------------------------------*/
// 1236 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock10 Using cfiCommon2
          CFI Function _xQueueGenericReceive
        CODE
// 1237 BaseType_t xQueueGenericReceive( QueueHandle_t xQueue, void * const pvBuffer, TickType_t xTicksToWait, const BaseType_t xJustPeeking )
// 1238 {
_xQueueGenericReceive:
        ; * Stack frame (at entry) *
        ; Param size: 6
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 20
        SUBW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+24
// 1239 BaseType_t xEntryTimeSet = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 1240 TimeOut_t xTimeOut;
// 1241 int8_t *pcOriginalReadPosition;
// 1242 Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 1243 
// 1244 	configASSERT( pxQueue );
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_109  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_109:
        BNZ       ??prvNotifyQueueSetContainer_110  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_111  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_111:
        BR        S:??prvNotifyQueueSetContainer_111  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1245 	configASSERT( !( ( pvBuffer == NULL ) && ( pxQueue->uxItemSize != ( UBaseType_t ) 0U ) ) );
??prvNotifyQueueSetContainer_110:
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_112  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_112:
        BNZ       ??prvNotifyQueueSetContainer_113  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_114  ;; 4 cycles
          CFI FunCall _xTaskGetSchedulerState
        ; ------------------------------------- Block: 12 cycles
// 1246 	#if ( ( INCLUDE_xTaskGetSchedulerState == 1 ) || ( configUSE_TIMERS == 1 ) )
// 1247 	{
// 1248 		configASSERT( !( ( xTaskGetSchedulerState() == taskSCHEDULER_SUSPENDED ) && ( xTicksToWait != 0 ) ) );
??prvNotifyQueueSetContainer_113:
        CALL      F:_xTaskGetSchedulerState  ;; 3 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_115  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_115  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_116  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_116:
        BR        S:??prvNotifyQueueSetContainer_116  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1249 	}
??prvNotifyQueueSetContainer_114:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_117  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_117:
        BR        S:??prvNotifyQueueSetContainer_117  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1250 	#endif
// 1251 
// 1252 	/* This function relaxes the coding standard somewhat to allow return
// 1253 	statements within the function itself.  This is done in the interest
// 1254 	of execution time efficiency. */
// 1255 
// 1256 	for( ;; )
// 1257 	{
// 1258 		taskENTER_CRITICAL();
// 1259 		{
// 1260 			const UBaseType_t uxMessagesWaiting = pxQueue->uxMessagesWaiting;
// 1261 
// 1262 			/* Is there data in the queue now?  To be running the calling task
// 1263 			must be the highest priority task wanting to access the queue. */
// 1264 			if( uxMessagesWaiting > ( UBaseType_t ) 0 )
// 1265 			{
// 1266 				/* Remember the read position in case the queue is only being
// 1267 				peeked. */
// 1268 				pcOriginalReadPosition = pxQueue->u.pcReadFrom;
// 1269 
// 1270 				prvCopyDataFromQueue( pxQueue, pvBuffer );
// 1271 
// 1272 				if( xJustPeeking == pdFALSE )
// 1273 				{
// 1274 					traceQUEUE_RECEIVE( pxQueue );
// 1275 
// 1276 					/* Actually removing data, not just peeking. */
// 1277 					pxQueue->uxMessagesWaiting = uxMessagesWaiting - 1;
// 1278 
// 1279 					#if ( configUSE_MUTEXES == 1 )
// 1280 					{
// 1281 						if( pxQueue->uxQueueType == queueQUEUE_IS_MUTEX )
// 1282 						{
// 1283 							/* Record the information required to implement
// 1284 							priority inheritance should it become necessary. */
// 1285 							pxQueue->pxMutexHolder = ( int8_t * ) pvTaskIncrementMutexHeldCount(); /*lint !e961 Cast is not redundant as TaskHandle_t is a typedef. */
// 1286 						}
// 1287 						else
// 1288 						{
// 1289 							mtCOVERAGE_TEST_MARKER();
// 1290 						}
// 1291 					}
// 1292 					#endif /* configUSE_MUTEXES */
// 1293 
// 1294 					if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToSend ) ) == pdFALSE )
// 1295 					{
// 1296 						if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToSend ) ) != pdFALSE )
// 1297 						{
// 1298 							queueYIELD_IF_USING_PREEMPTION();
// 1299 						}
// 1300 						else
// 1301 						{
// 1302 							mtCOVERAGE_TEST_MARKER();
// 1303 						}
// 1304 					}
// 1305 					else
// 1306 					{
// 1307 						mtCOVERAGE_TEST_MARKER();
// 1308 					}
// 1309 				}
// 1310 				else
// 1311 				{
// 1312 					traceQUEUE_PEEK( pxQueue );
// 1313 
// 1314 					/* The data is not being removed, so reset the read
// 1315 					pointer. */
// 1316 					pxQueue->u.pcReadFrom = pcOriginalReadPosition;
// 1317 
// 1318 					/* The data is being left in the queue, so see if there are
// 1319 					any other tasks waiting for the data. */
// 1320 					if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
// 1321 					{
// 1322 						if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
// 1323 						{
// 1324 							/* The task waiting has a higher priority than this task. */
// 1325 							queueYIELD_IF_USING_PREEMPTION();
// 1326 						}
// 1327 						else
// 1328 						{
// 1329 							mtCOVERAGE_TEST_MARKER();
// 1330 						}
// 1331 					}
// 1332 					else
// 1333 					{
// 1334 						mtCOVERAGE_TEST_MARKER();
// 1335 					}
// 1336 				}
// 1337 
// 1338 				taskEXIT_CRITICAL();
// 1339 				return pdPASS;
// 1340 			}
// 1341 			else
// 1342 			{
// 1343 				if( xTicksToWait == ( TickType_t ) 0 )
// 1344 				{
// 1345 					/* The queue was empty and no block time is specified (or
// 1346 					the block time has expired) so leave now. */
// 1347 					taskEXIT_CRITICAL();
// 1348 					traceQUEUE_RECEIVE_FAILED( pxQueue );
// 1349 					return errQUEUE_EMPTY;
// 1350 				}
// 1351 				else if( xEntryTimeSet == pdFALSE )
// 1352 				{
// 1353 					/* The queue was empty and a block time was specified so
// 1354 					configure the timeout structure. */
// 1355 					vTaskSetTimeOutState( &xTimeOut );
// 1356 					xEntryTimeSet = pdTRUE;
// 1357 				}
// 1358 				else
// 1359 				{
// 1360 					/* Entry time was already set. */
// 1361 					mtCOVERAGE_TEST_MARKER();
// 1362 				}
// 1363 			}
// 1364 		}
// 1365 		taskEXIT_CRITICAL();
// 1366 
// 1367 		/* Interrupts and other tasks can send to and receive from the queue
// 1368 		now the critical section has been exited. */
// 1369 
// 1370 		vTaskSuspendAll();
// 1371 		prvLockQueue( pxQueue );
// 1372 
// 1373 		/* Update the timeout state to see if it has expired yet. */
// 1374 		if( xTaskCheckForTimeOut( &xTimeOut, &xTicksToWait ) == pdFALSE )
// 1375 		{
// 1376 			if( prvIsQueueEmpty( pxQueue ) != pdFALSE )
// 1377 			{
// 1378 				traceBLOCKING_ON_QUEUE_RECEIVE( pxQueue );
// 1379 
// 1380 				#if ( configUSE_MUTEXES == 1 )
// 1381 				{
// 1382 					if( pxQueue->uxQueueType == queueQUEUE_IS_MUTEX )
// 1383 					{
// 1384 						taskENTER_CRITICAL();
// 1385 						{
// 1386 							vTaskPriorityInherit( ( void * ) pxQueue->pxMutexHolder );
// 1387 						}
// 1388 						taskEXIT_CRITICAL();
// 1389 					}
// 1390 					else
// 1391 					{
// 1392 						mtCOVERAGE_TEST_MARKER();
// 1393 					}
// 1394 				}
// 1395 				#endif
// 1396 
// 1397 				vTaskPlaceOnEventList( &( pxQueue->xTasksWaitingToReceive ), xTicksToWait );
// 1398 				prvUnlockQueue( pxQueue );
// 1399 				if( xTaskResumeAll() == pdFALSE )
// 1400 				{
// 1401 					portYIELD_WITHIN_API();
// 1402 				}
// 1403 				else
// 1404 				{
// 1405 					mtCOVERAGE_TEST_MARKER();
// 1406 				}
// 1407 			}
// 1408 			else
// 1409 			{
// 1410 				/* Try again. */
// 1411 				prvUnlockQueue( pxQueue );
??xQueueGenericReceive_0:
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvUnlockQueue
        CALL      F:_prvUnlockQueue  ;; 3 cycles
// 1412 				( void ) xTaskResumeAll();
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        ; ------------------------------------- Block: 8 cycles
// 1413 			}
??prvNotifyQueueSetContainer_115:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_118  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_118:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_119  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      [SP], AX           ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x0A], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+26
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
          CFI FunCall _prvCopyDataFromQueue
        CALL      F:_prvCopyDataFromQueue  ;; 3 cycles
        MOVW      AX, [SP+0x1C]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_120  ;; 4 cycles
        ; ------------------------------------- Block: 38 cycles
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_121  ;; 4 cycles
        ; ------------------------------------- Block: 25 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_121:
        BNZ       ??prvNotifyQueueSetContainer_122  ;; 4 cycles
          CFI FunCall _pvTaskIncrementMutexHeldCount
        ; ------------------------------------- Block: 4 cycles
        CALL      F:_pvTaskIncrementMutexHeldCount  ;; 3 cycles
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+26
        POP       BC                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 19 cycles
??prvNotifyQueueSetContainer_122:
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_123  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_124  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        BR        S:??prvNotifyQueueSetContainer_123  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_120:
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+26
        POP       BC                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_123  ;; 4 cycles
        ; ------------------------------------- Block: 31 cycles
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
??prvNotifyQueueSetContainer_124:
        BRK                          ;; 5 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_123:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_125  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_125  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??prvNotifyQueueSetContainer_125:
        ONEW      AX                 ;; 1 cycle
        BR        R:??prvNotifyQueueSetContainer_126  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
??prvNotifyQueueSetContainer_119:
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        OR        A, C               ;; 1 cycle
        OR        A, B               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_127  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_128  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_128  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        BR        R:??prvNotifyQueueSetContainer_128  ;; 3 cycles
        ; ------------------------------------- Block: 9 cycles
??prvNotifyQueueSetContainer_127:
        MOVW      AX, [SP]           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_129  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
          CFI FunCall _vTaskSetTimeOutState
        CALL      F:_vTaskSetTimeOutState  ;; 3 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 9 cycles
??prvNotifyQueueSetContainer_129:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_130  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_130  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
          CFI FunCall _vTaskSuspendAll
        ; ------------------------------------- Block: 6 cycles
??prvNotifyQueueSetContainer_130:
        CALL      F:_vTaskSuspendAll  ;; 3 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_131  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_131:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x3A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        INC       A                  ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_132  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_132:
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        INC       A                  ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_133  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_133:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_134  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_134  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??prvNotifyQueueSetContainer_134:
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x18          ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, #0xF            ;; 1 cycle
        MOVW      DE, SP             ;; 1 cycle
        INCW      DE                 ;; 1 cycle
        INCW      DE                 ;; 1 cycle
          CFI FunCall _xTaskCheckForTimeOut
        CALL      F:_xTaskCheckForTimeOut  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_135  ;; 4 cycles
          CFI FunCall _prvIsQueueEmpty
        ; ------------------------------------- Block: 21 cycles
        CALL      F:_prvIsQueueEmpty  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??xQueueGenericReceive_0  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_136  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, DE             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_136:
        BNZ       ??prvNotifyQueueSetContainer_137  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_138  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_138:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vTaskPriorityInherit
        CALL      F:_vTaskPriorityInherit  ;; 3 cycles
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_137  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_137  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??prvNotifyQueueSetContainer_137:
        MOVW      AX, [SP+0x1A]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x18]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+26
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+28
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vTaskPlaceOnEventList
        CALL      F:_vTaskPlaceOnEventList  ;; 3 cycles
        MOV       A, [SP+0x16]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvUnlockQueue
        CALL      F:_prvUnlockQueue  ;; 3 cycles
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+24
        CMPW      AX, #0x0           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_115  ;; 4 cycles
        ; ------------------------------------- Block: 31 cycles
        BRK                          ;; 5 cycles
        BR        R:??prvNotifyQueueSetContainer_115  ;; 3 cycles
          CFI FunCall _prvUnlockQueue
        ; ------------------------------------- Block: 8 cycles
// 1414 		}
// 1415 		else
// 1416 		{
// 1417 			prvUnlockQueue( pxQueue );
??prvNotifyQueueSetContainer_135:
        CALL      F:_prvUnlockQueue  ;; 3 cycles
// 1418 			( void ) xTaskResumeAll();
          CFI FunCall _xTaskResumeAll
        CALL      F:_xTaskResumeAll  ;; 3 cycles
// 1419 
// 1420 			if( prvIsQueueEmpty( pxQueue ) != pdFALSE )
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvIsQueueEmpty
        CALL      F:_prvIsQueueEmpty  ;; 3 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_115  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 1421 			{
// 1422 				traceQUEUE_RECEIVE_FAILED( pxQueue );
// 1423 				return errQUEUE_EMPTY;
??prvNotifyQueueSetContainer_128:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvNotifyQueueSetContainer_126:
        ADDW      SP, #0x14          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock10
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 678 cycles
// 1424 			}
// 1425 			else
// 1426 			{
// 1427 				mtCOVERAGE_TEST_MARKER();
// 1428 			}
// 1429 		}
// 1430 	}
// 1431 }
// 1432 /*-----------------------------------------------------------*/
// 1433 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock11 Using cfiCommon2
          CFI Function _xQueueReceiveFromISR
        CODE
// 1434 BaseType_t xQueueReceiveFromISR( QueueHandle_t xQueue, void * const pvBuffer, BaseType_t * const pxHigherPriorityTaskWoken )
// 1435 {
_xQueueReceiveFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 14
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+18
// 1436 BaseType_t xReturn;
// 1437 UBaseType_t uxSavedInterruptStatus;
// 1438 Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 1439 
// 1440 	configASSERT( pxQueue );
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_139  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_139:
        BNZ       ??prvNotifyQueueSetContainer_140  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_141  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_141:
        BR        S:??prvNotifyQueueSetContainer_141  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1441 	configASSERT( !( ( pvBuffer == NULL ) && ( pxQueue->uxItemSize != ( UBaseType_t ) 0U ) ) );
??prvNotifyQueueSetContainer_140:
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_142  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_142:
        BNZ       ??prvNotifyQueueSetContainer_143  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_144  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 1442 
// 1443 	/* RTOS ports that support interrupt nesting have the concept of a maximum
// 1444 	system call (or maximum API call) interrupt priority.  Interrupts that are
// 1445 	above the maximum system call priority are kept permanently enabled, even
// 1446 	when the RTOS kernel is in a critical section, but cannot make any calls to
// 1447 	FreeRTOS API functions.  If configASSERT() is defined in FreeRTOSConfig.h
// 1448 	then portASSERT_IF_INTERRUPT_PRIORITY_INVALID() will result in an assertion
// 1449 	failure if a FreeRTOS API function is called from an interrupt that has been
// 1450 	assigned a priority above the configured maximum system call priority.
// 1451 	Only FreeRTOS functions that end in FromISR can be called from interrupts
// 1452 	that have been assigned a priority at or (logically) below the maximum
// 1453 	system call	interrupt priority.  FreeRTOS maintains a separate interrupt
// 1454 	safe API to ensure interrupt entry is as fast and as simple as possible.
// 1455 	More information (albeit Cortex-M specific) is provided on the following
// 1456 	link: http://www.freertos.org/RTOS-Cortex-M3-M4.html */
// 1457 	portASSERT_IF_INTERRUPT_PRIORITY_INVALID();
// 1458 
// 1459 	uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
??prvNotifyQueueSetContainer_143:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 1460 	{
// 1461 		const UBaseType_t uxMessagesWaiting = pxQueue->uxMessagesWaiting;
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1462 
// 1463 		/* Cannot block in an ISR, so check there is data available. */
// 1464 		if( uxMessagesWaiting > ( UBaseType_t ) 0 )
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_145  ;; 4 cycles
        ; ------------------------------------- Block: 24 cycles
// 1465 		{
// 1466 			const int8_t cRxLock = pxQueue->cRxLock;
// 1467 
// 1468 			traceQUEUE_RECEIVE_FROM_ISR( pxQueue );
// 1469 
// 1470 			prvCopyDataFromQueue( pxQueue, pvBuffer );
// 1471 			pxQueue->uxMessagesWaiting = uxMessagesWaiting - 1;
// 1472 
// 1473 			/* If the queue is locked the event list will not be modified.
// 1474 			Instead update the lock count so the task that unlocks the queue
// 1475 			will know that an ISR has removed data while the queue was
// 1476 			locked. */
// 1477 			if( cRxLock == queueUNLOCKED )
// 1478 			{
// 1479 				if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToSend ) ) == pdFALSE )
// 1480 				{
// 1481 					if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToSend ) ) != pdFALSE )
// 1482 					{
// 1483 						/* The task waiting has a higher priority than us so
// 1484 						force a context switch. */
// 1485 						if( pxHigherPriorityTaskWoken != NULL )
// 1486 						{
// 1487 							*pxHigherPriorityTaskWoken = pdTRUE;
// 1488 						}
// 1489 						else
// 1490 						{
// 1491 							mtCOVERAGE_TEST_MARKER();
// 1492 						}
// 1493 					}
// 1494 					else
// 1495 					{
// 1496 						mtCOVERAGE_TEST_MARKER();
// 1497 					}
// 1498 				}
// 1499 				else
// 1500 				{
// 1501 					mtCOVERAGE_TEST_MARKER();
// 1502 				}
// 1503 			}
// 1504 			else
// 1505 			{
// 1506 				/* Increment the lock count so the task that unlocks the queue
// 1507 				knows that data was removed while it was locked. */
// 1508 				pxQueue->cRxLock = ( int8_t ) ( cRxLock + 1 );
// 1509 			}
// 1510 
// 1511 			xReturn = pdPASS;
// 1512 		}
// 1513 		else
// 1514 		{
// 1515 			xReturn = pdFAIL;
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??xQueueReceiveFromISR_0:
        MOVW      HL, AX             ;; 1 cycle
// 1516 			traceQUEUE_RECEIVE_FROM_ISR_FAILED( pxQueue );
// 1517 		}
// 1518 	}
// 1519 	portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        BR        F:?Subroutine0     ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_144:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_146  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_146:
        BR        S:??prvNotifyQueueSetContainer_146  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_145:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x3A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _prvCopyDataFromQueue
        CALL      F:_prvCopyDataFromQueue  ;; 3 cycles
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
        MOV       A, [SP]            ;; 1 cycle
        INC       A                  ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_147  ;; 4 cycles
        ; ------------------------------------- Block: 42 cycles
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_148  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_148  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       A, [SP+0x14]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x12]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_149  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOVW      AX, HL             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_149:
        BZ        ??prvNotifyQueueSetContainer_148  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        ONEW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        BR        S:??prvNotifyQueueSetContainer_148  ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
??prvNotifyQueueSetContainer_147:
        MOV       A, [SP]            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        INC       B                  ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x3A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 9 cycles
??prvNotifyQueueSetContainer_148:
        ONEW      AX                 ;; 1 cycle
        BR        R:??xQueueReceiveFromISR_0  ;; 3 cycles
          CFI EndBlock cfiBlock11
        ; ------------------------------------- Block: 4 cycles
        ; ------------------------------------- Total: 221 cycles
// 1520 
// 1521 	return xReturn;
// 1522 }
// 1523 /*-----------------------------------------------------------*/
// 1524 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock12 Using cfiCommon2
          CFI Function _xQueuePeekFromISR
        CODE
// 1525 BaseType_t xQueuePeekFromISR( QueueHandle_t xQueue,  void * const pvBuffer )
// 1526 {
_xQueuePeekFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 14
        SUBW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+18
// 1527 BaseType_t xReturn;
// 1528 UBaseType_t uxSavedInterruptStatus;
// 1529 int8_t *pcOriginalReadPosition;
// 1530 Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 1531 
// 1532 	configASSERT( pxQueue );
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_150  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_150:
        BNZ       ??prvNotifyQueueSetContainer_151  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_152  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_152:
        BR        S:??prvNotifyQueueSetContainer_152  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1533 	configASSERT( !( ( pvBuffer == NULL ) && ( pxQueue->uxItemSize != ( UBaseType_t ) 0U ) ) );
??prvNotifyQueueSetContainer_151:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_153  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_153:
        MOVW      AX, HL             ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_154  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_155  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1534 	configASSERT( pxQueue->uxItemSize != 0 ); /* Can't peek a semaphore. */
??xQueuePeekFromISR_0:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_156  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_156:
        BR        S:??prvNotifyQueueSetContainer_156  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_154:
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??xQueuePeekFromISR_0  ;; 4 cycles
        ; ------------------------------------- Block: 5 cycles
// 1535 
// 1536 	/* RTOS ports that support interrupt nesting have the concept of a maximum
// 1537 	system call (or maximum API call) interrupt priority.  Interrupts that are
// 1538 	above the maximum system call priority are kept permanently enabled, even
// 1539 	when the RTOS kernel is in a critical section, but cannot make any calls to
// 1540 	FreeRTOS API functions.  If configASSERT() is defined in FreeRTOSConfig.h
// 1541 	then portASSERT_IF_INTERRUPT_PRIORITY_INVALID() will result in an assertion
// 1542 	failure if a FreeRTOS API function is called from an interrupt that has been
// 1543 	assigned a priority above the configured maximum system call priority.
// 1544 	Only FreeRTOS functions that end in FromISR can be called from interrupts
// 1545 	that have been assigned a priority at or (logically) below the maximum
// 1546 	system call	interrupt priority.  FreeRTOS maintains a separate interrupt
// 1547 	safe API to ensure interrupt entry is as fast and as simple as possible.
// 1548 	More information (albeit Cortex-M specific) is provided on the following
// 1549 	link: http://www.freertos.org/RTOS-Cortex-M3-M4.html */
// 1550 	portASSERT_IF_INTERRUPT_PRIORITY_INVALID();
// 1551 
// 1552 	uxSavedInterruptStatus = portSET_INTERRUPT_MASK_FROM_ISR();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
// 1553 	{
// 1554 		/* Cannot block in an ISR, so check there is data available. */
// 1555 		if( pxQueue->uxMessagesWaiting > ( UBaseType_t ) 0 )
        MOV       A, [SP+0x0C]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_157  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
// 1556 		{
// 1557 			traceQUEUE_PEEK_FROM_ISR( pxQueue );
// 1558 
// 1559 			/* Remember the read position so it can be reset as nothing is
// 1560 			actually being removed from the queue. */
// 1561 			pcOriginalReadPosition = pxQueue->u.pcReadFrom;
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1562 			prvCopyDataFromQueue( pxQueue, pvBuffer );
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _prvCopyDataFromQueue
        CALL      F:_prvCopyDataFromQueue  ;; 3 cycles
// 1563 			pxQueue->u.pcReadFrom = pcOriginalReadPosition;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1564 
// 1565 			xReturn = pdPASS;
        ONEW      AX                 ;; 1 cycle
        BR        S:??prvNotifyQueueSetContainer_158  ;; 3 cycles
        ; ------------------------------------- Block: 55 cycles
// 1566 		}
??prvNotifyQueueSetContainer_155:
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_159  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_159:
        BR        S:??prvNotifyQueueSetContainer_159  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1567 		else
// 1568 		{
// 1569 			xReturn = pdFAIL;
??prvNotifyQueueSetContainer_157:
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
??prvNotifyQueueSetContainer_158:
        MOVW      HL, AX             ;; 1 cycle
// 1570 			traceQUEUE_PEEK_FROM_ISR_FAILED( pxQueue );
// 1571 		}
// 1572 	}
// 1573 	portCLEAR_INTERRUPT_MASK_FROM_ISR( uxSavedInterruptStatus );
        MOVW      AX, [SP]           ;; 1 cycle
          CFI EndBlock cfiBlock12
        ; ------------------------------------- Block: 2 cycles
        ; ------------------------------------- Total: 199 cycles
        REQUIRE ?Subroutine0
        ; // Fall through to label ?Subroutine0
// 1574 
// 1575 	return xReturn;
// 1576 }

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock13 Using cfiCommon2
          CFI NoFunction
          CFI CFA SP+18
        CODE
?Subroutine0:
        MOV       A, X               ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        ROL       A, 0x1             ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock13
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 18 cycles
// 1577 /*-----------------------------------------------------------*/
// 1578 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock14 Using cfiCommon2
          CFI Function _uxQueueMessagesWaiting
          CFI NoCalls
        CODE
// 1579 UBaseType_t uxQueueMessagesWaiting( const QueueHandle_t xQueue )
// 1580 {
_uxQueueMessagesWaiting:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1581 UBaseType_t uxReturn;
// 1582 
// 1583 	configASSERT( xQueue );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_160  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_160:
        BNZ       ??prvNotifyQueueSetContainer_161  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_162  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_162:
        BR        S:??prvNotifyQueueSetContainer_162  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1584 
// 1585 	taskENTER_CRITICAL();
??prvNotifyQueueSetContainer_161:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_163  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_163:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1586 	{
// 1587 		uxReturn = ( ( Queue_t * ) xQueue )->uxMessagesWaiting;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
// 1588 	}
// 1589 	taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_164  ;; 4 cycles
        ; ------------------------------------- Block: 20 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_164  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1590 
// 1591 	return uxReturn;
??prvNotifyQueueSetContainer_164:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock14
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 97 cycles
// 1592 } /*lint !e818 Pointer cannot be declared const as xQueue is a typedef not pointer. */
// 1593 /*-----------------------------------------------------------*/
// 1594 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock15 Using cfiCommon2
          CFI Function _uxQueueSpacesAvailable
          CFI NoCalls
        CODE
// 1595 UBaseType_t uxQueueSpacesAvailable( const QueueHandle_t xQueue )
// 1596 {
_uxQueueSpacesAvailable:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1597 UBaseType_t uxReturn;
// 1598 Queue_t *pxQueue;
// 1599 
// 1600 	pxQueue = ( Queue_t * ) xQueue;
// 1601 	configASSERT( pxQueue );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_165  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_165:
        BNZ       ??prvNotifyQueueSetContainer_166  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_167  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_167:
        BR        S:??prvNotifyQueueSetContainer_167  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1602 
// 1603 	taskENTER_CRITICAL();
??prvNotifyQueueSetContainer_166:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_168  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_168:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1604 	{
// 1605 		uxReturn = pxQueue->uxLength - pxQueue->uxMessagesWaiting;
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        SUBW      AX, HL             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
// 1606 	}
// 1607 	taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_169  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_169  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1608 
// 1609 	return uxReturn;
??prvNotifyQueueSetContainer_169:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock15
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 104 cycles
// 1610 } /*lint !e818 Pointer cannot be declared const as xQueue is a typedef not pointer. */
// 1611 /*-----------------------------------------------------------*/
// 1612 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock16 Using cfiCommon2
          CFI Function _uxQueueMessagesWaitingFromISR
          CFI NoCalls
        CODE
// 1613 UBaseType_t uxQueueMessagesWaitingFromISR( const QueueHandle_t xQueue )
// 1614 {
_uxQueueMessagesWaitingFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1615 UBaseType_t uxReturn;
// 1616 
// 1617 	configASSERT( xQueue );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_170  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_170:
        BNZ       ??prvNotifyQueueSetContainer_171  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_172  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_172:
        BR        S:??prvNotifyQueueSetContainer_172  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1618 
// 1619 	uxReturn = ( ( Queue_t * ) xQueue )->uxMessagesWaiting;
??prvNotifyQueueSetContainer_171:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
          CFI EndBlock cfiBlock16
        ; ------------------------------------- Block: 2 cycles
        ; ------------------------------------- Total: 42 cycles
        REQUIRE ?Subroutine2
        ; // Fall through to label ?Subroutine2
// 1620 
// 1621 	return uxReturn;
// 1622 } /*lint !e818 Pointer cannot be declared const as xQueue is a typedef not pointer. */

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock17 Using cfiCommon2
          CFI NoFunction
          CFI CFA SP+8
        CODE
?Subroutine2:
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock17
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 1623 /*-----------------------------------------------------------*/
// 1624 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock18 Using cfiCommon2
          CFI Function _vQueueDelete
          CFI NoCalls
        CODE
// 1625 void vQueueDelete( QueueHandle_t xQueue )
// 1626 {
_vQueueDelete:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1627 Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 1628 
// 1629 	configASSERT( pxQueue );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_173  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_173:
        BNZ       ??prvNotifyQueueSetContainer_174  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_175  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_175:
        BR        S:??prvNotifyQueueSetContainer_175  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1630 	traceQUEUE_DELETE( pxQueue );
// 1631 
// 1632 	#if ( configQUEUE_REGISTRY_SIZE > 0 )
// 1633 	{
// 1634 		vQueueUnregisterQueue( pxQueue );
// 1635 	}
// 1636 	#endif
// 1637 
// 1638 	#if( ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 0 ) )
// 1639 	{
// 1640 		/* The queue can only have been allocated dynamically - free it
// 1641 		again. */
// 1642 		vPortFree( pxQueue );
// 1643 	}
// 1644 	#elif( ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) && ( configSUPPORT_STATIC_ALLOCATION == 1 ) )
// 1645 	{
// 1646 		/* The queue could have been allocated statically or dynamically, so
// 1647 		check before attempting to free the memory. */
// 1648 		if( pxQueue->ucStaticallyAllocated == ( uint8_t ) pdFALSE )
// 1649 		{
// 1650 			vPortFree( pxQueue );
// 1651 		}
// 1652 		else
// 1653 		{
// 1654 			mtCOVERAGE_TEST_MARKER();
// 1655 		}
// 1656 	}
// 1657 	#else
// 1658 	{
// 1659 		/* The queue must have been statically allocated, so is not going to be
// 1660 		deleted.  Avoid compiler warnings about the unused parameter. */
// 1661 		( void ) pxQueue;
// 1662 	}
// 1663 	#endif /* configSUPPORT_DYNAMIC_ALLOCATION */
// 1664 }
??prvNotifyQueueSetContainer_174:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock18
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 47 cycles
// 1665 /*-----------------------------------------------------------*/
// 1666 
// 1667 #if ( configUSE_TRACE_FACILITY == 1 )
// 1668 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock19 Using cfiCommon2
          CFI Function _uxQueueGetQueueNumber
          CFI NoCalls
        CODE
// 1669 	UBaseType_t uxQueueGetQueueNumber( QueueHandle_t xQueue )
// 1670 	{
_uxQueueGetQueueNumber:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1671 		return ( ( Queue_t * ) xQueue )->uxQueueNumber;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x40          ;; 1 cycle
        BR        F:?Subroutine2     ;; 3 cycles
          CFI EndBlock cfiBlock19
        ; ------------------------------------- Block: 10 cycles
        ; ------------------------------------- Total: 10 cycles
// 1672 	}
// 1673 
// 1674 #endif /* configUSE_TRACE_FACILITY */
// 1675 /*-----------------------------------------------------------*/
// 1676 
// 1677 #if ( configUSE_TRACE_FACILITY == 1 )
// 1678 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock20 Using cfiCommon0
          CFI Function _vQueueSetQueueNumber
          CFI NoCalls
        CODE
// 1679 	void vQueueSetQueueNumber( QueueHandle_t xQueue, UBaseType_t uxQueueNumber )
// 1680 	{
_vQueueSetQueueNumber:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1681 		( ( Queue_t * ) xQueue )->uxQueueNumber = uxQueueNumber;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x40          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1682 	}
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock20
        ; ------------------------------------- Block: 18 cycles
        ; ------------------------------------- Total: 18 cycles
// 1683 
// 1684 #endif /* configUSE_TRACE_FACILITY */
// 1685 /*-----------------------------------------------------------*/
// 1686 
// 1687 #if ( configUSE_TRACE_FACILITY == 1 )
// 1688 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock21 Using cfiCommon2
          CFI Function _ucQueueGetQueueType
          CFI NoCalls
        CODE
// 1689 	uint8_t ucQueueGetQueueType( QueueHandle_t xQueue )
// 1690 	{
_ucQueueGetQueueType:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1691 		return ( ( Queue_t * ) xQueue )->ucQueueType;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x42          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock21
        ; ------------------------------------- Block: 17 cycles
        ; ------------------------------------- Total: 17 cycles
// 1692 	}
// 1693 
// 1694 #endif /* configUSE_TRACE_FACILITY */
// 1695 /*-----------------------------------------------------------*/
// 1696 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock22 Using cfiCommon2
          CFI Function _prvCopyDataToQueue
        CODE
// 1697 static BaseType_t prvCopyDataToQueue( Queue_t * const pxQueue, const void *pvItemToQueue, const BaseType_t xPosition )
// 1698 {
_prvCopyDataToQueue:
        ; * Stack frame (at entry) *
        ; Param size: 2
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 16
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+20
// 1699 BaseType_t xReturn = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1700 UBaseType_t uxMessagesWaiting;
// 1701 
// 1702 	/* This function is called from a critical section. */
// 1703 
// 1704 	uxMessagesWaiting = pxQueue->uxMessagesWaiting;
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      [SP], AX           ;; 1 cycle
// 1705 
// 1706 	if( pxQueue->uxItemSize == ( UBaseType_t ) 0 )
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_176  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
// 1707 	{
// 1708 		#if ( configUSE_MUTEXES == 1 )
// 1709 		{
// 1710 			if( pxQueue->uxQueueType == queueQUEUE_IS_MUTEX )
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_177  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_177:
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_178  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1711 			{
// 1712 				/* The mutex is no longer being held. */
// 1713 				xReturn = xTaskPriorityDisinherit( ( void * ) pxQueue->pxMutexHolder );
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskPriorityDisinherit
        CALL      F:_xTaskPriorityDisinherit  ;; 3 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 1714 				pxQueue->pxMutexHolder = NULL;
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        BR        R:??prvNotifyQueueSetContainer_179  ;; 3 cycles
        ; ------------------------------------- Block: 28 cycles
// 1715 			}
// 1716 			else
// 1717 			{
// 1718 				mtCOVERAGE_TEST_MARKER();
// 1719 			}
// 1720 		}
// 1721 		#endif /* configUSE_MUTEXES */
// 1722 	}
// 1723 	else if( xPosition == queueSEND_TO_BACK )
??prvNotifyQueueSetContainer_176:
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        OR        A, X               ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        SKZ                          ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_180  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
// 1724 	{
// 1725 		( void ) memcpy( ( void * ) pxQueue->pcWriteTo, pvItemToQueue, ( size_t ) pxQueue->uxItemSize ); /*lint !e961 !e418 MISRA exception as the casts are only redundant for some ports, plus previous logic ensures a null pointer can only be passed to memcpy() if the copy size is 0. */
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
          CFI FunCall ?MEMCPY_FAR
        CALL      N:?MEMCPY_FAR      ;; 3 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
// 1726 		pxQueue->pcWriteTo += pxQueue->uxItemSize;
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+20
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1727 		if( pxQueue->pcWriteTo >= pxQueue->pcTail ) /*lint !e946 MISRA exception justified as comparison of pointers is the cleanest solution. */
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, DE             ;; 1 cycle
        SKNC                         ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_178  ;; 4 cycles
        ; ------------------------------------- Block: 76 cycles
// 1728 		{
// 1729 			pxQueue->pcWriteTo = pxQueue->pcHead;
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x8           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        ; ------------------------------------- Block: 23 cycles
??prvNotifyQueueSetContainer_179:
        MOVW      ES:[HL], AX        ;; 2 cycles
        BR        R:??prvNotifyQueueSetContainer_178  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 1730 		}
// 1731 		else
// 1732 		{
// 1733 			mtCOVERAGE_TEST_MARKER();
// 1734 		}
// 1735 	}
// 1736 	else
// 1737 	{
// 1738 		( void ) memcpy( ( void * ) pxQueue->u.pcReadFrom, pvItemToQueue, ( size_t ) pxQueue->uxItemSize ); /*lint !e961 MISRA exception as the casts are only redundant for some ports. */
??prvNotifyQueueSetContainer_180:
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+26
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+24
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
          CFI FunCall ?MEMCPY_FAR
        CALL      N:?MEMCPY_FAR      ;; 3 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+20
// 1739 		pxQueue->u.pcReadFrom -= pxQueue->uxItemSize;
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        XOR       A, #0xFF           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        XOR       A, #0xFF           ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        INCW      BC                 ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x06], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP+0x04], AX      ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, X               ;; 1 cycle
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+20
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1740 		if( pxQueue->u.pcReadFrom < pxQueue->pcHead ) /*lint !e946 MISRA exception justified as comparison of pointers is the cleanest solution. */
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        CMPW      AX, DE             ;; 1 cycle
        BNC       ??prvNotifyQueueSetContainer_181  ;; 4 cycles
        ; ------------------------------------- Block: 76 cycles
// 1741 		{
// 1742 			pxQueue->u.pcReadFrom = ( pxQueue->pcTail - pxQueue->uxItemSize );
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        XCHW      AX, BC             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+22
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+24
        MOV       A, [SP+0x12]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+22
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 30 cycles
// 1743 		}
// 1744 		else
// 1745 		{
// 1746 			mtCOVERAGE_TEST_MARKER();
// 1747 		}
// 1748 
// 1749 		if( xPosition == queueOVERWRITE )
??prvNotifyQueueSetContainer_181:
        MOVW      AX, [SP+0x14]      ;; 1 cycle
        CMPW      AX, #0x2           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_178  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1750 		{
// 1751 			if( uxMessagesWaiting > ( UBaseType_t ) 0 )
        MOVW      AX, [SP]           ;; 1 cycle
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_178  ;; 4 cycles
        ; ------------------------------------- Block: 6 cycles
// 1752 			{
// 1753 				/* An item is not being added but overwritten, so subtract
// 1754 				one from the recorded number of items in the queue so when
// 1755 				one is added again below the number of recorded items remains
// 1756 				correct. */
// 1757 				--uxMessagesWaiting;
        MOVW      AX, [SP]           ;; 1 cycle
        DECW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        ; ------------------------------------- Block: 3 cycles
// 1758 			}
// 1759 			else
// 1760 			{
// 1761 				mtCOVERAGE_TEST_MARKER();
// 1762 			}
// 1763 		}
// 1764 		else
// 1765 		{
// 1766 			mtCOVERAGE_TEST_MARKER();
// 1767 		}
// 1768 	}
// 1769 
// 1770 	pxQueue->uxMessagesWaiting = uxMessagesWaiting + 1;
??prvNotifyQueueSetContainer_178:
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        MOVW      ES:[DE], AX        ;; 2 cycles
// 1771 
// 1772 	return xReturn;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      SP, #0x10          ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock22
        ; ------------------------------------- Block: 19 cycles
        ; ------------------------------------- Total: 334 cycles
// 1773 }
// 1774 /*-----------------------------------------------------------*/
// 1775 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock23 Using cfiCommon2
          CFI Function _prvCopyDataFromQueue
        CODE
// 1776 static void prvCopyDataFromQueue( Queue_t * const pxQueue, void * const pvBuffer )
// 1777 {
_prvCopyDataFromQueue:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 12
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+16
// 1778 	if( pxQueue->uxItemSize != ( UBaseType_t ) 0 )
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        SKNZ                         ;; 4 cycles
        BR        R:??prvNotifyQueueSetContainer_182  ;; 4 cycles
        ; ------------------------------------- Block: 19 cycles
// 1779 	{
// 1780 		pxQueue->u.pcReadFrom += pxQueue->uxItemSize;
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, DE             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        MOV       [SP+0x02], A       ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x0C]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 1781 		if( pxQueue->u.pcReadFrom >= pxQueue->pcTail ) /*lint !e946 MISRA exception justified as use of the relational operator is the cleanest solutions. */
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x4           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        CMPW      AX, DE             ;; 1 cycle
        BC        ??prvNotifyQueueSetContainer_183  ;; 4 cycles
        ; ------------------------------------- Block: 46 cycles
// 1782 		{
// 1783 			pxQueue->u.pcReadFrom = pxQueue->pcHead;
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        POP       BC                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
        ; ------------------------------------- Block: 25 cycles
// 1784 		}
// 1785 		else
// 1786 		{
// 1787 			mtCOVERAGE_TEST_MARKER();
// 1788 		}
// 1789 		( void ) memcpy( ( void * ) pvBuffer, ( void * ) pxQueue->u.pcReadFrom, ( size_t ) pxQueue->uxItemSize ); /*lint !e961 !e418 MISRA exception as the casts are only redundant for some ports.  Also previous logic ensures a null pointer can only be passed to memcpy() when the count is 0. */
??prvNotifyQueueSetContainer_183:
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x38          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0xC           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, [SP]            ;; 1 cycle
        ADDW      SP, #0x2           ;; 1 cycle
          CFI CFA SP+16
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall ?MEMCPY_FAR
        CALL      N:?MEMCPY_FAR      ;; 3 cycles
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        ; ------------------------------------- Block: 32 cycles
// 1790 	}
// 1791 }
??prvNotifyQueueSetContainer_182:
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock23
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 129 cycles
// 1792 /*-----------------------------------------------------------*/
// 1793 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock24 Using cfiCommon2
          CFI Function _prvUnlockQueue
        CODE
// 1794 static void prvUnlockQueue( Queue_t * const pxQueue )
// 1795 {
_prvUnlockQueue:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 6
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
// 1796 	/* THIS FUNCTION MUST BE CALLED WITH THE SCHEDULER SUSPENDED. */
// 1797 
// 1798 	/* The lock counts contains the number of extra data items placed or
// 1799 	removed from the queue while the queue was locked.  When a queue is
// 1800 	locked items can be added or removed, but the event lists cannot be
// 1801 	updated. */
// 1802 	taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_184  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_184:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1803 	{
// 1804 		int8_t cTxLock = pxQueue->cTxLock;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x3B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        SUB       A, #0x81           ;; 1 cycle
        BC        ??prvNotifyQueueSetContainer_185  ;; 4 cycles
        ; ------------------------------------- Block: 18 cycles
        MOV       A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1805 
// 1806 		/* See if data was added to the queue while it was locked. */
// 1807 		while( cTxLock > queueLOCKED_UNMODIFIED )
// 1808 		{
// 1809 			/* Data was posted while the queue was locked.  Are any tasks
// 1810 			blocked waiting for data to become available? */
// 1811 			#if ( configUSE_QUEUE_SETS == 1 )
// 1812 			{
// 1813 				if( pxQueue->pxQueueSetContainer != NULL )
??prvUnlockQueue_0:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_186  ;; 4 cycles
        ; ------------------------------------- Block: 17 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_186:
        BZ        ??prvNotifyQueueSetContainer_187  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 1814 				{
// 1815 					if( prvNotifyQueueSetContainer( pxQueue, queueSEND_TO_BACK ) != pdFALSE )
        CLRW      BC                 ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvNotifyQueueSetContainer
        CALL      F:_prvNotifyQueueSetContainer  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_188  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        BR        S:??prvNotifyQueueSetContainer_189  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1816 					{
// 1817 						/* The queue is a member of a queue set, and posting to
// 1818 						the queue set caused a higher priority task to unblock.
// 1819 						A context switch is required. */
// 1820 						vTaskMissedYield();
// 1821 					}
// 1822 					else
// 1823 					{
// 1824 						mtCOVERAGE_TEST_MARKER();
// 1825 					}
// 1826 				}
// 1827 				else
// 1828 				{
// 1829 					/* Tasks that are removed from the event list will get
// 1830 					added to the pending ready list as the scheduler is still
// 1831 					suspended. */
// 1832 					if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
??prvNotifyQueueSetContainer_187:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_185  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 1833 					{
// 1834 						if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _vTaskMissedYield
        ; ------------------------------------- Block: 9 cycles
// 1835 						{
// 1836 							/* The task waiting has a higher priority so record that a
// 1837 							context	switch is required. */
// 1838 							vTaskMissedYield();
??prvNotifyQueueSetContainer_188:
        CALL      F:_vTaskMissedYield  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1839 						}
// 1840 						else
// 1841 						{
// 1842 							mtCOVERAGE_TEST_MARKER();
// 1843 						}
// 1844 					}
// 1845 					else
// 1846 					{
// 1847 						break;
// 1848 					}
// 1849 				}
// 1850 			}
// 1851 			#else /* configUSE_QUEUE_SETS */
// 1852 			{
// 1853 				/* Tasks that are removed from the event list will get added to
// 1854 				the pending ready list as the scheduler is still suspended. */
// 1855 				if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
// 1856 				{
// 1857 					if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
// 1858 					{
// 1859 						/* The task waiting has a higher priority so record that
// 1860 						a context switch is required. */
// 1861 						vTaskMissedYield();
// 1862 					}
// 1863 					else
// 1864 					{
// 1865 						mtCOVERAGE_TEST_MARKER();
// 1866 					}
// 1867 				}
// 1868 				else
// 1869 				{
// 1870 					break;
// 1871 				}
// 1872 			}
// 1873 			#endif /* configUSE_QUEUE_SETS */
// 1874 
// 1875 			--cTxLock;
??prvNotifyQueueSetContainer_189:
        MOV       A, [SP]            ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 1876 		}
        BNZ       ??prvUnlockQueue_0  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 1877 
// 1878 		pxQueue->cTxLock = queueUNLOCKED;
??prvNotifyQueueSetContainer_185:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x3B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xFF           ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 1879 	}
// 1880 	taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_190  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_190  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1881 
// 1882 	/* Do the same for the Rx lock. */
// 1883 	taskENTER_CRITICAL();
??prvNotifyQueueSetContainer_190:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_191  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_191:
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1884 	{
// 1885 		int8_t cRxLock = pxQueue->cRxLock;
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       X, A               ;; 1 cycle
        XOR       A, #0x80           ;; 1 cycle
        SUB       A, #0x81           ;; 1 cycle
        BC        ??prvNotifyQueueSetContainer_192  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        MOV       A, X               ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
// 1886 
// 1887 		while( cRxLock > queueLOCKED_UNMODIFIED )
// 1888 		{
// 1889 			if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToSend ) ) == pdFALSE )
??prvUnlockQueue_1:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_192  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 1890 			{
// 1891 				if( xTaskRemoveFromEventList( &( pxQueue->xTasksWaitingToSend ) ) != pdFALSE )
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x10          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        SKZ                          ;; 1 cycle
          CFI FunCall _vTaskMissedYield
        ; ------------------------------------- Block: 9 cycles
// 1892 				{
// 1893 					vTaskMissedYield();
        CALL      F:_vTaskMissedYield  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1894 				}
// 1895 				else
// 1896 				{
// 1897 					mtCOVERAGE_TEST_MARKER();
// 1898 				}
// 1899 
// 1900 				--cRxLock;
??prvUnlockQueue_2:
        MOV       A, [SP]            ;; 1 cycle
        DEC       A                  ;; 1 cycle
        MOV       [SP], A            ;; 1 cycle
// 1901 			}
// 1902 			else
// 1903 			{
// 1904 				break;
// 1905 			}
// 1906 		}
        BNZ       ??prvUnlockQueue_1  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 1907 
// 1908 		pxQueue->cRxLock = queueUNLOCKED;
??prvNotifyQueueSetContainer_192:
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x3A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xFF           ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
// 1909 	}
// 1910 	taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_193  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_193  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1911 }
??prvNotifyQueueSetContainer_193:
        ADDW      SP, #0x6           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock24
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 240 cycles
// 1912 /*-----------------------------------------------------------*/
// 1913 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock25 Using cfiCommon2
          CFI Function _prvIsQueueEmpty
          CFI NoCalls
        CODE
// 1914 static BaseType_t prvIsQueueEmpty( const Queue_t *pxQueue )
// 1915 {
_prvIsQueueEmpty:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1916 BaseType_t xReturn;
// 1917 
// 1918 	taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_194  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_194:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1919 	{
// 1920 		if( pxQueue->uxMessagesWaiting == ( UBaseType_t )  0 )
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        ONEW      BC                 ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
        CLRW      BC                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1921 		{
// 1922 			xReturn = pdTRUE;
// 1923 		}
// 1924 		else
// 1925 		{
// 1926 			xReturn = pdFALSE;
// 1927 		}
// 1928 	}
// 1929 	taskEXIT_CRITICAL();
??prvIsQueueEmpty_0:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_195  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_195  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1930 
// 1931 	return xReturn;
??prvNotifyQueueSetContainer_195:
        MOVW      AX, BC             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock25
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 63 cycles
// 1932 }
// 1933 /*-----------------------------------------------------------*/
// 1934 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock26 Using cfiCommon2
          CFI Function _xQueueIsQueueEmptyFromISR
          CFI NoCalls
        CODE
// 1935 BaseType_t xQueueIsQueueEmptyFromISR( const QueueHandle_t xQueue )
// 1936 {
_xQueueIsQueueEmptyFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1937 BaseType_t xReturn;
// 1938 
// 1939 	configASSERT( xQueue );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_196  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_196:
        BNZ       ??prvNotifyQueueSetContainer_197  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_198  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_198:
        BR        S:??prvNotifyQueueSetContainer_198  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1940 	if( ( ( Queue_t * ) xQueue )->uxMessagesWaiting == ( UBaseType_t ) 0 )
??prvNotifyQueueSetContainer_197:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        ONEW      AX                 ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 8 cycles
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1941 	{
// 1942 		xReturn = pdTRUE;
// 1943 	}
// 1944 	else
// 1945 	{
// 1946 		xReturn = pdFALSE;
// 1947 	}
// 1948 
// 1949 	return xReturn;
??xQueueIsQueueEmptyFromISR_0:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock26
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 56 cycles
// 1950 } /*lint !e818 xQueue could not be pointer to const because it is a typedef. */
// 1951 /*-----------------------------------------------------------*/
// 1952 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock27 Using cfiCommon2
          CFI Function _prvIsQueueFull
          CFI NoCalls
        CODE
// 1953 static BaseType_t prvIsQueueFull( const Queue_t *pxQueue )
// 1954 {
_prvIsQueueFull:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1955 BaseType_t xReturn;
// 1956 
// 1957 	taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_199  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_199:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 1958 	{
// 1959 		if( pxQueue->uxMessagesWaiting == pxQueue->uxLength )
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        ONEW      BC                 ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 20 cycles
        CLRW      BC                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1960 		{
// 1961 			xReturn = pdTRUE;
// 1962 		}
// 1963 		else
// 1964 		{
// 1965 			xReturn = pdFALSE;
// 1966 		}
// 1967 	}
// 1968 	taskEXIT_CRITICAL();
??prvIsQueueFull_0:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_200  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_200  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 1969 
// 1970 	return xReturn;
??prvNotifyQueueSetContainer_200:
        MOVW      AX, BC             ;; 1 cycle
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock27
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 69 cycles
// 1971 }
// 1972 /*-----------------------------------------------------------*/
// 1973 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock28 Using cfiCommon2
          CFI Function _xQueueIsQueueFullFromISR
          CFI NoCalls
        CODE
// 1974 BaseType_t xQueueIsQueueFullFromISR( const QueueHandle_t xQueue )
// 1975 {
_xQueueIsQueueFullFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 1976 BaseType_t xReturn;
// 1977 
// 1978 	configASSERT( xQueue );
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_201  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_201:
        BNZ       ??prvNotifyQueueSetContainer_202  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_203  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_203:
        BR        S:??prvNotifyQueueSetContainer_203  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 1979 	if( ( ( Queue_t * ) xQueue )->uxMessagesWaiting == ( ( Queue_t * ) xQueue )->uxLength )
??prvNotifyQueueSetContainer_202:
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        CMPW      AX, HL             ;; 1 cycle
        ONEW      AX                 ;; 1 cycle
        SKZ                          ;; 1 cycle
        ; ------------------------------------- Block: 14 cycles
        CLRW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 1980 	{
// 1981 		xReturn = pdTRUE;
// 1982 	}
// 1983 	else
// 1984 	{
// 1985 		xReturn = pdFALSE;
// 1986 	}
// 1987 
// 1988 	return xReturn;
??xQueueIsQueueFullFromISR_0:
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock28
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 62 cycles
// 1989 } /*lint !e818 xQueue could not be pointer to const because it is a typedef. */
// 1990 /*-----------------------------------------------------------*/
// 1991 
// 1992 #if ( configUSE_CO_ROUTINES == 1 )
// 1993 
// 1994 	BaseType_t xQueueCRSend( QueueHandle_t xQueue, const void *pvItemToQueue, TickType_t xTicksToWait )
// 1995 	{
// 1996 	BaseType_t xReturn;
// 1997 	Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 1998 
// 1999 		/* If the queue is already full we may have to block.  A critical section
// 2000 		is required to prevent an interrupt removing something from the queue
// 2001 		between the check to see if the queue is full and blocking on the queue. */
// 2002 		portDISABLE_INTERRUPTS();
// 2003 		{
// 2004 			if( prvIsQueueFull( pxQueue ) != pdFALSE )
// 2005 			{
// 2006 				/* The queue is full - do we want to block or just leave without
// 2007 				posting? */
// 2008 				if( xTicksToWait > ( TickType_t ) 0 )
// 2009 				{
// 2010 					/* As this is called from a coroutine we cannot block directly, but
// 2011 					return indicating that we need to block. */
// 2012 					vCoRoutineAddToDelayedList( xTicksToWait, &( pxQueue->xTasksWaitingToSend ) );
// 2013 					portENABLE_INTERRUPTS();
// 2014 					return errQUEUE_BLOCKED;
// 2015 				}
// 2016 				else
// 2017 				{
// 2018 					portENABLE_INTERRUPTS();
// 2019 					return errQUEUE_FULL;
// 2020 				}
// 2021 			}
// 2022 		}
// 2023 		portENABLE_INTERRUPTS();
// 2024 
// 2025 		portDISABLE_INTERRUPTS();
// 2026 		{
// 2027 			if( pxQueue->uxMessagesWaiting < pxQueue->uxLength )
// 2028 			{
// 2029 				/* There is room in the queue, copy the data into the queue. */
// 2030 				prvCopyDataToQueue( pxQueue, pvItemToQueue, queueSEND_TO_BACK );
// 2031 				xReturn = pdPASS;
// 2032 
// 2033 				/* Were any co-routines waiting for data to become available? */
// 2034 				if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
// 2035 				{
// 2036 					/* In this instance the co-routine could be placed directly
// 2037 					into the ready list as we are within a critical section.
// 2038 					Instead the same pending ready list mechanism is used as if
// 2039 					the event were caused from within an interrupt. */
// 2040 					if( xCoRoutineRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
// 2041 					{
// 2042 						/* The co-routine waiting has a higher priority so record
// 2043 						that a yield might be appropriate. */
// 2044 						xReturn = errQUEUE_YIELD;
// 2045 					}
// 2046 					else
// 2047 					{
// 2048 						mtCOVERAGE_TEST_MARKER();
// 2049 					}
// 2050 				}
// 2051 				else
// 2052 				{
// 2053 					mtCOVERAGE_TEST_MARKER();
// 2054 				}
// 2055 			}
// 2056 			else
// 2057 			{
// 2058 				xReturn = errQUEUE_FULL;
// 2059 			}
// 2060 		}
// 2061 		portENABLE_INTERRUPTS();
// 2062 
// 2063 		return xReturn;
// 2064 	}
// 2065 
// 2066 #endif /* configUSE_CO_ROUTINES */
// 2067 /*-----------------------------------------------------------*/
// 2068 
// 2069 #if ( configUSE_CO_ROUTINES == 1 )
// 2070 
// 2071 	BaseType_t xQueueCRReceive( QueueHandle_t xQueue, void *pvBuffer, TickType_t xTicksToWait )
// 2072 	{
// 2073 	BaseType_t xReturn;
// 2074 	Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 2075 
// 2076 		/* If the queue is already empty we may have to block.  A critical section
// 2077 		is required to prevent an interrupt adding something to the queue
// 2078 		between the check to see if the queue is empty and blocking on the queue. */
// 2079 		portDISABLE_INTERRUPTS();
// 2080 		{
// 2081 			if( pxQueue->uxMessagesWaiting == ( UBaseType_t ) 0 )
// 2082 			{
// 2083 				/* There are no messages in the queue, do we want to block or just
// 2084 				leave with nothing? */
// 2085 				if( xTicksToWait > ( TickType_t ) 0 )
// 2086 				{
// 2087 					/* As this is a co-routine we cannot block directly, but return
// 2088 					indicating that we need to block. */
// 2089 					vCoRoutineAddToDelayedList( xTicksToWait, &( pxQueue->xTasksWaitingToReceive ) );
// 2090 					portENABLE_INTERRUPTS();
// 2091 					return errQUEUE_BLOCKED;
// 2092 				}
// 2093 				else
// 2094 				{
// 2095 					portENABLE_INTERRUPTS();
// 2096 					return errQUEUE_FULL;
// 2097 				}
// 2098 			}
// 2099 			else
// 2100 			{
// 2101 				mtCOVERAGE_TEST_MARKER();
// 2102 			}
// 2103 		}
// 2104 		portENABLE_INTERRUPTS();
// 2105 
// 2106 		portDISABLE_INTERRUPTS();
// 2107 		{
// 2108 			if( pxQueue->uxMessagesWaiting > ( UBaseType_t ) 0 )
// 2109 			{
// 2110 				/* Data is available from the queue. */
// 2111 				pxQueue->u.pcReadFrom += pxQueue->uxItemSize;
// 2112 				if( pxQueue->u.pcReadFrom >= pxQueue->pcTail )
// 2113 				{
// 2114 					pxQueue->u.pcReadFrom = pxQueue->pcHead;
// 2115 				}
// 2116 				else
// 2117 				{
// 2118 					mtCOVERAGE_TEST_MARKER();
// 2119 				}
// 2120 				--( pxQueue->uxMessagesWaiting );
// 2121 				( void ) memcpy( ( void * ) pvBuffer, ( void * ) pxQueue->u.pcReadFrom, ( unsigned ) pxQueue->uxItemSize );
// 2122 
// 2123 				xReturn = pdPASS;
// 2124 
// 2125 				/* Were any co-routines waiting for space to become available? */
// 2126 				if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToSend ) ) == pdFALSE )
// 2127 				{
// 2128 					/* In this instance the co-routine could be placed directly
// 2129 					into the ready list as we are within a critical section.
// 2130 					Instead the same pending ready list mechanism is used as if
// 2131 					the event were caused from within an interrupt. */
// 2132 					if( xCoRoutineRemoveFromEventList( &( pxQueue->xTasksWaitingToSend ) ) != pdFALSE )
// 2133 					{
// 2134 						xReturn = errQUEUE_YIELD;
// 2135 					}
// 2136 					else
// 2137 					{
// 2138 						mtCOVERAGE_TEST_MARKER();
// 2139 					}
// 2140 				}
// 2141 				else
// 2142 				{
// 2143 					mtCOVERAGE_TEST_MARKER();
// 2144 				}
// 2145 			}
// 2146 			else
// 2147 			{
// 2148 				xReturn = pdFAIL;
// 2149 			}
// 2150 		}
// 2151 		portENABLE_INTERRUPTS();
// 2152 
// 2153 		return xReturn;
// 2154 	}
// 2155 
// 2156 #endif /* configUSE_CO_ROUTINES */
// 2157 /*-----------------------------------------------------------*/
// 2158 
// 2159 #if ( configUSE_CO_ROUTINES == 1 )
// 2160 
// 2161 	BaseType_t xQueueCRSendFromISR( QueueHandle_t xQueue, const void *pvItemToQueue, BaseType_t xCoRoutinePreviouslyWoken )
// 2162 	{
// 2163 	Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 2164 
// 2165 		/* Cannot block within an ISR so if there is no space on the queue then
// 2166 		exit without doing anything. */
// 2167 		if( pxQueue->uxMessagesWaiting < pxQueue->uxLength )
// 2168 		{
// 2169 			prvCopyDataToQueue( pxQueue, pvItemToQueue, queueSEND_TO_BACK );
// 2170 
// 2171 			/* We only want to wake one co-routine per ISR, so check that a
// 2172 			co-routine has not already been woken. */
// 2173 			if( xCoRoutinePreviouslyWoken == pdFALSE )
// 2174 			{
// 2175 				if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToReceive ) ) == pdFALSE )
// 2176 				{
// 2177 					if( xCoRoutineRemoveFromEventList( &( pxQueue->xTasksWaitingToReceive ) ) != pdFALSE )
// 2178 					{
// 2179 						return pdTRUE;
// 2180 					}
// 2181 					else
// 2182 					{
// 2183 						mtCOVERAGE_TEST_MARKER();
// 2184 					}
// 2185 				}
// 2186 				else
// 2187 				{
// 2188 					mtCOVERAGE_TEST_MARKER();
// 2189 				}
// 2190 			}
// 2191 			else
// 2192 			{
// 2193 				mtCOVERAGE_TEST_MARKER();
// 2194 			}
// 2195 		}
// 2196 		else
// 2197 		{
// 2198 			mtCOVERAGE_TEST_MARKER();
// 2199 		}
// 2200 
// 2201 		return xCoRoutinePreviouslyWoken;
// 2202 	}
// 2203 
// 2204 #endif /* configUSE_CO_ROUTINES */
// 2205 /*-----------------------------------------------------------*/
// 2206 
// 2207 #if ( configUSE_CO_ROUTINES == 1 )
// 2208 
// 2209 	BaseType_t xQueueCRReceiveFromISR( QueueHandle_t xQueue, void *pvBuffer, BaseType_t *pxCoRoutineWoken )
// 2210 	{
// 2211 	BaseType_t xReturn;
// 2212 	Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 2213 
// 2214 		/* We cannot block from an ISR, so check there is data available. If
// 2215 		not then just leave without doing anything. */
// 2216 		if( pxQueue->uxMessagesWaiting > ( UBaseType_t ) 0 )
// 2217 		{
// 2218 			/* Copy the data from the queue. */
// 2219 			pxQueue->u.pcReadFrom += pxQueue->uxItemSize;
// 2220 			if( pxQueue->u.pcReadFrom >= pxQueue->pcTail )
// 2221 			{
// 2222 				pxQueue->u.pcReadFrom = pxQueue->pcHead;
// 2223 			}
// 2224 			else
// 2225 			{
// 2226 				mtCOVERAGE_TEST_MARKER();
// 2227 			}
// 2228 			--( pxQueue->uxMessagesWaiting );
// 2229 			( void ) memcpy( ( void * ) pvBuffer, ( void * ) pxQueue->u.pcReadFrom, ( unsigned ) pxQueue->uxItemSize );
// 2230 
// 2231 			if( ( *pxCoRoutineWoken ) == pdFALSE )
// 2232 			{
// 2233 				if( listLIST_IS_EMPTY( &( pxQueue->xTasksWaitingToSend ) ) == pdFALSE )
// 2234 				{
// 2235 					if( xCoRoutineRemoveFromEventList( &( pxQueue->xTasksWaitingToSend ) ) != pdFALSE )
// 2236 					{
// 2237 						*pxCoRoutineWoken = pdTRUE;
// 2238 					}
// 2239 					else
// 2240 					{
// 2241 						mtCOVERAGE_TEST_MARKER();
// 2242 					}
// 2243 				}
// 2244 				else
// 2245 				{
// 2246 					mtCOVERAGE_TEST_MARKER();
// 2247 				}
// 2248 			}
// 2249 			else
// 2250 			{
// 2251 				mtCOVERAGE_TEST_MARKER();
// 2252 			}
// 2253 
// 2254 			xReturn = pdPASS;
// 2255 		}
// 2256 		else
// 2257 		{
// 2258 			xReturn = pdFAIL;
// 2259 		}
// 2260 
// 2261 		return xReturn;
// 2262 	}
// 2263 
// 2264 #endif /* configUSE_CO_ROUTINES */
// 2265 /*-----------------------------------------------------------*/
// 2266 
// 2267 #if ( configQUEUE_REGISTRY_SIZE > 0 )
// 2268 
// 2269 	void vQueueAddToRegistry( QueueHandle_t xQueue, const char *pcQueueName ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
// 2270 	{
// 2271 	UBaseType_t ux;
// 2272 
// 2273 		/* See if there is an empty space in the registry.  A NULL name denotes
// 2274 		a free slot. */
// 2275 		for( ux = ( UBaseType_t ) 0U; ux < ( UBaseType_t ) configQUEUE_REGISTRY_SIZE; ux++ )
// 2276 		{
// 2277 			if( xQueueRegistry[ ux ].pcQueueName == NULL )
// 2278 			{
// 2279 				/* Store the information on this queue. */
// 2280 				xQueueRegistry[ ux ].pcQueueName = pcQueueName;
// 2281 				xQueueRegistry[ ux ].xHandle = xQueue;
// 2282 
// 2283 				traceQUEUE_REGISTRY_ADD( xQueue, pcQueueName );
// 2284 				break;
// 2285 			}
// 2286 			else
// 2287 			{
// 2288 				mtCOVERAGE_TEST_MARKER();
// 2289 			}
// 2290 		}
// 2291 	}
// 2292 
// 2293 #endif /* configQUEUE_REGISTRY_SIZE */
// 2294 /*-----------------------------------------------------------*/
// 2295 
// 2296 #if ( configQUEUE_REGISTRY_SIZE > 0 )
// 2297 
// 2298 	const char *pcQueueGetName( QueueHandle_t xQueue ) /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
// 2299 	{
// 2300 	UBaseType_t ux;
// 2301 	const char *pcReturn = NULL; /*lint !e971 Unqualified char types are allowed for strings and single characters only. */
// 2302 
// 2303 		/* Note there is nothing here to protect against another task adding or
// 2304 		removing entries from the registry while it is being searched. */
// 2305 		for( ux = ( UBaseType_t ) 0U; ux < ( UBaseType_t ) configQUEUE_REGISTRY_SIZE; ux++ )
// 2306 		{
// 2307 			if( xQueueRegistry[ ux ].xHandle == xQueue )
// 2308 			{
// 2309 				pcReturn = xQueueRegistry[ ux ].pcQueueName;
// 2310 				break;
// 2311 			}
// 2312 			else
// 2313 			{
// 2314 				mtCOVERAGE_TEST_MARKER();
// 2315 			}
// 2316 		}
// 2317 
// 2318 		return pcReturn;
// 2319 	}
// 2320 
// 2321 #endif /* configQUEUE_REGISTRY_SIZE */
// 2322 /*-----------------------------------------------------------*/
// 2323 
// 2324 #if ( configQUEUE_REGISTRY_SIZE > 0 )
// 2325 
// 2326 	void vQueueUnregisterQueue( QueueHandle_t xQueue )
// 2327 	{
// 2328 	UBaseType_t ux;
// 2329 
// 2330 		/* See if the handle of the queue being unregistered in actually in the
// 2331 		registry. */
// 2332 		for( ux = ( UBaseType_t ) 0U; ux < ( UBaseType_t ) configQUEUE_REGISTRY_SIZE; ux++ )
// 2333 		{
// 2334 			if( xQueueRegistry[ ux ].xHandle == xQueue )
// 2335 			{
// 2336 				/* Set the name to NULL to show that this slot if free again. */
// 2337 				xQueueRegistry[ ux ].pcQueueName = NULL;
// 2338 
// 2339 				/* Set the handle to NULL to ensure the same queue handle cannot
// 2340 				appear in the registry twice if it is added, removed, then
// 2341 				added again. */
// 2342 				xQueueRegistry[ ux ].xHandle = ( QueueHandle_t ) 0;
// 2343 				break;
// 2344 			}
// 2345 			else
// 2346 			{
// 2347 				mtCOVERAGE_TEST_MARKER();
// 2348 			}
// 2349 		}
// 2350 
// 2351 	} /*lint !e818 xQueue could not be pointer to const because it is a typedef. */
// 2352 
// 2353 #endif /* configQUEUE_REGISTRY_SIZE */
// 2354 /*-----------------------------------------------------------*/
// 2355 
// 2356 #if ( configUSE_TIMERS == 1 )
// 2357 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock29 Using cfiCommon0
          CFI Function _vQueueWaitForMessageRestricted
        CODE
// 2358 	void vQueueWaitForMessageRestricted( QueueHandle_t xQueue, TickType_t xTicksToWait, const BaseType_t xWaitIndefinitely )
// 2359 	{
_vQueueWaitForMessageRestricted:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 4
// 2360 	Queue_t * const pxQueue = ( Queue_t * ) xQueue;
// 2361 
// 2362 		/* This function should not be called by application code hence the
// 2363 		'Restricted' in its name.  It is not part of the public API.  It is
// 2364 		designed for use by kernel code, and has special calling requirements.
// 2365 		It can result in vListInsert() being called on a list that can only
// 2366 		possibly ever have one item in it, so the list will be fast, but even
// 2367 		so it should be called with the scheduler locked and not from a critical
// 2368 		section. */
// 2369 
// 2370 		/* Only do anything if there are no messages in the queue.  This function
// 2371 		will not actually cause the task to block, just place it on a blocked
// 2372 		list.  It will not block until the scheduler is unlocked - at which
// 2373 		time a yield will be performed.  If an item is added to the queue while
// 2374 		the queue is locked, and the calling task blocks on the queue, then the
// 2375 		calling task will be immediately unblocked when the queue is unlocked. */
// 2376 		prvLockQueue( pxQueue );
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_204  ;; 4 cycles
        ; ------------------------------------- Block: 11 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_204:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x3A          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        INC       A                  ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_205  ;; 4 cycles
        ; ------------------------------------- Block: 16 cycles
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_205:
        INCW      HL                 ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        INC       A                  ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_206  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 3 cycles
??prvNotifyQueueSetContainer_206:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_207  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_207  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2377 		if( pxQueue->uxMessagesWaiting == ( UBaseType_t ) 0U )
??prvNotifyQueueSetContainer_207:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_208  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
        MOVW      AX, [SP+0x0A]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
// 2378 		{
// 2379 			/* There is nothing in the queue, block for the specified period. */
// 2380 			vTaskPlaceOnEventListRestricted( &( pxQueue->xTasksWaitingToReceive ), xTicksToWait, xWaitIndefinitely );
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+12
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _vTaskPlaceOnEventListRestricted
        CALL      F:_vTaskPlaceOnEventListRestricted  ;; 3 cycles
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+8
        ; ------------------------------------- Block: 13 cycles
// 2381 		}
// 2382 		else
// 2383 		{
// 2384 			mtCOVERAGE_TEST_MARKER();
// 2385 		}
// 2386 		prvUnlockQueue( pxQueue );
??prvNotifyQueueSetContainer_208:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP]           ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _prvUnlockQueue
        CALL      F:_prvUnlockQueue  ;; 3 cycles
// 2387 	}
        ADDW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock29
        ; ------------------------------------- Block: 15 cycles
        ; ------------------------------------- Total: 110 cycles
// 2388 
// 2389 #endif /* configUSE_TIMERS */
// 2390 /*-----------------------------------------------------------*/
// 2391 
// 2392 #if( ( configUSE_QUEUE_SETS == 1 ) && ( configSUPPORT_DYNAMIC_ALLOCATION == 1 ) )
// 2393 
// 2394 	QueueSetHandle_t xQueueCreateSet( const UBaseType_t uxEventQueueLength )
// 2395 	{
// 2396 	QueueSetHandle_t pxQueue;
// 2397 
// 2398 		pxQueue = xQueueGenericCreate( uxEventQueueLength, sizeof( Queue_t * ), queueQUEUE_TYPE_SET );
// 2399 
// 2400 		return pxQueue;
// 2401 	}
// 2402 
// 2403 #endif /* configUSE_QUEUE_SETS */
// 2404 /*-----------------------------------------------------------*/
// 2405 
// 2406 #if ( configUSE_QUEUE_SETS == 1 )
// 2407 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock30 Using cfiCommon2
          CFI Function _xQueueAddToSet
          CFI NoCalls
        CODE
// 2408 	BaseType_t xQueueAddToSet( QueueSetMemberHandle_t xQueueOrSemaphore, QueueSetHandle_t xQueueSet )
// 2409 	{
_xQueueAddToSet:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 8
// 2410 	BaseType_t xReturn;
// 2411 
// 2412 		taskENTER_CRITICAL();
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_209  ;; 4 cycles
        ; ------------------------------------- Block: 14 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_209:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 2413 		{
// 2414 			if( ( ( Queue_t * ) xQueueOrSemaphore )->pxQueueSetContainer != NULL )
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_210  ;; 4 cycles
        ; ------------------------------------- Block: 21 cycles
        MOVW      AX, DE             ;; 1 cycle
        OR        A, X               ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_210:
        BNZ       ??prvNotifyQueueSetContainer_211  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2415 			{
// 2416 				/* Cannot add a queue/semaphore to more than one queue set. */
// 2417 				xReturn = pdFAIL;
// 2418 			}
// 2419 			else if( ( ( Queue_t * ) xQueueOrSemaphore )->uxMessagesWaiting != ( UBaseType_t ) 0 )
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_212  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 2420 			{
// 2421 				/* Cannot add a queue/semaphore to a queue set if there are already
// 2422 				items in the queue/semaphore. */
// 2423 				xReturn = pdFAIL;
??prvNotifyQueueSetContainer_211:
        CLRW      AX                 ;; 1 cycle
        BR        S:??prvNotifyQueueSetContainer_213  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2424 			}
// 2425 			else
// 2426 			{
// 2427 				( ( Queue_t * ) xQueueOrSemaphore )->pxQueueSetContainer = xQueueSet;
??prvNotifyQueueSetContainer_212:
        MOV       A, [SP+0x02]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+14
        POP       BC                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, C               ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        MOVW      AX, DE             ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 2428 				xReturn = pdPASS;
        ONEW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 22 cycles
??prvNotifyQueueSetContainer_213:
        MOVW      HL, AX             ;; 1 cycle
// 2429 			}
// 2430 		}
// 2431 		taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_214  ;; 4 cycles
        ; ------------------------------------- Block: 9 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_214  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2432 
// 2433 		return xReturn;
??prvNotifyQueueSetContainer_214:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock30
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 117 cycles
// 2434 	}
// 2435 
// 2436 #endif /* configUSE_QUEUE_SETS */
// 2437 /*-----------------------------------------------------------*/
// 2438 
// 2439 #if ( configUSE_QUEUE_SETS == 1 )
// 2440 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock31 Using cfiCommon2
          CFI Function _xQueueRemoveFromSet
          CFI NoCalls
        CODE
// 2441 	BaseType_t xQueueRemoveFromSet( QueueSetMemberHandle_t xQueueOrSemaphore, QueueSetHandle_t xQueueSet )
// 2442 	{
_xQueueRemoveFromSet:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        XCH       A, X               ;; 1 cycle
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+10
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+12
        ; Auto size: 8
// 2443 	BaseType_t xReturn;
// 2444 	Queue_t * const pxQueueOrSemaphore = ( Queue_t * ) xQueueOrSemaphore;
// 2445 
// 2446 		if( pxQueueOrSemaphore->pxQueueSetContainer != xQueueSet )
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, BC             ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+16
        MOV       A, [SP+0x0A]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOV       A, ES:[HL+0x02]    ;; 2 cycles
        MOV       ES, A              ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        POP       BC                 ;; 1 cycle
          CFI CFA SP+14
        MOV       A, [SP]            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        POP       DE                 ;; 1 cycle
          CFI CFA SP+12
        MOV       A, ES              ;; 1 cycle
        CMP       A, X               ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_215  ;; 4 cycles
        ; ------------------------------------- Block: 36 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, BC             ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_215:
        BNZ       ??prvNotifyQueueSetContainer_216  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
// 2447 		{
// 2448 			/* The queue was not a member of the set. */
// 2449 			xReturn = pdFAIL;
// 2450 		}
// 2451 		else if( pxQueueOrSemaphore->uxMessagesWaiting != ( UBaseType_t ) 0 )
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x34          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_217  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 2452 		{
// 2453 			/* It is dangerous to remove a queue from a set when the queue is
// 2454 			not empty because the queue set will still hold pending events for
// 2455 			the queue. */
// 2456 			xReturn = pdFAIL;
??prvNotifyQueueSetContainer_216:
        CLRW      AX                 ;; 1 cycle
        BR        S:??prvNotifyQueueSetContainer_218  ;; 3 cycles
        ; ------------------------------------- Block: 4 cycles
// 2457 		}
// 2458 		else
// 2459 		{
// 2460 			taskENTER_CRITICAL();
??prvNotifyQueueSetContainer_217:
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_219  ;; 4 cycles
        ; ------------------------------------- Block: 8 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_219:
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        INCW      ES:_usCriticalNesting  ;; 3 cycles
// 2461 			{
// 2462 				/* The queue is no longer contained in the set. */
// 2463 				pxQueueOrSemaphore->pxQueueSetContainer = NULL;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        CLRB      A                  ;; 1 cycle
        MOV       ES:[HL+0x02], A    ;; 2 cycles
        CLRW      AX                 ;; 1 cycle
        MOVW      ES:[HL], AX        ;; 2 cycles
// 2464 			}
// 2465 			taskEXIT_CRITICAL();
        MOV       ES, #BYTE3(_usCriticalNesting)  ;; 1 cycle
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_220  ;; 4 cycles
        ; ------------------------------------- Block: 23 cycles
        DECW      ES:_usCriticalNesting  ;; 3 cycles
        MOVW      AX, ES:_usCriticalNesting  ;; 2 cycles
        CMPW      AX, #0x0           ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_220  ;; 4 cycles
        ; ------------------------------------- Block: 10 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        OR        A, #0x6            ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 6 cycles
// 2466 			xReturn = pdPASS;
??prvNotifyQueueSetContainer_220:
        ONEW      AX                 ;; 1 cycle
        ; ------------------------------------- Block: 1 cycles
// 2467 		}
// 2468 
// 2469 		return xReturn;
??prvNotifyQueueSetContainer_218:
        ADDW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock31
        ; ------------------------------------- Block: 7 cycles
        ; ------------------------------------- Total: 118 cycles
// 2470 	} /*lint !e818 xQueueSet could not be declared as pointing to const as it is a typedef. */
// 2471 
// 2472 #endif /* configUSE_QUEUE_SETS */
// 2473 /*-----------------------------------------------------------*/
// 2474 
// 2475 #if ( configUSE_QUEUE_SETS == 1 )
// 2476 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock32 Using cfiCommon2
          CFI Function _xQueueSelectFromSet
        CODE
// 2477 	QueueSetMemberHandle_t xQueueSelectFromSet( QueueSetHandle_t xQueueSet, TickType_t const xTicksToWait )
// 2478 	{
_xQueueSelectFromSet:
        ; * Stack frame (at entry) *
        ; Param size: 4
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 2479 	QueueSetMemberHandle_t xReturn = NULL;
        MOV       [SP+0x02], #0x0    ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 2480 
// 2481 		( void ) xQueueGenericReceive( ( QueueHandle_t ) xQueueSet, &xReturn, xTicksToWait, pdFALSE ); /*lint !e961 Casting from one typedef to another is not redundant. */
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        MOVW      AX, [SP+0x10]      ;; 1 cycle
        MOVW      BC, AX             ;; 1 cycle
        MOVW      AX, [SP+0x0E]      ;; 1 cycle
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+16
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0x6           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        MOV       A, [SP+0x10]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _xQueueGenericReceive
        CALL      F:_xQueueGenericReceive  ;; 3 cycles
// 2482 		return xReturn;
        MOV       A, [SP+0x08]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0xE           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock32
        ; ------------------------------------- Block: 43 cycles
        ; ------------------------------------- Total: 43 cycles
// 2483 	}
// 2484 
// 2485 #endif /* configUSE_QUEUE_SETS */
// 2486 /*-----------------------------------------------------------*/
// 2487 
// 2488 #if ( configUSE_QUEUE_SETS == 1 )
// 2489 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock33 Using cfiCommon2
          CFI Function _xQueueSelectFromSetFromISR
        CODE
// 2490 	QueueSetMemberHandle_t xQueueSelectFromSetFromISR( QueueSetHandle_t xQueueSet )
// 2491 	{
_xQueueSelectFromSetFromISR:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 8
        SUBW      SP, #0x4           ;; 1 cycle
          CFI CFA SP+12
// 2492 	QueueSetMemberHandle_t xReturn = NULL;
        MOV       [SP+0x02], #0x0    ;; 1 cycle
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP], AX           ;; 1 cycle
// 2493 
// 2494 		( void ) xQueueReceiveFromISR( ( QueueHandle_t ) xQueueSet, &xReturn, NULL ); /*lint !e961 Casting from one typedef to another is not redundant. */
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+14
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+16
        MOVW      HL, SP             ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        INCW      HL                 ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+18
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+20
        MOV       A, [SP+0x0E]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+18
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
          CFI FunCall _xQueueReceiveFromISR
        CALL      F:_xQueueReceiveFromISR  ;; 3 cycles
// 2495 		return xReturn;
        MOV       A, [SP+0x06]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x04]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock33
        ; ------------------------------------- Block: 41 cycles
        ; ------------------------------------- Total: 41 cycles
// 2496 	}
// 2497 
// 2498 #endif /* configUSE_QUEUE_SETS */
// 2499 /*-----------------------------------------------------------*/
// 2500 
// 2501 #if ( configUSE_QUEUE_SETS == 1 )
// 2502 

        SECTION `.textf`:FARCODE:NOROOT(0)
        SECTION_TYPE SHT_PROGBITS, SHF_EXECINSTR
          CFI Block cfiBlock34 Using cfiCommon0
          CFI Function _prvNotifyQueueSetContainer
        CODE
// 2503 	static BaseType_t prvNotifyQueueSetContainer( const Queue_t * const pxQueue, const BaseType_t xCopyPosition )
// 2504 	{
_prvNotifyQueueSetContainer:
        ; * Stack frame (at entry) *
        ; Param size: 0
        XCH       A, X               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+6
        PUSH      DE                 ;; 1 cycle
          CFI CFA SP+8
        ; Auto size: 12
        SUBW      SP, #0x8           ;; 1 cycle
          CFI CFA SP+16
// 2505 	Queue_t *pxQueueSetContainer = pxQueue->pxQueueSetContainer;
        MOV       A, X               ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, DE             ;; 1 cycle
        ADDW      AX, #0x3C          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES:[DE+0x02]    ;; 2 cycles
        MOV       [SP+0x04], A       ;; 1 cycle
        MOVW      AX, ES:[DE]        ;; 2 cycles
        MOVW      [SP+0x02], AX      ;; 1 cycle
// 2506 	BaseType_t xReturn = pdFALSE;
        CLRW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
// 2507 
// 2508 		/* This function must be called form a critical section. */
// 2509 
// 2510 		configASSERT( pxQueueSetContainer );
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        CMP       A, #0x0            ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_221  ;; 4 cycles
        ; ------------------------------------- Block: 27 cycles
        MOVW      AX, HL             ;; 1 cycle
        CMPW      AX, #0x0           ;; 1 cycle
        ; ------------------------------------- Block: 2 cycles
??prvNotifyQueueSetContainer_221:
        BNZ       ??prvNotifyQueueSetContainer_222  ;; 4 cycles
        ; ------------------------------------- Block: 4 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_223  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_223:
        BR        S:??prvNotifyQueueSetContainer_223  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2511 		configASSERT( pxQueueSetContainer->uxMessagesWaiting < pxQueueSetContainer->uxLength );
??prvNotifyQueueSetContainer_222:
        MOVW      AX, HL             ;; 1 cycle
        ADDW      AX, #0x36          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        MOVW      DE, AX             ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        DECW      HL                 ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, DE             ;; 1 cycle
        BC        ??prvNotifyQueueSetContainer_224  ;; 4 cycles
        ; ------------------------------------- Block: 15 cycles
        BRK                          ;; 5 cycles
        MOV       A, PSW             ;; 1 cycle
        ROR       A, 0x1             ;; 1 cycle
        AND       A, #0x3            ;; 1 cycle
        CMP0      A                  ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_225  ;; 4 cycles
        ; ------------------------------------- Block: 13 cycles
        MOV       A, PSW             ;; 1 cycle
        AND       A, #0xF9           ;; 1 cycle
        MOV       PSW, A             ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
??prvNotifyQueueSetContainer_225:
        BR        S:??prvNotifyQueueSetContainer_225  ;; 3 cycles
        ; ------------------------------------- Block: 3 cycles
// 2512 
// 2513 		if( pxQueueSetContainer->uxMessagesWaiting < pxQueueSetContainer->uxLength )
??prvNotifyQueueSetContainer_224:
        MOVW      AX, ES:[HL]        ;; 2 cycles
        CMPW      AX, DE             ;; 1 cycle
        BNC       ??prvNotifyQueueSetContainer_226  ;; 4 cycles
        ; ------------------------------------- Block: 7 cycles
// 2514 		{
// 2515 			const int8_t cTxLock = pxQueueSetContainer->cTxLock;
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x3B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, ES:[HL]         ;; 2 cycles
        MOV       [SP], A            ;; 1 cycle
// 2516 
// 2517 			traceQUEUE_SEND( pxQueueSetContainer );
// 2518 
// 2519 			/* The data copied is the handle of the queue that contains data. */
// 2520 			xReturn = prvCopyDataToQueue( pxQueueSetContainer, &pxQueue, xCopyPosition );
        PUSH      BC                 ;; 1 cycle
          CFI CFA SP+18
        MOVW      AX, SP             ;; 1 cycle
        ADDW      AX, #0xA           ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, #0xF            ;; 1 cycle
        MOV       X, A               ;; 1 cycle
        PUSH      AX                 ;; 1 cycle
          CFI CFA SP+20
        PUSH      HL                 ;; 1 cycle
          CFI CFA SP+22
        MOVW      AX, [SP+0x08]      ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
        POP       BC                 ;; 1 cycle
          CFI CFA SP+20
        MOV       X, A               ;; 1 cycle
        MOV       A, [SP]            ;; 1 cycle
        XCH       A, X               ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+18
          CFI FunCall _prvCopyDataToQueue
        CALL      F:_prvCopyDataToQueue  ;; 3 cycles
        MOVW      [SP+0x08], AX      ;; 1 cycle
// 2521 
// 2522 			if( cTxLock == queueUNLOCKED )
        MOV       A, [SP+0x02]       ;; 1 cycle
        POP       HL                 ;; 1 cycle
          CFI CFA SP+16
        INC       A                  ;; 1 cycle
        BNZ       ??prvNotifyQueueSetContainer_227  ;; 4 cycles
        ; ------------------------------------- Block: 33 cycles
// 2523 			{
// 2524 				if( listLIST_IS_EMPTY( &( pxQueueSetContainer->xTasksWaitingToReceive ) ) == pdFALSE )
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOVW      AX, ES:[HL]        ;; 2 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_226  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 2525 				{
// 2526 					if( xTaskRemoveFromEventList( &( pxQueueSetContainer->xTasksWaitingToReceive ) ) != pdFALSE )
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x22          ;; 1 cycle
        MOVW      DE, AX             ;; 1 cycle
        MOV       A, ES              ;; 1 cycle
          CFI FunCall _xTaskRemoveFromEventList
        CALL      F:_xTaskRemoveFromEventList  ;; 3 cycles
        OR        A, X               ;; 1 cycle
        BZ        ??prvNotifyQueueSetContainer_226  ;; 4 cycles
        ; ------------------------------------- Block: 12 cycles
// 2527 					{
// 2528 						/* The task waiting has a higher priority. */
// 2529 						xReturn = pdTRUE;
        ONEW      AX                 ;; 1 cycle
        MOVW      [SP+0x06], AX      ;; 1 cycle
        BR        S:??prvNotifyQueueSetContainer_226  ;; 3 cycles
        ; ------------------------------------- Block: 5 cycles
// 2530 					}
// 2531 					else
// 2532 					{
// 2533 						mtCOVERAGE_TEST_MARKER();
// 2534 					}
// 2535 				}
// 2536 				else
// 2537 				{
// 2538 					mtCOVERAGE_TEST_MARKER();
// 2539 				}
// 2540 			}
// 2541 			else
// 2542 			{
// 2543 				pxQueueSetContainer->cTxLock = ( int8_t ) ( cTxLock + 1 );
??prvNotifyQueueSetContainer_227:
        MOV       A, [SP]            ;; 1 cycle
        MOV       B, A               ;; 1 cycle
        INC       B                  ;; 1 cycle
        MOV       A, [SP+0x04]       ;; 1 cycle
        MOV       ES, A              ;; 1 cycle
        MOVW      AX, [SP+0x02]      ;; 1 cycle
        ADDW      AX, #0x3B          ;; 1 cycle
        MOVW      HL, AX             ;; 1 cycle
        MOV       A, B               ;; 1 cycle
        MOV       ES:[HL], A         ;; 2 cycles
        ; ------------------------------------- Block: 11 cycles
// 2544 			}
// 2545 		}
// 2546 		else
// 2547 		{
// 2548 			mtCOVERAGE_TEST_MARKER();
// 2549 		}
// 2550 
// 2551 		return xReturn;
??prvNotifyQueueSetContainer_226:
        MOVW      AX, [SP+0x06]      ;; 1 cycle
        ADDW      SP, #0xC           ;; 1 cycle
          CFI CFA SP+4
        RET                          ;; 6 cycles
          CFI EndBlock cfiBlock34
        ; ------------------------------------- Block: 8 cycles
        ; ------------------------------------- Total: 178 cycles
// 2552 	}

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
// 2553 
// 2554 #endif /* configUSE_QUEUE_SETS */
// 2555 
// 2556 
// 2557 
// 2558 
// 2559 
// 2560 
// 2561 
// 2562 
// 2563 
// 2564 
// 2565 
// 2566 
// 
// 6 303 bytes in section .textf
// 
// 6 303 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
