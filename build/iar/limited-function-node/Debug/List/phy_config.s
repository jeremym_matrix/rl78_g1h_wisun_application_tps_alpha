///////////////////////////////////////////////////////////////////////////////
//
// IAR C/C++ Compiler V4.10.1.2197 for RL78               24/Feb/2022  11:30:55
// Copyright 2011-2018 IAR Systems AB.
// Startup license - IAR Embedded Workbench for Renesas RL78 4.21
//
//    Core               =  s3
//    Calling convention =  v2
//    Code model         =  Far
//    Data model         =  Far
//                       =   
//    Source file        =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_config.c
//    Command line       =  
//        -f C:\Users\Jeremy\AppData\Local\Temp\EW32B7.tmp
//        (C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_config.c
//        --core s3 --code_model far --calling_convention v2
//        --near_const_location rom0 -o
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\Obj
//        --dlib_config "C:\Program Files (x86)\IAR Systems\Embedded Workbench
//        8.1\rl78\LIB\DLib_Config_Normal.h" --double=32 --use_c++_inline -e
//        -Oh --no_cross_call --no_scheduling --debug -D __RL78__ -D
//        RP_CPU_CLK=32 -D RTOS_USE=1 -D FreeRTOS=1 -D R_DEV_FIXED_GTKS=1 -D
//        MCU_R78G1H -D R_USE_RENESAS_STACK=1 -D RP_WISUN_FAN_STACK=1 -D
//        RP_aMaxPHYPacketSize=1600 -D R_MINIMAL_ROUTER_NODE=1 -D
//        R_MAX_CHILD_NODES=1 -D R_MAX_PARENT_CANDIDATES=2 -D
//        R_LOG_THRESHOLD=R_LOG_SEVERITY_DBG -D FILE=void -D
//        R_WISUN_FAN_VERSION=110 -D R_LEAF_NODE_ENABLED=1 -D R_DEV_FAST_JOIN=1
//        -lA
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List
//        --diag_suppress Pa039,Pa082 -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\include\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\FreeRTOS\Source\portable\Renesas\RL78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\app\config\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\auth\api\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\flash\rl78\library\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\modem\common\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\platform\TK_RLG1H_SB2\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\freertos_wrapper\inc\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\
//        -I
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\rl78\
//        --data_model far)
//    Locale             =  C
//    List file          =  
//        C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\build\iar\limited-function-node\Debug\List\phy_config.s
//
///////////////////////////////////////////////////////////////////////////////

        RTMODEL "__SystemLibrary", "DLib"
        RTMODEL "__calling_convention", "v2"
        RTMODEL "__code_model", "far"
        RTMODEL "__core", "s3"
        RTMODEL "__data_model", "far"
        RTMODEL "__dlib_version", "6"
        RTMODEL "__double_size", "32"
        RTMODEL "__far_rt_calls", "false"
        RTMODEL "__near_const", "rom0"
        RTMODEL "__rt_version", "2"

        #define SHT_PROGBITS 0x1

        PUBLIC _RpConfig
// C:\Users\Jeremy\git\rl78_g1h_wisun_application_tps_alpha\rl78_g1h_wisun_application_tps_alpha\stack\phy\raa604s00_rl78\phy_config.c
//    1 /***********************************************************************************************************************
//    2  * DISCLAIMER
//    3  * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
//    4  * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
//    5  * applicable laws, including copyright laws. 
//    6  * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
//    7  * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
//    8  * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
//    9  * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
//   10  * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
//   11  * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
//   12  * DAMAGES.
//   13  * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
//   14  * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
//   15  * following link:
//   16  * http://www.renesas.com/disclaimer 
//   17  **********************************************************************************************************************/
//   18 /***********************************************************************************************************************
//   19  * file name	: phy_config.c
//   20  * description	: This is the RF driver's configuration code.
//   21  ***********************************************************************************************************************
//   22  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
//   23  **********************************************************************************************************************/
//   24 /***************************************************************************************************************
//   25  * includes
//   26  **************************************************************************************************************/
//   27 #include <phy.h>
//   28 #include <phy_def.h>
//   29 #include <phy_config.h>
//   30 
//   31 #if (RP_USR_RF_LIMIT_TMR_SELECT == RP_CLK_LIMIT_TMR_32KHz)
//   32 	#if (RP_USR_RF_TAU_CH_SELECT != RP_TAU_CH01)
//   33 		#error "error: Not Select Timer Array Unit 0 Channel 1"
//   34 	#endif
//   35 #endif
//   36 
//   37 /***************************************************************************************************************
//   38  * Exported global variables and functions (to be accessed by other files)
//   39  * Please refer and modify definitions in phy_config.h
//   40  **************************************************************************************************************/

        SECTION `.constf`:FARCODE:REORDER:NOROOT(1)
//   41 const RP_CONFIG_CB RpConfig =
_RpConfig:
        DATA8
        DB 89, 0
        DATA16
        DW 65443
        DATA8
        DB 0, 0, 0, 0, 0, 0, 1, 0

        SECTION `.iar_vfe_header`:DATA:NOALLOC:NOROOT(1)
        SECTION_TYPE SHT_PROGBITS, 0
        DATA
        DC32 0

        END
//   42 {
//   43 	RP_USR_RF_DFLT_TRANSMIT_POWER,			// PIB phyTransmitPower Default
//   44 	(uint16_t)RP_USR_RF_DFLT_CCA_VTH,		// PIB phyCcaVth Default
//   45 	RP_USR_RF_DFLT_PROFILE_SPECIFIC_MODE,	// PIB phyProfileSpecificMode Default
//   46 	RP_USR_RF_DFLT_TX_ANTENNA_SWITCH_ENA,	// PIB phyTxAntennaSwitchEna Default
//   47 	RP_USR_RF_DFLT_ANTENNA_DIVERSITY_ENA,	// PIB phyAntennaDiversityEna Default
//   48 	RP_USR_RF_RSSI_LOSS,					// RSSI Loss diff dBm
//   49 	RP_USR_RF_CLK_SOURCE_SELECT,			// clock Source for RF
//   50 	RP_USR_RF_LIMIT_TMR_SELECT,				// clock Source for tx limit control
//   51 	RP_USR_RF_TAU_CH_SELECT,				// Select Timer Array Unit Channel
//   52 };
//   53 
//   54 /***********************************************************************************************************************
//   55  * Copyright (C) 2014-2021 Renesas Electronics Corporation.
//   56  **********************************************************************************************************************/
//   57  
// 
// 12 bytes in section .constf
// 
// 12 bytes of FARCODE memory
//
//Errors: none
//Warnings: none
