/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2011-2019 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/**
 * @file
 * @version 2.0
 * @brief This module provides unified OS services access
 */

#ifndef R_OS_WRAPPER_H
#define R_OS_WRAPPER_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "stddef.h"
#include "r_os_msg_types.h"
#include "r_os_wrapper_config.h"

/******************************************************************************
   Functions Prototypes
******************************************************************************/

/**
 * Receive OS message from OS queue with timeout
 * @param mbxId OS queue ID where to get the message from
 * @param timeOutMs Timeout in ms to wait for the message from the OS queue
 * @return the message or NULL if no message is available
 */
r_os_msg_header_t* R_OS_ReceiveMsg(r_os_id_t mbxId, uint32_t timeOutMs);

/**
 * Send OS message to OS queue
 * @attention this function does not support the MAC mailbox (see ::RoaSndMsg)
 * @param msg OS message to send to OS event
 * @param mbxId OS queue ID where to send the message to
 */
void R_OS_SendMsg(r_os_msg_header_t* msg, r_os_id_t mbxId);

/**
 * Send OS message to OS queue
 * @attention this function may only be called from an interrupt context and does not support sending to the MAC queue
 * @param msg OS message to send to OS event
 * @param mbxId OS queue ID where to send the message to
 */
void R_OS_SendMsgFromISR(r_os_msg_header_t* msg, r_os_id_t mbxId);

/**
 * Allocate a message on the specified heap
 * @param memId OS memory ID from where to get memory block
 * @param size the size of the new message
 * @return a pointer to the new message or NULL if out of memory
 */
r_os_msg_header_t* R_OS_MsgAlloc(r_os_id_t memId, size_t size);

/**
 * Free a message previously allocated with R_OS_MsgAlloc()
 * @param msg the OS message
 */
void R_OS_MsgFree(r_os_msg_header_t* msg);

/**
 * Mark the message so it will be ignored in R_OS_MsgFree()
 * @details msg->mem_id will be set accordingly
 * @param msg the OS message
 */
void R_OS_MsgMarkIgnoreFree(r_os_msg_header_t* msg);


/**
 * Delay execution of OS task
 * @param delayMs Delay in ms for the OS task
 */
void R_OS_DelayTaskMs(uint32_t delayMs);

/**
 * Get task ID of current task
 * @return Task ID
 */
r_os_id_t R_OS_GetTaskId(void);

/**
 * Sleep task
 * @return R_RESULT_SUCCESS, R_RESULT_FAILED
 */
r_os_result_t R_OS_Sleep(void);

/**
 * Sleep task for a maximum number of ticks
 * @return R_RESULT_SUCCESS, R_RESULT_FAILED
 */
r_os_result_t R_OS_SleepMaxTicks(unsigned ticks);

/**
 * Wakeup task
 * @return R_RESULT_SUCCESS, R_RESULT_FAILED
 */
r_os_result_t R_OS_WakeupTask(r_os_id_t tskId);

/**
 * Get task handle for task ID
 * @return Task handle or NULL if tskId is invalid
 */
void* R_OS_GetTaskHandle(r_os_id_t tskId);

/**
 * Start task
 */
void R_OS_StartTask(r_os_id_t tskId);


/**
 * Start cyclic task
 */
void R_OS_StartCyclic(r_os_id_t cycId);

/**
 * Stop cyclic task
 */
void R_OS_StopCyclic(r_os_id_t cycId);


/**
 * Set the bits of the flags
 * @param id the flag ID
 * @param bits the bit pattern
 */
void R_OS_SetFlag(r_os_id_t id, uint16_t bits);

/**
 * Set the bits of the flags (to be called from interrupt context)
 * @param id the flag ID
 * @param bits the bit pattern
 */
void R_OS_SetFlagFromISR(r_os_id_t id, uint16_t bits);

/**
 * Wait for bits of a flag
 * @param id the flag ID
 * @param bits the bit pattern
 * @param clearOnExit clear the specified bits unless timeout occurs
 * @param waitForAll wait for all bits ('and') instead of any ('or')
 * @param timeOutMs timeout in milli seconds
 * @param[out] resultBits the bits set
 * @return R_OS_RESULT_SUCCESS on success, R_OS_RESULT_TIMEOUT on timeout
 */
r_os_result_t R_OS_WaitForFlag(r_os_id_t id, uint16_t bits, int clearOnExit, int waitForAll, uint32_t timeOutMs, uint16_t* resultBits);


/**
 * Increase a semaphore
 * @param id the semaphore ID
 */
void R_OS_PostSemaphore(r_os_id_t id);

/**
 * Wait for a semaphore
 * @param id the semaphore ID
 */
void R_OS_WaitSemaphore(r_os_id_t id);


/**
 * Initialize the OS abstractions defined in r_os_wrapper_config.h
 * @return R_OS_RESULT_SUCCESS on success
 */
r_os_result_t R_OS_Init();

/**
 * Change own Task Priority
 * @param priority the task priority
 */
void R_OS_ChangeOwnTaskPriority(uint16_t priority);


#endif /* R_OS_WRAPPER_H */
