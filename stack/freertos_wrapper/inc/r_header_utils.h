/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2019 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/**
 * @file r_header_utils.h
 * @version 1.00
 * @brief Utilities for header files (mostly preprocessor)
 */

#ifndef R_HEADER_UTILS_H
#define R_HEADER_UTILS_H

#define R_HEADER_UTILS_TOKENPASTE(x, y)  x##y
#define R_HEADER_UTILS_TOKENPASTE2(x, y) R_HEADER_UTILS_TOKENPASTE(x, y)
#define R_HEADER_UTILS_STRINGIFY(x)      #x
#define R_HEADER_UTILS_STRINGIFY2(x)     R_HEADER_UTILS_STRINGIFY(x)


// to be used instead of pragmas for structure packing
#if defined(R_HEADER_UTILS_PRAGMA_PACK_1) && defined(R_HEADER_UTILS_PRAGMA_PACK_DEFAULT)
// use existing
#elif defined(__GNUC__) || defined(_MSC_VER)
#define R_HEADER_UTILS_PRAGMA_PACK_1       _Pragma("pack(1)")
#define R_HEADER_UTILS_PRAGMA_PACK_DEFAULT _Pragma("pack()")
#elif defined(__CCRX__) || defined(__CCRL__)
// Nothing to do as we expect compilation with disabled padding
#define R_HEADER_UTILS_PRAGMA_PACK_1
#define R_HEADER_UTILS_PRAGMA_PACK_DEFAULT
// #define R_HEADER_UTILS_PRAGMA_PACK_1 _Pragma("pack")
// #define R_HEADER_UTILS_PRAGMA_PACK_DEFAULT  _Pragma("unpack")
#elif defined(__ICCRL78__)
#define R_HEADER_UTILS_PRAGMA_PACK_1       _Pragma("pack(1)")
#define R_HEADER_UTILS_PRAGMA_PACK_DEFAULT _Pragma("pack()")
#elif defined(__arm)
#define R_HEADER_UTILS_PRAGMA_PACK_1       _Pragma("pack(1)")
#define R_HEADER_UTILS_PRAGMA_PACK_DEFAULT _Pragma("pack()")
#else  /* if defined(R_HEADER_UTILS_PRAGMA_PACK_1) && defined(R_HEADER_UTILS_PRAGMA_PACK_DEFAULT) */
// these will fail to compile on use with a hopefully semi-helpful error message (#error cannot be used in macros)
#define R_HEADER_UTILS_PRAGMA_PACK_1       the macro R_HEADER_UTILS_PRAGMA_PACK_1 is not supported with this compiler
#define R_HEADER_UTILS_PRAGMA_PACK_DEFAULT the macro R_HEADER_UTILS_PRAGMA_PACK_DEFAULT is not supported with this compiler
#endif /* if defined(R_HEADER_UTILS_PRAGMA_PACK_1) && defined(R_HEADER_UTILS_PRAGMA_PACK_DEFAULT) */


// to be used to make enums as short as possible (use after the enum keyword)
#if defined(R_HEADER_UTILS_ATTR_PACKED)
// use existing
#elif defined(__GNUC__)
#define R_HEADER_UTILS_ATTR_PACKED __attribute__ ((packed))
#elif defined(__CCRX__) || defined(__CCRL__) || defined(__ICCRL78__) || defined(__arm)
// Nothing to do as we expect minimal enums by compiler settings
#define R_HEADER_UTILS_ATTR_PACKED
#else
// these will fail to compile on use with a hopefully semi-helpful error message (#error cannot be used in macros)
#define R_HEADER_UTILS_ATTR_PACKED the macro R_HEADER_UTILS_ATTR_PACKED is not supported with this compiler
#endif


// to be used after a function declaration that includes a printf-like format string and variable arguments
// the index is 1 based (see https://gcc.gnu.org/onlinedocs/gcc-3.2/gcc/Function-Attributes.html)
#if defined(R_HEADER_UTILS_PRINTF_LIKE)
// use existing
#elif defined(__GNUC__)
#define R_HEADER_UTILS_PRINTF_LIKE(string_index, first_to_check) __attribute__ ((format(__printf__, string_index, first_to_check)))
#else
#define R_HEADER_UTILS_PRINTF_LIKE(string_index, first_to_check)
#endif

#endif /* R_HEADER_UTILS_H */
