/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
#ifndef __RM_roa_H
#define __RM_roa_H
/*******************************************************************************
 * file name    : roa.h
 * description  : The header file of OS abstraction module.
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/* --- --- */
#if  defined(__ICCRL78__)
#include <intrinsics.h>
#endif
#include "r_stdint.h"
#include "r_os_wrapper.h"
#include "r_header_utils.h"

#ifdef R_HYBRID_PLC_RF
R_HEADER_UTILS_PRAGMA_PACK_1
#endif

typedef r_os_id_t   RoaID;
typedef r_os_msg_t* RoaMsgT;
typedef struct
{
    r_os_msg_header_t osMsgHeader;
    uint16_t          rm_len;
} RoaMsgHdrT;

/* --- --- */
typedef uint16_t RoaUintT;
typedef uint8_t  RoaErrT;

/* --- --- */
typedef struct
{
    void* first;
    void* last;
} RoaMsgQ;

/* --- --- */

/* --- --- */
#ifndef NULL
#define NULL 0
#endif

/* --- --- */
#ifndef TRUE
#define TRUE  1
#define FALSE 0
#endif

/* --- --- */
#define ROA_SUCCESS           (0)
#define ROA_ERR_TIMEOUT       (1)
#define ROA_INVALID_PARAMETER (2)
#define ROA_ERR_FATAL         (0x80)

/* --- --- */
#define ROA_TMO_POLL          (0)
#define ROA_TMO_FEVR          (0xFFFF)

/* --- --- */
#define ROA_FLG_MSG           (1U) /* reserved */
#define ROA_MIN_EVENT_ID      (1)
#define ROA_MIN_MPL_ID        (1)
#define ROA_INVALID_ID        (0) /* for example: Interrupt Handler(task id) */

/* --- --- */
typedef struct
{
    void* next;
    void* data;
} RoaQEntry;

/* --- --- */
typedef struct
{
    uint8_t    numQBlk;
    RoaQEntry* pQBlk;
    uint8_t    numEvent;
    RoaMsgQ*   pEventMsgQ;
} RoaResourcesT;

/* --- --- */
#define RoaIsEmptyQ(queue) ((queue)->first == NULL)

/* --- --- */
void    RoaInitMsgQ(RoaMsgQ* queue);
void    RoaPutQ(void* pMsg, RoaMsgQ* queue);
void    RoaPutBackQ(void* pMsg, RoaMsgQ* queue);
void*   RoaGetQ(RoaMsgQ* queue);
void    RoaInit(RoaResourcesT* pResources);
void    RoaSetFlg(uint8_t eventid, uint16_t flgptn);
void    RoaISetFlg(uint8_t eventid, uint16_t flgptn);
uint8_t RoaWaiFlg(uint16_t* pFlgptn, uint8_t eventid, uint16_t flgptn, uint16_t tmo);
void*   RoaGetMsg(uint8_t eventid);
void    RoaGetSem(uint8_t semid);
void    RoaReleaseSem(uint8_t semid);

/* --- --- */
void*   RoaAllocBlf(uint8_t mplid, size_t size);
uint8_t RoaRelBlf(void* pMsg);
uint8_t RoaGetTskId(void);
void    RoaSndMsg(void* pMsg);
void    RoaSndMsgFromISR(void* pMsg);

/* --- --- */
#if defined(__i386) || defined(__x86_64)
// for unit tests: do nothing but prevent warnings
#define ROA_ALL_DI(a) do { a = 0; } while (0)
#define ROA_ALL_EI(a) do { (void)a; } while (0)
typedef int     roa_psw_t;

#elif defined(__RL78__)
typedef uint8_t roa_psw_t;

#if defined(__CCRL__)
    #define ROA_ALL_DI(a) a = __get_psw(); __DI()
    #define ROA_ALL_EI(a) __set_psw(a)
#elif  defined(__ICCRL78__)
unsigned char __get_psw(void);
void          __set_psw(unsigned char a);

    #define ROA_ALL_DI(a) { (a) = __get_psw(); __disable_interrupt(); __set_psw((a) & 0xF9u); __enable_interrupt(); }
    #define ROA_ALL_EI(a) { __set_psw(a); }
#endif

/*
    #define ROA_DI()        __asm(" push psw");\
                    __asm(" push ax");\
                    __asm(" mov a, psw");\
                    __asm(" and a, #0f9h");\
                    __asm(" or a,#000h");\
                    __asm(" mov psw,a");\
                    __asm(" pop ax")
    #define ROA_EI()        __asm(" pop psw")
    #define ROA_ALL_EI()            __asm(" pop psw")
    #define ROA_ALL_DI()            __asm(" push psw"); \
                    __asm(" di")
*/
#elif defined(__RX)
typedef uint32_t roa_psw_t;
    #pragma inline_asm _ROA_DI
static void _ROA_DI(void)
{
    int #9
}
    #pragma inline_asm _ROA_IPL
static void _ROA_IPL(void)
{
    int #10
}
    #pragma inline_asm ROA_POP_PSW_INT
static void ROA_POP_PSW_INT(uint32_t a)
{
    int #11
}
    #pragma inline ROA_POP_PSW
static void ROA_POP_PSW(uint32_t* a)
{
    ROA_POP_PSW_INT(*a);
}
    #pragma inline_asm ROA_PUSH_PSW
static void ROA_PUSH_PSW(uint32_t* a)
{
    mvfc psw, r2
         mov.l r2, [r1]
}

/* Push PSW and Set interrupt level to 5, only enable Level 6 or more interrupts */
    #define ROA_DI(a)     ROA_PUSH_PSW(&a); _ROA_IPL()

/* POP PSW */
    #define ROA_EI(a)     ROA_POP_PSW(&a)

/* Push PSW and All interrupts disable */
    #define ROA_ALL_DI(a) ROA_PUSH_PSW(&a); _ROA_DI()

/* POP PSW */
    #define ROA_ALL_EI(a) ROA_POP_PSW(&a)

#elif defined(__arm)
#include "cpx3.h"
typedef uint8_t roa_psw_t;
#define ROA_ALL_DI(a) { __disable_fault_irq(); }
#define ROA_ALL_EI(a) { __enable_fault_irq(); }


#endif /* if defined(__arm) */


/**
 * @return non-zero if currently in interrupt handler
 */
#if defined(__i386) || defined(__x86_64)
static inline int RoaIsInInterrupt(void)
{
    return 0;
}
#elif defined(__RX)
#pragma inline_asm RoaIsInInterrupt
static int RoaIsInInterrupt(void)
{
    // Bit 17: U 0: Interrupt stack pointer (ISP) is selected.
    mvfc psw, r1
    not r1
         and #131072, r1
}
#elif defined(__ICCRL78__)
static inline int RoaIsInInterrupt(void)
{
    return 0;
}
#elif defined(__arm)
static inline int RoaIsInInterrupt(void)
{
    return 0;
}
#else  /* if defined(__i386) || defined(__x86_64) */
#error "Missing implementation of RoaIsInInterrupt for platform"
#endif /* if defined(__i386) || defined(__x86_64) */

#ifdef R_HYBRID_PLC_RF
R_HEADER_UTILS_PRAGMA_PACK_DEFAULT
#endif

/******************************************************************************
 *  Copyright (C) 2014-2016 Renesas Electronics Corporation.
 *****************************************************************************/
#endif // #ifndef __RM_roa_H
