/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_def.h
 * description	: This is the RF driver's define code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
#ifndef _PHY_DEF_H
#define _PHY_DEF_H

/********************************************************************************
 *	Include Files
 *******************************************************************************/
#if defined(__RX)
#include <machine.h>
#endif

/********************************************************************************
 *	Clock Defination for RF
 *******************************************************************************/
/* Define clock source of RF */
#define RP_RFCLK_SOURCE_CRYSTAL		(0)
#define RP_RFCLK_SOURCE_EXTERNAL	(1)

/********************************************************************************
 *	Clock Source Defination for 10% Control
 *******************************************************************************/
/* Define Clock Source Defination for tx limit Control */
#define RP_CLK_LIMIT_TMR_FASTOCO	(0)
#define RP_CLK_LIMIT_TMR_32KHz		(1)

/********************************************************************************
 *	Select Timer Array Unit Channel
 *******************************************************************************/
/* Define Timer Array Unit Channel for tx limit Control */
#define RP_TAU_CH00					(0)		// no support
#define RP_TAU_CH01					(1)
#define RP_TAU_CH02					(2)
#define RP_TAU_CH03					(3)		// no support

/******************************************************************************
Macro definitions
******************************************************************************/
#if R_BORDER_ROUTER_ENABLED
#define RP_RX_BUF_NUM				(6)			// number of receive buffer
#else
#define RP_RX_BUF_NUM				(2)			// number of receive buffer
#endif

#define RP_MAX_TX_ACK_DATA			(32)
#ifdef RP_WISUN_FAN_STACK
	#undef RP_MAX_TX_ACK_DATA
	#define RP_MAX_TX_ACK_DATA		(54)
#endif

#define RP_PHY_BYTE_INT_LIMIT_DELAY	(80)		//

/******************************************************************************
Macro definitions
******************************************************************************/
#if !defined(RP_BIGENDIAN_USE)
#define RP_BIGENDIAN_USE			(0)			// little endian
#endif

#define RP_VAL_ARRAY_TO_UINT16(a)		( ((uint16_t)(*((a)+1)) << 8) + \
                                          ((uint16_t)(*(a))) )
#define RP_VAL_ARRAY_TO_UINT32(a)		( ((uint32_t)(*((a)+3)) << 24) + \
                                          ((uint32_t)(*((a)+2)) << 16) + \
	                                       (uint32_t)RP_VAL_ARRAY_TO_UINT16(a) )

#define RP_VAL_UINT16_TO_ARRAY(val,a)	{ (*(a))     = (uint8_t)(val); \
                                          (*((a)+1)) = (uint8_t)((val) >> 8); }
#define RP_VAL_UINT32_TO_ARRAY(val,a)	{ RP_VAL_UINT16_TO_ARRAY(val,a); \
                                          (*((a)+2)) = (uint8_t)((val) >> 16); \
                                          (*((a)+3)) = (uint8_t)((val) >> 24); }

#if defined(__RL78__)
	#define RP_PHY_INTLEVEL				(1)

#ifdef __CCRL__
	#define RP_PHY_INTLEVEL_EN1			((RP_PHY_INTLEVEL - 1) << 1)
	#define RP_PHY_INTLEVEL_EN2			(RP_PHY_INTLEVEL << 1)	

	// Push PSW and Set interrupt level to 0(RP_IPL_INTP - 1), only enable Level0 interrupt (RP_PHY_INTLEVEL == 1)
	#define RP_PHY_DI(push)     { (push) = __get_psw(); __set_psw(((push)&0xF9u)|RP_PHY_INTLEVEL_EN1); }
	#define RP_PHY_EI(pop)      { __set_psw(pop); }
	// Push PSW and Set interrupt level to 2(RP_IPL_INTP + 1), enable Level0 or Level1 interrupts (RP_PHY_INTLEVEL == 1)
	#define RP_PHY_EI_INV(push) { (push) = __get_psw(); __DI(); __set_psw(((push)&0xF9u)|RP_PHY_INTLEVEL_EN2); __EI(); } 
	#define RP_PHY_DI_INV(pop)  { __set_psw(pop); }
	#define RP_PHY_ALL_DI(push) { (push) = __get_psw(); __DI(); }
	#define RP_PHY_ALL_EI(pop)  { __set_psw(pop); }
#else /* __CCRL__ */
	#if (RP_PHY_INTLEVEL == 1)
	// Push PSW and Set interrupt level to 0(RP_IPL_INTP - 1), only enable Level0 interrupt
	#define RP_PHY_DI()  	__asm(" push psw");\
							__asm(" push ax");\
							__asm(" mov a, psw");\
							__asm(" and a, #0f9h");\
							__asm(" or a,#000h");\
							__asm(" mov psw,a");\
							__asm(" pop ax")
	#else // (RP_PHY_INTLEVEL == 2)
	// Push PSW and Set interrupt level to 2(RP_IPL_INTP + 1), enable Level0 or Level1 interrupts
	#define RP_PHY_DI() 	__asm(" push psw");\
							__asm(" push ax");\
							__asm(" mov a, psw");\
							__asm(" and a, #0f9h");\
							__asm(" or a,#002h");\
							__asm(" mov psw,a");\
							__asm(" pop ax")
	#endif
	// POP PSW
	#define RP_PHY_EI() 	__asm(" pop psw")

	#if (RP_PHY_INTLEVEL == 1)
	// Push PSW and Set interrupt level to 2(RP_IPL_INTP + 1), enable Level0 or Level1 interrupts
	#define RP_PHY_EI_INV()	__asm(" push psw");\
							__asm(" di");\
							__asm(" push ax");\
							__asm(" mov a, psw");\
							__asm(" and a, #0f9h");\
							__asm(" or a,#002h");\
							__asm(" mov psw,a");\
							__asm(" pop ax");\
							__asm(" ei");\
							__asm(" nop")
	#else // (RP_PHY_INTLEVEL == 2)
	// Push PSW and Set interrupt level to 4(RP_IPL_INTP + 3), enable Level0, Level1 or Level2 interrupts
	#define RP_PHY_EI_INV() __asm(" push psw");\
							__asm(" di");\
							__asm(" push ax");\
							__asm(" mov a, psw");\
							__asm(" and a, #0f9h");\
							__asm(" or a,#004h");\
							__asm(" mov psw,a");\
							__asm(" pop ax");\
							__asm(" ei");\
							__asm(" nop")
	#endif
	// POP PSW
	#define RP_PHY_DI_INV()	__asm(" pop psw")

	// Push PSW and All interrupts disable
	#define RP_PHY_ALL_DI() __asm(" push psw"); \
							__asm(" di")
	// POP PSW
	#define RP_PHY_ALL_EI() __asm(" pop psw")
#endif /* __CCRL__ */

#elif defined(__RX) //#if defined(__RL78__) 
	#define RP_PHY_INTLEVEL				(5)

#ifdef R_USE_RENESAS_STACK
	#pragma inline_asm RP_DI
	static void RP_DI(void)
	{
		int #9
	}
	#pragma inline_asm RP_IPL
	static void RP_IPL(void)
	{
		int #10
	}
	#pragma inline_asm RP_IPL_0
	static void RP_IPL_0(void)
	{
		int #12
	}
	#pragma inline_asm RP_POP_PSW_INT
	static void RP_POP_PSW_INT(unsigned long a)
	{
		int #11
	}
	#pragma inline RP_POP_PSW
	static void RP_POP_PSW(unsigned long *a)
	{
		RP_POP_PSW_INT(*a);
	}
	#define RP_IPL_5		RP_IPL

#else // #ifdef R_USE_RENESAS_STACK
	#pragma inline_asm RP_DI
	static void RP_DI(void)
	{
		clrpsw i
	}
	#pragma inline_asm RP_IPL_5
	static void RP_IPL_5(void)
	{
		mvtipl #RP_PHY_INTLEVEL
	}
	#pragma inline_asm RP_IPL_0
	static void RP_IPL_0(void)
	{
		mvtipl #0
	}
	#pragma inline RP_POP_PSW
	static void RP_POP_PSW(unsigned long *a)
	{
		set_psw(*a);
	}
#endif // #ifdef R_USE_RENESAS_STACK

	#pragma inline_asm RP_PUSH_PSW
	static void RP_PUSH_PSW(unsigned long *a)
	{
		mvfc psw, r2
		mov.l r2, [r1]
	}

	// Push PSW and Set interrupt level to 5, only enable Level 6 or more interrupts
	#define RP_PHY_DI(a)		RP_PUSH_PSW(&a);RP_IPL_5()
	// POP PSW
	#define RP_PHY_EI(a)		RP_POP_PSW(&a)
	// Push PSW and Set interrupt level to 0, enable all interrupts
	#define RP_PHY_EI_INV(a)	RP_PUSH_PSW(&a);RP_IPL_0()
	// POP PSW
	#define RP_PHY_DI_INV(a)	RP_POP_PSW(&a)
	// Push PSW and Set interrupt level to 5, only enable interrupts > level 5. Allow interruption-free UART communication.
    #define RP_PHY_ALL_DI(a)	RP_PUSH_PSW(&a);RP_IPL_5()
	// POP PSW
	#define RP_PHY_ALL_EI(a)	RP_POP_PSW(&a)
#elif defined(__arm)
	// Push PSW and Set interrupt level to 5, only enable Level 6 or more interrupts
	extern void RP_PHY_DI(uint8_t pIntEnStatus[]);
	// POP PSW
	extern void RP_PHY_EI(uint8_t pIntEnStatus[]);
	// Push PSW and Set interrupt level to 0, enable all interrupts
	extern void RP_PHY_DI_INV(void);
	// POP PSW
	extern void RP_PHY_EI_INV(void);
	// Push PSW and All interrupts disable
	extern void RP_PHY_ALL_DI(uint8_t pIntEnStatus[]);
	// POP PSW
	extern void RP_PHY_ALL_EI(uint8_t pIntEnStatus[]);
#endif //#if defined(__RL78__)

#if !defined(RP_STATIC)
	#define RP_STATIC	static
#endif

#define RP_PHY_DATA_RATE_BPS_UNIT	(1000)

#define RP_RF_TX_WARMUP_TIME				(350)					// tx warmup time
#define RP_RF_RX_WARMUP_TIME				(240)					// rx warmup time

#define RP_PHY_TX_WARM_UP_TIME	(((uint32_t)(RP_RF_TX_WARMUP_TIME) * RpCb.pib.phyDataRate) / RP_PHY_DATA_RATE_BPS_UNIT)
#define RP_PHY_RX_WARM_UP_TIME	(((uint32_t)(RP_RF_RX_WARMUP_TIME) * RpCb.pib.phyDataRate) / RP_PHY_DATA_RATE_BPS_UNIT)

#define RP_PHY_FLINT_DELAY_TIME				(40)

#define	RP_SPECIFIC_NORMAL					(0x00)
#define RP_SPECIFIC_WSUN					(0x01)

#define RP_SFD_LEN_2						(2)
#define RP_SFD_LEN_4						(4)
#define RP_PHR_LEN							(2)

#define RP_PHY_BANK_0						(0)
#define RP_PHY_BANK_1						(1)

#define RP_PHY_ANTENNA_UNUSE				(0xFF)
#define RP_PHY_ANTENNA_0					(0)
#define RP_PHY_ANTENNA_1					(1)

#define RP_PHY_ADDRFILTER_1					(0)

#define RP_PHY_RXMODE_0_ON_CSMACA			(0)
#define RP_PHY_RXMODE_1_ON_CSMACA			(1)

#define RP_PHY_CHANNELS_SUPPORTED_INDEX_0	(0)
#define RP_PHY_CHANNELS_SUPPORTED_INDEX_1	(1)
#define RP_PHY_CHANNELS_SUPPORTED_INDEX_2	(2)
#define RP_PHY_CHANNELS_SUPPORTED_INDEX_3	(3)

#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_0 (0)
#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_1 (1)
#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_2 (2)
#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_3 (3)
#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_4 (4)
#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_5 (5)
#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_6 (6)
#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_7 (7)
#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_8 (8)
#define RP_PHY_TABLE_INDEX_CHANNELS_SUPPORTED_9 (9)

#define RP_MAXNUM_TXLIMIT_MODE 				(3)
#define RP_MAXNUM_TXPOWER					(105)
#define RP_MAXNUM_TXPOWER_JPN				(RP_MAXNUM_TXPOWER - 3)
#define RP_MAXNUM_TXPOWER_EU				(RP_MAXNUM_TXPOWER - 2)
#define RP_MAXNUM_TXPOWER_KOREA				(RP_MAXNUM_TXPOWER - 2)
#define RP_MAXNUM_TXPOWER_US				(RP_MAXNUM_TXPOWER)
#define RP_MAXNUM_920MHz_CHANNEL			(38)
#define RP_MAXVAL_CSMABACKOFF				(5)
#define RP_MAXVAL_MINBE						(8)
#define RP_MAXVAL_CW						(3)
#define RP_MINVAL_MAXBE						(3)
#ifdef RP_WISUN_FAN_STACK
	#undef RP_MINVAL_MAXBE
	#define RP_MINVAL_MAXBE					(0)
#endif

#define RP_MAXVAL_MAXBE						(8)
#define RP_MAXVAL_FRAME_RETRIES				(7)
#define RP_MINVAL_CCAVTH					(0x0100)
#define RP_MAXVAL_CCAVTH					(0x01FF)
#define RP_MINVAL_CCADURATION				(1)
#define RP_MAXVAL_CCADURATION				(2000)
#define RP_MINVAL_PREAMBLELEN				(4)
#define RP_MAXVAL_PREAMBLELEN				(1000)
#define RP_MAXVAL_FRAMELEN_ARIB				(2500)
#define RP_SFDVAL_MD0_2FSKNOFEC				(0x72097209)
#define RP_SFDVAL_MD0_2FSKFEC				(0x72F672F6)
#define RP_SFDVAL_MD1_2FSKNOFEC				(0x705E705E)
#define RP_SFDVAL_MD1_2FSKFEC				(0xB4C6B4C6)
#define RP_SFDVAL_EXT_PREAMBLE				(0xAAAA)
#define RP_SFDVAL_MD0_4FSKNOFEC				(0xBFAEAAEB)
#define RP_SFDVAL_MD0_4FSKFEC				(0xBFAEFFBE)
#define RP_SFDVAL_MD1_4FSKNOFEC				(0xBFAABBFE)
#define RP_SFDVAL_MD1_4FSKFEC				(0xEFBAFABE)
#define RP_MAXVAL_TXLIMIT_MODE				(2)
#define RP_ACKTX_WARMUP_TIME				(RP_PHY_TX_WARM_UP_TIME)
#define RP_MINVAL_ACKREPLY_TIME				(RP_PHY_TX_WARM_UP_TIME + 2)
#define RP_MINVAL_ACKWAIT_DURATION			(1)
#define RP_MINVAL_BACKOFF_PERIOD			(15)
#define RP_DEFAULT_AGC_WAIT_GAIN			(68)
#define RP_MAXVAL_CCA_ED_BANDWIDTH			(3)
#define RP_MAXVAL_AGC_WAIT_GAIN_OFFSET		(31)
#define RP_MAXVAL_CCA_VTH_OFFSET			(31)
#define RP_MAXVAL_RSSI_OUTPUT_OFFSET		(31)
#define RP_MINVAL_ANTSWENA_TIMING			(0x0001)
#define RP_MAXVAL_ANTSWENA_TIMING			(0x0154)
#define RP_MAXVAL_RMODE_TON_MAX				(0x2710)
#define RP_MAXVAL_RMODE_TOFF_MIN			(0x03E8)
#define RP_MINVAL_RMODE_TCUM_SMP_PERIOD		(0x0001)
#define RP_MAXVAL_RMODE_TCUM_SMP_PERIOD		(0x0444)

#define RP_RF_PIB_DFLT_SUPPORT_CH_M1_H			(0x0000003f)	// Supported channel: SubGHz band #1 (0x0000003fffffffff)
#define RP_RF_PIB_DFLT_SUPPORT_CH_M1_L			(0xffffffff)
#define RP_RF_PIB_DFLT_SUPPORT_CH_M2_H			(0x0000001f)	// Supported channel: SubGHz band #2 (0x0000001ffffffeff)
#define RP_RF_PIB_DFLT_SUPPORT_CH_M2_L			(0xfffffeff)
#define RP_RF_PIB_DFLT_SUPPORT_CH_M3_H			(0x0000000f)	// Supported channel: SubGHz band #3 (0x0000000ffffffe7f)
#define RP_RF_PIB_DFLT_SUPPORT_CH_M3_L			(0xfffffe7f)
#define RP_RF_PIB_DFLT_SUPPORT_CH_M4_H			(0x0000000f)	// Supported channel: SubGHz band #4 (0x0000000ffffffe7f)
#define RP_RF_PIB_DFLT_SUPPORT_CH_M4_L			(0xfffffe7f)

#define RP_RF_PIB_DFLT_SUPPORT_CH_H	RP_RF_PIB_DFLT_SUPPORT_CH_M2_H	// PIB default supported channel: SubGHz band #2 (0x0000001ffffffeff)
#define RP_RF_PIB_DFLT_SUPPORT_CH_L	RP_RF_PIB_DFLT_SUPPORT_CH_M2_L	// 
#define RP_RF_PIB_DFLT_CURRENT_CHANNEL			(9)			// PIB default current channel: 0x09 for 922.5MHz(JPN 100kbps)
#define RP_RF_PIB_DFLT_CURRENT_PAGE				(0x09)		// PIB default current page: 0x09 for 920MHz FSK, 0x00 fixed for 2.4GHz
#define RP_RF_PIB_DFLT_CHANNEL_SUPPORTED_INDEX	(0x00)		// PIB default channel supported index
#define RP_RF_PIB_DFLT_ADDRESS_FILTER1_ENA		RP_FALSE	// PIB default Address Filter1 Ebaable: 0
#define RP_RF_PIB_DFLT_PANID1					(0xffff)	// PIB default PANID: 0xffff
#define RP_RF_PIB_DFLT_SHORTAD1					(0xffff)	// PIB default Short Address: 0xffff
#define RP_RF_PIB_DFLT_PANCOORD1				RP_FALSE	// PIB default Pan Coordinator; 0
#define RP_RF_PIB_DFLT_PENDBIT1					RP_FALSE	// PIB default Pending Bit; 0
#define RP_RF_PIB_DFLT_ADDRESS_FILTER2_ENA		RP_FALSE	// PIB default Address Filter2 Ebaable: 0
#define RP_RF_PIB_DFLT_PANID2					(0xffff)	// PIB default PANID2: 0xffff
#define RP_RF_PIB_DFLT_SHORTAD2					(0xffff)	// PIB default Short Address2: 0xffff
#define RP_RF_PIB_DFLT_PANCOORD2				RP_FALSE	// PIB default Pan Coordinator2; 0
#define RP_RF_PIB_DFLT_PENDBIT2					RP_FALSE	// PIB default Pending Bit2; 0
#define RP_RF_PIB_DFLT_MAXCSMABO				(0x04)		// PIB default macMaxCSMABackoff: 4
#define RP_RF_PIB_DFLT_MINBE					(0x03)		// PIB default macMinBE: 3
#define RP_RF_PIB_DFLT_MAXBE					(0x05)		// PIB default macMaxBE: 5
#define RP_RF_PIB_DFLT_MAX_FRAME_RETRIES		(0x03)		// PIB default frame retry: 3
#define RP_RF_PIB_DFLT_CRCERROR_UPMSG			RP_FALSE	// PIB default whether indicate CRCError Frame or not
#define RP_RF_PIB_DFLT_BACKOFF_SEED				(0x55)		// default backoffseed
#define RP_RF_PIB_DFLT_CCA_DURATION				(36)		// PIB default CCA duration 36symbol
#define RP_RF_PIB_DFLT_FSK_FEC_TX_ENA			RP_FALSE	// PIB default FEC Tx disable
#define RP_RF_PIB_DFLT_FSK_FEC_RX_ENA			RP_FALSE	// PIB default FEC Rx disable
#define RP_RF_PIB_DFLT_FSK_FEC_SCHEME			RP_FALSE	// PIB default FEC mode
#define RP_RF_PIB_DFLT_FSK_PREAMBLE_LENGTH		(15)		// PIB default FSK preamble length
#define RP_RF_PIB_DFLT_MRFSK_SFD				(0)			// PIB default MR FSK FSD pattern 0
#define RP_RF_PIB_DFLT_FSK_SCRAMBLE_PSDU		RP_TRUE		// PIB default Data whitening
#define RP_RF_PIB_DFLT_FSK_OPE_MODE RP_PHY_FSK_OPEMODE_2	// PIB default FSK operation mode
#define RP_RF_PIB_DFLT_FCS_LENGTH				(0x02)		// PIB default FCS Length: 2 or 4
#define RP_RF_PIB_DFLT_ACK_RESPONSE_TIME		(60)		// PIB default Tx Ack Response(symbol)
#define RP_RF_PIB_DFLT_ACK_WAIT_DURATION		(500)		// PIB default Tx Ack Wait Duration(symbol)
#define RP_RF_PIB_DFLT_ANTENNA_SELECT_ACKTX		(0x00)		// PIB default AckTx Antenna Select
#define RP_RF_PIB_DFLT_ANTENNA_SELECT_ACKRX		(0x00)		// PIB default AckRx Antenna Select
#define RP_RF_PIB_DFLT_RXTIMEOUT_MODE			RP_TRUE		// PIB default Rx Timeout Mode enable
#define RP_RF_PIB_DFLT_FREQ_BAND_ID RP_PHY_FREQ_BAND_920MHz	// PIB default FREQ band ID
#define RP_RF_PIB_DFLT_REGULATORY_MODE			(0)			// PIB default Regulatory Mode(Tx Time Limit Mode)
#define RP_RF_PIB_DFLT_LVL_VTH					(0x017E)	// PIB default Level Filter Vth
#define RP_RF_PIB_DFLT_BACKOFF_PERIOD			(113)		// PIB default backoff period
#ifdef RP_WISUN_FAN_STACK
	#undef RP_RF_PIB_DFLT_BACKOFF_PERIOD
	#define RP_RF_PIB_DFLT_BACKOFF_PERIOD		(37)
#endif

#define RP_RF_PIB_DFLT_PREAMBLE_4BYTE_RX_MODE	RP_FALSE	// PIB default preamble 4byte rx mode
#define RP_RF_PIB_DFLT_AGC_START_VTH			(0x0000)	// PIB default AGC start threshold
#define RP_RF_PIB_DFLT_CCA_BANDWIDTH			(0)			// PIB default CCA Bandwidth
#define RP_RF_PIB_DFLT_ED_BANDWIDTH				(0)			// PIB default ED Bandwidth
#define RP_RF_PIB_DFLT_ANTENNA_DIVERSITY_START_VTH	(0x0000)// PIB default Antenna Diversity Start Threshold
#define RP_RF_PIB_DFLT_ANTENNA_SWITCHING_TIME	(0x0000)	// PIB default Antenna Switching Time
#define RP_RF_PIB_DFLT_SFD_DETECTION_EXTEND		(0x00)		// PIB default SFD Detection Extend
#define RP_RF_PIB_DFLT_AGC_WAIT_GAIN_OFFSET		(0x00)		// PIB default AGC wait gain
#define RP_RF_PIB_DFLT_CCA_VTH_OFFSET			(0x00)		// PIB default CCA Vth Offset
#define RP_RF_PIB_DFLT_ANTENNA_SWITCH_ENA_TIMING	(0x012C)// PIB default Antenna Switch Enable Timing
#define RP_RF_PIB_DFLT_GPIO0_SETTING			(0xFF)		// PIB default GPIO0 Setting
#define RP_RF_PIB_DFLT_GPIO3_SETTING			(0xFF)		// PIB default GPIO3 Setting
#define RP_RF_PIB_DFLT_RMODE_TON_MAX			(0x0000)	// PIB default Regulatory Mode Ton Max
#define RP_RF_PIB_DFLT_RMODE_TOFF_MIN			(0x03E8)	// PIB default Regulatory Mode Toff Min
#define RP_RF_PIB_DFLT_RMODE_TCUM_SMP_PERIOD	(0x003C)	// PIB default Regulatory Mode Sampling Period to measure Tcum
#define RP_RF_PIB_DFLT_RMODE_TCUM_LIMIT			(0x00000000)// PIB default Regulatory Mode Limit of Tcum
#define RP_RF_PIB_DFLT_RMODE_TCUM				(0x00000000)// PIB default Regulatory Mode Tcum (Total Tx time)
#define RP_RF_PIB_DFLT_RSSI_OUTPUT_OFFSET		(0x00)		// PIB default RSSI Output Offset
#ifdef R_FREQUENCY_OFFSET_ENABLED
	#define RP_RF_HW_DFLT_FREQUENCY				(922500000)	// H/W efault Frequency
	#define RP_RF_PIB_DFLT_FREQUENCY_OFFSET		(0x00000000)// PIB default Frequency Offset
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
#define RP_RF_HW_DFLT_ADDRESS_FILTER1_ENA		RP_FALSE	// H/W default Address Filter1 Ebaable: 0
#define RP_RF_HW_DFLT_PANID1					(0xffff)	// H/W default PANID: 0xffff
#define RP_RF_HW_DFLT_SHORTAD1					(0xffff)	// H/W default Short Address: 0xffff
#define RP_RF_HW_DFLT_PANCOORD1					RP_FALSE	// H/W default Pan Coordinator; 0
#define RP_RF_HW_DFLT_PENDBIT1					RP_FALSE	// H/W default Pending Bit; 0
#define RP_RF_HW_DFLT_ADDRESS_FILTER2_ENA		RP_FALSE	// H/W default Address Filter2 Ebaable: 0
#define RP_RF_HW_DFLT_PANID2					(0xffff)	// H/W default PANID2: 0xffff
#define RP_RF_HW_DFLT_SHORTAD2					(0xffff)	// H/W default Short Address2: 0xffff
#define RP_RF_HW_DFLT_PANCOORD2					RP_FALSE	// H/W default Pan Coordinator2; 0
#define RP_RF_HW_DFLT_PENDBIT2					RP_FALSE	// H/W default Pending Bit2; 0
#define RP_RF_HW_DFLT_MAXCSMABO					(0x00)		// H/W default macMaxCSMABackoff: 0
#define RP_RF_HW_DFLT_MINBE						(0x03)		// H/W default macMinBE: 3
#define RP_RF_HW_DFLT_MAXBE						(0x05)		// H/W default macMaxBE: 5
#define RP_RF_HW_DFLT_MAX_FRAME_RETRIES			(0x03)		// H/W default frame retry: 3
#define RP_RF_HW_DFLT_CCA_DURATION				(13)		// H/W default CCA duration 13symbol
#define RP_RF_HW_DFLT_FSK_FEC_TX_ENA			RP_FALSE	// H/W default FEC Tx disable
#define RP_RF_HW_DFLT_FSK_FEC_RX_ENA			RP_FALSE	// H/W default FEC Rx disable
#define RP_RF_HW_DFLT_FSK_FEC_SCHEME			RP_FALSE	// H/W default FEC mode
#define RP_RF_HW_DFLT_FSK_PREAMBLE_LENGTH		(4)			// H/W default FSK preamble length
#define RP_RF_HW_DFLT_MRFSK_SFD					(0)			// H/W default MR FSK FSD pattern 0
#define RP_RF_HW_DFLT_FSK_SCRAMBLE_PSDU			RP_TRUE		// H/W default Data whitening
#define RP_RF_HW_DFLT_FSK_OPE_MODE 	RP_PHY_FSK_OPEMODE_2	// H/W default FSK operation mode
#define RP_RF_HW_DFLT_FCS_LENGTH				(0x02)		// H/W default FCS Length: 2 or 4
#define RP_RF_HW_DFLT_ACK_RESPONSE_TIME			(2)			// H/W default Tx Ack Response(symbol)
#define RP_RF_HW_DFLT_ACK_WAIT_DURATION			(500)		// H/W default Tx Ack Wait Duration(symbol)
#define RP_RF_HW_DFLT_ANTENNA_SELECT_ACKTX		(0x00)		// H/W default AckTx Antenna Select
#define RP_RF_HW_DFLT_ANTENNA_SELECT_ACKRX		(0x00)		// H/W default AckRx Antenna Select
#define RP_RF_HW_DFLT_RXTIMEOUT_MODE			RP_TRUE		// H/W default Rx Timeout Mode enable
#define RP_RF_HW_DFLT_FREQ_BAND_ID RP_PHY_FREQ_BAND_920MHz	// H/W default FREQ band ID
#define RP_RF_HW_DFLT_RXMODE_ON_CSMACSA			RP_FALSE	// H/W default RxMode On CSMA-CA
#define RP_RF_HW_DFLT_LVL_VTH					(0x0100)	// H/W default Level Filter Vth
#define RP_RF_HW_DFLT_BACKOFF_PERIOD			(113)		// H/W default backoff period

#define RP_RF_HW_DFLT_BBTIMECON 			(0x00)
#define RP_RF_HW_DFLT_BBTXRXMODE0 			(0x00)
#define RP_RF_HW_DFLT_BBTXRXMODE1 			(0xC0)
#define RP_RF_HW_DFLT_BBTXRXMODE2 			(0x30)
#define RP_RF_HW_DFLT_BBTXRXMODE3 			(0x00)
#define RP_RF_HW_DFLT_BBTXRXMODE4 			(0x01)
#define RP_RF_HW_DFLT_BBCSMACON0			(0x00)
#define RP_RF_HW_DFLT_BBCSMACON1			(0x20)
#define RP_RF_HW_DFLT_BBCSMACON2			(0x15)
#define RP_RF_HW_DFLT_BBGPIODATA			(0x00)
#define RP_RF_HW_DFLT_BBFECCON				(0x00)
#define RP_RF_HW_DFLT_BBSUBGCON				(0x0C)
#define RP_RF_HW_DFLT_BBTXCON0				(0xF4)
#define RP_RF_HW_DFLT_BBTXCON2				(0x92)
#define RP_RF_HW_DFLT_BB19ER2				(0xC8)
#define RP_RF_HW_DFLT_BB25ER2				(0x1F)
#define RP_RF_HW_DFLT_BB1AER2				(0xF0)

#define RP_TIME_MASK						(0xffffffff)
#define RP_TIME_LIMIT						(0x80000000)

#define RP_RXOFF_MERGIN_TIME				(6)

#define	RP_RETX_TIMER_WASTE_TIME 			(2)
#define	RP_RXTMOUT_TIMER_WASTE_TIME 		(3)

#define RP_BB_TX_RAM_SIZE					(128)
#define RP_BB_RX_RAM_SIZE					(128)

// RpCb.status
#define RP_PHY_STAT_RX						(0x0001)
#define RP_PHY_STAT_TX						(0x0002)
#define RP_PHY_STAT_CCA						(0x0004)
#define RP_PHY_STAT_ED						(0x0008)
#define RP_PHY_STAT_TRX_OFF					(0x0010)
// --- no overrupping status above this line
#define RP_PHY_STAT_BUSY					(0x0020)		// on going some processess
#define RP_PHY_STAT_WAIT_TMRTRG				(0x0040)		// waiting timer trigger
#define RP_PHY_STAT_TX_CCA					(0x0080)		// TX with CCA
#define RP_PHY_STAT_RXON_BACKOFF			(0x0100)		// RX at Backoffperiod on CSMA-CA
#define RP_PHY_STAT_TRX_ACK					(0x0200)		// TX with ACK receiving or RX with auto ACK reply
#define RP_PHY_STAT_RX_TMOUT				(0x0400)		// RX with timeout
#define RP_PHY_STAT_SLOTTED					(0x0800)		// slotted
#define RP_PHY_STAT_TRX_TO_RX_AUTO			(0x1000)		// Auto receiving when finishing Tx or Rx
#define RP_PHY_STAT_LOWPOWER				(0x2000)		// Lowpower Mode
#define RP_PHY_STAT_TRX_CONTINUOUS			(0x4000)		// Continuous Mode for TELEC
#define RP_PHY_STAT_TX_UNDERFLOW			(0x8000)		// Underflow
#define RP_PHY_STAT_ANTENNA_SELECT			(0x4000)		// Antenna selection assistance

// RpCb.statusRxTimeout
#define RP_STATUS_RX_TIMEOUT_INIT			(0x00)
#define RP_STATUS_RX_TIMEOUT_TX				(0x01)
#define RP_STATUS_RX_TIMEOUT_EXTENTION		(0x02)

// RpCb.PIB.fcs_length
#define RP_FCS_LENGTH_16BIT					(2)
#define RP_FCS_LENGTH_32BIT					(4)

// RpCb.PIB.crcerror_upmsg
#define RP_RX_CRCERROR_IGNORE				(0)
#define RP_RX_CRCERROR_UPMSG				(1)

// RpCb.PIB.phyFskFecRxEna
#define RP_FEC_RX_MODE_DISABLE				(0)
#define RP_FEC_RX_MODE_ENABLE				(1)
#define RP_FEC_RX_MODE_AUTO_DISTINCT		(2)

// RpCb.PIB.phyAntennaSelectAckTx
#define RP_ANT_SEL_ACK_TX_ANT0				(0)
#define RP_ANT_SEL_ACK_TX_ANT1				(1)
#define RP_ANT_SEL_ACK_TX_RCVD_ANT			(2)
#define RP_ANT_SEL_ACK_TX_RCVD_INV_ANT		(3)

// RpCb.PIB.phyAntennaSelectAckRx
#define RP_ANT_SEL_ACK_RX_ANT0				(0)
#define RP_ANT_SEL_ACK_RX_ANT1				(1)
#define RP_ANT_SEL_ACK_RX_AUTO				(2)

// RpCb.PIB.phyRegulatoryMode
#define RP_RMODE_DISABLE					(0x00)
#define RP_RMODE_ENABLE						(0x01)
#define RP_RMODE_ENABLE_ARIB				(0x02)

// bbrfcon
#define RP_RF_POWEROFF						(0x00)

#define RP_TMOUT_CNT_WHEN_SET_TRX_STATE_OFF			(5000)		// 5 msec
#define RP_TMOUT_CNT_WHEN_SET_TRX_STATE_OFF_FOR_TX	(2000000)	// 2 sec

// _phy_cb.rx.cnt
#define RP_PHY_RX_STAT_INIT					(-2)	// initial status, no error
#define RP_PHY_RX_STAT_PHR_DETECT			(0)		// phr detect

// _phy_cb.rx.len
#ifdef R_FSB_FAN_ENABLED
	#define RP_PHY_RX_ADDRESS_DECODE_LEN	(120)	//128
#else
	#define RP_PHY_RX_ADDRESS_DECODE_LEN		(21)
	#ifdef RP_WISUN_FAN_STACK
		#undef RP_PHY_RX_ADDRESS_DECODE_LEN
		#define RP_PHY_RX_ADDRESS_DECODE_LEN	(27)	// 21 + 6 (AUX security header)
	#endif
#endif // #ifdef R_FSB_FAN_ENABLED

// _phy_cb.PIB.fsk_ope_mode
#define RP_PHY_FSK_OPEMODE_1				(0x01)
#define RP_PHY_FSK_OPEMODE_2				(0x02)
#define RP_PHY_FSK_OPEMODE_3				(0x03)
#define RP_PHY_FSK_OPEMODE_4				(0x04)
#define RP_PHY_FSK_OPEMODE_5				(0x05)
#define RP_PHY_FSK_OPEMODE_6				(0x06)
#define RP_PHY_FSK_OPEMODE_7				(0x07)

// _phy_cb.PIB.freq_band_id
#define RP_PHY_FREQ_BAND_169MHz				(0)		// 169.400 - 169.475 (Europe)
#define RP_PHY_FREQ_BAND_450MHz				(1)		// 450 - 470 (US FCC Part 22/90)
#define RP_PHY_FREQ_BAND_470MHz				(2)		// 470 - 510 (Chaina)
#define RP_PHY_FREQ_BAND_780MHz				(3)		// 779 - 787 (Chaina)
#define RP_PHY_FREQ_BAND_863MHz				(4)		// 863 - 870 (Europe)
#define RP_PHY_FREQ_BAND_896MHz				(5)		// 896 - 901 (US FCC Part 90)
#define RP_PHY_FREQ_BAND_901MHz				(6)		// 901 - 902 (US FCC Part 24)
#define RP_PHY_FREQ_BAND_915MHz				(7)		// 902 - 928 (US)
#define RP_PHY_FREQ_BAND_917MHz				(8)		// 917 - 923.5 (Korea)
#define RP_PHY_FREQ_BAND_920MHz				(9)		// 920 - 928 (Japan)
#define RP_PHY_FREQ_BAND_928MHz				(10)	// 928 - 960 (US non-contiguous)
#define RP_PHY_FREQ_BAND_950MHz				(11)	// 950 - 958 (Japan)
#define RP_PHY_FREQ_BAND_1427MHz			(12)	// 1427 - 1518 (US and Canada non-contiguous)
#define RP_PHY_FREQ_BAND_2450MHz			(13)	// 2400 - 2483.5
#define RP_PHY_FREQ_BAND_920MHz_Others		(14)	// 920 - 928 (Japan) other settings
#define RP_PHY_FREQ_BAND_870MHz				(15)	// 870 - xxx (Europe2)
#define RP_PHY_FREQ_BAND_902MHz				(16)	// 902 - xxx (PH/MY/AZ)
#define RP_PHY_FREQ_BAND_921MHz				(17)	// 921 - xxx (CH)
#define RP_PHY_FREQ_BAND_MAX				(18)	// 

// datarate setting
#define RP_PHY_010K_M05						(0)
#define RP_PHY_020K_M05						(1)
#define RP_PHY_040K_M05						(2)
#define RP_PHY_050K_M05						(3)
#define RP_PHY_050K_M10						(4)
#define RP_PHY_050K_M10_FP					(5)
#define RP_PHY_050K_M10_CH					(6)
#define RP_PHY_100K_M05						(7)
#define RP_PHY_100K_M05_CH					(8)
#define RP_PHY_100K_M10_JP					(9)
#define RP_PHY_100K_M10_IN					(10)
#define RP_PHY_150K_M05_IN					(11)
#define RP_PHY_150K_M05_EU					(12)
#define RP_PHY_150K_M05_US					(13)
#define RP_PHY_150K_M05_JP					(14)
#define RP_PHY_200K_M05						(15)
#define RP_PHY_200K_M10						(16)
#define RP_PHY_300K_M05						(17)
#define RP_PHY_200K_M03						(18)
#define RP_PHY_400K_M03						(19)

// Tx interval time
#define RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE		(9)
#define RP_PHY_TX_INTERVAL_TIME_LOW_CHANNEL		(50000UL)		// 50msec
#define RP_PHY_SENT_FSK1_LONG_FRAME_UPPER_LIMIT	(400000UL)		// 400msec
#define RP_PHY_SENT_FSK1_LONG_FRAME_LOWER_LIMIT	(200000UL)		// 200msec
#define RP_PHY_TX_INTERVAL_LONG_FRAME_10TIMES 	(101UL)			// 10.1 times
#define RP_PHY_SENT_FSK1_MIDDLE_FRAME_UPPER_LIMIT	(200000UL)	// 200msec
#define RP_PHY_SENT_FSK1_MIDDLE_FRAME_LOWER_LIMIT	(6000UL)	// 6msec
#define RP_PHY_SENT_FSK2_LONG_FRAME_UPPER_LIMIT	(200000UL)		// 200msec
#define RP_PHY_SENT_FSK2_LONG_FRAME_LOWER_LIMIT	(3000UL)		// 3msec
#define RP_PHY_SENT_FSK3_LONG_FRAME_UPPER_LIMIT	(100000UL)		// 100msec
#define RP_PHY_SENT_FSK3_LONG_FRAME_LOWER_LIMIT	(2000UL)		// 2msec
#define RP_PHY_TX_INTERVAL_TIME_SHORT_FRAME		(2000UL)		// 2msec

// ACK Respons Limit Time for ARIB
#define RP_ACK_RESPONSE_SINGLE_CH_LIMIT			(50)			// 50msec
#define RP_ACK_RESPONSE_MULTIPLE_CH_LIMIT		(5)				// 5msec

//Tx interval margin time
#define RP_PHY_INTERVAL_CLEAR_MARGIN			(10UL)		//10sec(Arib Max Interval 4sec + 6sec margin)

// baseband registers
#define	BBRFCON								(0x0000 << 3)
#define RFSTART								(0x01)
#define ANARESETB							(0x02)
#define CSONSET								(0x04)
#define	BBTXRXRST							(0x0001 << 3)
#define RFSTOP								(0x01)
#define	BBTXRXMODE0							(0x0002 << 3)
#define CCATYPE0							(0x01)
#define AUTOACKEN							(0x04)
#define AUTORCV0							(0x08)
#define AUTORCV1							(0x10)
#define BATLIFEEXT							(0x20)
#define BEACON								(0x40)
#define INTRAPANEN							(0x80)
#define	BBTXRXMODE1							(0x0003 << 3)
#define CCASEL								(0x01)
#define ACKFVEN								(0x08)
#define ACKFV0								(0x10)
#define ACKFV1								(0x20)
#define SQCNUMSUPEN							(0x40)
#define ACKRCVPOINT							(0x80)
#define	BBRSSICCARSLT						(0x0004 << 3)
#define	BBRSSICCARSLT_H						(0x0005 << 3)
#define	BBENHACKMODE						(0x0006 << 3)
#define ENHACKMODEBIT						(0x01)
#define	BBTXRXST0							(0x0007 << 3)
#define CCA									(0x01)
#if defined(__RX)
#undef  CRC
#endif
#define CRC									(0x02)
#define CSMACA								(0x04)
#define TRNRCVSQC							(0x08)
#define RCVRAMBANK0							(0x10)
#define RCVRAMBANK1							(0x20)
#define RCVPEND								(0x40)
#define RCVRAMST							(0x80)
#define	BBTXRXMODE2							(0x0009 << 3)
#define NOCRC								(0x01)
#define FLMPEND								(0x02)
#define FLMPENDST							(0x04)
#define ENHACKEN							(0x08)
#define RETRN0								(0x10)
#define RETRN1								(0x20)
#define RETRN2								(0x40)
#define RETRN3								(0x80)
#define RETRN								(RETRN0 | RETRN1 | RETRN2 | RETRN3)
#define	BBTXRXMODE3							(0x000A << 3)
#define ADRSFILEN							(0x01)
#define PANCORD								(0x02)
#define RXEN								(0x04)
#define LVLFILEN							(0x08)
#define RCVSTOREBANKSEL						(0x10)
#define RCVOVERWREN							(0x20)
#define ADFEXTEN							(0x40)
#define ADFGENMODE							(0x80)
#define	BBTXRXST1							(0x000B << 3)
#define RCVSTOREST							(0x02)
#define	BBTXRXCON							(0x000C << 3)
#define RCVTRG								(0x01)
#define TRNTRG								(0x02)
#define CCATRG								(0x04)
#define ACKRCVEN							(0x08)
#define TRNWITHACKTRG						(TRNTRG | ACKRCVEN)
#define	BBCSMACON0							(0x000D << 3)
#define CSMAST								(0x01)
#define CSMATRNST							(0x02)
#define CSMARCVEN							(0x04)
#define UNICASTFRM							(0x10)
#define	BBCCAVTH							(0x000E << 3)
#define	BBCCAVTH_H							(0x000F << 3)
#define	BBTXRXST2							(0x0010 << 3)
#define RCVRAMBANKFLG						(0x01)
#define RCVSTOREFLG							(0x02)
#define	BBTXRXMODE4							(0x0011 << 3)
#define CCAINTSEL							(0x01)
#define INTOUTSEL							(0x10)
#define TIMEOUTRCV							(0x40)
#define TIMEOUTINTSEL						(0x80)
#define	BBCSMACON1							(0x0012 << 3)
#define NB0									(0x01)
#define NB1									(0x02)
#define NB2									(0x04)
#define NB									(NB0 | NB1 | NB2)
#define CW0									(0x10)
#define CW1									(0x20)
#define CW									(CW0 | CW1)
#define	BBCSMACON2							(0x0013 << 3)
#define BEMAX0								(0x01)
#define BEMAX1								(0x02)
#define BEMAX2								(0x04)
#define BEMAX3								(0x08)
#define BEMAX								(BEMAX0 | BEMAX1 | BEMAX2 | BEMAX3)
#define MACMINBECON							(0x10)
#define UNICASTFRMEN						(0x20)
#define	BBPANID0							(0x0014 << 3)
#define	BBPANID0_H							(0x0015 << 3)
#define	BBSHORTAD0							(0x0016 << 3)
#define	BBSHORTAD0_H						(0x0017 << 3)
#define	BBEXTENDAD00						(0x0018 << 3)
#define	BBEXTENDAD00_H						(0x0019 << 3)
#define	BBEXTENDAD01						(0x001A << 3)
#define	BBEXTENDAD01_H						(0x001B << 3)
#define	BBEXTENDAD02						(0x001C << 3)
#define	BBEXTENDAD02_H						(0x001D << 3)
#define	BBEXTENDAD03						(0x001E << 3)
#define	BBEXTENDAD03_H						(0x001F << 3)
#define	BBTIMEREAD0							(0x0020 << 3)
#define	BBTIMEREAD0_H						(0x0021 << 3)
#define	BBTIMEREAD1							(0x0022 << 3)
#define	BBTIMEREAD1_H						(0x0023 << 3)
#define	BBTCOMP0REG0						(0x0024 << 3)
#define	BBTCOMP0REG0_H						(0x0025 << 3)
#define	BBTCOMP0REG1						(0x0026 << 3)
#define	BBTCOMP0REG1_H						(0x0027 << 3)
#define	BBTCOMP1REG0						(0x0028 << 3)
#define	BBTCOMP1REG0_H						(0x0029 << 3)
#define	BBTCOMP1REG1						(0x002A << 3)
#define	BBTCOMP1REG1_H						(0x002B << 3)
#define	BBTCOMP2REG0						(0x002C << 3)
#define	BBTCOMP2REG0_H						(0x002D << 3)
#define	BBTCOMP2REG1						(0x002E << 3)
#define	BBTCOMP2REG1_H						(0x002F << 3)
#define	BBTSTAMP0							(0x0030 << 3)
#define	BBTSTAMP0_H							(0x0031 << 3)
#define	BBTSTAMP1							(0x0032 << 3)
#define	BBTSTAMP1_H							(0x0033 << 3)
#define	BBTIMECON							(0x0034 << 3)
#define TIMEEN								(0x01)
#define COMP0TRG							(0x02)
#define STAMPTIMSEL							(0x04)
#define COMP0TRGSEL							(0x08)
#define STAMPPRDSEL0						(0x10)
#define STAMPPRDSEL1						(0x20)
#define STAMPPRDSEL							(STAMPPRDSEL0 | STAMPPRDSEL1)
#define CNTSRCSEL							(0x40)
#define	BBBOFFPROD							(0x0035 << 3)
#define BOFFPRODDIS							(0x00)
#define BOFFPRODEN							(0x01)
#define	BBINTREQ0							(0x0036 << 3)
#define TIM0INTREQ							(0x01)
#define TIM1INTREQ							(0x02)
#define TIM2INTREQ							(0x04)
#define TRNFININTREQ						(0x08)
#define TRN0INTREQ							(0x10)
#define TRN1INTREQ							(0x20)
#define CALINTREQ							(0x40)
#define CCAINTREQ							(0x80)
#define	BBINTREQ1							(0x0037 << 3)
#define RCVFININTREQ						(0x01)
#define RCV0INTREQ							(0x02)
#define RCV1INTREQ							(0x04)
#define RCVSTINTREQ							(0x08)
#define ADRSINTREQ							(0x10)
#define ROVRINTREQ							(0x20)
#define MODESWINTREQ						(0x40)
#define LVLFILINTREQ						(0x80)
#define	BBINTREQ2							(0x0038 << 3)
#define RCVCUNTINTREQ						(0x10)
#define FLINTREQ							(0x20)
#define BYTERCVINTREQ						(0x40)
#define	BBINTEN0							(0x0039 << 3)
#define TIM0INTEN							(0x01)
#define TIM1INTEN							(0x02)
#define TIM2INTEN							(0x04)
#define TRNFININTEN							(0x08)
#define TRN0INTEN							(0x10)
#define TRN1INTEN							(0x20)
#define CALINTEN							(0x40)
#define CCAINTEN							(0x80)
#define	BBINTEN1							(0x003A << 3)
#define RCVFININTEN							(0x01)
#define RCV0INTEN							(0x02)
#define RCV1INTEN							(0x04)
#define RCVSTINTEN							(0x08)
#define ADRSINTEN							(0x10)
#define ROVRINTEN							(0x20)
#define MODESWINTEN							(0x40)
#define LVLFILINTEN							(0x80)
#define	BBINTEN2							(0x003B << 3)
#define RCVCUNTINTEN						(0x10)
#define FLINTEN								(0x20)
#define BYTERCVINTEN						(0x40)
#define	BBCSMACON3							(0x003E << 3)
#define BEMIN0								(0x01)
#define BEMIN1								(0x02)
#define BEMIN2								(0x04)
#define BEMIN3								(0x08)
#define BEMIN								(BEMIN0 | BEMIN1 | BEMIN2 | BEMIN3)
#define	BBCAL								(0x003F << 3)
#define CALSTART							(0x01)
#define BBACKCOMP0							(0x0040 << 3)
#define BBACKCOMP0_H						(0x0041 << 3)
#define BBACKCOMP1							(0x0042 << 3)
#define BBACKCOMP1_H						(0x0043 << 3)
#define BBLVLVTH							(0x0044 << 3)
#define BBLVLVTH_H							(0x0045 << 3)
#define BBACKRTNTIM							(0x0046 << 3)
#define BBACKRTNTIM_H						(0x0047 << 3)
#define BBAUTORCVCNT						(0x0048 << 3)
#define BBAUTORCVCNT_H						(0x0049 << 3)
#define BBBOFFPERIOD						(0x004A << 3)
#define BBBOFFPERIOD_H						(0x004B << 3)
#define BBCSMAENDCOUNT						(0x004C << 3)
#define BBCSMAENDCOUNT_H					(0x004D << 3)
#define BBCSMASTACOUNT						(0x004E << 3)
#define BBCSMASTACOUNT_H					(0x004F << 3)
#define BBCOMSTATE							(0x0066 << 3)
#define RCVSTATE							(0x01)
#define TRNSTATE							(0x02)
#define CCASTATE							(0x04)
#define FRCVSTATE							(0x08)
#define BBCOMSTATE2							(0x0067 << 3)
#define ACKSTATE							(0x01)
#define BBEVAREG							(0x0068 << 3)
#define CONTTX								(0x01)
#define NOMOD								(0x02)
#define CONTRX								(0x04)
#define NOSPECTX							(0x08)
#define NOSPECRX							(0x10)
#define BBBOFFPROD2							(0x0069 << 3)
#define BBCOMSTATE3							(0x006F << 3)
#define RETRNRD0							(0x01)
#define RETRNRD1							(0x02)
#define RETRNRD2							(0x04)
#define RETRNRD3							(0x08)
#define RETRNRD								(RETRNRD0 | RETRNRD1 | RETRNRD2 | RETRNRD3)
#define CCARD0								(0x10)
#define CCARD1								(0x20)
#define CCARD2								(0x40)
#define CCARD								(CCARD0 | CCARD1 | CCARD2)
#define BBACKRCVWIT							(0x0070 << 3)
#define BBACKRCVWIT_H						(0x0071 << 3)
#define BBRETRNWUP							(0x0072 << 3)
#define BBRETRNWUP_H						(0x0073 << 3)
#define BBANTSWTIMG							(0x007A << 3)
#define BBANTSWTIMG_H						(0x007B << 3)
#define BBANTSWCON							(0x0080 << 3)
#define ANTSWEN								(0x01)
#define BBCLKOUTCON							(0x0081 << 3)
#define CLKOUTEN							(0x01)
#define CLKOUTSEL0							(0x02)
#define CLKOUTSEL1							(0x04)
#define CLKOUTSEL							(CLKOUTSEL0 | CLKOUTSEL1)
#define BBGPIODIR							(0x0082 << 3)
#define GPIO0DIR							(0x01)
#define GPIO1DIR							(0x02)
#define GPIO2DIR							(0x04)
#define GPIO3DIR							(0x08)
#define GPIO4DIR							(0x10)
#define GPIOXDIR							(GPIO0DIR | GPIO1DIR | GPIO2DIR | GPIO3DIR | GPIO4DIR)
#define BBGPIODATA							(0x0083 << 3)
#define GPIO0DATA							(0x01)
#define GPIO1DATA							(0x02)
#define GPIO2DATA							(0x04)
#define GPIO3DATA							(0x08)
#define GPIO4DATA							(0x10)
#define GPIOXDATA							(GPIO0DATA | GPIO1DATA | GPIO2DATA | GPIO3DATA | GPIO4DATA)
#define BBIDLECON2							(0x008E << 3)
#define BBTXCON0							(0x0090 << 3)
#define TXDDCVAR0							(0x01)
#define TXDDCVAR1							(0x02)
#define TXDDCVAR							(TXDDCVAR0 | TXDDCVAR1)
#define TXDDCRES0							(0x04)
#define TXDDCRES1							(0x08)
#define TXDDCRES							(TXDDCRES0 | TXDDCRES1)
#define TXDDCDRV0							(0x10)
#define TXDDCDRV1							(0x20)
#define TXDDCDRV2							(0x40)
#define TXDDCDRV3							(0x80)
#define TXDDCDRV							(TXDDCDRV0 | TXDDCDRV1 | TXDDCDRV2 | TXDDCDRV3)
#define BBTXCON2							(0x0092 << 3)
#define TXLDOTRXVAR0						(0x01)
#define TXLDOTRXVAR1						(0x02)
#define TXLDOTRXVAR2						(0x04)
#define TXLDOTRXVAR3						(0x08)
#define TXLDOTRXVAR4						(0x10)
#define TXLDOTRXVAR							(TXLDOTRXVAR0 | TXLDOTRXVAR1 | TXLDOTRXVAR2 | TXLDOTRXVAR3 | TXLDOTRXVAR4)
#define TXLDOOSCVAR0						(0x20)
#define TXLDOOSCVAR1						(0x40)
#define TXLDOOSCVAR2						(0x80)
#define TXLDOOSCVAR							(TXLDOOSCVAR0 | TXLDOOSCVAR1 | TXLDOOSCVAR2)
#define BBRXFLEN							(0x00A0 << 3)
#define BBRXFLEN_H							(0x00A1 << 3)
#define BBRXCOUNT							(0x00A2 << 3)
#define BBRXCOUNT_H							(0x00A3 << 3)
#define BBTXFLEN							(0x00A4 << 3)
#define BBTXFLEN_H							(0x00A5 << 3)
#define BBPAMBL								(0x00A6 << 3)
#define BBPAMBL_H							(0x00A7 << 3)
#define BBFREQ								(0x00A8 << 3)
#define BBFREQ_1							(0x00A9 << 3)
#define BBFREQ_2							(0x00AA << 3)
#define BBFREQ_3							(0x00AB << 3)
#define BBSYMBLRATE							(0x00AC << 3)
#define BBSYMBLRATE_1						(0x00AD << 3)
#define BBSYMBLRATE_2						(0x00AE << 3)
#define BBSUBGCON							(0x00B0 << 3)
#define FECENRX								(0x01)
#define FECMODE								(0x02)
#define CRCBIT								(0x04)
#define DWEN								(0x08)
#define PHRTX1								(0x10)
#define PHRTX2								(0x20)
#define INTERLEAVEEN						(0x40)
#define BBMODSET							(0x00B1 << 3)
#define MODSET0								(0x01)
#define MODSET1								(0x02)
#define MODINDEX0							(0x40)
#define MODINDEX1							(0x80)
#define MODINDEX							(MODINDEX0 | MODINDEX1)
#define BBCCATIME							(0x00B2 << 3)
#define BBCCATIME_H							(0x00B3 << 3)
#define BBANTDIV							(0x00B4 << 3)
#define ANTDIVEN							(0x01)
#define ANTSWTRNSET							(0x02)
#define ANTACKRTNDIS						(0x04)
#define ANTACKRCVDIS						(0x08)
#define ANTCOUNT0							(0x10)
#define ANTCOUNT1							(0x20)
#define ANTCOUNT2							(0x40)
#define ANTCOUNT							(ANTCOUNT0 | ANTCOUNT1 | ANTCOUNT2)
#define ANTST								(0x80)
#define BBANTDIVCON							(0x00B5 << 3)
#define BBTXMODESW							(0x00B8 << 3)
#define MODESW								(0x01)
#define BBTXMODESW_H						(0x00B9 << 3)
#define BBRXMODESW							(0x00BA << 3)
#define BBRXMODESW_H						(0x00BB << 3)
#define BBTXCOUNT							(0x00BC << 3)
#define BBTXCOUNT_H							(0x00BD << 3)
#define BBPHRRX								(0x00BE << 3)
#define PHRRX0								(0x01)
#define PHRRX1								(0x02)
#define PHRRX2								(0x04)
#define FCSTYPE								(0x08)
#define DWBIT								(0x10)
#define BBPABL								(0x00C0 << 3)
#define BBPABL_H							(0x00C1 << 3)
#define BBSFD								(0x00C2 << 3)
#define BBSFD_1								(0x00C3 << 3)
#define BBSFD_2								(0x00C4 << 3)
#define BBSFD_3								(0x00C5 << 3)
#define BBSHRCON							(0x00C6 << 3)
#define PABLBYTE							(0x01)
#define SFDBYTE0							(0x02)
#define SFDBYTE1							(0x04)
#define SFDBYTE								(SFDBYTE0 | SFDBYTE1)
#define BBANT0RD							(0x00C8 << 3)
#define BBANT0RD_H							(0x00C9 << 3)
#define BBANT1RD							(0x00CA << 3)
#define BBANT1RD_H							(0x00CB << 3)
#define BBANTDIVTIM							(0x00CE << 3)
#define BBANTDIVTIM_H						(0x00CF << 3)
#define BBANTTIMOUT							(0x00D0 << 3)
#define BBANTTIMOUT_H						(0x00D1 << 3)
#define BBRCVINTCOMP						(0x00D2 << 3)
#define BBRCVINTCOMP_H						(0x00D3 << 3)
#define BBBOPTOTAL							(0x00D4 << 3)
#define BBBOPTOTAL_H						(0x00D5 << 3)
#define BBCCATOTAL							(0x00D6 << 3)
#define RFINI00								(0x00D8 << 3)
#define RFINI01								(0x00D9 << 3)
#define RFINI02								(0x00DA << 3)
#define RFINI10								(0x00DC << 3)
#define RFINI11								(0x00DD << 3)
#define RFINI12								(0x00DE << 3)
#define BBPANID1							(0x00E0 << 3)
#define BBPANID1_H							(0x00E1 << 3)
#define BBSHORTAD1							(0x00E2 << 3)
#define BBSHORTAD1_H						(0x00E3 << 3)
#define BBEXTENDAD10						(0x00E4 << 3)
#define BBEXTENDAD10_H						(0x00E5 << 3)
#define BBEXTENDAD11						(0x00E6 << 3)
#define BBEXTENDAD11_H						(0x00E7 << 3)
#define BBEXTENDAD12						(0x00E8 << 3)
#define BBEXTENDAD12_H						(0x00E9 << 3)
#define BBEXTENDAD13						(0x00EA << 3)
#define BBEXTENDAD13_H						(0x00EB << 3)
#define BBTIMEOUT							(0x00EC << 3)
#define BBTIMEOUT_H							(0x00ED << 3)
#define BBSFD2								(0x0100 << 3)
#define BBSFD2_1							(0x0101 << 3)
#define BBSFD2_2							(0x0102 << 3)
#define BBSFD2_3							(0x0103 << 3)
#define BBSFD3								(0x0104 << 3)
#define BBSFD3_1							(0x0105 << 3)
#define BBSFD3_2							(0x0106 << 3)
#define BBSFD3_3							(0x0107 << 3)
#define BBSFD4								(0x0108 << 3)
#define BBSFD4_1							(0x0109 << 3)
#define BBSFD4_2							(0x010A << 3)
#define BBSFD4_3							(0x010B << 3)
#define BBFECCON							(0x010C << 3)
#define FECAUTOEN							(0x01)
#define FECENTX								(0x02)
#define FECCONACKRTN						(0x04)
#define FECENAACKRTN						(0x08)
#define FECCONACKRCV						(0x10)
#define FECENAACKRCV						(0x20)
#define MRFSKSFD							(0x40)
#define BBADFCON							(0x010D << 3)
#define PANCORD2							(0x01)
#define FLMPEND2							(0x02)
#define ADFMONI1							(0x04)
#define ADFMONI2							(0x08)
#define BBANTDIV2							(0x010E << 3)
#define ANTACKRTNSET						(0x01)
#define ANTACKRCVSET						(0x02)
#define ANTMONI								(0x04)
#define ACKRTNRVS							(0x08)
#define BBANTDIVVTH							(0x0110 << 3)
#define BBANTDIVVTH_H						(0x0111 << 3)
#define BBWS2REV							(0x0112 << 3)
#define TIMORCVSEL							(0x01)
#define MSBITRDSEL							(0x02)

// baseband ram
#define BBTXRAM0							(0x0200 << 3)
#define BBTXRAM1							(0x0280 << 3)
#define	BBRXRAM0							(0x0300 << 3)
#define BBRXRAM1							(0x0380 << 3)

#define RP_PHY_STAT_TX_BUSY()		(RpRegRead(BBCOMSTATE) & TRNSTATE)									// on Transmiting Frame

#ifdef RP_WISUN_FAN_STACK
	#define RP_PHY_STAT_ONACKREPLY()	((RP_PHY_STAT_TX_BUSY()) || (RpCb.rx.onReplyingAck))
#else
	#define RP_PHY_STAT_ONACKREPLY()	(RP_PHY_STAT_TX_BUSY())
#endif

#define RP_PHY_STAT_ONFRAMERCV()	(RpRegRead(BBCOMSTATE) & FRCVSTATE)
#define RP_PHY_STAT_RX_BUSY()		(RP_PHY_STAT_ONFRAMERCV() || RP_PHY_STAT_ONACKREPLY())	// on Receiving Frame including on Ack Reply
#define RP_PHY_STAT_CCA_BUSY()		(RpRegRead(BBCOMSTATE) & CCASTATE)									// on CCA
#define RP_PHY_STAT_IDLE()			(RP_PHY_STAT_RX_BUSY() == 0 && RP_PHY_STAT_TX_BUSY() == 0 && RP_PHY_STAT_CCA_BUSY() == 0) // Idle

// indirect function call
#define	INDIRECT_RpPdDataIndCallback	RpCb.callback.pDataIndCallback
#define	INDIRECT_RpPdDataCfmCallback	RpCb.callback.pDataCfmCallback
#define	INDIRECT_RpPlmeCcaCfmCallback	RpCb.callback.pCcaCfmCallback
#define	INDIRECT_RpPlmeEdCfmCallback	RpCb.callback.pEdCfmCallback
#define	INDIRECT_RpRxOffIndCallback		RpCb.callback.pRxOffIndCallback
#define	INDIRECT_WarningIndCallback		RpCb.callback.pWarningIndCallback
#define	INDIRECT_FatalErrorIndCallback	RpCb.callback.pFatalErrorIndCallback
#define	INDIRECT_RpCalcLqiCallback		RpCb.callback.pCalcLqiCallback
#define	INDIRECT_RpAckCheckCallback		RpCb.callback.pAckCheckCallback

// BBINTREQ0
#define RP_BB_NO_IREQ		0x00000000
#define RP_BBTIM0_IREQ		0x00000001
#define RP_BBTIM1_IREQ		0x00000002
#define RP_BBTIM2_IREQ		0x00000004
#define RP_BBTRNFIN_IREQ	0x00000008
#define RP_BBTRN0_IREQ		0x00000010
#define RP_BBTRN1_IREQ		0x00000020
#define RP_BBCAL_IREQ		0x00000040
#define RP_BBCCA_IREQ		0x00000080
// BBINTREQ1
#define RP_BBRCVFIN_IREQ	0x00000100
#define RP_BBRCV0_IREQ		0x00000200
#define RP_BBRCV1_IREQ		0x00000400
#define RP_BBRCVST_IREQ		0x00000800
#define RP_BBADRS_IREQ		0x00001000
#define RP_BBROVR_IREQ		0x00002000
#define RP_BBMODESW_IREQ	0x00004000
#define RP_BBRCVLVL_IREQ	0x00008000
// BBINTREQ2
#define RP_BBAES_IREQ		0x00010000
#define RP_BBRNG_IREQ		0x00020000
#define RP_BBRNGINI_IREQ	0x00040000
#define RP_BBPLL_IREQ		0x00080000
#define RP_BBRCVCUNT_IREQ	0x00100000
#define RP_BBFL_IREQ		0x00200000
#define RP_BBBYTE_IREQ		0x00400000
#define RP_BBMODEVA_IREQ	0x00800000

#define RP_SYMBOL_OFFSET_L			0	// 0xAC
#define RP_SYMBOL_OFFSET_H			1	// 0xAD
#define RP_MODESET_OFFSET			2	// 0xB1
#define RP_PABL_OFFSET				3	// 0xC0
#define RP_SHRCON_OFFSET			4	// 0xC6
#define RP_0x00CC_OFFSET			5
#define RP_CCA_ANTDVT_ON			6	// 0xCE(ANTDV ON)
#define RP_CCA_ANTDVT_ON_ARIB		7	// 0xCE(ANTDV ON for ARIB)
#define RP_ANTDVT_TIMEOUT			8	// 0xD0
#define RP_ANTDVT_TIMEOUT_H			9	// 0xD1
#define RP_ANTDVT_TIMEOUT_ARIB		10	// 0xD0(for ARIB)
#define RP_ANTDVT_TIMEOUT_ARIB_H	11	// 0xD1(for ARIB)
#define RP_TXCPISL1_OFFSET			12	// 0xF1
#define RP_TXCPISL2_OFFSET			13	// 0xF2
#define	RP_TXCR0_OFFSET				14	// 0xF3
#define	RP_TXCR1_OFFSET				15	// 0xF4
#define RP_ANTDIVVTH_OFFSET			16	// 0x110
#define RP_0x0403_OFFSET			17
#define RP_0x0405_OFFSET			18
#define RP_0x0415_OFFSET			19
#define RP_0x0423_OFFSET			20
#define RP_0x042D_OFFSET			21
#define RP_0x042D_ANDVON_OFFSET		22
#define RP_0x042D_DIGICCA_OFFSET	23
#define RP_0x0430_OFFSET			24
#define RP_0x0430_DIGICCA_OFFSET	25
#define RP_0x0432_OFFSET			26
#define RP_0x0432_DIGICCA_OFFSET	27
#define RP_0x0436_OFFSET			28
#define RP_0x0436_DIGICCA_OFFSET	29
#define RP_0x043A_OFFSET			30
#define RP_0x043A_DIGICCA_OFFSET	31
#define RP_0x0454_OFFSET			32
#define RP_0x0456_OFFSET			33
#define RP_0x0456_DIGICCA_OFFSET	34
#define RP_0x0457_F_OFFSET			35
#define RP_0x0457_OFFSET			36
#define RP_0x0458_F_OFFSET			37
#define RP_0x0458_OFFSET			38
#define RP_0x0471_OFFSET			39
#define RP_0x0473_OFFSET			40
#define RP_0x0474_OFFSET			41
#define RP_0x047C_OFFSET			42
#define RP_0x047D_OFFSET			43
#define RP_0x047E_OFFSET			44
#define RP_0x047F_OFFSET			45
#define RP_0x0480_OFFSET			46
#define RP_0x0486_F_OFFSET			47
#define RP_0x0486_OFFSET			48
#define RP_0x0486_DIGICCA_OFFSET	49
#define RP_0x0487_F_OFFSET			50
#define RP_0x0487_OFFSET			51
#define RP_0x0487_DIGICCA_OFFSET	52
#define RP_0x0488_OFFSET			53
#define RP_0x0488_DIGICCA_OFFSET	54
#define RP_0x048D_OFFSET			55
#define RP_0x048F_OFFSET			56
#define RP_0x0493_OFFSET			57
#define RP_0x0494_OFFSET			58
#define RP_0x04D9_OFFSET			59
#define RP_0x04F4_OFFSET			60
#define RP_0x04F4_DIGICCA_OFFSET	61
#define RP_0x04F6_OFFSET			62
#define RP_0x04F8_OFFSET			63
#define RP_0x0505_OFFSET			64
#define RP_0x0505_DIGICCA_OFFSET	65
#define RP_0x050F_F_OFFSET			66
#define RP_0x050F_OFFSET			67
#define RP_0x0580_OFFSET			68
#define RP_0x0580_DIGICCA_OFFSET	69
#define RP_0x0581_OFFSET			70
#define RP_0x0582_OFFSET			71
#define RP_0x058F_F_OFFSET			72
#define RP_0x058F_OFFSET			73
#define RP_0x059F_OFFSET			74
#define RP_0x05A0_F_OFFSET			75
#define RP_0x05A0_OFFSET			76
#define RP_0x05A1_OFFSET			77
#define RP_0x05A2_F_OFFSET			78
#define RP_0x05A2_OFFSET			79
#define RP_MAXNUM_OFFSET			80

// CCA ED Bandwidth Index
#define RP_PHY_CCA_BANDWIDTH_INDEX_225K		(0)
#define RP_PHY_CCA_BANDWIDTH_INDEX_200K		(1)
#define RP_PHY_CCA_BANDWIDTH_INDEX_150K		(2)
#define RP_PHY_CCA_BANDWIDTH_INDEX_NARROW	(3)

#define RP_PHY_REG488H_DEFAULT	(0x82)

#define RP_PHY_ANTDV_DATARATE	(100)
#define RP_PHY_ANTDV_TIMERVALUE	(290)

/******************************************************************************
Strucutres, Variable Data Base
******************************************************************************/
// PIB Structure
typedef struct
{
	uint8_t		phyCurrentChannel;
	uint8_t		phyTransmitPower;
	uint32_t	phyChannelsSupported[2];
	uint8_t		phyChannelsSupportedPage;
	uint8_t		phyFskFecTxEna;
	uint8_t		phyFskFecRxEna;
	uint8_t		phyFskFecScheme;
	uint16_t 	phyCcaVth;
	uint16_t 	phyLvlFltrVth;
	uint16_t 	refVthVal;
	uint8_t		phyCrcErrorUpMsg;
	uint8_t		phyBackOffSeed;
	uint16_t 	phyRssiLoss;
	uint8_t		macAddressFilter1Ena;
	uint16_t 	macShortAddr1;
	uint16_t 	macPanId1;
	uint32_t 	macExtendedAddress1[2];
	uint8_t 	macPanCoord1;
	uint8_t 	macPendBit1;
	uint8_t 	macAddressFilter2Ena;
	uint16_t 	macShortAddr2;
	uint16_t 	macPanId2;
	uint32_t 	macExtendedAddress2[2];
	uint8_t 	macPanCoord2;
	uint8_t 	macPendBit2;
	uint8_t 	macMaxCsmaBackOff;
	uint8_t 	macMinBe;
	uint8_t 	macMaxBe;
	uint8_t 	macMaxFrameRetries;
	uint16_t	phyCcaDuration;
	uint16_t	phyFskPreambleLength;
	uint8_t		phyMrFskSfd;
	uint8_t		phyFskScramblePsdu;
	uint8_t		phyFskOpeMode;
	uint8_t		phyFcsLength;
	uint16_t	phyAckReplyTime;
	uint16_t	phyAckWaitDuration;
	uint8_t		phyProfileSpecificMode;
	uint8_t		phyAntennaSwitchEna;
	uint8_t		phyAntennaDiversityRxEna;
	uint8_t		phyAntennaSelectTx;
	uint8_t		phyAntennaSelectAckTx;
	uint8_t		phyAntennaSelectAckRx;
	uint8_t		phyRxTimeoutMode;
	uint16_t	phyDataRate;
	uint8_t		phyFreqBandId;
	uint8_t		phyRegulatoryMode;
	uint32_t	phyFrequency;
	uint16_t	phyCsmaBackoffPeriod;
	uint8_t		phyPreamble4ByteRxMode;
	uint16_t	phyAgcStartVth;
	uint8_t		phyCcaBandwidth;
	uint8_t		phyEdBandwidth;
	uint16_t	phyAntennaDiversityStartVth;
	uint16_t	phyAntennaSwitchingTime;
	uint8_t		phySfdDetectionExtend;
	uint8_t		phyAgcWaitGainOffset;
	uint8_t		phyCcaVthOffset;
	uint16_t	phyAntennaSwitchEnaTiming;
	uint8_t		phyGpio0Setting;
	uint8_t		phyGpio3Setting;
	uint16_t	phyRmodeTonMax;
	uint16_t	phyRmodeToffMin;
	uint16_t	phyRmodeTcumSmpPeriod;
	uint32_t	phyRmodeTcumLimit;
	uint32_t	phyRmodeTcum;
	uint8_t		phyAckWithCca;
	uint8_t 	backup_macMaxCsmaBackOff;
	uint8_t 	backup_macMinBe;
	uint16_t	backup_phyCsmaBackoffPeriod;
	uint16_t	backup_phyCcaDuration;
	uint8_t		phyRssiOutputOffset;
#ifdef R_FREQUENCY_OFFSET_ENABLED
	int32_t		phyFrequencyOffset;
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
} RP_PHY_PIB;

// RF Errors
typedef struct
{
	uint32_t	rxRamOverrun;
	uint32_t	txRamUnderrun;
	uint32_t	failToGetRxBuf;
} RP_PHY_ERROR;

// Transmitting
typedef struct
{
	int16_t		len;			// length	(total size)
	int16_t		cnt;			// counter	(transmitted size)
	uint8_t		*pTopData;		// top of trasmitted data
	uint8_t		data[RP_aMaxPHYPacketSize];
	uint8_t		ackData[RP_MAX_TX_ACK_DATA];
	uint8_t		rtrnTimes;		// retry times
	uint8_t		ccaTimes;		// cca times
	uint8_t		*pNextData;		// next data to be transmitted
	uint32_t	time;			// starting time of TX or CSMA-CA warmup
	uint16_t	sentLen;		// sent length recently
	uint32_t	sentTime;		// sent time (frame_end) recently
	uint8_t		onCsmaCa;		// now on Csma-Ca
	uint8_t		ccaTimesOneFrame;
	uint8_t		needTxWaitRegulatoryMode; // wait flag for pause duration in ARIB Std or ToffMin in Regulatory Mode
	uint32_t	sentElapsedTimeS;		 // sent time (frame_end) recently expressed in RpCb.tx.regulatoryModeElapsedTimeS(sec)
	uint32_t	regulatoryModeElapsedTimeS;
	uint32_t	ackCompLimitTimeBit;	 // ACK transmission completion limit time for ARIB
	uint16_t	ackLenForRegulatoryMode; // Ack length to apply the limits for Regulatory Mode
	uint8_t     applyAckTotalTxTime;     // Flag for applying total Tx time limit
	uint8_t     applyAckToffMin;         // Flag for applying ToffMin limit
#ifdef R_FSB_FAN_ENABLED
	uint8_t		isFsbDownlink;  // flag for FSB Downlink
#endif // #ifdef R_FSB_FAN_ENABLED
} RP_PHY_TX_CB;

// Receiving
typedef struct
{
	uint16_t	len;			// length   (total size)
	uint16_t	lenNoFcs;		// length   (total size exclude fcs)
	int16_t		cnt;			// counter  (received size)
	uint8_t		*pTopRxBuf;		// top of receive buffer
	uint32_t	timeStamp;		// RX timestamp (top of PSDU)
	uint32_t	timeAcKRtn;		// Ack Return Time
	uint8_t		lqi;			// RX LQI
	uint16_t	rssi;			// RX RSSI
	uint8_t		selectedAntenna;// Selected Antenna
	uint16_t	rssi0;			// RSSI0
	uint16_t	rssi1;			// RSSI1
	uint8_t		fcsLength;		// FCS length
	uint8_t		fcs32bit;		// 32bit FCS
	uint8_t		datawhite;		// Data Whitening
	uint8_t		filteredAdress;	// Filtered  Address Filter
	uint32_t	timeout;		// RX timeout time
	uint8_t		onReplyingAck;	// on replying Ack
	uint8_t		softwareAdf;	// software Adress filter
	uint8_t		waitRelRxBufWhenAutoRx;	// waiting Release Rx Buffer when Auto Rx
	uint8_t		unreadRamPrev;
	uint8_t		rcvRamSt;
	uint8_t		bkupAckAnt;
	uint8_t		phrRx;
	uint16_t	antdvTimerValue;
} RP_PHY_RX_CB;

// Callbacks
typedef struct
{
	RpPdDataIndCallbackT		pDataIndCallback;
	RpPdDataCfmCallbackT		pDataCfmCallback;
	RpPlmeCcaCfmCallbackT		pCcaCfmCallback;
	RpPlmeEdCfmCallbackT		pEdCfmCallback;
	RpRxOffIndCallbackT			pRxOffIndCallback;
	RpFatalErrorIndCallbackT	pFatalErrorIndCallback;
	RpWarningIndCallbackT		pWarningIndCallback;
	RpCalcLqiCallbackT			pCalcLqiCallback;
	RpAckCheckCallbackT			pAckCheckCallback;
} RP_PHY_CALLBACK_CB;

// Transmit Limitation Manegement
typedef struct
{
	uint32_t	msecCnt;						// 0~3600000
	uint32_t	secCnt;							// 0~1092 (Temporarily exceeds 65535 during processing.)
	uint8_t		samplingCnt;					// 0~RP_MAX_NUM_TX_LMT_BUF-1
	uint32_t	txLimitTime;					// [mS]
	uint32_t	lackTime;
} RP_TXLMT_TMR_CB;

// RF register value
typedef struct
{
	uint8_t		bbIntEn[3];			// BBINTEN
	uint32_t 	bbIntReq; 			// BBINTREQ
	uint8_t		bbTimeCon;			// BBTIMECON
	uint8_t		bbTxRxMode0;		// BBTXRXMODE0
	uint8_t		bbTxRxMode1;		// BBTXRXMODE1
	uint8_t		bbTxRxMode2;		// BBTXRXMODE2
	uint8_t		bbTxRxMode3;		// BBTXRXMODE3
	uint8_t		bbTxRxMode4;		// BBTXRXMODE4
	uint8_t		bbCsmaCon0;			// BBCSMACON0
	uint8_t		bbCsmaCon1;			// BBCSMACON1
	uint8_t		bbCsmaCon2;			// BBCSMACON2
	uint8_t		bbGpioDir;			// BBGPIODIR
	uint8_t		bbGpioData;			// BBGPIODATA
	uint8_t		bbFecCon;			// BBFECCON
	uint8_t		bbSubCon;			// BBSUBCON
	uint8_t		bbTxCon0;			// BBTXCON0
	uint8_t		bbTxCon2;			// BBTXCON2
	uint8_t		bb19Er2;			// Evareg 7h 19
	uint8_t 	bb25Er2;			// Evareg 7h 25
	uint8_t 	bb1AEr2;			// Evareg 7h 1A
} RP_REG_VALUE_CB;

typedef struct
{
	uint8_t		writeIn;
	uint16_t	txAddress0;
	uint16_t	txAddress1;
	uint16_t	txValue0;
	uint16_t	txValue1;
	uint16_t	rxAddress0;
	uint16_t	rxAddress1;
	uint8_t		rxFecAuto;
	uint8_t		enableFec;
} RP_SFD_EXTEND;

// PHY Structure
typedef struct
{
	volatile uint16_t	status;			// status
	volatile uint8_t	statusRxTimeout;// status for RX Timeout
	RP_PHY_TX_CB		tx;				// TX control
	RP_PHY_RX_CB		rx;				// RX control
	RP_PHY_PIB			pib;			// PHY PIB
	RP_PHY_CALLBACK_CB	callback;		// Callbacks
	RP_TXLMT_TMR_CB		txLmtTimer;		// TX total limited timer
	uint8_t				freqIdIndex;	// freqId table Index
	uint8_t				prohibitLowPower;	// prohibit low power mode
	RP_REG_VALUE_CB		reg;			// register value
	RP_SFD_EXTEND		sfdExtend;
} RP_PHY_CB;

// Receiving Buffers
typedef struct
{
	uint8_t		status;
	uint8_t		buf[RP_aMaxPHYPacketSize];
} RP_RX_BUF;

// RF driver initial value
typedef struct
{
	uint8_t		txPowerDefault;			// PIB phyTransmitPower inital value
	uint16_t 	ccaVthDefault;			// PIB phyCcaVth initial value
	uint8_t		profileSpecificModeDefault;// PIB phyProfileSpecificMode inital value 0:IEEE802.15.4 1:Wi-SUN 2:U-BUS Air
	uint8_t 	antSwEnaDefault;		// PIB phyAntennaSwitchEna inital value 0:Antenna Switch Disable 1:Anntenna Switch(GPIO4) Enable
	uint8_t		antDvrEnaDefault;		// PIB phyAntennaDiversityRxEna inital value 0:Antenna Diversity Disable 1:Antenna Diversity Enable(GPIO1:ANTSELOUT0, GPIO2:ANTSELOUT)
	int8_t		rssiLoss;				// RSSI Loss diff dBm
	uint8_t		rfClkSource;			// clock source for RF
	uint8_t		tmrClkSelect;			// clock Source for 10% control
	uint8_t		selectTAUchannel;		// Select Timer Array Unit Channel
} RP_CONFIG_CB;

typedef struct
{
	uint16_t	frameType: 3;
	uint16_t	securityEnabled: 1;
	uint16_t	framePending: 1;
	uint16_t	ackRequest: 1;
	uint16_t	panIdCompression: 1;
	uint16_t	reserved: 1;
	uint16_t	sequenceNumberSuppress: 1;
	uint16_t	ieListPresent: 1;
	uint16_t	dstAddrMode: 2;
	uint16_t	frameVersion: 2;
	uint16_t	srcAddrMode: 2;
} RpMacFrameControlT;

/***********************************************************************
 * LOG macro definitions
 **********************************************************************/
// API or Callback (return)
#define	RP_LOG_API_INIT					(0x10)
#define	RP_LOG_API_RESET				(0x20)
#define	RP_LOG_API_RXOFF				(0x30)
#define	RP_LOG_API_RXON					(0x40)
#define	RP_LOG_API_DATAREQ				(0x50)
#define	RP_LOG_API_ED					(0x60)
#define	RP_LOG_API_CCA					(0x70)
#define	RP_LOG_API_SETIB				(0x80)
#define	RP_LOG_API_GETIB				(0x90)
#define	RP_LOG_API_RELBUF				(0xA0)
#define	RP_LOG_API_SETPOW				(0xB0)
#define	RP_LOG_API_GETPOW				(0xC0)
#define	RP_DEBUG_IRQ_MODEM_HDR			(0xD0)
#define	RP_DEBUG_IRQ_MODEM_TIMER		(0xD1)
#define	RP_LOG_CB						(0xF0)
#define	RP_LOG_API_RET					(0x01)
#define RP_LOG_API_RETERR				(0x02)

// API primary factor
#define RP_LOG_API_RETERR_TOTALTXTIME	(0x81)
#define RP_LOG_API_RETERR_DATAREQ		(0x82)
#define RP_LOG_API_RETERR_FRAMELEN		(0x83)
#define RP_LOG_API_RETERR_NODATA		(0x84)

// Callback primary factor
#define	RP_LOG_CB_DATAIND				(0x01)
#define	RP_LOG_CB_DATACFM				(0x02)
#define	RP_LOG_CB_CCACFM				(0x03)
#define	RP_LOG_CB_EDCFM					(0x04)
#define	RP_LOG_CB_OFFIND				(0x05)
#define	RP_LOG_CB_CALCLQI				(0x06)
#define	RP_LOG_CB_ACKCHECK				(0x07)
#define	RP_LOG_CB_WARNING_RXOVERRUN		(0x08)
#define	RP_LOG_CB_WARNING_RXDELAY		(0x09)
#define	RP_LOG_CB_WARNING_TXACKSTOP		(0x0A)
#define	RP_LOG_CB_WARNING_NOTGETBUF		(0x0B)

/***************************************************************************************************************
 * function prototypes
 **************************************************************************************************************/
/* phy_api.c */
uint8_t RpRxOffBeforeReplyingAck( void );
uint8_t RpExtChkErrFrameLen( uint16_t len, uint8_t useRxFcsLength );
void RpTC0SetReg( uint8_t csmaStaEna, uint32_t time );
uint8_t RpSetTxTriggerTimer( uint32_t willTxTime, uint8_t onRetrn, uint8_t options );
uint32_t RpCalcTxInterval( uint16_t totalTxLength, uint8_t onRetrn );
uint16_t RpCalcTotalBytes( uint16_t dataPayloadLength, uint16_t *pSduLen, uint8_t useRxFcsLength );
void RpTrnxHdrFunc( uint8_t trnxNo );
void RpSetAdrfAndAutoAckVal( void );
void RpAvailableRcvRamEnable( void );
void RpRxOnStart( void );
void RpSetStateRxOnToTrxOff( void );
void RpRxOnStop( void );
uint32_t RpReadIrq( void );
void RpAddressFilterSetting( uint8_t addressFilterMode );
void RpWrEvaReg2( uint16_t aData );
uint8_t RpRdEvaReg2( uint16_t aData );
uint8_t RpSetChannelVal( uint8_t channel );
void RpSetFskOpeModeVal( uint8_t afterReset );
void RpSetChannelsSupportedPageAndVal( void );
uint8_t RpSetTxPowerVal( uint8_t gainSet );
void RpSetAntennaSwitchVal( void );
void RpSetAntennaDiversityVal( void );
void RpSetAntennaSelectTxVal( void );
void RpSetCcaVthVal( void );
void RpSetLvlVthVal( void );
void RpSetCcaDurationVal( uint8_t pibValReq );
void RpInverseTxAnt( uint8_t invAntPortEna );
void RpSetPreambleLengthVal( void );
void RpSetMrFskSfdVal( void );
void RpSetFskScramblePsduVal( void );
void RpSetFcsLengthVal( void );
void RpSetAckReplyTimeVal( void );
void RpSetAckWaitDurationVal( void );
void RpSetCsmaBackoffPeriod( void );
void RpSetBackOffSeedVal( void );
void RpSetFecVal( void );
void RpSetMaxCsmaBackoffVal( void );
void RpSetMaxBeVal( void );
void RpSetSpecificModeVal( uint8_t afterReset );
void RpSetPreamble4ByteRxMode( void );
void RpSetAgcStartVth( uint8_t reset );
uint16_t RpGetAgcStartVth( void );
void RpSetCcaEdBandwidth( void );
void RpSetAntennaDiversityStartVth( uint8_t reset );
uint16_t RpGetAntennaDiversityStartVth( void );
uint16_t RpGetAntennaSwitchingTime( void );
void RpSetSfdDetectionExtend( void );
void RpSetSfdDetectionExtendWrite( uint16_t status );
void RpSetAgcWaitGain( void );
void RpSetAntennaSwitchEnaTiming( void );
void RpSetGpio0Setting( void );
void RpSetGpio3Setting( void );
void RpPrevSentTimeReSetting( void );
uint8_t *RpGetRxBuf( void );
int16_t RpExtChkIdLenGetReq( uint8_t id, uint8_t valLen, void *pVal );
int16_t RpExtChkIdLenSetReq( uint8_t id, uint8_t valLen, void RP_FAR *pVal );
void *RpMemcpy( void *s1, const void RP_FAR *s2, uint16_t n );
void *RpMemset( void *s, int16_t c, uint16_t n );
uint8_t RpCheckLongerThanTotalTxTime( uint16_t txLength, uint8_t txOpt, uint8_t useRxFcsLength );
void RpUpdateTxTime( uint8_t trndTimes, uint8_t isAck, uint8_t useRxFcsLength );
void RpSetMacTxLimitMode( void );
void RpRegCcaBandwidth225k( void );
void RpRegAdcVgaDefault( void );
void RpRegTxRxDataRateDefault( void );
void RpRegRxDataRateDefault( void );
void RpLog_Event( uint8_t function, uint8_t option );
#ifdef RP_ST_ENV
void RpSetRfInt( uint8_t enable );
#endif // #ifdef RP_ST_ENV

/* phy_attr.c */
uint8_t RpSetAttr_phyAckWithCca( uint8_t attrValue );
#ifdef R_FREQUENCY_OFFSET_ENABLED
	uint8_t RpSetAttr_phyFrequencyOffset( int32_t attrValue );
#endif // #ifdef R_FREQUENCY_OFFSET_ENABLED
#ifdef RP_ST_ENV
void RpPlmeGetReq( uint8_t id, void *pVal );
int16_t RpPlmeSetReq( uint8_t attrId, void RP_FAR *p_attrValue );
#endif // #ifdef RP_ST_ENV

/* phy_int.c */
void RpChangeDefaultFilter( void );
void RpChangeNarrowBandFilter( void );
void RpChangeFilter( void );
void RpStartTransmitWithCca( void );
void RpAntSelAssist_StartProc( void );
void RpAntSelAssist_StopProc( uint16_t status );
void RpAntSelAssist_RecoveryProc( void );
uint8_t RpAckCheckCallback( uint8_t *pData, uint16_t dataLen, uint8_t *pAck, uint16_t *pAckLen );
#ifndef RP_WISUN_FAN_STACK
	uint8_t RpCalcLqiCallback( uint16_t rssiEdValue, uint8_t rssiEdSelect );
#endif // #ifndef RP_WISUN_FAN_STACK

/* phy_drv.c */
#if defined(__RL78__)
	void RpSetMcuInt( uint8_t enable );
	void RpRegWrite( uint16_t adr, uint8_t dat );
	uint8_t RpRegRead( uint16_t adr );
	uint8_t RpCheckRfIRQ( void );
	uint32_t RpLimitTimerControl( uint8_t timerOn );
	void RpMcuPeripheralInit( void );
	void RpWakeupSequence( void );
	void RpExecuteCalibration( void );
	void RpPowerdownSequence( void );
	#if RP_DMA_WRITE_RAM_ENA
		void RpRegBlockWrite( uint16_t adr, uint8_t RP_FAR *dat, uint16_t len );
	#endif // #if RP_DMA_WRITE_RAM_ENA
	#if RP_DMA_READ_RAM_ENA
		void RpRegBlockRead( uint16_t adr, uint8_t *dat, uint16_t len );
	#endif // #if RP_DMA_READ_RAM_ENA
#endif // #if defined(__RL78__)

#if defined(__RX)
	void RpSetMcuInt( uint8_t enable );
	void RpRegWrite( uint16_t adr, uint8_t dat );
	uint8_t RpRegRead( uint16_t adr );
	uint8_t RpCheckRfIRQ( void );
	uint32_t RpLimitTimerControl( uint8_t timerOn );
	void RpMcuPeripheralInit( void );
	void RpWakeupSequence( void );
	void RpExecuteCalibration( void );
	void RpPowerdownSequence( void );
	#if RP_DMA_WRITE_RAM_ENA
		void RpRegBlockWrite( uint16_t adr, uint8_t RP_FAR *pDat, uint16_t len );
	#endif // #if RP_DMA_WRITE_RAM_ENA
	#if RP_DMA_READ_RAM_ENA
		void RpRegBlockRead( uint16_t adr, uint8_t *pDat, uint16_t len );
	#endif // if RP_DMA_READ_RAM_ENA
#endif // #if defined(__RX)

#if defined(__arm)
	void RpSetMcuInt( uint8_t enable );
	void RpRegWrite(uint16_t adr, uint8_t dat);
	uint8_t RpRegRead( uint16_t adr );
	uint8_t RpCheckRfIRQ( void );
	uint32_t RpLimitTimerControl( uint8_t timerOn );
	void RpMcuPeripheralInit( void );
	void RpWakeupSequence( void );
	void RpPowerdownSequence( void );
	void RpExecuteCalibration( void );
	void RpRegBlockWrite( uint16_t adr, uint8_t RP_FAR *pDat, uint16_t len );
	void RpRegBlockRead( uint16_t adr, uint8_t *pDat, uint16_t len );
#endif

#if defined(__i386) || defined(__x86_64)
	void RpRegBlockRead( uint16_t adr, uint8_t *pDat, uint16_t len );
#endif // #if defined(__i386) || defined(__x86_64)

#endif // _PHY_DEF_H
/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/

