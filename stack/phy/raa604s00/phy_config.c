/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_config.c
 * description	: This is the RF driver's configuration code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
/***************************************************************************************************************
 * includes
 **************************************************************************************************************/
#include <phy.h>
#include <phy_def.h>
#include <phy_config.h>

#if (RP_USR_RF_LIMIT_TMR_SELECT == RP_CLK_LIMIT_TMR_32KHz)
	#if (RP_USR_RF_TAU_CH_SELECT != RP_TAU_CH01)
		#error "error: Not Select Timer Array Unit 0 Channel 1"
	#endif
#endif

/***************************************************************************************************************
 * Exported global variables and functions (to be accessed by other files)
 * Please refer and modify definitions in phy_config.h
 **************************************************************************************************************/
const RP_CONFIG_CB RpConfig =
{
	RP_USR_RF_DFLT_TRANSMIT_POWER,			// PIB phyTransmitPower Default
	(uint16_t)RP_USR_RF_DFLT_CCA_VTH,		// PIB phyCcaVth Default
	RP_USR_RF_DFLT_PROFILE_SPECIFIC_MODE,	// PIB phyProfileSpecificMode Default
	RP_USR_RF_DFLT_TX_ANTENNA_SWITCH_ENA,	// PIB phyTxAntennaSwitchEna Default
	RP_USR_RF_DFLT_ANTENNA_DIVERSITY_ENA,	// PIB phyAntennaDiversityEna Default
	RP_USR_RF_RSSI_LOSS,					// RSSI Loss diff dBm
	RP_USR_RF_CLK_SOURCE_SELECT,			// clock Source for RF
	RP_USR_RF_LIMIT_TMR_SELECT,				// clock Source for tx limit control
	RP_USR_RF_TAU_CH_SELECT,				// Select Timer Array Unit Channel
};

/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
 
