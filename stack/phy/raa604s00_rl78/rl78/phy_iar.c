/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *****************************************************************************/
/******************************************************************************
 * file name	: phy_iar.c
 * description	: This is the RF driver's api code.
 ******************************************************************************
 * Copyright (C) 2014-2018 Renesas Electronics Corporation.
 *****************************************************************************/

/******************************************************************************
Includes <System Includes> , "Project Includes"
******************************************************************************/

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Imported global variables and functions (from other files)
******************************************************************************/

/******************************************************************************
Exported global variables and functions (to be accessed by other files)
******************************************************************************/

/******************************************************************************
Macro definitions
******************************************************************************/

/******************************************************************************
Private global variables and functions
*****************************************************************************/

/******************************************************************************
Function Name:       __get_psw
Parameters:	     none
Return value:        PSW register
Description:         This function returns the PSW register value.
                     It replaces the intrinsic function __get_psw
                     which is only available on the CC-RL compiler
******************************************************************************/
/* Mask Compiler Warning[Pe940]: missing return statement at end of non-void function */ 
#pragma diag_suppress=Pe940
/* Reason: The return parameter is passed implicit in the inline assembler
   instruction by using register a. This is inline and according to the IAR
   calling conventions.*/               

unsigned char __get_psw(void)
{
    asm(" mov a, psw");
}

/******************************************************************************
Function Name:       __set_psw
Parameters:	     PSW register value
Return value:        none
Description:         This function writtes to the PSW register.
                     It replaces the intrinsic function __get_psw
                     which is only available on the CC-RL compiler
******************************************************************************/
void __set_psw(unsigned char a)
{
    asm(" mov psw,a");
}

/******************************************************************************
 * Copyright (C) 2014-2018 Renesas Electronics Corporation.
 *****************************************************************************/
