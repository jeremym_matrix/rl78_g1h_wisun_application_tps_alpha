/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
/*******************************************************************************
 * file name	: phy_mac.c
 * description	: Mac Callback Operation
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/
#include <intrinsics.h>

#include <ior5f11fll.h>
#include <ior5f11fll_ext.h>

/***********************************************************
 * include
 **********************************************************/

#include "r_stdint.h"
#include "RL78G1H.h"

/***********************************************************
 * Interrupt Level
 **********************************************************/
#define RP_PHY_INTLEVEL_SYSCALLTimer	(2)	// OS system call
#define RP_PHY_INTLEVEL_SYSCALLTimer_b0	(RP_PHY_INTLEVEL_SYSCALLTimer & 0x01)
#define RP_PHY_INTLEVEL_SYSCALLTimer_b1	((RP_PHY_INTLEVEL_SYSCALLTimer & 0x02) >> 1)

/***********************************************************
 * typedef
 **********************************************************/
typedef void (*RmMacOSCallbackT)(uint8_t);
volatile RmMacOSCallbackT pRmMacOSCback;
volatile uint8_t RmWupTskEventId[2];
volatile uint8_t RmWupTskEventIdNum = 0;

/***********************************************************************
 * program start
 **********************************************************************/

/***********************************************************************
 *	function Name  : RpInitWupTsk
 *	parameters     : none
 *	return value   : none
 *	description    : initial wakeup-task ram.
 **********************************************************************/
void RpInitWupTsk( void )
{
	uint8_t macnum;

	for ( macnum=0; macnum < 2 ; macnum++ )
	{
		RmWupTskEventId[macnum] = 0;
	}
	RmWupTskEventIdNum = 0;
	return;
}

/***********************************************************************
 *	function Name  : RpWupTsk
 *	parameters     : eventId
 *	return value   : none
 *	description    : set wakeup-task handler.
 **********************************************************************/
void RpWupTsk( uint8_t eventId )
{
	if ( RmWupTskEventIdNum < 2 )
	{
		RmWupTskEventId[RmWupTskEventIdNum++] = eventId;
	}

	/* generate timer interrupt */
	TMIF00 = 0;		// INTTM00 interrupt flag clear
	TDR00 = 1;
	TMPR100 = RP_PHY_INTLEVEL_SYSCALLTimer_b1;	//
	TMPR000 = RP_PHY_INTLEVEL_SYSCALLTimer_b0;	// Level_2
	TMMK00 = 0;		// INTTM00 enabled
	TS0 |= TAU_CH0_START_TRG_ON;
}

/***********************************************************************
 *	function Name  : RpWupTskHdr
 *	parameters     : none
 *	return value   : none
 *	description    : This handler is OS depending interrupt for "iset_flg".
 **********************************************************************/
#if defined (__ICCRL78__)
#pragma vector = INTTM00_vect
__interrupt void RpWupTskHdr(void)
#else
void RpWupTskHdr(void)
#endif
{
	uint8_t   j;

	__enable_interrupt();

	// stop timer
	TMMK00 = 1;		// INTTM00 disabled
	TT0 |= TAU_CH0_STOP_TRG_ON;
	// Call MAC OS Callback
	for (j = 0; j < RmWupTskEventIdNum; j++)
	{
		(*pRmMacOSCback)(RmWupTskEventId[j]);
	}
	RmWupTskEventIdNum = 0;
}

/*******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

