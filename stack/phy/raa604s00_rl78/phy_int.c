/***********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products.
 * No other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
 * applicable laws, including copyright laws. 
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING THIS SOFTWARE, WHETHER EXPRESS, 
 * IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE 
 * AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS 
 * AFFILIATED COMPANIES SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR 
 * ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH 
 * DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the 
 * following link:
 * http://www.renesas.com/disclaimer 
 **********************************************************************************************************************/
/***********************************************************************************************************************
 * file name	: phy_int.c
 * description	: This is the RF driver's interrupts code.
 ***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/
/***************************************************************************************************************
 * includes
 **************************************************************************************************************/
#include <string.h>
//
#include "phy.h"
#include "phy_def.h"
//
#if defined(__arm)
	#include "cpx3.h"
	#include "phy_drv.h"
#endif
//
#ifdef R_FSB_FAN_ENABLED
	#include "r_fsb.h"
#endif // #ifdef R_FSB_FAN_ENABLED

/***************************************************************************************************************
 * extern definitions
 **************************************************************************************************************/
extern RP_PHY_CB RpCb;
extern RP_PHY_ERROR RpRfStat;
extern const uint16_t RpTc0WasteTime[];
extern const uint8_t RpFreqBandTbl[][RP_MAXNUM_OFFSET];
extern const uint16_t RpCcaVthOffsetTblDefault[];
extern const uint16_t RpCcaVthOffsetTblNarrow[];

/***************************************************************************************************************
 * private function prototypes
 **************************************************************************************************************/
/* "Interrupt source number 1" */
/* "Timer compare 0 interrupt handler" */
static void RpBbTim0Hdr( void );

/* "Interrupt source number 2" */
/* "Timer compare 1 interrupt handler" */
static void RpBbTim1Hdr( void );

/* "Interrupt source number 3" */
/* "Timer compare 2 interrupt handler" */
static void RpBbTim2Hdr( void );

/* "Interrupt source number 4" */
/* "Frame transmit completion interrupt handler" */
static void RpTrnFinHdr( void );

/* "Interrupt source number 5" */
/* "Bank0 transmit completion interrupt handler" */
static void RpTrn0Hdr( void );

/* "Interrupt source number 6" */
/* "Bank1 transmit completion interrupt handler" */
static void RpTrn1Hdr( void );

/* "Interrupt source number 8,9" */
/* "CCA/CSMA-CA completion interrupt handler" */
static void RpCcaHdr( void );

/* "Interrupt source number 10" */
/* "Frame receive completion interrupt handler" */
static void RpRcvFinHdr( void );

/* "Interrupt source number 11" */
/* "Bank0 receive completion interrupt handler" */
static void RpRcv0Hdr( void );

/* "Interrupt source number 12" */
/* "Bank1 receive completion interrupt handler" */
static void RpRcv1Hdr( void );

/* "Interrupt source number 13" */
/* "Start reception interrupt handler" */
static void RpRcvStHdr( void );

/* "Interrupt source number 14" */
/* "Address filter interrupt handler" */
static void RpAdrsHdr( void );

/* "Interrupt source number 15" */
/* "Receive overrun interrupt handler" */
static void RpRovrHdr( void );

/* "Interrupt source number 16" */
/* "Mode switch receive completion interrupt handler" */
static void RpRcvModeswHdr( void );

/* "Interrupt source number 17" */
/* "Receive level filter interrupt handler" */
static void RpRcvLvlIntHdr( void );

/* "Interrupt source number 18" */
/* "Receive byte counts interrupt handler" */
static void RpRcvCuntHdr( void );

/* "Interrupt source number 19" */
/* "Frame length interrupt handler" */
static void RpFlenHdr( void );

/* "Interrupt source number 20" */
/* "Byte reception interrupt handler" */
static void RpByteRcvHdr( void );

/* === Unused interrupt handler === */
/* "Interrupt source number 7" "Calibration completion" */
/* "Interrupt source number 21" "Automatic receive timeout" */

//
static void RpSetAntennaDiversityModeReg( void );
static void RpSetNumberOfCrcBitSwitchBit( void );
static void RpGetAntennaDiversityRssi( void );
static void RpGetSelectedAntennaInfo( uint16_t status );
static void RpValidityCheckSFDTimeStamp( void );
static void RpPdDataIndCallbackPrc( uint8_t *pBuf, uint8_t stOptInd, uint8_t warningIndCallbackFlag );
static void RpTrnHdrFunc( void );
static void RpCancelTrn( uint16_t status );
static uint8_t RpDriverRetryTrn( uint16_t status );
static void RpAfterTrnEnd( uint16_t status );
static void RpResumeCsmaTrn( uint16_t status, uint8_t ccaTotal, uint8_t rxEna );
static void RpStateRx_SetTxTrigger( uint8_t useCca );
static void RpEndTransmitWithCca( void );
static uint8_t RpChkCrcStatus( uint8_t *pStatOpt, uint8_t txrxst0 );
static void RpRxEndSetting( uint16_t status );
static void RpRcvHdrFunc( uint8_t rcvNo );
static void RpRcvRamEnable( uint8_t rcvxNos );
static void RpReadRam( uint8_t rcvNo );
static int16_t RpChkRcvBank( uint8_t rcvBankNo );
static void RpSelectRcvDataBank( void );
static int16_t RpReadRxDataBank( void );
static uint8_t RpDetRcvdAntenna( void );
static void RpReadAddressInfo( void );
static void RpDumPrcRcvPhr( void );
static void RpReStartProc( void );
static void RpAntSelAssist_StateCheckHdr( void );
static void RpAntSelAssist_TimerProc( void );
static void RpAntSelAssist_ReStartProc( void );

/***************************************************************************************************************
 * private variables
 **************************************************************************************************************/
#ifdef R_FSB_FAN_ENABLED
	typedef struct {
		uint8_t enable;
		uint8_t resData[R_SIZE_PSDU];
	} r_phy_fsb_t;
	r_phy_fsb_t RpFsb;
#endif // #ifdef R_FSB_FAN_ENABLED

typedef struct {
	uint8_t enable;
	uint8_t result;
} r_phy_cca_ack_t;
r_phy_cca_ack_t RpCcaAck;

/***************************************************************************************************************
 * program
 **************************************************************************************************************/
/***************************************************************************************************************
 * function name  : RpIntpHdr
 * description    : intp interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
void RpIntpHdr( void )
{
	#if defined(__arm)
	volatile uint32_t ireq, ien;
	#else
	uint32_t ireq, ien;
	#endif

	while (RpCheckRfIRQ() == RP_TRUE)
	{
		#if !defined(__CCRL__) && !defined(__ICCRL78__)
		ien = *(uint32_t *)(RpCb.reg.bbIntEn);
		#else
		ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
			  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
		#endif

		ireq = RpReadIrq();
		ireq &= ien;

		while (ireq != RP_BB_NO_IREQ)
		{
			if (ireq & RP_BBRCVST_IREQ)
			{
				#if defined(__arm)
				RpLedRxOff();
				#endif

				RpRcvStHdr();

				#if !defined(__CCRL__) && !defined(__ICCRL78__)
				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
				#else
				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
				#endif

				ireq &= ien;
				ireq &= (~RP_BBRCVST_IREQ);
			}
			else if (ireq & RP_BBMODEVA_IREQ)
			{
				RpAntSelAssist_StateCheckHdr();

				#if !defined(__CCRL__) && !defined(__ICCRL78__)
				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
				#else
				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
				#endif

				ireq |= RpReadIrq();
				ireq &= ien;
				ireq &= (~RP_BBMODEVA_IREQ);
			}
			else if (ireq & RP_BBBYTE_IREQ)
			{
				RpByteRcvHdr();

				#if !defined(__CCRL__) && !defined(__ICCRL78__)
				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
				#else
				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
				#endif

				ireq |= RpReadIrq();
				ireq &= ien;
				ireq &= (~(RP_BBBYTE_IREQ | RP_BBTIM2_IREQ));
			}
			else if (ireq & RP_BBTIM2_IREQ)
			{
				RpBbTim2Hdr();

				#if !defined(__CCRL__) && !defined(__ICCRL78__)
				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
				#else
				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
				#endif

				ireq |= (RpCb.reg.bbIntReq & ien);
				ireq &= (~(RP_BBBYTE_IREQ | RP_BBTIM2_IREQ));
			}
			else if (ireq & RP_BBROVR_IREQ)
			{
				RpRovrHdr();
				ireq &= (~(RP_BBROVR_IREQ | RP_BBADRS_IREQ | RP_BBFL_IREQ | RP_BBTIM2_IREQ));
			}
			else if (ireq & RP_BBADRS_IREQ)
			{
				RpAdrsHdr();
				ireq &= (~RP_BBADRS_IREQ);
				if ((RpCb.status & RP_PHY_STAT_RX_TMOUT) && (RpCb.pib.phyRxTimeoutMode == RP_TRUE) &&
						(RpCb.rx.cnt == RP_PHY_RX_STAT_PHR_DETECT))
				{
					ireq |= RpReadIrq();
					ireq &= ien;
					ireq &= (~RP_BBTIM1_IREQ);
				}
			}
			else if (ireq & RP_BBFL_IREQ)
			{
				RpFlenHdr();
				ireq &= (~RP_BBFL_IREQ);
				if ((RpCb.status & RP_PHY_STAT_RX_TMOUT) && (RpCb.pib.phyRxTimeoutMode == RP_TRUE) &&
						(RpCb.rx.cnt == RP_PHY_RX_STAT_PHR_DETECT))
				{
					ireq |= RpReadIrq();
					ireq &= ien;
					ireq &= (~RP_BBTIM1_IREQ);
				}
			}
			else if (ireq & RP_BBRCVCUNT_IREQ)
			{
				RpRcvCuntHdr();
				ireq &= (~RP_BBRCVCUNT_IREQ);
			}
			else if (ireq & RP_BBRCV0_IREQ)
			{
				RpRcv0Hdr();
				ireq &= (~RP_BBRCV0_IREQ);
			}
			else if (ireq & RP_BBRCV1_IREQ)
			{
				RpRcv1Hdr();
				ireq &= (~RP_BBRCV1_IREQ);
			}
			else if (ireq & RP_BBRCVFIN_IREQ)
			{
				#if defined(__arm)
				RpLedRxOn();
				#endif

				RpRcvFinHdr();

				#if !defined(__CCRL__) && !defined(__ICCRL78__)
				ien = *(uint32_t *)(RpCb.reg.bbIntEn);
				#else
				ien = ((uint32_t)(RpCb.reg.bbIntEn[2]) << 16u) +
					  (uint32_t)RP_VAL_ARRAY_TO_UINT16(RpCb.reg.bbIntEn);
				#endif

				ireq |= (RpCb.reg.bbIntReq & ien);
				ireq &= ien;
				ireq &= (~RP_BBRCVFIN_IREQ);
			}
			else if (ireq & RP_BBCCA_IREQ)
			{
				RpCcaHdr();
				ireq &= (~RP_BBCCA_IREQ);
			}
			else if (ireq & RP_BBTRN0_IREQ)
			{
				#if defined(__arm)
				RpLedTxOff();
				#endif

				RpTrn0Hdr();
				ireq &= (~RP_BBTRN0_IREQ);
			}
			else if (ireq & RP_BBTRN1_IREQ)
			{
				#if defined(__arm)
				RpLedTxOff();
				#endif

				RpTrn1Hdr();
				ireq &= (~RP_BBTRN1_IREQ);
			}
			else if (ireq & RP_BBTRNFIN_IREQ)
			{
				#if defined(__arm)
				RpLedTxOn();
				#endif

				RpTrnFinHdr();
				ireq &= (~RP_BBTRNFIN_IREQ);
			}
			else if (ireq & RP_BBTIM0_IREQ)
			{
				RpBbTim0Hdr();
				ireq &= (~RP_BBTIM0_IREQ);
			}
			else if (ireq & RP_BBTIM1_IREQ)
			{
				RpBbTim1Hdr();
				ireq &= (~RP_BBTIM1_IREQ);
			}
			else if (ireq & RP_BBMODESW_IREQ)
			{
				RpRcvModeswHdr();
				ireq &= (~(RP_BBMODESW_IREQ | RP_BBTIM2_IREQ));
			}
			else if (ireq & RP_BBRCVLVL_IREQ)
			{
				RpRcvLvlIntHdr();
				ireq &= (~(RP_BBRCVLVL_IREQ | RP_BBTIM2_IREQ));
			}
			else
			{
				ireq = RP_BB_NO_IREQ;
			}
		}
	}
}

/* "Interrupt source number 1" */
/***************************************************************************************************************
 * function name  : RpBbTim0Hdr
 * description    : Timer compare 0 interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpBbTim0Hdr( void )
{
	uint16_t	status;

	RpCb.reg.bbIntEn[0] &= ~TIM0INTEN;
	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
	status = RpCb.status;
	if (status & RP_PHY_STAT_WAIT_TMRTRG)
	{
		RpCb.status &= (~RP_PHY_STAT_WAIT_TMRTRG);
		if (status & RP_PHY_STAT_TX)
		{
			RpCb.status |= RP_PHY_STAT_BUSY;
		}
		else if (status & RP_PHY_STAT_RX)
		{
			RpAvailableRcvRamEnable();
			RpRxOnStart();	// RX trigger
		}
	}
}

/* "Interrupt source number 2" */
/***************************************************************************************************************
 * function name  : RpBbTim1Hdr
 * description    : Timer compare 1 interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpBbTim1Hdr( void )
{
	uint16_t	status;

	RpCb.reg.bbIntEn[0] &= ~TIM1INTEN;
	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
	status = RpCb.status;

	if (status & RP_PHY_STAT_RX)
	{
		if (status & RP_PHY_STAT_RX_TMOUT)
		{
			if (RpRxOffBeforeReplyingAck() == RP_FALSE)
			{
				RpCb.statusRxTimeout |= RP_STATUS_RX_TIMEOUT_TX;		// just only sign force-RxOff here
			}
			else
			{
				RpSetStateRxOnToTrxOff();
				RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;

				/* Callback function execution Log */
				RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
				/* Callback function execution */
				INDIRECT_RpRxOffIndCallback();
			}
		}
	}
}

/* "Interrupt source number 4" */
/***************************************************************************************************************
 * function name  : RpTrnFinHdr
 * description    : Frame transmit completion interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpTrnFinHdr( void )
{
	RpTrnHdrFunc();
}

/***********************************************************************
 * function name  : RpChangeDefaultFilter
 * parameters     : none
 * return value   : none
 * description    : 
 **********************************************************************/
void RpChangeDefaultFilter( void )
{
	uint8_t index = RpCb.freqIdIndex;
	uint8_t antDivRxEna = RpCb.pib.phyAntennaDiversityRxEna;
	uint8_t fecRxEna = RpCb.pib.phyFskFecRxEna;
	uint8_t arrayChar[3];

	// filter change case
	arrayChar[0] = (antDivRxEna == RP_TRUE)? (uint8_t)RpFreqBandTbl[index][RP_0x042D_ANDVON_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x042D_OFFSET];
	RpRegBlockWrite((0x042D << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 1);

	RpRegWrite((0x0430 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0430_OFFSET]);
	RpRegWrite((0x0432 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0432_OFFSET]);
	RpRegWrite((0x0436 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0436_OFFSET]);
	RpRegWrite((0x043A << 3), (uint8_t)RpFreqBandTbl[index][RP_0x043A_OFFSET]);
	RpRegWrite((0x0456 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0456_OFFSET]);

	arrayChar[0] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0486_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0486_OFFSET];
	arrayChar[1] = (fecRxEna == RP_FEC_RX_MODE_ENABLE)? (uint8_t)RpFreqBandTbl[index][RP_0x0487_F_OFFSET]: (uint8_t)RpFreqBandTbl[index][RP_0x0487_OFFSET];
	arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x0488_OFFSET];
	RpRegBlockWrite((0x0486 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);

	RpRegWrite((0x04F4 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x04F4_OFFSET]);
	RpRegWrite((0x0505 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0505_OFFSET]);
	RpRegWrite((0x0580 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0580_OFFSET]);
}

/***********************************************************************
 * function name  : RpChangeNarrowBandFilter
 * parameters     : none
 * return value   : none
 * description    : 
 **********************************************************************/
void RpChangeNarrowBandFilter( void )
{
	uint8_t index = RpCb.freqIdIndex;
	uint8_t arrayChar[3];

	// filter change case
	RpRegWrite((0x042D << 3), (uint8_t)RpFreqBandTbl[index][RP_0x042D_DIGICCA_OFFSET]);
	RpRegWrite((0x0430 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0430_DIGICCA_OFFSET]);
	RpRegWrite((0x0432 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0432_DIGICCA_OFFSET]);
	RpRegWrite((0x0436 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0436_DIGICCA_OFFSET]);
	RpRegWrite((0x043A << 3), (uint8_t)RpFreqBandTbl[index][RP_0x043A_DIGICCA_OFFSET]);
	RpRegWrite((0x0456 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0456_DIGICCA_OFFSET]);

	arrayChar[0] = (uint8_t)RpFreqBandTbl[index][RP_0x0486_DIGICCA_OFFSET];
	arrayChar[1] = (uint8_t)RpFreqBandTbl[index][RP_0x0487_DIGICCA_OFFSET];
	arrayChar[2] = (uint8_t)RpFreqBandTbl[index][RP_0x0488_DIGICCA_OFFSET];
	RpRegBlockWrite((0x0486 << 3), (uint8_t RP_FAR *)&(arrayChar[0]), 3);

	RpRegWrite((0x04F4 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x04F4_DIGICCA_OFFSET]);
	RpRegWrite((0x0505 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0505_DIGICCA_OFFSET]);
	RpRegWrite((0x0580 << 3), (uint8_t)RpFreqBandTbl[index][RP_0x0580_DIGICCA_OFFSET]);
}

/***********************************************************************
 * function name  : RpChangeFilter
 * parameters     : none
 * return value   : none
 * description    : 
 **********************************************************************/
void RpChangeFilter( void )
{
	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
	uint8_t reg488h;

	// filter change case
	if ((ccaBandwidth != RP_PHY_CCA_BANDWIDTH_INDEX_225K)
		|| (edBandwidth != RP_PHY_CCA_BANDWIDTH_INDEX_225K))
	{
		reg488h = RpRegRead(0x0488<<3);
		if (reg488h != RP_PHY_REG488H_DEFAULT)
		{
			if ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW)
				|| (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_NARROW))
			{
				RpChangeDefaultFilter();
			}
			else
			{
				RpRegCcaBandwidth225k();
				RpRegAdcVgaDefault();
				RpRegRxDataRateDefault();
			}
		}
	}
//#ifdef RP_NARROW_CCA
}

/***********************************************************************
 * function name  : RpSetAntennaDiversityModeReg
 * parameters     : none
 * return value   : none
 * description    : 
 **********************************************************************/
static void RpSetAntennaDiversityModeReg( void )
{
	uint8_t antDvrCon;

	if (RpCb.pib.phyAntennaDiversityRxEna)
	{
		antDvrCon = RpRegRead(BBANTDIV);
		antDvrCon &= ~ANTSWTRNSET;
		if (RpCb.rx.bkupAckAnt)
		{
			antDvrCon |= ANTSWTRNSET;
		}
		RpRegWrite(BBANTDIV, antDvrCon);
	}
}

/***********************************************************************
 * function name  : RpSetNumberOfCrcBitSwitchBit
 * parameters     : none
 * return value   : none
 * description    : 
 **********************************************************************/
static void RpSetNumberOfCrcBitSwitchBit( void )
{
	if (RpCb.rx.fcsLength != RpCb.pib.phyFcsLength)
	{
		RpSetFcsLengthVal();
	}
}

/***********************************************************************
 * function name  : RpGetAntennaDiversityRssi
 * parameters     : none
 * return value   : none
 * description    : 
 **********************************************************************/
static void RpGetAntennaDiversityRssi( void )
{
	RpCb.rx.rssi0 = 0x0100;
	RpCb.rx.rssi1 = 0x0100;

	if (RpCb.pib.phyAntennaDiversityRxEna)
	{
		RpRegBlockRead(BBANT0RD, (uint8_t *)&RpCb.rx.rssi0, sizeof(uint16_t));
		RpRegBlockRead(BBANT1RD, (uint8_t *)&RpCb.rx.rssi1, sizeof(uint16_t));
	}
}

/***********************************************************************
 * function name  : RpGetSelectedAntennaInfo
 * parameters     : status...phy status
 * return value   : none
 * description    : 
 **********************************************************************/
static void RpGetSelectedAntennaInfo( uint16_t status )
{
	if (RpCb.pib.phyAntennaDiversityRxEna)
	{
		if (RpCb.rx.softwareAdf == RP_FALSE || ((status & RP_PHY_STAT_TX) == 0))
		{
			RpCb.rx.selectedAntenna = RpDetRcvdAntenna();
		}
	}
	else
	{
		RpCb.rx.selectedAntenna = 0;
	}
}

/***********************************************************************
 * function name  : RpValidityCheckSFDTimeStamp
 * parameters     : none
 * return value   : none
 * description    : 
 **********************************************************************/
static void RpValidityCheckSFDTimeStamp( void )
{
	if (RpCb.rx.softwareAdf == RP_FALSE)
	{
		if ((RpCb.pib.macAddressFilter1Ena == RP_TRUE)
			||(RpCb.pib.macAddressFilter2Ena == RP_TRUE))
		{
			RpCb.rx.timeStamp = RP_NULL;
		}
	}

	if (RpCb.pib.phyFskFecRxEna != RP_FEC_RX_MODE_DISABLE)
	{
		RpCb.rx.timeStamp = RP_NULL;
	}

	switch (RpCb.freqIdIndex)
	{
		case RP_PHY_200K_M03:
		case RP_PHY_400K_M03:
			RpCb.rx.timeStamp = RP_NULL;
			break;
		default:
			break;
	}
}

/***********************************************************************
 * function name  : RpPdDataIndCallbackPrc
 * parameters     : pBuf...
 *				  : stOptInd...
 *				  : warningIndCallbackFlag...
 * return value   : none
 * description    : 
 **********************************************************************/
static void RpPdDataIndCallbackPrc( uint8_t *pBuf, uint8_t stOptInd, uint8_t warningIndCallbackFlag )
{
	/* Callback function execution Log */
	RpLog_Event( RP_LOG_CB, RP_LOG_CB_CALCLQI );
	/* Callback function execution */
	RpCb.rx.lqi = INDIRECT_RpCalcLqiCallback( RpCb.rx.rssi, RP_TRUE );

#ifdef R_FSB_FAN_ENABLED
	if ( RpFsb.enable == RP_FALSE )
	{
#endif // #ifdef R_FSB_FAN_ENABLED

		/* Callback function execution Log */
		RpLog_Event( RP_LOG_CB, RP_LOG_CB_DATAIND );
		/* Callback function execution */
		INDIRECT_RpPdDataIndCallback( pBuf, RpCb.rx.lenNoFcs, RpCb.rx.timeStamp, RpCb.rx.lqi,
									 RpCb.rx.rssi, RpCb.rx.selectedAntenna, RpCb.rx.rssi0, RpCb.rx.rssi1,
									 stOptInd, RpCb.rx.filteredAdress, RpCb.rx.phrRx );
#ifdef R_FSB_FAN_ENABLED
	}
	else
	{
		R_FSB_UplinkCallback( pBuf, RpCb.rx.lenNoFcs, RpCb.rx.lqi );
		RpRelRxBuf( pBuf );
		RpFsb.enable = RP_FALSE;
	}
#endif // #ifdef R_FSB_FAN_ENABLED

	if (warningIndCallbackFlag == RP_TRUE)
	{
		RpCb.rx.pTopRxBuf = RpGetRxBuf();
		if (RpCb.rx.pTopRxBuf == RP_NULL)
		{
			/* Callback function execution Log */
			RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
			/* Callback function execution */
			INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );

			RpRfStat.failToGetRxBuf++;
			RpCb.rx.waitRelRxBufWhenAutoRx = RP_TRUE;
		}
	}
}

/******************************************************************************
Function Name:       RpTrnHdrFunc
Parameters:          none
Return value:        none
Description:         trn interrupt function.
******************************************************************************/
static void RpTrnHdrFunc( void )
{
	uint16_t	status;
	uint8_t stCfm = RP_SUCCESS, framePend = RP_NO_FRMPENBIT_ACK;
	uint8_t trnTimes = 0;
	uint8_t txrxst0, comstat3, ccaTotal;
	uint16_t	sduLen;
	uint8_t interruptDisable = RP_FALSE;

	#if defined(__RX)
	uint32_t bkupPsw2 = 0;
	#elif defined(__arm)
	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
	#else
	uint8_t bkupPsw2 = 0;
	#endif

	uint8_t *pBuf;
	uint8_t warningIndCallbackFlag = RP_FALSE;
	uint8_t stOptInd;
	uint8_t statusRxTimeout;
	uint8_t useRxFcsLength = RP_TRUE;
	uint8_t applyRegulatoryModeLimit = RP_TRUE;

	RpCb.reg.bbIntEn[0] &= (uint8_t)(~(TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN));
	RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
	status = RpCb.status;
	statusRxTimeout = RpCb.statusRxTimeout;

	if (status & RP_PHY_STAT_TX)
	{
		RpCancelTrn(status);
		ccaTotal = RpRegRead(BBCCATOTAL);

		if (RpCb.rx.onReplyingAck == RP_TRUE)
		{
			/** RpAfterTrnEnd (Start) **************************
			***************************************************/
			/* --- --- */
			RpCb.rx.onReplyingAck = RP_FALSE;

			/* enable phyAckWithCca? */
			if ( RpCcaAck.enable )
			{
				RpEndTransmitWithCca();
			}

			/* Apply the limits for Regulatory Mode to Ack */
			if (RpCb.tx.ackLenForRegulatoryMode != 0)
			{
				/* Check Tx Failure */
				if (status & RP_PHY_STAT_TX_CCA)
				{
					txrxst0 = RpRegRead(BBTXRXST0);
					/* CSMA-CA NG */
					if (txrxst0 & CSMACA)
					{
						applyRegulatoryModeLimit = RP_FALSE;
					}
				}
#ifdef R_FSB_FAN_ENABLED
				if (RpCb.tx.isFsbDownlink == RP_TRUE)
				{
					useRxFcsLength = RP_FALSE;
					RpCb.tx.isFsbDownlink = RP_FALSE;
				}
#endif // #ifdef R_FSB_FAN_ENABLED

				/* ToffMin Limit */
				if (RpCb.tx.applyAckToffMin == RP_TRUE)
				{
					if (applyRegulatoryModeLimit == RP_TRUE)
					{
						RpCb.tx.sentTime = RpGetTime();
						RpCb.tx.sentElapsedTimeS = RpCb.tx.regulatoryModeElapsedTimeS;
						RpCb.tx.needTxWaitRegulatoryMode = RP_TRUE;
						if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
						{
							RpCb.tx.sentLen = RpCalcTotalBytes(RpCb.tx.ackLenForRegulatoryMode, &sduLen, useRxFcsLength);
						}
					}
					RpCb.tx.applyAckToffMin = RP_FALSE;
				}

				/* Total Tx time limit */
				if (RpCb.tx.applyAckTotalTxTime == RP_TRUE)
				{
					if (applyRegulatoryModeLimit == RP_TRUE)
					{
						RpUpdateTxTime(1, RP_TRUE, useRxFcsLength);
					}
					RpCb.tx.applyAckTotalTxTime = RP_FALSE;
				}

				RpCb.tx.ackLenForRegulatoryMode = 0;
			}

			if (RpCb.tx.onCsmaCa == RP_TRUE)	// backoffRx
			{
				RpSetAntennaDiversityModeReg();
				RpSetNumberOfCrcBitSwitchBit();	// for TX
			
				RpResumeCsmaTrn(status, ccaTotal, RP_TRUE);
			}
			else
			{
				if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
				{
					;
				}
				else
				{
					if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
					{
						RpCb.status = RP_PHY_STAT_RX;
						RpCb.status |= RP_PHY_STAT_TRX_TO_RX_AUTO;

						if (status & RP_PHY_STAT_RX_TMOUT)
						{
							RpCb.status |= RP_PHY_STAT_RX_TMOUT;
						}

						RpInverseTxAnt(RP_TRUE);

						RpAvailableRcvRamEnable();
						RpChangeFilter();
						RpSetSfdDetectionExtendWrite(RpCb.status);

						/** Interrupt disable Start **/

						/* Disable interrupt */
						#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
						RP_PHY_ALL_DI(bkupPsw2);
						#else
						RP_PHY_ALL_DI();
						#endif
						interruptDisable = RP_TRUE;

						/* --- --- */
						RpRxOnStart();			// rxtrg  and xxxic

						/* Start Antenna select assist */
						RpAntSelAssist_StartProc();
					}
				}
				RpSetAntennaDiversityModeReg();
				RpSetNumberOfCrcBitSwitchBit(); // for TX
			}

			/** RpDataIndCall (Start) **************************
			***************************************************/
			RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
			pBuf = RpCb.rx.pTopRxBuf;

			if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
			{
				if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
				{
					RpSetStateRxOnToTrxOff();
					RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;
				}
				else
				{
					warningIndCallbackFlag = RP_TRUE;
				}
			}
			else if (status & RP_PHY_STAT_RXON_BACKOFF)
			{
				warningIndCallbackFlag = RP_TRUE;
			}
			else	// Single RX
			{
				RpCb.status = RP_PHY_STAT_TRX_OFF;
				if (status & RP_PHY_STAT_RX_TMOUT)
				{
					RpCb.reg.bbIntEn[0] &= ~TIM1INTEN;
					RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
				}
			}

			/* --- --- */
			RpRegBlockRead(BBRSSICCARSLT, (uint8_t *)&RpCb.rx.rssi, sizeof(uint16_t));
			RpCb.rx.rssi -= RpCb.pib.phyRssiOutputOffset;
			RpCb.rx.rssi &= 0x01FF;

			RpGetAntennaDiversityRssi();
			RpGetSelectedAntennaInfo(status);
			RpValidityCheckSFDTimeStamp();

			/** Interrupt disable End **/
			if (interruptDisable == RP_TRUE)
			{
				/* Enable interrupt */
				#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
				RP_PHY_ALL_EI(bkupPsw2);
				#else
				RP_PHY_ALL_EI();
				#endif
			}

			stOptInd = (uint8_t)((RpCb.tx.ackData[0] & 0x10) ? RP_FRMPENBIT_ACK : RP_NO_FRMPENBIT_ACK);

			if ( RpCcaAck.enable )
			{
				stOptInd = RpCcaAck.result;
			}

			RpPdDataIndCallbackPrc(pBuf, stOptInd, warningIndCallbackFlag);

			if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
			{
				if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
				{
					/* Callback function execution Log */
					RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
					/* Callback function execution */
					INDIRECT_RpRxOffIndCallback();
				}
			}
			/** RpDataIndCall (End) ****************************
			***************************************************/
		}
		else
		{
			comstat3 = RpRegRead(BBCOMSTATE3);
			txrxst0 = RpRegRead(BBTXRXST0);
			if (status & RP_PHY_STAT_TX_CCA)
			{
				if (status & RP_PHY_STAT_CCA)
				{
					RpEndTransmitWithCca();
				}

				RpCb.tx.ccaTimes += ccaTotal;
			}
			if ((status & RP_PHY_STAT_TX_CCA) && (txrxst0 & CSMACA))
			{
				// csmaca NG
				if ((RpCb.pib.phyProfileSpecificMode == RP_SPECIFIC_WSUN) &&
						(RpDriverRetryTrn(status) == RP_TRUE))
				{
					return;
				}
				else
				{
					stCfm = RP_BUSY;	// busy channel
				}
			}
			else
			{
				RpCb.tx.sentTime =  RpGetTime();
				RpCb.tx.sentLen = RpCalcTotalBytes((uint16_t)(RpCb.tx.len), &sduLen, RP_FALSE);

				if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
					((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
				{
					RpCb.tx.sentElapsedTimeS = RpCb.tx.regulatoryModeElapsedTimeS;
					RpCb.tx.needTxWaitRegulatoryMode = RP_TRUE;
				}

				if (status & RP_PHY_STAT_TX_UNDERFLOW)
				{
					stCfm = RP_TX_UNDERFLOW;
				}
				else
				{
					if ((status & RP_PHY_STAT_TRX_ACK) && (txrxst0 & TRNRCVSQC))
					{
						// sequence complete NG
						if (RpDriverRetryTrn(status) == RP_TRUE)
						{
							trnTimes += 1;
							RpUpdateTxTime((uint8_t)((comstat3 & RETRNRD) + trnTimes), RP_FALSE, RP_FALSE);	// retry frames + a success frame

							return;
						}
						else
						{
							stCfm = RP_NO_ACK;
						}
					}
					else
					{
						RpAfterTrnEnd(status);

						if ((status & RP_PHY_STAT_TRX_ACK) && (txrxst0 & RCVPEND))
						{
							framePend = RP_FRMPENBIT_ACK;
						}
					}
				}
				trnTimes += 1;
			}
			RpUpdateTxTime((uint8_t)((comstat3 & RETRNRD) + trnTimes), RP_FALSE, RP_FALSE);	// retry frames + a success frame

			/* Callback function execution Log */
			RpLog_Event( RP_LOG_CB, RP_LOG_CB_DATACFM );
			/* Callback function execution */
			INDIRECT_RpPdDataCfmCallback( stCfm, framePend, RpCb.tx.ccaTimes );
		}

		if (RpCb.status & RP_PHY_STAT_TRX_OFF)
		{
			RpCb.rx.waitRelRxBufWhenAutoRx = RP_FALSE;
		}
	}
}

/******************************************************************************
Function Name:       RpCancelTrn
Parameters:          status:current transmit statusus
Return value:        none
Description:         cancel Transimit process.
******************************************************************************/
static void RpCancelTrn( uint16_t status )
{
	RpRxOnStop();
	RpCb.reg.bbTimeCon &= ~COMP0TRG;
	RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
	if (status & RP_PHY_STAT_RXON_BACKOFF)
	{
		RpRelRxBuf(RpCb.rx.pTopRxBuf);
	}
}

/******************************************************************************
Function Name:       RpDriverRetryTrn
Parameters:          status:current transmit statusus
Return value:        none
Description:         Driver Retry Transimit process.
******************************************************************************/
static uint8_t RpDriverRetryTrn( uint16_t status )
{
	uint8_t rtn = RP_FALSE;

	#if defined(__RX)
	uint32_t bkupPsw2;
	#elif defined(__CCRL__) || defined(__ICCRL78__)
	uint8_t  bkupPsw2;
	#elif defined(__arm)
	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
	#endif

	if (status & RP_PHY_STAT_TRX_ACK)
	{
		if (RpCb.tx.rtrnTimes)
		{
			RpCb.tx.rtrnTimes--;
			RpCb.status = status;
			if (status & RP_PHY_STAT_TX_CCA)
			{
				RpInverseTxAnt(RP_TRUE);

				if (status & RP_PHY_STAT_RXON_BACKOFF)
				{
					RpCb.rx.pTopRxBuf  = RpGetRxBuf();
					if (RpCb.rx.pTopRxBuf == RP_NULL)
					{
						/* Callback function execution Log */
						RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
						/* Callback function execution */
						INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );

						RpRfStat.failToGetRxBuf++;
						return (rtn);
					}
					RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
					RpAvailableRcvRamEnable();
				}

				RpCb.tx.onCsmaCa = RP_TRUE;
				RpSetMaxCsmaBackoffVal();
				RpRegWrite(BBCSMACON3, RpCb.pib.macMinBe);
				RpCb.tx.ccaTimesOneFrame = 0;
			}
			else
			{
				RpInverseTxAnt(RP_FALSE);
				RpCb.tx.onCsmaCa = RP_FALSE;
			}

			/* Disable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
			RP_PHY_ALL_DI(bkupPsw2);
			#else
			RP_PHY_ALL_DI();
			#endif

			if (RpSetTxTriggerTimer(RpGetTime(), RP_TRUE, 0x00) == RP_TRUE)	// TX trigger
			{
				RpCb.status &= ~RP_PHY_STAT_BUSY;
				RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;
			}
			else
			{
				RpCb.reg.bbCsmaCon0 |= CSMAST;
				RpRegWrite(BBCSMACON0, RpCb.reg.bbCsmaCon0);	// TX trigger
				RpCb.status |= RP_PHY_STAT_BUSY;
			}
			RpCb.reg.bbIntEn[0] |= (TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN);
			RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
			RpCb.tx.cnt = 0x00;

			if ((status & (RP_PHY_STAT_TX_CCA | RP_PHY_STAT_RXON_BACKOFF)) == (RP_PHY_STAT_TX_CCA | RP_PHY_STAT_RXON_BACKOFF) || (RpCb.tx.len > (RP_BB_TX_RAM_SIZE * 2)))
			{
#if defined (__ICCRL78__)
				RpCb.tx.pNextData = RpCb.tx.pOrgData;
#else
				RpCb.tx.pNextData = RpCb.tx.data;
#endif
				RpTrnxHdrFunc(RP_PHY_BANK_0);
				RpTrnxHdrFunc(RP_PHY_BANK_1);
			}

			/* Enable interrupt */
			#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
			RP_PHY_ALL_EI(bkupPsw2);
			#else
			RP_PHY_ALL_EI();
			#endif

			rtn = RP_TRUE;
		}
	}

	return (rtn);
}

/******************************************************************************
Function Name:       RpAfterTrnEnd
Parameters:          status:current transmit statusus
Return value:        none
Description:         process after finishing trn interrupt handler for Auto Tx->Rx.
******************************************************************************/
static void RpAfterTrnEnd( uint16_t status )
{
	if ((status & RP_PHY_STAT_TRX_TO_RX_AUTO) && (RpCb.tx.onCsmaCa == RP_FALSE))
	{
		if (RpCb.rx.onReplyingAck == RP_FALSE)
		{
			RpCb.rx.pTopRxBuf  = RpGetRxBuf();
			if (RpCb.rx.pTopRxBuf == RP_NULL)
			{
				/* Callback function execution Log */
				RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_NOTGETBUF );
				/* Callback function execution */
				INDIRECT_WarningIndCallback( RP_WARN_NOT_GET_RXBUF );

				RpRfStat.failToGetRxBuf++;
				RpCb.rx.waitRelRxBufWhenAutoRx = RP_TRUE;
			}
		}
		RpCb.status = RP_PHY_STAT_RX;
		if (((RpCb.pib.macAddressFilter1Ena == RP_TRUE) || (RpCb.pib.macAddressFilter2Ena == RP_TRUE))
				&& (RpCb.rx.softwareAdf == RP_FALSE))
		{
			RpCb.reg.bbTxRxMode0 |= AUTOACKEN;
			RpCb.reg.bbTxRxMode3 |= ADRSFILEN;
			RpRegWrite(BBTXRXMODE3, (uint8_t)(RpCb.reg.bbTxRxMode3));
			if (status & RP_PHY_STAT_SLOTTED)
			{
				RpCb.status |= RP_PHY_STAT_SLOTTED;
			}
		}
		RpCb.status |= RP_PHY_STAT_TRX_TO_RX_AUTO;

		if (status & RP_PHY_STAT_RX_TMOUT)
		{
			RpCb.status |= RP_PHY_STAT_RX_TMOUT;
		}
		RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
		RpCb.tx.onCsmaCa = RP_FALSE;

		if (RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE)
		{
			RpSetCcaDurationVal(RP_FALSE);
		}
		RpInverseTxAnt(RP_TRUE);

		RpAddressFilterSetting((uint8_t)(RpCb.pib.macAddressFilter1Ena | RpCb.pib.macAddressFilter2Ena));
		RpCb.reg.bbTxRxMode1 |= CCASEL; // RSSI
		RpRegWrite(BBTXRXMODE1, (uint8_t)(RpCb.reg.bbTxRxMode1));
		RpAvailableRcvRamEnable();

		RpChangeFilter();
		RpSetSfdDetectionExtendWrite(RpCb.status);

		RpRxOnStart();			// rxtrg  and xxxic

		/* Start Antenna select assist */
		RpAntSelAssist_StartProc();
	}
}

/******************************************************************************
Function Name:	 RpResumeCsmaTrn
Parameters: 		 status : RF status
Return value:		 none
Description:		 Resume Transmit function.
******************************************************************************/
static void RpResumeCsmaTrn( uint16_t status, uint8_t ccaTotal, uint8_t rxEna )
{
	uint16_t txFlen;
	uint8_t csmacon1, csmacon3;

	RpCb.status = status;
	if (status & RP_PHY_STAT_TRX_ACK)
	{
		RpRegWrite(BBTXRXCON, ACKRCVEN);
	}
	else
	{
		RpRegWrite(BBTXRXCON, 0x00);
	}

	RpInverseTxAnt(RP_TRUE);

	RpCb.reg.bbIntEn[0] |= (TRNFININTEN | TRN0INTEN | TRN1INTEN | CCAINTEN);
	RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
	RpCb.tx.cnt = 0x00;
#if defined (__ICCRL78__)
	RpCb.tx.pNextData = RpCb.tx.pOrgData;
#else
	RpCb.tx.pNextData = RpCb.tx.data;
#endif
	RpTrnxHdrFunc(RP_PHY_BANK_0);
	RpTrnxHdrFunc(RP_PHY_BANK_1);
	txFlen = RpCb.tx.len + RpCb.pib.phyFcsLength;
	RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));
	RpCb.tx.ccaTimes += ccaTotal;
	RpCb.tx.ccaTimesOneFrame += ccaTotal;
	csmacon1 = RpCb.reg.bbCsmaCon1;
	csmacon1 &= (~NB);
	if (RpCb.pib.macMaxCsmaBackOff >= RpCb.tx.ccaTimesOneFrame)
	{
		csmacon1 |= (RpCb.pib.macMaxCsmaBackOff - RpCb.tx.ccaTimesOneFrame);
	}
	RpCb.reg.bbCsmaCon1 = csmacon1;
	RpRegWrite(BBCSMACON1, RpCb.reg.bbCsmaCon1);
	csmacon3 = RpRegRead(BBCSMACON3);
	csmacon3 &= (~BEMIN);
	csmacon3 |= (RpCb.tx.ccaTimesOneFrame + RpCb.pib.macMinBe);
	if (csmacon3 > RpCb.pib.macMaxBe)
	{
		csmacon3 = RpCb.pib.macMaxBe;
	}
	RpRegWrite(BBCSMACON3, csmacon3);
	RpCb.reg.bbCsmaCon0 |= CSMAST;
	if(rxEna == RP_FALSE)
	{
		RpCb.reg.bbCsmaCon0 &= ~CSMARCVEN;
		RpCb.status &= ~RP_PHY_STAT_RXON_BACKOFF;
		RpRelRxBuf(RpCb.rx.pTopRxBuf);
	}
	RpRegWrite(BBCSMACON0, RpCb.reg.bbCsmaCon0);	// TX trigger

	#if defined(__arm)
	RpLedTxOff();
	#endif
}

/* "Interrupt source number 5" */
/***************************************************************************************************************
 * function name  : RpTrn0Hdr
 * description    : Bank0 transmit completion interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpTrn0Hdr( void )
{
	RpTrnxHdrFunc(RP_PHY_BANK_0);
}

/* "Interrupt source number 6" */
/***************************************************************************************************************
 * function name  : RpTrn1Hdr
 * description    : Bank1 transmit completion interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpTrn1Hdr( void )
{
	RpTrnxHdrFunc(RP_PHY_BANK_1);
}

/* "Interrupt source number 11" */
/***************************************************************************************************************
 * function name  : RpRcv0Hdr
 * description    : Bank0 receive completion interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRcv0Hdr( void )
{
	RpRcvHdrFunc(RP_PHY_BANK_0);
}

/* "Interrupt source number 12" */
/***************************************************************************************************************
 * function name  : RpRcv1Hdr
 * description    : Bank1 receive completion interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRcv1Hdr( void )
{
	RpRcvHdrFunc(RP_PHY_BANK_1);
}

/***********************************************************************
 * function name  : RpStartTransmitWithCca
 * description    : 
 * parameters     : none
 * return value   : none
 **********************************************************************/
void RpStartTransmitWithCca( void )
{
	uint8_t wk;
	uint16_t wk16;

	/* Save the current value */
	RpCb.pib.backup_macMaxCsmaBackOff    = RpCb.pib.macMaxCsmaBackOff;
	RpCb.pib.backup_macMinBe             = RpCb.pib.macMinBe;
	RpCb.pib.backup_phyCsmaBackoffPeriod = RpCb.pib.phyCsmaBackoffPeriod;
	RpCb.pib.backup_phyCcaDuration       = RpCb.pib.phyCcaDuration;

	/* Set the value for ACK transmission */
	RpCb.pib.macMaxCsmaBackOff    = 0x00;
	RpCb.pib.macMinBe             = 0x00;

	switch( RpCb.pib.phyDataRate )
	{
		case 10:
		case 20:
			wk = 0x00;
			break;
		case 40:
			if (RpCb.pib.phyCcaDuration < 0x12) {
				wk = 0x12;
			} else {
				wk = 0x00; }
			break;
		case 50:
			if (RpCb.pib.phyCcaDuration < 0x16) {
				wk = 0x16; }
			else {
				wk = 0x00; }
			break;
		case 100:
			if (RpCb.pib.phyCcaDuration < 0x28) {
				wk = 0x28; }
			else {
				wk = 0x00; }
			break;
		case 150:
			if (RpCb.pib.phyCcaDuration < 0x3A) {
				wk = 0x3A; }
			else {
				wk = 0x00; }
			break;
		case 200:
			if (RpCb.pib.phyCcaDuration < 0x4D) {
				wk = 0x4D; }
			else {
				wk = 0x00; }
			break;
		case 300:
			if (RpCb.pib.phyCcaDuration < 0x6E) {
				wk = 0x6E; }
			else {
				wk = 0x00; }
			break;
		case 400:
			if (RpCb.pib.phyCcaDuration < 0x96) {
				wk = 0x96; }
			else {
				wk = 0x00; }
			break;
		default:
			wk = 0x00;
			break;
	}

	if ( wk != 0x00 )
	{
		wk16 = (RpCb.pib.phyCcaDuration+1) / 2;
		RpCb.pib.phyCsmaBackoffPeriod = wk16 + (wk / 2);
	}
	else
	{
		RpCb.pib.phyCsmaBackoffPeriod = 0x10;
	}

	/* Set the value in the register */
	/* macMaxCsmaBackOff */
	/* === BBCSMACON1(NB) === */
	RpSetMaxCsmaBackoffVal();

	/* macMinBe */
	/* === BBCSMACON3 === */
	RpCb.pib.macMinBe &= BEMIN;		// b0-b3: BEMIN bit (Sets the macMinBE value.)
	RpRegWrite( BBCSMACON3, RpCb.pib.macMinBe );

	/* phyCsmaBackoffPeriod */
	/* === BBBOFFPERIOD === */
	RpSetCsmaBackoffPeriod();

	/* phyCcaDuration */
	RpSetCcaDurationVal(RP_TRUE);
}

/***********************************************************************
 * function name  : RpEndTransmitWithCca
 * description    : 
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RpEndTransmitWithCca( void )
{
	RpCb.pib.macMaxCsmaBackOff    = RpCb.pib.backup_macMaxCsmaBackOff;
	RpCb.pib.macMinBe             = RpCb.pib.backup_macMinBe;
	RpCb.pib.phyCsmaBackoffPeriod = RpCb.pib.backup_phyCsmaBackoffPeriod;
	RpCb.pib.phyCcaDuration       = RpCb.pib.backup_phyCcaDuration;

	/* Set the value in the register */
	/* macMaxCsmaBackOff */
	RpSetMaxCsmaBackoffVal();

	/* macMinBe */
	/* === BBCSMACON3 === */
	RpCb.pib.macMinBe &= BEMIN;		// b0-b3: BEMIN bit (Sets the macMinBE value.)
	RpRegWrite( BBCSMACON3, RpCb.pib.macMinBe );

	/* phyCsmaBackoffPeriod */
	RpSetCsmaBackoffPeriod();

	/* phyCcaDuration */
	RpSetCcaDurationVal(RP_TRUE);
}

/***************************************************************************************************************
 * function name  : RpStateRx_SetTxTrigger
 * description    : 
 * parameters     : useCca...
 * return value   : none
 **************************************************************************************************************/
static void RpStateRx_SetTxTrigger( uint8_t useCca )
{
	if ( useCca == RP_FALSE )
	{
		RpRegWrite(BBTXRXCON, TRNTRG); // TX trigger without CCA
		RpCb.status |= RP_PHY_STAT_BUSY;
	}
	else
	{
		/* === BBTXRXMODE4 === */
		RpCb.reg.bbTxRxMode4 = 0x00;	// init
		RpCb.reg.bbTxRxMode4 |= CCAINTSEL;		// b0: Upon completion of the CSMA-CA sequence
	//	RpCb.reg.bbTxRxMode4 &= ~INTOUTSEL;		// b4: When with the interrupt request = INTOUT pin is at high level
	//	RpCb.reg.bbTxRxMode4 &= ~TIMEOUTRCV;	// b6: Automatic normal reception mode
	//	RpCb.reg.bbTxRxMode4 &= ~TIMEOUTINTSEL;	// b7: Byte receive interrupt
		RpRegWrite( BBTXRXMODE4, RpCb.reg.bbTxRxMode4 );

		/* === BBCSMACON0 === */
		RpCb.reg.bbCsmaCon0 = 0x00;		// init
	//	RpCb.reg.bbCsmaCon0 &= ~UNICASTFRM;		// b4: Prohibition of automatic ack reception
	//	RpCb.reg.bbCsmaCon0 &= ~CSMARCVEN;		// b2: Reception during CSMA-CA function disable
		RpCb.reg.bbCsmaCon0 |= CSMATRNST;		// b1: Transmit after CSMA-CA process
		RpCb.reg.bbCsmaCon0 |= CSMAST;			// b0: Automatic CSMA-CA start
		RpRegWrite( BBCSMACON0, RpCb.reg.bbCsmaCon0 );	// TX trigger with CCA (if the timer trigger is off)
	}
}

/* "Interrupt source number 10" */
/***************************************************************************************************************
 * function name  : RpRcvFinHdr
 * description    : Frame receive completion interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRcvFinHdr( void )
{
	uint16_t	status;
	uint8_t		statusRxTimeout;
	uint8_t	stOptInd;
	uint8_t	rcvRamSt;
	uint16_t txFlen;
	uint16_t txFlenBk;
	uint8_t errCd, errCdAnd;
	uint8_t rcvRamSt2;
	uint32_t nowTime, difTime;
	uint32_t willTxTime;
	uint32_t toffMinTime;
	uint8_t antTxSel, antDvrCon;
	uint8_t txrxst0;
	uint32_t ackTxTrigerLimitTimeBit;
	uint8_t ackTxTrigerFlag = RP_TRUE;
	uint16_t dummy;
	uint8_t interruptDisable = RP_FALSE;
	uint8_t useRxFcsLength = RP_TRUE;

	#if defined(__RX)
	uint32_t bkupPsw2;
	#elif defined(__CCRL__) || defined(__ICCRL78__)
	uint8_t  bkupPsw2;
	#elif defined(__arm)
	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
	#endif

	uint8_t *pBuf;
	uint8_t warningIndCallbackFlag = RP_FALSE;
#ifdef R_FSB_FAN_ENABLED
	uint8_t res;
#endif // #ifdef R_FSB_FAN_ENABLED

	RpCb.reg.bbIntReq = 0;
	status = RpCb.status;
	statusRxTimeout = RpCb.statusRxTimeout;

	// receive mode
	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
	{
		if (RpCb.rx.cnt != RP_PHY_RX_STAT_INIT)
		{
			txrxst0 = RpRegRead(BBTXRXST0);
			if (RpChkCrcStatus(&stOptInd, txrxst0) == RP_ERROR)
			{
				RpRxEndSetting(status);
			}
			else
			{
#ifdef R_FSB_FAN_ENABLED
				RpFsb.enable = RP_FALSE;	// init
#endif // #ifdef R_FSB_FAN_ENABLED
				RpCcaAck.enable = RP_FALSE;	// init

				if ( RpCb.rx.softwareAdf )
				{
					RpCb.rx.rcvRamSt = (uint8_t)((txrxst0 & RCVRAMST) ? 0x00 : RCVRAMST);
					RpReadAddressInfo();

#ifdef R_FSB_FAN_ENABLED
					res = R_FSB_DownlinkCallback( RpCb.rx.pTopRxBuf, (uint16_t)RpCb.rx.lenNoFcs, &(RpFsb.resData[0]), (uint16_t *)&txFlen );
					switch ( res )
					{
						case R_FSB_INVALID_FRAME:
							errCd = RP_INVALID_FRAME;
							break;
						case R_FSB_VALID_FRAME:
							errCd = RP_VALID_FRAME;
							RpFsb.enable = RP_TRUE;
							break;
						case R_FSB_NEED_RESPONSE:
							errCd = (RP_VALID_FRAME | RP_NEED_ACK);
							RpFsb.enable = RP_TRUE;
							useRxFcsLength = RP_FALSE;
							break;
						default:
							/* Callback function execution Log */
							RpLog_Event( RP_LOG_CB, RP_LOG_CB_ACKCHECK );
							/* Callback function execution */
							errCd = INDIRECT_RpAckCheckCallback( RpCb.rx.pTopRxBuf, (uint16_t)RpCb.rx.lenNoFcs, RpCb.tx.ackData, (uint16_t *)&txFlen );
							break;
					}
#else  // #ifdef R_FSB_FAN_ENABLED
					/* Callback function execution Log */
					RpLog_Event( RP_LOG_CB, RP_LOG_CB_ACKCHECK );
					/* Callback function execution */
					errCd = INDIRECT_RpAckCheckCallback( RpCb.rx.pTopRxBuf, (uint16_t)RpCb.rx.lenNoFcs, RpCb.tx.ackData, (uint16_t *)&txFlen );
#endif // #ifdef R_FSB_FAN_ENABLED

#ifdef R_FSB_FAN_ENABLED
					if (( RpCb.pib.phyAckWithCca )||( RpFsb.enable == RP_TRUE ))
#else
					if ( RpCb.pib.phyAckWithCca )
#endif // #ifdef R_FSB_FAN_ENABLED
					{
						RpCcaAck.enable = RP_TRUE;
					}

					if (errCd & (RP_VALID_FRAME | RP_VALID_FRAME2))
					{
						errCdAnd = (uint8_t)(errCd & (RP_VALID_FRAME | RP_VALID_FRAME2));
						if (errCdAnd == RP_VALID_FRAME)
						{
							RpCb.rx.filteredAdress = 0x01;
						}
						else if (errCdAnd == RP_VALID_FRAME2)
						{
							RpCb.rx.filteredAdress = 0x02;
						}
						else if (errCdAnd == (RP_VALID_FRAME | RP_VALID_FRAME2))
						{
							RpCb.rx.filteredAdress = 0x03;
						}
						else
						{
							RpCb.rx.filteredAdress = 0x00;
						}
						if ((stOptInd & RP_CRC_BAD) == 0)
						{
							if ((errCd & (RP_NEED_ACK | RP_NEED_ACK2)) && (((RpCb.reg.bbIntReq = (uint32_t)RpRegRead(BBINTREQ0)) & RP_BBCCA_IREQ) == 0))
							{
								if (RpCb.tx.onCsmaCa == RP_TRUE)
								{
									RpRegWrite(BBTXRXRST, RFSTOP);		// RF Stop
								}

								/* Disable interrupt */
								#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
								RP_PHY_ALL_DI(bkupPsw2);
								#else
								RP_PHY_ALL_DI();
								#endif

								/* Begin ACK Transmit */
								nowTime = RpGetTime();
								if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
								{
									if ( RpCcaAck.enable ) // Ack with CCA or R_FSB_DownlinkCallback
									{
										if (((RpCb.pib.phyCurrentChannel >= RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE) && (RpCheckLongerThanTotalTxTime(txFlen, 0x00, useRxFcsLength) == RP_TRUE)) || // Total Tx Time
											(RpExtChkErrFrameLen(txFlen, useRxFcsLength) == RP_TRUE)) // TonMax
										{
											ackTxTrigerFlag = RP_FALSE;
										}
										else
										{
											RpCb.tx.ackLenForRegulatoryMode = txFlen;
											RpCb.tx.applyAckToffMin = RP_TRUE;
											toffMinTime = (RpCb.tx.sentTime + RpCalcTxInterval(RpCb.tx.sentLen, RP_FALSE) - RP_PHY_TX_WARM_UP_TIME) & RP_TIME_MASK;
											if (RpCb.pib.phyCurrentChannel >= RP_PHY_MIN_CHANNEL_VALID_ARIB_MODE)
											{
												RpCb.tx.applyAckTotalTxTime = RP_TRUE;
#ifdef R_FSB_FAN_ENABLED
												if ( RpFsb.enable == RP_TRUE ) // R_FSB_DownlinkCallback
												{
													RpCb.tx.isFsbDownlink = RP_TRUE;
												}
#endif // #ifdef R_FSB_FAN_ENABLED
											}
										}
									}
									else // Ack without CCA
									{
										/* Check the conditions of response to skip carrier sense in ARIB. */
										ackTxTrigerLimitTimeBit = RpCb.tx.ackCompLimitTimeBit - RP_ACKTX_WARMUP_TIME - (RpCalcTotalBytes(txFlen, &dummy, useRxFcsLength) << 3);
										if (((ackTxTrigerLimitTimeBit - RpCb.rx.timeAcKRtn) > RP_TIME_LIMIT) ||
											((ackTxTrigerLimitTimeBit - nowTime) > RP_TIME_LIMIT))
										{
											ackTxTrigerFlag = RP_FALSE;
										}
									}
								}
								else if (RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE)
								{
									if ((RpCheckLongerThanTotalTxTime(txFlen, 0x00, useRxFcsLength) == RP_TRUE) ||	// Total Tx Time
										(RpExtChkErrFrameLen(txFlen, useRxFcsLength) == RP_TRUE))					// TonMax
									{
										ackTxTrigerFlag = RP_FALSE;
									}
									else
									{
										RpCb.tx.ackLenForRegulatoryMode = txFlen;
										RpCb.tx.applyAckTotalTxTime = RP_TRUE;
#ifdef R_FSB_FAN_ENABLED
										if ( RpFsb.enable == RP_TRUE ) // R_FSB_DownlinkCallback
										{
											RpCb.tx.applyAckToffMin = RP_TRUE;
											toffMinTime = (RpCb.tx.sentTime + ((uint32_t)(RpCb.pib.phyRmodeToffMin) * (uint32_t)(RpCb.pib.phyDataRate)) - RP_PHY_TX_WARM_UP_TIME) & RP_TIME_MASK;
											RpCb.tx.isFsbDownlink = RP_TRUE;
										}
#endif // #ifdef R_FSB_FAN_ENABLED
									}
								}

								if ( ackTxTrigerFlag == RP_TRUE )
								{
									/* update status */
									RpCb.status |= RP_PHY_STAT_TX;	// TX

									/* enable phyAckWithCca? */
									RpCcaAck.result = RP_NO_FRMPENBIT_ACK; //RP_SUCCESS;

									/* CCA-ACK enabled? */
									if ( RpCcaAck.enable )
									{
										/* update status */
										RpCb.status |= RP_PHY_STAT_TX_CCA;	// TX with CCA

										RpStartTransmitWithCca();

										/* Change Narrow Band Filter */
										if( RpCb.pib.phyCcaBandwidth != RP_PHY_CCA_BANDWIDTH_INDEX_225K )
										{
											RpChangeNarrowBandFilter();
										}
									}

									/* Select a more future time from timeAcKRtn and toffminTime for the Tx time */
									willTxTime = RpCb.rx.timeAcKRtn;
									if (RpCb.tx.applyAckToffMin == RP_TRUE)
									{
										difTime = (toffMinTime - RpCb.rx.timeAcKRtn) & RP_TIME_MASK;
										if (difTime <= RP_TIME_LIMIT)
										{
											willTxTime = toffMinTime;
										}
									}

									difTime = (willTxTime - nowTime) & RP_TIME_MASK;
									if ((difTime <= RP_TIME_LIMIT) && (difTime >= (uint32_t)RpTc0WasteTime[RpCb.freqIdIndex])) // (willTxTime >= nowTime) and (willTxTime - nowTime >= RpTc0WasteTime)
									{
										/************/
										RpTC0SetReg( RpCcaAck.enable, willTxTime ); // TX timer trigger with/without CCA
										/************/
										RpCb.status |= RP_PHY_STAT_WAIT_TMRTRG;          // TX waiting timer trigger

										nowTime = RpGetTime();
										difTime = (willTxTime - nowTime) & RP_TIME_MASK;
										if ((difTime > RP_TIME_LIMIT) || (difTime == 0)) // willTxTime < nowTime
										{
											if (RpCheckRfIRQ() == RP_FALSE)
											{
												/* Cancel timer trigger */
												RpCb.reg.bbIntEn[0] &= ~TIM0INTEN;
												RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
												RpCb.reg.bbTimeCon &= ~(COMP0TRG | COMP0TRGSEL);
												RpRegWrite(BBTIMECON, RpCb.reg.bbTimeCon);
												RpCb.status &= ~RP_PHY_STAT_WAIT_TMRTRG; // remove TX waiting timer trigger

												/* Set TX trigger with/without CCA */
												RpStateRx_SetTxTrigger( RpCcaAck.enable );

												#if defined(__arm)
												RpLedTxOff();
												#endif
											}
										}
									}
									else
									{
										if (difTime <= RP_TIME_LIMIT) // (willTxTime >= nowTime) and (willTxTime - nowTime < RpTc0WasteTime)
										{
											while (((willTxTime - RpGetTime()) & RP_TIME_MASK) <= RP_TIME_LIMIT); // wait for RpTc0WasteTime
										}

										/* Set TX trigger with/without CCA */
										RpStateRx_SetTxTrigger( RpCcaAck.enable );
									}

									#if defined(__arm)
									RpLedTxOff();
									#endif

/******************************************************************************************************
******************************************************************************************************/

									RpSetSfdDetectionExtendWrite(RpCb.status);

									txFlen = (uint16_t)(txFlen + RpCb.rx.fcsLength);

									if (RpCb.rx.fcsLength != RpCb.pib.phyFcsLength)
									{
										if (RpCb.rx.fcsLength == RP_FCS_LENGTH_16BIT)
										{
											RpCb.reg.bbSubCon |= CRCBIT;
										}
										else if (RpCb.rx.fcsLength == RP_FCS_LENGTH_32BIT)
										{
											RpCb.reg.bbSubCon &= ~CRCBIT;
										}
										RpRegWrite(BBSUBGCON, (uint8_t)(RpCb.reg.bbSubCon));
									}
									RpRegBlockWrite(BBTXFLEN, (uint8_t RP_FAR *)&txFlen, sizeof(uint16_t));

									if (RpCb.pib.phyAntennaDiversityRxEna)
									{
										RpCb.rx.selectedAntenna = RpDetRcvdAntenna();
										switch (RpCb.pib.phyAntennaSelectAckTx)
										{
											case 0x00:
												antTxSel = RP_PHY_ANTENNA_0;
												break;
											case 0x01:
												antTxSel = RP_PHY_ANTENNA_1;
												break;
											case 0x02:
												antTxSel = RpCb.rx.selectedAntenna;
												break;
											case 0x03:
												if (RpCb.rx.selectedAntenna == RP_PHY_ANTENNA_0)
												{
													antTxSel = RP_PHY_ANTENNA_1;
												}
												else
												{
													antTxSel = RP_PHY_ANTENNA_0;
												}
												break;
										}
										antDvrCon = RpRegRead(BBANTDIV);
										RpCb.rx.bkupAckAnt = (uint8_t)(antDvrCon & ANTSWTRNSET);
										antDvrCon &= (~ANTSWTRNSET);
										if (antTxSel)
										{
											antDvrCon |= ANTSWTRNSET;
										}
										RpRegWrite(BBANTDIV, antDvrCon);
									}
									else
									{
										RpInverseTxAnt(RP_FALSE);
									}

									RpCb.rx.onReplyingAck = RP_TRUE;
									RpCb.reg.bbIntEn[0] &= ~CCAINTEN;

									if ( RpCcaAck.enable )
									{
										RpCb.reg.bbIntEn[0] |= CCAINTEN;
									}

									RpCb.reg.bbIntEn[0] |= (TRNFININTEN | TRN0INTEN | TRN1INTEN);
									RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
									RpCb.status |= (RP_PHY_STAT_TX | RP_PHY_STAT_BUSY);
									RpCb.tx.cnt = 0x00;
#ifdef R_FSB_FAN_ENABLED
									if ( RpFsb.enable == RP_TRUE )
									{
										RpCb.tx.pNextData = &(RpFsb.resData[0]);
									}
									else
									{
										RpCb.tx.pNextData = RpCb.tx.ackData;
									}
#else
									RpCb.tx.pNextData = RpCb.tx.ackData;
#endif // #ifdef R_FSB_FAN_ENABLED
									txFlenBk = RpCb.tx.len;
									RpCb.tx.len = txFlen - RpCb.rx.fcsLength;
									RpTrnxHdrFunc(RP_PHY_BANK_0);
									RpTrnxHdrFunc(RP_PHY_BANK_1);
									if (statusRxTimeout & RP_STATUS_RX_TIMEOUT_EXTENTION)
									{
										RpCb.reg.bbIntEn[0] &= ~TIM1INTEN;
										RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
									}

									/* Disable interrupt */
									#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
									RP_PHY_ALL_EI(bkupPsw2);
									#else
									RP_PHY_ALL_EI();
									#endif

									RpCb.tx.len = txFlenBk;
									rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);

									if (RpCb.rx.unreadRamPrev == RP_TRUE)
									{
										RpCb.rx.unreadRamPrev = RP_FALSE;
										if (rcvRamSt)
										{
											rcvRamSt2 = 0x00;
										}
										else
										{
											rcvRamSt2 = 0x80;
										}
										RpReadRam(rcvRamSt2);
									}
									RpReadRam(rcvRamSt);

									return;
								}
								else
								{
									/* Enable interrupt */
									#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
									RP_PHY_ALL_EI(bkupPsw2);
									#else
									RP_PHY_ALL_EI();
									#endif

									RpRxEndSetting(status);

									/* Callback function execution Log */
									RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_TXACKSTOP );
									/* Callback function execution */
									INDIRECT_WarningIndCallback( RP_WARN_TX_ACK_STOP );
#ifdef R_FSB_FAN_ENABLED
									RpFsb.enable = RP_FALSE;
#endif // #ifdef R_FSB_FAN_ENABLED
									RpCcaAck.enable = RP_FALSE;
									return;
								}
							}
						}

#ifdef R_FSB_FAN_ENABLED
//						g_SnBridge = RP_FALSE;
#endif // #ifdef R_FSB_FAN_ENABLED

						rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);
						if (RpCb.rx.unreadRamPrev == RP_TRUE)
						{
							RpCb.rx.unreadRamPrev = RP_FALSE;
							if (rcvRamSt)
							{
								rcvRamSt2 = 0x00;
							}
							else
							{
								rcvRamSt2 = 0x80;
							}
							RpReadRam(rcvRamSt2);
						}
						RpReadRam(rcvRamSt);

						if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
						{
							;
						}
						else
						{
							if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
							{
								/* Disable interrupt */
								#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
								RP_PHY_ALL_DI(bkupPsw2);
								#else
								RP_PHY_ALL_DI();
								#endif
								interruptDisable = RP_TRUE;

								RpRxOnStart();		// RX trigger

								/* Start Antenna select assist */
								RpAntSelAssist_ReStartProc();
							}
						}
					}
					else
					{
#ifdef R_FSB_FAN_ENABLED
						RpFsb.enable = RP_FALSE;
#endif // #ifdef R_FSB_FAN_ENABLED
						RpCcaAck.enable = RP_FALSE;
						RpRxEndSetting(status);

						return;
					}
				}
				else
				{
					if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
					{
						;
					}
					else
					{
						if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
						{
							/* Disable interrupt */
							#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
							RP_PHY_ALL_DI(bkupPsw2);
							#else
							RP_PHY_ALL_DI();
							#endif
							interruptDisable = RP_TRUE;

							RpRxOnStart();		// RX trigger

							/* Start Antenna select assist */
							RpAntSelAssist_ReStartProc();
						}
					}

					rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);
					RpReadRam(rcvRamSt);
				}

				/** RpDataIndCall (Start) **************************
				***************************************************/
				RpCcaAck.enable = RP_FALSE;

				RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;
				pBuf = RpCb.rx.pTopRxBuf;

				if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
				{
					if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
					{
						RpSetStateRxOnToTrxOff();
						RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;
					}
					else
					{
						warningIndCallbackFlag = RP_TRUE;
					}
				}
				else if (status & RP_PHY_STAT_RXON_BACKOFF)
				{
					warningIndCallbackFlag = RP_TRUE;
				}
				else	// Single RX
				{
					RpCb.status = RP_PHY_STAT_TRX_OFF;
					if (status & RP_PHY_STAT_RX_TMOUT)
					{
						RpCb.reg.bbIntEn[0] &= ~TIM1INTEN;
						RpRegWrite(BBINTEN0,	(uint8_t)(RpCb.reg.bbIntEn[0]));
					}
				}

				RpRegBlockRead(BBRSSICCARSLT, (uint8_t *)&RpCb.rx.rssi, sizeof(uint16_t));
				RpCb.rx.rssi -= RpCb.pib.phyRssiOutputOffset;
				RpCb.rx.rssi &= 0x01FF;

				RpGetAntennaDiversityRssi();
				RpGetSelectedAntennaInfo(status);
				RpValidityCheckSFDTimeStamp();

				if (interruptDisable == RP_TRUE)
				{
					/* Enable interrupt */
					#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
					RP_PHY_ALL_EI(bkupPsw2);
					#else
					RP_PHY_ALL_EI();
					#endif
				}

				RpPdDataIndCallbackPrc(pBuf, stOptInd, warningIndCallbackFlag);

				if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
				{
					if (status & RP_PHY_STAT_TRX_TO_RX_AUTO)
					{
						/* Callback function execution Log */
						RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
						/* Callback function execution */
						INDIRECT_RpRxOffIndCallback();
					}
				}
				/** RpDataIndCall (End) ****************************
				***************************************************/
			}
		}
		else
		{
			RpRxEndSetting(status);
		}
	}
}

/******************************************************************************
Function Name:       RpChkCrcStatus
Parameters:          pData:The pointer of dest copy
					 len:The pointer of received length
					 tmStmp:The pointer of timestamp
					 lqi:The pointer of LQI
					 pStatOpt:The pointer of status
Return value:        0:normal received
                     1:crc error ocuur
Description:         Recieved frame analize.
******************************************************************************/
static uint8_t RpChkCrcStatus(uint8_t *pStatOpt, uint8_t txrxst0)
{
	uint8_t rtn = RP_SUCCESS;
	uint8_t	crcRslt = (uint8_t)(txrxst0 & CRC);

	if (pStatOpt != RP_NULL)
	{
		*pStatOpt = RP_NO_FRMPENBIT_ACK;
		if ((crcRslt == 0) && (RpCb.rx.pTopRxBuf != RP_NULL))
		{
			if (((RpCb.pib.macAddressFilter1Ena == RP_TRUE) || (RpCb.pib.macAddressFilter2Ena == RP_TRUE))
					&& (RpCb.rx.softwareAdf == RP_FALSE))
			{
				if (RpRegRead(BBTXRXMODE2) & FLMPENDST)
				{
					*pStatOpt = RP_FRMPENBIT_ACK;
				}
			}
		}
		else
		{
			if ((crcRslt) && (RpCb.pib.phyCrcErrorUpMsg == RP_RX_CRCERROR_UPMSG) && (RpCb.rx.pTopRxBuf != RP_NULL))
			{
				*pStatOpt = RP_CRC_BAD;
			}
			else
			{
				rtn = RP_ERROR;
			}
		}
	}

	return (rtn);
}

/******************************************************************************
Function Name:       RpRdxEndSetting
Parameters:          current status
Return value:        none
Description:         Rx End Setting.
******************************************************************************/
static void RpRxEndSetting( uint16_t status )
{
	uint8_t statusRxTimeout;
	statusRxTimeout = RpCb.statusRxTimeout;

	RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;

	RpAvailableRcvRamEnable();

	if (statusRxTimeout & (RP_STATUS_RX_TIMEOUT_TX | RP_STATUS_RX_TIMEOUT_EXTENTION))
	{
		RpSetStateRxOnToTrxOff();
		RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;

		/* Callback function execution Log */
		RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
		/* Callback function execution */
		INDIRECT_RpRxOffIndCallback();
	}
	else
	{
		if ((status & RP_PHY_STAT_RXON_BACKOFF) == 0)
		{
			RpRxOnStart();		// RX trigger

			/* Start Antenna select assist */
			RpAntSelAssist_ReStartProc();
		}
	}
}

/******************************************************************************
Function Name:       RpRcvHdrFunc
Parameters:          rcvNo:received bank number: 0 or 1
Return value:        none
Description:         this function is called rcv0/1 interrupt handler.
******************************************************************************/
static void RpRcvHdrFunc( uint8_t rcvNo )
{
	uint16_t	status;
	uint16_t nowReadLen, readLen;

	status = RpCb.status;
	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
	{
		if (RpChkRcvBank(rcvNo) == RP_SUCCESS)
		{
			if (RpCb.rx.cnt != RP_PHY_RX_STAT_INIT)
			{
				if (RpCb.rx.softwareAdf ==  RP_FALSE)
				{
					RpReadRam(rcvNo);
				}
				else
				{
					nowReadLen = (uint16_t)(RpCb.rx.cnt % RP_BB_RX_RAM_SIZE);
					nowReadLen = (uint16_t)(RP_BB_RX_RAM_SIZE - nowReadLen);
					readLen = (uint16_t)(RpCb.rx.len - (RpCb.rx.cnt + nowReadLen));
					if (readLen > RP_BB_RX_RAM_SIZE)
					{
						RpReadRam(rcvNo);
					}
					else
					{
						if (RpCb.rx.len != RP_BB_RX_RAM_SIZE)	// In case that
						{
							RpCb.rx.unreadRamPrev = RP_TRUE;
						}
					}
				}
			}
			else
			{
				RpRcvRamEnable(rcvNo);
			}
		}
	}
}

/******************************************************************************
Function Name:       RpRcvRamEnable
Parameters:          rcvxNos:received bank number: 0 or 1
Return value:        none
Description:         change recieve bank.
******************************************************************************/
static void RpRcvRamEnable(uint8_t rcvxNos)
{
	uint8_t tmpRegBuf = RpRegRead(BBTXRXST0);

	if (rcvxNos == 0)
	{
		tmpRegBuf &= ~0x10;		// rcvbank0_tmp_regbuf(bit4) = 0
		tmpRegBuf |= 0x20;		// rcvbank1_tmp_regbuf(bit5) = 1
	}
	else
	{
		tmpRegBuf &= ~0x20;		// rcvbank1_tmp_regbuf(bit5) = 0
		tmpRegBuf |= 0x10;		// rcvbank0_tmp_regbuf(bit4) = 1
	}
	RpRegWrite(BBTXRXST0, tmpRegBuf);
}

/******************************************************************************
Function Name:       RpReadRam
Parameters:          rcvNo
Return value:        none
Description:         read receive RAMx.
******************************************************************************/
static void RpReadRam( uint8_t rcvNo )
{
	uint16_t readLen, readAddrOffset;

	readAddrOffset = (uint16_t)(RpCb.rx.cnt % RP_BB_RX_RAM_SIZE);
	readLen = (uint16_t)(((RpCb.rx.cnt + (RP_BB_RX_RAM_SIZE - readAddrOffset)) < RpCb.rx.len) ?
						 (RP_BB_RX_RAM_SIZE - readAddrOffset) : (RpCb.rx.len - RpCb.rx.cnt));
	RpRegBlockRead((uint16_t)((rcvNo ? BBRXRAM1 : BBRXRAM0) + (readAddrOffset << 3)), (uint8_t *)&RpCb.rx.pTopRxBuf[RpCb.rx.cnt], readLen);	// Data Read from received RAMx
	RpRcvRamEnable(rcvNo);
	RpCb.rx.cnt += readLen;
}

/******************************************************************************
Function Name:       RpChkRcvBank
Parameters:          rcvBankNo:
Return value:        RP_SUCCESS:success RP_ERROR:no data on bankx
Description:         process check recieving frame bank.
******************************************************************************/
static int16_t RpChkRcvBank( uint8_t rcvBankNo )
{
	uint8_t rtn = RP_SUCCESS;
	uint8_t txrxst0;

	txrxst0 = RpRegRead(BBTXRXST0);
	if (((rcvBankNo == RP_PHY_BANK_0) && ((txrxst0 & RCVRAMBANK0) == 0)) ||
			((rcvBankNo == RP_PHY_BANK_1) && ((txrxst0 & RCVRAMBANK1) == 0)))
	{
		// bank no data
		rtn = RP_ERROR;
	}

	return ((int16_t)rtn);
}

/* "Interrupt source number 14" */
/***************************************************************************************************************
 * function name  : RpAdrsHdr
 * description    : Address filter interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpAdrsHdr( void )
{
	RpSelectRcvDataBank();
}

/******************************************************************************
Function Name:       RpSelectRcvDataBank
Parameters:          none
Return value:        none
Description:         Select Rcvdatabank function.
******************************************************************************/
static void RpSelectRcvDataBank( void )
{
	uint16_t status;
	uint8_t	txrxst0;
	uint8_t	txrxst2;
	uint8_t curStaBank;
	uint8_t prvSelBank;
	uint8_t	rcvRamSt;
	uint16_t sduLen;
	uint32_t difTime;
	uint32_t newRxOffTime;

	status = RpCb.status;
	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
	{
		RpCb.rx.cnt = RP_PHY_RX_STAT_PHR_DETECT;
		txrxst0 = RpRegRead(BBTXRXST0);
		txrxst2 = RpRegRead(BBTXRXST2);
		RpCb.rx.rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);
		curStaBank = (uint8_t)((txrxst2 & RCVSTOREFLG) ? RP_PHY_BANK_1 : RP_PHY_BANK_0);
		prvSelBank = (uint8_t)((RpCb.reg.bbTxRxMode3 & RCVSTOREBANKSEL) ? RP_PHY_BANK_1 : RP_PHY_BANK_0);
		
		if (curStaBank == prvSelBank)
		{
			if ((txrxst0 & RCVRAMBANK0) || (txrxst0 & RCVRAMBANK1))
			{
				/* Callback function execution Log */
				RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_RXDELAY );
				/* Callback function execution */
				INDIRECT_WarningIndCallback( RP_WARN_RX_PROCESS_DELAY );

				rcvRamSt = (uint8_t)(txrxst0 & RCVRAMST);

				if ((txrxst0 & RCVRAMBANK0) && (txrxst0 & RCVRAMBANK1))
				{
					RpRcvRamEnable((~rcvRamSt) & RCVRAMST);
				}
				else
				{
					RpRcvRamEnable(rcvRamSt);
				}
			}
		}

		// Select Current RcvDataBank
		if (curStaBank == RP_PHY_BANK_1)
		{
			RpCb.reg.bbTxRxMode3 |= RCVSTOREBANKSEL;
		}
		else
		{
			RpCb.reg.bbTxRxMode3 &= (~RCVSTOREBANKSEL);
		}
		RpRegWrite(BBTXRXMODE3, RpCb.reg.bbTxRxMode3);

		if (RpReadRxDataBank())
		{
			if ((status & RP_PHY_STAT_RX_TMOUT) && (RpCb.pib.phyRxTimeoutMode == RP_TRUE))
			{			
				RpCalcTotalBytes(RpCb.rx.lenNoFcs, &sduLen, RP_FALSE);
				newRxOffTime = (RpGetTime() + (sduLen << 3) + RP_RXOFF_MERGIN_TIME);
				newRxOffTime += (RpCb.pib.phyAckReplyTime + RP_PHY_TX_WARM_UP_TIME + (256 << 3));
				difTime = (RpCb.rx.timeout - newRxOffTime) & RP_TIME_MASK;

				if ((difTime > RP_TIME_LIMIT) || (difTime < RP_RXTMOUT_TIMER_WASTE_TIME))
				{
					RpCb.statusRxTimeout |= RP_STATUS_RX_TIMEOUT_EXTENTION;
						
					// RpCb.rx.timeout already unavailable and set TC1 for new timeout
					RpRegBlockWrite(BBTCOMP1REG0, (uint8_t RP_FAR *)&newRxOffTime, sizeof(uint32_t));
					RpCb.rx.timeout = newRxOffTime;
				}
			}
		}
		else
		{
			RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;

			RpRxOnStop();
			RpAvailableRcvRamEnable();
			if ((status & RP_PHY_STAT_RXON_BACKOFF) == 0)
			{
				RpRxOnStart();	// RX trigger
				RpCb.status = status;

				/* Start Antenna select assist */
				RpAntSelAssist_ReStartProc();
			}
			else
			{
				RpResumeCsmaTrn(status, RpRegRead(BBCCATOTAL), RP_FALSE);
			}
		}
	}
}

/******************************************************************************
Function Name:       RpReadRxDataBank
Parameters:          none
Return value:        RP_FALSE:illegal frame length
Description:         read out Rx data bank.
******************************************************************************/
static int16_t RpReadRxDataBank( void )
{
	int16_t rtn, len;
	uint8_t adrfDet1 = 0, adrfDet2 = 0, adrfTmp, adrfRd;
	uint16_t sduLen;
	uint32_t flameLenIntTimeStamp;
	uint8_t ackResponsTimeMs;
	uint16_t ackResponsTimeBit;

	RpRegBlockRead(BBRXFLEN, (uint8_t *)&len, sizeof(uint16_t));
	RpCb.rx.len = len;
	RpCb.rx.phrRx = (uint8_t)RpRegRead(BBPHRRX);
	if ((((RpCb.pib.macAddressFilter1Ena) || (RpCb.pib.macAddressFilter2Ena)) && (len < RP_aMinPHYPacketSize)) ||
			(len > RP_aMaxPHYPacketSize) || (RpCb.rx.waitRelRxBufWhenAutoRx) || (RpCb.rx.phrRx & 0x07))
	{
		rtn = RP_FALSE;
	}
	else
	{
		RpCb.rx.fcsLength = (uint8_t)((RpCb.rx.phrRx & FCSTYPE) ? RP_FCS_LENGTH_16BIT : RP_FCS_LENGTH_32BIT);
		RpCb.rx.lenNoFcs = (uint16_t)(RpCb.rx.len - RpCb.rx.fcsLength);

		if (RpCb.rx.softwareAdf == RP_FALSE)
		{
			adrfRd = RpRegRead(BBADFCON);
			if (adrfRd & ADFMONI1)
			{
				adrfDet1 = RP_TRUE;
			}
			if (adrfRd & ADFMONI2)
			{
				adrfDet2 = RP_TRUE;
			}
			if ((RpCb.pib.macAddressFilter1Ena == RP_FALSE) && (RpCb.pib.macAddressFilter2Ena == RP_TRUE))
			{
				adrfTmp = adrfDet2;
				adrfDet2 = adrfDet1;
				adrfDet1 = adrfTmp;
			}
			if (RpCb.pib.macAddressFilter1Ena == RP_FALSE)
			{
				adrfDet1 = RP_FALSE;
			}
			if (RpCb.pib.macAddressFilter2Ena == RP_FALSE)
			{
				adrfDet2 = RP_FALSE;
			}
			adrfDet2 <<= 1;
			RpCb.rx.filteredAdress = (uint8_t)(adrfDet1 | adrfDet2);
		}
		RpRegBlockRead(BBTSTAMP0, (uint8_t *)&flameLenIntTimeStamp, sizeof(uint32_t));
		RpCalcTotalBytes(RpCb.rx.lenNoFcs, &sduLen, RP_FALSE);
		RpCb.rx.timeStamp = flameLenIntTimeStamp - RP_PHY_FLINT_DELAY_TIME;	
		RpCb.rx.timeAcKRtn = RpCb.rx.timeStamp + (sduLen << 3) + RpCb.pib.phyAckReplyTime - RP_PHY_TX_WARM_UP_TIME;

		if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz))
		{
			if ((RpCb.pib.phyFskOpeMode == RP_PHY_FSK_OPEMODE_1) || (RpCb.pib.phyFskOpeMode == RP_PHY_FSK_OPEMODE_6))
			{
				ackResponsTimeMs = RP_ACK_RESPONSE_SINGLE_CH_LIMIT;
			}
			else
			{
				ackResponsTimeMs = RP_ACK_RESPONSE_MULTIPLE_CH_LIMIT;
			}
			ackResponsTimeBit = (uint16_t)ackResponsTimeMs * RpCb.pib.phyDataRate;
			RpCb.tx.ackCompLimitTimeBit = flameLenIntTimeStamp + (sduLen << 3) - RP_PHY_FLINT_DELAY_TIME + ackResponsTimeBit;
		}

		rtn = RP_TRUE;
	}

	return (rtn);
}

/******************************************************************************
Function Name:       RpDetRcvdAntenna
Parameters:          none
Return value:        received anntenna
Description:         determine received anntenna.
******************************************************************************/
static uint8_t RpDetRcvdAntenna( void )
{
	return ((uint8_t)((RpRegRead(BBANTDIV2) & ANTMONI) ? RP_PHY_ANTENNA_1 : RP_PHY_ANTENNA_0));
}

/* "Interrupt source number 8" */
/***************************************************************************************************************
 * function name  : RpRcv0Hdr
 * description    : CCA completion interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpCcaHdr( void )
{
	uint16_t	status;
	uint8_t	stCfm, val;
	uint16_t	rssi;
	uint16_t wk16;
	uint8_t edBandwidth = RpCb.pib.phyEdBandwidth;
	uint8_t ccaBandwidth = RpCb.pib.phyCcaBandwidth;
	uint8_t index = RpCb.freqIdIndex;

	status = RpCb.status;

	if ((status & (RP_PHY_STAT_TX | RP_PHY_STAT_TX_CCA)) == (RP_PHY_STAT_TX | RP_PHY_STAT_TX_CCA))
	{
		RpInverseTxAnt(RP_FALSE);
		RpCb.tx.onCsmaCa = RP_FALSE;
	}

	if ((status & (RP_PHY_STAT_TX | RP_PHY_STAT_TX_CCA | RP_PHY_STAT_TRX_ACK)) == (RP_PHY_STAT_TX | RP_PHY_STAT_TX_CCA))
	{
		if ((RpRegRead(BBTXRXST0)) & CSMACA)
		{
			RpCcaAck.result = RP_CHANNEL_BUSY;
			RpTrnHdrFunc();
		}
	}
	else
	{
		RpCb.reg.bbIntEn[0] &= ~CCAINTEN;
		RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));

		if ((index != RP_PHY_100K_M10_JP) && (index != RP_PHY_100K_M10_IN)) // except for 100kbps m=1
		{
			if (((status & RP_PHY_STAT_CCA) && ((ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (ccaBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K))) ||
				((status & RP_PHY_STAT_ED) && ((edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_200K) || (edBandwidth == RP_PHY_CCA_BANDWIDTH_INDEX_150K))))
			{
				if ((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE) ||
					((RpCb.pib.phyRegulatoryMode == RP_RMODE_ENABLE_ARIB) && (RpCb.pib.phyFreqBandId == RP_PHY_FREQ_BAND_920MHz)))
				{
					RpPrevSentTimeReSetting();
				}

				RpCb.reg.bbTimeCon &= ~(TIMEEN);// Timer Count Disable, Comp0 transmit Disable
				RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));

				RpRegTxRxDataRateDefault();

				RpCb.reg.bbTimeCon |= TIMEEN;	// Timer Count Start, Comp0 transmit Disable Stay
				RpRegWrite(BBTIMECON, (uint8_t)(RpCb.reg.bbTimeCon));
			}
		}

		if (status & RP_PHY_STAT_CCA)
		{
			// CCA mode
			RpCb.status = RP_PHY_STAT_TRX_OFF;
			if ((RpRegRead(BBTXRXST0) & CCA) == 0)
			{
				stCfm = RP_IDLE;							// idle channel
			}
			else
			{
				// busy channel
				stCfm = RP_BUSY;							// busy channel
			}

			/* Callback function execution Log */
			RpLog_Event( RP_LOG_CB, RP_LOG_CB_CCACFM );
			/* Callback function execution */
			INDIRECT_RpPlmeCcaCfmCallback( stCfm );
		}
		else if (status & RP_PHY_STAT_ED)
		{
			// ED mode
			RpCb.status = RP_PHY_STAT_TRX_OFF;
			
			RpRegBlockRead(BBRSSICCARSLT, (uint8_t *)&rssi, sizeof(uint16_t));

			switch (RpCb.pib.phyEdBandwidth)
			{
				case RP_PHY_CCA_BANDWIDTH_INDEX_225K:
					wk16 = RpCcaVthOffsetTblDefault[index];
					break;
				case RP_PHY_CCA_BANDWIDTH_INDEX_200K:
				case RP_PHY_CCA_BANDWIDTH_INDEX_150K:
					wk16 = 0x0000;
					break;
				case RP_PHY_CCA_BANDWIDTH_INDEX_NARROW:
				default:
					wk16 = RpCcaVthOffsetTblNarrow[index];
					break;
			}
			rssi -= wk16;
			rssi -= RpCb.pib.phyRssiOutputOffset;
			rssi &= 0x01FF;

			/* Callback function execution Log */
			RpLog_Event( RP_LOG_CB, RP_LOG_CB_CALCLQI );
			/* Callback function execution */
			val = INDIRECT_RpCalcLqiCallback( rssi, RP_FALSE );

			stCfm = RP_SUCCESS;
			/* Callback function execution Log */
			RpLog_Event( RP_LOG_CB, RP_LOG_CB_EDCFM );
			/* Callback function execution */
			INDIRECT_RpPlmeEdCfmCallback( stCfm , val, rssi );
		}
	}
}

/* "Interrupt source number 15" */
/***************************************************************************************************************
 * function name  : RpRovrHdr
 * description    : Receive overrun interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRovrHdr( void )
{
	uint16_t	status;

	status = RpCb.status;
	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
	{
		/* Callback function execution Log */
		RpLog_Event( RP_LOG_CB, RP_LOG_CB_WARNING_RXOVERRUN );
		/* Callback function execution */
		INDIRECT_WarningIndCallback( RP_WARN_RXRAMOVERRUN );

		RpRfStat.rxRamOverrun++;
		RpAvailableRcvRamEnable();
		if ((status & RP_PHY_STAT_RXON_BACKOFF) == 0)
		{
			RpRxOnStart();		// RX trigger

			/* Start Antenna select assist */
			RpAntSelAssist_ReStartProc();
		}
	}
}

/* "Interrupt source number 16" */
/***************************************************************************************************************
 * function name  : RpRcvModeswHdr
 * description    : Mode switch receive completion interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRcvModeswHdr( void )
{
	uint16_t	status;

	status = RpCb.status;
	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
	{
		RpAvailableRcvRamEnable();
		if ((status & RP_PHY_STAT_RXON_BACKOFF) == 0)
		{
			RpRxOnStart();		// RX trigger

			/* Start Antenna select assist */
			RpAntSelAssist_ReStartProc();
		}
	}
}

/* "Interrupt source number 17" */
/***************************************************************************************************************
 * function name  : RpRcvLvlIntHdr
 * description    : Receive level filter interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRcvLvlIntHdr( void )
{
	uint16_t	status;
	uint8_t		statusRxTimeout;

	status = RpCb.status;
	statusRxTimeout = RpCb.statusRxTimeout;

	if (status & (RP_PHY_STAT_RX | RP_PHY_STAT_RXON_BACKOFF))
	{
		if (statusRxTimeout & RP_STATUS_RX_TIMEOUT_EXTENTION)
		{
			RpSetStateRxOnToTrxOff();
			RpCb.statusRxTimeout = RP_STATUS_RX_TIMEOUT_INIT;

			/* Callback function execution Log */
			RpLog_Event( RP_LOG_CB, RP_LOG_CB_OFFIND );
			/* Callback function execution */
			INDIRECT_RpRxOffIndCallback();
		}
		else
		{
			RpAvailableRcvRamEnable();
			RpCb.rx.cnt = RP_PHY_RX_STAT_INIT;

			/* Start Antenna select assist */
			RpAntSelAssist_ReStartProc();
		}
	}
}

/* "Interrupt source number 19" */
/***************************************************************************************************************
 * function name  : RpFlenHdr
 * description    : Frame length interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpFlenHdr( void )
{
	RpSelectRcvDataBank();
}

/* "Interrupt source number 18" */
/***************************************************************************************************************
 * function name  : RpRcvCuntHdr
 * description    : Receive byte counts interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRcvCuntHdr( void )
{
	RpReadAddressInfo();
}

/******************************************************************************
Function Name:       RpReadAddressInfo
Parameters:          none
Return value:        none
Description:         Read Address Information.
******************************************************************************/
static void RpReadAddressInfo( void )
{
	uint16_t readLen;

	if (RpCb.rx.cnt == RP_PHY_RX_STAT_PHR_DETECT)
	{
		if (RpCb.rx.len < RP_PHY_RX_ADDRESS_DECODE_LEN)
		{
			readLen = RpCb.rx.len;
		}
		else
		{
			readLen = RP_PHY_RX_ADDRESS_DECODE_LEN;
		}
		RpRegBlockRead((uint16_t)(RpCb.rx.rcvRamSt ? BBRXRAM0 : BBRXRAM1),	// Inverse here
					   (uint8_t *)&RpCb.rx.pTopRxBuf[RP_PHY_RX_STAT_PHR_DETECT], readLen);
		RpCb.rx.cnt += readLen;
	}
}

/******************************************************************************
 * Notice About Reception
 *****************************************************************************/
/* "Interrupt source number 13" */
/***************************************************************************************************************
 * function name  : RpRcvStHdr
 * description    : Start reception interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpRcvStHdr( void )
{
	uint32_t time;
	uint8_t evacon0 = RpRegRead(BBCOMSTATE);
	uint16_t status;

	/* get current status */
	status = RpCb.status;
	/* Stop Antenna selection assistance */
	RpAntSelAssist_StopProc( status );

	if (evacon0 & FRCVSTATE)
	{
		time = RpGetTime() + RP_PHY_BYTE_INT_LIMIT_DELAY;
		RpRegBlockWrite(BBTCOMP2REG0, (uint8_t RP_FAR *)&time, sizeof(uint32_t));
		RpCb.reg.bbIntEn[0] |= TIM2INTEN;
		RpCb.reg.bbIntEn[2] |= BYTERCVINTEN;
		RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
		RpRegWrite(BBINTEN2, (uint8_t)(RpCb.reg.bbIntEn[2]));
	}
}

/* "Interrupt source number 20" */
/***************************************************************************************************************
 * function name  : RpByteRcvHdr
 * description    : Byte reception interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpByteRcvHdr( void )
{
	RpCb.reg.bbIntEn[0] &= ~TIM2INTEN;
	RpCb.reg.bbIntEn[2] &= ~BYTERCVINTEN;
	RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));
	RpRegWrite(BBINTEN2, (uint8_t)(RpCb.reg.bbIntEn[2]));
}

/* "Interrupt source number 3" */
/***************************************************************************************************************
 * function name  : RpBbTim2Hdr
 * description    : Timer compare 2 interrupt handler
 * parameters     : none
 * return value   : none
 **************************************************************************************************************/
static void RpBbTim2Hdr( void )
{
	uint8_t reg0x04E3;
	uint16_t status;

	/* bbtimer2 interrupt disable */
	RpCb.reg.bbIntEn[0] &= ~TIM2INTEN;
	RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));

 	if ((RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE) &&
		(RpCb.pib.phyDataRate == RP_PHY_ANTDV_DATARATE)) // AntennaDiver and 100kbps only
 	{
		status = RpCb.status;

		/* get regvalue */
		reg0x04E3 = RpRegRead(0x04E3 << 3);

		if ( status & RP_PHY_STAT_ANTENNA_SELECT )
		{
			/* before reception start interrupt occurs */
			if ( reg0x04E3 == 0x22 )
			{
				/* Antenna selection assistance */
				RpAntSelAssist_TimerProc();
			}
			else
			{
				INDIRECT_WarningIndCallback( reg0x04E3 );	// Warning

				// Safty proc
				RpRegWrite(BBTXRXRST, RFSTOP);	// RF Stop
				RpAntSelAssist_StopProc( status );
				RpAntSelAssist_StartProc();
				RpRegWrite(BBTXRXCON, RCVTRG);	// RX Start
			}
		}
		else
		{
			switch ( reg0x04E3 )
			{
				case 0x88:	// antsel on
				case 0x74:	// antsel off
					break;
				default:
					INDIRECT_WarningIndCallback( reg0x04E3 );	// Warning
					break;
			}

			RpDumPrcRcvPhr();
		}
 	}
	/* after the reception start interrupt occurs */
	else
	{
		RpDumPrcRcvPhr();
	}
}

/***********************************************************************
 * function name  : RpDumPrcRcvPhr
 * description    : Dummy process receiving PHR.
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RpDumPrcRcvPhr( void )
{
	RpCb.reg.bbIntReq = 0;
	RpRegWrite(0x0077 << 3,	0x05);
	RpCb.reg.bbIntReq = RpReadIrq();
	if (RpCb.reg.bbIntReq & RP_BBBYTE_IREQ)
	{
		RpReStartProc();
	}
	else
	{
		RpRegWrite(0x0077 << 3, 0x06);
		RpRegWrite(0x0060 << 3,	0x0D);
		RpRegWrite(0x0063 << 3,	0x00);
		RpRegWrite(0x0063 << 3,	0x00);
		RpRegWrite(0x0060 << 3,	0x01);
		RpRegWrite(0x0077 << 3,	0x07);
	}
}

/***********************************************************************
 * function name  : RpReStartProc
 * description    : restart process function.
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RpReStartProc( void )
{
	uint16_t status;
	uint8_t ccaTotal, csmacon1, csmacon3;

	status = RpCb.status;
	ccaTotal = RpRegRead(BBCCATOTAL);
	RpRxOnStop();
	RpRegWrite(0x0077 << 3, 0x07);
	if (status & RP_PHY_STAT_RX)
	{
		RpRxOnStart();			// RX trigger
		RpCb.status = status;

		/* Start Antenna select assist */
		RpAntSelAssist_ReStartProc();
	}
	else if (status & RP_PHY_STAT_TX)
	{
		if (RpCb.tx.onCsmaCa) // Rx on backoffperiod
		{
			RpCb.tx.ccaTimes += ccaTotal;
			RpCb.tx.ccaTimesOneFrame += ccaTotal;
			csmacon1 = RpCb.reg.bbCsmaCon1;
			csmacon1 &= (~NB);
			if (RpCb.pib.macMaxCsmaBackOff >= RpCb.tx.ccaTimesOneFrame)
			{
				csmacon1 |= (RpCb.pib.macMaxCsmaBackOff - RpCb.tx.ccaTimesOneFrame);
			}
			RpCb.reg.bbCsmaCon1 = csmacon1;
			RpRegWrite(BBCSMACON1, RpCb.reg.bbCsmaCon1);
			csmacon3 = RpRegRead(BBCSMACON3);
			csmacon3 &= (~BEMIN);
			csmacon3 |= (RpCb.tx.ccaTimesOneFrame + RpCb.pib.macMinBe);
			if (csmacon3 > RpCb.pib.macMaxBe)
			{
				csmacon3 = RpCb.pib.macMaxBe;
			}
			RpRegWrite(BBCSMACON3, csmacon3);
			RpCb.reg.bbCsmaCon0 |= CSMAST;
			RpRegWrite(BBCSMACON0, RpCb.reg.bbCsmaCon0);	// TX trigger
			RpCb.status = status;
		}
		else // Rx on Ack Receiving
		{
			// sequence complete NG
			RpUpdateTxTime(1, RP_FALSE, RP_FALSE);
			RpCancelTrn(status);
			if (RpDriverRetryTrn(status) == RP_FALSE)
			{
				if (status & RP_PHY_STAT_TX_CCA)
				{
					RpCb.tx.ccaTimes += ccaTotal;
				}

				/* Callback function execution Log */
				RpLog_Event( RP_LOG_CB, RP_LOG_CB_DATACFM );
				/* Callback function execution */
				INDIRECT_RpPdDataCfmCallback( RP_NO_ACK, RP_NO_FRMPENBIT_ACK, RpCb.tx.ccaTimes );
			}
		}
	}
}

/******************************************************************************
 * Antenna selection assistance function
 *****************************************************************************/
/***********************************************************************
 * function name  : RpAntSelAssist_StartProc
 * description    : Start of antenna selection assistance process
 * parameters     : none
 * return value   : none
 **********************************************************************/
void RpAntSelAssist_StartProc( void )
{
	uint16_t wk16;

 	if ((RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE) &&
		( RpCb.pib.phyDataRate == RP_PHY_ANTDV_DATARATE )) // AntennaDiver and 100kbps only
	{
		RpCb.status |= RP_PHY_STAT_ANTENNA_SELECT;

		wk16 = 0x0198;
		RpRegBlockWrite(BBCCAVTH, (uint8_t RP_FAR *)&(wk16), 2);
		wk16 = 0x0007;
		RpRegBlockWrite(BBCCATIME, (uint8_t RP_FAR *)&(wk16), 2);
		RpRegBlockWrite(BBANTDIVTIM, (uint8_t RP_FAR *)&(wk16), 2);
		wk16 = 0xFFFF;
		RpRegBlockWrite(BBANTTIMOUT, (uint8_t RP_FAR *)&(wk16), 2);
		RpRegWrite(0x042D << 3, 0x06);
		RpRegWrite(0x04E2 << 3, 0x00);
		RpRegRead(0x0408 << 3); 		// Dummy Read
		RpRegWrite(0x04E3 << 3, 0x88);
		RpRegWrite(0x04E4 << 3, 0x88);
		wk16 = 0x0198;
		RpRegBlockWrite(0x04E7 << 3, (uint8_t RP_FAR *)&(wk16), 2);
		RpRegWrite(0x04F4 << 3, 0x06);
		RpRegWrite(0x04F9 << 3, 0x03);
		RpRegWrite(0x04FC << 3, 0x01);
		RpRegWrite(0x0513 << 3, 0x0E);
		RpReadIrq();

		RpCb.reg.bbIntEn[2] |= 0x80;
		RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);
	}
}

/***********************************************************************
 * function name  : RpAntSelAssist_StopProc
 * description    : Stop processing for antenna selection assistance
 * parameters     : none
 * return value   : none
 **********************************************************************/
void RpAntSelAssist_StopProc( uint16_t status )
{
	if ( status & RP_PHY_STAT_ANTENNA_SELECT )
	{
		RpCb.status &= ~RP_PHY_STAT_ANTENNA_SELECT;

		/* bbtimer2 interrupt disable */
		RpCb.reg.bbIntEn[0] &= ~TIM2INTEN;
		RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));

		RpCb.reg.bbIntEn[2] &= ~(0x80);
		RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);

		RpRegWrite(0x04E3 << 3, 0x88);
	}
}

/***********************************************************************
 * function name  : RpAntSelAssist_RecoveryProc
 * description    : Recovery processing for antenna selection assistance
 * parameters     : none
 * return value   : none
 **********************************************************************/
void RpAntSelAssist_RecoveryProc( void )
{
	uint8_t wk;

	wk = RpRegRead(0x04E3 << 3);

	if ( wk != 0x74 )
	{
		RpSetPowerMode(RP_RF_LOWPOWER);
		RpSetPowerMode(RP_RF_IDLE);
	}
}

/***********************************************************************
 * function name  : RpAntSelAssist_StateCheckHdr
 * description    : State processing for antenna selection assistance
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RpAntSelAssist_StateCheckHdr( void )
{
	uint8_t reg0x04E3;
	uint32_t futureTime;

	#if defined(__RX)
	uint32_t bkupPsw2;
	#elif defined(__CCRL__) || defined(__ICCRL78__)
	uint8_t  bkupPsw2;
	#elif defined(__arm)
	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_ALL_DI(bkupPsw2);
	#else
	RP_PHY_ALL_DI();
	#endif

	reg0x04E3 = RpRegRead(0x04E3 << 3);

	if ( reg0x04E3 == 0x88 )
	{
		RpCb.reg.bbIntEn[2] &= ~(0x80);
		RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);

		RpRegRead(0x0408 << 3); 		// Dummy Read					
		RpRegWrite(0x04E3 << 3, 0x22);
		RpRegRead(0x0408 << 3); 		// Dummy Read

		// timer set
		futureTime = RpGetTime() + (RpCb.rx.antdvTimerValue/10);
		RpRegBlockWrite(BBTCOMP2REG0, (uint8_t RP_FAR *)&futureTime, sizeof(uint32_t));
		
		// timer start
		RpCb.reg.bbIntEn[0] |= TIM2INTEN;
		RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));

		RpRegWrite(0x04FC << 3, 0x00);
	}
	else // if ( reg0x04E3 == 0x94 )
	{
		RpRegWrite(BBTXRXRST, RFSTOP);	// RF Stop
		RpRegWrite(0x04E2 << 3, 0x00);
		RpRegWrite(0x04E3 << 3, 0x88);
		RpRegRead(0x0408 << 3); 		// Dummy Read
		RpRegWrite(0x04FC << 3, 0x01);
		RpRegWrite(BBTXRXCON, RCVTRG);	// RX Start
	}

	/* set execution log */
	RpLog_Event(RP_DEBUG_IRQ_MODEM_HDR, reg0x04E3);

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_ALL_EI(bkupPsw2);
	#else
	RP_PHY_ALL_EI();
	#endif
}

/***********************************************************************
 * function name  : RpAntSelAssist_TimerProc
 * description    : Timer processing for antenna selection assistance
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RpAntSelAssist_TimerProc( void )
{
	uint8_t reg0x04FA;

	#if defined(__RX)
	uint32_t bkupPsw2;
	#elif defined(__CCRL__) || defined(__ICCRL78__)
	uint8_t  bkupPsw2;
	#elif defined(__arm)
	uint8_t bkupPsw2[RP_INT_BKUP_BUF_SIZE];
	#endif

	/* Disable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_ALL_DI(bkupPsw2);
	#else
	RP_PHY_ALL_DI();
	#endif

	RpCb.reg.bbIntReq = 0;

	reg0x04FA = RpRegRead(0x04FA << 3);

	if ( 3 <= reg0x04FA )
	{
		RpLog_Event(RP_DEBUG_IRQ_MODEM_TIMER, reg0x04FA);
		RpRegWrite(0x04E2 << 3, 0x06);
		RpRegWrite(0x04E3 << 3, 0x94);
		RpCb.reg.bbIntReq |= RpReadIrq();
		RpCb.reg.bbIntReq &= ~(RP_BBMODEVA_IREQ);

		if ( reg0x04FA < 5 )
		{
			reg0x04FA = RpRegRead(0x04FA << 3);
		}
	}

	if ( reg0x04FA < 3 )
	{
		RpRegWrite(BBTXRXRST, RFSTOP);	// RF Stop
		RpRegWrite(0x04E2 << 3, 0x00);
		RpRegWrite(0x04E3 << 3, 0x88);
		RpCb.reg.bbIntReq |= RpReadIrq();
		RpCb.reg.bbIntReq &= ~(RP_BBMODEVA_IREQ);
		RpRegRead(0x0408 << 3); 		// Dummy Read
		RpRegWrite(0x04FC << 3, 0x01);
		RpRegWrite(BBTXRXCON, RCVTRG);	// RX Start
		RpLog_Event(RP_DEBUG_IRQ_MODEM_TIMER, reg0x04FA);
	}

	RpCb.reg.bbIntEn[2] |= 0x80;
	RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);

	/* Enable interrupt */
	#if defined(__RX) || defined(__CCRL__) || defined(__arm) || defined(__ICCRL78__)
	RP_PHY_ALL_EI(bkupPsw2);
	#else
	RP_PHY_ALL_EI();
	#endif
}

/***********************************************************************
 * function name  : RpAntSelAssist_ReStartProc
 * description    : Start of antenna selection assistance process
 * parameters     : none
 * return value   : none
 **********************************************************************/
static void RpAntSelAssist_ReStartProc( void )
{
	uint16_t status;

	/* get current status */
	status = RpCb.status;

	if ( status & RP_PHY_STAT_RX )
	{
		if (( status & RP_PHY_STAT_RXON_BACKOFF ) == 0 )
		{
			if (( RpCb.pib.phyAntennaDiversityRxEna == RP_TRUE )
				&&( RpCb.pib.phyDataRate == RP_PHY_ANTDV_DATARATE ))
			{
				RpRegWrite(BBTXRXRST, RFSTOP);

				/* bbtimer2 interrupt disable */
				RpCb.reg.bbIntEn[0] &= ~TIM2INTEN;
				RpRegWrite(BBINTEN0, (uint8_t)(RpCb.reg.bbIntEn[0]));

				RpRegWrite(0x04E2 << 3, 0x00);
				RpRegWrite(0x04E3 << 3, 0x88);

				RpCb.status |= RP_PHY_STAT_ANTENNA_SELECT;

				RpRegWrite(0x04FC << 3, 0x01);
				RpRegWrite(BBTXRXCON, RCVTRG);

				RpCb.reg.bbIntEn[2] |= 0x80;
				RpRegWrite(BBINTEN2, (uint8_t)RpCb.reg.bbIntEn[2]);
			}
		}
	}
}

/******************************************************************************
 * H/W address filter assistance function
 *****************************************************************************/
/***************************************************************************************************************
 * function name  : RpAckCheckCallback
 * description    : The frame header structure and ACK advisability check callback function
 * parameters     : pData...The pointer to receive buffer.
 *                : dataLen...Size of the received data.
 *                : pAck...The pointer to ACK frame to be sent back
 *                : pAckLen...The pointer to the area of the ACK frame length
 * return value   : RP_INVALID_FRAME...Not to be received
 *                : RP_VALID_FRAME...Valid frame but not need reply ACK.
 *                : RP_NEED_ACK | RP_VALID_FRAME...Valid frame and need reply ACK
 **************************************************************************************************************/
uint8_t RpAckCheckCallback(uint8_t *pData, uint16_t dataLen, uint8_t *pAck, uint16_t *pAckLen)
{
    uint8_t *srcAddInfoPtr;
    RpMacFrameControlT frameControl;
    uint8_t dstAddrMode;
    uint8_t srcAddrMode;
    uint16_t panId;
    uint8_t retVal = RP_INVALID_FRAME;
    uint8_t noUpDataScndAckLen = RP_FALSE;
    uint8_t frmVersion;
    uint16_t frmType;
    uint16_t ackDataCnt;
    uint8_t dstPanExist = RP_FALSE, srcPanExist = RP_FALSE;
    uint16_t panCompression;
    uint8_t ackReq;
    uint8_t sqnNoSuppress;
    uint16_t refPanId, chkPanId;
    uint32_t refExtAddr[2];
    uint8_t refPanCoord;
    uint8_t refFramePend;
    uint8_t *bkupPtrData = pData;
    uint8_t *bkupPtrAck = pAck;
    uint32_t extAddr[2];

    pData = bkupPtrData;
    pAck = bkupPtrAck;
    ackDataCnt = 2;

    if (RpCb.pib.macAddressFilter1Ena == RP_FALSE)
    {
        return retVal;
    }
    refPanId = RpCb.pib.macPanId1;
    refExtAddr[0] = RpCb.pib.macExtendedAddress1[0];
    refExtAddr[1] = RpCb.pib.macExtendedAddress1[1];
    refPanCoord = RpCb.pib.macPanCoord1;
    refFramePend = RpCb.pib.macPendBit1;

    if (noUpDataScndAckLen == RP_FALSE)
    {
        *pAckLen = 0x03;
        RP_VAL_UINT16_TO_ARRAY(RP_FRM_FrameType_Acknowledgment, pAck);
    }

    // frame controle decode
    *((uint16_t *)(&frameControl)) = RP_VAL_ARRAY_TO_UINT16(pData);
    frmVersion = frameControl.frameVersion;
    frmType = frameControl.frameType;
    dstAddrMode = frameControl.dstAddrMode;
    srcAddrMode = frameControl.srcAddrMode;
    panCompression = frameControl.panIdCompression;
    ackReq = frameControl.ackRequest;
    if (frmVersion == RP_MAC2012_FRAME_VERSION)
    {
        sqnNoSuppress = frameControl.sequenceNumberSuppress;
    }
    else
    {
        sqnNoSuppress = RP_FALSE;
    }

    if (frmVersion == RP_MAC2012_FRAME_VERSION)
    {
        if (panCompression)
        {
            if (dstAddrMode == 0 && srcAddrMode == 0)
            {
                dstPanExist = RP_TRUE;
            }
        }
        else
        {
            if (dstAddrMode != 0)
            {
                dstPanExist = RP_TRUE;
            }
            else
            {
                if (srcAddrMode != 0)
                {
                    srcPanExist = RP_TRUE;
                }
            }
        }
    }
    else
    {
        if ((panCompression && dstAddrMode != 0) && (srcAddrMode != 0))
        {
            dstPanExist = RP_TRUE;
        }
        else
        {
            if (dstAddrMode != 0)
            {
                dstPanExist = RP_TRUE;
            }
            if (srcAddrMode != 0 && panCompression == 0)
            {
                srcPanExist = RP_TRUE;
            }
        }
    }
    pData += sizeof(uint16_t);
    // invalid  FrameType check
    if (frmType > RP_FRM_FrameType_MACCommand)
    {
        return retVal;
    }
    if (frmVersion > RP_MAC2012_FRAME_VERSION)
    {
        return retVal;
    }
    else if ((frmVersion == RP_MAC2003_FRAME_VERSION) || (frmVersion == RP_MAC2011_FRAME_VERSION))
    {
        // invalid  PANID check
        if ((dstPanExist == 0) && (srcPanExist == 0))
        {
            return retVal;
        }
        // invalid  Address Mode check
        if ((dstAddrMode == RP_AddressingMode_No_Address) && (srcAddrMode == RP_AddressingMode_No_Address))
        {
            return retVal;
        }
        // invalid  Address Mode check
        if ((dstAddrMode == 0x0001) || (srcAddrMode == 0x0001))
        {
            return retVal;
        }
    }
    else if (frmVersion == RP_MAC2012_FRAME_VERSION)
    {
        // invalid  Address Mode check
        if ((dstAddrMode == RP_AddressingMode_No_Address) && (srcAddrMode == RP_AddressingMode_No_Address))
        {
            return retVal;
        }
    }
    // beacon frame or ACK frame ?
    if (!(frmType & 0x01))
    {
        if (frmType == RP_FRM_FrameType_Beacon)
        {
            if (frmVersion != RP_MAC2012_FRAME_VERSION)
              {
                pData ++;
                panId = RP_VAL_ARRAY_TO_UINT16(pData);
                if ((dstAddrMode == RP_AddressingMode_No_Address) && ((panId == refPanId) || (refPanId == 0xFFFFU)))
                {
                    retVal = RP_VALID_FRAME;
                }
                return retVal;
              }
        }
        else
        {
            return retVal;
        }
    }
    // sequence numberdecode
    // Build Ack
    retVal = (uint8_t)(ackReq ? (RP_NEED_ACK | RP_VALID_FRAME) : RP_VALID_FRAME);
    if (sqnNoSuppress)
    {
        *(pAck + 1) |= 0x01;
    }
    else
    {
        pAck[ackDataCnt] = *pData;
        pData++;
        ackDataCnt++;
    }
    // dest Address
    if (dstAddrMode == RP_AddressingMode_No_Address)
    {
        if (frmType == RP_FRM_FrameType_Beacon)
        {
            chkPanId = RP_VAL_ARRAY_TO_UINT16(pData);

            if ((srcPanExist == RP_TRUE) && (refPanId != 0xFFFF) &&
                ((refPanId != chkPanId) || (chkPanId == 0xFFFF)))
            {
                return RP_INVALID_FRAME;
            }
            if ((refPanId == 0xFFFF) && (!panCompression))
            {
                retVal &= ~RP_NEED_ACK;
            }
        }
        else // if(frmType =! RP_FRM_FrameType_Beacon)
        {
            chkPanId = RP_VAL_ARRAY_TO_UINT16(pData);
            if (!refPanCoord)
            {
                return RP_INVALID_FRAME;
            }
            else if ((srcPanExist == RP_TRUE) &&
#if !defined(RP_BROAD_CAST_ACK_DISABLE)
                     (refPanId != 0xFFFF) &&
#endif
                     (chkPanId == 0xFFFF))
            {
                retVal &= ~RP_NEED_ACK;
            }
            else if ((!panCompression && (refPanId == 0xFFFF) && (chkPanId != 0xFFFF)))
            {
                return RP_INVALID_FRAME;
            }
        }
    }
    else
    {
        if (dstPanExist == RP_TRUE)
        {
            panId = RP_VAL_ARRAY_TO_UINT16(pData);
            pData += sizeof(uint16_t);

            if ((frmVersion == RP_MAC2012_FRAME_VERSION) && (refPanId == 0xFFFF) && (panId != 0xFFFF) &&
                ((dstAddrMode != RP_AddressingMode_Extended_Address) || (srcAddrMode != RP_AddressingMode_Extended_Address)))
            {
                return RP_INVALID_FRAME;
            }
            if ((frmType != RP_FRM_FrameType_Beacon) && (refPanId != panId) && (panId != 0xFFFF))
            {
                return RP_INVALID_FRAME;
            }
            if ((frmVersion != RP_MAC2012_FRAME_VERSION) && (srcAddrMode == RP_AddressingMode_No_Address) && panCompression)
            {
                return RP_INVALID_FRAME;
            }
            if (dstAddrMode != RP_AddressingMode_Extended_Address)
            {
                if (((refPanId == 0xFFFF) || (panId == 0xFFFF))
#if !defined(RP_BROAD_CAST_ACK_DISABLE)
                    && !((refPanId == 0xFFFF) && (panId == 0xFFFF))
#endif
                    )
                {
                    retVal &= ~RP_NEED_ACK;
                }
            }
            if (srcAddrMode != RP_AddressingMode_No_Address)
            {
                RP_VAL_UINT16_TO_ARRAY(panId, &pAck[ackDataCnt]);
                ackDataCnt += sizeof(uint16_t);
            }
        }
        if (dstAddrMode == RP_AddressingMode_Extended_Address)
        {
            RpMemcpy(extAddr, pData, (sizeof(uint32_t) * 2));
            pData += (sizeof(uint32_t) * 2);

            if ((extAddr[0] != refExtAddr[0]) || (extAddr[1] != refExtAddr[1]))
            {
                return RP_INVALID_FRAME;
            }
        }
    }

    // src Address
    srcAddInfoPtr = pData;
    if ((frmVersion == RP_MAC2012_FRAME_VERSION) && (ackReq))
    {
        if (srcPanExist == RP_TRUE)
        {
            pAck[ackDataCnt]	 = *(pData);
            pAck[ackDataCnt + 1] = *(pData + 1);
            pData += sizeof(uint16_t);
            ackDataCnt += sizeof(uint16_t);
        }
        switch (srcAddrMode)
        {
            case RP_AddressingMode_Extended_Address:
                pAck[ackDataCnt] = *pData;
                pAck[ackDataCnt + 1] = *(pData + 1);
                pAck[ackDataCnt + 2] = *(pData + 2);
                pAck[ackDataCnt + 3] = *(pData + 3);
                pAck[ackDataCnt + 4] = *(pData + 4);
                pAck[ackDataCnt + 5] = *(pData + 5);
                pAck[ackDataCnt + 6] = *(pData + 6);
                pAck[ackDataCnt + 7] = *(pData + 7);
                pData += (sizeof(uint32_t) * 2);
                ackDataCnt += (sizeof(uint32_t) * 2);
                *(pAck + 1) |= 0x0C;
                break;

            case RP_AddressingMode_No_Address:
            default:
                break;
        }
    }
    if ((srcAddInfoPtr - bkupPtrData) <= dataLen)
    {
        if (ackReq)
        {
            if (frmVersion == RP_MAC2012_FRAME_VERSION)
            {
                *(pAck + 1) |= 0x20;
                if ((panCompression) && (srcAddrMode != RP_AddressingMode_No_Address))
                {
                    *(pAck) |= 0x40;
                }
                *pAckLen = (uint16_t)ackDataCnt;
            }
            else if (frmVersion == RP_MAC2011_FRAME_VERSION)
            {
                *(pAck + 1) |= 0x10;
            }
            if ((retVal & RP_NEED_ACK) && (refFramePend))
            {
                *(pAck) |= 0x10;
            }
        }
    }
    else
    {
        retVal = RP_INVALID_FRAME;
    }
    if (retVal & RP_NEED_ACK)
    {
        noUpDataScndAckLen = RP_TRUE;
    }

    return retVal;
}

#ifndef RP_WISUN_FAN_STACK
/******************************************************************************
 * callback program
 *****************************************************************************/
#define RP_EDFLOORLVL		(-75)
#define RP_EDSATURLVL		(RP_EDFLOORLVL + (256/4))
#define RP_RSSIFLOORLVL		(-99)
#define RP_RSSISATURLVL		(-35)

/******************************************************************************
 *	function Name  : RpCalcLqiCallback
 *	parameters     : rssiEdValue : read value of baseband RSSI
 *				   : rssiEdSelect : RP_TRUE: rssi select
 *				   :                RP_FALSE:ED select
 *	return value   : IEEE802.15.4 LQI or ED format
 *	description    : convert rssirslt to the value which format is specifiled
 *				   : in IEEE802.15.4.
 *****************************************************************************/
uint8_t RpCalcLqiCallback( uint16_t rssiEdValue, uint8_t rssiEdSelect )
{
	int16_t schg;
	uint8_t val;

	schg = (int16_t)rssiEdValue;
	schg = (int16_t)((schg < 0x0100) ? schg : (schg | 0xfe00));
	if (rssiEdSelect == RP_FALSE)
	{
		if (schg <= RP_EDFLOORLVL)
		{
			val = 0x00;
		}
		else if (schg >= RP_EDSATURLVL)
		{
			val = 0xff;
		}
		else // if(schg > EDFLOORLVL && schg < EDSATURLVL)
		{
			schg = (int16_t)(schg - RP_EDFLOORLVL);
			val = (uint8_t)(schg << 2);
		}
	}
	else // if (rssiSelect == RP_TRUE)
	{
		if	(schg <= RP_RSSIFLOORLVL)
		{
			// floor level 		= RSSIFLOORLVL(dBm)
			val = 0x00;
		}
		else if (schg >= RP_RSSISATURLVL)
		{
			// saturation level = RSSISATURLVL(dBm)
			val = 0xff;
		}
		else // if(schg > RSSIFLOORLVL && schg < RSSISATURLVL)
		{
			schg = (int16_t)(schg - RP_RSSIFLOORLVL);
			val = (uint8_t)(schg << 2);
		}
	}

	return (val);	// min(-99dBm) = 0, max(-35dBm) = 255 for (rssiSelect == RP_TRUE)
}
#endif // #ifndef RP_WISUN_FAN_STACK

/***********************************************************************************************************************
 * Copyright (C) 2014-2021 Renesas Electronics Corporation.
 **********************************************************************************************************************/

