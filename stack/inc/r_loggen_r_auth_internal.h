/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* (C) 2014-2015 Renesas Electronics Corporation All rights reserved.
*******************************************************************************/

#ifndef R_LOGGEN_R_AUTH_INTERNAL_H
#define R_LOGGEN_R_AUTH_INTERNAL_H

/* THIS FILE IS AUTOMATICALLY GENERATED - DO NOT EDIT */

#include "r_log_internal.h"


#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_122(const uint8_t arg0, const uint8_t arg1, const uint8_t* arg2)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(90, R_LOG_SEVERITY_DBG, sizeof(arg0) + sizeof(arg1) + 8))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(&arg1, sizeof(arg1));
        r_log_append(arg2, 8);
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_164(const uint32_t arg0)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(91, R_LOG_SEVERITY_ERR, sizeof(arg0)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_271(const uint8_t arg0, const uint8_t* arg1, const uint32_t arg2, const uint8_t arg3, const uint32_t arg4, const uint32_t arg5)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(92, R_LOG_SEVERITY_DBG, sizeof(arg0) + 16 + sizeof(arg2) + sizeof(arg3) + sizeof(arg4) + sizeof(arg5)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(arg1, 16);
        r_log_append(&arg2, sizeof(arg2));
        r_log_append(&arg3, sizeof(arg3));
        r_log_append(&arg4, sizeof(arg4));
        r_log_append(&arg5, sizeof(arg5));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_282(const uint8_t arg0, const uint8_t arg1)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(93, R_LOG_SEVERITY_ERR, sizeof(arg0) + sizeof(arg1)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(&arg1, sizeof(arg1));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_285(const uint8_t arg0)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(94, R_LOG_SEVERITY_DBG, sizeof(arg0)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
static inline void r_loggen_319(const uint8_t arg0, const uint8_t arg1)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(95, R_LOG_SEVERITY_ERR, sizeof(arg0) + sizeof(arg1)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_append(&arg1, sizeof(arg1));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
static inline void r_loggen_323(const uint8_t arg0)
{
    R_LOG_DISABLE_CONTEXT_SWITCH();
    if (r_log_start(96, R_LOG_SEVERITY_DBG, sizeof(arg0)))
    {
        r_log_append(&arg0, sizeof(arg0));
        r_log_finish();
    }
    R_LOG_ENABLE_CONTEXT_SWITCH();
}
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#endif /* R_LOGGEN_R_AUTH_INTERNAL_H */
