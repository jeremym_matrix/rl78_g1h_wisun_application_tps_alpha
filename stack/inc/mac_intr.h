/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corp. and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corp. and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *****************************************************************************/

#ifndef __RM_mac_intr_H
#define __RM_mac_intr_H
/******************************************************************************
 *  File Name       : mac_intr.h
 *  Description     : The definition of a type to use inside MAC.
 ******************************************************************************
 *  Copyright (C) 2010-2016 Renesas Electronics Corporation
 *****************************************************************************/

/* --- --- */
#include "r_stdint.h"

/* --- --- */
#define ROA_NEW_IF 0  /* can not change */

#define RmMemcpy   memcpy
#define RmMemset   memset

#include "mac_types.h"
#include "mac_msg.h"
#include "mac_mlme.h"
#include "mac_mcps.h"
#include "mac_frame.h"
#include "mac_api.h"
#if R_PHY_TYPE_CWX_M
#include "r_phy.h"
#else
#include "phy.h"
#endif

#define RM_NUM_INTERNAL_MSGS    (8U) /* PHY callbacks + 2 */

#define RM_FLG_MSG              ROA_FLG_MSG
#define RM_FLG_EXCEPTION        (1U << 1)
#define RM_FLG_INTRMSG          (1U << 2)
#define RM_FLG_DATAREQ_COMPL    (1U << 3)
#define RM_FLG_BC_DATAREQ_COMPL (1U << 4)  /* data complete in BC suspended case    */
#define RM_FLG_DUMMY            (0x8000U)
#define RM_FLG_ALL              (0xFFFFU)
#define RM_GEN_TMR              (1U)
#define RM_RSV_TMR              (2U)

#define RM_ASTATE_KEPP_NUMS     (8U)

#define RP_TX_ON                (0x09)

#ifndef MAX
#define MAX(a, b) ((a < b) ? b : a)
#endif

#ifndef MIN
#define MIN(a, b) ((a > b) ? b : a)
#endif

#if !defined(RM_MAC_MAX_MINCNT)
#define RM_MAC_MAX_MINCNT 61
#endif

typedef struct
{
    uint8_t               macAckWaitDuration; // phyAck..

    /* --- AddressFilter --- */
    boolean_t             macPromiscuousMode;
    uint16_t              macPANId;
    uint16_t              macShortAddress;
    RmExtendedAddressT    macExtendedAddress;
    boolean_t             macPANCoord;
    boolean_t             macFramePend;

    /* --- MAC Security --- */
    uint32_t              macFrameCountIndInterval;

    /* --- Metrics --- */
    boolean_t             macMetricEnabled; //!< The activation state of the MAC performance metrics collection

    /* --- Performance metrics (IEEE Std 801.15.4-2015 8.4.2.6) --- */
    uint32_t              macCounterOctets;        //!< The size of the MAC metrics counters in octets
    uint32_t              macRetryCount;           //!< The number of transmitted frames that required exactly one retry before acknowledgement
    uint32_t              macMultipleRetryCount;   //!< The number of transmitted frames that required more than one retry before acknowledgement
    uint32_t              macTXFailCount;          //!< The number of transmitted frames that did not result in an acknowledgement after macMaxFrameRetries
    uint32_t              macTXSuccessCount;       //!< The number of transmitted frames that were acknowledged after the initial Data frame transmission
    uint32_t              macFCSErrorCount;        //!< The number of frames that were discarded due to an incorrect FCS
    uint32_t              macSecurityFailureCount; //!< The number of frames that did not pass the MAC frame security check successfully
    uint32_t              macDuplicateFrameCount;  //!< The number of frames that contained the same sequence number as the previous frame
    uint32_t              macRXSuccessCount;       //!< The number of frames that were received correctly

    /* --- PowerMode --- */
    uint8_t               macLowPowerMode;

    /* --- TX --- */
    uint8_t               macMaxCSMABackoffs;
    uint8_t               macMinBE;
    uint8_t               macMaxBE;
    uint16_t              macMaxFrameTotalWaitTime;
    uint8_t               macMaxFrameRetries;
    uint8_t               macDSN;

    /* --- RX --- */
    boolean_t             macRxOnWhenIdle;

    /* --- for internal --- */
    boolean_t             macProfileSpecificMode;
#ifndef R_HYBRID_PLC_RF

    /* --- for conversion of WiSUN PHY to RF Driver config --- */
    RmMlmeWiSunPhyConfigT phyWiSunPhyConfig;
#endif

    /* --- for channel conversion WiSUN <-> RF Driver --- */
    uint8_t               phyChannelConversion;

    uint8_t               macDuplicateDetectionTTL;

#ifdef R_HYBRID_PLC_RF

    /* --- EBR Filter --- */
    RmBitmap8T macEBRFilters;

    /* --- EB/EBR --- */
    boolean_t  macUseEnhancedBeacon;
    RmOctetT   macEBSN;
    boolean_t  macBeaconAutoRespond;
    uint8_t    macBeaconPayload[RM_aMaxBeaconPayloadLength];
    boolean_t  macAutoRequest;

    /* Duty Cycle Handling. */
    uint16_t   macDutyCyclePeriod;
    uint16_t   macDutyCycleLimit;
    uint8_t    macDutyCycleThreshold;

    boolean_t  macDisablePHY;
    uint8_t    macPOSTableEntryTTL;
    uint8_t    macBeaconRandomizationWindowLength;
    uint8_t    macFrequencyBand;
#endif /* R_HYBRID_PLC_RF */

} RmPIBAttributesT;

typedef enum
{
    RM_MACAState_Idle = 0,        /* Idle state                                   */
    RM_MACAState_Mlme_Active,     /* In active MLME state                         */
    RM_MACAState_Xfer,            /* In data transfer                             */
    RM_MACAState_Tracking_Beacon, /* In tracking beacon                           */
    RM_MACAState_ED,              /* In ED Scan                                   */
    RM_MACAState_X_as,            /* In Active Scan                               */
    RM_MACAState_X_ps,            /* In Passive Scan                              */
    RM_MACAState_AS,              /* In Associate req                             */
    RM_MACAState_await_frame,     /* In wait indirect data                        */
    RM_MACAState_Wait_Response,   /* In waiting the responce from coordinator */
    RM_MACAState_wait_TRX,        /* In RxOnTime Timeout (do RX_ENABLE.request)   */
    RM_MACAState_X_Eas,           /* In Enhanced Active Scan                      */
    RM_MACAstate_Wait_Eack,       /* Waiting Enhanced Ack                         */
    RM_MACAState_reset_pending    /* In reset process */
} RmMacActiveStateEnumT;
typedef uint8_t RmMacAStateT;

/* UTILITY */
#define RM_NO_POOL_ID 0

/* local define */
/* CSMA/CA define */
#define RmInterMsgInitialize(pMsg, pMac, prmid, len) do { \
        ((RmMsgHdrT*)(pMsg))->osMsgHeader.mem_id = 0xFF; \
        ((RmMsgHdrT*)(pMsg))->osMsgHeader.task_id = 0xFF; \
        ((RmMsgHdrT*)(pMsg))->osMsgHeader.param_id = (uint8_t)(prmid); \
        ((RmMsgHdrT*)(pMsg))->rm_len = (uint8_t)(len); \
} while (0)

typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
} RmInterMsgHdrT;

typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
    uint8_t            mm_status;
    uint8_t            mm_optStatus;
    uint8_t            mm_numBackoffs;
} RmCsmaDataCfmT;

#define RmCsmaInitializeDataCfm(pMac, msg) \
    RmInterMsgInitialize((msg), (pMac), \
                         RM_Pid_PD_DATA_confirm, sizeof(RmCsmaDataCfmT) - sizeof(RmMsgHdrT))


typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
    uint32_t           mm_time;
    uint16_t           mm_length;
    uint8_t*           mm_pdata;
    uint8_t            mm_liq;
    uint8_t            mm_status;
} RmCsmaDataIndT;

#define RmCsmaInitializeDataInd(pMac, msg) \
    RmInterMsgInitialize((msg), (pMac), \
                         RM_Pid_PD_DATA_indication, sizeof(RmCsmaDataIndT) - sizeof(RmMsgHdrT))

typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
    uint8_t            mm_status;
    uint8_t            mm_edValue;
} RmCsmaEdCfmT;

#define RmCsmaInitializeEdCfm(pMac, msg) \
    RmInterMsgInitialize((msg), (pMac), \
                         RM_Pid_PLME_ED_confirm, sizeof(RmCsmaEdCfmT) - sizeof(RmMsgHdrT))


#define RM_Pid_INTERNAL_RXOFF_indication (RM_Pid_Reserved + 1)
typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
} RmCsmaRxOffIndT;

#define RmCsmaInitializeRxOffInd(pMac, msg) \
    RmInterMsgInitialize((msg), (pMac), \
                         RM_Pid_INTERNAL_RXOFF_indication, sizeof(RmCsmaRxOffIndT) - sizeof(RmMsgHdrT))


#define RM_Pid_INTERNAL_Pending_Data_Tout_indication (RM_Pid_Reserved + 2)
typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
    RmDataXferElemT*   mm_pElement;
} RmMcpsPendDataToutIndT;

typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
    uint8_t            mm_status;
} RmWarningIndT;

void RpWarningIndCallback(unsigned char phyStatus);

typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
    uint8_t            mm_status;
} RmFatalErrorIndT;
void RpFatalErrorIndCallback(unsigned char phyStatus);

#define RmMcpsInitializePendDataToutInd(pMac, msg) \
    RmInterMsgInitialize((msg), (pMac), \
                         RM_Pid_INTERNAL_Pending_Data_Tout_indication, \
                         sizeof(RmMcpsPendDataToutIndT) - sizeof(RmMsgHdrT))

typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
} RmMlmePANIDConfIndT;


typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
    uint8_t            mm_addrMode;
    RmDeviceAddressT   mm_addr;
} RmMlmeAutoReqIndT;


typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
} RmMlmeRxOffIndT;

#ifdef R_HYBRID_PLC_RF
#define RM_Pid_INTERNAL_MLME_NEXTSCAN_request (RM_Pid_Reserved + 6)
typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
} RmMlmeNextScanReqT;

#define RmMlmeInitializeNextScanReq(pMac, msg) \
    RmInterMsgInitialize((msg), (pMac), \
                         RM_Pid_INTERNAL_MLME_NEXTSCAN_request, sizeof(RmMlmeRxOffIndT) - sizeof(RmMsgHdrT))
#endif

typedef enum
{
    R_PHY_CHAN_CONV_X     =  0,
    R_PHY_CHAN_CONV_2X    =  1,
    R_PHY_CHAN_CONV_2Xp1  =  2,
    R_PHY_CHAN_CONV_Xp20  =  3,
    R_PHY_CHAN_CONV_2Xp20 =  4,
    R_PHY_CHAN_CONV_Xp30  =  5,
    R_PHY_CHAN_CONV_2Xp30 =  6,
    R_PHY_CHAN_CONV_Xp65  =  7,
    R_PHY_CHAN_CONV_2Xp66 =  8,
    R_PHY_CHAN_CONV_Xp33  =  9,
} r_phy_channel_conversion_t;

typedef enum
{
    R_PHY_FSK_OPEMODE_1 =  1,
    R_PHY_FSK_OPEMODE_2 =  2,
    R_PHY_FSK_OPEMODE_3 =  3,
    R_PHY_FSK_OPEMODE_4 =  4,
    R_PHY_FSK_OPEMODE_5 =  5,
    R_PHY_FSK_OPEMODE_6 =  6,
} r_phy_fsk_opemode_t;

#ifdef R_HYBRID_PLC_RF
typedef enum
{
    R_MAC_FREQ_BAND_863   =  0, //!< Europe 1 -> 863-870MHz
    R_MAC_FREQ_BAND_866   =  1, //!< India
    R_MAC_FREQ_BAND_870   =  2, //!< Europe 2 -> 870-876MHz
    R_MAC_FREQ_BAND_915   =  3, //!< North America, Canada, Mexico, Colombia
    R_MAC_FREQ_BAND_915_A =  4, //!< North America, Canada, Mexico, Colombia
    R_MAC_FREQ_BAND_915_B =  5, //!< Brazil
    R_MAC_FREQ_BAND_915_C =  6, //!< Argentina, Australia, NZ
    R_MAC_FREQ_BAND_920   =  7, //!< Japan
} r_mac_freq_band_designation_t;

typedef enum
{
    R_PHY_BAND_EUROPE_1_IN_SG   =  4,  //!< Europe 1 -> 863-870MHz
    R_PHY_BAND_NORTH_AMERICA_BZ =  7,  //!< North America
    R_PHY_BAND_JAPAN            =  9,  //!< Japan
    R_PHY_BAND_EUROPE_2         =  15, //!< Europe 2 -> 870-876MH
} r_phy_freq_band_id_t;

typedef struct
{
    r_mac_freq_band_designation_t mac_freqband_designation;  // Frequency band designation (input)
    r_phy_fsk_opemode_t           phy_fsk_opemode;           // FSK operating mode following PHY driver definition (input)
    r_phy_freq_band_id_t          phy_freqband_id;           // PHY driver frequency band ID (output)
    r_phy_channel_conversion_t    phy_chan_conv;             // Channel conversion factor (output)
} r_rf_wisun_translate_info_t;

#else /* R_HYBRID_PLC_RF */

typedef enum
{
    APP_CMD_ParamType_UnicastSchedule   = 0,
    APP_CMD_ParamType_BroadcastSchedule = 1,
} r_app_schedule_type_t;

typedef r_ie_wp_schedule_regulatory_domain_t r_wisun_phy_regulatory_domain_t;

typedef enum
{
    R_OPERATING_CLASS_1 = 1,
    R_OPERATING_CLASS_2 = 2,
    R_OPERATING_CLASS_3 = 3,
    R_OPERATING_CLASS_4 = 4,
} r_wisun_phy_operating_class_t;

typedef enum
{
    R_OPERATING_MODE_1a = 0x1a,
    R_OPERATING_MODE_1b = 0x1b,
    R_OPERATING_MODE_2a = 0x2a,
    R_OPERATING_MODE_2b = 0x2b,
    R_OPERATING_MODE_3  = 0x03,
} r_wisun_phy_operating_mode_t;

typedef enum
{
    R_PHY_BAND_NORTH_AMERICA =  7,  //!< North America
    R_PHY_BAND_BRAZIL        =  7,  //!< Brazil
    R_PHY_BAND_JAPAN         =  9,  //!< Japan
    R_PHY_BAND_EUROPE_1      =  4,  //!< Europe 1 -> 863-870MHz
    R_PHY_BAND_EUROPE_2      =  15, //!< Europe 2 -> 870-876MHz
    R_PHY_BAND_INDIA         =  4,  //!< India
    R_PHY_BAND_AZ_NZ_50kbps  =  7,  //!< Australia/New Zealand @  50kbps
    R_PHY_BAND_AZ_NZ_150kbps =  16, //!< Australia/New Zealand @ 150kbps
    R_PHY_BAND_SINGAPORE     =  4,  //!< Singapore
} r_phy_freq_band_id_t;

typedef struct
{
    r_wisun_phy_regulatory_domain_t regulatory_domain;
    r_wisun_phy_operating_class_t   operating_class;
    r_wisun_phy_operating_mode_t    operating_mode;
    r_phy_freq_band_id_t            phy_freqband_id;
    r_phy_fsk_opemode_t             phy_fsk_opemode;
    r_phy_channel_conversion_t      phy_chan_conv;
    uint16_t phy_cca_duration;
} r_rf_wisun_translate_info_t;

typedef struct
{
    r_wisun_phy_regulatory_domain_t regulatory_domain;
    r_wisun_phy_operating_class_t   operating_class;
    r_wisun_phy_operating_mode_t    operating_mode;
    uint8_t  modulation;
    uint8_t  phyType;
    uint8_t  phyMode;
    uint8_t  channelPlan;
    uint16_t fskPreambleLength;
} r_rf_wisun_1_1_translate_info_t;

#endif /* R_HYBRID_PLC_RF */

typedef struct
{
    RmMsgHdrT          mm_Hdr;
    RmInterMsgHandlerT mm_handler;
    RmMacEnumT         mm_status;
    uint8_t            mm_optStatus;
    uint8_t            mm_numBackoffs;
} RmCsmaMacDataCfmT;

typedef union
{
    RmCsmaDataCfmT         dataCfm;
    RmCsmaDataIndT         dataInd;
    RmCsmaEdCfmT           edCfm;
    RmCsmaRxOffIndT        rxOff;
    RmMcpsPendDataToutIndT pendDataToutInd;
    RmMlmePANIDConfIndT    PANIDConfInd;
    RmMlmeAutoReqIndT      autoReqInd;
    RmMlmeRxOffIndT        mlmeRxOff;
#ifdef R_HYBRID_PLC_RF
    RmMlmeNextScanReqT     mlmeNextScanReq;
#endif /* R_HYBRID_PLC_RF */
    RmCsmaMacDataCfmT      macDataCfm;
    RmWarningIndT          rpWarningInd;
    RmFatalErrorIndT       rpFatalErrorInd;
} RmInterMsgUnionT;

typedef struct
{
    RmInterMsgUnionT body;
    void*            next;
} RmInterMsgEntryT;


typedef  struct
{
    RmInterMsgEntryT* first;
    RmInterMsgEntryT* last;
} RmInterMsgFreeQT;

typedef struct
{
    /* currenct Request information */
    void*            pRetBuf;       /* return buffer               */
    RmPrimitiveIdT   requestId;     /* current request prim ID     */
    uint8_t          devAddrMode;   /* dev addrmode of current request */
    uint16_t         devPANId;      /* dev panid of current request   */
    RmDeviceAddressT devAddr;       /* dev addr of current request */
    uint32_t         arg1;          /* The tmp. place of the argument.  */
    uint8_t          awaitAddrMode; /* peer device addr mode in await time */
    uint16_t         awaitPANId;    /* peer device PAN ID in await time    */
    RmDeviceAddressT awaitAddr;     /* peer device addr in await time      */
} RmMlmeCbT;

typedef struct
{
    RoaMsgQ          indirectQ;           /* The indirect queue.                   */
    RmMacEnumT       dataReqStatus;       /* status of MCPS Data Confirm           */
    RmDataXferElemT  directXferElem;      /* TX Element for direct send            */
    RmDataXferElemT* pCurXferElem;        /* pointer to current TX element         */
    uint32_t         lastPersisCheckTime; /* the time of last
                                                of persis time check           */
} RmMcpsCbT;

typedef struct
{
    RmDataXferElemT* pCurElem;            /* Current TX element                        */
    uint32_t         nextIfsTime;         /* end time of Inter Frame Space for last TX */
    uint32_t         nextTimeout;         /* timeout specified in request for PHY      */
    uint32_t         originalNextTimeout; /* area of saving NextTimeout temporarily    */
    RmPhyEnumT       phyStatus;           /* status specified in request for PHY       */
    RmPhyEnumT       originalPhyStatus;   /* area of saving PHY status temporarily     */
    RmBitmap8T       phyOptions;          /* options specified in request for PHY      */
} RmCsmaCbT;

/* the structure for MAC management. */
typedef struct
{
    RmCallbaksT        cbacks;              /* member for registration of callback functions for
                                    interface for next higher layer                     */
    RmResourcesT*      pResources;          /* pointer to resources used in MAC.
                                Next higher layer or application must allocate resources */
    RmConfigParamsT*   pConfig;             /* pointer to configuration information in MAC.
                      Next higher layer or application specify configuration information */
    RoaMsgQ            cmdPendQ;            /* The command pending queue.                           */
    uint8_t            mac_rx_mpl;          /* Memory ID which MAC uses for RX                */
    uint8_t            mac_flg;             /* Event flag ID which MAC uses for synchronization    */
    uint8_t            mac_warning_ind_mpl; /* Memory ID for warning indication messages */
    uint8_t            mac_fatal_ind_mpl;   /* Memory ID for fatal error indication messages */
    RmPIBAttributesT   PIBAttributes;       /* PIB Attributes                                   */
    RmExtendedAddressT aExtendedAddress;    /* Extended Address of itself                  */
    RmMacAStateT       activeState;         /* Current status                              */
    RmMacAStateT       activeStateStack[RM_ASTATE_KEPP_NUMS];

    /* area for saving previous status             */
    uint8_t            asStackPtr;      /* current position of saving activeStateStack */
    uint8_t            currentChannel;  /* current channel                             */
    uint8_t            transmitPower;   /* transmit power  //@@@Rel.01                 */
    uint8_t            originalChannel; /* member of saving channel temporarily        */
#ifdef R_HYBRID_PLC_RF

    /* Scan Parameters */
    RmOctetT           scanDuration;       /* scan duration specified by next higher layer*/
    RmTimeT            scanEndTime;        /* End of Scan Duration time                   */
    uint8_t            resultListSize;     /* length of Scan Result saved                 */
    RmPANIdentifierT   originalPANId;      /* member of saving PANID of itself temporarily*/

#if 0                                      // Currently not used for active scan, consider removal once frequency hopping is implemented
    uint8_t            currentPage;        /* current channel page */
    uint8_t            originalPage;       /* original channel page */
    RmBitmap32T        unscanedChannels_l; /* unscanned Channels Low(bit map)             */
    RmBitmap32T        unscanedChannels_h; /* unscanned Channels High(bit map)            */
    RmMlmeScanDataT*   pScanData;          /* pointer to area of saving Scan Result       */
    RmEAScanParamT     EAScanParam;
#endif

    uint8_t            beaconBuffer[RM_FRM_BreaconFrameBufLen]; /* buffer only for beacon TX */
#endif /* R_HYBRID_PLC_RF */
    boolean_t          BorderRouter;                            /* Configuration as BorderRouter               */

    uint8_t            NB;                                      /* number of Backoff in current TX(non beacon) */
    uint8_t            BE;                                      /* backoff exponent in currenct TX(non beacon) */

    RmMlmeCbT          mlmeCb;                                  /* Control Block of MLME-SAP                    */
    RmMcpsCbT          mcpsCb;                                  /* Control Block of MCPS-SAP                    */
    RmCsmaCbT          csmaCb;                                  /* Control Block of CSMA Manager                */
    RmInterMsgEntryT   interMsgPool[RM_NUM_INTERNAL_MSGS];      /* internal Message pool    */
    RmInterMsgFreeQT   interMsgFreeQ;                           /* free queue of internal message   */
    uint16_t           sentFrameControl;
    RoaMsgQ            interMsgQ;                               /* run queue of internal message                */
    RmSecParamT        workSParam;
} RmMacCbT;

typedef struct
{
    uint8_t    state;
    RmBitmap8T options;
    uint32_t   time;
} RmCsmaSetTrxStateReqParamT;

typedef struct
{
    uint8_t*  psdu;
    uint16_t  psduLength;
    uint32_t  time;
    uint8_t   linkQuality;
    boolean_t isPendInAck;
} RmMcpsActionCsmaDataIndicationParamT;


#define  RmPutInterMsg(pMac, pMsg) RoaPutQ((RoaMsgT)pMsg, &((pMac)->interMsgQ))


uint16_t RmConvEndian16(uint16_t arg);
uint32_t RmConvEndian32(uint32_t);

/* M16 / ARM / x86 .... */
#define RmHtoLeS(x)  (x)
#define RmHtoLeL(x)  (x)
#define RmHtoLeLL(x) (x)

#define RmHtoBeS(x)  RmConvEndian16(x)
#define RmHtoBeL(x)  RmConvEndian32(x)

#define RmBetoHS(x)  RmHtoBeS(x)
#define RmBetoHL(x)  RmHtoBeL(x)

#define RmLetoHS(x)  RmHtoLeS(x)
#define RmLetoHL(x)  RmHtoLeL(x)

#define RM_LOGNUM_MAC   (0)
#define RM_LOGNUM_MLME  ((uint32_t)10000)
#define RM_LOGNUM_MCPS  ((uint32_t)20000)
#define RM_LOGNUM_CSMA  ((uint32_t)30000)
#define RM_LOGNUM_FRAME ((uint32_t)40000)
#define RM_LOGNUM_UTIL  ((uint32_t)50000)

#define RmGetTrace(pdata, typ)

#define RM_UP_MSG(pMac, pMsg) (pMac)->cbacks.upMsg(pMsg)


/************************ MCPS-SAP local functions ***************************/
#if defined(__RX)
#define RM_MCPS_PERSIS_CHECK_PERIOD (500U) /* the interval time of
                                    checking of the Persistence time. */
#else
#define RM_MCPS_PERSIS_CHECK_PERIOD (50U)  /* the interval time of
                                    checking of the Persistence time. */
#endif

#define RM_MCPS_MLME_HANDLE         (0xFFFFU)   /* msdu handle of MLME commands */
typedef uint16_t RmInterMsduHandleT;

/** extern *****************************************************
 **************************************************************/
extern const RmDeviceAddressT rmBroadcastAddr;
#define rmInvalidAddr rmBroadcastAddr
extern const RmDeviceAddressT rmNoAddr;

typedef void (* RmMacOSCallbackT)(uint8_t);
extern volatile RmMacOSCallbackT pRmMacOSCback;

extern RmMacCbT* g_pActiveMac;

#if R_DEV_TBU_ENABLED
extern boolean_t rmFrameSubscriptionEnabled;
#endif

/** Logging functions ******************************************
 **************************************************************/
#define RmClearErrorlogs(pLog)     do {} while (0)
#define RmAddErrorLogs(pMac, data) do {} while (0)


/** prototype callback(RFdriver) *******************************
 **************************************************************/
#if R_PHY_TYPE_CWX_M

void RpPdDataIndCallback(uint8_t* pData,
                         uint16_t dataLength,
                         uint8_t  currentOpeMode,
                         uint32_t time,
                         uint8_t  linkQuality,
                         uint16_t rssi,
                         uint8_t  selectedAntenna,
                         uint8_t  status,
                         uint8_t  modulation,
                         uint32_t phr);

void RpPdDataCfmCallback(uint8_t phyStatus, uint8_t currentOpeMode, uint8_t numBackoffs, uint8_t bank, uint8_t newPhyModeId, uint8_t msNumBackoffs);
void RpRxOffIndCallback(uint8_t status);

#else /* R_PHY_TYPE_CWX_M */

void RpPdDataIndCallback(uint8_t* pData,
                         uint16_t dataLength,
                         uint32_t time,
                         uint8_t  linkQuality,
                         uint16_t rssirslt,
                         uint8_t  selectedAntenna,
                         uint16_t rssi_0,
                         uint16_t rssi_1,
                         uint8_t  status,
                         uint8_t  filteredAddress,
                         uint8_t  phr);

void RpPdDataCfmCallback(uint8_t phyStatus, uint8_t framepend, uint8_t numBackoffs);
void RpRxOffIndCallback(void);

#endif /* R_PHY_TYPE_CWX_M */

void RpPlmeCcaCfmCallback(uint8_t phyStatus);
void RpPlmeEdCfmCallback(uint8_t phyStatus, uint8_t edValue, uint16_t rssi);

/** prototype **************************************************
 **************************************************************/

/* mac.c */
void    RmSetRFIdle(RmMacCbT* pMac);
uint8_t RmSetRFRxOn(RmMacCbT* pMac, RmCsmaSetTrxStateReqParamT* pData);
void    RmInitializePIBAttributes(RmMacCbT* pMac);
void    RmInitializeMacCB(RmMacCbT* pMac);
void    RmProcessHipriEvents(void* pmac, RoaUintT flgptn);
void    RmFatalErrorIndication(uint8_t status);
void    RmWarningIndication(uint8_t status);

/* mac_csma.c */
void       RmCsmaInitialize(RmCsmaCbT* pCsmaCb);
RmMacEnumT RmCsmaDataReq(RmMacCbT* pMac, RmDataXferElemT* pElem);

#ifdef R_HYBRID_PLC_RF

/* mac_frame.c */
RmOctetT RmFrmCmdBuildBeaconRequest(RmMacCbT*  pMac,
                                    uint8_t*   pPsdu,
                                    RmBitmap8T txOptions  /* for security enable/disable */,
                                    RmOctetT   frameVersion);

RmOctetT RmFrmBuildBeacon(RmMacCbT*        pMac,
                          uint8_t*         pPsdu,
                          RmBitmap8T       txOptions,
                          RmOctetT         frmVersion,
                          RmOctetT         linkQuality,
                          RmBooleanT       BSNSuppression,
                          RmOctetT         dstAddrMode,
                          RmPANIdentifierT dstPANId,
                          RmDeviceAddressT dstAddr);
#endif /* R_HYBRID_PLC_RF */
/* mac_mcps.c */
void       RmMcpsInitialize(RmMacCbT* pMac);
RmMacEnumT RmMcpsEdfeDataRequest(void* pmac, void* pMsg);
void       RmMcpsEdfeDataIndication(void* pmac, uint16_t flgptn);
RmMacEnumT RmMcpsNonBlockingDataReq(RmMacCbT* pMac, RmBitmap8T txOptions, uint8_t* pdata,
                                    uint16_t dataLength, RmInterMsduHandleT msduHandle, RmMcpsHookFuncT hookFunc);
void       RmMcpsActionCsmaDataConfirm(RmMacCbT* pMac, RmMacEnumT status, boolean_t isPendInAck, uint8_t numBackoffs);
RmMacEnumT RmMcpsBlockingDataReq(RmMacCbT* pMac, RmBitmap8T txOptions, uint8_t* pdata,
                                 uint16_t dataLength, RmInterMsduHandleT msduHandle);
void RmMcpsActionCsmaDataIndication(RmMacCbT* pMac, RmMcpsActionCsmaDataIndicationParamT* pData);
void RmMcpsCheckTimeoutInPendingQueue(RmMacCbT* pMac, uint16_t count);

/* mac_mlme.c */
void       RmMlmeInitialize(RmMlmeCbT* pMlmeCb);
void       mlmeDataReqCmdHook(RmMacCbT* pMac, RmMacEnumT status, void* pArg);
RmMacEnumT RmMlmeSetphyCurrentChannel(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyTransmitPower(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFrequencyOffset(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyCCABandwidth(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyCCADuration(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFSKPreambleLength(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyMRFSKSFD(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFSKOpeMode(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFSKScramblePsdu(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFCSLength(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFskFecRxEna(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFskFecTxEna(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFskFecScheme(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyAckReplyTime(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyAntennaSwitchEna(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyAntennaDiversityRxEna(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyAntennaSelectTx(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyLvlfltrVth(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyRssiOutputOffset(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyChannelsSupportedPage(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFreqBandId(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyRegulatoryMode(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyAgcWaitGainOffset(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyCcaVth(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
#if R_PHY_TYPE_CWX_M
RmMacEnumT RmMlmeSetphyFskFCSLength(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFskCcaVth(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFskFecRxAutoEna(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyOfdmCcaVth(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyFskCCADuration(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyOfdmCCADuration(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
#endif /* R_PHY_TYPE_CWX_M */
RmMacEnumT RmMlmeSetphyCcaVthOffset(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyAntennaSwitchEnaTiming(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyGpio0Setting(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyGpio3Setting(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyRmodeTonMax(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyRmodeToffMin(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyRmodeTcumSmpPeriod(RmMacCbT* pMac, RmShortT cumSmpPeriod);
RmMacEnumT RmMlmeSetphyRmodeTcumLimit(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyCcaWithAck(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetmacPromiscuousMode(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmSetmacAddressFilterEna(RmMacCbT* pMac, boolean_t macAddressFilterEna);
void       RmMlmeSetmacPANId(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacPANId(RmMacCbT* pMac);
void       RmMlmeSetmacShortAddress(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacShortAddress(RmMacCbT* pMac);
void       RmMlmeSetmacExtendedAddress(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacExtendedAddress(RmMacCbT* pMac);
void       RmMlmeSetmacBorderRouter(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacBorderRouter(RmMacCbT* pMac);
void       RmMlmeSetmacFramePend(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacFramePend(RmMacCbT* pMac, boolean_t macFramePend);
void       RmMlmeSetmacLowPowerMode(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmMlmeSetmacMaxCSMABackoffs(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacMaxCSMABackoffs(RmMacCbT* pMac);
void       RmMlmeSetmacMinBE(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacMinBE(RmMacCbT* pMac);
void       RmMlmeSetmacMaxBE(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacMaxBE(RmMacCbT* pMac);
void       RmMlmeSetmacMaxFrameRetries(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacMaxFrameRetries(RmMacCbT* pMac);
RmMacEnumT RmMlmeSetmacRxOnWhenIdle(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
void       RmSetmacBackoffSeed(uint16_t seed);
void       RmInitmacExtendedAddress(RmMacCbT* pMac);
#if R_PHY_TYPE_CWX_M
RmMacEnumT RmMlmeSetphyOperatingMode(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyRxModulation(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyCcaModulation(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyTxModulation(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyNewModeParam(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyNewModeBank(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyNewModeChannel(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetphyModeSwitchPhrParam(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSwitchBackBaseOperatingMode(RmMacCbT* pMac);
RmMacEnumT RmMlmeSetMDRNewModeParameters(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
RmMacEnumT RmMlmeSetMDRNewModeSwitchBank(RmMacCbT* pMac, RmMlmeMacPIBAttributeValueT* pValue);
#endif /* R_PHY_TYPE_CWX_M */
#ifdef R_HYBRID_PLC_RF
RmOctetT   RmMlmeGetDutyCycleUsage(void);
RmMacEnumT RmMlmeScanRequest(void* pmac, void* pMsg);
void       mlmeUpdataUnscanned(RmMacCbT* pMac);
RmMacEnumT mlmeDoScan(RmMacCbT* pMac);
void       mlmeScanCmdHook(RmMacCbT* pMac, RmMacEnumT status, void* pArg);
void       RmMlmeActionEdConfirm(void* pArgMac, void* pArgMsg);
void       RmMlmeActionScanDurationTimeout(RmMacCbT* pMac);
void       mlmePostScan(RmMacCbT* pMac);
RmBooleanT mlmeScanSelectChannel(RmMacCbT* pMac);
RmMacEnumT RmMlmeStartRequest(void* pmac, void* pMsg);
RmMacEnumT mlmeRcvBeaconReqCmd(RmMacCbT* pMac, RmFrmMhrT* pMhr, RmOctetT* pdata,
                               RmBooleanT secUse, RmOctetT aclEntry,
                               RmOctetT linkQuality,
                               RmOctetT dstAddrMode, RmPANIdentifierT dstPANId, RmDeviceAddressT dstAddr,
                               RmShortT headerIELength, RmOctetT* headerIE,
                               RmShortT payloadIELength, RmOctetT* payloadIE);
void RmMlmeActionRcvCmdFrame(RmMacCbT* pMac, RmFrmMhrT* pMhr, RmOctetT* pdata,
                             RmShortT len, RmLongT time, RmOctetT linkQuality,
                             RmBooleanT secUse, RmOctetT aclEntry, RmBooleanT isPendInAck,
                             RmShortT headerIELength, RmOctetT* headerIE,
                             RmShortT payloadIELength, RmOctetT* payloadIE);
void mlmeBuildFrameCountIndicationWMhr(RmMacCbT*                    pMac,
                                       RmMlmeFrameCountIndicationT* pRet,
                                       uint32_t                     framecounter);
#endif /* R_HYBRID_PLC_RF */
void RmCheckRxOnWhenIdle(RmMacCbT* pMac);

void mlmeBuildCommStatusIndicationWMhr(RmMacCbT* pMac,
                                       RmMlmeCommStatusIndicationT* pRet, uint8_t status,
                                       RmFrmMhrT* pMhr, uint8_t primid);

#ifdef R_HYBRID_PLC_RF
void mlmeBuildScanConfirm(RmMacCbT* pMac, RmMlmeScanConfirmT* pRet, RmOctetT status);
void mlmeEndCmdTrans(RmMacCbT* pMac, RmOctetT status);
void RmMlmeActionRcvBeaconFrame(RmMacCbT* pMac, RmFrmMhrT* pMhr, RmOctetT* pMsdu,
                                RmShortT msduLength, RmTimeT rftime, RmOctetT linkQuality,
                                RmBooleanT secUse, RmOctetT aclEntry, RmBooleanT secFail, RmMacEnumT status,
                                RmShortT headerIELength, RmOctetT* headerIE,
                                RmShortT payloadIELength, RmOctetT* payloadIE);
RmMacEnumT RmMlmeBeaconRequest(void* pmac, void* pMsg);
#else
RmMacEnumT RmMlmeSetWiSunPhyConfig(RmMacCbT* pMac, RmMlmeWiSunPhyConfigT* wisunphyconfig);
void       RmMlmeGetWiSunPhyConfig(RmMacCbT* pMac, RmMlmeWiSunPhyConfigT* wisunphyconfig);
#endif /* R_HYBRID_PLC_RF */
void RmMlmeActionRxOffIndication(void* pArgMac, void* pMsg);

void RmMlmeActionTransmitFail(RmMacCbT* pMac, RmMacEnumT macStatus, uint8_t* psdu);

RmMacEnumT RmMlmeWsAsyncFrameRequest(void* pmac, void* pMsg);

int16_t RmSetChannelConverted(uint8_t* wisun_channel);
void    RmGetChannelConverted(uint8_t* wisun_channel);

/* mac_rand.c */
void    RmSrand(uint16_t new_seed);
int16_t RmRand(void);

/* mac_util.c */
RoaErrT RmRelBlf(void* buf);
void    RmInitInterMsg(RmMacCbT* pMac);

/// @return ::r_os_msg_header_t
void* RmAllocUpMsg(RmMacCbT* pMac, size_t size);

void* RmRcvInterMsg(RmMacCbT* pMac);
void  RmSndInterMsg(RmMacCbT* pMac, void* pMsg);
void* RmAlcInterMsg(RmMacCbT* pMac);
void  RmRelInterMsg(RmMacCbT* pMac, void* pMsg);

void       RmPushAState(RmMacCbT* pMac, RmMacAStateT state);
void       RmPopAState(RmMacCbT* pMac);
RoaErrT    RmWaitTxComplete(RmMacCbT* pMac, RoaUintT waitFlgptn);
RmMACEnumT RmBackoffWait(RmMacCbT* pMac, uint32_t delayMs);

RmMacEnumT RmMlmeActionWsAsyncFrameIndication(RmMacCbT*  pMac,
                                              RmFrmMhrT* mhr,
                                              RmUBYTE    keyIndex,
                                              r_ie_wh_t* headerIE,
                                              r_ie_wp_t* payloadIE,
                                              uint8_t    linkQuality);

/**
 * Reset the whitelist and zero out all entries
 * @retval None
 */
void R_MAC_ResetWhitelist(void);

/**
 * Get all entries from the MAC whitelist.
 * @param[out] buf The output buffer where the whitelist entries should be written to.
 * @param buf_max_entries The maximum number of whitelist entries that can be stored in the output buffer.
 * @param[out] num_entries The number of whitelist entries that have been written to the output buffer on success.
 * @retval R_RESULT_SUCCESS if all whitelist entries have been written to the output buffer.
 * @retval R_RESULT_INSUFFICIENT_BUFFER if the output buffer is too small for all whitelist entries.
 */
r_result_t R_MAC_GetWhitelist(r_eui64_t* buf, size_t buf_max_entries, size_t* num_entries);

/**
 * Reset the MAC whitelist and add the specified addresses. If no addresses are specified, the whitelist is disabled.
 * @param mac_addresses The MAC addresses that should be written to the whitelist.
 * @param num_addresses The number of MAC addresses to be written.
 * @return R_RESULT_SUCCESS if all addresses have been successfully written to the MAC whitelist.
 * @return R_RESULT_INVALID_PARAMETER if the number of addresses exceeds the whitelist capacity.
 */
r_result_t R_MAC_SetWhitelist(const r_eui64_t* mac_addresses, size_t num_addresses);

/**
 * Check whether communication with the given src MAC address is permitted by the MAC whitelist or not.
 * @param mac_address The MAC address whose presence in the MAC whitelist should be verified.
 * @return True if the specified MAC address is on the whitelist or the whitelist is disabled. False otherwise.
 */
boolean_t R_MAC_IsWhitelisted(const RmDeviceAddressT* mac_address);

/******************************************************************************
 *  Copyright (C) 2010-2016 Renesas Electronics Corporation
 *****************************************************************************/
#endif  // #ifndef __RM_mac_intr_H
