/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* Copyright (C) 2020 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/**
 * @file r_nd_rpl_neighbors.h
 * @version 1.0
 * @brief The public API of the RPL neighbor management module
 */

#include <stdint.h>
#include "r_nwk_api_base.h"
#include "r_nd.h"

#ifndef R_ND_RPL_NEIGHBORS
#define R_ND_RPL_NEIGHBORS

// Map contiki type to r_ type
typedef r_nd_neighbor_cache_entry_t uip_ds6_nbr_t;

/**
 * Reset the configuration and internal state of the RPL neighbor management module
 * @param vp_nwkGlobal The global NWK information structure
 */
void R_ND_RPL_Reset(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Reset the internal state of the RPL neighbor management module while retaining its configuration
 * @param vp_nwkGlobal The global NWK information structure
 */
void R_ND_RPL_ResetState(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Lookup the entry with the given IP address in the neighbor cache
 * @param neighborCache The neighbor cache
 * @param address The IP address of the neighbor to lookup
 * @param[out] p_index The index of the matching neighbor entry (if found)
 * @return R_RESULT_SUCCESS if entry was found in the neighbor cache. Appropriate error code otherwise.
 */
r_result_t R_ND_RPL_LookupIpAddress(const r_nd_neighbor_cache_entry_t neighborCache[],
                                    const uint8_t                     address[],
                                    uint16_t* const                   p_index);

/**
 * Lookup the entry whose IP address either exactly matches the specified address or whose link-local address has the
 * same interface identifier.
 * @param ipaddr The IP address that should be searched in the RPL neighbor cache
 * @param vp_nwkGlobal The global NWK information structure
 * @return The corresponding RPL neighbor cache entry or NULL if the neighbor is not present in the cache.
 */
r_nd_neighbor_cache_entry_t* R_ND_RPL_LookupGuaOrLinkLocalAddr(const r_ipv6addr_t* ipaddr, void* vp_nwkGlobal);

/**
 * Lookup the entry with the given MAC address in the neighbor cache.
 * @deprecated This function is deprecated and will be removed in a future version. Use @ref R_ND_RPL_Lookup instead.
 * @param neighborCache The neighbor cache
 * @param address The MAC address of the entry that should be retrieved
 * @param addressLength The length of the MAC address in bytes
 * @param[out] p_index The index of the matching entry within the neighbor cache
 * @return R_RESULT_SUCCESS if entry was found. Appropriate error code otherwise.
 */
r_result_t R_ND_RPL_LookupMacAddress(const r_nd_neighbor_cache_entry_t neighborCache[],
                                     const uint8_t                     address[],
                                     const uint8_t                     addressLength,
                                     uint16_t* const                   p_index);

/**
 * Retrieve the entry with the given MAC address from the neighbor cache.
 * @param llAddress The MAC address of the entry that should be retrieved
 * @param vp_nwkGlobal The global NWK information structure
 * @return The requested neighbor cache entry if it exists; NULL otherwise.
 */
r_nd_neighbor_cache_entry_t* R_ND_RPL_Lookup(const r_eui64_t* llAddress, void* vp_nwkGlobal);

/**
 * Retrieve the entry with the given MAC address from the neighbor cache or try to add it if does not exist
 * @param llAddress The MAC address of the entry that should be retrieved or added
 * @param vp_nwkGlobal The global NWK information structure
 * @return The requested neighbor cache entry if it exists or could be created; NULL otherwise.
 */
r_nd_neighbor_cache_entry_t* R_ND_RPL_LookupOrAdd(const r_eui64_t* llAddress, void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Remove the specified entry from the neighbor cache
 * @details This operation fails if the removal criteria for the specified neighbor are not met (e.g. if its a parent)
 * @param nbr The neighbor entry that should be removed
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the entry was successfully removed from the cache. Appropriate error code otherwise.
 */
r_result_t R_ND_RPL_Remove(r_nd_neighbor_cache_entry_t* nbr, void* vp_nwkGlobal);

/**
 * Add the given entry to the neighbor cache if there is a free slot left or an existing entry can be evicted
 * @param p_entry The entry that should be added to the neighbor cache
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the entry was successfully added to the cache. Appropriate error code otherwise.
 */
r_result_t R_ND_RPL_AddEntry(r_nd_neighbor_cache_entry_t* const p_entry, void* vp_nwkGlobal);

#if R_LEAF_NODE_ENABLED
/**
 * Callback function if a neighbor has successfully injected a route for our address registration
 * @details This function should be called on reception of the corresponding Neighbor Advertisement with an EARO
 * @param ipAddress The IP address of the neighbor that injected our address registration
 * @param vp_nwkGlobal The global NWK information structure
 */
void R_ND_RPL_NeighborHasInjectedOurRouteToRpl(const uint8_t* ipAddress, void* vp_nwkGlobal);
#endif

/**
 * Callback function if a neighbor has rejected our address registration
 * @details This function should be called on reception of a Neighbor Advertisement with an ARO status != SUCCESS
 * @param ipAddress The IP address of the neighbor that rejected our address registration
 * @param vp_nwkGlobal The global NWK information structure
 */
void R_ND_RPL_NeighborHasRejectedAddressRegistration(const uint8_t* ipAddress, void* vp_nwkGlobal);

/**
 * Get the MAC address corresponding to the specified IP address from the neighbor cache
 * @param ipAddress The IP address to lookup
 * @param[out] llAddress The corresponding MAC address (if the IP address was found in the neighbor cache)
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the MAC address was successfully retrieved. Appropriate error code otherwise
 */
r_result_t R_ND_RPL_GetMacByIpAddress(const uint8_t ipAddress[], uint8_t const llAddress[], void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Get the IPv6 address corresponding to the specified MAC address from the neighbor cache
 * @param llAddress The MAC address to lookup
 * @param[out] ipAddress The corresponding IP address if the MAC address was found in the neighbor cache
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the IP address was successfully retrieved. Appropriate error code otherwise
 */
r_result_t R_ND_RPL_GetIpByMacAddress(const uint8_t llAddress[], uint8_t const ipAddress[], void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Retrieve the entry at the specified index from the neighbor cache.
 * @param index The index of the neighbor cache entry that should be retrieved
 * @param vp_nwkGlobal The global NWK information structure
 * @return The requested neighbor cache entry if it exists; NULL otherwise.
 */
r_nd_neighbor_cache_entry_t* R_ND_RPL_ReadEntry(const uint16_t index, void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Set the minimum lifetime of RPL neighbor cache entries that protects new entries from eviction
 * @param lifetimeSeconds The new minimum lifetime in seconds
 */
void R_ND_RPL_SetMinLifetime(uint16_t lifetimeSeconds);

/**
 * Get the current minimum lifetime of RPL neighbor cache entries that protects new entries from eviction
 * @return The current minimum lifetime of RPL neighbor cache entries in seconds
 */
uint16_t R_ND_RPL_GetMinLifetime();

/**
 * Get the link-local IPv6 address of the preferred parent
 * @param vp_nwkGlobal The global NWK information structure
 * @param[out] parentLinkLocalIp Output buffer for the parent's link-local IPv6 address
 * @retval R_RESULT_SUCCESS if the parent's link-local address was written to the output buffer
 * @retval R_RESULT_FAILED if the parent's link-local address could not be determined
 * @retval R_RESULT_ILLEGAL_NULL_POINTER if parentLinkLocalIp was NULL
 */
r_result_t R_ND_RPL_GetParentLinkLocalAddress(void* vp_nwkGlobal, uint8_t* parentLinkLocalIp);

/**
 * Get the link-local IPv6 address of the alternate parent
 * @param vp_nwkGlobal The global NWK information structure
 * @param[out] parentLinkLocalIp Output buffer for the parent's link-local IPv6 address
 * @retval R_RESULT_SUCCESS if the parent's link-local address was written to the output buffer
 * @retval R_RESULT_FAILED if the parent's link-local address could not be determined
 * @retval R_RESULT_ILLEGAL_NULL_POINTER if parentLinkLocalIp was NULL
 */
r_result_t R_ND_RPL_GetAlternateParentLinkLocalAddress(void* vp_nwkGlobal, uint8_t* parentLinkLocalIp);

#endif /* R_ND_RPL_NEIGHBORS */
