/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* (C) 2014-2015 Renesas Electronics Corporation All rights reserved.
*******************************************************************************/

/******************************************************************************
* File Name     : r_log_internal.h
* Description   : The logging API for the developers
******************************************************************************/
/******************************************************************************
* History       : DD.MM.YYYY Version Description
*               : 30.01.2015 1.00    First Release
*******************************************************************************/

#ifndef R_LOG_INTERNAL_H
#define R_LOG_INTERNAL_H

#include "r_log_flags.h"
#include "r_log_config.h"

/** Suppress compiler warning (-Wunused-but-set-variable) for variables that are only used for logging */
#define LOG_ONLY_VAR(x) ((void)(x))

#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF && !defined(__R_LOG_CODE_GENERATOR__)

#include "r_log_handler_internal.h"

#define R_LOG_TOKENPASTE2(x, y) x ## y
#define R_LOG_TOKENPASTE(x, y)  R_LOG_TOKENPASTE2(x, y)

#ifndef R_LOG_DISABLE_CONTEXT_SWITCH
#define R_LOG_DISABLE_CONTEXT_SWITCH()
#endif

#ifndef R_LOG_ENABLE_CONTEXT_SWITCH
#define R_LOG_ENABLE_CONTEXT_SWITCH()
#endif

#define R_LOG_ERR(fmt, ...)    R_LOG_TOKENPASTE(r_loggen_, __LINE__)(__VA_ARGS__)
#define R_LOG_ERRV(...)        R_LOG_TOKENPASTE(r_loggen_, __LINE__)(__VA_ARGS__)
#define R_LOG_ERR_FUNC_PARAM() R_LOG_TOKENPASTE(R_LOGGEN_, __LINE__)()


#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
#define R_LOG_WARN(fmt, ...)    R_LOG_TOKENPASTE(r_loggen_, __LINE__)(__VA_ARGS__)
#define R_LOG_WARNV(...)        R_LOG_TOKENPASTE(r_loggen_, __LINE__)(__VA_ARGS__)
#define R_LOG_WARN_FUNC_PARAM() R_LOG_TOKENPASTE(R_LOGGEN_, __LINE__)()
#else
#define R_LOG_WARN(...)         (void)0
#define R_LOG_WARNV(...)        (void)0
#define R_LOG_WARN_FUNC_PARAM() (void)0
#endif

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
#define R_LOG_DBG(fmt, ...)    R_LOG_TOKENPASTE(r_loggen_, __LINE__)(__VA_ARGS__)
#define R_LOG_DBGV(...)        R_LOG_TOKENPASTE(r_loggen_, __LINE__)(__VA_ARGS__)
#define R_LOG_DBG_FUNC_PARAM() R_LOG_TOKENPASTE(R_LOGGEN_, __LINE__)()
#else
#define R_LOG_DBG(...)         (void)0
#define R_LOG_DBGV(...)        (void)0
#define R_LOG_DBG_FUNC_PARAM() (void)0
#endif

#elif defined(__R_LOG_CODE_GENERATOR__)

/* do nothing (necessary for correct code generation) */

#else /* R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF && !defined(__R_LOG_CODE_GENERATOR__) */

#define R_LOG_ERR(...)          (void)0
#define R_LOG_ERRV(...)         (void)0
#define R_LOG_ERR_FUNC_PARAM()  (void)0
#define R_LOG_WARN(...)         (void)0
#define R_LOG_WARNV(...)        (void)0
#define R_LOG_WARN_FUNC_PARAM() (void)0
#define R_LOG_DBG(...)          (void)0
#define R_LOG_DBGV(...)         (void)0
#define R_LOG_DBG_FUNC_PARAM()  (void)0

#endif /* R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF && !defined(__R_LOG_CODE_GENERATOR__) */

#endif /* R_LOG_INTERNAL_H */
