/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_nd.h
 * Version     : 1.0
 * Description : Header file for the neighbor discovery module
 ******************************************************************************/

/*!
   \file      r_nd.h
   \version   1.0
   \brief     Header file for the neighbor discovery module
 */

#ifndef R_ND_H
#define R_ND_H

#include "r_nwk_api_base.h"
#include "r_ipv6_headers.h"
#include "r_icmpv6_nd.h"
#include "r_io_vec.h"
#include "sys/clock.h"

/******************************************************************************
   Macro definitions
******************************************************************************/
#define R_ND_EUI64_LEN       (8u)            // <! EUI-64 length
#define R_ND_INIFINITY_TIMER (0xFFFFFFFFuL)  // <! Infinity timer (all ones)

/******************************************************************************
   Typedef definitions
******************************************************************************/

struct rpl_dag;

/*!
    \struct r_nd_neighbor_cache_entry_t
    \brief Structure for neighbor cache entries
 */
typedef struct
{
    uint8_t         ipAddress[R_IPV6_ADDRESS_LENGTH]; //!< On-link unicast IP address
    uint8_t         llAddress[R_ND_EUI64_LEN];        //!< Link-layer address
    uint16_t        linkMetric;                       //!< RPL link metric (ETX)
    uint32_t        lastNsAttemptSeconds;             //!< Last time a NS has been (tried to) sent to this neighbor
    uint8_t         isUsed : 1;                       //!< Is this cache entry used
    uint8_t         isLocked : 1;                     //!< Is locked because it is preferred or alternate parent
    uint8_t         isCandidate : 1;                  //!< Is part of the candidate set
    uint8_t         hasRejectedOurAro : 1;            //!< Has rejected our ARO (via NA) and may not be used as a candidate
    uint32_t        minLifetimeSeconds;               //!< The timestamp until this entry must not be removed
    // Link information
    uint8_t         nodeToNeighRsl;                   //!< EWMA of the RSL for the node-to-neighbor direction
    uint8_t         neighToNodeRsl;                   //!< EWMA of the RSL for the neighbor-to-node direction
#if R_DEV_TBU_ENABLED
    uint8_t         rssi;                             //!< RSSI of the last message (only used for TBU API)
#endif
    uint16_t        transmissionAttempts;             //!< Frame transmission attempts in the current epoch
    uint16_t        receivedAcks;                     //!< Received ACKs in the current epoch
    uint32_t        lastHeard;                        //!< Last time we received a msg or ack from this neighbor
    // RPL Parent information
    uint16_t        rank;                             //!< RPL rank
    uint8_t         dtsn;                             //!< RPL Destination Advertisement Trigger Sequence Number
    struct rpl_dag* dag;                              //!< RPL DAG
    uint8_t         flags;                            //!< RPL parent flags
} r_nd_neighbor_cache_entry_t;

/*!
    \struct r_nd_default_route_t
    \brief Structure for default route
 */
typedef struct
{
    r_nd_neighbor_cache_entry_t* neighbor;             //!< Pointer to neighbor cache entry
    unsigned long                lastNsWithAroSeconds; //!< Last time we successfully sent a Neighbor Solicitation with ARO
} r_nd_default_route_t;

/**
 * Management structure for a single IPv6 address
 */
typedef struct
{
    r_ipv6addr_t address;    //!< IPv6 address
    uint8_t      state;      //!< State of the IPv6 address
    uint8_t      type;       //!< Type of address
    uint8_t      isInfinite; //!< Infinite valid time flag
    uint8_t      isUsed;     //!< Flag to identify if the address is used
} r_nd_ipv6_addr_entry_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn void R_ND_Init(void)
    \brief Initializes the neighbor discovery process
    \return void
 */
void R_ND_Init(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/*!
    \fn void R_ND_Init(void)
    \brief Initializes the neighbor discovery process
    \return void
 */
void R_ND_Reset(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Send a neighbor solicitation defined by the given parameters
 * @param targetAddress The IP address of the target of the solicitation (written to the "Target Address" field)
 * @param srcAddress The IP source address. If an ARO is included, this IP address is registered.
 * @param dstAddress The IP destination address
 * @param eui64 The EUI-64 used for the address registration (ARO)
 * @param registrationLifetime The lifetime of the address registration (ARO)
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the Neighbor Solicitation was successfully sent. Appropriate error code otherwise
 */
r_result_t R_ND_SendNeighborSolicitation(const uint8_t targetAddress[],
                                         const uint8_t srcAddress[],
                                         const uint8_t dstAddress[],
                                         const uint8_t eui64[],
                                         uint16_t      registrationLifetime,
                                         void*         vp_nwkGlobal); // void* -> r_nwk_cb_t*

/*!
    \fn r_result_t R_ND_ProcessNeighborAdvertisement(const uint8_t eui64[],
                                                        const r_ipv6_hdr_t* const p_ipv6Hdr,
                                                        const r_iovec_class_t* const p_icmpIoVec)
    \brief Process incoming neighbor advertisement message
           (<a href="../../eap_neighborDiscovery/index.htm">Procedure described here</a>)
    \param[in]  eui64       EUI-64 of the interface on which the message arrived (used for checking of options)
    \param[in]  p_ipv6Hdr   Pointer to IPv6 header of the neighbor advertisement
    \param[in]  p_icmpIoVec Pointer to iovec containing the ICMP message and possible options
    \return R_ND_RESULT_SUCCESS or other values
 */
r_result_t R_ND_ProcessNeighborAdvertisement(const uint8_t                eui64[],
                                             const r_ipv6_hdr_t* const    p_ipv6Hdr,
                                             const r_iovec_class_t* const p_icmpIoVec,
                                             void*                        vp_nwkGlobal);     // void* -> r_nwk_cb_t*

/*!
    \fn r_result_t R_ND_ProcessNeighborSolicitation(const uint8_t eui64[],
                                                       const uint8_t ipAddress[],
                                                       const r_ipv6_hdr_t* const p_ipv6Hdr,
                                                       const r_iovec_class_t* const p_icmpIoVec)
    \brief Process incoming neighbor solicitation message
           (<a href="../../eap_neighborDiscovery/index.htm">Procedure described here</a>)
    \param[in]  eui64       EUI-64 of the interface on which the message arrived
    \param[in]  ipAddress   Own IP address, to be used as source address
    \param[in]  p_ipv6Hdr   Pointer to IPv6 header of the neighbor solicitation
    \param[in]  p_icmpIoVec Pointer to iovec containing the ICMP message and possible options
    \return R_ND_RESULT_SUCCESS or other values
 */
r_result_t R_ND_ProcessNeighborSolicitation(const uint8_t                eui64[],
                                            const uint8_t                ipAddress[],
                                            const r_ipv6_hdr_t* const    p_ipv6Hdr,
                                            const r_iovec_class_t* const p_icmpIoVec,
                                            void*                        vp_nwkGlobal); // void* -> r_nwk_cb_t*

/*!
\fn r_result_t R_ND_CheckOptionsLength(const r_iovec_class_t* const p_icmpIoVec)
\brief Check if all options are of length different than zero
\param[in] p_icmpIoVec  Pointer to iovec structure including the options starting at position 1
\return R_ND_RESULT_SUCCESS if all options are of non-zero length, or other values
*/
r_result_t R_ND_CheckOptionsLength(const r_iovec_class_t* const p_icmpIoVec);

/*!
    \fn void r_result_t R_ND_LookupOption(const r_iovec_class_t* const p_icmpIoVec,
                                                      const uint8_t type,
                                                      r_boolean_t* const p_found,
                                                      uint8_t* const p_index)
    \brief Lookup for a specific option in a message
    \param[in]  p_icmpIoVec Pointer to iovec structure including the options starting at position 1
    \param[in]  type        Option type to look for
    \param[out] p_found     Pointer to where to store the result of the search
    \param[out] p_index     Pointer to where to store the index of matching entry
    \return None
 */
void R_ND_LookupOption(const r_iovec_class_t* const p_icmpIoVec,
                       const uint8_t                type,
                       r_boolean_t* const           p_found,
                       uint8_t* const               p_index);

/**
 * Send a Neighbor Advertisement (NA) message including an Address Registration Option (ARO)
 * @param p_ipv6Hdr The IPv6 header that should be used to send the neighbor advertisement
 * @param targetAddress Target address to use within the neighbor advertisement message
 * @param aroStatus The value of the status field within the ARO option
 * @param registrationLifetime The registration lifetime that should be set within the ARO option
 * @param routerFlag Value of the router flag within the neighbor advertisement
 * @param solicitedFlag Value of the solicited flag within the neighbor advertisement
 * @param overrideFlag Value of the override flag within the neighbor advertisement
 * @param eui64 EUI-64 to be used for the ARO option or TLLA option within the Neighbor Advertisement
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the NA was successfully sent; Appropriate error code otherwise
 */
r_result_t R_ND_SendNeighborAdvertisementWithARO(r_ipv6_hdr_t* const  p_ipv6Hdr,
                                                 const uint8_t        targetAddress[],
                                                 const r_aro_status_t aroStatus,
                                                 const uint16_t       registrationLifetime,
                                                 const r_boolean_t    routerFlag,
                                                 const r_boolean_t    solicitedFlag,
                                                 const r_boolean_t    overrideFlag,
                                                 const uint8_t        eui64[],
                                                 void*                vp_nwkGlobal); // void* -> r_nwk_cb_t*

/**
 * Send a Neighbor Advertisement (NA) message including an Extended Address Registration Option (EARO)
 * @param p_ipv6Hdr The IPv6 header that should be used to send the neighbor advertisement
 * @param targetAddress Target address to use within the neighbor advertisement message
 * @param earoStatus The value of the status field within the EARO option
 * @param earoFlags The EARO flags (RSVD, I, R, T) that should be set within the EARO option
 * @param earoTransactionId The transaction ID (TID) that should be set within the EARO option
 * @param registrationLifetime The registration lifetime that should be set within the EARO option
 * @param routerFlag Value of the router flag within the neighbor advertisement
 * @param solicitedFlag Value of the solicited flag within the neighbor advertisement
 * @param overrideFlag Value of the override flag within the neighbor advertisement
 * @param eui64 EUI-64 to be used for the EARO option or TLLA option within the Neighbor Advertisement
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the NA was successfully sent; Appropriate error code otherwise
 */
r_result_t R_ND_SendNeighborAdvertisementWithEARO(r_ipv6_hdr_t* const  p_ipv6Hdr,
                                                  const uint8_t        targetAddress[],
                                                  const r_aro_status_t earoStatus,
                                                  uint8_t              earoFlags,
                                                  uint8_t              earoTransactionId,
                                                  const uint16_t       registrationLifetime,
                                                  const r_boolean_t    routerFlag,
                                                  const r_boolean_t    solicitedFlag,
                                                  const r_boolean_t    overrideFlag,
                                                  const uint8_t        eui64[],
                                                  void*                vp_nwkGlobal); // void* -> r_nwk_cb_t*

#if (R_WISUN_FAN_VERSION >= 110)
struct rpl_dao_control;

/**
 * Finish the LFN registration after the DAO proxy exchange is done
 * @details This function should be called when a DAO ACK was received for the respective LFN or its transmission failed
 * @param lfn_address The IPv6 address of the LFN
 * @param tid The transaction ID that was advertised by the LFN (in its NS)
 * @param lifetime The address lifetime that was advertised by the LFN (in its NS)
 * @param daoAckStatus  The status of the proxy DAO exchange (from DAO ACK)
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the registration was completed successfully; Appropriate error code otherwise
 */
r_result_t R_ND_FinishLfnRegistration(const r_ipv6addr_t* lfn_address, uint8_t tid, uint16_t lifetime, uint8_t daoAckStatus, void* vp_nwkGlobal);
#endif

/**
 * Select a source IPv6 address based on the given destination address
 * If dstAddress == NULL, the srcAddress will be either a global address, or if not set, the link-local address
 * If dstAddress != NULL, the srcAddress matches the dstAddress as much as possible (i.e. both link-local or both global)
 * @param [out] srcAddress The source IPv6 address that should be set
 * @param dstAddress The destination IPv6 address for which the source address should be selected (may be NULL)
 * @return R_RESULT_ILLEGAL_NULL_POINTER if srcAddress is NULL, R_RESULT_SUCCESS otherwise
 */
r_result_t R_ND_SelectSrcAddress(uint8_t* srcAddress, const uint8_t* dstAddress);

/**
 * Returns a const pointer to our link-local IPv6 address
 * @return A const pointer to our link-local IPv6 address or NULL if no link-local address is available
 */
const r_ipv6addr_t* R_ND_GetOwnLinkLocalAddress();

/**
 * Get a copy of our own link-local IPv6 address
 * @param[out] outAddr The output variable where the link-local IPv6 address should be stored
 * @retval R_RESULT_SUCCESS if the link-local IPv6 address was successfully copied. Appropriate error code otherwise.
 */
r_result_t R_ND_CopyOwnLinkLocalAddress(r_ipv6addr_t* outAddr);

/**
 * Returns a const pointer to our global IPv6 address
 * @return A const pointer to our global IPv6 address or NULL if no global address is available
 */
const r_ipv6addr_t* R_ND_GetOwnGlobalAddress();

/**
 * Get a copy of our own global IPv6 address
 * @param[out] outAddr The outAddr variable where the global IPv6 address should be stored
 * @return R_RESULT_SUCCESS if the global IPv6 is available and was successfully copied. Appropriate error code otherwise.
 */
r_result_t R_ND_CopyOwnGlobalAddress(r_ipv6addr_t* outAddr);

#endif /* R_ND_H */
