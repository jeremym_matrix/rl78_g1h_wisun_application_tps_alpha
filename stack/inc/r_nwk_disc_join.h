/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name    : r_nwk_disc_join.h
 * Version      : 1.00
 * Description  : Header for the module that implements the network discovery and joining processing
 ******************************************************************************/

/*!
   \file    r_nwk_disc_join.h
   \version 1.0
   \brief   Header for the module that implements the network task processing
 */

#ifndef R_NWK_DISC_JOIN_H
#define R_NWK_DISC_JOIN_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_nwk_api_base.h"
#include "r_nwk_api.h"
#include "r_mac_wrapper.h"
#include "r_mac_ie.h"
#include "mac_mcps.h"
#include "net/uipopt.h"
#include "sys/ctimer.h"

extern uint16_t r_pan_timeout;                               //!< Interval over which a FAN node must have indication of liveness from Border Router
extern uint16_t r_target_panId;                              //!< PAN ID of the network to be joined to. The default value 0xffff disables PAN ID checking for network selection
extern uint16_t r_pan_id_blacklist[R_PAN_ID_BLACKLIST_SIZE]; //!< PAN ID blacklist
extern uint16_t r_auth_join_max_backoff;                     //!< Maximum auth timer backoff period
extern uint16_t r_auth_join_timer_base;                      //!< The initial retransmission time of of unsuccessful authentication.
extern uint8_t r_auth_join_timer_backoff_factor;             //!< The backoff factor of authentication retransmissions.

/******************************************************************************
   Functions Prototypes
******************************************************************************/

/**
 * Initialize the Discovery and Join module.
 * @warning This function must be called before any other function within this module is used.
 * @param vp_nwkGlobal The global NWK information structure.
 */
void R_NWK_FAN_DiscJoin_Init(void* vp_nwkGlobal);

uint16_t R_NWK_FAN_DiscJoin_GetPANVersion(void);

#if R_BORDER_ROUTER_ENABLED || R_LEAF_NODE_ENABLED || R_LFN_PARENTING_ENABLED
uint16_t R_NWK_FAN_DiscJoin_GetLfnVersion(void);
#endif

r_result_t R_NWK_FAN_DiscJoin_Receive(r_mac_mlme_wsasyncframe_ind_t* p_WSAsyncFrame,
                                      void*                          vp_nwkGlobal);

/**
 * Process an incoming EAPOL message.
 * @param ind The indication of the EAPOL message that should be processed. The message is freed within this function
 * so the pointer MUST NOT be used after calling this function.
 * @param vp_nwkGlobal The global NWK information structure.
 * @return R_RESULT_SUCCESS, if the EAPOL message was processed successfully. Otherwise, an appropriate error code.
 */
r_result_t R_NWK_Eapol_Receive(RmMcpsEapolIndicationT* ind, void* vp_nwkGlobal);

/**
 * Handle updated authentication state (after processing or transmitting an EAPOL message)
 * @param vp_nwkGlobal The global NWK information structure.
 */
void R_NWK_FAN_DiscJoin_HandleAuthUpdate(r_result_t recentResult, void* vp_nwkGlobal);

void       R_NWK_FAN_DiscJoin_Set_JoinState1(void);
r_result_t R_NWK_FAN_DiscJoin_Set_JoinState5(void);

void R_NWK_Reset_PAN_Timeout_Timer(void);

/**
 * Retrieve the current join state of this device
 * @return The current join state of this device
 */
r_nwk_join_state_t R_NWK_FAN_DiscJoin_GetJoinState(void);

#if R_BORDER_ROUTER_ENABLED
void R_NWK_FAN_DiscJoin_BR_IncreasePanVersion(void* vp_nwkGlobal);
void R_NWK_FAN_DiscJoin_IncreaseLfnVersion(void* vp_nwkGlobal);
#endif

#if (R_WISUN_FAN_VERSION >= 110)
#if (R_LEAF_NODE_ENABLED)
/**
 * Reset the PARENT_LOST timer
 * @details This function should be called whenever a frame is received from our FFN parent.
 */
void R_NWK_Reset_ParentLost_Timer(void);
#else
void R_NWK_SetLfnBroadcastSyncTimer(void);
#endif /* (R_LEAF_NODE_ENABLED) */
#endif /* (R_WISUN_FAN_VERSION >= 110) */

#endif /* R_NWK_DISC_JOIN_H */
