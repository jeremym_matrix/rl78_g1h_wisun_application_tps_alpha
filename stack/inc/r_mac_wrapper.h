/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_mac_wrapper.h
 * Version     : 1.0
 * Description : This module provides unified MAC layer access
 ******************************************************************************/

/**
 * @file      r_mac_wrapper.h
 * @version   1.0
 * @brief     This module provides unified MAC layer access
 */

#ifndef R_MAC_WRAPPER_H
#define R_MAC_WRAPPER_H

#include "mac_mcps.h"
#include "mac_mlme.h"
#include "mac_types.h"
#include "r_io_vec.h"
#include "r_frag.h"

/******************************************************************************
   Macro definitions
******************************************************************************/

#define R_MAC_EUI64_LEN (8u)  //!< The length of an EUI-64 address in bytes

/******************************************************************************
   Typedefs
******************************************************************************/

/**
 * MAC addressing mode
 */
typedef enum
{
    R_MAC_ADDRESS_MODE_NONE     = 0, //!< No address
    R_MAC_ADDRESS_MODE_EXTENDED = 3  //!< Extended addressing mode

} r_mac_addressing_mode_t;

/**
 * MCPS-DATA indication
 */
typedef struct
{
    uint8_t                 srcAddr[8];      //!< The individual device address of the entity from which the MSDU is being transferred
    uint8_t                 dstAddr[8];      //!< The individual device address of the entity to which the MSDU is being transferred
    uint8_t*                msdu;            //!< The set of octets forming the MSDU being indicated by the MAC sublayer entity
    uint16_t                srcPanId;        //!< The 16-bit PAN identifier of the entity from which the MSDU was received
    uint16_t                dstPanId;        //!< The 16-bit PAN identifier of the entity to which the MSDU is being transferred
    uint16_t                msduLength;      //!< The number of octets contained in the MSDU being indicated by the MAC sublayer entity
    r_mac_addressing_mode_t srcAddrMode;     //!< Source addressing mode
    r_mac_addressing_mode_t dstAddrMode;     //!< Destination addressing mode
    uint8_t                 mpduLinkQuality; //!< LQI value measured during reception of the MPDU. Lower values represent lower LQI
    r_boolean_t             secured;         //!< Was the frame encrypted
} r_mac_mcps_data_ind_t;

/**
 * MLME-WS-ASYNC-FRAME indication
 */
typedef struct
{
    uint8_t    srcAddr[8];  //!< The individual device address
    uint16_t   srcPanId;    //!< The 16-bit PAN identifier
    uint8_t    linkQuality; //!< The Link Quality (RSSI)
    uint8_t    keyIndex;    //!< The key Index (valid for Pan Configuration and enabled security)
    r_ie_wh_t* p_headerIE;  //!< Pointer to Header IEs
    r_ie_wp_t* p_payloadIE; //!< Pointer to Payload IEs
} r_mac_mlme_wsasyncframe_ind_t;

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/**
 * Parse a message from the MAC layer
 * @param[in] p_msg The message. It is freed within this function so the pointer MUST NOT be used afterwards
 * @param[in,out] vp_nwkGlobal The global NWK information structure
 */
void R_MAC_Parse(RoaMsgT p_msg, void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Convert an incoming MAC frame to the internally used format
 * @param[in] ind_in The incoming MCPS data indication
 * @param[out] ind_out The converted MCPS data indication
 * @return R_RESULT_SUCCESS in case of proper processing, R_RESULT_FAILED otherwise
 */
r_result_t R_MAC_HndMcpsDataIndication(RmMcpsDataIndicationT* ind_in, r_mac_mcps_data_ind_t* ind_out);

/**
 * Send an MCPS data request to the MAC layer
 * @param[in] inIovec Void pointer to a iovec class describing the payload to be sent
 * @param[in] srcAddrMode Addressing mode for the source address
 * @param[in] dstAddrMode Addressing mode for the destination address
 * @param[in] dstAddr Destination address
 * @param[in] txOptions Tx options (enable/disable security)
 * @param[in] pointer to NWK global.
 * @return R_RESULT_SUCCESS in case of proper processing. R_RESULT_FAILED otherwise
 */
r_result_t R_MAC_HndMcpsDataRequest(r_iovec_class_t*        inIovec,
                                    r_mac_addressing_mode_t srcAddrMode,
                                    r_mac_addressing_mode_t dstAddrMode,
                                    uint8_t*                dstAddr,
                                    uint16_t                txOptions,
                                    void*                   vp_nwkGlobal);    // void* -> r_nwk_cb_t*

#if !R_EDFE_INITIATOR_DISABLED
/**
 * Send an MCPS data request to the mac layer
 * @param[in] inIovec Void pointer to a iovec class describing the payload to be sent
 * @param[in] srcAddrMode Addressing mode for the source address
 * @param[in] dstAddrMode Addressing mode for the destination address
 * @param[in] dstAddr Destination address
 * @param[in] txOptionsTx options (enable/disable security)
 * @param[in] fragResult result of fragmentation
 * @param[in,out] vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS in case of proper processing. R_RESULT_FAILED otherwise
 */
r_result_t R_MAC_HndMcpsEDFEDataRequest(r_iovec_class_t*        inIovec,
                                        r_mac_addressing_mode_t srcAddrMode,
                                        r_mac_addressing_mode_t dstAddrMode,
                                        uint8_t*                dstAddr,
                                        uint16_t                txOptions,
                                        r_frag_result_t         fragResult,
                                        void*                   vp_nwkGlobal);    // void* -> r_nwk_cb_t*
#endif /* !R_EDFE_INITIATOR_DISABLED */


/**
 * Convert an incoming asynchronous MAC frame to the internally used format
 * @param[in] ind_in The incoming MLME-WS-ASYNC-FRAME indication
 * @param[out] ind_out The converted MLME-WS-ASYNC-FRAME indication
 * @return R_RESULT_SUCCESS in case of proper processing, R_RESULT_FAILED otherwise
 */
r_result_t R_MAC_HndMlmeWSAsyncIndication(RmMlmeWSAsyncFrameIndicationT* ind_in,
                                          r_mac_mlme_wsasyncframe_ind_t* ind_out);

r_result_t R_MAC_MlmeWSAsyncReq(r_ie_wh_frame_type_t frametype, uint8_t currentChannel, void* vp_nwkGlobal);

/**
 * Retrieve the extended address (EUI-64) of this device
 * @param[out] address The output buffer that the address should be written to
 * @param[in,out] vp_nwkGlobal The global NWK information structure
 */
void R_MAC_GetExtendedAddress(uint8_t address[], void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*



#if !R_DEV_DISABLE_AUTH
/**
 * Send an EAPOL message that is embedded in a MAC compatible data buffer (OS header and MCPS data request)
 * @param src The EUI-64 source address
 * @param dst The EUI-64 destination address
 * @param[in,out] data A data buffer representing an MCPS Data Request including an MPX-IE with the EAPOL data to send.
 * Ownership is transferred so this pointer MUST NOT be used after calling this function.
 * @param dataSize The size of the data buffer
 * @param authenticator The authenticator EUI-64 for the EA-IE, which may be NULL
 * @param[in,out] vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS in case of successful processing and transmission. Appropriate error code otherwise
 */
r_result_t R_MAC_EapolRequestRaw(const uint8_t* src,
                                 const uint8_t* dst,
                                 uint8_t*       data,
                                 uint16_t       dataSize,
                                 const uint8_t* authenticator,
                                 void*          vp_nwkGlobal);

/**
 * Send an EAPOL message using the MPX-IE
 * @param src The EUI-64 source address
 * @param dst The EUI-64 destination address
 * @param type The EAPOL type (with send-only extensions)
 * @param data The data to send
 * @param size The size of data
 * @param authenticator The authenticator EUI-64 for the EA-IE, which may be NULL
 * @param[in,out] vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS in case of successful processing and transmission. Appropriate error code otherwise
 */
r_result_t R_MAC_EapolReq(const uint8_t* src,
                          const uint8_t* dst,
                          r_mac_eapol_type_t type,
                          const uint8_t* data, uint16_t size,
                          const uint8_t* authenticator,
                          void* vp_nwkGlobal);
#endif /* !R_DEV_DISABLE_AUTH */

r_result_t R_MAC_ResetReq(r_boolean_t pibResetFlg, void* vp_nwkGlobal);                                                               // void* -> r_nwk_cb_t*
r_result_t R_MAC_SetReq(uint8_t pibId, void* val, uint16_t len, void* vp_nwkGlobal);                                                  // void* -> r_nwk_cb_t*
r_result_t R_MAC_GetReq(uint8_t attrId, void* p_attrValue, uint16_t attrLength, void* vp_nwkGlobal);                                  // void* -> r_nwk_cb_t*
r_result_t R_MAC_GetReqIndexed(uint8_t attrId, const uint8_t* attrIndex, void* p_attrValue, uint16_t attrLength, void* vp_nwkGlobal); // void* -> r_nwk_cb_t*

void R_MAC_Get_Broadcast_Schedule_Information(r_ie_wp_broadcast_schedule_t* broadcast_schedule);
void R_MAC_Get_Unicast_Schedule_Information(r_ie_wp_unicast_schedule_t* unicast_schedule);


#if (R_WISUN_FAN_VERSION >= 110)
/**
 * Send an directed LFN PAN frame
 * @param[in] frametype The LFN PAN frame type LFN_PAN_ADVERT, LFN_PAN_CONFIG_SOLICIT or LFN_PAN_CONFIG
 * @param[in] dstAddr Destination address
 * @param[in,out] vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the PAN frame was successfully sent. Appropriate error code otherwise
 */
r_result_t R_MAC_LfnPanRequest(r_ie_wh_frame_type_t frametype,
                               const uint8_t*       dst,
                               void*                vp_nwkGlobal);

#if !R_LEAF_NODE_ENABLED
/**
 * Send an LFN Time Sync (LTS) frame
 * @param[in,out] vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the LTS frame was successfully sent. Appropriate error code otherwise
 */
r_result_t R_MAC_LfnTimeSyncRequest(void* vp_nwkGlobal);
#endif /* !R_LEAF_NODE_ENABLED */
#endif /* (R_WISUN_FAN_VERSION >= 110) */

#endif /* R_MAC_WRAPPER_H */
