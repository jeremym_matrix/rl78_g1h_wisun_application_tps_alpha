/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corp. and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corp. and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *****************************************************************************/
#ifndef __RM_frequency_hopping_H
#define __RM_frequency_hopping_H
/******************************************************************************
 *  File Name       : r_frequency_hopping.h
 *  Description     : The frequency hopping functions.
 ******************************************************************************
 *  Copyright (C) 2017 Renesas Electronics
 ******************************************************************************/

#include "r_stdint.h"
#include "mac_intr.h"
#include "mac_mcps.h"
#include "r_schedule_utils.h"

/******************************************************************************
    Type Definitions
******************************************************************************/

typedef struct
{
    RmMcpsDataRequestT*  pDataReq;
    volatile r_boolean_t scheduled;
} RmFrameToTx;

typedef struct
{
    r_ie_wp_broadcast_schedule_t broadcast;
    r_ie_wp_unicast_schedule_t   unicast;
} r_mac_schedule_ie_t;

typedef struct
{
    r_computed_broadcast_schedule_t* broadcast;
    r_computed_unicast_schedule_t*   unicast;
} r_mac_computed_schedule_t;

/******************************************************************************
    Definitions
******************************************************************************/

/* Frequency Hopping timer interval in ms */
#define  R_FREQ_HOPP_TIMER_INTERVAL_MS (1)

/******************************************************************************
    Prototypes
******************************************************************************/
uint16_t DH1CF_getUnicastChannelIndex(uint16_t slotNumber, RmDeviceAddressT* addr, uint32_t nChannels);
uint16_t DH1CF_getBroadcastChannelIndex(uint16_t slotNumber, uint16_t broadcastSchedID, uint32_t nChannels);

/**
 * Update the unicast schedule of the specified neighbor according to a Unicast Schedule Information Element (US-IE)
 * @param addr The MAC address of the neighbor, whose unicast schedule should be updated
 * @param sched_ie The US-IE with the updated schedule information
 * @return R_RESULT_SUCCESS if the updated schedule info was successfully stored. Appropriate error code otherwise
 */
r_result_t RmNeighborScheduleListUpdateSchedule(RmDeviceAddressT* addr, r_ie_wp_unicast_schedule_t* sched_ie);

r_result_t RmNeighborScheduleListUpdateLocalTiming(RmDeviceAddressT* addr, uint32_t receiveTime, uint32_t ufsi);

RmMACEnumT RmSetUnicastChannel(RmDeviceAddressT* addr, uint8_t sec_delay_ms);

uint32_t RmCalcRxPropagationDelay(uint32_t rx_time_phy);
uint32_t RmCalcUnicastFractionalSequenceInterval(uint8_t sec_delay_ms);

RmMACEnumT RmBroadcastScheduleUpdatePermitted(const RmDeviceAddressT* src);
void       RmUpdateBroadcastIntervalTiming(uint32_t receiveTime, uint16_t slotNumber, uint32_t broadcast_interval_offset);
void       RmGetBroadcastSchedule(uint32_t* bcast_interval_offset, uint16_t* bcast_slotNumber, uint8_t sec_delay_ms);

/**
 * Compute and set the unicast schedule of this device based on the given US-IE. This function also sets the global
 * NWK fields for the unicast schedule IE as well as the computed unicast schedule.
 * @details This function may only be called before the device is started
 * @param vp_nwkGlobal The global NWK information structure
 * @param serialized_us_ie The serialized Unicast Schedule Information Element that should be set
 * @param size The size of the serialized US-IE in bytes
 * @return R_RESULT_SUCCESS if a new unicast schedule was computed and set based on the given US-IE.
 * Appropriate error code otherwise
 */
#ifdef R_HYBRID_PLC_RF
r_result_t RmSetUnicastSchedule(r_ie_wp_unicast_schedule_t schedule_ie);
#else
r_result_t RmSetUnicastSchedule(const uint8_t* serialized_us_ie, uint16_t size);
#endif
/**
 * Compute and set the broadcast schedule of this device based on the given BS-IE. This function also sets the global
 * NWK fields for the broadcast schedule IE and the computed broadcast schedule.
 * @details This function may only be called before the device is started
 * @param vp_nwkGlobal The global NWK information structure
 * @param serialized_bs_ie The serialized Broadcast Schedule Information Element that should be set
 * @param size The size of the serialized BS-IE in bytes
 * @return R_RESULT_SUCCESS if a new broadcast schedule was computed and set based on the given BS-IE.
 * Appropriate error code otherwise
 */
#ifdef R_HYBRID_PLC_RF
r_result_t RmSetBroadcastSchedule(r_ie_wp_broadcast_schedule_t schedule_ie);
#else
r_result_t RmSetBroadcastSchedule(const uint8_t* serialized_bs_ie, uint16_t size);
#endif
/**
 * Update the broadcast schedule of this device with the specified parent schedule. This function also sets the global
 * NWK pointers for the broadcast schedule IE and the corresponding computed schedule. It also sets the broadcast
 * schedule to be enabled for frequency hopping.
 * @param vp_nwkGlobal The global NWK information structure
 * @param parent_schedule The Broadcast Schedule Information Element (BS-IE) received by our parent
 * @return R_RESULT_SUCCESS if a new broadcast schedule was computed and set based on the parent BS-IE.
 * Appropriate error code otherwise
 */
r_result_t RmSetAndEnableBroadcastSchedule(const r_ie_wp_broadcast_schedule_t* parent_schedule);

RmMACEnumT RmSetBroadcastChannel(void);

r_result_t RmFrequencyHoppingStart(void);
void       RmFrequencyHoppingReset(void);
void       RmFrequencyHoppingRestart(void);

RmMACEnumT RmTxUnicastWindowActive(RmMacCbT* pMac, RmDeviceAddressT* dstAddr, uint8_t sec_delay_ms);

RmMACEnumT RmScheduleBroadcastFrameTransmit(RmMcpsDataRequestT* pReq);
RmMACEnumT RmScheduleUnicastFrameTransmit(RmMcpsDataRequestT* pReq);

uint8_t RmCalcEncryptionDelay(uint16_t payload_length, boolean_t security);
void    RmFreqHopp_LOCK_Channel(void);
void    RmFreqHopp_UNLOCK_Channel(void);
void    RmFreqHopp_Set_Phy_Tx_Delay(uint8_t delay);
uint8_t RmFreqHopp_Get_Phy_Tx_Delay(void);

RmMACEnumT RmUpdateHoppingChannel(uint8_t channel);
RmMACEnumT RmSetHoppingChannel(uint8_t channel);

void RmFreqHoppTimer_Handler(unsigned short msec_inc_step);

const r_mac_computed_schedule_t* RmGetComputedSchedulePtr(void);
const r_mac_schedule_ie_t*       RmGetScheduleIePtr(void);

void R_MAC_Get_Broadcast_Schedule_Information(r_ie_wp_broadcast_schedule_t* broadcast_schedule);
void R_MAC_Get_Unicast_Schedule_Information(r_ie_wp_unicast_schedule_t* unicast_schedule);

#endif // #ifndef __RM_frequency_hopping_H
