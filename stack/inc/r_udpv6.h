/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name    : r_udpv6.h
 * Version      : 1.00
 * Description  : Header for the module for processing of UDP messages
 ******************************************************************************/

/*!
   \file    r_udpv6.h
   \version 1.0
   \brief   Header for the module for processing of UDP messages
 */

#include "r_nwk_api_base.h"

#ifndef R_UDPV6_H
#define R_UDPV6_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/**
 * Result codes of UDP functions
 */
typedef enum
{
    R_UDP_RESULT_SUCCESS       = 0,  //!< Processing successful
    R_UDP_RESULT_FAILED        = 1,  //!< Processing failed
    R_UDP_RESULT_INVALID_VALUE = 2,  //!< Invalid value of data
    R_UDP_RESULT_NULL_POINTER  = 3,  //!< Input to a function was null pointer
    R_UDP_RESULT_BUFFER_ERROR  = 4,  //!< Some error occurred in buffer usage
    R_UDP_RESULT_UNKNOWN       = 5   //!< Default value to set before proper values have been set

} r_udp_result_t;

/**
 * UDP header structure
 */
typedef struct
{
    uint16_t srcPort;  //! Source port number
    uint16_t dstPort;  //! Destination port number
    uint16_t length;   //! UDP Length
    uint16_t checkSum; //! Checksum
} r_udp_hdr_t;


/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Variable Externs
******************************************************************************/

/******************************************************************************
   Functions Prototypes
******************************************************************************/

/**
 * Serialize a UDP header to a byte buffer
 * @param udpHdr Pointer to the UDP header structure that should be serialized
 * @param[out] output Output buffer to hold the serialized UDP header
 */
void R_UDP_PackHeader(const r_udp_hdr_t* udpHdr, uint8_t output[]);

/**
 * Deserialize a byte buffer containing a UDP header to the corresponding structure
 * @param input Pointer to a buffer where the packed UDP header is located
 * @param[out] udpHdr Output buffer to hold the unpacked UDP header
 */
void R_UDP_UnpackHeader(const uint8_t input[], r_udp_hdr_t* udpHdr);

r_udp_result_t R_UDP_Process(const uint8_t srcAddress[], const uint8_t dstAddress[], const uint8_t message[],
                             uint16_t length, r_boolean_t secured, uint8_t lqi,
                             void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/**
 * Determine if a UDP port is reserved for Wi-SUN stack operation
 * @param port The UDP port number
 * @return True, if the specified port is reserved for Wi-SUN stack operation. False otherwise.
 */
r_boolean_t R_UDP_IsReservedPort(uint16_t port);

#endif /* R_UDPV6_H */
