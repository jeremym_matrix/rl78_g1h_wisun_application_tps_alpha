/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011-2019 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_6lp_rx_processing.h
 * Version     : 2.0
 * Description : This module parses incoming frames
 ******************************************************************************/

/*!
   @file
   @version   2.0
   @brief     This module parses incoming MAC frames
 */

#ifndef R_6LP_RX_PROCESSING_H
#define R_6LP_RX_PROCESSING_H

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/**
 * Parse incoming MAC frames and call IPv6 header uncompression and fragment reassembly
 * @attention ipMessageSize must be >= R_HC_MAX_UNCOMPRESSD_HDR_LENGTH and ipMessage may be used
 * regardless of the return value
 * @param mcpsDataInd the incoming data indication
 * @param ipMessage the output buffer for the IP message
 * @param ipMessageSize the size of the output buffer for the IP message
 * @param vp_nwkGlobal pointer to nwkGlobal
 * @return the size of the reassembled IP message or 0 on error or fragments still missing
 */
uint16_t R_6LP_FrameParser(const r_mac_mcps_data_ind_t* mcpsDataInd,
                           uint8_t*                     ipMessage,
                           uint16_t                     ipMessageSize,
                           void*                        vp_nwkGlobal); // void* -> r_nwk_cb_t*

#endif /* R_6LP_RX_PROCESSING_H */
