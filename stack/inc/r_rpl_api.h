/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_rpl_api.h
 * Version     : 1.0
 * Description : This module implements the API for the RFC6550 RPL routing
 *               protocol
 ******************************************************************************/

/*!
   \file      r_rpl_api.h
   \version   1.0
   \brief     This module implements the API for the RFC6550 RPL routing
              protocol
 */

#ifndef R_RPL_API_H
#define R_RPL_API_H

#include "rpl.h"

/******************************************************************************
   Macro definitions
******************************************************************************/

/* RPL message types */
#define RPL_CODE_DIS                0x00 /* DAG Information Solicitation */
#define RPL_CODE_DIO                0x01 /* DAG Information Option */
#define RPL_CODE_DAO                0x02 /* Destination Advertisement Option */
#define RPL_CODE_DAO_ACK            0x03 /* DAO acknowledgment */

/**
 * Maximum ETX value according to  [FANTPS-1v32-6.2.3.1.6.1: Link Metrics]
 * @warning The actual link metric of a RPL neighbor never reaches this value because of the EWMA calculation!
 * The max link metric value is defined as R_RPL_MRHOF_MAX_LINK_METRIC.
 */
#define R_RPL_ETX_MAX               1024

#define R_RPL_DAO_CONTROL_OWN_DAO   0  /* The index in the dao control structure for own DAOs */
#define R_RPL_DAO_CONTROL_PROXY_DAO 1  /* The starting index in the dao control structure for proxy DAOs */

/******************************************************************************
   Typedef definitions
******************************************************************************/
typedef rpl_dag_t      r_rpl_dag_t;
typedef rpl_instance_t r_rpl_instance_t;
typedef rpl_parent_t   r_rpl_parent_t;
typedef enum rpl_mode  r_rpl_mode_t;

/******************************************************************************
   Exported global variables
******************************************************************************/
extern uint16_t r_rpl_nud_timeout;  // Neighbor unreachability timeout in seconds

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/**
 * Initialize the RPL module
 * @details Must be called before any other R_RPL_* function (exceptions are marked explicitly)
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS on successful initialization. Appropriate error code otherwise.
 */
r_result_t R_RPL_Init(void* vp_nwkGlobal);

/**
 * Stop the RPL module
 * @param vp_nwkGlobal The global NWK information structure
 */
void R_RPL_Stop(void* vp_nwkGlobal);

/**
 * Stop the RPL module and reset its configuration parameters to their default values
 * @param vp_nwkGlobal The global NWK information structure
 */
void R_RPL_Reset(void* vp_nwkGlobal);

#if R_BORDER_ROUTER_ENABLED
/*!
    \fn r_rpl_dag_t *R_RPL_SetRoot(uint8_t instance_id, r_ipv6addr_t * dag_id);
    \brief This function creates a new DAG with given instance ID and DAG ID.
    \details This function should be called from the application layer of the
             border router and sets the DAG as the root.
    \param[in] instance_id: Instance ID
    \param[in] dag_id: DAG ID
    \return Pointer to Directed Acyclic Graph structure
 */
r_rpl_dag_t* R_RPL_SetRoot(uint8_t instance_id, const r_ipv6addr_t* dag_id);

/**
 * Set the specified IPv6 address and update the DAG's prefix information
 * @details If a prefix was set previously, this function will also remove the corresponding IPv6 addresses.
 * @param dag Pointer to DAG structure.
 * @param prefix The new IPv6 address that should be set.
 * @param len The IPv6 prefix length in bits.
 * @return R_RESULT_SUCCESS if the new address and prefix were applied successfully. Appropriate error code otherwise.
 */
r_result_t R_RPL_SetPrefixAndAddress(r_rpl_dag_t* dag, const r_ipv6addr_t* newAddr, uint32_t len);

/**
 * Initiate global repair mechanism of RPL by incrementing the DTSN and DODAG version.
 * @details This will cause all nodes in the network to re-perform RPL parent selection.
 * @return R_RESULT_SUCCESS if the global repair was started; Appropriate error code otherwise.
 */
r_result_t R_RPL_InitiateGlobalRepair();

/**
 * Increment the Destination Advertisement Trigger Sequence Number (DTSN).
 * @details Incrementing the DTSN causes immediate child nodes to send a DAO and increment their own DTSN.
 * @return R_RESULT_SUCCESS if the DTSN was incremented; Appropriate error code otherwise.
 */
r_result_t R_RPL_IncrementDtsn();
#endif // R_BORDER_ROUTER_ENABLED

/*!
    \fn r_rpl_dag_t *R_RPL_GetAnyDag(void);
    \brief Returns pointer to an active DAG (used RPL module internal only).
    \return Pointer to Directed Acyclic Graph structure
 */
r_rpl_dag_t* R_RPL_GetAnyDag(void);

/**
 * Get a pointer to the RPL instance structure.
 * @details For Wi-SUN FAN, there is only a single RPL instance.
 * @return Pointer to the RPL instance structure
 */
r_rpl_instance_t* R_RPL_GetInstance();

/*!
    \fn rpl_dao_control_t* R_RPL_GetDaoControlByIdx(uint8_t dao_control_id)
    \brief This function gets a pointer to one of the entries in the global
    DAO control structure that takes care of timers and acks.
    \param[in] dao_control_id: The index of the DAO control structure
    \return Pointer to the entry corresponding to the index or NULL if it does not exist.
 */
rpl_dao_control_t* R_RPL_GetDaoControlByIdx(uint8_t dao_control_id);

#if R_LFN_PARENTING_ENABLED
/*!
    \fn rpl_dao_control_t* R_RPL_GetLfnDaoControlSlot(void);
    \brief This function gets a pointer to one of the free entries in the global
    DAO control structure that takes care of timers and acks. The first entry
    is reserved for our own DAOs, so the function looks for slots starting
    at 1.
    \return Pointer to the entry corresponding to the index or NULL if there is no free slot.
 */
rpl_dao_control_t* R_RPL_GetLfnDaoControlSlot(void);
#endif /* R_LFN_PARENTING_ENABLED */

/*!
    \fn rpl_dao_control_t* R_RPL_GetDaoControlFromSeqNo(uint8_t sequence)
    \brief This function gets a pointer to one of the entries in the global
    DAO control structure that takes care of timers and acks.
    \param[in] sequence: The sequence number used to search for the right DAO control
    \return Pointer to the entry corresponding to the sequence or NULL if it does not exist.
 */
rpl_dao_control_t* R_RPL_GetDaoControlFromSeqNo(uint8_t sequence);

/*!
    \fn void R_RPL_ResetDaoControl(void);
    \brief This function stops all timers and sets the whole structure to all zeros.
    \return This function does not return anything.
 */
void R_RPL_ResetDaoControl(void);

/*!
    \fn r_result_t R_RPL_UpdateHeaderEmpty(void);
    \param[in] rpl_opt_hdr_flags: RPL option header flags
    \brief This function updates the content of an empty RPL options header.
 */
r_result_t R_RPL_UpdateHeaderEmpty(uint8_t rpl_opt_hdr_flags);

/*!
    \fn r_result_t R_RPL_UpdateHeaderFinal(r_ipv6addr_t *addr);
    \brief This function updates the content of an already present RPL options header.
    \param[in] addr: Pointer to IPv6 address
    \param[in] forwarding: True if the packet is forwarded by this node
    \return R_RESULT_SUCCESS if successful, R_RESULT_FAILED otherwise
 */
r_result_t R_RPL_UpdateHeaderFinal(const r_ipv6addr_t* addr, r_boolean_t forwarding);

/*!
    \fn r_result_t R_RPL_VerifyHeader(int32_t uip_ext_opt_offset);
    \brief This function verifies the RPL options header contents.
    \param[in] uip_ext_opt_offset: Offset to extension headers
    \param[out] direction_up: direction of the frame
    \param[out] rpl_opt_hdr_flags: RPL option header flags
    \return R_RESULT_SUCCESS if successful, R_RESULT_FAILED otherwise
 */
r_result_t R_RPL_VerifyHeader(int32_t uip_ext_opt_offset, r_boolean_t* direction_up, uint8_t* rpl_opt_hdr_flags);

/*!
    \fn void R_RPL_InsertHeader(const uint16_t uIpLength, const uint16_t uIpExtLength);
    \brief This function inserts a RPL options header.
 */
r_result_t R_RPL_InsertHeader(const uint16_t uIpLength, const uint16_t uIpExtLength);

/*!
    \fn void R_RPL_UseAlternateParentHeader(void);
    \brief This function patches the next hop address of an RPL option header to use
    the alternate parent instead of the preferred parent.
 */
r_result_t R_RPL_UseAlternateParentHeader(void);

/*!
    \fn void R_RPL_RecomputeSourceHeader(void);
    \brief This function removes and adds computes again the source header created by
    the border router. Called after an update to the fail counters stored in the source
    route segments. This function can only be called if there is already a source header.
*/
r_result_t R_RPL_RecomputeSourceHeader(void);

/*!
    \fn void R_RPL_IsRoutedPacketUp(void);
    \brief This function checks if the packet is a routed packet and contains a Hop-by-hop
    option followed by an RPL option that indicates that the packet is going up to the
    border router. Returns R_TRUE if this is case or R_FALSE otherwise.
*/
r_result_t R_RPL_IsRoutedPacketUp(void);

/*!
    \fn r_rpl_mode_t R_RPL_SetMode(enum rpl_mode mode);
    \brief This function sets the RPL module operation mode (used RPL module internal only).
    \param[in] mode: RPL operation mode to be set
    \return Returned RPL operation mode
 */
r_rpl_mode_t R_RPL_SetMode(enum rpl_mode mode);

/*!
    \fn r_rpl_mode_t R_RPL_GetMode(void);
    \brief This function gets the RPL module operation mode (used RPL module internal only).
    \return Returned RPL operation mode
 */
r_rpl_mode_t R_RPL_GetMode(void);

/*!
    \fn void R_RPL_DisInput(void);
    \brief Processing function for incoming DIS messages.
 */
void R_RPL_DisInput(void);

/*!
    \fn void R_RPL_DioInput(void);
    \brief Processing function for incoming DIO messages.
 */
void R_RPL_DioInput(void);

/*!
    \fn void R_RPL_DaoInput(void);
    \brief Processing function for incoming DAO messages.
 */
void R_RPL_DaoInput(void);

/*!
    \fn void R_RPL_DaoAckInput(void);
    \brief Processing function for incoming DAO ACK messages.
 */
void R_RPL_DaoAckInput(void);

/*!
    \fn void R_RPL_Link_Neighbor_Callback(const r_eui64_t *addr, int status, int numtx);
    \brief This function checks for a new neighbor in case transmission fails (used RPL module internal only).
 */
void R_RPL_Link_Neighbor_Callback(const uint8_t* lladdr, unsigned transmissions, unsigned acked);

/**
 * Update the node-to-neighbor RSL for the specified link-layer address.
 * If the neighbor does not exist, this is a no-op.
 * @param llAddress The link-layer address of the neighbor that should be updated
 * @param rsl The RSL value from the received RSL-IE
 */
void R_RPL_UpdateNodeToNeighRsl(const uint8_t* lladdr, uint8_t rsl);

/**
 * Update the neighbor-to-node RSL for the specified link-layer address.
 * If the neighbor does not exist or the RPL module is not initialized, this is a no-op.
 * @details This function should be called every time a message is received from a neighbor
 * @param llAddress The link-layer address of the neighbor that should be updated
 * @param lqi The link quality of the recently received frame
 */
void R_RPL_UpdateNeighToNodeRsl(const uint8_t* llAddress, uint8_t lqi);

/**
 * Schedule a Neighbor Solicitation message to the link-local IP address corresponding to the specified EUI-64
 * @param target The target address of the Neighbor Solicitation and the EUI-64 to derive the destination IP address
 * @retval R_RESULT_SUCCESS if the NS was successfully scheduled.
 * @retval R_RESULT_FAILED if no slot was available to schedule NS
 */
r_result_t R_RPL_ScheduleNeighborSolicitation(const r_eui64_t* target);

/**
 * Register our global IPv6 address at preferred parent and alternate parent via NS with ARO (if not already done or
 * the previous registration is almost expired)
 * @return True, if at least one Neighbor Solicitation (with ARO) has been sent successfully. False otherwise.
 */
r_boolean_t R_RPL_RegisterIpAtParents();

/**
 * Register IPv6 address(es) at parent(s)
 * @details Should be called whenever the IPv6 address or the parent (preferred or alternate) of this device changes
 */
void R_RPL_AddressOrParentChange();

/**
 * Set the preferred and alternate parent and (un)register the global IP address
 * @param new_preferred_parent The neighbor that should become the preferred parent
 * @param new_alternate_parent The neighbor that should become the alternate parent
 */
void R_RPL_SetParents(uip_ds6_nbr_t* new_preferred_parent, uip_ds6_nbr_t* new_alternate_parent);

#if !R_LEAF_NODE_ENABLED

/**
 * Get the IPv6 address of the border router.
 * @return The IPv6 address of the border router or NULL, if this device has not joined a RPL instance yet.
 */
const uint8_t* R_RPL_BorderRouterAddress();

/**
 * Determine our own routing cost (used for the PAN Information element).
 * @return The routing cost for the PAN-IE.
 */
uint16_t R_RPL_RoutingCost();

/**
 * Leave the network and advertise this intention to our neighbors, child nodes, the border router and our parent(s).
 * @details This function assumes that the device is reset after this function returns. Otherwise, RPL may restore to
 * a sane state in the next ETX epoch.
 * @return R_RESULT_SUCCESS if all operations succeeded. Appropriate error code otherwise.
 */
r_result_t R_RPL_LeaveNetwork();
#endif

/*!
    \fn r_result_t R_RPL_Reset_Dio_Timer(uint8_t instance_id);
    \brief This function resets the DIO timer.
    \param[in] instance_id: Current DAG instance ID
    \return R_RESULT_SUCCESS in case of successful operation, R_RESULT_FAILED otherwise
 */
r_result_t R_RPL_Reset_Dio_Timer(uint8_t instance_id);

/*!
    \fn r_result_t R_RPL_PoisonRoute(void);
    \brief This function poisons the route and resets the DIO timer.
    \return R_RESULT_SUCCESS in case the route was poisoned, R_RESULT_FAILED otherwise
 */
r_result_t R_RPL_PoisonRoute(void);

/*!
    \fn r_nd_neighbor_cache_entry_t* R_RPL_IsPreferredOrAlternateParent(r_nd_neighbor_cache_entry_t* parent);
    \brief Return if the specified parent is the default or alternate parent
    \return R_TRUE if the specified parent is the default or alternate parent
 */
r_boolean_t R_RPL_IsPreferredOrAlternateParent(r_nd_neighbor_cache_entry_t* parent);

/*!
    \fn r_nd_neighbor_cache_entry_t* R_RPL_FirstParentEntry();
    \brief Return the first parent entry of the neighbor cache
    \return the first parent entry of the neighbor cache or NULL if none exists
 */
r_nd_neighbor_cache_entry_t* R_RPL_FirstParentEntry();

/*!
    \fn r_nd_neighbor_cache_entry_t* R_RPL_NextParentEntry(r_nd_neighbor_cache_entry_t* current);
    \brief Return the next parent entry of the neighbor cache
    \return the next parent entry of the neighbor cache or NULL if none exists
 */
r_nd_neighbor_cache_entry_t* R_RPL_NextParentEntry(r_nd_neighbor_cache_entry_t* current);

/*!
    \fn r_nd_neighbor_cache_entry_t* R_RPL_PreferredParent();
    \return the preferred parent (may be NULL)
 */
r_nd_neighbor_cache_entry_t* R_RPL_PreferredParent();

/*!
    \fn r_nd_neighbor_cache_entry_t* R_RPL_AlternateParent();
    \return the alternate parent (may be NULL)
 */
r_nd_neighbor_cache_entry_t* R_RPL_AlternateParent();

/**
 * Get the IPv6 address of our preferred parent.
 * @return Pointer to our preferred parent's IPv6 address or NULL, if there is no preferred parent.
 */
const uint8_t* R_RPL_PreferredParentAddress();

/**
 * Get a copy of our preferred parent's IPv6 address
 * @param[out] out Pointer to the output buffer that should hold the IP address
 * @return R_RESULT_SUCCESS if the parent address was copied to the output argument. R_RESULT_FAILED otherwise.
 */
r_result_t R_RPL_PreferredParentAddressCopy(r_ipv6addr_t* out);

/**
 * Get the MAC address (EUI-64) of our preferred parent.
 * @details This function may be called before RPL_Init() since it performs the necessary checks. If RPL is not
 * initialized yet, this function always returns a NULL pointer.
 * @return Pointer to our preferred parent's MAC address or NULL, if there is no preferred parent.
 */
const r_eui64_t* R_RPL_PreferredParentMacAddress();

/**
 * Get the IPv6 address of our alternate parent.
 * @return Pointer to our alternate parent's IPv6 address or NULL, if there is no alternate parent.
 */
const uint8_t* R_RPL_AlternateParentAddress();

/**
 * Set the RPL demo mode.
 * @param mode The RPL demo mode value to be set (0 or 1).
 */
void R_RPL_SetDemoMode(uint8_t mode);

/**
 * Get the current RPL demo mode value.
 * @return The current RPL demo mode value.
 */
uint8_t R_RPL_GetDemoMode();

#if R_BORDER_ROUTER_ENABLED

/**
 * Determine the number of source routes on this border router.
 * @return The number of available source routes.
 */
uint16_t R_RPL_GetRouteCount();
#endif /* R_BORDER_ROUTER_ENABLED */

#if R_LFN_PARENTING_ENABLED && R_BORDER_ROUTER_ENABLED
r_result_t R_RPL_AddSourceRouteForLfn(const r_ipv6addr_t* lfn_addr, uint16_t aroLifetime);
#endif

/*---------------------------------------------------------------------------*/
#endif /* R_RPL_API_H */
