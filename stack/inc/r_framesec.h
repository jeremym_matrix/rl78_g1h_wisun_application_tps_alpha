#ifndef R_FRAMESEC_H
#define R_FRAMESEC_H

#include "r_stdint.h"
#include "mac_neighbors.h"

#define R_FRAMESEC_KEY_SIZE       16
#define R_FRAMESEC_AUXHEADER_SIZE 6
#ifdef R_HYBRID_PLC_RF
#define R_FRAMESEC_MIC_SIZE       4
#define AUX_SEC_CONTROL           0x0d
#else /* R_HYBRID_PLC_RF */
#define R_FRAMESEC_MIC_SIZE       8
#define AUX_SEC_CONTROL           0x0e
#endif /* R_HYBRID_PLC_RF */

/**
 * Reset the frame security module
 */
void R_FRAMESEC_Reset();

/**
 * Sets a frame counter value
 */
void R_FRAMESEC_SetFrameCounter(uint32_t frame_counter, uint8_t index);

/**
 * Reads a frame counter value
 */
uint32_t R_FRAMESEC_GetFrameCounter(uint8_t index);

/**
 * Enables / disables frame counter checking
 */
void R_FRAMESEC_SetFrameCounterCheck(uint8_t enabled);

/**
 * Retrieves activation of frame counter checking
 */
uint8_t R_FRAMESEC_GetFrameCounterCheck();

/**
 * Set the specified GAK
 * @param index the zero-based key index
 * @param key the GAK
 * @param frame_counter the frame counter (usually 0)
 */
int R_FRAMESEC_SetGak(uint8_t index, const uint8_t* key, uint32_t frame_counter);

/**
 * Delete the specified GAK
 * @param index the zero-based key index
 */
int R_FRAMESEC_DeleteGak(uint8_t index);

/**
 * Get status of specified key
 * @param index the zero-based key index
 */
int R_FRAMESEC_GetKeyState(uint8_t index);

/**
 * Write an Auxiliary Security Header (ASH) to the specified buffer
 * @param[out] buf The output buffer where the ASH should be written
 * @param key_index The local (zero-based) index of the encryption key
 * @return 0 on success, 3 if the specified key is not present
 */
int R_FRAMESEC_AddAuxSecHeader(uint8_t* buf, uint8_t key_index);

/**
 * Encrypt the outgoing frame and append the MIC
 * @attention not re-entrant due to the used AES implementation
 * @param frame the incoming frame
 * @param size the frame size without the MIC
 * @param aux_header_pos the position of the auxiliary security header
 * @param mhr_size the size of the MHR (up to and including the header IE)
 * @param rev_src_eui64 the EUI-64 in reverse byte order (as transmitted)
 */
void R_FRAMESEC_Encrypt(uint8_t* frame, uint16_t size, uint16_t aux_header_pos, uint16_t mhr_size, const uint8_t* rev_src_eui64);

/**
 * Verify the auxiliary security header
 * @param frame the incoming frame
 * @param size the frame size without the MIC
 * @param aux_header_pos the position of the auxiliary security header
 * @param[out] out_key_index out parameter for the adjusted 0-based key index of the incoming packet
 * @return 0 if the verification checks pass
 */
int R_FRAMESEC_VerifyAuxSecHeader(const uint8_t* frame, uint16_t size, uint16_t aux_header_pos, uint8_t* out_key_index);

/**
 * Verify if it is possible to decrypt the incoming frame
 * @param frame the incoming frame
 * @param aux_header_pos the position of the auxiliary security header within the frame
 * @param nbr The neighbor cache entry corresponding to the sender of the frame
 * @retval 0 if the frame counter check passed
 * @retval 1 if the specified neighbor cache entry is invalid (no stored frame counter for comparison)
 * @retval 4 if the frame counter check fails
 */
int R_FRAMESEC_VerifyOrUpdateFrameCounter(const uint8_t* frame, uint16_t aux_header_pos, r_mac_neighbor_t* nbr);

/**
 * Decrypt the incoming frame (in-place) and verify the MIC
 * @param frame the incoming frame (including the MIC)
 * @param size the frame size without the MIC
 * @param aux_header_pos the position of the auxiliary security header
 * @param mhr_size the size of the MHR (up to and including the header IE)
 * @param rev_src_eui64 the EUI-64 in reverse byte order (as transmitted)
 * @return 0 if the MIC verification succeeds
 */
int R_FRAMESEC_Decrypt(uint8_t* frame, uint16_t size, uint16_t aux_header_pos, uint16_t mhr_size, const uint8_t* rev_src_eui64);

#endif /* R_FRAMESEC_H */
