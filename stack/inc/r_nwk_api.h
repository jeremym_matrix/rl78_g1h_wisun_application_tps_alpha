/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2019 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/*******************************************************************************
 * File Name    : r_nwk_api.h
 * Version      : 1.00
 * Description  : Header for the module that implements network layer API
 ******************************************************************************/

/*!
 * @file r_nwk_api.h
 * @version 1.0
 * @brief Header for the module that implements network layer API
 */

#ifndef R_NWK_API_H
#define R_NWK_API_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include <stddef.h>
#include "r_nwk_api_base.h"
#include "r_nwk_config.h"
#include "r_nwk_inter_config.h"
#include "r_dhcpv6.h"
#include "r_mac_ie.h"
#include "r_os_msg_types.h"
#include "r_header_utils.h"
#include "r_assert.h"
#if !R_DEV_DISABLE_AUTH
#include "r_auth_types.h"
#endif /* !R_DEV_DISABLE_AUTH */
#include "mac_mlme.h"

/******************************************************************************
   Macro definitions
******************************************************************************/

/** The maximum total size of an NWK message (request, confirm or indication) */
#define R_NWK_MSG_MAX_TOTAL_SIZE        (1600U)

/** The maximum size of the payload (message field) within an NWK message (request, confirm or indication) */
#define R_NWK_MSG_MAX_PAYLOAD_SIZE      (R_NWK_MSG_MAX_TOTAL_SIZE - offsetof(r_nwk_msg_t, message))

/** The maximum number of source routes that may be included in a single NWK GET confirm. The number of routes is
 * limited because the source routing table may be too large to be transmitted/requested as a whole. */
#define R_NWK_MAX_ROUTES_PER_CONFIRM    (5U)

/** The maximum number of neighbor cache entries that may be included in a single NWK GET confirm. The number of routes is
 * limited because the neighbor cache may be too large to be transmitted/requested as a whole. */
#define R_NWK_MAX_NEIGHBORS_PER_CONFIRM (5U)

#if (R_MODEM_SERVER || R_MODEM_CLIENT)

/* 128 bytes should be enough since each log record is sent immediately to modem client (application layer) */
#define R_NWK_LOG_INDICATION_MAX_BYTES 128
#endif

/******************************************************************************
   Typedef definitions
******************************************************************************/

/**
 * Type IDs of messages that may be sent from the application layer to the network layer (requests)
 * and vice versa (confirms and indications)
 */
typedef enum
{
    R_NWK_API_MSG_NWK_WARNING_IND            = 0x6E, //!< NWK warning indication
    R_NWK_API_MSG_NWK_FATAL_ERROR_IND        = 0x6F, //!< NWK fatal error indication
    R_NWK_API_MSG_GENERIC_CONFIRM            = 0xA0, //!< Generic confirm message from the NWK layer
#if R_BORDER_ROUTER_ENABLED
    R_NWK_API_MSG_NWK_START_REQ              = 0xA1, //!< Network start request (border router)
    R_NWK_API_MSG_NWK_START_CFM              = 0xA2, //!< Network start confirm (border router)
#endif
    R_NWK_API_MSG_NWK_JOIN_REQ               = 0xA3, //!< Network join request (router node)
    R_NWK_API_MSG_NWK_JOIN_CFM               = 0xA4, //!< Network join confirm (router node)
    R_NWK_API_MSG_RESET_REQ                  = 0xA5, //!< Reset request
    R_NWK_API_MSG_RESET_CFM                  = 0xA6, //!< Reset confirm
    R_NWK_API_MSG_SET_REQ                    = 0xA7, //!< Set request
    R_NWK_API_MSG_SET_CFM                    = 0xA8, //!< Set confirm
    R_NWK_API_MSG_GET_REQ                    = 0xA9, //!< Get request
    R_NWK_API_MSG_GET_CFM                    = 0xAA, //!< Get confirm
    R_NWK_API_MSG_SUSPEND_REQ                = 0xAB, //!< Suspend request
    R_NWK_API_MSG_SUSPEND_CFM                = 0xAC, //!< Suspend confirm
    R_NWK_API_MSG_RESUME_REQ                 = 0xAD, //!< Resume request
    R_NWK_API_MSG_RESUME_CFM                 = 0xAE, //!< Resume confirm
    R_NWK_API_MSG_JOIN_STATE_IND             = 0xAF, //!< NWK Join State Update Indication
    R_NWK_API_MSG_ICMP_ECHO_REQ              = 0xB0, //!< ICMP Echo request
    R_NWK_API_MSG_ICMP_ECHO_CFM              = 0xB1, //!< ICMP Echo confirm
    R_NWK_API_MSG_ICMP_ECHO_IND              = 0xB2, //!< ICMP echo response indication
    R_NWK_API_MSG_ICMP_ERROR_IND             = 0xB3, //!< ICMP error indication
    R_NWK_API_MSG_UDP_DATA_REQ               = 0xB4, //!< UDP data request
    R_NWK_API_MSG_UDP_DATA_CFM               = 0xB5, //!< UDP data confirm
    R_NWK_API_MSG_UDP_DATA_IND               = 0xB6, //!< UDP data indication
#if R_BORDER_ROUTER_ENABLED
    R_NWK_API_MSG_PAN_VER_INC_REQ            = 0xB7, //!< PAN Version increment request
    R_NWK_API_MSG_PAN_VER_INC_CFM            = 0xB8, //!< Confirm message for PAN Version increment request
    R_NWK_API_MSG_LFN_VER_INC_REQ            = 0xB9, //!< LFN Version increment request
    R_NWK_API_MSG_LFN_VER_INC_CFM            = 0xBA, //!< Confirm message for LFN Version increment request
    R_NWK_API_MSG_AUTH_BR_START_IND          = 0xBF, //!< Start the border router authentication process
    R_NWK_API_MSG_AUTH_BR_PER_UPDATE         = 0xC0, //!< Indication for the BR authentication to perform the periodic update
    R_NWK_API_MSG_RPL_REFRESH_ROUTES_REQ     = 0xC1, //!< Refresh source routes by DTSN increment (forces new DAOs)
    R_NWK_API_MSG_RPL_REFRESH_ROUTES_CFM     = 0xC2, //!< Confirm message for RPL route refresh
    R_NWK_API_MSG_RPL_GLOBAL_REPAIR_REQ      = 0xC3, //!< Increment DODAG version and DTSN to initiate a global repair of all source routes
    R_NWK_API_MSG_RPL_GLOBAL_REPAIR_CFM      = 0xC4, //!< Confirm message for RPL Global Repair request
    R_NWK_API_MSG_DEVICE_STATUS_UPDATE_IND   = 0xC5, //!< Indication for status update of another FAN device
#endif
    R_NWK_API_MSG_DHCP_VENDOR_OPT_DATA       = 0xC6, //!< Incoming DHCPv6 Vendor-specific Information Option data
#if R_BORDER_ROUTER_ENABLED
    R_NWK_API_MSG_DHCP_IND                   = 0xC7, //!< Incoming message for the DHCPv6 server
#endif
#if R_DEV_TBU_ENABLED
    R_NWK_API_MSG_SUBSCRIPT_FRAME_IND        = 0xC8, //!< Subscription frame indication
#endif
    R_NWK_API_MSG_IP_DATA_REQ                = 0xC9, //!< IPv6 data request
    R_NWK_API_MSG_IP_DATA_CFM                = 0xCA, //!< IPv6 data confirm
    R_NWK_API_MSG_IP_DATA_IND                = 0xCB, //!< IP data indication
    R_NWK_API_MSG_ERASE_FRAMECNT_REQ         = 0xCC, //!< Erase Frame Counter request
    R_NWK_API_MSG_ERASE_FRAMECNT_CFM         = 0xCD, //!< Erase Frame Counter confirm
    R_NWK_API_MSG_EAPOL_DATA_REQ             = 0xCE, //!< Request to send an EAPOL message
    R_NWK_API_MSG_EAPOL_DATA_CFM             = 0xCF, //!< Confirm for an EAPOL data request
    R_NWK_API_MSG_EAPOL_DATA_IND             = 0xD0, //!< Incoming EAPOL message
    R_NWK_API_MSG_AUTH_RN_START              = 0xD1, //!< Start the router node authentication process
    R_NWK_API_MSG_AUTH_RN_RESET              = 0xD2, //!< Reset the route node authentication module
    R_NWK_API_MSG_AUTH_RN_DELETE_GTK         = 0xD3, //!< Delete a GTK within the router node authentication module
    R_NWK_API_MSG_PAN_TIMEOUT_RESET          = 0xD4, //!< Reset the PAN Timeout timer
    R_NWK_API_MSG_AUTH_RN_CLEAR_GTK_VALIDITY = 0xD5, //!< Indication to clear the GTK validity flag
    R_NWK_API_MSG_AUTH_MSG_PROCESSED         = 0xD6, //!< Notification for NWK that an authentication msg was processed
    R_NWK_API_MSG_EAP_TLS_START              = 0xD7, //!< Free uncritical memory before EAP-TLS handshake is starting
    R_NWK_API_MSG_EAP_TLS_DONE               = 0xD8, //!< Re-alloc uncritical memory after EAP-TLS handshake is done
#if !(R_MODEM_SERVER || R_MODEM_CLIENT)
    R_NWK_API_MSG_EAPOL_DATA_REQ_OPT         = 0xD9, //!< Memory optimized version of EAPOL data request for single-chip RL78
#endif
#if (R_MODEM_SERVER || R_MODEM_CLIENT)
    R_NWK_API_MSG_LOG_IND                    = 0xDA, //!< Indication for binary log records
#endif
    R_NWK_API_MSG_LEAVE_NETWORK_REQ          = 0xDB, //!< Leave the network that this device is currently connected to
    R_NWK_API_MSG_LEAVE_NETWORK_CFM          = 0xDC, //!< Leave the network that this device is currently connected to
    R_NWK_API_MSG_LAST                       = 0xDF, //!< Last value for messages between APL and NWK layer (placeholder)

    /* The following message types are intended for NWK-internal usage only! */
    R_NWK_IN_MSG_CYCTIMER_TICK_NTF           = 0xE0, //!< Timer process for 6LoWPAN fragmentation
    R_NWK_IN_MSG_LAST                        = 0xFF, //!< Last value for NWK-internal messages (placeholder)
} r_nwk_msg_type_t;

typedef enum
{
#if R_BORDER_ROUTER_ENABLED
    R_BORDERROUTER = 0,  //!< FFN Border Router
#endif
    R_ROUTERNODE   = 1,  //!< FFN Router Node
#if R_LEAF_NODE_ENABLED
    R_LEAFNODE     = 2,  //!< LFN
#endif
} r_device_type_t;

typedef enum
{
    R_NWK_phyCurrentChannel           = 0x00,
    R_NWK_phyChannelsSupported        = 0x01,
    R_NWK_phyTransmitPower            = 0x02,
    R_NWK_phyChannelsSupportedPage    = 0x03,
    R_NWK_phyRssiOutputOffset         = 0x04,
    R_NWK_phyLvlfltrVth               = 0x09,
    R_NWK_phyAntennaSwitchEna         = 0x13,
    R_NWK_phyAntennaDiversityRxEna    = 0x14,
    R_NWK_phyAntennaSelectTx          = 0x15,
    R_NWK_phyRegulatoryMode           = 0x1a,
    R_NWK_phyFrequency                = 0x1b,
    R_NWK_phyFrequencyOffset          = 0x1c,
    R_NWK_phyWiSunPhyConfig           = 0x20,
    R_NWK_phyFSKOpeMode               = 0x21,
    R_NWK_phyFreqBandId               = 0x2d,
    R_NWK_phyCcaDuration              = 0x2e,
    R_NWK_phyAgcWaitGainOffset        = 0x30,
    R_NWK_phyCcaVth                   = 0x31,
    R_NWK_phyCcaVthOffset             = 0x32,
    R_NWK_phyAntennaSwitchEnaTiming   = 0x33,
    R_NWK_phyGpio0Setting             = 0x34,
    R_NWK_phyGpio3Setting             = 0x35,
    R_NWK_phyDataRate                 = 0x36,

#if R_PHY_TYPE_CWX_M
    R_NWK_phyOperatingMode            = 0x38,
    R_NWK_phyRxModulation             = 0x39,
    R_NWK_phyCcaModulation            = 0x3a,
    R_NWK_phyTxModulation             = 0x3b,
    R_NWK_phyFskCcaVth                = 0x3c,
    R_NWK_phyOfdmCcaVth               = 0x3d,
    R_NWK_phyFSKPreambleLength        = 0x3e,
    R_NWK_phyMDRNewModeSwitchBank     = 0x3f,
#endif /* R_PHY_TYPE_CWX_M */

    R_NWK_macMaxCSMABackoffs          = 0x4e,
    R_NWK_macMinBE                    = 0x4f,
    R_NWK_macPANId                    = 0x50,
    R_NWK_macRxOnWhenIdle             = 0x52,
    R_NWK_macMaxBE                    = 0x57,
    R_NWK_macMaxFrameRetries          = 0x59,
    R_NWK_macFrameCounters            = 0x77,
    R_NWK_macFrameCounterCheckEnabled = 0x78,
    R_NWK_macFrameCounterOffset       = 0x79,
    R_NWK_macExtendedAddress          = 0x80,
    R_NWK_macDiscoveryMode            = 0x81,
    R_NWK_macToneMask                 = 0x82,
    R_NWK_macWhitelist                = 0x83,

    /* --- MAC metrics --- */
    R_NWK_macMetricsEnabled           = 0x84,    //!< The activation state of the MAC performance metrics collection

    /* --- MAC performance metrics (IEEE Std 801.15.4-2015 8.4.2.6) --- */
    R_NWK_macCounterOctets            = 0x85,    //!< The size of the MAC metrics counters in octets
    R_NWK_macRetryCount               = 0x86,    //!< The number of transmitted frames that required exactly one retry before acknowledgement
    R_NWK_macMultipleRetryCount       = 0x87,    //!< The number of transmitted frames that required more than one retry before acknowledgement
    R_NWK_macTXFailCount              = 0x88,    //!< The number of transmitted frames that did not result in an acknowledgement after @ref R_NWK_macMaxFrameRetries
    R_NWK_macTXSuccessCount           = 0x89,    //!< The number of transmitted frames that were acknowledged after the initial Data frame transmission
    R_NWK_macFCSErrorCount            = 0x8a,    //!< The number of frames that were discarded due to an incorrect FCS
    R_NWK_macSecurityFailureCount     = 0x8b,    //!< The number of frames that did not pass the MAC frame security check successfully
    R_NWK_macDuplicateFrameCount      = 0x8c,    //!< The number of frames that contained the same sequence number as the previous frame
    R_NWK_macRXSuccessCount           = 0x8d,    //!< The number of frames that were received correctly

    R_NWK_nwkSchedule_Unicast         = 0x90,    //!< The unicast schedule of this device
    R_NWK_nwkSchedule_Broadcast       = 0x91,    //!< The broadcast schedule of this device

    R_NWK_rplDioIntervalMin           = 0xa0,    //!< DIO trickle timer Imin value is defined as 2 to the power of this value in milliseconds.
    R_NWK_rplDioIntervalDoublings     = 0xa1,    //!< DIO Trickle timer Imax value in doublings of Imin
    R_NWK_rplDioRedundancyConstant    = 0xa2,    //!< DIO Trickle timer redundancy constant k
    R_NWK_deviceMinSens               = 0xa3,    //!< Minimum receiver sensitivity level
    R_NWK_rplNbrMinLifetimeSecs       = 0xa5,    //!< Minimum lifetime (in seconds) of a RPL neighbor cache entry
    R_NWK_rplNudTimeout               = 0xa6,    //!< Maximum period in seconds after which Neighbor Unreachability Detection will be performed

    R_NWK_discIntervalImin            = 0xb0,    //!< Discovery Trickle timer Imin value in seconds
    R_NWK_discIntervalImax            = 0xb1,    //!< Discovery Trickle timer Imax value in doublings of Imin

    R_NWK_panTimeout                  = 0xb2,    //!< Interval in minutes over which a FAN node must have indication of liveness from the Border Router
    R_NWK_udpIpIndication             = 0xb3,    //!< Generate IP data indications instead of UDP data indications for incoming UDP messages
    R_NWK_targetPanId                 = 0xb4,    //!< The PAN ID of the network to be joined to. Value 0xffff disables PAN ID checking for network selection
    R_NWK_panIdBlacklist              = 0xb5,    //!< Blacklist of PAN IDs not to be used during PAN discovery. All entries set to 0xffff disables the blacklisting.
    R_NWK_authJoinTimerMax            = 0xb6,    //!< Maximum time in seconds until nodes in join state 2 fall back to join state 1 in case of unsuccessful authentication.
    R_NWK_authJoinTimerBase           = 0xb7,    //!< The initial retransmission time of unsuccessful authentication.
    R_NWK_authJoinTimerBackoffFactor  = 0xb8,    //!< The backoff factor of authentication retransmissions.

    R_NWK_gtkRequestIminMinutes       = 0xba,    //!< GKH Trickle timer Imin value in minutes
    R_NWK_gtkRequestImax              = 0xbb,    //!< GKH Trickle timer Imax value in doublings of Imin
    R_NWK_gtkMaxMismatchMinutes       = 0xbc,    //!< Max time in minutes between a SUP detecting a GTKHASH mismatch and the SUP initiating Msg1 of the auth flow

    R_NWK_mplTrickleImin              = 0xc0,    //!< MPL Trickle timer Imin value in seconds
    R_NWK_mplTrickleImax              = 0xc1,    //!< MPL Trickle timer Imax value in doublings of Imin
    R_NWK_mplTrickleK                 = 0xc2,    //!< The redundancy constant for MPL Data Message transmissions
    R_NWK_mplTrickleTimerExpirations  = 0xc3,    //!< MPL Trickle timer expirations, the number of timer expirations until this message is removed
    R_NWK_mplSeedSetEntryLifetime     = 0xc4,    //!< The minimum lifetime for an MPL entry in the Seed (in seconds)

    R_NWK_fanTPSVersion               = 0xc5,    //!< indicating the Wi-SUN FAN TPS version

#if (R_WISUN_FAN_VERSION >= 110)
#if R_LEAF_NODE_ENABLED
    R_NWK_lfnLostParentTime           = 0xce,    //!< The number of LFN_BROADCAST_SYNC_PERIODs after which an LFN assumes the parent is lost (if nothing was received)
#endif
    R_NWK_lfnBroadcastSyncPeriod      = 0xcf,    //!< The time between Broadcast Sync messages from an FFN parent.
#endif
    R_NWK_nwkSecurityEnabled          = 0xd0,
    R_NWK_nwkIpv6Address              = 0xd1,  //!< Preferred IPv6 address (global or link-local)
    R_NWK_ndCache                     = 0xd2,
    R_NWK_borderRouterAddress         = 0xd3,
    R_NWK_preferredParentAddress      = 0xd4,
    R_NWK_alternateParentAddress      = 0xd5,
    R_NWK_ipAddrRegRefreshThreshold   = 0xde,  //!< The interval in which our global IPv6 address is re-registered to our parent(s) via ARO (in percentage of total lifetime)
    R_NWK_ipAddrRegLifetime           = 0xdf,  //!< The lifetime of our global IPv6 address when registering it to our parent(s) via ARO
    R_NWK_sixLowpanMtu                = 0xe0,
    R_NWK_rplDemoMode                 = 0xe1,  //!< The value for the RPL demo mode (0 means disabled)
#if (R_MODEM_SERVER || R_MODEM_CLIENT)
    R_NWK_logVerbosity                = 0xe2,  //!< The log level for log indications sent by the modem server
#endif
    R_NWK_deviceType                  = 0xe3,
    R_NWK_joinState                   = 0xe4,

    R_NWK_framesecActiveGtk           = 0xe8,  //!< Mark a specific GTK or LGTK as active
    R_NWK_framesecGtk                 = 0xe9,
#if R_DEV_TBU_ENABLED
    R_NWK_macFrameSubscriptionEnabled = 0xea, //!< Frame subscription activation
#endif
    R_NWK_authStateCache              = 0xf0, //!< Authentication state cached by the stack
    R_NWK_rplInfo                     = 0xf7, //!< RPL statistics
#if R_BORDER_ROUTER_ENABLED
    R_NWK_sourceRoutes                = 0xf8, //!< RPL source routes
#endif
#if !(R_MODEM_SERVER || R_MODEM_CLIENT)
    R_NWK_dhcpEnableVendorOption      = 0xf9,  //!< Request/Transmit DHCPv6 Vendor-specific Information Option to/from server
#endif
} r_nwk_attr_id_t;

typedef enum R_HEADER_UTILS_ATTR_PACKED
{
    /* Warning indication status */
    R_NWK_WARN_INSUFFICIENT_BUFFER_MCPS_DATA_IND         = 0x01,
    R_NWK_WARN_INSUFFICIENT_BUFFER_MLME_COMM_STATUS_IND  = 0x07,
    R_NWK_WARN_INSUFFICIENT_BUFFER_EAPOL                 = 0x18,
    R_NWK_WARN_INSUFFICIENT_BUFFER_IP_IND                = 0x19,
    R_NWK_WARN_INSUFFICIENT_BUFFER_LOG_IND               = 0x1B,
    R_NWK_WARN_INSUFFICIENT_BUFFER_DHCPV6_VENDOR_OPT_IND = 0x1C,
    R_NWK_WARN_INSUFFICIENT_BUFFER_JOIN_STATE_UPDATE_IND = 0x1D,
#if R_BORDER_ROUTER_ENABLED
    R_NWK_WARN_INSUFFICIENT_BUFFER_DHCPV6_IND            = 0x20,
    R_NWK_WARN_INSUFFICIENT_BUFFER_DEVICE_STATUS_IND     = 0x21,
#endif
    R_NWK_WARN_INSUFFICIENT_BUFFER_UDP_IND               = 0x30,
    R_NWK_WARN_INSUFFICIENT_BUFFER_ICMP_IND              = 0x31,
    R_NWK_WARN_INSUFFICIENT_BUFFER_ICMP_ERROR_IND        = 0x32,
    R_NWK_WARN_INSUFFICIENT_BUFFER_6LP_RX                = 0x33,
    R_NWK_WARN_INSUFFICIENT_BUFFER_6LP_FRAG_RX           = 0x34,
} r_nwk_warning_type_t;

/** Fatal error indication status */
typedef enum R_HEADER_UTILS_ATTR_PACKED
{
#if R_SHARED_NWK_MEM

    /** Insufficient heap memory available for critical system resources after EAP-TLS handshake was done */
    R_NWK_FATAL_INSUFFICIENT_HEAP_AFTER_EAP_TLS = 0x01,
#endif
    R_NWK_FATAL_BUFFER_OVERFLOW                 = 0x02, //!< Buffer was overwritten
    R_NWK_FATAL_UNKNOWN                         = 0xFF, //!< Placeholder for unknown critical error
} r_nwk_fatal_error_type_t;

typedef enum R_HEADER_UTILS_ATTR_PACKED
{
    R_NWK_JoinState0_Reset            = 0x00,
    R_NWK_JoinState1_SelectPAN        = 0x01,
    R_NWK_JoinState2_Authenticate     = 0x02,
    R_NWK_JoinState3_AcquirePANConfig = 0x03,
    R_NWK_JoinState4_ConfigureRouting = 0x04,
    R_NWK_JoinState5_Operational      = 0x05
} r_nwk_join_state_t;

/* Disable padding for the following structs so that they can be transmitted via the modem serial interface */
R_HEADER_UTILS_PRAGMA_PACK_1

typedef union
{
    uint8_t                    phyCurrentChannel;
    uint32_t                   phyChannelsSupported[2];
    uint8_t                    phyTransmitPower;

    uint8_t                    macMaxCSMABackoffs;
    uint8_t                    macMinBE;
    uint16_t                   macPANId;
    r_boolean_t                macRxOnWhenIdle;
    uint8_t                    macMaxBE;
    uint8_t                    macMaxFrameRetries;

    uint8_t                    macLowPowerMode;
    uint32_t                   macExtendedAddress[2];

    /* MAC metrics */
    r_boolean_t                macMetricsEnabled;

    /* --- Performance metrics (IEEE Std 801.15.4-2015 8.4.2.6) --- */
    uint32_t                   macCounterOctets;
    uint32_t                   macRetryCount;
    uint32_t                   macMultipleRetryCount;
    uint32_t                   macTXFailCount;
    uint32_t                   macTXSuccessCount;
    uint32_t                   macFCSErrorCount;
    uint32_t                   macSecurityFailureCount;
    uint32_t                   macDuplicateFrameCount;
    uint32_t                   macRXSuccessCount;

    r_ie_wp_serialized_sched_t schedule;
} r_mac_attr_value_t;

/**
 * A Group Transient Key used by the Frame Security module
 */
typedef struct
{
    uint8_t     index;                //! The index of the GTK to modify
    r_boolean_t remove;               //! TRUE for remove the GTK, FALSE for set the GTK
    uint8_t     gtk[R_AUTH_GTK_SIZE]; //! The new GTK (only used for SET)
} r_nwk_framesec_gtk_t;

/**
 * A Group Transient Key or LFN Group Transient Key identified by its local key index (0-based)
 */
typedef struct
{
    uint8_t index;  //! The index of the GTK or LGTK
} r_nwk_gtk_index_t;

/**
 * Structure for the PHY configuration according to the Wi-SUN PHY specification
 */
typedef RmMlmeWiSunPhyConfigT r_nwk_wisun_phy_config_t;

/**
 * Container for whitelist entries
 */
typedef struct
{
    uint16_t  count;     //!< The number of whitelist entries following
    r_eui64_t entries[]; //!< The entries from the MAC whitelist
} r_nwk_mac_whitelist_t;

/**
 * Compact version of a Neighbor Discovery cache entry
 */
typedef struct
{
    r_eui64_t    llAddress;             //!< Link-layer address
    uint16_t     linkMetric;            //!< RPL link metric
    uint8_t      neighToNodeRsl;        //!< EWMA of the RSL metric for the neighbor-to-node direction
    uint8_t      rssi;                  //!< RSSI of the last message
    uint16_t     panSize;               //!< The reported PAN Size of the parent from the PAN-IE
    uint16_t     rank;                  //!< RPL rank
    uint16_t     routingCost;           //!< The reported Routing Cost of the parent from the PAN-IE
    r_ipv6addr_t ipAddress;             //!< On-link unicast IPv6 address
    uint32_t     lastReceiveSecondsAgo; //!< How many seconds ago a neighbor-to-node message has been received
    uint8_t      status;                //!< The status of the neighbor
} r_nwk_nd_cache_entry_t;

/**
 * Container for Neighbor Discovery cache entries
 */
typedef struct
{
    uint16_t               count;     //!< The number of ND cache entries following
    r_nwk_nd_cache_entry_t entries[]; //!< The entries from the Neighbor Discovery cache
} r_nwk_nd_cache_t;

/** The part of the supplicant authentication module state that is relevant to the Wi-SUN stack */
typedef struct
{
    uint8_t authenticator[8]; //!< the authenticator EUI-64
    uint8_t gtks_live;        // bit mask
} r_auth_sup_state_cache_t;

/** The part of the authentication module state that is relevant to the Wi-SUN stack */
typedef struct
{
    r_auth_sup_state_cache_t sup;
    uint8_t gtks_valid;  // bit mask; for BR also indicates liveness
} r_nwk_auth_state_cache_t;

/**
 * Container for various RPL information
 */
typedef struct
{
    uint8_t      instance_id;          //!< The ID of the RPL instance that this device is part of
    r_ipv6addr_t dodag_id;             //!< The DODAG identifier (routable IPv6 address that belongs to the DODAG root)
    uint8_t      dodag_version;        //!< The DODAG version number set by the DODAG root
    uint8_t      dodag_preference : 3; //!< The preference value of the root of this DODAG compared to other DODAG roots within the instance
    uint8_t      dodag_grounded : 1;   //!< True, if the DODAG is grounded; False, if the DODAG is floating; Advertised by DODAG root
    uint8_t      mop : 3;              //!< The mode of operation (provisioned at and distributed by the DODAG root)
    uint8_t      rank;                 //!< The rank of this device
    uint8_t      dtsn;                 //!< The Destination Advertisement Trigger Sequence Number of this device
} r_nwk_rpl_info_t;

#if R_BORDER_ROUTER_ENABLED

/**
 * Compact version of a source route
 */
typedef struct
{
    r_ipv6addr_t destination;       //!< The IPv6 destination address of this route
    r_ipv6addr_t preferredParent;   //!< The IPv6 address of the preferred parent
    r_ipv6addr_t alternateParent;   //!< The IPv6 address of the alternate parent
    uint32_t     remainingLifetime; //!< Remaining lifetime of this route in seconds
} r_nwk_route_t;

/**
 * Container for source routes
 */
typedef struct
{
    uint16_t      count;     //!< The number of route entries following
    r_nwk_route_t entries[]; //!< The entries from the source routing table
} r_nwk_routing_entries_t;

/**
 * Status codes to indicate the type of status update for a certain device
 */
typedef enum R_HEADER_UTILS_ATTR_PACKED
{
    R_NWK_DEVICE_STATUS_ROUTE_ADDED   = 1,  //!< A source route for this device was added to the source routing table
    R_NWK_DEVICE_STATUS_ROUTE_CHANGED = 2,  //!< The source route for this device has changed
    R_NWK_DEVICE_STATUS_ROUTE_REMOVED = 3,  //!< A source route for this device was removed from the source routing table
} r_nwk_device_update_code_t;

/**
 * Indication for updated device status for a participant of this border router's PAN
 */
typedef struct
{
    r_ipv6addr_t               deviceAddress; // <! The IPv6 address to identify the device whose status has changed
    r_nwk_device_update_code_t updateType;    // <! The type of status update that occurred
} r_nwk_device_status_ind_t;

#endif /* R_BORDER_ROUTER_ENABLED */

typedef union
{
    r_nwk_mac_whitelist_t    macWhitelist;
    uint32_t                 macFrameCounter[R_KEY_NUM];
    uint8_t                  macFrameCounterCheckEnabled;
    uint16_t                 macFrameCounterOffset;
    uint8_t                  nwkSecurityEnabled;
    uint8_t                  nwkIpv6Address[R_IPV6_ADDRESS_LENGTH];
    r_nwk_nd_cache_t         nwkNdCache;
    uint8_t                  borderRouterAddress[R_IPV6_ADDRESS_LENGTH];
    uint8_t                  preferredParentAddress[R_IPV6_ADDRESS_LENGTH];
    uint8_t                  alternateParentAddress[R_IPV6_ADDRESS_LENGTH];
    uint16_t                 sixLowpanMtu;
    uint8_t                  deviceType;
    r_nwk_join_state_t       joinState;
    uint8_t                  deviceMinSens;                           //!< Minimum receiver sensitivity level (-174 (0) to +80 (254) dBm)
    uint16_t                 ipAddrRegLifetimeMinutes;                //!< The lifetime (in minutes) of our global IPv6 address when registering to our parent(s)
    uint8_t                  ipAddrRegRefreshThreshold;               //!< The threshold (in percentage of lifetime) after which a GUA/ULA is re-registered at parent(s)
    uint8_t                  rplDioIntervalMin;                       //!< DIO Trickle timer Imin value is defined as 2 to the power of this value in milliseconds
    uint8_t                  rplDioIntervalDoublings;                 //!< DIO Trickle timer Imax value in doublings of Imin
    uint8_t                  rplDioRedundancyConstant;
    uint16_t                 rplNbrMinLifetimeSecs;                   //!< Minimum lifetime (in seconds) of a RPL neighbor cache entry
    uint16_t                 rplNudTimeout;                           //!< Maximum period in seconds after which Neighbor Unreachability Detection will be performed
    uint8_t                  rplDemoMode;                             //!< The setting for the RPL demo mode (0=disabled)
    r_nwk_rpl_info_t         rplInfo;                                 //!< RPL information

    uint8_t                  pan_timeout;                             //!< Interval over which a FAN node must have indication of liveness from PAN Border Router
    r_boolean_t              udpIpIndication;                         //!< Send IP (instead of UDP) indications for incoming UDP messages
    uint16_t                 targetPanId;                             //!< The PAN ID of the network to be joined to. Value 0xffff disables PAN ID checking for network selection
    uint16_t                 panIdBlacklist[R_PAN_ID_BLACKLIST_SIZE]; //!< Blacklist of PAN IDs not to be used during PAN discovery. All entries set to 0xffff disables the blacklisting.
    uint16_t                 authJoinTimerMax;                        //!< Maximum time in seconds until nodes in join state 2 fall back to join state 1 in case of unsuccessful authentication.
    uint16_t                 authJoinTimerBase;                       //!< The initial retransmission time of of unsuccessful authentication.
    uint8_t                  authJoinTimerBackoffFactor;              //!< The backoff factor of authentication retransmissions.

    uint8_t                  discPanImin;                             //!< Discovery Trickle timer Imin value in seconds
    uint8_t                  discPanImax;                             //!< Discovery Trickle timer Imax value in doublings of Imin
    uint8_t                  discGtkRequestIminMinutes;               //!< GKH Trickle timer Imin value in minutes
    uint8_t                  discGtkRequestImax;                      //!< GKH Trickle timer Imax value in doublings of Imin
    uint8_t                  discGtkMaxMismatchMinutes;               //!< Max time between a SUP detecting a GTKHASH mismatch and the SUP initiating Msg1 of the auth flow

    uint8_t                  mplTrickleImin;                          //!< MPL Trickle timer Imin value in seconds
    uint8_t                  mplTrickleImax;                          //!< MPL Trickle timer Imax value in doublings of Imin
    uint8_t                  mplTrickleK;                             //!< The redundancy constant for MPL Data Message transmissions
    uint8_t                  mplTrickleTimerExpirations;              //!< MPL Trickle timer expirations, the number of timer expirations until this message is removed
    uint16_t                 mplSeedSetEntryLifetime;                 //!< The minimum lifetime for an MPL entry in the Seed set in seconds

    uint8_t                  fan_tps_version;                         //!< Indicating the Wi-SUN FAN TPS version

    r_nwk_gtk_index_t        framesecActiveGtk;                       //!< The GTK to be activated
    r_nwk_framesec_gtk_t     framesecGtk;                             //!< The GTK to be added or deleted
    r_nwk_auth_state_cache_t authStateCache;                          //!< The authentication state that should be cached in the NWK task
#if R_BORDER_ROUTER_ENABLED
    r_nwk_routing_entries_t  rplRoutes;                               //!< RPL source routes
#endif
#if (R_MODEM_SERVER || R_MODEM_CLIENT)
    uint8_t                  logVerbosity;           //!< higher values -> more log statements (0=disabled; 1=errors; 2=warnings; 3=debug)
#else
    r_boolean_t              dhcpEnableVendorOption; //!< Transmit DHCPv6 Vendor-specific Information Option to server and request this option from the server
#endif
#if (R_WISUN_FAN_VERSION >= 110)
#if R_LEAF_NODE_ENABLED
    uint8_t                  lfnLostParentTime;      //!< The number of LFN_BROADCAST_SYNC_PERIODs after which an LFN assumes the parent is lost (if nothing was received)
#endif
    uint8_t                  lfnBroadcastSyncPeriod; //!< The time between Broadcast Sync messages from an FFN parent.
#endif
} r_nwk_attr_value_t;                                // union

/*!
    \struct r_icmp_echo_reply_ind_t
    \brief ICMPv6 echo reply indication
 */
typedef struct
{
    uint8_t  dstAddress[R_IPV6_ADDRESS_LENGTH]; //!< The 128-bit IPv6 address of the destination of the packet
    uint8_t  srcAddress[R_IPV6_ADDRESS_LENGTH]; //!< The 128-bit IPv6 address of the source of the packet
    uint16_t identifier;                        //!< An identifier to aid in matching Echo Replies
    uint16_t sequence;                          //!< A sequence number to aid in matching Echo Replies
    uint16_t dataLength;                        //!< Length of data field
    uint16_t status;                            //!< Rx status (secured/unsecured)
    uint8_t  lqi;
    uint8_t  data[];                            //!< Storage to the actual data
} r_icmp_echo_reply_ind_t;

/**
 * Request to send an ICMPv6 echo message (echo request or echo reply)
 */
typedef struct
{
    uint8_t     dstAddress[R_IPV6_ADDRESS_LENGTH]; //!< The 128-bit IPv6 address of the destination of the packet
    uint16_t    identifier;                        //!< An identifier to aid in matching Echo Replies
    uint16_t    sequence;                          //!< A sequence number to aid in matching Echo Replies
    uint16_t    dataLength;                        //!< Length of data field
    r_boolean_t request;                           //!< Identifier of request (R_TRUE) or reply (R_FALSE)
    uint16_t    options;                           //!< Tx options (enable/disable security)
    uint8_t     hopLimit;                          //!< Limit on the number of hops a packet is allowed before being discarded
    uint8_t     inlineData[];                      //!< Zero or more octets of arbitrary data
} r_icmp_echo_req_t;

/**
 * Confirm for an ICMPv6 echo message request
 */
typedef struct
{
    r_result_t result;  //!< Report the result of an ICMPv6 echo request
} r_icmp_echo_cfm_t;

/*!
    \struct r_icmp_error_ind_t
    \brief ICMP error indication
 */
typedef struct
{
    uint8_t  dstAddress[R_IPV6_ADDRESS_LENGTH]; //!< The 128-bit IPv6 address of the destination of the packet
    uint8_t  srcAddress[R_IPV6_ADDRESS_LENGTH]; //!< The 128-bit IPv6 address of the source of the packet
    uint8_t  type;                              //!< ICMP message type
    uint8_t  code;                              //!< Code values of for ICMP error messages
    uint32_t value;                             //!< Field of 32 bit, different use depending on error type
    uint16_t invokingPacketLength;              //!< The number of bytes contained in the invoking packet
    uint16_t status;                            //!< Rx status (secured/unsecured)
    uint8_t  lqi;
    uint8_t  invokingPacket[];                  //!< The actual invoking packet
} r_icmp_error_ind_t;

/*!
    \struct r_udp_data_ind_t
    \brief UDP data indication
 */
typedef struct
{
    uint8_t  dstAddress[R_IPV6_ADDRESS_LENGTH]; //!< The 128-bit IPv6 address of the destination of the packet
    uint16_t dstPort;                           //!< Has a meaning within the context of a particular internet destination address
    uint8_t  srcAddress[R_IPV6_ADDRESS_LENGTH]; //!< The 128-bit IPv6 address of the source of the packet
    uint16_t srcPort;                           //!< Port of the sending process
    uint16_t dataLength;                        //!< The number of bytes contained in the data
    uint16_t status;                            //!< Rx status (secured/unsecured)
    uint8_t  lqi;
    uint8_t  data[];                            //!< The actual UDP data
} r_udp_data_ind_t;

/*!
    \struct r_udp_data_req_t
    \brief UDP data request
 */
typedef struct
{
    r_ipv6addr_t dstAddress;   //!< The 128-bit IPv6 address of the destination of the packet
    uint16_t     dstPort;      //!< Has a meaning within the context of a particular internet destination address
    uint16_t     srcPort;      //!< Port of the sending process
    uint16_t     dataLength;   //!< The number of bytes contained in the data
    uint16_t     options;      //!< Tx options (enable/disable security)
    uint8_t      inlineData[]; //!< The actual UDP data
} r_udp_data_req_t;

/*!
    \struct r_udp_data_cfm_t
    \brief UDP data confirm
 */
typedef struct
{
    r_result_t result;  //!< Report the result of a UDP data request
} r_udp_data_cfm_t;

/*!
    \struct r_nwk_start_req_t
    \brief Network start request
 */
typedef struct
{
    uint16_t     panId;                                        //!< The 16-bit PAN identifier for use within the network
    uint16_t     panSize;                                      //!< The 16-bit PAN size
    uint16_t     useParent_BS_IE;                              //!< Use Parent BS-IE field of PAN-IE
    uint8_t      networkName[R_NETWORK_NAME_STRING_MAX_BYTES]; //!< Network Name
    r_ipv6addr_t ipAddr;                                       //!< The unique IPv6 address (GUA/ULA) to be used by RPL on the BR
    uint16_t     options;                                      //!< Options
} r_nwk_start_req_t;

/*!
    \struct r_nwk_start_cfm_t
    \brief Network start confirm
 */
typedef struct
{
    r_result_t result;  //!< Report the result of a startup attempt
} r_nwk_start_cfm_t;

/*!
    \struct r_nwk_join_req_t
    \brief Network join request
 */
typedef struct
{
    uint8_t  networkName[R_NETWORK_NAME_STRING_MAX_BYTES]; //!< Network Name
    uint16_t options;                                      //!< Options
} r_nwk_join_req_t;

/*!
    \struct r_nwk_join_cfm_t
    \brief Network join confirm
 */
typedef struct
{
    r_result_t result;  //!< Report the result of a join attempt
} r_nwk_join_cfm_t;

/*!
    \struct r_nwk_reset_req_t
    \brief Reset request
 */
typedef struct
{
    uint16_t options;  //!< Reset options
} r_nwk_reset_req_t;

/*!
    \struct r_nwk_reset_cfm_t
    \brief Reset confirm
 */
typedef struct
{
    r_result_t result;  //!< Report the result of a reset request
} r_nwk_reset_cfm_t;

/*!
    \struct r_nwk_erase_framecounter_req_t
    \brief Reset request
 */
typedef struct
{
    uint16_t options;  //!< Erase options
} r_nwk_erase_framecounter_req_t;

/*!
    \struct r_nwk_erase_framecounter_cfm_t
    \brief Reset confirm
 */
typedef struct
{
    r_result_t result;  //!< Report the result of a erase frame counter request
} r_nwk_erase_framecounter_cfm_t;

/*!
    \struct r_nwk_set_req_t
    \brief Set request
 */
typedef struct
{
    uint8_t  attrId;
    uint16_t attrLength;  // Need to set if size of attribute is variable.
    union
    {
        r_mac_attr_value_t mac;
        r_nwk_attr_value_t nwk;
    } attrValue;
} r_nwk_set_req_t;

/*!
    \struct r_nwk_set_cfm_t
    \brief Set request confirm
 */
typedef struct
{
    r_result_t result;
    uint8_t    attrId;
} r_nwk_set_cfm_t;

/**
 * Request to retrieve the data for the respective attribute ID from the NWK layer
 */
typedef struct
{
    uint8_t     attrId;      //!< The ID of the attribute whose value should be retrieved
    r_boolean_t contPrevReq; //!< Continue data collection from the previous GET request for this attribute
} r_nwk_get_req_t;

/*!
    \struct r_nwk_get_cfm_t
    \brief Get request confirm
 */
typedef struct
{
    r_result_t result;
    uint8_t    attrId;
    uint16_t   attrLength;
    union
    {
        r_mac_attr_value_t mac;
        r_nwk_attr_value_t nwk;
    } attrValue;
} r_nwk_get_cfm_t;

/**
 * EAPOL message type (derived from 802.15.9 KMP ID)
 */
typedef enum R_HEADER_UTILS_ATTR_PACKED
{
    R_EAPOL_TYPE_8021X                   = 1,   //!< IEEE 802.1X/MKA
    R_EAPOL_TYPE_4WH                     = 6,   //!< IEEE 802.11/4WH
    R_EAPOL_TYPE_GKH                     = 7,   //!< IEEE 802.11/GKH
    R_EAPOL_TYPE_CONVERT_FROM_EAP        = 250, //!< Just the EAP payload (just for sending)
    R_EAPOL_TYPE_CONVERT_FROM_4WH_OR_GKH = 251, //!< Automatically derive 4WH or GKH (just for sending)
} r_mac_eapol_type_t;

/**
 * EAPOL data request for outgoing messages
 */
typedef struct
{
    r_mac_eapol_type_t type;          //!< The KMP ID
    r_eui64_t          dst;           //!< The EUI-64 address of the destination
    r_eui64_t          authenticator; //!< The EUI-64 address that is written to the EA-IE
    uint16_t           dataLength;    //!< The length of the EAPOL message in bytes
    uint8_t            data[];        //!< The EAPOL message
} r_eapol_data_req_t;

#if !(R_MODEM_SERVER || R_MODEM_CLIENT)
/**
 * Memory optimized version of EAPOL data request for outgoing messages
 */
typedef struct
{
    r_mac_eapol_type_t type;          //!< The KMP ID
    r_eui64_t          dst;           //!< The EUI-64 address of the destination
    r_eui64_t          authenticator; //!< The EUI-64 address that is written to the EA-IE
    uint16_t           dataLength;    //!< The length of the data buffer in bytes
    uint8_t*           data;          //!< Pointer to the fully prepared MAC buffer containing the EAPOL message to send
} r_eapol_data_req_opt_t;
#endif /* !(R_MODEM_SERVER || R_MODEM_CLIENT) */

/**
 * Confirm message for an EAPOL data request
 */
typedef struct
{
    r_result_t result;
} r_eapol_data_cfm_t;

/**
 * EAPOL data indication for incoming messages
 */
typedef struct
{
    r_eui64_t          src;           //!< The EUI-64 address of the source
    r_eui64_t          authenticator; //!< The EUI-64 address of the authenticator (only if EA-IE was present)
    r_mac_eapol_type_t type;
    uint16_t           dataLength;    //!< The length of the EAPOL payload in bytes
    uint8_t            data[];        //!< The EAPOL payload
} r_eapol_data_ind_t;

/**
 * Indication to start the authentication process as a router node with the specified target
 */
typedef struct
{
    r_eui64_t target;  //!< The EUI-64 address of the target to authenticate with
} r_nwk_auth_rn_start_ind_t;

/**
 * Indication for the Router Node authentication module to delete the specified GTKs
 */
typedef struct
{
    uint8_t mask;  //!< Bitmask specifying the GTKs that should be deleted (bit 0 represents index 0; bit 3 is index 3)
} r_nwk_auth_rn_gtk_del_ind_t;

/**
 * Notification for the NWK layer that an authentication message has been processed
 */
typedef struct
{
    r_result_t status;
} r_nwk_auth_msg_result;

#if R_BR_AUTHENTICATOR_ENABLED

/**
 * PAN Version Increment request
 */
typedef struct
{
    uint8_t dummy;  // No data required
} r_nwk_pan_ver_inc_req_t;

/**
 * PAN Version Increment confirm
 */
typedef struct
{
    r_result_t result;
} r_nwk_pan_ver_inc_cfm_t;

/**
 * An incoming message for the DHCPv6 server
 */
typedef struct
{
    uint8_t  srcAddress[R_IPV6_ADDRESS_LENGTH]; //!< The IPv6 address of the sender
    uint16_t srcPort;                           //!< The UDP source port
    uint16_t dataLength;                        //!< The length of the message in bytes
    uint8_t  data[];                            //!< The DHCP message
} r_dhcp_data_ind_t;
#endif /* R_BR_AUTHENTICATOR_ENABLED */

/**
 * Indication for a DHCPv6 Vendor-specific Information Option received by the DHCPv6 client
 */
typedef struct
{
    r_eui64_t src;               //!< The EUI-64 of the sender
    uint32_t  enterprise_number; //!< The vendor's registered Enterprise Number as registered with IANA
    uint16_t  option_data_len;   //!< The length of the following option data in bytes
    uint8_t   option_data[];     //!< The DHCPv6 Vendor-specific Information Option data
} r_nwk_dhcp_vend_opt_data_ind_t;

/**
 * Indication for the current NWK join state of this device
 */
typedef struct
{
    r_nwk_join_state_t currentJoinState;  //!< The new/current join state of this device
} r_nwk_join_state_ind_t;

/**
 * Warning indication to signal non-critical (recoverable) errors to the application
 * @details These messages indicate failures like out-of-memory conditions, which may result in certain messages/data
 * being lost or certain operations to fail but the Wi-SUN FAN stack as a whole should continue normal operation.
 */
typedef struct
{
    uint8_t status;
} r_nwk_warning_ind_t;

/**
 * Fatal Error indication to signal a critical (unrecoverable) error to the application
 * @details If this message is sent, regular operation cannot be guaranteed anymore and subsequent behavior is undefined
 */
typedef struct
{
    uint8_t status;
} r_nwk_fatal_error_ind_t;

/**
 * Internal NWK message for the tick of the cyclic timer
 */
typedef struct
{
    uint32_t ticktimeMs;
} r_nwk_cyctimer_tick_ntf_t;

typedef struct
{
    uint8_t _reserved;
} r_nwk_suspend_req_t;

typedef struct
{
    r_result_t result;
} r_nwk_suspend_cfm_t;

typedef struct
{
    uint32_t suspendedTimeMs;
} r_nwk_resume_req_t;

typedef struct
{
    r_result_t result;
} r_nwk_resume_cfm_t;


#if R_DEV_TBU_ENABLED
/**
 * Indication for Frame Subscription
 */
typedef struct
{
    uint8_t  srcAddr[8];   //!< The 64-bit EUI64 address of the source of the packet
    uint8_t  dstAddr[8];   //!< The 64-bit EUI64 address of the destination of the packet
    uint16_t frameLength;  //!< The number of bytes contained in the frame
    uint8_t  frame[];      //!< The frame
} r_subscription_frame_ind_t;
#endif

/**
 * IP data indication
 */
typedef struct
{
    uint16_t packetLength; //!< The length of the IP packet
    uint8_t  packet[];     //!< The IP packet
} r_ip_data_ind_t;

/**
 * IP data request
 */
typedef struct
{
    uint16_t options;      //!< Tx options
    uint16_t packetLength; //!< The length of the IP packet
    uint8_t  packet[];     //!< The IP packet
} r_ip_data_req_t;

/**
 * IP data confirm
 */
typedef struct
{
    r_result_t result;  //!< Result code of the corresponding IP data request
} r_ip_data_cfm_t;

/**
 * Generic confirm message from the NWK layer for various NWK API requests that do not return specific data
 */
typedef struct
{
    r_result_t result;  //!< Result code of the corresponding NWK API request
} r_nwk_generic_cfm_t;

#if (R_MODEM_SERVER || R_MODEM_CLIENT)
/**
 * Logging indication from the Wi-SUN FAN stack (Modem Server)
 */
typedef struct
{
    uint16_t size;   //!< The number of bytes following
    uint8_t  data[]; //!< Binary log data (RLog)
} r_nwk_log_ind_t;
#endif

/**
 * Messages that may be sent from the application layer to the network layer (requests)
 * and vice versa (confirms and indications)
 */
typedef struct
{
    r_os_msg_header_t hdr;  //!< Message header (handled by OS)
    union
    {
        r_nwk_reset_req_t              resetReq;                    //!< Reset request
        r_nwk_reset_cfm_t              resetCfm;                    //!< Reset confirm
        r_nwk_start_req_t              nwkStartReq;                 //!< Network start request
        r_nwk_start_cfm_t              nwkStartCfm;                 //!< Network start confirm
        r_nwk_join_req_t               nwkJoinReq;                  //!< Network join request
        r_nwk_join_cfm_t               nwkJoinCfm;                  //!< Network join confirm
        r_icmp_echo_req_t              icmpEchoReq;                 //!< ICMP echo request
        r_icmp_echo_cfm_t              icmpEchoCfm;                 //!< ICMP echo confirm
        r_icmp_echo_reply_ind_t        icmpEchoInd;                 //!< ICMP echo indication
        r_icmp_error_ind_t             icmpErrorInd;                //!< ICMP error indication
        r_udp_data_req_t               udpReq;                      //!< UDP data request
        r_udp_data_cfm_t               udpCfm;                      //!< UDP data confirm
        r_udp_data_ind_t               udpInd;                      //!< UDP data indication
        r_nwk_set_req_t                setReq;                      //!< Set request
        r_nwk_set_cfm_t                setCfm;                      //!< Set confirm
        r_nwk_get_req_t                getReq;                      //!< Get request
        r_nwk_get_cfm_t                getCfm;                      //!< Get confirm
        r_nwk_join_state_ind_t         joinStateInd;                //!< NWK Join State Update Indication
#if R_BORDER_ROUTER_ENABLED
        r_nwk_pan_ver_inc_req_t        panVersionIncReq;            //!< PAN Version Increment request
        r_nwk_pan_ver_inc_cfm_t        panVersionIncCfm;            //!< PAN Version Increment confirm
        r_nwk_device_status_ind_t      deviceStatusUpdateInd;       //!< Status Update Indication for other FAN devices
        r_dhcp_data_ind_t              dhcpInd;                     //!< DHCPv6 data indication
#endif
        r_nwk_dhcp_vend_opt_data_ind_t dhcpVendorOptionInd;         //!< DHCPv6 Vendor-specific Information Option data indication
        r_eapol_data_req_t             eapolDataReq;                //!< EAPOL data request
#if !(R_MODEM_SERVER || R_MODEM_CLIENT)
        r_eapol_data_req_opt_t         eapolDataReqOpt;             //!< Raw EAPOL data request
#endif
        r_eapol_data_cfm_t             eapolDataCfm;                //!< EAPOL data confirm
        r_eapol_data_ind_t             eapolDataInd;                //!< EAPOL data indication
        r_nwk_auth_msg_result          authMsgResult;               //!< Processing result of an EAPOL message
        r_nwk_auth_rn_start_ind_t      authRnStartInd;              //!< Start the authentication as a router node
        r_nwk_auth_rn_gtk_del_ind_t    authRnGtkDeleteInd;          //!< Delete a GTK from the router node authentication module

        r_ip_data_req_t                ipReq;                       //!< IP data request
        r_ip_data_cfm_t                ipCfm;                       //!< IP data confirm
        r_ip_data_ind_t                ipInd;                       //!< IP data indication
#if R_DEV_TBU_ENABLED
        r_subscription_frame_ind_t     subscriptionFrameIndication; //!< Subscription Frame indication
#endif

#if (R_MODEM_SERVER || R_MODEM_CLIENT)
        r_nwk_log_ind_t                logInd;                      //!< Logging Indication
#endif

        r_nwk_warning_ind_t            warningInd;    //!< Warning indication
        r_nwk_fatal_error_ind_t        fatalErrorInd; //!< Fatal error indication

        r_nwk_suspend_req_t            suspendReq;
        r_nwk_suspend_cfm_t            suspendCfm;
        r_nwk_resume_req_t             resumeReq;
        r_nwk_resume_cfm_t             resumeCfm;

        r_nwk_generic_cfm_t            genericCfm; //!< Generic confirm type (used for various NWK API requests)

        r_nwk_erase_framecounter_req_t eraseFrameCounterReq;
        r_nwk_erase_framecounter_cfm_t eraseFrameCounterCfm;

        r_nwk_cyctimer_tick_ntf_t      cycTimeTickNtf; //!< (internal)
    } message;
} r_nwk_msg_t;

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

typedef struct
{
    uint8_t nwkSecurityEnabled;
    uint8_t deviceType;
} r_nwk_attr_t;  // struct

// config

typedef struct
{
    uint8_t joinCount;  //!< Join Count (0 = free entry)
    uint8_t group[15];  //!< Group Address (without leading 0xff)
} r_multicast_group_entry_t;

typedef struct
{
    uint16_t    srcPort;        //!< UDP source port
    uint16_t    dstPort;        //!< UDP destination port
    uint16_t    checkSum;       //!< UDP checksum
    uint16_t    udpOffset;      //!< Offset to UDP header
    uint16_t    length;         //!< UDP payload length
    r_boolean_t checkSumElided; //!< Flag indicating elided checksum
    r_boolean_t compUdpPresent; //!< Flag indicating present UDP header
} r_hc_upd_uncomp_t;

typedef struct
{
    uint8_t*          buffer;        //!< Pointer to reassembly buffer
    uint8_t           srcAddress[8]; //!< Address of fragment origin
    r_boolean_t       secured;       //!< Is the fragment encrypted
    uint16_t          dtgTag;        //!< Assigned tag for fragmentation process
    uint16_t          offset;        //!< Offset (IN BYTES) for the current process
    uint16_t          dtgSize;       //!< Datagram size in bytes
    r_hc_upd_uncomp_t udpHdrInf;     //!< Structure containing information for UDP header reconstruction
    r_boolean_t       running;       //!< Flag to indicate running process
    uint32_t          reassemblyTimeout;
} r_frag_reassembly_process_table_t;

typedef struct
{
    uint8_t apl_mbxId;  //!< Mailbox for messages to the application layer
    uint8_t apl_mplId;  //!< Memory ID for the application layer
    uint8_t apl_nonblocking_mbxId;
    uint8_t apl_blocking_mbxId;

    uint8_t nwk_tskId;       //!< Task ID of the network layer task
    uint8_t nwk_mbxId;       //!< Mailbox ID for messages to the network layer
    uint8_t nwk_inter_mbxId; //!< Mailbox ID for messages exchanged within the network layer
    uint8_t nwk_mplId;       //!< Memory ID for requests from the network layer to the MAC layer (NWK->MAC)
    uint8_t dataind_mplId;   //!< Memory ID for indications from the network layer to the application layer (NWK->APL)
    uint8_t cyclic_tick_ntf_mplId;
    uint8_t nwk_cyclicId;

    uint8_t mac_tskId;
    uint8_t mac_flgId;
    uint8_t mac_mbxId;
    uint8_t mac_rx_mplId;
    uint8_t mac_warning_ind_mplId;
    uint8_t mac_fatal_ind_mplId;
    uint8_t mac_rfdrv_semId;
} r_sys_config_t;

typedef struct
{
    /* NWK */
    uint8_t*                           p_nwkGlobal;
    uint32_t                           nwkSizeofGlobal;
    const r_sys_config_t*              p_sysConfigTBL;

    /* 6LoWPAN : Reassembly Process Table */
    uint16_t                           fragMaxReassemblyProcessTableEntries;
    r_frag_reassembly_process_table_t* p_fragReassemblyProcessTable;

    /* Multicast Groups : Multicast Groups Table */
    uint16_t                           multicastMaxGroupTableEntries;
    r_multicast_group_entry_t*         p_multicastGroupTable;
} r_nwk_table_resources_t;


typedef struct
{

    // 6LoWPAN Fragment
    uint32_t fragReassemblyTimeout;

    const r_nwk_table_resources_t* const p_nwkTableResources;
} r_nwk_config_params_t;


/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/
// --------------------------------------------
// API functions (for initialization)
// --------------------------------------------
r_result_t R_NWK_Init(void);
r_result_t R_NWK_ResetRequest(void);
r_result_t R_NWK_EraseFrameCounterRequest(void);

// --------------------------------------------
// API functions (for start/join network)
// --------------------------------------------

/**
 * Create and start a new network (Border Router)
 * @param panId The PAN identifier of the network
 * @param panSize The PAN size of the network
 * @param useParent_BS_IE Use Parent BS-IE field of PAN-IE
 * @param p_networkName The network name as a null-terminated string
 * @param ipAddr The unique IP address (GUA/ULA) that the border router should use (also used by RPL)
 * @param options
 * @return R_RESULT_SUCCESS on successful transmission. Appropriate error code otherwise.
 */
r_result_t R_NWK_StartRequest(uint16_t            panId,
                              uint16_t            panSize,
                              uint8_t             useParent_BS_IE,
                              const char*         p_networkName,
                              const r_ipv6addr_t* ipAddr,
                              uint16_t            options);

/**
 * Join an existing network with the specified name (Router Node)
 * @param p_networkName The network name as a null-terminated string
 * @param options
 * @return R_RESULT_SUCCESS on successful transmission. Appropriate error code otherwise.
 */
r_result_t R_NWK_JoinRequest(const char* p_networkName, uint16_t options);

// --------------------------------------------
// API functions (for send/receive frames)
// --------------------------------------------
r_result_t R_ICMP_EchoRequest(const uint8_t* p_dstAddress,
                              uint16_t       identifier,
                              uint16_t       sequence,
                              const uint8_t* p_data,
                              uint16_t       dataLength,
                              uint16_t       options,
                              uint8_t        hopLimit);

/*!
   \fn          r_result_t R_UDP_DataRequest(
                              uint8_t* p_dstAddress,
                              uint16_t dstPort,
                              uint16_t srcPort,
                              uint8_t* p_data,
                              uint16_t dataLength,
                              uint16_t options );
   \brief       Network function to initiate UDP data request
   \param[in]   p_dstAddress destination address
   \param[in]   dstPort    destination port
   \param[in]   srcPort    source port
   \param[in]   p_data     pointer to data
   \param[in]   dataLength  length of data
   \param[in]   options     Tx options
   \return      R_RESULT_SUCCESS in case of proper processing, R_RESULT_FAILED otherwise.
 */
r_result_t R_UDP_DataRequest(const uint8_t* p_dstAddress,
                             uint16_t       dstPort,
                             uint16_t       srcPort,
                             const uint8_t* p_data,
                             uint16_t       dataLength,
                             uint16_t       options);

/**
 * Request to send a raw IP packet.
 * @param p_data Pointer to the raw IP packet data.
 * @param dataLength The length of the IP data in bytes.
 * @param options The TX options.
 * @return R_RESULT_SUCCESS in case of proper processing. Appropriate error code otherwise.
 */
r_result_t R_NWK_IP_DataRequest(const uint8_t* p_data, uint16_t dataLength, uint16_t options);

// --------------------------------------------
// API functions (for attributes)
// --------------------------------------------
/**
 * Retrieve the length of the specified attribute.
 * @attention For variable sized attribute (e.g., schedules, tables) this returns the maximum size
 * @param attrId The ID of the attribute whose length should be retrieved.
 * @return The length of the attribute or 0 if the attribute is not known
 */
uint16_t R_NWK_GetAttrLength(uint8_t attrId);

/**
 * Retrieve the value of the specified attribute.
 * @param attrId The ID of the attribute that should be retrieved.
 * @param[out] out Pointer to an output buffer to hold the attribute value.
 * @param outSize The size of the output buffer.
 * @param[out] outWritten The number of bytes that have been written to the output buffer (i.e. the attribute length).
 * @return R_RESULT_SUCCESS, if the requested attribute was successfully retrieved and written to the output buffer.
 * Otherwise, a result code that specifies the error cause.
 */
r_result_t R_NWK_GetRequestVarSize(uint8_t attrId, void* out, uint16_t outSize, uint16_t* outWritten);

/**
 * Continue the previous GET request for an attribute and retrieve the next part of its data
 * @param attrId The ID of the attribute that should be retrieved.
 * @param[out] out Pointer to an output buffer to hold the attribute value.
 * @param outSize The size of the output buffer.
 * @param[out] outWritten The number of bytes that have been written to the output buffer (i.e. the attribute length).
 * @return R_RESULT_SUCCESS, if the requested attribute was successfully retrieved and written to the output buffer.
 * Otherwise, a result code that specifies the error cause.
 */
r_result_t R_NWK_GetRequestVarSizeCont(uint8_t attrId, void* out, uint16_t outSize, uint16_t* outWritten);

/**
 * Retrieve the value of the specified attribute.
 * @param attrId The ID of the attribute that should be retrieved.
 * @param[out] out Pointer to an output buffer to hold the attribute value.
 * @param outSize The size of the output buffer.
 * @return R_RESULT_SUCCESS, if the requested attribute was successfully retrieved and written to the output buffer.
 * Otherwise, a result code that specifies the error cause.
 */
r_result_t R_NWK_GetRequest(uint8_t attrId, void* out, uint16_t outSize);

/**
 * Retrieve the value of the specified attribute in (potentially) multiple parts (suitable for large attributes)
 * @param attrId The ID of the attribute that should be retrieved.
 * @param[out] out Pointer to an output buffer to hold the requested attribute value.
 * @param outSize The size of the output buffer.
 * @param proceedPrevious True, if this request should proceed the data collection where the last request terminated.
 * @return R_RESULT_SUCCESS, if the complete attribute value was successfully retrieved and written to the output buffer.
 * @return R_RESULT_SUCCESS_ADDITIONAL_DATA, if a part of the data was retrieved successfully but more data is available.
 * @return Appropriate error code if the data could not be retrieved.
 */
r_result_t R_NWK_GetRequestMultipart(uint8_t attrId, void* out, uint16_t outSize, r_boolean_t proceedPrevious);

/**
 * Set the value of the specified attribute.
 * @param attrId The ID of the attribute that should be set.
 * @param p_attrValue Pointer to the value that the attribute should be set to.
 * @param attrLength The size of the value that the attribute should be set to.
 * @return R_RESULT_SUCCESS, if the requested attribute was successfully set. Otherwise, a result code that specifies
 * the error cause.
 */
r_result_t R_NWK_SetRequest(uint8_t attrId, const void* p_attrValue, uint16_t attrLength);

/**
 * Return the actual size of the msg->message field taking variable sized members into account
 * @attention for internal use only
 * @param msg the network message
 * @return the size of the msg->message field
 */
uint16_t R_NWK_MessageSize(const r_nwk_msg_t* msg);

/**
 * Reset the PAN timeout timer.
 * @return R_RESULT_SUCCESS on successful processing. Appropriate error code otherwise.
 */
r_result_t R_NWK_ResetPanTimeoutRequest();

/**
 * Leave the network that this device is currently connected to and reset NWK and lower layers.
 * @details This device will notify its children (via poisoned DIO), the border router (via zero-path DAO) and parent(s)
 * (via zero-lifetime NS) that it is about to leave network. Afterwards, the NWK layer and lower layers are reset.
 * @retval R_RESULT_SUCCESS if all operations succeeded and the device has left the network.
 * @retval R_RESULT_RPL_FAILED if at least one of the de-registration operations failed but device has left the network.
 * @retval R_RESULT_UNSUPPORTED_FEATURE if this device is not a router node so the request was denied.
 * @retval R_RESULT_ILLEGAL_STATE if this device is not in join state 5 so the request was denied.
 */
r_result_t R_NWK_LeaveNetworkRequest();

#if R_BORDER_ROUTER_ENABLED
/**
 * Increase the PAN version.
 * @return R_RESULT_SUCCESS on successful processing. Appropriate error code otherwise.
 */
r_result_t R_NWK_IncreasePanVersionRequest();

/**
 * Increase the LFN version.
 * @details The PAN version will also be increased (as stated in FANTPS-1.1v03).
 * @return R_RESULT_SUCCESS on successful processing. Appropriate error code otherwise.
 */
r_result_t R_NWK_IncreaseLfnVersionRequest();

/**
 * Initiate a refresh of all source routes by incrementing the Destination Advertisement Trigger Sequence Number.
 * @details Incrementing the DTSN will cause all immediate child nodes to send a DAO and increment their own DTSN.
 * @return R_RESULT_SUCCESS if the DTSN increment was successful. Appropriate error code otherwise.
 */
r_result_t R_NWK_RplRouteRefreshRequest();

/**
 * Initiate a global repair of all source routes by incrementing the DODAG version and the DTSN.
 * @details This will cause all nodes to re-perform RPL parent selection.
 * @retval R_RESULT_SUCCESS if the global repair was started.
 * @retval R_RESULT_ILLEGAL_STATE if this device is not a border router or the device has not been started yet.
 */
r_result_t R_NWK_RplGlobalRepairRequest();

#endif /* R_BORDER_ROUTER_ENABLED */

/**
 * Send an EAPOL message
 * @param type The EAPOL type (KMP ID)
 * @param dst The EUI-64 destination address
 * @param authenticator The authenticator EUI-64 for the EA-IE, which may be NULL
 * @param data The EAPOL message to send
 * @param dataLength The size of data in bytes
 * @return R_RESULT_SUCCESS on successful transmission. Appropriate error code otherwise.
 */
r_result_t R_NWK_EapolDataRequest(r_mac_eapol_type_t type, const uint8_t* dst, const uint8_t* authenticator,
                                  const uint8_t* data, uint16_t dataLength);

#if !(R_MODEM_SERVER || R_MODEM_CLIENT)
/**
 * Send an EAPOL message that is embedded in a fully prepared buffer for the NWK task allowing memory optimizations
 * @param dst The EUI-64 destination address
 * @param data A data buffer representing an MCPS Data Request including an MPX-IE with the EAPOL message to send.
 * @param dataLength The size of the data buffer in bytes
 * @return R_RESULT_SUCCESS on successful transmission. Appropriate error code otherwise.
 */
r_result_t R_NWK_EapolDataRequestOptimized(const uint8_t* dst, uint8_t* data, uint16_t dataLength);
#endif /* !(R_MODEM_SERVER || R_MODEM_CLIENT) */

/**
 * Indication to the NWK task that an EAPOL message has been processed
 * @param status The processing result of the EAPOL message
 * @return R_RESULT_SUCCESS in case of proper processing by NWK. Appropriate error code otherwise.
 */
r_result_t R_NWK_EapolDataProcessed(r_result_t status);

/**
 * Indication to the NWK task that an EAP-TLS exchange is starting
 * @details This may be used to free non-critical memory before EAP-TLS begins
 * @return R_RESULT_SUCCESS in case of proper processing by NWK. Appropriate error code otherwise.
 */
r_result_t R_NWK_EapTlsIsStarting();

/**
 * Indication to the NWK task that the current EAP-TLS exchange is done
 * @details This can be used to re-allocate non-critical memory after EAP-TLS ended
 * @return R_RESULT_SUCCESS in case of proper processing by NWK. Appropriate error code otherwise.
 */
r_result_t R_NWK_EapTlsIsDone();

// --------------------------------------------
// API functions (for low power consumption)
// --------------------------------------------
r_result_t R_NWK_Suspend(void);
r_result_t R_NWK_Resume(uint32_t suspendedTimeMs);

#endif /* R_NWK_API_H */
