/*
 * Copyright (c) 2010, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 * \file
 *  Public API declarations for ContikiRPL.
 * \author
 *  Joakim Eriksson <joakime@sics.se> & Nicolas Tsiftes <nvt@sics.se>
 *
 */

#ifndef RPL_H
#define RPL_H

#include "rpl-conf.h"

#include "lib/list.h"

#include "net/uip.h"
#include "net/uip-ds6.h"
#include "net/uip-ds6-source-route.h"

#include "sys/ctimer.h"

#include "r_nd.h"

/*---------------------------------------------------------------------------*/
typedef uint16_t rpl_rank_t;
typedef uint16_t rpl_ocp_t;

/*---------------------------------------------------------------------------*/
struct rpl_instance;
struct rpl_dag;

/*---------------------------------------------------------------------------*/
#define RPL_PARENT_FLAG_UPDATED           0x1
#define RPL_PARENT_FLAG_LINK_METRIC_VALID 0x2

typedef r_nd_neighbor_cache_entry_t rpl_parent_t;

/*---------------------------------------------------------------------------*/
/* RPL DIO prefix suboption */
struct rpl_prefix
{
    r_ipv6addr_t prefix;
    uint32_t     lifetime;
    uint8_t      length;
    uint8_t      flags;
};
typedef struct rpl_prefix rpl_prefix_t;

/*---------------------------------------------------------------------------*/
/* Directed Acyclic Graph */
struct rpl_dag
{
    r_ipv6addr_t         dag_id;
    rpl_rank_t           min_rank; /* should be reset per DAG iteration! */
    uint8_t              version;
    uint8_t              grounded;
    uint8_t              preference;
    uint8_t              used;

    /* live data for the DAG */
    uint8_t              joined;
    rpl_parent_t*        preferred_parent;
    rpl_rank_t           rank;
    struct rpl_instance* instance;
    rpl_prefix_t         prefix_info;
    uint32_t             lifetime;
};
typedef struct rpl_dag      rpl_dag_t;
typedef struct rpl_instance rpl_instance_t;

/*---------------------------------------------------------------------------*/
/*
 * API for RPL objective functions (OF)
 *
 * reset(dag)
 *
 *  Resets the objective function state for a specific DAG. This function is
 *  called when doing a global repair on the DAG.
 *
 * neighbor_link_callback(parent, known, etx)
 *
 *  Receives link-layer neighbor information. The parameter "known" is set
 *  either to 0 or 1. The "etx" parameter specifies the current
 *  ETX(estimated transmissions) for the neighbor.
 *
 * best_parent(parent1, parent2)
 *
 *  Compares two parents and returns the best one, according to the OF.
 *
 * best_dag(dag1, dag2)
 *
 *  Compares two DAGs and returns the best one, according to the OF.
 *
 * rank_via_parent(parentk)
 *
 *  Calculates the rank value using the parent rank and link metric.
 *
 * update_metric_container(dag)
 *
 *  Updates the metric container for outgoing DIOs in a certain DAG.
 *  If the objective function of the DAG does not use metric containers,
 *  the function should set the object type to RPL_DAG_MC_NONE.
 */
struct rpl_of
{
    void          (* reset)(struct rpl_dag*);
    rpl_parent_t* (* best_parent)(rpl_parent_t*, rpl_parent_t*);
    rpl_dag_t*    (* best_dag)(rpl_dag_t*, rpl_dag_t*);
    rpl_rank_t    (* rank_via_parent)(rpl_parent_t*);
    void          (* update_metric_container)(rpl_instance_t*);
    rpl_ocp_t     ocp;
};
typedef struct rpl_of rpl_of_t;

/* Declare the selected objective function. */
extern rpl_of_t RPL_OF;

/*---------------------------------------------------------------------------*/
/* Instance */
struct rpl_instance
{
    /* DAG configuration */
    rpl_of_t*     of;
    rpl_dag_t*    current_dag;
    rpl_dag_t     dag_table[RPL_MAX_DAG_PER_INSTANCE];
    uint8_t       instance_id;
    uint8_t       used;
    uint8_t       dtsn_out;
    uint8_t       mop;
    uint8_t       dio_intdoubl;
    uint8_t       dio_intmin;
    uint8_t       dio_redundancy;
    uint8_t       default_lifetime;
    uint8_t       dio_intcurrent;
    uint8_t       dio_send; /* for keeping track of which mode the timer is in */
    uint8_t       dio_counter;
    rpl_rank_t    max_rankinc;
    rpl_rank_t    min_hoprankinc;

    uint16_t      lifetime_unit; /* lifetime in seconds = l_u * d_l */
#if RPL_CONF_STATS
    uint16_t      dio_totint;
    uint16_t      dio_totsend;
    uint16_t      dio_totrecv;
#endif /* RPL_CONF_STATS */
    clock_time_t  dio_next_delay; /* delay for completion of dio interval */
    struct ctimer dio_timer;
    struct ctimer unicast_dio_timer;
    rpl_parent_t* unicast_dio_target;
};

/*
 * DAO control structures
 */
struct rpl_dao_control
{
    /* current registered DAO sequence that is waiting for DAO-ACK */
    uint8_t       dao_seqno;
    uint8_t       dao_transmissions;  //!< The number of transmission that have been attempted for this DAO by now
    uint16_t      path_lifetime;
    uint8_t       path_sequence;
    uint8_t       requestDaoAck : 1;
    struct ctimer dao_timer;
    struct ctimer dao_lifetime_timer;
#if (R_WISUN_FAN_VERSION >= 110)
    r_ipv6addr_t  lfn_address; // The IPv6 address of the LFN for which this DAO is sent; All-zero is our own DAO
    r_boolean_t   solicited;   // True, if this DAO transmission was triggered by a NS-with-EARO. False otherwise.
#endif
};
typedef struct rpl_dao_control rpl_dao_control_t;

/*---------------------------------------------------------------------------*/
/* Public RPL functions. */
void            rpl_init(void);
rpl_dag_t*      rpl_set_root(uint8_t instance_id, const r_ipv6addr_t* dag_id);
int             rpl_set_prefix_and_addr(rpl_dag_t* dag, const r_ipv6addr_t* newAddr, unsigned prefixLength);
int             rpl_initiate_global_repair(uint8_t instance_id);
rpl_dag_t*      rpl_get_any_dag(void);
rpl_instance_t* rpl_get_instance(uint8_t instance_id);
int             rpl_update_header_empty(uint8_t rpl_opt_hdr_flags);
int             rpl_update_header_final(const r_ipv6addr_t* addr, r_boolean_t forwarding);
int             rpl_verify_header(int uip_ext_opt_offset, r_boolean_t* direction_up,  uint8_t* rpl_opt_flags);
r_result_t      rpl_insert_header(void);
void            rpl_remove_header(void);
uint8_t         rpl_invert_header(void);
r_ipv6addr_t*   rpl_get_parent_ipaddr(rpl_parent_t* nbr);
rpl_parent_t*   rpl_get_parent(uip_lladdr_t* addr);
rpl_rank_t      rpl_get_parent_rank(uip_lladdr_t* addr);
uint16_t        rpl_get_parent_link_metric(const uip_lladdr_t* addr);
uip_ds6_nbr_t*  rpl_get_nbr(rpl_parent_t* parent);

#if RPL_CONF_NON_STORING_ROOT_SUPPORT
void rpl_extract_final_destination_from_srh(uint8_t*            source_route_header,
                                            const r_ipv6addr_t* current_destination,
                                            r_ipv6addr_t*       final_destination);
void rpl_extract_final_destination(r_ipv6addr_t* final_destination);
#endif // RPL_CONF_NON_STORING_ROOT_SUPPORT

/**
 * \brief Update the RPL source routing header for non-storing mode
 *        according to (RFC6554 Section 4.2).
 * \retval 0 OK; continue processing
 * \retval 1 error seg_left > n; send appropriate ICMP6_PARAM_PROB
 * \retval 2 error multicast address; drop
 * \retval 3 error loop detected; send appropriate ICMP6_PARAM_PROB
 * \retval 4 error seg-left > ttl; send ICMP6_TIME_EXCEED_TRANSIT
 * \retval 5 error final hop not on link; send ICMP6_DST_UNREACH_SOURCEROUTE
 */
int rpl_update_source_routing_header(void);

uip_ds6_source_route_t* rpl_add_source_route(rpl_dag_t* dag, r_ipv6addr_t* prefix, uint8_t prefix_len, uint8_t lifetime, r_ipv6addr_t* transit_addr[], uint8_t path_control[], uint8_t nr_transits);

/*---------------------------------------------------------------------------*/
#endif /* RPL_H */
