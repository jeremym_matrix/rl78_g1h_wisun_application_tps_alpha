/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
#ifndef __RM_mac_mlme_H
#define __RM_mac_mlme_H
/*******************************************************************************
 * file name    : mac_mlme.h
 * description  : The definition for MLME-SAP
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/* --- --- */
#include "mac_types.h"
#include "mac_msg.h"
#ifdef R_HYBRID_PLC_RF
#include "mac_neighbors.h"
#else
#include "r_mac_ie.h"
#endif /* R_HYBRID_PLC_RF */

#if R_PHY_TYPE_CWX_M
#include "r_phy.h"
#include "r_phy_def.h"
#endif

/******************************************************************************
    Macro Definitions
******************************************************************************/

/* The Primitive (request)  length  (MLME-SAP) */
#ifdef R_HYBRID_PLC_RF
#define RM_MLME_SCAN_request_length             (sizeof(RmMlmeScanRequestT) - sizeof(RmMsgHdrT))
#define RM_MLME_START_request_length            (sizeof(RmMlmeStartRequestT) - sizeof(RmMsgHdrT))
#endif /* R_HYBRID_PLC_RF */
#define RM_MLME_GET_request_length              (sizeof(RmMlmeGetRequestT) - sizeof(RmMsgHdrT))
#define RM_MLME_RESET_request_length            (sizeof(RmMlmeResetRequestT) - sizeof(RmMsgHdrT))
#define RM_MLME_SET_request_length              (sizeof(RmMlmeSetRequestT) - sizeof(RmMsgHdrT))
#define RM_MLME_WSASYNCFRAME_request_length     (sizeof(RmMlmeWSAsyncFrameRequestT) - sizeof(RmMsgHdrT))
#define RM_MLME_GET_PIB_Index_length            (8)

/* The Primitive Id (confirm)  definition (MLME-SAP) */
#define RM_MLME_COMM_STATUS_indication_length   (sizeof(RmMlmeCommStatusIndicationT) - sizeof(r_os_msg_header_t))
#ifdef R_HYBRID_PLC_RF
#define RM_MLME_BEACON_NOTIFY_indication_length (sizeof(RmMlmeBeaconNotifyIndicationT) - sizeof(r_os_msg_header_t))
#define RM_MLME_SCAN_confirm_length             (sizeof(RmMlmeScanConfirmT) - sizeof(RmMsgHdrT))
#define RM_MLME_START_confirm_length            (sizeof(RmMlmeStartConfirmT) - sizeof(RmMsgHdrT))
#define RM_MLME_FRAMECOUNT_indication_length    (sizeof(RmMlmeFrameCountIndicationT) - sizeof(r_os_msg_header_t))
#endif /* R_HYBRID_PLC_RF */
#define RM_MLME_GET_confirm_length              (sizeof(RmMlmeGetConfirmT) - sizeof(RmMsgHdrT))
#define RM_MLME_RESET_confirm_length            (sizeof(RmMlmeResetConfirmT) - sizeof(RmMsgHdrT))
#define RM_MLME_SET_confirm_length              (sizeof(RmMlmeSetConfirmT) - sizeof(RmMsgHdrT))
#define RM_MLME_WSASYNCFRAME_confirm_length     (sizeof(RmMlmeWSAsyncFrameConfirmT) - sizeof(RmMsgHdrT))
#define RM_MLME_WSASYNCFRAME_indication_length  (sizeof(RmMlmeWSAsyncFrameIndicationT) - sizeof(RmMsgHdrT))

/* The Primitive Id (original indication)  definition (MLME-SAP) */
#define RM_WARNING_indication_length            (sizeof(RmWarningIndicationT) - sizeof(r_os_msg_header_t))
#define RM_FATAL_ERROR_indication_length        (sizeof(RmFatalErrorIndicationT) - sizeof(r_os_msg_header_t))

R_HEADER_UTILS_PRAGMA_PACK_1


/******************************************************************************
    Type Definitions
******************************************************************************/
#ifdef R_HYBRID_PLC_RF

/* The Type definition (MLME-SAP dependent type)                     */
/* PAN Descripter Type */
typedef struct
{
    RmPANIdentifierT CoordPANId;
    RmShortAddressT  CoordAddress;
    uint8_t          LinkQuality;
} RmMlmePANDescriptorT;

/* PAN Descripter List Type */
#define RM_aMaxPANDescriptors (5U)
typedef RmMlmePANDescriptorT
    RmMlmePANDescriptorListT[RM_aMaxPANDescriptors];

/* ED List Type */
typedef RmEnergyLevelT RmMlmeEnergyDetectListT[RM_aMaxChannel + 1U];

/* Scan Result Union Type */
typedef union
{
    RmMlmeEnergyDetectListT  energyDetectList;
    RmMlmePANDescriptorListT PANDescriptorList;
} RmMlmeScanDataT;

/*
 * Message MLME-BEACON.request format
 */
typedef struct
{
    RmMsgHdrT        mm_Hdr;
    RmOctetT         dstAddrMode;
    RmPANIdentifierT dstPANId;
    RmDeviceAddressT dstAddr;
} RmMlmeBeaconRequestT;

#endif /* R_HYBRID_PLC_RF */

/*
 * Message MLME-WS-ASYNC-FRAME.request format
 */
typedef struct
{
    RmMsgHdrT              mm_Hdr;
    RmMACPANOperationEnumT mm_Operation;
    r_ie_wh_frame_type_t   mm_FrameType;
    RmUBYTE                mm_SrcAddrMode;
    RmPANIdentifierT       mm_SrcPANId;
    RmDeviceAddressT       mm_SrcAddr;
    RmUBYTE                mm_PhyChannel;
    r_ie_wh_t              mm_headerIE;
    r_ie_wp_t              mm_payloadIE;
    RmSecParamT            mm_SecParam;
} RmMlmeWSAsyncFrameRequestT;

#define RmMlmeInitializeWSAsyncFrameRequest(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_WSASYNCFRAME_request, RM_MLME_WSASYNCFRAME_request_length)

/*
 * Message MLME-WS-ASYNC-FRAME.confirm format
 */
typedef struct
{
    RmMsgHdrT  mm_Hdr;
    RmMacEnumT mm_status;
} RmMlmeWSAsyncFrameConfirmT;

#define RmMlmeInitializeWSAsyncFrameConfirm(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_WSASYNCFRAME_confirm, RM_MLME_WSASYNCFRAME_confirm_length)

/*
 * Message MLME-WS-ASYNC-FRAME.indication format
 */
typedef struct
{
    RmMsgHdrT        mm_Hdr;
    RmPANIdentifierT mm_srcPANId;
    RmDeviceAddressT mm_srcAddr;
    RmUBYTE          mm_linkQuality;
    RmUBYTE          mm_keyIndex;        /* the key index used for encryption */
    r_ie_wh_t        mm_headerIE;
    r_ie_wp_t        mm_payloadIE;
} RmMlmeWSAsyncFrameIndicationT;

#define RmMlmeInitializeWSAsyncFrameIndication(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_WSASYNCFRAME_indication, RM_MLME_WSASYNCFRAME_indication_length)

#ifndef R_HYBRID_PLC_RF

/* ED List Type */
typedef RmEnergyLevelT RmMlmeEnergyDetectListT[RM_aMaxChannel + 1U];
#endif /* R_HYBRID_PLC_RF */

#if R_PHY_TYPE_CWX_M

/* The Type definition of PHY Operating Mode */
typedef struct
{
    uint8_t  modulation;
    uint8_t  regulatoryDomain;
    uint8_t  phyType;
    uint8_t  phyMode;
    uint8_t  channelPlan;
    uint32_t chanSpacing;
    uint32_t chanCenterFreq0;
} RmMlmePHYOperatingModeT;

/* The Type definition of PHY New Operating Mode Parameters */
typedef struct
{
    uint8_t                bank;
    r_phy_new_mode_param_t new_mode;
} RmMlmePHYNewModeParamT;

/* The Type definition of PHY New Mode Channel */
typedef struct
{
    uint8_t bank;
    uint8_t new_channel;
} RmMlmePHYNewModeChannelT;
#endif /* R_PHY_TYPE_CWX_M */

#ifndef R_HYBRID_PLC_RF

/* The Type definition of the Wi-SUN PHY configuration */
typedef struct
{
    uint8_t regulatory_domain; //!< The Regulatory Domain MUST be set to one of the Regulatory Domain values defined in [PHYSPEC]
    uint8_t operating_class;   //!< The Operating Class MUST be set to one of the Operating Class values defined in [PHYSPEC]
    uint8_t operating_mode;    //!< The Operating Mode MUST be set to one of the PHY Operating Mode values defined in [PHYSPEC]
} RmMlmeWiSunPhyConfigT;
#endif

/* MAC PIB Attribute value define (for MLME-SET/GET) */
typedef union
{
    /* --- PHY Attribute --- */
    RmOctetT                 phyCurrentChannel;
    RmBitmap32T              phyChannelsSupported[2];
    RmOctetT                 phyTransmitPower;
    RmOctetT                 phyCCABandwidth;
    RmShortT                 phyCCADuration;
    RmShortT                 phyFSKPreambleLength;
    RmOctetT                 phyMRFSKSFD;
    RmOctetT                 phyFSKOpeMode;
    RmOctetT                 phyFSKScramblePsdu;
    RmOctetT                 phyFCSLength;
    RmOctetT                 phyFskFecRxEna;
    RmOctetT                 phyFskFecTxEna;
    RmOctetT                 phyFskFecScheme;
    RmShortT                 phyAckReplyTime;
    RmOctetT                 phyAntennaSwitchEna;
    RmOctetT                 phyAntennaDiversityRxEna;
    RmOctetT                 phyAntennaSelectTx;
    RmShortT                 phyLvlfltrVth;
    RmOctetT                 phyRssiOutputOffset;
    RmOctetT                 phyChannelsSupportedPage;
    RmOctetT                 phyFreqBandId;
    RmOctetT                 phyRegulatoryMode;         // uint8_t  REL/ADI
    RmOctetT                 phyAgcWaitGainOffset;
    RmShortT                 phyCcaVth;
    RmOctetT                 phyCcaVthOffset;
    RmShortT                 phyAntennaSwitchEnaTiming;
    RmOctetT                 phyGpio0Setting;
    RmOctetT                 phyGpio3Setting;
    RmShortT                 phyDataRate;
    RmOctetT                 phyChannelConversion;
    RmOctetT                 phyTxDelayMs;
    RmShortT                 phyRmodeTonMax;
    RmShortT                 phyRmodeToffMin;
    RmShortT                 phyRmodeTcumSmpPeriod;
    RmLongT                  phyRmodeTcumLimit;
    RmLongT                  phyRmodeTcum;
    RmOctetT                 phyCcaWithAck;
    RmUINT32                 phyFrequency;
    RmINT32                  phyFrequencyOffset;
#ifndef R_HYBRID_PLC_RF
    RmMlmeWiSunPhyConfigT    phyWiSunPhyConfig;
#endif

#if R_PHY_TYPE_CWX_M
    RmMlmePHYOperatingModeT  phyOperatingMode;
    RmOctetT                 phyRxModulation;
    RmOctetT                 phyCcaModulation;
    RmOctetT                 phyTxModulation;
    RmShortT                 phyFskCcaVth;
    RmShortT                 phyOfdmCcaVth;
    RmShortT                 phyFskCCADuration;
    RmShortT                 phyOfdmCCADuration;
    RmMlmePHYNewModeParamT   phyNewModeParam;
    RmOctetT                 phyNewModeBank;
    RmMlmePHYNewModeChannelT phyNewModeChannel;
    RmShortT                 phyModeSwitchPhrParam;

    RmOctetT                 phyMDRRegulatoryDomain;
    RmOctetT                 phyMDRNewModeSwitchBank;

#endif /* if R_PHY_TYPE_CWX_M */

    /* --- AddressFilter --- */
    RmOctetT                   macPromiscuousMode;
    RmPANIdentifierT           macPANId;
    RmShortAddressT            macShortAddress;
    RmExtendedAddressT         macExtendedAddress;
    RmBooleanT                 macBorderRouter;
    RmBooleanT                 macFramePend;

    /* --- MAC Security --- */
#ifndef R_HYBRID_PLC_RF
    RmSecSetKeyT               macSecSetKey;
    RmSecDeleteKeyT            macSecDeleteKey;
#endif
    uint32_t                   macFrameCountIndInterval;

    /* --- MAC Neighbor Cache --- */
    RmNeighborLockT            macNeighborLock;
    RmOctetT                   macRxFrameCounterFlush;
    RmNeighborEntryT           macNeighborEntry;

    /* --- Metric --- */
    RmBooleanT                 macMetricEnabled;
    RmCounterT                 macCounterOctets;
    RmCounterT                 macRetryCount;
    RmCounterT                 macMultipleRetryCount;
    RmCounterT                 macTXFailCount;
    RmCounterT                 macTXSuccessCount;
    RmCounterT                 macFCSErrorCount;
    RmCounterT                 macSecurityFailureCount;
    RmCounterT                 macDuplicateFrameCount;
    RmCounterT                 macRXSuccessCount;

    /* --- PowerMode --- */
    RmOctetT                   macLowPowerMode;

    /* --- TX --- */
    RmOctetT                   macMaxCSMABackoffs;
    RmOctetT                   macMinBE;
    RmOctetT                   macMaxBE;
    RmShortTimeT               macMaxFrameTotalWaitTime;
    RmOctetT                   macMaxFrameRetries;
    RmOctetT                   macDSN;

    /* --- RX --- */
    RmBooleanT                 macRxOnWhenIdle;
#ifndef R_HYBRID_PLC_RF

    /* Schedules */
    r_ie_wp_serialized_sched_t unicastSchedule;
    r_ie_wp_serialized_sched_t broadcastSchedule;
#endif

    /* --- Frame Subscription --- */
    RmBooleanT                 macFrameSubscriptionEnabled;

    RmOctetT                   macDuplicateDetectionTTL;

#ifdef R_HYBRID_PLC_RF
    RmOctetT                   macKeyTableEntry[16];
    RmBooleanT                 macSecurityEnabled;
    RmBooleanT                 macTimeStampSupported;
    r_mac_device_entry_t       macDeviceTableEntry;
    uint32_t                   macFrameCounter;

    /* --- EB/EBR --- */
    RmBitmap8T                 macEBRFilters;
    RmBooleanT                 macBeaconAutoRespond;
    RmBooleanT                 macUseEnhancedBeacon;
    RmOctetT                   macEBSN;
    uint8_t macBeaconPayload[RM_aMaxBeaconPayloadLength];
    RmBooleanT                 macAutoRequest;

    r_mac_pos_entry_t          macPosTableEntry;

    /* Duty Cycle Handling */
    RmOctetT                   macDutyCycleUsage;
    RmShortT                   macDutyCyclePeriod;
    RmShortT                   macDutyCycleLimit;
    RmOctetT                   macDutyCycleThreshold;

    RmBooleanT                 macDisablePHY;
    RmOctetT                   macPOSTableEntryTTL;
    RmOctetT                   macBeaconRandomizationWindowLength;
    RmOctetT                   macSoftVersion[3 + RM_PHY_VERSION_LENGTH];
    RmBooleanT                 macKeyState;
    RmOctetT                   macFrequencyBand;
    RmBooleanT                 macHoppingEnabled;
#endif /* R_HYBRID_PLC_RF */

} RmMlmeMacPIBAttributeValueT;

/* The message definition (MLME-SAP dependent type)                     */
/* The Variable Types definition (MLME-SAP dependent type)              */

#ifdef R_HYBRID_PLC_RF
/*
 * Message MLME-BEACON-NOTIFY.indication format
 */
typedef struct
{
    r_os_msg_header_t    mm_OsMsgHdr;
    RmMlmePANDescriptorT mm_PANDescriptor;
    RmOctetT             mm_EBSN;
    RmOctetT             mm_payloadLength;
    RmBeaconPayloadT     mm_BeaconPayload;
#if 0  // Currently not used, but kept for future extension
    RmShortT             mm_IEListLength;
    uint8_t*             mm_IEList;
#endif
} RmMlmeBeaconNotifyIndicationT;

#define RmMlmeInitializeBeaconNotifyIndication(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_BEACON_NOTIFY_indication, \
                    RM_MLME_BEACON_NOTIFY_indication_length)
#endif /* R_HYBRID_PLC_RF */

/*
 * Message MLME-GET.request format
 */
typedef struct
{
    RmMsgHdrT mm_Hdr;
    RmShortT  mm_PIBAttribute;

#ifndef R_HYBRID_PLC_RF
    RmOctetT  mm_PIBAttributeIndex[RM_MLME_GET_PIB_Index_length];
#else
    RmShortT  mm_PIBAttributeIndex;
#endif
} RmMlmeGetRequestT;

#define RmMlmeInitializeGetRequest(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_GET_request, RM_MLME_GET_request_length)

/*
 * Message MLME-GET.confirm format
 */
typedef struct
{
    RmMsgHdrT  mm_Hdr;
    RmMacEnumT mm_status;
    RmShortT   mm_PIBAttribute;
#ifndef R_HYBRID_PLC_RF
    RmOctetT   mm_PIBAttributeIndex[RM_MLME_GET_PIB_Index_length];
#else
    RmShortT   mm_PIBAttributeIndex;
#endif
    RmMlmeMacPIBAttributeValueT mm_PIBAttributeValue;
} RmMlmeGetConfirmT;

#define RmMlmeInitializeGetConfirm(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_GET_confirm, RM_MLME_GET_confirm_length)
#define RM_GET_SLEN_BASE (sizeof(RmMlmeGetConfirmT) - sizeof(RmMsgHdrT) - sizeof(RmMlmeMacPIBAttributeValueT))

/*
 * Message MLME-RESET.request format
 */
typedef struct
{
    RmMsgHdrT  mm_Hdr;
    RmBooleanT mm_setDefaultPIB;
} RmMlmeResetRequestT;


#define RmMlmeInitializeResetRequest(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_RESET_request, RM_MLME_RESET_request_length)
/*
 * Message MLME-RESET.confirm format
 */
typedef struct
{
    RmMsgHdrT  mm_Hdr;
    RmMacEnumT mm_status;
} RmMlmeResetConfirmT;

#define RmMlmeInitializeResetConfirm(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_RESET_confirm, RM_MLME_RESET_confirm_length)

#ifdef R_HYBRID_PLC_RF
/*
 * Message MLME-SCAN.request format
 */
typedef struct
{
    RmMsgHdrT       mm_Hdr;
    RmScanDurationT mm_scanDuration;
#if 0  // Currently not used in G3 RF MAC, consider removal after integration of frequency hopping
    RmBitmap32T     mm_scanChannels_l;
    RmBitmap32T     mm_scanChannels_h;
    RmBooleanT      mm_LinkQualityScan;
    RmOctetT        mm_frameControlOptions;
    RmOctetT        mm_headerIEListLength;
    uint8_t*        mm_headerIEList;
    RmShortT        mm_payloadIEListLength;
    uint8_t*        mm_payloadIEList;
    RmIEsT          mm_ScanIEList;
#endif
} RmMlmeScanRequestT;

#define RmMlmeInitializeScanRequest(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_SCAN_request, RM_MLME_SCAN_request_length)

/*
 * Message MLME-SCAN.confirm format
 */
typedef struct
{
    RmMsgHdrT       mm_Hdr;
    RmMacEnumT      mm_status;
#if 0  // Currently not used in G3 RF MAC, consider removal after integration of frequency hopping
    RmBitmap32T     mm_unscanedChannels_l;
    RmBitmap32T     mm_unscanedChannels_h;
    RmOctetT        mm_resultListSize;
    RmMlmeScanDataT mm_scanData;  /* ED List or PAN Desc List */
#endif
} RmMlmeScanConfirmT;

#define RmMlmeInitializeScanConfirm(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_SCAN_confirm, RM_MLME_SCAN_confirm_length)

/*
 * Message MLME-FRAMECOUNT.indication format
 */
typedef struct
{
    r_os_msg_header_t mm_OsMsgHdr;
    uint32_t          framecounter;
} RmMlmeFrameCountIndicationT;

#define RmMlmeInitializeFrameCountIndication(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_FRAMECOUNT_indication, \
                    RM_MLME_FRAMECOUNT_indication_length)
#endif /* R_HYBRID_PLC_RF */

/*
 * Message MLME-COMM-STATUS.indication format
 */
typedef struct
{
    r_os_msg_header_t mm_OsMsgHdr;
    RmPANIdentifierT  mm_PANId;
    RmOctetT          mm_srcAddrMode;
    RmDeviceAddressT  mm_srcAddr;
    RmOctetT          mm_dstAddrMode;
    RmDeviceAddressT  mm_dstAddr;
    RmMacEnumT        mm_status;
    RmSecParamT       mm_secParam;
} RmMlmeCommStatusIndicationT;


#define RmMlmeInitializeCommStatusIndication(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_COMM_STATUS_indication, \
                    RM_MLME_COMM_STATUS_indication_length)

/*
 * Message MLME-SET.request format
 */
typedef struct
{
    RmMsgHdrT mm_Hdr;
    RmShortT  mm_PIBAttribute;
    RmShortT  mm_PIBAttributeIndex;
    RmMlmeMacPIBAttributeValueT mm_PIBAttributeValue;
} RmMlmeSetRequestT;

#define RmMlmeInitializeSetRequest(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_SET_request, RM_MLME_SET_request_length)

/*
 * Message MLME-SET.confirm format
 */
typedef struct
{
    RmMsgHdrT  mm_Hdr;
    RmMacEnumT mm_status;
    RmShortT   mm_PIBAttribute;
    RmShortT   mm_PIBAttributeIndex;
} RmMlmeSetConfirmT;

#define RmMlmeInitializeSetConfirm(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_SET_confirm, RM_MLME_SET_confirm_length)

#ifdef R_HYBRID_PLC_RF
/*
 * Message MLME-START.request format
 */
typedef struct
{
    RmMsgHdrT        mm_Hdr;
    RmPANIdentifierT mm_PANId;
} RmMlmeStartRequestT;


#define RmMlmeInitializeStargRequest(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_START_request, RM_MLME_START_request_length)

/*
 * Message MLME-START.confirm format
 */
typedef struct
{
    RmMsgHdrT  mm_Hdr;
    RmMacEnumT mm_status;
} RmMlmeStartConfirmT;

#define RmMlmeInitializeStartConfirm(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MLME_START_confirm, RM_MLME_START_confirm_length)
#endif /* R_HYBRID_PLC_RF */

/*
 * FatalError/Warning indication format
 */
typedef struct
{
    r_os_msg_header_t mm_OsMsgHdr;
    RmOctetT          mm_status;
} RmFatalErrorIndicationT;

typedef struct
{
    r_os_msg_header_t mm_OsMsgHdr;
    RmOctetT          mm_status;
} RmWarningIndicationT;

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

/******************************************************************************
 *  Copyright (C) 2014-2016 Renesas Electronics Corporation.
 *****************************************************************************/
#endif // #ifndef __RM_mac_mlme_H
