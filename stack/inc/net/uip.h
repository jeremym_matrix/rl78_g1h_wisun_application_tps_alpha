/*
 * Copyright (c) 2001-2003, Adam Dunkels.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. The name of the author may not be used to endorse or promote
 *    products derived from this software without specific prior
 *    written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE AUTHOR ``AS IS'' AND ANY EXPRESS
 * OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 * WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE AUTHOR BE LIABLE FOR ANY
 * DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE
 * GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 * WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * This file is part of the uIP TCP/IP stack.
 *
 *
 */

/**
 * \addtogroup uip
 * @{
 */

/**
 * \file
 * Header file for the uIP TCP/IP stack.
 * \author  Adam Dunkels <adam@dunkels.com>
 * \author  Julien Abeille <jabeille@cisco.com> (IPv6 related code)
 * \author  Mathilde Durvy <mdurvy@cisco.com> (IPv6 related code)
 *
 * The uIP TCP/IP stack header file contains definitions for a number
 * of C macros that are used by uIP programs as well as internal uIP
 * structures, TCP/IP header structures and function declarations.
 *
 */

#ifndef UIP_H_
#define UIP_H_

#include "net/uipopt.h"
#include "r_nwk_api_base.h"

/* Header sizes. */
#define UIP_IPH_LEN            40u

#define UIP_ICMPH_LEN          4 /* Size of ICMP header */

/**
 * The sums below are quite used in ND. When used for uip_buf, we
 * include link layer length when used for uip_len, we do not, hence
 * we need values with and without LLH_LEN we do not use capital
 * letters as these values are variable
 */
#define uip_l2_l3_hdr_len      (UIP_LLH_LEN + UIP_IPH_LEN + uip_ext_len)
#define uip_l2_l3_icmp_hdr_len (UIP_LLH_LEN + UIP_IPH_LEN + uip_ext_len + UIP_ICMPH_LEN)
#define uip_l3_icmp_hdr_len    (UIP_IPH_LEN + uip_ext_len + UIP_ICMPH_LEN)


/* For memcmp */
#include <string.h>

/*---------------------------------------------------------------------------*/

/** \brief 64 bit 802.15.4 address */
typedef r_eui64_t uip_lladdr_t;


/** \brief 802.15.4 address */
#define UIP_LLADDR_LEN 8

/*---------------------------------------------------------------------------*/

/**
 * \defgroup uipdevfunc uIP device driver functions
 * @{
 *
 * These functions are used by a network device driver for interacting
 * with uIP.
 */

extern uint8_t* uip_buf;

/** @} */


/* uIP convenience and converting functions. */

/**
 * \defgroup uipconvfunc uIP conversion functions
 * @{
 *
 * These functions can be used for converting between different data
 * formats used by uIP.
 */

/**
 * Construct an IPv6 address from eight 16-bit words.
 *
 * This function constructs an IPv6 address.
 *
 * \hideinitializer
 */
#define uip_ip6addr(addr, addr0, addr1, addr2, addr3, addr4, addr5, addr6, addr7, addr8, addr9, addr10, addr11, addr12, addr13, addr14, addr15) do { \
        (addr)->bytes[0] = (addr0);                                      \
        (addr)->bytes[1] = (addr1);                                      \
        (addr)->bytes[2] = (addr2);                                      \
        (addr)->bytes[3] = (addr3);                                      \
        (addr)->bytes[4] = (addr4);                                      \
        (addr)->bytes[5] = (addr5);                                      \
        (addr)->bytes[6] = (addr6);                                      \
        (addr)->bytes[7] = (addr7);                                      \
        (addr)->bytes[8] = (addr8);                                      \
        (addr)->bytes[9] = (addr9);                                      \
        (addr)->bytes[10] = (addr10);                                    \
        (addr)->bytes[11] = (addr11);                                    \
        (addr)->bytes[12] = (addr12);                                    \
        (addr)->bytes[13] = (addr13);                                    \
        (addr)->bytes[14] = (addr14);                                    \
        (addr)->bytes[15] = (addr15);                                    \
} while (0)


/**
 * Copy an IP address from one place to another.
 *
 * Copies an IP address from one place to another.
 *
 * Example:
 \code
 r_ipv6addr_t ipaddr1, ipaddr2;

 uip_ipaddr(&ipaddr1, 192,16,1,2);
 uip_ipaddr_copy(&ipaddr2, &ipaddr1);
 \endcode
 *
 * \param dest The destination for the copy.
 * \param src The source from where to copy.
 *
 * \hideinitializer
 */
#ifndef uip_ipaddr_copy
#define uip_ipaddr_copy(dest, src)  (*(dest) = *(src))
#endif
#ifndef uip_ip6addr_copy
#define uip_ip6addr_copy(dest, src) (*((r_ipv6addr_t*)dest) = *((r_ipv6addr_t*)src))
#endif

/**
 * Compare two IP addresses
 *
 * Compares two IP addresses.
 *
 * Example:
 \code
 r_ipv6addr_t ipaddr1, ipaddr2;

 uip_ipaddr(&ipaddr1, 192,16,1,2);
 if(uip_ipaddr_cmp(&ipaddr2, &ipaddr1)) {
  printf("They are the same");
 }
 \endcode
 *
 * \param addr1 The first IP address.
 * \param addr2 The second IP address.
 *
 * \hideinitializer
 */
#define uip_ip6addr_cmp(addr1, addr2)              (memcmp(addr1, addr2, sizeof(r_ipv6addr_t)) == 0)

#define uip_ipaddr_cmp(addr1, addr2)               uip_ip6addr_cmp(addr1, addr2)

#define uip_ipaddr_prefixcmp(addr1, addr2, length) (memcmp(addr1, addr2, length >> 3) == 0)


/**
 * Convert 16-bit quantity from host byte order to network byte order.
 *
 * This macro is primarily used for converting constants from host
 * byte order to network byte order. For converting variables to
 * network byte order, use the uip_htons() function instead.
 *
 * \hideinitializer
 */
#ifndef UIP_HTONS
#if UIP_BYTE_ORDER == UIP_BIG_ENDIAN
#define UIP_HTONS(n) (n)
#define UIP_HTONL(n) (n)
#else /* UIP_BYTE_ORDER == UIP_BIG_ENDIAN */
#define UIP_HTONS(n) (uint16_t)((((uint32_t)(n)) << 8) | (((uint16_t)(n)) >> 8))
#define UIP_HTONL(n) (((uint32_t)UIP_HTONS(n) << 16) | UIP_HTONS((uint32_t)(n) >> 16))
#endif /* UIP_BYTE_ORDER == UIP_BIG_ENDIAN */
#else
#error "UIP_HTONS already defined!"
#endif /* UIP_HTONS */

/**
 * Convert a 16-bit quantity from host byte order to network byte order.
 *
 * This function is primarily used for converting variables from host
 * byte order to network byte order. For converting constants to
 * network byte order, use the UIP_HTONS() macro instead.
 */
#ifndef uip_htons
uint16_t uip_htons(uint16_t val);
#endif /* uip_htons */
#ifndef uip_ntohs
#define uip_ntohs uip_htons
#endif

#ifndef uip_htonl
uint32_t uip_htonl(uint32_t val);
#endif /* uip_htonl */
#ifndef uip_ntohl
#define uip_ntohl uip_htonl
#endif

/** @} */


/**
 * \defgroup uipdrivervars Variables used in uIP device drivers
 * @{
 *
 * uIP has a few global variables that are used in device drivers for
 * uIP.
 */

/**
 * The length of the packet in the uip_buf buffer.
 *
 * The global variable uip_len holds the length of the packet in the
 * uip_buf buffer.
 *
 * When the network device driver calls the uIP input function,
 * uip_len should be set to the length of the packet in the uip_buf
 * buffer.
 *
 * When sending packets, the device driver should use the contents of
 * the uip_len variable to determine the length of the outgoing
 * packet.
 *
 */
extern uint16_t uip_len;

/**
 * The length of the extension headers
 */
extern uint8_t uip_ext_len;

/** @} */

/*
 * Clear uIP buffer
 *
 * This function clears the uIP buffer by reseting the uip_len and
 * uip_ext_len pointers.
 */
#define uip_clear_buf() do { \
        uip_len = 0; \
        uip_ext_len = 0; \
} while (0)

/*---------------------------------------------------------------------------*/
/* All the stuff below this point is internal to uIP and should not be
 * used directly by an application or by a device driver.
 */

/*---------------------------------------------------------------------------*/


/*
 * In IPv6 the length of the L3 headers before the transport header is
 * not fixed, due to the possibility to include extension option headers
 * after the IP header. hence we split here L3 and L4 headers
 */

/* The IP header */
struct uip_ip_hdr
{
    /* IPV6 header */
    uint8_t      vtc;
    uint8_t      tcflow;
    uint16_t     flow;
    uint8_t      len[2];
    uint8_t      proto, ttl;
    r_ipv6addr_t srcipaddr, destipaddr;
};


/*
 * IPv6 extension option headers: we are able to process
 * the 4 extension headers defined in RFC2460 (IPv6):
 * - Hop by hop option header, destination option header:
 *   These two are not used by any core IPv6 protocol, hence
 *   we just read them and go to the next. They convey options,
 *   the options defined in RFC2460 are Pad1 and PadN, which do
 *   some padding, and that we do not need to read (the length
 *   field in the header is enough)
 * - Routing header: this one is most notably used by MIPv6,
 *   which we do not implement, hence we just read it and go
 *   to the next
 * - Fragmentation header: we read this header and are able to
 *   reassemble packets
 *
 * We do not offer any means to send packets with extension headers
 *
 * We do not implement Authentication and ESP headers, which are
 * used in IPSec and defined in RFC4302,4303,4305,4385
 */

/* common header part */
typedef struct uip_ext_hdr
{
    uint8_t next;
    uint8_t len;
} uip_ext_hdr;

/* Hop by Hop option header */
typedef struct uip_hbho_hdr
{
    uint8_t next;
    uint8_t len;
} uip_hbho_hdr;


/* We do not define structures for PAD1 and PADN options */

#define UIP_HDR_ROUTING_TYPE_SOURCE_ROUTING 3

/*
 * an option within the destination or hop by hop option headers
 * it contains type an length, which is true for all options but PAD1
 */
typedef struct uip_ext_hdr_opt
{
    uint8_t type;
    uint8_t len;
} uip_ext_hdr_opt;

/* RPL option */
typedef struct uip_ext_hdr_opt_rpl
{
    uint8_t  opt_type;
    uint8_t  opt_len;
    uint8_t  flags;
    uint8_t  instance;
    uint16_t senderrank;
} uip_ext_hdr_opt_rpl;

/* The ICMP headers. */
struct uip_icmp_hdr
{
    uint8_t  type, icode;
    uint16_t icmpchksum;
};



#define UIP_PROTO_ICMP6 58


/** @{ */
/** \brief  extension headers types */
#define UIP_PROTO_HBHO    0
#define UIP_PROTO_DESTO   60
#define UIP_PROTO_ROUTING 43
#define UIP_PROTO_FRAG    44
#define UIP_PROTO_NONE    59
#define UIP_PROTO_IPV6    41

/** @} */

/** @{ */
/** \brief  Destination and Hop By Hop extension headers option types */
#define UIP_EXT_HDR_OPT_PAD1 0
#define UIP_EXT_HDR_OPT_PADN 1
#define UIP_EXT_HDR_OPT_RPL  0x63

/** @} */


extern uip_lladdr_t uip_lladdr;


/**
 * \brief Checks whether the address a is link local.
 * a is of type r_ipv6addr_t
 */
#define uip_is_addr_linklocal(a)                 \
    ((a)->bytes[0] == 0xfe &&                         \
     (a)->bytes[1] == 0x80)


#define uip_create_linklocal_prefix(addr) do { \
        (addr)->bytes[0] = 0xfe;                  \
        (addr)->bytes[1] = 0x80;                  \
        (addr)->bytes[2] = 0;                     \
        (addr)->bytes[3] = 0;                     \
        (addr)->bytes[4] = 0;                     \
        (addr)->bytes[5] = 0;                     \
        (addr)->bytes[6] = 0;                     \
        (addr)->bytes[7] = 0;                     \
} while (0)


/**
 * \brief put in b the solicited node address corresponding to address a
 * both a and b are of type r_ipv6addr_t*
 * */
#define uip_create_solicited_node(a, b)    \
    (((b)->bytes[0]) = 0xFF);                        \
    (((b)->bytes[1]) = 0x02);                        \
    (((b)->bytes[2]) = 0);                           \
    (((b)->bytes[3]) = 0);                           \
    (((b)->bytes[4]) = 0);                           \
    (((b)->bytes[5]) = 0);                           \
    (((b)->bytes[6]) = 0);                           \
    (((b)->bytes[7]) = 0);                           \
    (((b)->bytes[8]) = 0);                           \
    (((b)->bytes[9]) = 0);                           \
    (((b)->bytes[10]) = 0);                          \
    (((b)->bytes[11]) = 0x01);                       \
    (((b)->bytes[12]) = 0xFF);                       \
    (((b)->bytes[13]) = ((a)->bytes[13]));              \
    (((b)->bytes[14]) = ((a)->bytes[14]));              \
    (((b)->bytes[15]) = ((a)->bytes[15]));

/**
 * \brief is address a multicast address, see RFC 3513
 * a is of type r_ipv6addr_t*
 * */
#define uip_is_addr_mcast(a)                    \
    (((a)->bytes[0]) == 0xFF)


/**
 * \brief is address a routable multicast address.
 * Scope 3 (Realm-Local) or higher are routable
 * Realm-Local scope is defined in draft-ietf-6man-multicast-scopes
 * See RFC4291 and draft-ietf-6man-multicast-scopes
 * a is of type r_ipv6addr_t*
 * */
#define uip_is_addr_mcast_routable(a) \
    ((((a)->bytes[0]) == 0xFF) && \
     (((a)->bytes[1] & 0x0F) > 0x02))


const uip_lladdr_t* packetbuf_addr_sender();
void                R_CTK_PacketbufSetEui64(const uint8_t* addr);


#endif /* UIP_H_ */


/** @} */
