/*
 * Copyright (c) 2016, Locoslab GmbH (www.locoslab.com)
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of the copyright holder nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * ``AS IS'' AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
 * FOR A PARTICULAR PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE
 * COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
 * INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES
 * (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT,
 * STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED
 * OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 * Authors: Pedro Jose Marron <pjmarron@locoslab.com>
 */
/**
 * \addtogroup uip6
 * @{
 */
/**
 * \file
 *    Header file for source routing table manipulation
 */
#ifndef UIP_DS6_SOURCE_ROUTE_H
#define UIP_DS6_SOURCE_ROUTE_H

#include "net/uip.h"
#include "rpl-conf.h"

#include "lib/list.h"

struct rpl_dag;

void uip_ds6_source_route_init(void* vp_nwkGlobal);

/** \brief An entry in the routing table */
typedef struct uip_ds6_source_route
{
    struct uip_ds6_source_route* next;
    /* Each source_route entry belongs to a specific node in the network. That node
     holds a list with the sequence of nodes that need to be travelled to reach it.
    */

    r_ipv6addr_t ipaddr;

    /* transit contains up to RPL_CONF_NON_STORING_MAX_PARENTS stored for a given node. transit[0]
    * always contains the preferred parent or the parent with the highest path control
    * value.
    */
    struct uip_ds6_source_route* transit[RPL_CONF_NON_STORING_MAX_PARENTS];

    /* path_control contains the corresponding value of the path control field for
    * each stored transit parent.
    */
    uint8_t         path_control[RPL_CONF_NON_STORING_MAX_PARENTS];
    uint8_t         fail_counter[RPL_CONF_NON_STORING_MAX_PARENTS];
    uint8_t         nr_transits;
    uint8_t         prefix_len;
    struct rpl_dag* dag;
    uint32_t        lifetime; ///< Route is valid until lifetime (in seconds), 0 -> route is stale, -1 infinite lifetime
} uip_ds6_source_route_t;

/** \name Routing Table basic routines */
/** @{ */

/**
 * Add a source route based on a DAO transit option
 * @param dag the DAG of the route
 * @param prefix the route prefix
 * @param prefix_len the length of the route prefix
 * @param lifetime the lifetime of the route from the transit option
 * @param transit_addr the parents of the node
 * @param path_control the path control elements for each parent
 * @param nr_transits number of parents/path control elements
 * @return the added route or NULL if adding the route failed
 */
uip_ds6_source_route_t* uip_ds6_source_route_add(struct rpl_dag* dag,
                                                 r_ipv6addr_t*   prefix,
                                                 uint8_t         prefix_len,
                                                 uint8_t         lifetime,
                                                 r_ipv6addr_t*   transit_addr[],
                                                 uint8_t         path_control[],
                                                 uint8_t         nr_transits);

/** Remove the node specified by destination */
void uip_ds6_source_route_rm_by_destination(r_ipv6addr_t* destination);

/** Remove the specified route */
void uip_ds6_source_route_rm(uip_ds6_source_route_t* route);

/** Expire and remove outdated routes
 * @return 0 if no route has been removed
 */
int uip_ds6_source_route_expire_routes(void);


/** @return the (possibly incomplete) route for destipaddr or NULL if not found */
uip_ds6_source_route_t* uip_ds6_source_route_lookup(const r_ipv6addr_t* destipaddr);

/** @return the next segment of a router or NULL if not found */
uip_ds6_source_route_t* uip_ds6_source_route_next_segment(uip_ds6_source_route_t* current);

/** @return the first route in the table or NULL if not present */
uip_ds6_source_route_t* uip_ds6_source_route_head(void);

/** @return the next route in the table or NULL if not present */
uip_ds6_source_route_t* uip_ds6_source_route_next(uip_ds6_source_route_t* route);

/**
 * Update the fail counter for a specific destination address (the offending node)
 * If the route is not found or there is no transit from the offending node, fail
 * silently.
 */
void uip_ds6_source_route_update_fail_counter(const r_ipv6addr_t* final_destination, const r_ipv6addr_t* destination);

#ifdef UNUSED_FUNCTION

/** Update the fail counter for all routes that go through a particular transit. Fail silently if not found */
void uip_ds6_source_route_update_all_fail_counters(r_ipv6addr_t* transit);

#endif // UNUSED_FUNCTION

/**
 * Reset the iterator to the first route in the table and return it
 * @return The first route in the table or NULL if not present
 */
uip_ds6_source_route_t* uip_ds6_source_route_iter_start();

/**
 * Retrieve the route that the iterator is currently pointing to without moving it to the next route
 * @return The route that the iterator is currently pointing to
 */
uip_ds6_source_route_t* uip_ds6_source_route_iter_current();

/**
 * Move the iterator to the next route within the routing table and return it
 * @return The next route in the table or NULL if not present
 */
uip_ds6_source_route_t* uip_ds6_source_route_iter_next();

/** @return the route and length for destination or NULL if not found or route is incomplete */
uip_ds6_source_route_t* uip_ds6_source_route_compute(r_ipv6addr_t* destination, uint8_t* route_length);

/** @return the length of the route or -1 if the route is incomplete or has a loop */
int16_t uip_ds6_source_route_route_length(uip_ds6_source_route_t* dest);

/** @} */

#endif /* UIP_DS6_SOURCE_ROUTE_H */
/** @} */
