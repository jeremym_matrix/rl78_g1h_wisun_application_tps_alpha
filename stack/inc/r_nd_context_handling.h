/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2020 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/**
 * @file   r_nd_context_handling.h
 * @brief  Header for the module that implements the neighbor discovery context handling
 */

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_nwk_inter_config.h"
#include "r_nwk_api_base.h"
#include "r_stdint.h"

/******************************************************************************
   Macro definitions
******************************************************************************/
#ifndef R_ND_CONTEXT_HANDLING_H
#define R_ND_CONTEXT_HANDLING_H

#if R_HC_IPHC_STATEFUL_COMPRESSION_ENABLED

/******************************************************************************
   Typedef definitions
******************************************************************************/
#define R_ND_DEFAULT_CONTEXT_LENGTH (64u)  //!< Default context length in bit
#define R_ND_MIN_6LP_CID_COUNT      (4u)   //!< The minimum number of 6LowPAN header compression context identifiers

/*!
    \struct r_nd_context_table_t
    \brief The 6LoWPAN context table
 */
typedef struct
{
    uint8_t     prefixValue[R_ND_DEFAULT_CONTEXT_LENGTH / 8]; //!< Contains the actual context value
    uint32_t    expireTime;                                   //!< Contains the expire time stamp
    r_boolean_t validFlag;                                    //!< Identifies valid entry
    r_boolean_t compressionFlag;                              //!< This flag indicates if the context is valid for use in compression

} r_nd_context_table_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn void  R_ND_InitContextHandling(void)
    \brief Initializes the context handling module
    \return None
 */
void R_ND_InitContextHandling(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/*!
    \fn void  R_ND_ResetContextHandling(void)
    \brief Initializes the context handling module
    \return None
 */
void R_ND_ResetContextHandling(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/*!
    \fn r_nd_context_table_t R_ND_GetContextTable(void)
    \brief Get a pointer to the context table
    \return Pointer to the context table
 */
r_nd_context_table_t* R_ND_GetContextTable(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/*!
    \fn uint8_t R_ND_GetContextIdFromPrefix(const uint8_t* prefixValue, uint8_t* contextLength)
    \brief Gets context Id from prefix
    \param[in] prefixValue Pointer to the actual prefix
    \param[in] contextLength The length of the prefix in bits
    \return Context Id of the actual prefix
 */
uint8_t R_ND_GetContextIdFromPrefix(const uint8_t* prefixValue,
                                    uint8_t*       contextLength);

/*!
    \fn uint8_t* R_ND_GetPrefixFromContextId(const uint8_t contextId,
                                      uint8_t* contextLength);
    \brief Gets prefix from a provided context Id
    \param[in] contextId Context Id of the actual prefix
    \param[out] contextLength Length of the actual prefix in bits
    \return Pointer to the actual prefix
 */
uint8_t* R_ND_GetPrefixFromContextId(const uint8_t contextId,
                                     uint8_t*      contextLength);

#endif  /* R_HC_IPHC_STATEFUL_COMPRESSION_ENABLED */

#endif /* R_ND_CONTEXT_HANDLING_H */
