/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2018 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/**
 * @file r_mac_ie.h
 * @version 1.00
 * @brief Header file for the information elements
 */

/*
 * Prevent nested inclusions
 */
#ifndef R_MAC_IE_H
#define R_MAC_IE_H

#include "r_header_utils.h"
#ifdef R_HYBRID_PLC_RF
#include "r_typedefs.h"
#else /* R_HYBRID_PLC_RF */
#include "r_nwk_api_base.h"
#endif /* R_HYBRID_PLC_RF */
#include "r_header_utils.h"

/******************************************************************************
* Defines
******************************************************************************/
#ifndef R_IE_WP_MP_FRAGMENT_MAX_SIZE
    #define R_IE_WP_MP_FRAGMENT_MAX_SIZE 1576
#endif

/* Constants regarding the serialized size of schedules. The serialized sizes of schedules vary depending on the used
 * options (e.g., RegDomain vs explicit) and variable parts (e.g., excluded channel ranges). These constants express
 * the maximum size _without_ taking the variable parts into accounts and can be used for buffer reservation. */

/* 4: DwellInterval-ExcludedChannelControl; 6:CH0-NumberOfChannels; 2: FixedChannel; 1: ChannelHopCount */
#define R_IE_WP_UNICAST_SCHEDULE_BASE_BYTES          (4 + 6 + 2 + 1)
#define R_IE_WP_BROADCAST_SCHEDULE_BASE_BYTES        (6 + R_IE_WP_UNICAST_SCHEDULE_BASE_BYTES)
#define R_IE_WP_EXCLUDED_RANGE_BYTES                 4 // start channel and end channel (2 bytes each)

#ifndef R_SCHEDULE_IE_EXCLUDED_CHANNEL_MASK_MAX_SIZE
#define R_SCHEDULE_IE_EXCLUDED_CHANNEL_MASK_MAX_SIZE ((129 + 7) / 8)
#endif
#ifndef R_SCHEDULE_IE_EXCLUDED_CHANNEL_RANGES_MAX
#define R_SCHEDULE_IE_EXCLUDED_CHANNEL_RANGES_MAX    10
#endif

#define R_SERIALIZED_SCHEDULE_IE_BYTES_MAX \
    (R_IE_WP_BROADCAST_SCHEDULE_BASE_BYTES + \
     MAX( \
         R_SCHEDULE_IE_EXCLUDED_CHANNEL_RANGES_MAX * R_IE_WP_EXCLUDED_RANGE_BYTES, \
         R_SCHEDULE_IE_EXCLUDED_CHANNEL_MASK_MAX_SIZE \
         ) \
    )

/*
 * Wi-SUN Vendor Identifier Number
 */
#define R_IE_VENDOR_ID_RENESAS 6

/******************************************************************************
* Typedef definitions
******************************************************************************/

/*!
    \enum r_ie_wh_id_t
    \brief Wi-SUN Header Information Element ID
 */
typedef enum r_ie_wh_id_e
{
#ifdef R_HYBRID_PLC_RF
    R_IE_WH_ID_VENDOR          = 0x00, //!< Vendor IE ID
#endif /* R_HYBRID_PLC_RF */
    R_IE_WH_ID_RENDEZVOUS_TIME = 0x1d, //!< Rendezvous Time IE, according to IEEE-802.15.4-2015
    R_IE_WH_ID_WISUN           = 0x2a, //!< Wi-SUN Header IE ID
    R_IE_WH_ID_HT1             = 0x7e, //!< HT1 (payload IE follows)
    R_IE_WH_ID_HT2             = 0x7f, //!< HT2 (payload follows)
} r_ie_wh_id_t;

/*!
    \enum r_ie_wh_sub_id_t
    \brief Wi-SUN Header Information Elements Sub ID
 */
typedef enum r_ie_wh_sub_id_e
{
    R_IE_WH_SUB_ID_UTT                        = 0x01, //!< Unicast Timing and Frame Type Information Element (UTT-IE)
    R_IE_WH_SUB_ID_BROADCAST_TIMING           = 0x02, //!< Broadcast Timing Information Element (BT-IE)
    R_IE_WH_SUB_ID_FLOW_CONTROL               = 0x03, //!< Flow Control IE (FC-IE)
    R_IE_WH_SUB_ID_RSL                        = 0x04, //!< Received Signal Level Information Element (RSL-IE)
    R_IE_WH_SUB_ID_VENDOR                     = 0x06, //!< Vendor Header Information Element (VH-IE)
    R_IE_WH_SUB_ID_NFT                        = 0x07, //!< Netricity Frame Type (NFT-IE)
    R_IE_WH_SUB_ID_EAPOL_AUTHENTICATOR        = 0x09, //!< EAPOL Authenticator Header Information Element (EA-IE)
#if (R_WISUN_FAN_VERSION >= 110)
    R_IE_WH_SUB_ID_LFN_UTT                    = 0x0a, //!< LFN Unicast Timing and Frame Type Information Element (LUTT-IE)
    R_IE_WH_SUB_ID_LFN_BROADCAST_TIMING       = 0x0b, //!< LFN Broadcast Timing Information Element (LBT-IE)
    R_IE_WH_SUB_ID_NODE_ROLE                  = 0x0c, //!< Node Role Information Element (NR-IE)
    R_IE_WH_SUB_ID_LFN_UNICAST_SCHEDULE       = 0x0d, //!< LFN Unicast Schedule Information Element (LUS-IE)
    R_IE_WH_SUB_ID_FFN_4_LFN_UNICAST_SCHEDULE = 0x0e, //!< FFN for LFN Unicast Schedule Information Element (FLUS-IE)
    R_IE_WH_SUB_ID_LFN_BROADCAST_SCHEDULE     = 0x0f, //!< LFN Broadcast Schedule Information Element (LBS-IE)
    R_IE_WH_SUB_ID_LFN_NETWORK_DISCOVERY      = 0x10, //!< LFN Network Discovery IE (LND-IE)
    R_IE_WH_SUB_ID_LFN_TIMING_OFFSET          = 0x11, //!< LFN Timing Offset IE (LTO-IE)
    R_IE_WH_SUB_ID_PAN_IDENTIFIER             = 0x12, //!< PAN Identifier IE (PANID-IE)
#endif /* (R_WISUN_FAN_VERSION >= 110) */
} r_ie_wh_sub_id_t;

#ifdef R_HYBRID_PLC_RF
/*!
    \enum r_ie_g3h_sub_id_t
    \brief G3 Header Information Elements Sub ID
 */
typedef enum r_ie_g3h_sub_id_e
{
    R_IE_G3H_SUB_ID_RLQ              = 0, //!< The RLQ-IE is used to propagate reverse link quality information between nodes.
    R_IE_G3H_SUB_ID_LI               = 1, //!< The LI-IE is used to propagate duty cycle usage information and TX output power reduction between nodes.
    R_IE_G3H_SUB_ID_UTT              = 2, //!< TODO
    R_IE_G3H_SUB_ID_BROADCAST_TIMING = 3, //!< TODO
} r_ie_g3h_sub_id_t;
#endif /* R_HYBRID_PLC_RF */

/*!
    \struct r_ie_wh_header_t
    \brief Wi-SUN Header Information Elements (WH-IEs) header
 */
typedef struct
{
    uint8_t          length : 7;
    r_ie_wh_id_t     id;     //!< 8 bits ID (in create ignored and assumed to be R_IE_WH_ID_WISUN)
    // 1 bits constant: type = 0
    r_ie_wh_sub_id_t sub_id; //!< Sub ID (after parsing, valid if ID == R_IE_WH_ID_WISUN)
} r_ie_wh_header_t;

#ifdef R_HYBRID_PLC_RF
/*!
    \struct r_ie_g3h_header_t
    \brief G3 Header Information Elements (WH-IEs) header
 */
typedef struct
{
    uint8_t           length : 7;
    r_ie_wh_id_t      id;     //!< 8 bits ID (in create ignored and assumed to be R_IE_WH_ID_VENDOR)
    // 1 bits constant: type = 0
    uint8_t           cid[3]; //!< 3 bytes CID
    r_ie_g3h_sub_id_t sub_id; //!< Sub ID (after parsing, valid if ID == R_IE_WH_ID_VENDOR)
} r_ie_g3h_header_t;
#endif /* R_HYBRID_PLC_RF */

/*!
    \enum r_ie_wh_frame_type_t
    \brief Frame Type
 */
typedef enum r_ie_wh_frame_type_e
{
    R_IE_WH_FRAME_TYPE_PAN_ADVERT             = 0,  //!< PAN Advert
    R_IE_WH_FRAME_TYPE_PAN_ADVERT_SOLICIT     = 1,  //!< PAN Advert Solicit
    R_IE_WH_FRAME_TYPE_PAN_CONFIG             = 2,  //!< PAN Config
    R_IE_WH_FRAME_TYPE_PAN_CONFIG_SOLICIT     = 3,  //!< PAN Config Solicit
    R_IE_WH_FRAME_TYPE_DATA                   = 4,  //!< Data
    R_IE_WH_FRAME_TYPE_ACK                    = 5,  //!< Ack
    R_IE_WH_FRAME_TYPE_EAPOL                  = 6,  //!< EAPOL
#if (R_WISUN_FAN_VERSION >= 110)
    R_IE_WH_FRAME_TYPE_RESERVE_07             = 7,  //!< Reserved
    R_IE_WH_FRAME_TYPE_RESERVE_08             = 8,  //!< Reserved
    R_IE_WH_FRAME_TYPE_LFN_PAN_ADVERT         = 9,  //!< LFN PAN Advert
    R_IE_WH_FRAME_TYPE_LFN_PAN_ADVERT_SOLICIT = 10, //!< LFN PAN Advert Solicit
    R_IE_WH_FRAME_TYPE_LFN_PAN_CONFIG         = 11, //!< LFN PAN Config
    R_IE_WH_FRAME_TYPE_LFN_PAN_CONFIG_SOLICIT = 12, //!< LFN PAN Config Solicit
    R_IE_WH_FRAME_TYPE_LFN_TIME_SYNC          = 13, //!< LFN Time Sync
    R_IE_WH_FRAME_TYPE_RESERVE_14             = 14, //!< Reserved
    R_IE_WH_FRAME_TYPE_EXTENDED               = 15, //!< Extended Type (reserved, not used)
#endif /* (R_WISUN_FAN_VERSION >= 110) */
} r_ie_wh_frame_type_t;

/*!
    \struct r_ie_wh_utt_t
    \brief Unicast Timing and Frame Type Information Element (UTT-IE)
 */
typedef struct
{
    r_ie_wh_frame_type_t frame_type_id; //!< Frame Type ID
    // 4 bits reserved
    uint32_t             ufsi;          //!< Unicast Fractional Sequence Interval (24 bits)
} r_ie_wh_utt_t;

/*!
    \struct r_ie_wh_broadcast_timing_t
    \brief Broadcast Timing Information Element (BT-IE)
 */
typedef struct
{
    uint16_t slot;            //!< Broadcast Slot Number
    uint32_t interval_offset; //!< Broadcast Interval Offset (24 bits)
} r_ie_wh_broadcast_timing_t;

/*!
    \struct r_ie_wh_flow_control_t
    \brief Flow Control IE (FC-IE)
 */
typedef struct
{
    uint8_t transmit; //!< Transmit Flow Control in milliseconds
    uint8_t receive;  //!< Receive Flow Control in milliseconds
} r_ie_wh_flow_control_t;

/*!
    \struct r_ie_wh_rsl_t
    \brief Received Signal Level Information Element (RSL-IE)
 */
typedef struct
{
    uint8_t rsl;  //!< Received Signal Level in dB
} r_ie_wh_rsl_t;

/*!
    \struct r_ie_wh_vendor_t
    \brief Vendor Header Information Element (VH-IE)
 */
typedef struct
{
    uint16_t vendor_id;    //!< Wi-SUN Vendor Identifier
    uint8_t  content_size; //!< Size of 'content' field
    uint8_t* content;      //!< Vendor Defined Content
} r_ie_wh_vendor_t;

/*!
    \struct r_ie_wh_eapol_authenticator_t
    \brief EAPOL Authenticator EUI-64 Information Element (EA-IE)
 */
typedef struct
{
    uint8_t authenticator_eui64[8];  //!< EAPOL Authenticator EUI-64
} r_ie_wh_eapol_authenticator_t;

#ifdef R_HYBRID_PLC_RF
/*!
    \struct r_ie_g3h_link_information_t
    \brief G3 Link Information Information Element (LI-IE)
 */
typedef struct
{
    uint8_t duty_cycle;      //!< Duty Cycle Information
    uint8_t power_reduction; //! Power reduction
} r_ie_g3h_link_information_t;

/*!
    \struct r_ie_g3h_reverse_link_quality_t
    \brief G3 Reverse Link Quality Information Element (RLQ-IE)
 */
typedef struct
{
    uint8_t rlq;  //!< Duty Cycle Information
} r_ie_g3h_reverse_link_quality_t;
#endif /* R_HYBRID_PLC_RF */

#if (R_WISUN_FAN_VERSION >= 110)
/*!
    \struct r_ie_wh_lfn_utt_t
    \brief LFN Unicast Timing and Frame Type Information Element (LUTT-IE)
 */
typedef struct
{
    r_ie_wh_frame_type_t frame_type_id;   //!< Frame Type ID
    // 4 bits reserved
    uint16_t             slot;            //!< Unicast Slot Number (16 bits)
    uint32_t             interval_offset; //!< Unicast Interval Offset (24 bits)
} r_ie_wh_lfn_utt_t;

/*!
    \enum r_ie_wh_node_role_id_t
    \brief NR-IE Node Role ID
 */
typedef enum r_ie_wh_node_role_id_e
{
    R_IE_WH_NODE_ROLE_ID_FFN_BR     = 0,  //!< FFN (Full Function Node) Border Router
    R_IE_WH_NODE_ROLE_ID_FFN_ROUTER = 1,  //!< FFN (Full Function Node) Router Node
    R_IE_WH_NODE_ROLE_ID_LFN        = 2,  //!< LFN Limited Functional Node
} r_ie_wh_node_role_id_t;

/*!
    \enum r_ie_wh_node_role_listening_type_t
    \brief NR-IE Listening Type
 */
typedef enum r_ie_wh_node_role_listening_type_e
{
    R_IE_WH_NODE_ROLE_LISTENING_TYPE_CSL = 0,  //!< coordinated sampled listening (CSL)
    R_IE_WH_NODE_ROLE_LISTENING_TYPE_RIT = 1,  //!< sampled listening window is the semi-synchronized style (RIT)
} r_ie_wh_node_role_listening_type_t;

/*!
    \struct r_ie_wh_node_role_t
    \brief Node Role Information Element (NR-IE)
 */
typedef struct
{
    r_ie_wh_node_role_id_t             node_role_id : 3;   //!< Node Role ID field 3-bit
    // 4 bits reserved
    r_ie_wh_node_role_listening_type_t listening_type : 1; //!< Listening Type field is a boolean indicating how an LFN performs sampled listening

    uint8_t  clock_drift;                                  //!< Worst case drift of the clock used to measure its frequency hopping dwell interval
    uint8_t  timing_accuracy;                              //!< accuracy of time values generated by the node in 10ms

    uint32_t listening_interval_min;                       //!< The minimum acceptable interval for the LFN listening interval (in milliseconds), 24-bit unsigned integer field.
    uint32_t listening_interval_max;                       //!< The maximum acceptable interval for the LFN listening interval (in milliseconds), 24-bit unsigned integer field.
} r_ie_wh_node_role_t;

/*!
    \struct r_ie_wh_lfn_unicast_schedule_t
    \brief LFN Unicast Schedule Information Element (LUS-IE)
 */
typedef struct
{
    uint32_t listen_interval;  //!< Listen Interval (24-bit), MUST be set to the time between sampled listening points for an LFN (in milliseconds)
    uint8_t  channel_plan_tag; //!< Channel Plan Identifier (8-bit), MUST be set to identify the LFN Channel Plan IE (LCP-IE)
} r_ie_wh_lfn_unicast_schedule_t;

/*!
    \struct r_ie_wh_ffn_4_lfn_unicast_schedule_t
    \brief FFN for LFN Unicast Schedule Information Element (FLUS-IE)
 */
typedef struct
{
    uint8_t dwell_interval;   //!< Time in milliseconds during which the node is active on each channel in the hopping schedule
    uint8_t channel_plan_tag; //!< Channel Plan Identifier (8-bit), MUST be set to identify the LFN Channel Plan IE (LCP-IE)
} r_ie_wh_ffn_4_lfn_unicast_schedule_t;

/*!
    \struct r_ie_wh_lfn_broadcast_schedule_t
    \brief LFN Broadcast Schedule Information Element (LBS-IE)
 */
typedef struct
{
    uint32_t broadcast_interval;            //!< Time in milliseconds during which the node is active on each channel in the hopping schedule
    uint16_t broadcast_schedule_identifier; //!< Seed value for the FAN channel functions
    uint8_t  channel_plan_tag;              //!< Channel Plan Identifier (8-bit), MUST be set to identify the LFN Channel Plan IE (LCP-IE)
} r_ie_wh_lfn_broadcast_schedule_t;

/*!
    \struct r_ie_wh_lfn_network_discovery_t
    \brief LFN Network Discovery IE (LND-IE)
 */
typedef struct
{
    uint8_t  response_threshold;   //!< The LFN minimum required receive level above sensitivity
    uint32_t response_delay;       //!< time in milliseconds at which the requesting LFN will begin to listen for LPA responses (24-bit)
    uint8_t  discovery_slot_time;  //!< Time in milliseconds that the LFN will listen on a given channel for potential parent responses (8-bit)
    uint8_t  discovery_slots;      //!< The number of DST periods during which the LFN will be listening for FFN parent responses
    uint16_t discovery_first_slot; //!< Random number indicating the slot number used to determine the channel of the first DST
} r_ie_wh_lfn_network_discovery_t;

/*!
    \struct r_ie_wh_lfn_timing_offset
    \brief LFN Timing Offset IE (LTO-IE)
 */
typedef struct
{
    uint32_t offset;                      //!< Time advancement (in milliseconds) that the FFN desires the LFN apply to the LFN’s Listening Interval (24-bit)
    uint32_t adjusted_listening_interval; //!< The listening interval desired of the LFN (24-bit)
} r_ie_wh_lfn_timing_offset_t;

/*!
    \struct r_ie_wh_pan_id
    \brief PAN Identifier Information Element (PANID-IE)
 */
typedef struct
{
    uint16_t panid;  //!< PAN Identifier
} r_ie_wh_pan_id_t;

/*!
    \struct r_ie_wh_rendezvous_time
    \brief Rendezvous Time IE (RENDEZVOUSTIME-IE)
 */
typedef struct
{
    uint16_t rendezvous_time;  //!< Rendezvous Time field
    uint16_t wakeup_interval;  //!< Wake-up Interval field
} r_ie_wh_rendezvous_time_t;

/*
 * Definition of LGTK includes
 */
#define INCLUDES_LGTK0 1
#define INCLUDES_LGTK1 2
#define INCLUDES_LGTK2 4

/*!
    \struct r_ie_wp_lgtkhash_t
    \brief LGTK Hash Information Element (LGTKHASH-IE)
 */
typedef struct
{
    uint8_t includes      : 3; //!< Indicates the included LGTK Hashes
    uint8_t active_idx    : 2; //!< Indicates the currently active LGTK
    uint8_t reserved      : 3; //!< Reserved
    uint8_t hashes[3][8];      //!< First 128 bits (low order) of the SHA256 hash of the 3 LGTKs
} r_ie_wp_lgtkhash_t;

#endif /* (R_WISUN_FAN_VERSION >= 110) */

/*!
    \enum r_ie_wh_termination_t
    \brief Header Information Elements Termination Mode
 */
typedef enum
{
    R_IE_WH_TERMINATION_NONE = 0,  //!< No Termination Element
    R_IE_WH_TERMINATION_1    = 1,  //!< Termination Element 1 (0x7e)
    R_IE_WH_TERMINATION_2    = 2,  //!< Termination Element 2 (0x7f)
} r_ie_wh_termination_t;

/*!
    \struct r_ie_wh_t
    \brief Header Information Elements Composite
 */
typedef struct
{
    r_ie_wh_termination_t termination;
    union
    {
        struct
        {
            uint8_t flow_control : 1;
            uint8_t utt : 1;
            uint8_t rsl : 1;
            uint8_t broadcast_timing : 1;
            uint8_t vendor : 1;
            uint8_t eapol_authenticator : 1;
#if (R_WISUN_FAN_VERSION >= 110)
            uint8_t lfn_utt : 1;
            uint8_t lfn_broadcast_timing : 1;
            uint8_t node_role : 1;
            uint8_t lfn_unicast_schedule : 1;
            uint8_t ffn_4_lfn_unicast_schedule : 1;
            uint8_t lfn_broadcast_schedule : 1;
            uint8_t lfn_network_discovery : 1;
            uint8_t lfn_timing_offset : 1;
            uint8_t panid : 1;
            uint8_t rendezvous_time : 1;
#endif /* (R_WISUN_FAN_VERSION >= 110) */
#ifdef R_HYBRID_PLC_RF
            uint8_t link_information : 1;
            uint8_t rlq : 1;
#endif /* R_HYBRID_PLC_RF */
        } e;
#if (R_WISUN_FAN_VERSION >= 110)
        uint16_t mask;
#else
        uint8_t  mask;
#endif /* (R_WISUN_FAN_VERSION >= 110) */
    } has;
#ifndef R_HYBRID_PLC_RF
    r_ie_wh_flow_control_t               flow_control;
    r_ie_wh_utt_t                        utt;
    r_ie_wh_rsl_t                        rsl;
    r_ie_wh_broadcast_timing_t           broadcast_timing;
    r_ie_wh_vendor_t                     vendor;
    r_ie_wh_eapol_authenticator_t        eapol_authenticator;
#if (R_WISUN_FAN_VERSION >= 110)
    r_ie_wh_lfn_utt_t                    lfn_utt;
    r_ie_wh_broadcast_timing_t           lfn_broadcast_timing;
    r_ie_wh_node_role_t                  node_role;
    r_ie_wh_lfn_unicast_schedule_t       lfn_unicast_schedule;
    r_ie_wh_ffn_4_lfn_unicast_schedule_t ffn_4_lfn_unicast_schedule;
    r_ie_wh_lfn_broadcast_schedule_t     lfn_broadcast_schedule;
    r_ie_wh_lfn_network_discovery_t      lfn_network_discovery;
    r_ie_wh_lfn_timing_offset_t          lfn_timing_offset;
    r_ie_wh_pan_id_t                     pan_id;
    r_ie_wh_rendezvous_time_t            rendezvous_time;
#endif /* (R_WISUN_FAN_VERSION >= 110) */
#else  /* ifndef R_HYBRID_PLC_RF */
    r_ie_g3h_link_information_t          link_information;
    r_ie_g3h_reverse_link_quality_t      rlq;
    r_ie_wh_utt_t                        utt;
    r_ie_wh_broadcast_timing_t           broadcast_timing;
#endif /* R_HYBRID_PLC_RF */
} r_ie_wh_t;


/*!
    \enum r_ie_wp_group_t
    \brief Wi-SUN Payload Information Element Group
 */
typedef enum r_ie_wp_group_e
{
    R_IE_WP_GROUP_MP          =  3,  //!< MP Information Element (MP-IE)
    R_IE_WP_GROUP_WP          =  4,  //!< Payload Information Elements (WP-IEs)
    R_IE_WP_GROUP_TERMINATION = 15,  //!< Payload Termination Element
} r_ie_wp_group_t;

/*!
    \struct r_ie_wp_header_t
    \brief Wi-SUN Payload Information Elements (WP-IEs) header
 */
typedef struct
{
    uint16_t        length;
    r_ie_wp_group_t group;  //!< Group ID
    // 1 bits constant: type = 1
} r_ie_wp_header_t;

/*!
    \enum r_ie_wp_short_nested_sub_id_t
    \brief Wi-SUN Payload Information Elements Short Nested Sub ID
 */
typedef enum r_ie_wp_short_nested_sub_id_e
{
    R_IE_WP_SHORT_NESTED_SUB_ID_PAN      =  0x04,  //!< PAN Information Element (PAN-IE)
    R_IE_WP_SHORT_NESTED_SUB_ID_NETNAME  =  0x05,  //!< Network Name Information Element (NETNAME-IE)
    R_IE_WP_SHORT_NESTED_SUB_ID_PANVER   =  0x06,  //!< PAN Version Information Element (PANVER-IE)
    R_IE_WP_SHORT_NESTED_SUB_ID_GTKHASH  =  0x07,  //!< GTK Hash Information Element (GTKHASH-IE)
#if (R_WISUN_FAN_VERSION >= 110)
    R_IE_WP_SHORT_NESTED_SUB_ID_PCAP     =  0x08,  //!< PHY Capability Information Element (PCAP-IE)
    R_IE_WP_SHORT_NESTED_SUB_ID_LFNVER   =  0x40,  //!< LFN Version Information Element (LFNVER-IE)
    R_IE_WP_SHORT_NESTED_SUB_ID_LGTKHASH =  0x41,  //!< LFN GTK Hash Information IE (LGTKHASH-IE)
#endif /* (R_WISUN_FAN_VERSION >= 110) */

} r_ie_wp_short_nested_sub_id_t;

/*!
    \struct r_ie_wp_short_nested_t
    \brief Short Nested Information Elements (WH-IEs) header
 */
typedef struct
{
    uint8_t length;
    r_ie_wp_short_nested_sub_id_t sub_id;  //!< Sub ID
    // 1 bits constant: type = 0
} r_ie_wp_short_nested_t;

/*!
    \enum r_ie_wp_long_nested_sub_id_t
    \brief Wi-SUN Payload Information Elements Long Nested Sub ID
 */
typedef enum r_ie_wp_long_nested_sub_id_e
{
    R_IE_WP_LONG_NESTED_SUB_ID_UNICAST_SCHEDULE   = 1,  //!< Unicast Schedule Information Element (US-IE)
    R_IE_WP_LONG_NESTED_SUB_ID_BROADCAST_SCHEDULE = 2,  //!< Broadcast Schedule Information Element (BS-IE)
    R_IE_WP_LONG_NESTED_SUB_ID_VENDOR             = 3,  //!< Vendor Payload Information Element (VP-IE)
#if (R_WISUN_FAN_VERSION >= 110)
    R_IE_WP_LONG_NESTED_SUB_ID_LFN_CHANNEL_PLAN   = 4,  //!< LFN Channel Plan Information Element (LCP-IE)
#endif /* (R_WISUN_FAN_VERSION >= 110) */
} r_ie_wp_long_nested_sub_id_t;

/*!
    \struct r_ie_wp_long_nested_t
    \brief Short Nested Information Elements (WH-IEs) header
 */
typedef struct
{
    uint16_t length;
    r_ie_wp_long_nested_sub_id_t sub_id;  //!< Sub ID
    // 1 bits constant: type = 1
} r_ie_wp_long_nested_t;

/*!
    \enum r_ie_wp_pan_routing_method_t
    \brief PAN Information Element (PAN-IE) Routing Method
 */
typedef enum r_ie_wp_pan_routing_method_e
{
    R_IE_WP_PAN_ROUTING_METHOD_L2_MHDS = 0,  //!< PAN is L2 routed (MHDS)
    R_IE_WP_PAN_ROUTING_METHOD_L3_RPL  = 1,  //!< PAN is L3 routed (RPL)
} r_ie_wp_pan_routing_method_t;

/*!
    \enum r_ie_wp_pan_fan_tps_version_t
    \brief PAN Information Element (PAN-IE) Wi-SUN FAN TPS Version
 */
typedef enum r_ie_wp_pan_fan_tps_version_e
{
    R_IE_WP_PAN_FAN_TPS_VERSION_1_0 = 1,  //!< Wi-SUN FAN version 1.0
    R_IE_WP_PAN_FAN_TPS_VERSION_1_1 = 2,  //!< Wi-SUN FAN version 1.1
} r_ie_wp_pan_fan_tps_version_t;

/*!
    \struct r_ie_wp_pan_t
    \brief PAN Information Element (PAN-IE)
 */
typedef struct
{
    uint16_t pan_size;                                 //!< Number of nodes communicating through the Border Router
    uint16_t routing_cost;                             //!< Transmitting node's routing path ETX to the Border Router
    uint8_t  use_parent_bs_ie                     : 1; //!< Is set to 1 if a node MUST propagate the parent's BS-IE
    r_ie_wp_pan_routing_method_t routing_method   : 1; //!< Routing Method
    uint8_t  reserved                             : 3; //!< Reserved
    uint8_t  fan_tps_version                      : 3; //!< Wi-SUN FAN TPS version
} r_ie_wp_pan_t;

/*!
    \struct r_ie_wp_netname_t
    \brief Network Name Information Element (NETNAME-IE)
 */
typedef struct
{
    char name[32 + 1];  //!< Network Name administratively configured on a node
} r_ie_wp_netname_t;

/*!
    \struct r_ie_wp_panver_t
    \brief PAN Version Information Element (PANVER-IE)
 */
typedef struct
{
    uint16_t pan_version;  //!< Current value of the PAN Version disseminated by the PAN Border Router
} r_ie_wp_panver_t;

/*!
    \struct r_ie_wp_gtkhash_t
    \brief GTK Hash Information Element (GTKHASH-IE)
 */
typedef struct
{
    uint8_t hashes[4][8];  //!< First 128 bits (low order) of the SHA256 hash of the 4 GTKs
} r_ie_wp_gtkhash_t;

/*!
    \enum r_ie_wp_schedule_channel_plan_t
    \brief Unicast/Broadcast Schedule Information Element Channel Plan
 */
typedef enum r_ie_wp_schedule_channel_plan_e
{
    R_IE_WP_SCHEDULE_CHANNEL_PLAN_INDIRECT = 0,  //!< Regulatory Domain and Operating Class
    R_IE_WP_SCHEDULE_CHANNEL_PLAN_EXPLICIT = 1,  //!<  Channel 0 frequency, number of channels, and channel spacing
} r_ie_wp_schedule_channel_plan_t;

/*!
    \enum r_ie_wp_schedule_channel_function_t
    \brief Unicast/Broadcast Schedule Information Element Channel Function
 */
typedef enum r_ie_wp_schedule_channel_function_e
{
    R_IE_WP_SCHEDULE_CHANNEL_FUNCTION_FIXED       = 0,  //!< Single fixed channel
    R_IE_WP_SCHEDULE_CHANNEL_FUNCTION_TR51        = 1,  //!< Channel function described in section 7.1 of [ANSITIA-4957.200] (TR51CF)
    R_IE_WP_SCHEDULE_CHANNEL_FUNCTION_DIRECT_HASH = 2,  //!< Direct Hash channel function
    R_IE_WP_SCHEDULE_CHANNEL_FUNCTION_VENDOR      = 3,  //!< Vendor Defined channel function explicitly provided by the Hop Count and Hop List
} r_ie_wp_schedule_channel_function_t;

/*!
    \enum r_ie_wp_schedule_excluded_channel_control_t
    \brief Unicast/Broadcast Schedule Information Element Excluded Channel Control
 */
typedef enum r_ie_wp_schedule_excluded_channel_control_e
{
    R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_NONE  = 0,  //!< No excluded channels specification is included in the IE
    R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_RANGE = 1,  //!< Excluded Channel Range field is included in the IE
    R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_MASK  = 2,  //!< Excluded Channel Mask field is included in the IE
} r_ie_wp_schedule_excluded_channel_control_t;

/*!
    \enum r_ie_wp_schedule_regulatory_domain_t
    \brief Unicast/Broadcast Schedule Information Element Regulatory Domain
 */
typedef enum r_ie_wp_schedule_regulatory_domain_e
{
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_WORLD         =  0,  //!< World
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_NORTH_AMERICA =  1,  //!< North America
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_JAPAN         =  2,  //!< Japan
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_EUROPE        =  3,  //!< Europe
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_CHINA         =  4,  //!< China
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_INDIA         =  5,  //!< India
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_MEXICO        =  6,  //!< Mexico
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_BRAZIL        =  7,  //!< Brazil
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_AUSTRALIA_NZ  =  8,  //!< Australia/New Zealand
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_KOREA         =  9,  //!< Korea
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_PHILIPPINES   = 10,  //!< Philippines
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_MALAYSIA      = 11,  //!< Malaysia
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_HONG_KONG     = 12,  //!< Hong Kong
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_SINGAPORE     = 13,  //!< Singapore
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_THAILAND      = 14,  //!< Thailand
    R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_VIETNAM       = 15,  //!< Vietnam
} r_ie_wp_schedule_regulatory_domain_t;

/*!
    \enum r_ie_wp_schedule_channel_spacing_t
    \brief Unicast/Broadcast Schedule Information Element Channel Spacing
 */
typedef enum r_ie_wp_schedule_channel_spacing_e
{
    R_IE_WP_SCHEDULE_CHANNEL_SPACING_200  =  0,  //!<  200 kHz channel spacing
    R_IE_WP_SCHEDULE_CHANNEL_SPACING_400  =  1,  //!<  400 kHz channel spacing
    R_IE_WP_SCHEDULE_CHANNEL_SPACING_600  =  2,  //!<  600 kHz channel spacing
    R_IE_WP_SCHEDULE_CHANNEL_SPACING_100  =  3,  //!<  100 kHz channel spacing
#if  (R_WISUN_FAN_VERSION >= 110)
    R_IE_WP_SCHEDULE_CHANNEL_SPACING_800  =  4,  //!<  800 kHz channel spacing
    R_IE_WP_SCHEDULE_CHANNEL_SPACING_1000 =  5,  //!< 1000 kHz channel spacing
    R_IE_WP_SCHEDULE_CHANNEL_SPACING_1200 =  6,  //!< 1200 kHz channel spacing
    R_IE_WP_SCHEDULE_CHANNEL_SPACING_2400 =  7,  //!< 2400 kHz channel spacing
#endif /* (R_WISUN_FAN_VERSION >= 110) */
} r_ie_wp_schedule_channel_spacing_t;

/* Disable padding so that we can use memcmp for these structs and transmit them via the serial interface */
R_HEADER_UTILS_PRAGMA_PACK_1

/*!
    \struct r_ie_wp_excluded_range_t
    \brief Unicast/Broadcast Schedule Information Element Excluded Channel Ranges
 */
typedef struct
{
    uint16_t start; //!< Start Channel Number
    uint16_t end;   //!< End Channel Number
} r_ie_wp_excluded_channel_range_t;

/*!
    \struct r_ie_wp_unicast_schedule_t
    \brief Unicast Schedule Information Element (US-IE)
 */
typedef struct
{
    uint8_t dwell_interval;                                                   //!< Time in milliseconds during which the node is active on each channel in the hopping schedule
    uint8_t clock_drift;                                                      //!< Worst case drift of the clock used to measure its frequency hopping dwell interval
    uint8_t timing_accuracy;                                                  //!< accuracy of time values generated by the node in 10ms
    r_ie_wp_schedule_channel_plan_t             channel_plan : 3;             //!< Channel Plan
    r_ie_wp_schedule_channel_function_t         channel_function : 3;         //!< Channel Function
    r_ie_wp_schedule_excluded_channel_control_t excluded_channel_control : 2; //!< Excluded Channel Control
    r_ie_wp_schedule_regulatory_domain_t        regulatory_domain : 8;        //!< Regulatory Domain
    uint8_t operating_class;                                                  //!< Operating Class
    uint32_t ch0;                                                             //!< Channel 0 frequency in kHz
    r_ie_wp_schedule_channel_spacing_t          channel_spacing : 4;          //!< Channel Spacing
    // 4 bits reserved
    uint16_t no_of_channels;                                                  //!< Number of channels in the channel plan (including any excluded channels)
    uint16_t fixed_channel;                                                   //!< The single channel used by the transmitting node
#if !R_IE_VENDOR_DEFINED_CHANNEL_FUNCTION_DISABLED
    uint8_t  channel_hop_count;                                               //!< Number of channels in the vendor defined hop sequence
    uint8_t* channel_hop_list;                                                //!< The channels in the vendor defined hop sequence
#endif /* !R_IE_VENDOR_DEFINED_CHANNEL_FUNCTION_DISABLED */
    uint8_t  no_of_excluded_ranges;                                           //!< Number of channel ranges in the list of ranges
    union
    {
        const uint8_t* bytes;                     //!< Pointer to the raw bytes of the excluded channel ranges set by the parse function
        r_ie_wp_excluded_channel_range_t* ranges; //!< Pointer to the excluded channel ranges used by the create function
    } excluded_ranges;                            //!< Excluded Channel Ranges (see R_IE_WP_ExcludedChannelRangesFromBytes to convert from bytes to ranges)
    uint8_t* excluded_channel_mask;               //!< Excluded Channel Mask
} r_ie_wp_unicast_schedule_t;

/*!
    \struct r_ie_wp_broadcast_schedule_t
    \brief Broadcast Schedule Information Element (BS-IE)
 */
typedef struct
{
    uint32_t broadcast_interval;            //!< Time in milliseconds during which the node is active on each channel in the hopping schedule
    uint16_t broadcast_schedule_identifier; //!< Seed value for the FAN channel functions
    r_ie_wp_unicast_schedule_t us;          //!< Shared fields with Unicast Schedule
} r_ie_wp_broadcast_schedule_t;

/**
 * A unicast or broadcast schedule information element (US-IE or BS-IE) that has been serialized to a byte buffer.
 */
typedef struct
{
    uint16_t size;    //!< The size of the serialized schedule in bytes
    uint8_t  bytes[]; //!< The byte buffer that holds the serialized schedule
} r_ie_wp_serialized_sched_t;

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

/*!
    \enum r_ie_wp_transfer_type_t
    \brief MP-IE Transfer Type
 */
typedef enum r_ie_wp_transfer_type_e
{
    R_IE_WP_TRANSFER_TYPE_FULL_FRAME          = 0,  //!< Full Upper-Layer frame
    R_IE_WP_TRANSFER_TYPE_FULL_FRAME_NO_MUXID = 1,  //!< Full Upper-Layer frame for protocols having small Multiplex ID. The Multiplex ID of 0x00-0x1f will be encoded inside the Transaction ID field, and the Multiplex ID field is omitted.
    R_IE_WP_TRANSFER_TYPE_FRAGMENT            = 2,  //!< First fragment, total size of the packet and Multiplex ID or Middle fragment
    R_IE_WP_TRANSFER_TYPE_RESERVED1           = 3,  //!< Reserved
    R_IE_WP_TRANSFER_TYPE_LAST_FRAGMENT       = 4,  //!< Last fragment
    R_IE_WP_TRANSFER_TYPE_RESERVED2           = 5,  //!< Reserved
    R_IE_WP_TRANSFER_TYPE_ABORT               = 6,  //!< Abort (possibly with total_frame_size indicating max size)
    R_IE_WP_TRANSFER_TYPE_RESERVED3           = 7,  //!< Reserved
} r_ie_wp_transfer_type_t;

/*!
    \enum r_ie_wp_mp_multiplex_id_t
    \brief MP-IE Multiplex ID
 */
typedef enum r_ie_wp_mp_multiplex_id_e
{
    R_IE_WP_MP_MULTIPLEX_ID_KMP    = 0x0001,  //!< Key Management Protocol
    R_IE_WP_MP_MULTIPLEX_ID_WI_SUN = 0x0002,  //!< Wi-SUN Defined Payload
    R_IE_WP_MP_MULTIPLEX_ID_LoWPAN = 0xA0ED,  //!< LoWPAN encapsulation (refer [RFC7973])
} r_ie_wp_mp_multiplex_id_t;

/*!
    \struct r_ie_wp_mp_t
    \brief MP Information Element (MP-IE)
 */
typedef struct
{
    r_ie_wp_transfer_type_t   transfer_type : 3;              //!< Transfer Type
    uint8_t                   transaction_id : 5;             //!< Transaction ID field is an application layer issue not in scope of this specification
    uint8_t                   fragment_number;                //!< Number of fragment (0: first, else intermediate)
    r_boolean_t               abort_has_total_frame_size : 1; //!< Abort with total frame size indication (only relevant for ABORT)
    uint16_t                  total_frame_size;               //!< Total Upper Layer Frame Size
    r_ie_wp_mp_multiplex_id_t multiplex_id;                   //!< Multiplex ID
    uint16_t                  fragment_size;                  //!< Size of 'fragment' field
    uint8_t*                  fragment;                       //!< Upper Layer Fragment
} r_ie_wp_mp_t;

/*!
    \struct r_ie_wp_vendor_t
    \brief Vendor Payload Information Element (VP-IE)
 */
typedef struct
{
    uint16_t vendor_id;    //!< Wi-SUN Vendor Identifier
    uint16_t content_size; //!< Size of 'content' field
    uint8_t* content;      //!< Vendor Defined Content
} r_ie_wp_vendor_t;

#if (R_WISUN_FAN_VERSION >= 110)

/*
 * Definition of supported OFDM operating modes
 */
#define OFDM_MCS0    0x0001
#define OFDM_MCS1    0x0002
#define OFDM_MCS2    0x0004
#define OFDM_MCS3    0x0008
#define OFDM_MCS4    0x0010
#define OFDM_MCS5    0x0020
#define OFDM_MCS6    0x0040

/*
 * Definition of supported FSK operating modes
 */
#define FSK_RESERVED 0x0001
#define FSK_MODE_1A  0x0002
#define FSK_MODE_1B  0x0004
#define FSK_MODE_2A  0x0008
#define FSK_MODE_2B  0x0010
#define FSK_MODE_3   0x0020
#define FSK_MODE_4A  0x0040
#define FSK_MODE_4B  0x0080
#define FSK_MODE_5   0x0100

/*!
    \struct r_ie_wp_phy_descriptor_t
    \brief PHY Descriptor field
 */
typedef struct
{
    uint8_t  phy_type : 4;    //!< PHY Type field
    // 4 bits reserved
    uint16_t operating_mode;  //!< Operating Mode
} r_ie_wp_phy_descriptor_t;

/*!
    \struct r_ie_wp_pcap_t
    \brief PHY Capabilities Information Element (PCAP-IE)
 */
typedef struct
{
    uint8_t no_of_phy_descriptors : 4;         //!< Number of PHY Descriptors
    // 4 bits reserved
    r_ie_wp_phy_descriptor_t* phy_descriptors; //!< Pointer to the phy descriptors used by the create function
} r_ie_wp_pcap_t;

/*!
    \struct r_ie_wp_lfn_channel_plan_t
    \brief LFN Channel Plan Information Element (LCP-IE)
 */
typedef struct
{
    uint8_t channel_plan_tag;       //!< 8-bit field which MUST be set to identify a specific LFN Channel Plan IE (LCP-IE)
    r_ie_wp_unicast_schedule_t lcp; //!< Shared fields with Unicast Schedule
} r_ie_wp_lfn_channel_plan_t;

/*!
    \struct r_ie_wp_lfn_version_t
    \brief LFN Version Information Element (LFNVER-IE)
 */
typedef struct
{
    uint16_t lfn_version;  //!< LFN Version
} r_ie_wp_lfn_version_t;

#endif /* (R_WISUN_FAN_VERSION >= 110) */

/*!
    \struct r_ie_wp_t
    \brief Payload Information Elements Composite
 */
typedef struct
{
    union
    {
        struct
        {
            uint8_t unicast_schedule : 1;
            uint8_t broadcast_schedule : 1;
            uint8_t pan : 1;
            uint8_t netname : 1;
            uint8_t panver : 1;
            uint8_t gtkhash : 1;
            uint8_t mp : 1;
            uint8_t vendor : 1;
#if (R_WISUN_FAN_VERSION >= 110)
            uint8_t pcap : 1;
            uint8_t lfn_channel_plan : 1;
            uint8_t lfn_version : 1;
            uint8_t lfn_gtkhash : 1;
        } e;
        uint16_t mask;
#else  /* (R_WISUN_FAN_VERSION >= 110) */
        } e;
        uint8_t mask;
#endif /* (R_WISUN_FAN_VERSION >= 110) */
    } has;
    uint8_t has_termination : 1;
    r_ie_wp_unicast_schedule_t unicast_schedule;
    r_ie_wp_broadcast_schedule_t broadcast_schedule;
    r_ie_wp_pan_t pan;
    r_ie_wp_netname_t netname;
    r_ie_wp_panver_t panver;
    r_ie_wp_gtkhash_t gtkhash;
    r_ie_wp_vendor_t vendor;
    r_ie_wp_mp_t mp;
#if (R_WISUN_FAN_VERSION >= 110)
    r_ie_wp_pcap_t pcap;
    r_ie_wp_lfn_channel_plan_t lfn_channel_plan;
    r_ie_wp_lfn_version_t lfn_version;
    r_ie_wp_lgtkhash_t lfn_gtkhash;
#endif /* (R_WISUN_FAN_VERSION >= 110) */
} r_ie_wp_t;

/***********************************************************************
* Function definitions
***********************************************************************/

/******************************************************************************************************************//**
 * Parse the buffer for header information elements.
 * @param[out] whie Pointer to the struct where the parsed information is stored.
 * @param buf The input buffer.
 * @param size The input buffer size.
 * @param[out] consumed The number of bytes read from the buffer.
 * @return R_RESULT_SUCCESS on success.
 *********************************************************************************************************************/
r_result_t R_IE_WH_Parse(r_ie_wh_t* whie, const uint8_t* buf, uint16_t size, uint16_t* consumed);

/******************************************************************************************************************//**
 * Serialize header information elements to a buffer.
 * @param whie Pointer to the header IE.
 * @param[out] buf The output buffer.
 * @param size The output buffer size.
 * @param[out] written The number of bytes written to the buffer.
 * @return R_RESULT_SUCCESS on success.
 *********************************************************************************************************************/
r_result_t R_IE_WH_Create(const r_ie_wh_t* whie, uint8_t* buf, uint16_t size, uint16_t* written);

/******************************************************************************************************************//**
 * Parse the buffer for payload information elements.
 * @param[out] wpie Pointer to the struct where the parsed information is stored.
 * @param buf The input buffer.
 * @param size The input buffer size.
 * @param[out] consumed The number of bytes read from the buffer.
 * @return R_RESULT_SUCCESS on success.
 *********************************************************************************************************************/
r_result_t R_IE_WP_Parse(r_ie_wp_t* wpie, const uint8_t* buf, uint16_t size, uint16_t* consumed);

/******************************************************************************************************************//**
 * Serialize payload information elements to a buffer.
 * @param whie Pointer to the header IE.
 * @param[out] buf The output buffer.
 * @param size The output buffer size.
 * @param[out] written The number of bytes written to the buffer.
 * @return R_RESULT_SUCCESS on success.
 *********************************************************************************************************************/
r_result_t R_IE_WP_Create(const r_ie_wp_t* wpie, uint8_t* buf, uint16_t size, uint16_t* written);

/******************************************************************************************************************//**
 * Serialize a unicast schedule to a buffer.
 * @param p Pointer to the unicast schedule.
 * @param[out] buf The output buffer.
 * @param size The size of the output buffer.
 * @param pos The starting position in the output buffer.
 * @retval The number of bytes written to the output buffer on success.
 * @retval A negative result code on failure.
 *********************************************************************************************************************/
int16_t R_IE_UnicastScheduleCreate(const r_ie_wp_unicast_schedule_t* p, uint8_t* buf, uint16_t size, uint16_t pos);

/******************************************************************************************************************//**
 * Parse a buffer for a unicast schedule.
 * @param[out] p Pointer to the struct where the parsed information is stored.
 * @param buf The input buffer.
 * @param size The size of the input buffer.
 * @param pos The starting position in the input buffer.
 * @retval The number of bytes read from the input buffer on success.
 * @retval A negative result code on failure.
 *********************************************************************************************************************/
int16_t R_IE_UnicastScheduleParse(r_ie_wp_unicast_schedule_t* p, const uint8_t* buf, uint16_t size, uint16_t pos);

/******************************************************************************************************************//**
 * Serialize a broadcast schedule to a buffer.
 * @param p Pointer to the broadcast schedule.
 * @param[out] buf The output buffer.
 * @param size The size of the output buffer.
 * @param pos The starting position in the output buffer.
 * @retval The number of bytes written to the output buffer on success.
 * @retval A negative result code on failure.
 *********************************************************************************************************************/
int16_t R_IE_BroadcastScheduleCreate(const r_ie_wp_broadcast_schedule_t* p, uint8_t* buf, uint16_t size, uint16_t pos);

/******************************************************************************************************************//**
 * Parse a buffer for a broadcast schedule.
 * @param[out] p Pointer to the struct where the parsed information is stored.
 * @param buf The input buffer.
 * @param size The size of the input buffer.
 * @param pos The starting position in the input buffer.
 * @retval The number of bytes read from the input buffer on success.
 * @retval A negative result code on failure.
 *********************************************************************************************************************/
int16_t R_IE_BroadcastScheduleParse(r_ie_wp_broadcast_schedule_t* p, const uint8_t* buf, uint16_t size, uint16_t pos);

#if (R_WISUN_FAN_VERSION >= 110)
/******************************************************************************************************************//**
 * Serialize a LFN channel plan to a buffer.
 * @param p Pointer to the LFN channel plan.
 * @param[out] buf The output buffer.
 * @param size The size of the output buffer.
 * @param pos The starting position in the output buffer.
 * @retval The number of bytes written to the output buffer on success.
 * @retval A negative result code on failure.
 *********************************************************************************************************************/
int16_t R_IE_LfnCannelPlanCreate(const r_ie_wp_lfn_channel_plan_t* p, uint8_t* buf, uint16_t size, uint16_t pos);

/******************************************************************************************************************//**
 * Parse a buffer for a LFN channel plan.
 * @param[out] p Pointer to the struct where the parsed information is stored.
 * @param buf The input buffer.
 * @param size The size of the input buffer.
 * @param pos The starting position in the input buffer.
 * @retval The number of bytes read from the input buffer on success.
 * @retval A negative result code on failure.
 *********************************************************************************************************************/
int16_t R_IE_LfnCannelPlanParse(r_ie_wp_lfn_channel_plan_t* p, const uint8_t* buf, uint16_t size, uint16_t pos);

/******************************************************************************************************************//**
 * Serialize a the PHY capabilities to a buffer.
 * @param p Pointer to the PHY capabilities.
 * @param[out] buf The output buffer.
 * @param size The size of the output buffer.
 * @param pos The starting position in the output buffer.
 * @retval The number of bytes written to the output buffer on success.
 * @retval A negative result code on failure.
 *********************************************************************************************************************/
int16_t R_IE_PhyCapabilitiesCreate(const r_ie_wp_pcap_t* p, uint8_t* buf, uint16_t size, uint16_t pos);

/******************************************************************************************************************//**
 * Parse a buffer for the PHY capabilities.
 * @param[out] p Pointer to the struct where the parsed information is stored.
 * @param buf The input buffer.
 * @param size The size of the input buffer.
 * @param pos The starting position in the input buffer.
 * @retval The number of bytes read from the input buffer on success.
 * @retval A negative result code on failure.
 *********************************************************************************************************************/
int16_t R_IE_PhyCapabilitiesParse(r_ie_wp_pcap_t* p, const uint8_t* buf, uint16_t size, uint16_t pos);
#endif /* (R_WISUN_FAN_VERSION >= 110) */

/******************************************************************************************************************//**
 * Determine the number of available channels for a given schedule.
 * @param schedule The schedule whose number of channels should be determined.
 * @retval The number of channels for the specified schedule.
 * @retval A negative result code if the specified schedule is illegal.
 *********************************************************************************************************************/
int16_t R_IE_ScheduleNumberOfChannels(const r_ie_wp_unicast_schedule_t* schedule);

/******************************************************************************************************************//**
 * Determine the number of channels used for the specified combination of regulatory domain and operating class
 * @param rd The regulatory domain
 * @param operating_class The operating class
 * @retval The number of channels for the specified combination.
 * @retval A negative result code if the specified combination is illegal.
 *********************************************************************************************************************/
int16_t R_IE_RegOpNumberOfChannels(r_ie_wp_schedule_regulatory_domain_t rd, uint8_t operating_class);

/******************************************************************************************************************//**
 * Determine the correct size of the excluded channel mask (based on the number of channels) for the given schedule.
 * @param schedule The unicast schedule.
 * @retval The size of the excluded channel mask in bytes.
 * @retval A negative result code if the values within the specified schedule are illegal.
 *********************************************************************************************************************/
int16_t R_IE_ScheduleNumberOfMaskBytes(const r_ie_wp_unicast_schedule_t* schedule);

/******************************************************************************************************************//**
 * Set IE Termination flags in header_ie according to Header IE, Payload IE or Payload existence
 * @param headerIE    Pointer to structure containing the header information elements.
 * @param payloadIE   Pointer to structure containing the payload information elements.
 * @param msduLen     Payload frame length
 *********************************************************************************************************************/
void R_IE_Update_IE_termination_final(r_ie_wh_t* header_ie,
                                      r_ie_wp_t* payload_ie,
                                      uint16_t   msduLen);

#endif /* R_MAC_IE_H */
