/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2022 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/**
 * @file r_nwk_task.h
 * @brief API of the central network (NWK) task processing module.
 * @attention This API must only be used from within the NWK task.
 */

#ifndef R_NWK_TASK_H
#define R_NWK_TASK_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_nwk_api.h"
#include "mac_mcps.h"

#if R_MODEM_SERVER
#include "r_log_flags.h"
#endif

/******************************************************************************
   Macro definitions
******************************************************************************/

#define R_MAC_MSG_LAST (0x80)

/******************************************************************************
   Variable Externs
******************************************************************************/

/******************************************************************************
   Functions Prototypes
******************************************************************************/
// -----------------------------------------------
// function for processing API event (ntf/ind)
// -----------------------------------------------
r_result_t R_ICMP_IcmpEchoReplyInd(const r_icmp_echo_reply_ind_t* p_indication,
                                   const uint8_t*                 data,
                                   void*                          vp_nwkGlobal);     // void* -> r_nwk_cb_t*
r_result_t R_ICMP_IcmpErrorInd(const r_icmp_error_ind_t* p_indication,
                               const uint8_t*            invokingPacket,
                               void*                     vp_nwkGlobal);     // void* -> r_nwk_cb_t*
r_result_t R_UDP_UdpDataInd(uint8_t* p_srcAddress,
                            uint8_t* p_dstAddress,
                            uint16_t srcPort,
                            uint16_t dstPort,
                            uint8_t* p_data,
                            uint16_t dataLength,
                            uint16_t status,
                            uint8_t  lqi,
                            void*    vp_nwkGlobal);

/**
 * Indicate an NWK join state update to the application layer
 * @param joinState The new join state of this device.
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_JoinStateUpdateInd(r_nwk_join_state_t joinState, void* vp_nwkGlobal);

/**
 * Pass incoming DHCPv6 Vendor-specific Information Option data from a DHCPv6 client message to the application layer
 * @param src The EUI-64 of the sender
 * @param enterprise_number The vendor's registered Enterprise Number as registered with IANA
 * @param data The DHCPv6 Vendor-specific Information Option data
 * @param dataLength The length of the DHCPv6 Vendor-specific Information Option data in bytes
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_DhcpVendorOptionDataInd(const r_eui64_t* src, uint32_t enterprise_number, const uint8_t* data,
                                         uint16_t dataLength, void* vp_nwkGlobal);
#if R_BORDER_ROUTER_ENABLED

/**
 * Indicate a status update for another FAN device to the application layer
 * @param devAddr The IPv6 address of the device whose status has changed
 * @param updateCode Identifier for the type of status update that has been detected.
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_DeviceStatusInd(const r_ipv6addr_t* devAddr, r_nwk_device_update_code_t updateCode, void* vp_nwkGlobal);

/**
 * Indicate the application layer to start the border router authentication module
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_AuthBrStartInd(void* vp_nwkGlobal);

/**
 * Indicate the application layer to perform the periodic update of the border router authentication module
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_AuthBrPeriodicUpdateInd(void* vp_nwkGlobal);

#endif

/**
 * Indicate the application layer to start the router node authentication process with the specified target
 * @param vp_nwkGlobal The global NWK information structure
 * @param target The EUI-64 of the EAPOL target to authenticate with. If NULL, the previous EAPOL target will be used.
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_AuthRnStartInd(void* vp_nwkGlobal, const uint8_t* target);

/**
 * Notify the application layer to process incoming EAPOL data
 * @param src The EUI-64 that sent the EAPOL message
 * @param type The EAPOL message type
 * @param authenticator The EUI-64 of the 802.1X authenticator if an EA-IE was present. NULL otherwise
 * @param p_data The EAPOL data
 * @param dataLength The size of the EAPOL data in bytes
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_EapolDataInd(const uint8_t* src, r_mac_eapol_type_t type, const uint8_t* authenticator,
                              const uint8_t* p_data, uint16_t dataLength, void* vp_nwkGlobal);

/**
 * Notify the application layer to reset the Router Node authentication module
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_AuthRnResetInd(void* vp_nwkGlobal);

/**
 * Notify the application layer to delete the specified GTK from the Router Node authentication module
 * @param vp_nwkGlobal The global NWK information structure
 * @param mask A bit mask specifying the GTKs that should be deleted (bit 0 represents GTK0, bit 1 GTK1 etc.)
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_AuthRnDeleteGtksInd(void* vp_nwkGlobal, uint8_t mask);

/**
 * Notify the application layer to clear the GTK validity flag
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the indication was sent to the application layer. Appropriate error code otherwise
 */
r_result_t R_NWK_AuthRnClearGtkValidityInd(void* vp_nwkGlobal);

#if R_MODEM_SERVER && (R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF)
r_result_t R_NWK_LogIndication();
#endif

r_result_t R_NWK_WarningInd(uint8_t status, void* vp_nwkGlobal);    // void* -> r_nwk_cb_t*
r_result_t R_NWK_FatalErrorInd(uint8_t status, void* vp_nwkGlobal); // void* -> r_nwk_cb_t*

// -----------------------------------------------
// function for running API function from own task
// -----------------------------------------------

/**
 * Send an ICMP echo message from within the NWK task
 * @param p_request The ICMP echo request
 * @param p_data Pointer to the ICMP payload
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the ICMP message was successfully sent. Appropriate error code otherwise
 */
r_result_t R_ICMP_SendEcho(const r_icmp_echo_req_t* p_request, const uint8_t* p_data, void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

R_HEADER_UTILS_PRAGMA_PACK_1
typedef struct
{
    r_os_msg_header_t hdr;
    r_udp_data_req_t  udp;
} r_nwk_sendudp_req_t;
R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

/**
 * Send a UDP message from within the NWK task
 * @attention The msg will be freed so it must be a valid OS message created by an appropriate OS alloc function
 * like @ref R_OS_MsgAlloc or @ref R_NWK_OS_AllocMsg
 * @param msg The request for the UDP message
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS, if the UDP message was successfully sent. Appropriate error code otherwise
 */
r_result_t R_NWK_SendUdpMsg(r_nwk_sendudp_req_t* msg, void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

// void* -> r_nwk_cb_t*

// -----------------------------------------------
// function for receiving frame
// -----------------------------------------------
r_result_t R_NWK_hndIPReceive(const uint8_t  eui64[],
                              uint8_t        ipFrame[],
                              uint16_t       ipFrameLen,
                              r_boolean_t    secured,
                              const uint8_t  lqi,
                              const uint8_t* pRelay_macAddr,
                              void*          vp_nwkGlobal);     // void* -> r_nwk_cb_t*

#if R_DEV_TBU_ENABLED
// -----------------------------------------------
// function for frame subscription
// -----------------------------------------------
r_result_t R_NWK_DataSubscription(const RmMcpsSubscriptionIndicationT* p_macSubscriptionInd, void* vp_nwkGlobal);
#endif

// -----------------------------------------------
// function for ACK RSL indication
// -----------------------------------------------
r_result_t R_NWK_HandleAckRslIndication(const RmMcpsAckRslIndicationT* p_ackRslInd, void* vp_nwkGlobal);

void R_NWK_SuspendIP(void* vp_nwkGlobal);
void R_NWK_ResumeIP(void* vp_nwkGlobal);
int  R_NWK_IsIPSuspended();

#endif /* R_NWK_TASK_H */
