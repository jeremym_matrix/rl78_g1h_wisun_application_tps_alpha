/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
#ifndef __RM_mac_types_H
#define __RM_mac_types_H
/*******************************************************************************
 * file name    : mac_types.h
 * description  : The Generic Types definition of IEEE802.15.4 MAC Sub Layer.
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/* --- --- */
#include "mac_config.h"
#include "r_stdint.h"
#include "r_header_utils.h"

#ifdef __RX
#define __far  _far
#define __near _near
#elif defined(__i386) || defined(__x86_64)
#define __far
#define __near
#endif

/* --- --- */
R_HEADER_UTILS_PRAGMA_PACK_1


/* Wi-SUN FAN spec 20130125-FANWG-FANTPS-1v00, Table 6-5 */
typedef enum
{
    RM_MAC_PAN_ASYNC_START = 0x00,
    RM_MAC_PAN_ASYNC_STOP  = 0x01,
} RmMACPANOperationEnumT;


/* --- --- */
/* The value in 802.15.4 Spec */
/* 802.15.4 spec  6.2.3 Table 16 PHY Enumeration */
typedef enum
{
    RM_PHY_BUSY                  = 0x00,
    RM_PHY_BUSY_RX               = 0x01,
    RM_PHY_BUSY_TX               = 0x02,
    RM_PHY_FORCE_TRX_OFF         = 0x03,
    RM_PHY_IDLE                  = 0x04,
    RM_PHY_INVALID_PARAMETER     = 0x05,
    RM_PHY_RX_ON                 = 0x06,
    RM_PHY_SUCCESS               = 0x07,
    RM_PHY_TRX_OFF               = 0x08,
    RM_PHY_TX_ON                 = 0x09,
    RM_PHY_UNSUPPORTED_ATTRIBUTE = 0x0a,
    RM_PHY_READ_ONLY             = 0x0b,
} RmPHYEnumT;

/* 802.15.4 spec  6.1.17 Table 64 MAC Enumeration */
typedef enum
{
    /* Generic */
    RM_MAC_SUCCESS                 = 0x00,
    RM_MAC_FAILED                  = 0x01,

    /* Internal MAC Status */
    RM_MAC_CMD_PENDING             = 0x80, /* ORIGINAL(Internal Use Only) */
    RM_MAC_CMD_BUSY                = 0x81, /* ORIGINAL(Internal Use Only) */
    RM_MAC_CMD_PURGED              = 0x82, /* ORIGINAL(Internal Use Only) */
    RM_MAC_CMD_RESET_ISSUED        = 0x83, /* ORIGINAL(Internal Use Only) */

    /* PHY Status */
    RM_MAC_BAD_CHANNEL             = 0xC3,
    RM_MAC_TRANSMIT_TIME_OVERFLOW  = 0xC4,
    RM_MAC_UNAVAILABLE_SCHEDULE    = 0xC7,

    /* Duty Cycle Handling */
    RM_MAC_DUTY_CYCLE_REACHED      = 0xD0,

    /* Security Status */
    RM_MAC_COUNTER_ERROR           = 0xdb,
    RM_MAC_IMPROPER_KEY_TYPE       = 0xdc,
    RM_MAC_IMPROPER_SECURITY_LEVEL = 0xdd,
    RM_MAC_UNSUPPORTED_LEGACY      = 0xde,
    RM_MAC_UNSUPPORTED_SECURITY    = 0xdf,

    /* MAC Status */
    RM_MAC_CHANNEL_ACCESS_FAILURE  = 0xe1,
    RM_MAC_SECURITY_ERROR          = 0xe4,
    RM_MAC_FRAME_TOO_LONG          = 0xe5,
    RM_MAC_INVALID_HANDLE          = 0xe7,
    RM_MAC_INVALID_PARAMETER       = 0xe8,
    RM_MAC_NO_ACK                  = 0xe9,
    RM_MAC_NO_BEACON               = 0xea,
    RM_MAC_NO_DATA                 = 0xeb,
    RM_MAC_NO_SHORT_ADDRESS        = 0xec,
    RM_MAC_OUT_OF_CAP              = 0xed,
    RM_MAC_PAN_ID_CONFLICT         = 0xee,
    RM_MAC_TX_ACTIVE               = 0xf2,
    RM_MAC_UNAVAILABLE_KEY         = 0xf3,
    RM_MAC_UNSUPPORTED_ATTRIBUTE   = 0xf4,
    RM_MAC_INVALID_ADDRESS         = 0xf5,
    RM_MAC_INVALID_INDEX           = 0xf9,
    RM_MAC_LIMIT_REACHED           = 0xfa,
    RM_MAC_READ_ONLY               = 0xfb,
    RM_MAC_SCAN_IN_PROGRESS        = 0xfc,
} RmMACEnumT;

/* 802.15.4 spec 6.4.2 Table 19 PHY PIB attributes and 7.4.2 Table 71 MAC PIB attributes */
typedef enum
{
    /* --- PHY attributes --- */
#ifdef R_HYBRID_PLC_RF
    RM_phyCurrentChannel           = 0x021F,   //!< The channel number as defined in cause 10.1.2.8 of [IEEE 802.15.4v] to be used for channel scanning and data transmission.
#else
    RM_phyCurrentChannel           = 0x00,     //!< The channel number as defined in cause 10.1.2.8 of [IEEE 802.15.4v] to be used for channel scanning and data transmission.
#endif
    RM_phyChannelsSupported        = 0x01,     //!< Attribute not used in G3 hybrid
    RM_phyTransmitPower            = 0x02,
    RM_phyRssiOutputOffset         = 0x06,
    RM_phyCcaBandwidth             = 0x07,
    RM_phyCcaDuration              = 0x08,
    RM_phyFSKPreambleLength        = 0x09,
    RM_phyMRFSKSFD                 = 0x0a,    //!< Attribute not used in G3 hybrid
#ifdef R_HYBRID_PLC_RF
    RM_phyFSKOpeMode               = 0x021E,  //!< The RF operating mode as defined in clause 20.3 of [IEEE 802.15.4v]
#else
    RM_phyFSKOpeMode               = 0x0b,    //!< The RF operating mode as defined in clause 20.3 of [IEEE 802.15.4v]
#endif
    RM_phyFSKScramblePsdu          = 0x0c,    //!< Attribute not used in G3 hybrid
    RM_phyFCSLength                = 0x0d,    //!< MAC internal use only
    RM_phyFskFecRxEna              = 0x0e,    //!< Attribute not used in G3 hybrid
    RM_phyFskFecTxEna              = 0x0f,    //!< Attribute not used in G3 hybrid
    RM_phyFskFecScheme             = 0x10,    //!< MAC internal use only
    RM_phyAckReplyTime             = 0x11,    //!< Attribute not used in G3 hybrid
    RM_phyAntennaSwitchEna         = 0x13,
    RM_phyAntennaDiversityRxEna    = 0x14,    //!< Attribute not used in G3 hybrid
    RM_phyAntennaSelectTx          = 0x15,
    RM_phyLvlfltrVth               = 0x18,    //!< Attribute not used in G3 hybrid
    RM_phyRegulatoryMode           = 0x1a,
    RM_phyAgcWaitGainOffset        = 0x1b,
    RM_phyCcaVth                   = 0x1c,
    RM_phyCcaVthOffset             = 0x1d,
    RM_phyAntennaSwitchEnaTiming   = 0x1e,
    RM_phyGpio0Setting             = 0x1f,
    RM_phyGpio3Setting             = 0x20,    //!< Attribute not used in G3 hybrid
    RM_phyDataRate                 = 0x21,
    RM_phyChannelConversion        = 0x22,    //!< MAC internal use only
    RM_phyTxDelayMs                = 0x23,    //!< Attribute not used in G3 hybrid
    RM_phyRmodeTonMax              = 0x24,
    RM_phyRmodeToffMin             = 0x25,
    RM_phyRmodeTcumSmpPeriod       = 0x26,    //!< Mapped to RM_macDutyCyclePeriod_RF in G3 hybrid
    RM_phyRmodeTcumLimit           = 0x27,    //!< Not used in G3 hybrid, macDutyCycleLimit_RF used instead
    RM_phyRmodeTcum                = 0x28,    //!< Not used in G3 hybrid, macDutyCycleUsage_RF used instead
    RM_phyCcaWithAck               = 0x29,    //!< Attribute not used in G3 hybrid
    RM_phyFrequency                = 0x2a,    //!< Attribute not used in G3 hybrid
    RM_phyFrequencyOffset          = 0x2b,    //!< Attribute not used in G3 hybrid
#ifndef R_HYBRID_PLC_RF
    RM_phyWiSunPhyConfig           = 0x2c,
#endif

#if R_PHY_TYPE_CWX_M
    RM_phyFskFCSLength             = 0x2d,
    RM_phyOperatingMode            = 0x2e,
    RM_phyRxModulation             = 0x2f,
    RM_phyCcaModulation            = 0x30,
    RM_phyTxModulation             = 0x31,
    RM_phyFskCcaVth                = 0x32,
    RM_phyOfdmCcaVth               = 0x33,
    RM_phyFskFecRxAutoEna          = 0x34,
    RM_phyFskCcaVthOffset          = 0x35,
    RM_phyFskCcaDuration           = 0x36,
    RM_phyOfdmCcaDuration          = 0x37,
    RM_phyMDRNewModeParameters     = 0x38,
    RM_phyMDRNewModeSwitchBank     = 0x39,
#endif

    /* --- AddressFilter --- */
    RM_phyChannelsSupportedPage    = 0x03,    //!< Attribute not used in G3 hybrid
#ifndef R_HYBRID_PLC_RF
    RM_phyFreqBandId               = 0x04,
#endif
    RM_macPromiscuousMode          = 0x51,

    /* Addresses */
    RM_macPANId                    = 0x50,
    RM_macShortAddress             = 0x53,
#ifdef R_HYBRID_PLC_RF
    RM_macExtendedAddress          = 0x0808,   //!< The 64-bit address of the device.
#else
    RM_macExtendedAddress          = 0x80,     //!< The 64-bit address of the device.
#endif

    RM_macBorderRouter             = 0x3e,     //!< Attribute not used in G3 hybrid

    /* --- MAC Security --- */
    RM_macSecurityEnabled          = 0x5D,     //!< Security enabled
    RM_macSecSetKey                = 0x60,     //!< Attribute not used in G3 hybrid
    RM_macSecDeleteKey             = 0x61,
    RM_macFrameCountIndInterval    = 0x62,
    RM_macKeyTable                 = 0x71,

    /* --- PowerMode --- */
    RM_macLowPowerMode             = 0x5f,     //!< Attribute not used in G3 hybrid

    /* --- TX --- */
#ifdef R_HYBRID_PLC_RF
    RM_macMaxCSMABackoffs          = 0x0202,   //!< Maximum number of back-off attempts
    RM_macMinBE                    = 0x0204,   //!< Minimum value of back-off exponent
    RM_macMaxBE                    = 0x0201,   //!< Maximum value of back-off exponent.
    RM_macDSN                      = 0x0200,   //!< Data frame sequence number
    RM_macMaxFrameRetries          = 0x0203,   //!< Maximum number of retransmission
#else
    RM_macMaxCSMABackoffs          = 0x4e,     //!< Maximum number of back-off attempts
    RM_macMinBE                    = 0x4f,     //!< Minimum value of back-off exponent
    RM_macMaxBE                    = 0x57,     //!< Maximum value of back-off exponent.
    RM_macDSN                      = 0x4c,     //!< Data frame sequence number
    RM_macMaxFrameRetries          = 0x59,     //!< Maximum number of retransmission
#endif

    /* --- RX --- */
    RM_macRxOnWhenIdle             = 0x52,

    /* --- Metrics --- */
    RM_macMetricsEnabled           = 0x82,       //!< The activation state of the MAC performance metrics collection

    /* --- Performance metrics (IEEE Std 801.15.4-2015 8.4.2.6) --- */
#ifdef R_HYBRID_PLC_RF
    RM_macCounterOctets            = 0x0209,     //!< The size of the MAC metrics counters in octets
    RM_macRetryCount               = 0x020A,     //!< The number of transmitted frames that required exactly one retry before acknowledgement
    RM_macMultipleRetryCount       = 0x020B,     //!< The number of transmitted frames that required more than one retry before acknowledgement
    RM_macTXFailCount              = 0x020C,     //!< The number of transmitted frames that did not result in an acknowledgement after @ref RM_macMaxFrameRetries
    RM_macTXSuccessCount           = 0x020D,     //!< The number of transmitted frames that were acknowledged after the initial Data frame transmission
    RM_macFCSErrorCount            = 0x020E,     //!< The number of frames that were discarded due to an incorrect FCS
    RM_macSecurityFailureCount     = 0x020F,     //!< The number of frames that did not pass the MAC frame security check successfully
    RM_macDuplicateFrameCount      = 0x0210,     //!< The number of frames that contained the same sequence number as the previous frame
    RM_macRXSuccessCount           = 0x0211,     //!< The number of frames that were received correctly
#else
    RM_macCounterOctets            = 0x86,       //!< The size of the MAC metrics counters in octets
    RM_macRetryCount               = 0x87,       //!< The number of transmitted frames that required exactly one retry before acknowledgement
    RM_macMultipleRetryCount       = 0x88,       //!< The number of transmitted frames that required more than one retry before acknowledgement
    RM_macTXFailCount              = 0x89,       //!< The number of transmitted frames that did not result in an acknowledgement after @ref RM_macMaxFrameRetries
    RM_macTXSuccessCount           = 0x8a,       //!< The number of transmitted frames that were acknowledged after the initial Data frame transmission
    RM_macFCSErrorCount            = 0x8b,       //!< The number of frames that were discarded due to an incorrect FCS
    RM_macSecurityFailureCount     = 0x8c,       //!< The number of frames that did not pass the MAC frame security check successfully
    RM_macDuplicateFrameCount      = 0x8d,       //!< The number of frames that contained the same sequence number as the previous frame
    RM_macRXSuccessCount           = 0x8e,       //!< The number of frames that were received correctly
#endif /* ifdef R_HYBRID_PLC_RF */

    /* Schedules */
    RM_macUnicastSchedule          = 0xa0,       //!< The unicast schedule of this device
    RM_macBroadcastSchedule        = 0xa1,       //!< The broadcast schedule of this device

    /* MAC Neighbor Cache */
    RM_macNeighborLock             = 0xa5,       //!< Add/remove a lock for a specified address in the neighbor cache
    RM_macNeighborDump             = 0xa6,       //!< Log the neighbor cache
    RM_macRxFrameCounterFlush      = 0xa7,       //!< Flush the RX frame counter

    RM_macNeighborEntry            = 0xa8,       //!< Neighbor Cache Entry

    /* --- Subscription --- */
    RM_macFrameSubscriptionEnabled = 0xea,  //!< Attribute not used in G3 hybrid

#ifdef R_HYBRID_PLC_RF

    /* Official G3 hybrid attributes. */
    // RM_macMaxBE_RF                     = 0x0201, -> Mapped to RM_macMaxBE
    // RM_macDSN_RF                       = 0x0200, -> Mapped to RM_macDSN
    // M_macMaxCSMABackoffs_RF            = 0x0202, -> Mapped to RM_macMaxCSMABackoffs
    // RM_macMinBE_RF                     = 0x0204, -> Mapped to RM_macMinBE
    // RM_macMaxFrameRetries_RF           = 0x0203, -> Mapped to RM_macMaxFrameRetries
    RM_macTimeStampSupported_RF    = 0x0205,  //!< MAC frame time stamp support enable
    RM_macDeviceTable_RF           = 0x0206,  //!< A table of Device　Descriptor entries
    RM_macFrameCounter_RF          = 0x0207,  //!< The outgoing frame counter for this device
    RM_macDuplicateDetectionTTL_RF = 0x0208,  //!< Time a received tuple [source address + sequence-number] is retained for duplicate frame detection, in seconds
    // RM_macCounterOctets_RF             = 0x0209, -> Mapped to RM_macCounterOctets
    // RM_macRetryCount_RF                = 0x020A, -> Mapped to RM_macRetryCount
    // RM_macMultipleRetryCount_RF        = 0x020B, -> Mapped to RM_macMultipleRetryCount
    // RM_macTxFailCount_RF               = 0x020C, -> Mapped to RM_macTXFailCount
    // RM_macTxSuccessCount_RF            = 0x020D, -> Mapped to RM_macTXSuccessCount
    // RM_macFcsErrorCount_RF             = 0x020E, -> Mapped to RM_macFCSErrorCount
    // RM_macSecurityFailure_RF           = 0x020F, -> Mapped to RM_macSecurityFailureCount
    // RM_macDuplicateFrameCount_RF       = 0x0210, -> Mapped to RM_macDuplicateFrameCount
    // RM_macRxSuccessCount_RF            = 0x0211, -> Mapped to RM_macRXSuccessCount
    // RM_macNackCount_RF                 = 0x0212, Not supported by API, not used in G3 RF MAC
    // RM_macEbrPermitJoining_RF          = 0x0213, Not supported by API, not used in G3 RF MAC
    RM_macEbrFilters_RF                   = 0x0214, //!< Contains which Enhanced Beacon Request command filter field bits should be set.
    // RM_macEbrAttributeList_RF          = 0x0215, Not supported by API, not used in G3 RF MAC
    RM_macBeaconAutoRespond_RF            = 0x0216, //!< When TRUE, device responds to beacon requests and enhanced beacon requests automatically.
    RM_macUseEnhancedBeacon_RF            = 0x0217, //!< When TRUE, in a beacon-enabled PAN the device should use Enhanced Beacons rather than standard beacons.
    // RM_macEbHeaderIeList_RF            = 0x0218, Not supported by API, not used in G3 RF MAC
    // RM_macEbPayloadIeList_RF           = 0x0219, Not supported by API, not used in G3 RF MAC
    // RM_macEbFilteringEnabled_RF        = 0x021A, Not supported by API, not used in G3 RF MAC
    RM_macEbsn_RF                         = 0x021B, //!< BSN used for Enhanced Beacon frames.
    // RM_macEbAutoSa_RF                  = 0x021C, Not supported by API, not used in G3 RF MAC
    // RM_secSecurityLevelList            = TBD,    Not supported by API, not used in G3 RF MAC
    RM_macPOSTable_RF                     = 0x021D, //!< The POS table defined in 3.11.3.
    // RM_macOperatingMode_RF              = 0x021E, -> Mapped to RM_phyFSKOpeMode
    // RM_macChannelNumber_RF              = 0x021F, -> Mapped to RM_phyCurrentChannel
    RM_macDutyCycleUsage_RF               = 0x0220, //!< Current usage of maximum allowed duty cycle in percent over the current sliding-window measurement period
    RM_macDutyCyclePeriod_RF              = 0x0221, //!< Duration of the measurement period in seconds
    RM_macDutyCycleLimit_RF               = 0x0222, //!< Duration of the allowed transmission time in seconds
    RM_macDutyCycleThreshold_RF           = 0x0223, //!< Duty cycle threshold for stopping RF transmissions
    RM_macDisablePHY_RF                   = 0x0224, //!< Disable RF PHY Tx and Rx
    RM_macFrequencyBand_RF                = 0x0225, //!< Current operating frequency band
    RM_macHoppingEnabled_RF               = 0x0226, //!< Frequency hopping mode enabled / disabled
    RM_macPOSTableEntryTTL                = 0x010E, //!< Maximum time to live for an entry in the neighbour table in minutes
    RM_macRCCoord                         = 0x010F, //!< Route cost to coordinator to be used in the beacon payload as RC_COORD
    RM_macBeaconRandomizationWindowLength = 0x0111, //!< Duration time in seconds for beacon randomization

    /* Internal G3 hybrid attributes. */
    RM_macKeyState                        = 0x0807, //!< Indication of whether the Key is valid for each key index of IBAttributeIndex.
    // RM_macExtAddress                   = 0x0808, -> Mapped to RM_macExtendedAddress
    RM_macSoftVersion                     = 0x0809, //!< Software version
    RM_macDeviceTableByShortAddr          = 0x080F, //!< RF-MLME-GET.confirm returns the DeviceTable Entry which is corresponding to given short address by PIBAttributeIndex.
    RM_macPosTableByShortAddr             = 0x0813, //!< RF-MLME-GET.confirm return the POS table Entry which is corresponding to given short address by PIBAttributeIndex.
    RM_macAutoRequest                     = 0x0042, //!< Controls usage of beacon notify indication

    RM_macInvalidPibAttributeId           = 0xffff  //!< Placeholder for the last PIB attribute ID value
#else /* R_HYBRID_PLC_RF */
    RM_macDuplicateDetectionTTL_RF        = 0xeb,   //!< Time a received tuple [source address + sequence-number] is retained for duplicate frame detection, in seconds
    RM_macInvalidPibAttributeId           = 0xff    //!< Placeholder for the last PIB attribute ID value
#endif /* R_HYBRID_PLC_RF */

} RmPIBAtrributeIdentifierEnumT;

#ifdef R_HYBRID_PLC_RF

/* 7.3 MAC command frames  Table 67 - MAC command frames */
typedef enum
{
    RM_AssociationRequest        = 0x01,
    RM_AssociationResponse       = 0x02,
    RM_DiassociationNotification = 0x03,
    RM_DataRequest               = 0x04,
    RM_PANIDConflictNotification = 0x05,
    RM_OrphanNotification        = 0x06,
    RM_BeaconRequest             = 0x07,
    RM_CoordinatorRealignment    = 0x08,
    RM_GTSRequest                = 0x09,
    RM_RITRequest                = 0x20,
} RmCommandFrameIdentifierEnumT;

/* 7.4.1 MAC constants Tablue 70 - MAC Constats plus */
#ifndef RM_aMaxPHYPacketSize
#define RM_aMaxPHYPacketSize       (2047U)
#endif
#define RM_aTurnaroundTime         (12U)
#define RM_aBaseSlotDuration       (60U)
/*
 * RM_aExtendedAddress
 */
#define RM_aMaxBeaconOverhead      (75U)
#define RM_aGTSDescPersistenceTime (4U)
#define aMaxMPDUUnsecuredOverhead  (25U)
#define aMinMPDUOverhead           (9U)
#define aMaxMacSafePayloadSize     (RM_aMaxPHYPacketSize - aMaxMPDUUnsecuredOverhead)
#define aMaxMacPayloadSize         (RM_aMaxPHYPacketSize - aMinMPDUOverhead)
#define RM_aMaxLostBeacons         (4U)
#define RM_aMinCAPLength           (440U)
#define RM_aNumSuperframeSlots     (16U)
#define RM_aMaxBeaconPayloadLength (2)
#define RM_aBaseSuperframeDuration (RM_aBaseSlotDuration * RM_aNumSuperframeSlots)
#endif /* R_HYBRID_PLC_RF */

#define RM_aMaxSIFSFrameSize       (18U)
#define RM_aMinLIFSPeriond         (40U)
#define RM_aMinSIFSPeriod          (12U)
#define RM_aUnitBackoffPeriod      (20U)
#define RM_aMaxChannel             (63U)

// time before we are allowed to expunge an entry in the device table
#ifndef R_MACSEC_MIN_DEVICE_LIFETIME_SECONDS
#define R_MACSEC_MIN_DEVICE_LIFETIME_SECONDS (60 * 60 * 24)
#endif

#ifndef NULL
#define NULL (0)
#endif

typedef int8_t   RmBYTE;
typedef int16_t  RmINT16;
typedef int32_t  RmINT32;
typedef int32_t  RmINT64[2];
typedef uint32_t RmUINT64[2];

typedef uint8_t  RmUBYTE;
typedef uint16_t RmUINT16;
typedef uint32_t RmUINT32;

/* The Variable Types definition (generic type in the MAC) */
/* Basic Type define */
typedef uint8_t  RmOctetT;
typedef uint16_t RmShortT;
typedef uint32_t RmLongT;
typedef uint8_t  RmBooleanT;
#define RM_FALSE (0)
#define RM_TRUE  (1)
typedef uint32_t RmBitmap32T;
typedef uint8_t  RmBitmap8T;

/* Special Number Type define */
typedef uint8_t  RmMacEnumT;
typedef int16_t  RmPhyEnumT;

typedef uint8_t  RmChannelT;
typedef uint8_t  RmEnergyLevelT;
#ifdef R_HYBRID_PLC_RF
typedef uint8_t  RmScanDurationT;
#endif /* R_HYBRID_PLC_RF */
typedef uint8_t  RmMsduHandleT;

typedef uint16_t RmPANIdentifierT;
typedef uint32_t RmTimeT;      /* Full Time (24bit available)*/
typedef uint32_t RmCounterT;
typedef uint16_t RmShortTimeT; /* ShortTime (16bit) */
typedef uint16_t RmFcsT;

#define RM_TIME_MASK    (0xFFFFFFFFUL)
#define RM_TIME_INVALID (0xFFFFFFFFUL)
#define RmIsGreaterThanTime(tim1, tim2) ((((tim2) - (tim1)) & RM_TIME_MASK) > 0xF8000000UL)

/* Address type difine define */
typedef uint8_t  Rm8bitAddressT;
typedef uint16_t RmShortAddressT;
typedef uint32_t RmExtendedAddressT[2];


typedef union
{
    RmExtendedAddressT bit64;
    RmShortAddressT    bit16;
    Rm8bitAddressT     bit8;
} RmDeviceAddressT;

#define RmClearExtendedAddress(address)      ((address)[0] = (address)[1] = 0UL)
#define RmExtendedAddressLow4bytes(address)  ((address)[0])
#define RmExtendedAddressHigh4bytes(address) ((address)[1])
#define RmClearDeviceAddress(x)              RmClearExtendedAddress((x).bit64)

#define RmCopyDeviceAddress(DstAddr, SrcAddr) \
    do { \
        RmExtendedAddressLow4bytes(DstAddr.bit64) = \
            RmExtendedAddressLow4bytes(SrcAddr.bit64); \
        RmExtendedAddressHigh4bytes(DstAddr.bit64) = \
            RmExtendedAddressHigh4bytes(SrcAddr.bit64); \
    } while (0)

#define RmCopyExtendedAddress(DstAddr, SrcAddr) \
    do { \
        RmExtendedAddressLow4bytes(DstAddr) = \
            RmExtendedAddressLow4bytes(SrcAddr); \
        RmExtendedAddressHigh4bytes(DstAddr) = \
            RmExtendedAddressHigh4bytes(SrcAddr); \
    } while (0)

#define RmCompExtendedAddress(DstAddr,  SrcAddr) \
    ((RmExtendedAddressLow4bytes(DstAddr) == \
      RmExtendedAddressLow4bytes(SrcAddr)) && \
     (RmExtendedAddressHigh4bytes(DstAddr) == \
      RmExtendedAddressHigh4bytes(SrcAddr)))

/* MAC SDU(service data unit) Type define */

/* PHY SDU(service data unit) Type define */
#define RM_AddressingMode_No_Address       (0x00U)
#define RM_AddressingMode_8bit_Address     (0x01U)
#define RM_AddressingMode_Short_Address    (0x02U)
#define RM_AddressingMode_Extended_Address (0x03U)

#define RmCompAdddress(addrMode, DstAddr, SrcAddr) \
    (((addrMode == RM_AddressingMode_8bit_Address) && \
      ((DstAddr).bit8 == (SrcAddr).bit8)) || \
     ((addrMode == RM_AddressingMode_Short_Address) && \
      ((DstAddr).bit16 == (SrcAddr).bit16)) || \
     ((addrMode == RM_AddressingMode_Extended_Address) && \
      (RmCompExtendedAddress((DstAddr).bit64, (SrcAddr).bit64))))

#ifdef R_HYBRID_PLC_RF
#define RM_ScanType_enhanced_active_scan (0x07U)
typedef RmOctetT RmBeaconPayloadT[RM_aMaxBeaconPayloadLength];
#define RmBeaconPayloadCopy(dst, src) \
    RmMemcpy(dst, src, sizeof(RmBeaconPayloadT))

#define RM_aMaxIESTBYTES (128)
typedef RmOctetT RmIEsT[RM_aMaxIESTBYTES];
typedef struct
{
    RmOctetT mm_frameControlOptions;
    RmOctetT mm_headerIEListLength;
    uint8_t* mm_headerIEList;
    RmShortT mm_payloadIEListLength;
    uint8_t* mm_payloadIEList;
    RmIEsT   mm_ScanIEList;
} RmEAScanParamT;
#else /* R_HYBRID_PLC_RF */
/* --- Security Parameter --- */
typedef struct
{
    RmUBYTE  KeyIndex;
    RmUBYTE  Key[16];
    RmUINT32 FrameCounter;
} RmSecSetKeyT;

typedef struct
{
    RmUBYTE KeyIndex;
} RmSecDeleteKeyT;
#endif /* R_HYBRID_PLC_RF */

typedef struct
{
    RmBooleanT Secured;
    RmUBYTE    KeyIndex;
} RmSecParamT;

typedef struct
{
    RmDeviceAddressT macAddress;
    RmBooleanT       isLocked;
} RmNeighborLockT;

typedef struct
{
    uint16_t   pan_size;
    uint16_t   routing_cost;
    RmBooleanT isLfn;
} RmNeighborEntryT;

#define RM_LowPowerMode_Idle     (0x00U)
#define RM_LowPowerMode_Lowpower (0x02U)

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

/* warning indication status */
#define RM_TGET_RX_BLF_ERR_WHEN_MCPS_DATA_INDICATION          (0x01U)
#define RM_TGET_RX_BLF_ERR_WHEN_MLME_COMM_STATUS_INDICATION   (0x07U)
#ifdef R_HYBRID_PLC_RF
#define RM_TGET_RX_BLF_ERR_WHEN_MLME_BEACON_NOTIFY_INDICATION (0x06U)
#else
#define RM_TGET_RX_BLF_ERR_WHEN_MLME_WSASYNCFRAME_INDICATION  (0x08U)
#endif /* R_HYBRID_PLC_RF */

/******************************************************************************
 *  Copyright (C) 2014-2016 Renesas Electronics Corporation.
 *****************************************************************************/
#endif // #ifndef __RM_mac_types_H
