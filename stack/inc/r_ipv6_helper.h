/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2022 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/**
 * @file r_ipv6_helper.h
 * @brief API of IPv6 related utility/helper functions
 */

#ifndef R_IPV6_HELPER_H
#define R_IPV6_HELPER_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include <stdint.h>
#include "r_nwk_api_base.h"

/******************************************************************************
   Typedef definitions
******************************************************************************/

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Variable Externs
******************************************************************************/

/******************************************************************************
   Functions Prototypes
******************************************************************************/

/**
 * Derive a link-local IPv6 address from an EUI-64 according to RFC4862 and RFC3513
 * @param eui64 The EUI-64 that should be used to derive the link-local IP address
 * @param[out] ipv6Address The output buffer to store the derived IP address
 */
void R_IPv6_LinkLocalAddressFromEui64(const uint8_t* eui64, uint8_t* ipv6Address);

/**
 * Check if the specified address is a link-local unicast address
 * @param address The IPv6 address to check
 * @return True if the specified address is a link-local unicast address. False otherwise
 */
r_boolean_t R_IPv6_IsLinkLocalUnicast(const uint8_t address[]);

/**
 * Check if the specified address is a global unicast address
 * @param address The IPv6 address to check
 * @return True if the specified address is a global unicast address. False otherwise
 */
r_boolean_t R_IPv6_IsGlobalUnicast(const r_ipv6addr_t* address);

/**
 * Check if the specified address is a unique local address
 * @param address The IPv6 address to check
 * @return True if the specified address is a unique local address. False otherwise
 */
r_boolean_t R_IPv6_IsUniqueLocal(const r_ipv6addr_t* address);

/**
 * Check if the specified IPv6 address is the unspecified address (all bits set to zero)
 * @param address The IPv6 address to check
 * @return True if the specified address is the unspecified address. False otherwise
 */
r_boolean_t R_IPv6_IsUnspecified(const r_ipv6addr_t* address);

/**
 * Check if the specified IPv6 address is a multicast address (within FF00::/8)
 * @param address The IPv6 address to check
 * @return True if the specified address is a multicast address. False otherwise
 */
r_boolean_t R_IPv6_IsMulticast(const r_ipv6addr_t* address);

/**
 * Check if the specified IPv6 address is a Solicited-Node multicast address
 * @param address The IPv6 address to check
 * @return True if the specified IPv6 address is a Solicited-Node multicast address. False otherwise
 */
r_boolean_t R_IPv6_IsSolicitedNodeMulticast(const r_ipv6addr_t* address);

/**
 * Check if the specified IPv6 address uniquely identifies a single node
 * @param address The IPv6 address to check
 * @return True if the specified IPv6 address uniquely identifies a single node. False otherwise
 */
r_boolean_t R_IPv6_UniquelyIdentifiesNode(const r_ipv6addr_t* address);

#endif /* R_IPV6_HELPER_H */
