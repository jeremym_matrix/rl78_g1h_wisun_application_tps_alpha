/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2022 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/**
 * @file r_mac_init.h
 * @brief API of the MAC layer initialization and start functionality
 */

#ifndef R_MAC_INIT_H
#define R_MAC_INIT_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_nwk_api.h"

/******************************************************************************
   Typedef definitions
******************************************************************************/

/******************************************************************************
   Macro definitions
******************************************************************************/

/* Short address related constants */
#define R_MAC_SHORT_ADDRESS (0xFFFEu)  //!< Short address that MAC would need/accept for using extended addressing */

/******************************************************************************
   Variable Externs
******************************************************************************/


/******************************************************************************
   Functions Prototypes
******************************************************************************/

/**
 * Initialize basic PIBs of the MAC layer.
 * @param vp_nwkGlobal The global NWK information structure
 */
void R_MAC_Init(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

#if R_BORDER_ROUTER_ENABLED
r_result_t R_MAC_Start(void*              vp_nwkGlobal,
                       r_nwk_start_req_t* p_request);
#endif

r_result_t R_MAC_Associate(void*             vp_nwkGlobal,
                           r_nwk_join_req_t* p_request);

#endif /* R_MAC_INIT_H */
