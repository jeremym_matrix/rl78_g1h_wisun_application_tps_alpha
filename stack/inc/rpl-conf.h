/*
 * Copyright (c) 2010, Swedish Institute of Computer Science.
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 * 1. Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 * 2. Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 * 3. Neither the name of the Institute nor the names of its contributors
 *    may be used to endorse or promote products derived from this software
 *    without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE INSTITUTE AND CONTRIBUTORS ``AS IS'' AND
 * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED.  IN NO EVENT SHALL THE INSTITUTE OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
 * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
 * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
 * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
 * SUCH DAMAGE.
 *
 * This file is part of the Contiki operating system.
 *
 * \file
 *  Public configuration and API declarations for ContikiRPL.
 * \author
 *  Joakim Eriksson <joakime@sics.se> & Nicolas Tsiftes <nvt@sics.se>
 *
 */

#ifndef RPL_CONF_H
#define RPL_CONF_H

#include "contiki-conf.h"

/*
 * The objective function used by RPL is configurable through the
 * RPL_CONF_OF parameter. This should be defined to be the name of an
 * rpl_of object linked into the system image, e.g., rpl_of0.
 */
#ifdef RPL_CONF_OF
#define RPL_OF RPL_CONF_OF
#else

/* ETX is the default objective function. */
#define RPL_OF rpl_mrhof
#endif /* RPL_CONF_OF */

/*
 * This value decides if this node must stay as a leaf or not
 * as allowed by draft-ietf-roll-rpl-19#section-8.5
 */
#ifdef RPL_CONF_LEAF_ONLY
#define RPL_LEAF_ONLY RPL_CONF_LEAF_ONLY
#else
#define RPL_LEAF_ONLY 0
#endif

/*
 * Maximum of concurent RPL instances.
 */
#ifdef RPL_CONF_MAX_INSTANCES
#define RPL_MAX_INSTANCES RPL_CONF_MAX_INSTANCES
#else
#define RPL_MAX_INSTANCES 1
#endif /* RPL_CONF_MAX_INSTANCES */

/*
 * Maximum number of DAGs within an instance.
 */
#ifdef RPL_CONF_MAX_DAG_PER_INSTANCE
#define RPL_MAX_DAG_PER_INSTANCE RPL_CONF_MAX_DAG_PER_INSTANCE
#else

/* FANTPS: "A FAN MUST support a single RPL instance, containing a single DODAG" */
#define RPL_MAX_DAG_PER_INSTANCE 1
#endif /* RPL_CONF_MAX_DAG_PER_INSTANCE */

/*
 *
 */
#ifndef RPL_CONF_DAO_SPECIFY_DAG
  #if RPL_MAX_DAG_PER_INSTANCE > 1
    #define RPL_DAO_SPECIFY_DAG 1
  #else
    #define RPL_DAO_SPECIFY_DAG 0
  #endif /* RPL_MAX_DAG_PER_INSTANCE > 1 */
#else
  #define RPL_DAO_SPECIFY_DAG   RPL_CONF_DAO_SPECIFY_DAG
#endif /* RPL_CONF_DAO_SPECIFY_DAG */

/*
 * The DIO Trickle timer Imin value is defined as 2 to the power of this value in milliseconds.
 *
 * Large network - 19 (an Imin of approximately 8 minutes)
 * Small network - 15 (an Imin of approximately 32 seconds)
 * see: FANTPS-1v29 Table 6-2a
 */
#ifdef RPL_CONF_DIO_INTERVAL_MIN
#define RPL_DEFAULT_DIO_INTERVAL_MIN RPL_CONF_DIO_INTERVAL_MIN
#else
#define RPL_DEFAULT_DIO_INTERVAL_MIN 15
#endif
extern uint8_t r_rpl_dio_interval_min;

/*
 * The DIO Trickle timer Imax value (in doublings of Imin).
 *
 * Large network - 1 (an Imax of approximately 16 minutes if Imin is 2^19)
 * Small network - 2 (an Imax of approximately 128 seconds if Imin is 2^15))
 * see: FANTPS-1v29 Table 6-2a
 */
#ifdef RPL_CONF_DIO_INTERVAL_DOUBLINGS
#define RPL_DEFAULT_DIO_INTERVAL_DOUBLINGS RPL_CONF_DIO_INTERVAL_DOUBLINGS
#else
#define RPL_DEFAULT_DIO_INTERVAL_DOUBLINGS 2
#endif
extern uint8_t r_rpl_dio_interval_doublings;

/*
 * DIO redundancy. To learn more about this, see RFC 6206.
 *
 * RFC 6550 suggests a default value of 10. It is unclear what the basis
 * of this suggestion is. Network operators might attain more efficient
 * operation by tuning this parameter for specific deployments.
 */
#ifdef RPL_CONF_DIO_REDUNDANCY
#define RPL_DEFAULT_DIO_REDUNDANCY RPL_CONF_DIO_REDUNDANCY
#else
#define RPL_DEFAULT_DIO_REDUNDANCY 0  /* disable suppression */
#endif
extern uint8_t r_rpl_dio_redundancy;

/*
 * Initial metric attributed to a link when the ETX is unknown
 */
#ifndef RPL_CONF_INIT_LINK_METRIC
#define RPL_INIT_LINK_METRIC 2
#else
#define RPL_INIT_LINK_METRIC RPL_CONF_INIT_LINK_METRIC
#endif

/*
 * Default route lifetime unit. This is the granularity of time
 * used in RPL lifetime values, in seconds.
 */
#define RPL_CONF_DEFAULT_LIFETIME_UNIT 60  /* According to Wi-SUN FAN spec version 1v00*/

#ifndef RPL_CONF_DEFAULT_LIFETIME_UNIT
#define RPL_DEFAULT_LIFETIME_UNIT      0xffff
#else
#define RPL_DEFAULT_LIFETIME_UNIT      RPL_CONF_DEFAULT_LIFETIME_UNIT
#endif

/*
 * Default route lifetime as a multiple of the lifetime unit.
 */

#define RPL_CONF_DEFAULT_LIFETIME 120  /* According to Wi-SUN FAN spec version 1v00*/

#ifndef RPL_CONF_DEFAULT_LIFETIME
#define RPL_DEFAULT_LIFETIME      0xff
#else
#define RPL_DEFAULT_LIFETIME      RPL_CONF_DEFAULT_LIFETIME
#endif

/*
 * DAG preference field
 */
#ifdef RPL_CONF_PREFERENCE
#define RPL_PREFERENCE RPL_CONF_PREFERENCE
#else
#define RPL_PREFERENCE 0
#endif

/*
 * Hop-by-hop option
 * This option control the insertion of the RPL Hop-by-Hop extension header
 * into packets originating from this node. Incoming Hop-by-hop extension
 * header are still processed and forwarded.
 */
#ifdef RPL_CONF_INSERT_HBH_OPTION
#define RPL_INSERT_HBH_OPTION RPL_CONF_INSERT_HBH_OPTION
#else
#define RPL_INSERT_HBH_OPTION 1
#endif

/*
 * Interval of DIS transmission
 */
#ifdef  RPL_CONF_DIS_INTERVAL
#define RPL_DIS_INTERVAL RPL_CONF_DIS_INTERVAL
#else
#define RPL_DIS_INTERVAL 60
#endif

/*
 * Added delay of first DIS transmission after boot
 */
#ifdef  RPL_CONF_DIS_START_DELAY
#define RPL_DIS_START_DELAY RPL_CONF_DIS_START_DELAY
#else
#define RPL_DIS_START_DELAY 5
#endif

/*
 * Authentication and PCS flag for DIO option
 */
#ifdef  RPL_CONF_DIS_AUTH_PCS_FLAG
#define RPL_DIS_AUTH_PCS_FLAG RPL_CONF_DIS_AUTH_PCS_FLAG
#else
#define RPL_DIS_AUTH_PCS_FLAG 7  // According to TPS 1v28 update
#endif

/* [FAN1v26]: A FAN node MUST be capable to maintain at least two upward routes to the DAG route and MUST do so when more than one parent is available. */
/* Note: 3 already leads to fragmented DAOs */
#ifndef RPL_CONF_NON_STORING_MAX_PARENTS
#define RPL_CONF_NON_STORING_MAX_PARENTS 2
#elif RPL_CONF_NON_STORING_MAX_PARENTS < 0 || RPL_CONF_NON_STORING_MAX_PARENTS > 254
#error Illegal value for RPL_CONF_NON_STORING_MAX_PARENTS
#endif

/* This is used for loop detection */
#ifndef RPL_CONF_NON_STORING_MAX_HOPS
#define RPL_CONF_NON_STORING_MAX_HOPS 24
#elif RPL_CONF_NON_STORING_MAX_HOPS < 0 || RPL_CONF_NON_STORING_MAX_HOPS > 0xffff
#error Illegal value for RPL_CONF_NON_STORING_MAX_HOPS
#endif

/* This is the maximum number of DAOs in flight for LFNs */
#if (R_WISUN_FAN_VERSION >= 110)
#ifndef RPL_CONF_MAX_DAO_CONTROL
#define RPL_CONF_MAX_DAO_CONTROL 5
#elif RPL_CONF_MAX_DAO_CONTROL < 2
#error Illegal value for RPL_CONF_MAX_DAO_CONTROL. Must be at least 2.
#endif
#else
#ifndef RPL_CONF_MAX_DAO_CONTROL
#define RPL_CONF_MAX_DAO_CONTROL 1
#elif RPL_CONF_MAX_DAO_CONTROL != 1
#error Illegal value for RPL_CONF_MAX_DAO_CONTROL. Must be 1.
#endif
#endif

#endif /* RPL_CONF_H */
