/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
#ifndef __RM_mac_mcps_H
#define __RM_mac_mcps_H
/*******************************************************************************
 * file name    : mac_mcps.h
 * description  : The definition for MCPS-SAP
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/* --- --- */
#include "r_stdint.h"
#include "mac_types.h"
#include "mac_msg.h"
#include "r_mac_ie.h"


/******************************************************************************
    Macro Definitions
******************************************************************************/
#define RM_BACKOFF_SLEEP_PERIOD                  (25U)  /* Backoff sleep period in milliseconds */

#define RM_MCPS_IE_WH_BUFFER_SIZE                sizeof(r_ie_wh_t)
#define RM_MCPS_IE_WP_BUFFER_SIZE                sizeof(r_ie_wp_t)

/* The Primitive (request)  length  (MCPS-SAP) */
#define RM_MCPS_DATA_request_length              (sizeof(RmMcpsDataRequestT) - sizeof(RmMsgHdrT))

/* The Primitive Id (confirm)  definition (MCPS-SAP) */
#define RM_MCPS_DATA_confirm_length              (sizeof(RmMcpsDataConfirmT) - sizeof(RmMsgHdrT))
#define RM_MCPS_DATA_indication_length           (sizeof(RmMcpsDataIndicationT) - sizeof(RmMsgHdrT))

#define RM_MCPS_DATA_request_Txo_Ack             (0x01U) /* acknowledged transmission.     */
#if R_PHY_TYPE_CWX_M
#define RM_MCPS_DATA_request_Txo_ModeSwitch      (0x02U) /* mode switch transmission. */
#endif
#define RM_MCPS_DATA_request_Txo_Sec             (0x08U) /* security enabled transmission. */
#define RM_MCPS_DATA_request_Txo_CCA             (0x10U) /* is CCA enable                  */
#define RM_MCPS_DATA_request_Txo_EAckPend        (0x20U) /* Pending Bit set for E-Ack      */

#define RM_MCPS_DATA_request_Fro_PanidSuppressed (0x01U) /* PANID suppressed  */
#define RM_MCPS_DATA_request_Fro_SQNSuppressed   (0x04U) /* Sequence Number suppressed */

/* The message definition (MCPS-SAP dependent type)                     */
/* The Variable Types definition (MCPS-SAP dependent type)              */

R_HEADER_UTILS_PRAGMA_PACK_1

/******************************************************************************
    Type Definitions
******************************************************************************/
#if !R_EDFE_INITIATOR_DISABLED
struct RmEdfePoolS;
#endif

/*
 * Message MCPS-DATA.request format
 */
typedef struct
{
    RmMsgHdrT           mm_Hdr;
    RmOctetT            mm_srcAddrMode;
    RmOctetT            mm_dstAddrMode;
    RmPANIdentifierT    mm_dstPANId;
    RmDeviceAddressT    mm_dstAddr;
#ifdef R_HYBRID_PLC_RF
    RmShortT            mm_msduLength;
#endif
    RmMsduHandleT       mm_msduHandle;
    RmBitmap8T          mm_txOptions;
#if !R_EDFE_INITIATOR_DISABLED
    struct RmEdfePoolS* mm_EdfePool;
#endif
#ifndef R_HYBRID_PLC_RF
    r_ie_wh_t           mm_headerIE;   //!< Header Information Elements
    r_ie_wp_t           mm_payloadIE;  //!< Payload Information Elements
#endif
    RmSecParamT         mm_secParam;
    RmOctetT            mm_frameControlOptions;
#ifdef R_HYBRID_PLC_RF
    RmShortT            mm_headerIELength;
    RmShortT            mm_payloadIELength;
#endif
    RmOctetT            mm_mpPayload[]; //!< Payload of mm_payloadIE.mp
} RmMcpsDataRequestT;

/*
 * Message MCPS-DATA.confirm format
 */
typedef struct
{
    RmMsgHdrT        mm_Hdr;
    RmMsduHandleT    mm_msduHandle;
    RmMacEnumT       mm_status;
#ifndef R_HYBRID_PLC_RF
    RmDeviceAddressT mm_dstAddr;
    RmShortT         mm_NumBackoffs;
    RmUBYTE          mm_transmissions;
    RmUBYTE          mm_acks;
#endif
} RmMcpsDataConfirmT;

#define RmMcpsInitializeDataConfirm(msg) \
    RmMsgInitialize((msg), \
                    RM_Pid_MCPS_DATA_confirm, RM_MCPS_DATA_confirm_length)

/*
 * Message MCPS-DATA.indication format
 */
typedef struct
{
    r_os_msg_header_t mm_OsMsgHdr;
    RmOctetT          mm_srcAddrMode;
    RmPANIdentifierT  mm_srcPANId;
    RmDeviceAddressT  mm_srcAddr;
    RmOctetT          mm_dstAddrMode;
    RmPANIdentifierT  mm_dstPANId;
    RmDeviceAddressT  mm_dstAddr;
#ifdef R_HYBRID_PLC_RF
    RmShortT          mm_msduLength;
#endif
    RmOctetT          mm_mpduLinkQuality;
    RmOctetT          mm_DSN;
    RmSecParamT       mm_secParam;
#ifdef R_HYBRID_PLC_RF
    RmShortT          mm_headerIELength;
    RmShortT          mm_payloadIELength;
#else
    r_ie_wh_t         mm_headerIE;
    r_ie_wp_t         mm_payloadIE;
#endif
    RmOctetT          mm_mpPayload[];   //!< Payload of mm_payloadIE.mp
} RmMcpsDataIndicationT;

/*
 * Message MCPS-EAPOL.indication format
 */
typedef struct
{
    r_os_msg_header_t osMsgHdr;
    r_eui64_t         srcEui64;
    r_ie_wh_t         headerIE;
    r_ie_wp_t         payloadIE;
    RmOctetT          mpPayload[];   //!< Payload of mm_payloadIE.mp
} RmMcpsEapolIndicationT;

/*
 * Message MCPS-ACKRSL.indication format
 */
typedef struct
{
    RmMsgHdrT mm_Hdr;
    RmUBYTE   mm_srcAddr[8];
    RmUBYTE   mm_rsl;
} RmMcpsAckRslIndicationT;


#if R_DEV_TBU_ENABLED
/*
 * Message MCPS-DATA.subscription format
 */
typedef struct
{
    r_os_msg_header_t mm_OsMsgHdr;
    RmDeviceAddressT  mm_srcAddr;
    RmDeviceAddressT  mm_dstAddr;
    RmShortT          mm_frameLength;       // frame length
    RmOctetT          mm_frame[];           // frame Content
} RmMcpsSubscriptionIndicationT;
#endif

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT


#if !R_EDFE_INITIATOR_DISABLED
#define R_MAC_EDFE_POOL_SIZE 4
/*
 * Pool of EDFE messages
 */
typedef struct RmEdfePoolS
{
    RmMcpsDataRequestT* messages[R_MAC_EDFE_POOL_SIZE];
    RmUBYTE             numMessages; //!< Current number of messages in this EDFE pool
} RmEdfePool;
#endif


/******************************************************************************
 *  Copyright (C) 2014-2016 Renesas Electronics Corporation.
 *****************************************************************************/
#endif // #ifndef __RM_mac_mcps_H
