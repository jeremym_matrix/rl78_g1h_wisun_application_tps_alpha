/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* (C) 2014-2015 Renesas Electronics Corporation All rights reserved.
*******************************************************************************/

/******************************************************************************
* File Name     : r_log_internal.h
* Description   : Project-specific configuration of the RLog module
******************************************************************************/
/******************************************************************************
* History       : DD.MM.YYYY Version Description
*               : 30.01.2015 1.00    First Release
*******************************************************************************/

#ifndef R_LOG_CONFIG_H
#define R_LOG_CONFIG_H

/* Disable FreeRTOS task scheduler during logging since it is not thread-safe and scheduling is preemptive */
#include "FreeRTOS.h"
#include "task.h"
#define R_LOG_DISABLE_CONTEXT_SWITCH() vTaskSuspendAll()
#define R_LOG_ENABLE_CONTEXT_SWITCH()  xTaskResumeAll()

#endif /* R_LOG_CONFIG_H */
