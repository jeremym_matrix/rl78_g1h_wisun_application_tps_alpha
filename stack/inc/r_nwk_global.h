/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name    : r_nwk_global.h
 * Version      : 1.00
 * Description  : Header for the module that implements the network task processing
 ******************************************************************************/

/*!
   \file    r_nwk_global.h
   \version 1.0
   \brief   Header for the module that implements the network task processing
 */

#ifndef R_NWK_GLOBAL_H
#define R_NWK_GLOBAL_H

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_nwk_api_base.h"
#include "r_nwk_api.h"
#include "r_nd.h"
#include "r_nd_cache.h"
#include "r_auth_types.h"
#if R_HC_IPHC_STATEFUL_COMPRESSION_ENABLED
#include "r_nd_context_handling.h"
#endif
#include "mac_types.h"

/******************************************************************************
   Typedef definitions
******************************************************************************/
typedef struct
{
    const r_sys_config_t* p_sysConfigTBL;

    /*=== Common settings ===*/
    struct
    {
        /*--- r_nwk_task.c ---*/
        uint8_t      g_ipv6Address[R_IPV6_ADDRESS_LENGTH]; //!< Link-local IPv6 address derived from EUI-64
#if R_BORDER_ROUTER_ENABLED
        r_ipv6addr_t g_ipv6AddressRpl;                     //!< GUA/LUA configured by application (used by RPL)
#endif
        uint8_t      g_eui64[R_MAC_EXTENDED_ADDRESS_LENGTH];
        r_nwk_attr_t g_nwkAttr;

        /*--- r_mac_init.c ---*/
        int8_t       g_networkName[R_NETWORK_NAME_STRING_MAX_BYTES];

        /*--- r_timer.c ---*/
        uint16_t     g_timerEventNtyFailedCnt;                          // (initial) 0u;

        /*--- (other) ---*/
        uint16_t     g_panId;
        uint16_t     g_panSize;
        uint16_t     g_panSizeOffset;
        uint8_t      g_useParent_BS_IE;
        uint8_t      g_fan_tps_version;
        uint8_t      gtkHashesFromBr[R_KEY_NUM][8]; //!< The GTK and LGTK hashes disseminated via [L]GTKHASH-IEs in [LFN] PAN Configuration frames
        uint16_t     g_ipRegLifetime;               //!< Lifetime of our global IP address in our AROs
        uint8_t      g_ipRegRefreshThreshold;       //!< The threshold (in percentage of lifetime) after which a GUA/ULA is re-registered at parent(s)
    } common;

    /*=== Network join and discovery ===*/
    struct
    {
        uint8_t g_panImin;               //!< Discovery Trickle timer minimum interval size (in seconds)
        uint8_t g_panImax;               //!< Discovery Trickle timer maximum interval size (in doublings of Imin)
        uint8_t g_gtkRequestIminMinutes; //!< Group Key handshake Trickle timer Imin value (in minutes)
        uint8_t g_gtkRequestImax;        //!< Group Key handshake Trickle timer Imax value (in doublings of Imin)
        uint8_t g_gtkMaxMismatchMinutes; //!< Max time between a SUP detecting a GTKHASH mismatch and the SUP initiating Msg1 of the auth flow
#if (R_WISUN_FAN_VERSION >= 110)
#if R_LEAF_NODE_ENABLED
        uint8_t lfnLostParentTime;      //!< The number of LFN_BROADCAST_SYNC_PERIODs after which an LFN assumes the parent is lost (if nothing was received)
#endif
        uint8_t lfnBroadcastSyncPeriod; //!< The time (in minutes) between Broadcast Sync messages from an FFN parent.
#endif
    } disc_join;

    /*=== MPL ===*/

    struct
    {
        uint8_t  g_trickleImin;          //!< MPL Trickle timer minimum interval size (in seconds)
        uint8_t  g_trickleImax;          //!< MPL Trickle timer maximum interval size (in doublings of Imin)
        uint8_t  g_trickleK;             //!< The redundancy constant for MPL Data Message transmissions.
        uint8_t  g_trickleExpirations;   //!< MPL Trickle timer expirations, the number of timer expirations until this message is removed
        uint16_t g_seedSetEntryLifetime; //!< The minimum lifetime for an MPL entry in the Seed (in seconds)
    } mpl;

    /*=== Buffer for Tx/Rx of IPv6 frame and uncompressed IPv6 header ===*/
    struct
    {
        /*--- r_nwk_task.c ---*/
        uint8_t* g_ipv6Frame;  ///< buffer with R_MAX_MTU_SIZE bytes
    } ipv6_tx;

    /*=== Fragment (Tx) ===*/
    struct
    {
        /*--- r_frag_tx.c ---*/
        uint16_t    addressOffset; // (initial) 0
        r_boolean_t fragRunning;   // (initial) R_FALSE
        uint16_t    datagramTag;   // (initial) 0
    } fragment_tx;

    /*=== Fragment (Rx) ===*/
    struct
    {
        /*--- r_frag_rx.c ---*/
        r_frag_reassembly_process_table_t* gp_reassemblyProcessTable;
        uint16_t g_reassemblyProcessTableEntries;
        uint32_t g_fragReassemblyTimeout;
    } fragment_rx;

    /*=== Multicast Groups ===*/
    struct
    {
        /*--- r_nwk_task.c ---*/
        r_multicast_group_entry_t* gp_multicastGroupTable;
        uint16_t g_gp_multicastGroupTableEntries;
    } multicast_groups;

#if !R_DEV_DISABLE_AUTH

    /*=== Authentication state cache (pushed into NWK by authentication, which runs in application task) ===*/
    r_nwk_auth_state_cache_t auth;
#endif

    /*=== MAC security ===*/
    struct
    {
        /*--- Wi-Sun specific ---*/
        uint8_t activeGtkIndex;
        uint8_t activeLgtkIndex;
        uint8_t gtkHashes[R_KEY_NUM][8];  //!< The GTK and LGTK hashes currently used for frame security
    } macsec;

    /*=== LowPower(suspend/resume) ===*/
    struct
    {
        r_boolean_t g_isSuspend;
        r_boolean_t g_rxOnWhenIdle;
    } lowpower;

    /*=== ND Cache ===*/
    struct
    {
        /*--- r_nd_cache.c ---*/
        r_nd_addr_reg_entry_t*       g_ipRegCache;     //!< IP neighbor cache based on address registrations (ARO)

        /*--- r_nd_rpl_neighbors.c ---*/
        r_nd_neighbor_cache_entry_t* g_neighborCache;  //!< RPL Neighbor cache for parent candidates
        r_nd_default_route_t         g_defaultRoute;
        r_nd_default_route_t         g_alternateRoute;
    } nd_cache;

#if R_HC_IPHC_STATEFUL_COMPRESSION_ENABLED

    /*=== ND Context table ===*/
    struct
    {
        /*--- r_nd_context_handling.c ---*/
        r_nd_context_table_t g_contextTable[R_ND_MIN_6LP_CID_COUNT];
    } nd_context;
#endif

    r_boolean_t udpIpIndication;  //!< If enabled, IP indications will be generated for all UDP messages (except for those that must be processed by the Wi-SUN stack)

#if R_IP_FRAG_ENABLED

    /*=== IP fragmentation ===*/
    struct
    {
        /*--- r_nwk_task.c ---*/
        uint8_t     g_ipFragBuff[R_NWK_IPFRAG_MAX_SIZE];
        uint16_t    g_ipFragBuffLen;
        r_boolean_t g_ipFragStart;
        uint8_t     g_ipFragDstAddr[R_IPV6_ADDRESS_LENGTH];
        uint8_t     g_ipFragNextHdr;
        uint32_t    g_ipFragRcvTimeout;
    } ipfrag;
#endif

} r_nwk_cb_t;

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Variable Externs
******************************************************************************/

/******************************************************************************
   Functions Prototypes
******************************************************************************/
r_result_t             R_NWKDual_ClearNwkGlobal();
r_nwk_cb_t*            R_NWKDual_GetNwkGlobal();
r_nwk_config_params_t* R_NWKDual_GetNwkConfigParams();


#endif /* R_NWK_GLOBAL_H */
