/**
   @file
   @version   1.0
   @brief     This file contains various utility macros and macros to abstract from different compilers
   @attention This file may not be included in other header files (see r_header_utils.h for its companion)
   @details   This file contains only macros (no global symbols) to allow having short names without a prefix.
              Additionally, the dependencies for the function wrapper macros are intentionally not #included
              by this file, but must be #included by the user to limit the overhead to what is actually used.
 */

#ifndef R_IMPL_UTILS_H
#define R_IMPL_UTILS_H

#include <string.h>
#include "r_mem_tools.h"

#define ARRAY_SIZE(array) (sizeof(array) / sizeof((array)[0]))

#undef MIN
#undef MAX
#define MIN(x, y)               ((x) <= (y) ? (x) : (y))
#define MAX(x, y)               ((x) >= (y) ? (x) : (y))

#define BETWEEN(x, a, b)        ((x) >= (a) && (x) < (b))

// bit mask utilities
#define BIT(bit)                (1u << (unsigned)(bit))
#define BIT_SET(mask, bit)      mask |= BIT(bit)
#define BIT_CLEAR(mask, bit)    mask &= ~BIT(bit)
#define BIT_IS_SET(mask, bit)   (mask & BIT(bit))
#define BIT_IS_CLEAR(mask, bit) (!BIT_IS_SET(mask, bit))

// padding utilities
#define BUCKETS(n, granularity) (((n) + (granularity) - 1) / (granularity))
#define ELEMENTS(size, type)    BUCKETS(size, sizeof(type))
#define ROUNDUP(n, granularity) (BUCKETS(n, granularity) * (granularity))
#define PADDED(size, type)      ROUNDUP(size, sizeof(type))
#define NBYTES(bits)            BUCKETS(bits, 8)

// Convert little-endian to host byte order (16 bit)
#define LETOHSA(buf)            ((uint16_t)((buf)[0] | ((uint16_t)((buf)[1]) << 8)))


#define MEMEQUAL(s1, s2, n)     (R_memcmp(s1, s2, n) == 0)
#define MEMDIFFER(s1, s2, n)    (R_memcmp(s1, s2, n) != 0)
#define MEMISZERO(s, n)         (R_MemCmpZero(s, n) == 0)
#define MEMISNOTZERO(s, n)      (R_MemCmpZero(s, n) != 0)

// mem* functions to work on structures with automatic use of sizeof
#define MEMSET_S(s, c)          R_memset(s, c, sizeof(*(s)))
#define MEMZERO_S(s)            R_memset(s, 0, sizeof(*(s)))
#define MEMCPY_S(s1, s2)        R_memcpy(s1, s2, sizeof(*(s1)))
#define MEMMOVE_S(s1, s2)       memmove(s1, s2, sizeof(*(s1)))
#define MEMCMP_S(s1, s2)        R_memcmp(s1, s2, sizeof(*(s1)))
#define MEMEQUAL_S(s1, s2)      (R_memcmp(s1, s2, sizeof(*(s1))) == 0)
#define MEMDIFFER_S(s1, s2)     (R_memcmp(s1, s2, sizeof(*(s1))) != 0)
#define MEMISZERO_S(s)          (R_MemCmpZero(s, sizeof(*(s))) == 0)
#define MEMISNOTZERO_S(s)       (R_MemCmpZero(s, sizeof(*(s))) != 0)

// mem* functions to work on arrays with automatic use of sizeof
#define MEMSET_A(a, c)          R_memset(a, c, sizeof(a))
#define MEMZERO_A(a)            R_memset(a, 0, sizeof(a))
#define MEMCPY_A(a1, a2)        R_memcpy(a1, a2, sizeof(a1))
#define MEMMOVE_A(a1, a2)       memmove(a1, a2, sizeof(a1))
#define MEMCMP_A(a1, a2)        R_memcmp(a1, a2, sizeof(a1))
#define MEMEQUAL_A(a1, a2)      (R_memcmp(a1, a2, sizeof(a1)) == 0)
#define MEMDIFFER_A(a1, a2)     (R_memcmp(a1, a2, sizeof(a1)) != 0)
#define MEMISZERO_A(a)          (R_MemCmpZero(a, sizeof(a)) == 0)
#define MEMISNOTZERO_A(a)       (R_MemCmpZero(a, sizeof(a)) != 0)


// Mark a function parameter as unused to suppress warning (only usable for function definitions)
#ifdef UNUSEDP
// nothing to do
#elif defined(__GNUC__)
// includes clang
#define UNUSEDP(x) UNUSEDP_##x __attribute__ ((__unused__))
#elif defined(__LCLINT__)
#define UNUSEDP(x) /*@unused@*/ x
#elif defined(__cplusplus)
#define UNUSEDP(x)
#else
#define UNUSEDP(x) x
#endif

// Mark a symbol (variable or function) as unused to suppress warning (only usable within functions)
#define UNUSED_SYM(a)            (void)a
#define UNUSED_SYMS2(a, b)       do { UNUSED_SYM(a); UNUSED_SYM(b); } while (0)
#define UNUSED_SYMS3(a, b, c)    do { UNUSED_SYM(a); UNUSED_SYM(b); UNUSED_SYM(c); } while (0)
#define UNUSED_SYMS4(a, b, c, d) do { UNUSED_SYM(a); UNUSED_SYM(b); UNUSED_SYM(c); UNUSED_SYM(d);} while (0)


// Important note: MESSAGE must be a valid C identifier to work on pre-C11 compilers
#if __STDC_VERSION__ >= 201112L || __GNUC__ >= 5 || __clang__ || HAVE_STATIC_ASSERT
#define STATIC_ASSERT(COND, MESSAGE) _Static_assert(COND, #MESSAGE)
#ifndef HAVE_STATIC_ASSERT
#define HAVE_STATIC_ASSERT 1
#endif
#else
#define STATIC_ASSERT(COND, MESSAGE) typedef char static_assertion_## MESSAGE[2*(!!(COND)) - 1]
#endif


// Suppress warning: comparison of unsigned expression >= 0 is always true
#if defined(PRAGMA_WARNING_CMP_UINT_WITH_ZERO_SUPPRESS) && defined(PRAGMA_WARNING_CMP_UINT_WITH_ZERO_RESET)
// use existing
#elif defined(__GNUC__)
#define PRAGMA_WARNING_CMP_UINT_WITH_ZERO_SUPPRESS _Pragma("GCC diagnostic ignored \"-Wtype-limits\"")
#define PRAGMA_WARNING_CMP_UINT_WITH_ZERO_RESET
#elif defined(__CCRX__)
#define PRAGMA_WARNING_CMP_UINT_WITH_ZERO_SUPPRESS _Pragma("diag_suppress=186")
#define PRAGMA_WARNING_CMP_UINT_WITH_ZERO_RESET    _Pragma("diag_default=186")
#elif defined(__ICCRL78__)
#define PRAGMA_WARNING_CMP_UINT_WITH_ZERO_SUPPRESS _Pragma("diag_suppress=Pe186")
#define PRAGMA_WARNING_CMP_UINT_WITH_ZERO_RESET    _Pragma("diag_default=Pe186")
#else
#define PRAGMA_WARNING_CMP_UINT_WITH_ZERO_SUPPRESS
#define PRAGMA_WARNING_CMP_UINT_WITH_ZERO_RESET
#endif


// from http://www.inwap.com/pdp10/hbaker/hakmem/hacks.html#item167
/// @return the number of 1-bits in the lower nibble of x
#define POPCOUNT_NIBBLE(x) (((uint32_t)(0x08040201u * ((x) & 0x0fu)) & 0x11111111u) % 0x0fu)
/// @return the number of 1-bits in the byte x
#define POPCOUNT_BYTE(x)   (POPCOUNT_NIBBLE(x) + POPCOUNT_NIBBLE((x) >> 4u))


#endif /* R_IMPL_UTILS_H */
