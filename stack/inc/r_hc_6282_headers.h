/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2012 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_hc_6282_headers.h
 * Version     : 1.0
 * Description : Header file for the header compression packing and unpacking functionality
 ******************************************************************************/

/*!
   \file      r_hc_6282_headers.h
   \version   1.0
   \brief     Header file for the header compression packing and unpacking functionality
 */

#ifndef R_HC_6282_HEADERS_H
#define R_HC_6282_HEADERS_H

/******************************************************************************
   Macro definitions
******************************************************************************/

/******************************************************************************
   Typedef definitions
******************************************************************************/

/*!
    \enum r_hc_traffic_class_field_t
    \brief IPHC traffic class field values enumeration
 */
typedef enum
{
    R_HC_IPHC_TF_ECN_DSCP_FLOW = 0, //!< ECN, DSCP and flow label are present and carried in line
    R_HC_IPHC_TF_ECN_FLOW      = 1, //!< ECN and flow label are present and carried in line, DSCP is elided
    R_HC_IPHC_TF_ECN_DSCP      = 2, //!< ECN and DSCP are present and carried in line, flow label is elided
    R_HC_IPHC_TF_NONE          = 3  //!< ECN, DSCP and flow label are elided

} r_hc_traffic_class_field_t;

/*!
    \enum r_hc_hop_limit_field_t
    \brief IPHC hop limit field values enumeration
 */
typedef enum
{
    R_HC_IPHC_HL_IN_LINE   = 0, //!< The Hop Limit field is carried in-line
    R_HC_IPHC_HL_COMPR_1   = 1, //!< The Hop Limit field is compressed and the hop limit is 1
    R_HC_IPHC_HL_COMPR_64  = 2, //!< The Hop Limit field is compressed and the hop limit is 64
    R_HC_IPHC_HL_COMPR_255 = 3  //!< The Hop Limit field is compressed and the hop limit is 255

} r_hc_hop_limit_field_t;

/*!
    \enum r_hc_next_header_field_t
    \brief IPv6 next header field enumeration
 */
typedef enum
{
    R_HC_NEXT_HDR_INLINE     = 0x00, //!< Full 8 bits for next header are carried in-line
    R_HC_NEXT_HDR_LOWPAN_NHC = 0x01  //!< The next header field is compressed and enconded using LOWPAN_NHC

} r_hc_next_header_field_t;

/*!
    \enum r_hc_address_compression_t
    \brief IPHC address compression
 */
typedef enum
{
    R_HC_IPHC_STATELESS_COMPRESSION = 0, //!< Address compression uses stateless compression
    R_HC_IPHC_STATEFUL_COMPRESSION  = 1  //!< Address compression uses stateful compression

} r_hc_address_compression_t;

/*!
    \enum r_hc_address_mode_t
    \brief IPHC source address mode
 */
typedef enum
{
    R_HC_IPHC_ADDRESS_MODE_0 = 0, //!< 128 bits. The full address is carried in-line.
    R_HC_IPHC_ADDRESS_MODE_1 = 1, //!< 64 bits. The last 64 bits are carried in-line.
    R_HC_IPHC_ADDRESS_MODE_2 = 2, //!< 16 bits. The last 16 bits are carried in-line.
    R_HC_IPHC_ADDRESS_MODE_3 = 3  //!< 0 bits. The address is fully elided.

} r_hc_address_mode_t;

/*!
    \struct r_iphc_base_encoding_t
    \brief IPv6 base encoding structure
 */
typedef struct
{
    r_boolean_t                cid;  //!< Context identifier extension
    r_hc_traffic_class_field_t tf;   //!< Traffic class, flow label
    r_hc_next_header_field_t   nh;   //!< Next header
    r_hc_hop_limit_field_t     hlim; //!< Hop limit
    r_hc_address_compression_t sac;  //!< Source address compression
    r_hc_address_mode_t        sam;  //!< Source adderss mode
    r_boolean_t                m;    //!< Multicast compression
    r_hc_address_compression_t dac;  //!< Destination address compression
    r_hc_address_mode_t        dam;  //!< Destination address mode
    uint8_t srcPrefixLength;         //!< Source address prefix length
    uint8_t dstPrefixLength;         //!< Destination address prefix length

} r_iphc_base_encoding_t;

/*!
    \struct r_iphc_context_id_ext_t
    \brief Context identifier extension structure
 */
typedef struct
{
    uint8_t sci;  //!< Source Context Identifier
    uint8_t dci;  //!< Destination Context Identifier

} r_iphc_context_id_ext_t;

/*!
    \enum r_iphc_udp_port_compression_t
    \brief Type of UDP port compression
 */
typedef enum
{
    R_HC_UDP_PORT_COMPRESSION_16_16 = 0, //!< All 16 bits for both source port and destination port are carried in-line
    R_HC_UDP_PORT_COMPRESSION_16_8  = 1, //!< 16 bits of source port and 8 bits of destination port are carried in-line
    R_HC_UDP_PORT_COMPRESSION_8_16  = 2, //!< 8 bits of source port and 16 bits of destination port are carried in-line
    R_HC_UDP_PORT_COMPRESSION_4_4   = 3  //!< 4 bits of source port and 4 bits of destination port are carried in-line

} r_iphc_udp_port_compression_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn r_hc_result_t R_HC_PackCompressedUdpHeader(const r_udp_hdr_t* udpHdr,
                                           const r_boolean_t cFlag,
                                           const r_iphc_udp_port_compression_t pFlag,
                                           uint8_t output[],
                                           uint16_t* writtenBytes,
                                           uint16_t* sumWrittenBytes,
                                           const uint16_t compressedHdrMaxSize);
    \brief The function packs the compressed UDP header
    \param[in] udpHdr Pointer to a structure describing the UDP header
    \param[in] cFlag The C flag of the packed UDP header
    \param[in] pFlag The P flag of the packed UDP header
    \param[in] output Address where the packed header will be written to
    \param[in] writtenBytes Pointer to a location where the current number of written bytes will be written to
    \param[in] sumWrittenBytes Pointer to a location where the total number of written bytes will be written to
    \param[in] compressedHdrMaxSize Maximum size of the compressed header
        \return If successfull R_HC_RESULT_SUCCESS, else R_HC_RESULT_ILLEGAL_NULL_POINTER, R_HC_RESULT_BUFFER_ERROR or R_HC_RESULT_FAILED
 */
r_hc_result_t R_HC_PackCompressedUdpHeader(const r_udp_hdr_t*                  udpHdr,
                                           const r_boolean_t                   cFlag,
                                           const r_iphc_udp_port_compression_t pFlag,
                                           uint8_t                             output[],
                                           uint16_t*                           writtenBytes,
                                           uint16_t*                           sumWrittenBytes,
                                           const uint16_t                      compressedHdrMaxSize);

/*!
    \fn r_hc_result_t R_HC_PackBaseIphcHeader(const r_iphc_base_encoding_t* baseHdr,
                                      uint8_t output[],
                                      uint16_t* sumWrittenBytes,
                                      const uint16_t compressedHdrMaxSize);
    \brief This function packs the base iphc header
    \param[in] baseHdr Pointer to a structure describing the base IPHC header
    \param[in] output Address where the packed header will be written to
    \param[in] sumWrittenBytes Pointer to a location where the total number of written bytes will be written to
    \param[in] compressedHdrMaxSize Maximum size of the compressed header
    \return If successfull R_HC_RESULT_SUCCESS, else R_HC_RESULT_ILLEGAL_NULL_POINTER or R_HC_RESULT_BUFFER_ERROR
 */
r_hc_result_t R_HC_PackBaseIphcHeader(const r_iphc_base_encoding_t* baseHdr,
                                      uint8_t                       output[],
                                      uint16_t*                     sumWrittenBytes,
                                      const uint16_t                compressedHdrMaxSize);

/*!
    \fn r_hc_result_t R_HC_UnpackBaseIphcHeader(r_iphc_base_encoding_t* baseHdr,
                                        const uint8_t input[],
                                        uint16_t* sumReadBytes);
    \brief This function unpacks the base iphc header
    \param[in] baseHdr Pointer to a structure where the base IPHC header will be unpacked
    \param[in] input Address containing the packed header
    \param[in] sumReadBytes Pointer to a location where the total number of read bytes will be written to
        \return If successfull R_HC_RESULT_SUCCESS, else R_HC_RESULT_ILLEGAL_NULL_POINTER
 */
r_hc_result_t R_HC_UnpackBaseIphcHeader(r_iphc_base_encoding_t* baseHdr,
                                        const uint8_t           input[],
                                        uint16_t*               sumReadBytes);

/*!
    \fn r_hc_result_t R_HC_PackContextIdExtHeader(const r_boolean_t cid,
                                          const r_iphc_context_id_ext_t* contextIdExt,
                                          uint8_t output[],
                                          uint16_t* sumWrittenBytes,
                                          const uint16_t compressedHdrMaxSize);
    \brief This function packs the context id extension header
    \param[in] cid R_TRUE if the CIE header is present, else R_FALSE
    \param[in] contextIdExt Pointer to a structure describing the CIE header
    \param[in] output Address where the packed header will be written to
    \param[in] sumWrittenBytes Pointer to a location where the total number of written bytes will be written to
    \param[in] compressedHdrMaxSize Maximum size of the compressed header
    \return If successfull R_HC_RESULT_SUCCESS, else R_HC_RESULT_ILLEGAL_NULL_POINTER or R_HC_RESULT_BUFFER_ERROR
 */
r_hc_result_t R_HC_PackContextIdExtHeader(const r_boolean_t              cid,
                                          const r_iphc_context_id_ext_t* contextIdExt,
                                          uint8_t                        output[],
                                          uint16_t*                      sumWrittenBytes,
                                          const uint16_t                 compressedHdrMaxSize);

/*!
    \fn void R_HC_UnpackContextIdExtHeader(const uint8_t input,
                                            r_iphc_context_id_ext_t* contextIdExt,
                                            uint16_t* sumReadBytes);
    \brief This function extracts the context IDs from the compressed header field.
    \param[in] input Single byte containing IDs for SRC and DST context ID (4 bit each)
    \param[out] contextIdExt Structure containing the decoded IDs.
    \param[out] sumReadBytes Counter that will be incremented by the number of bytes read from input.
    \return None
 */
r_hc_result_t R_HC_UnpackContextIdExtHeader(const uint8_t            input,
                                            r_iphc_context_id_ext_t* contextIdExt,
                                            uint16_t*                sumReadBytes);

/*!
    \fn r_hc_result_t R_HC_PackTrafficClassFlowLabelHeader(const r_hc_traffic_class_field_t tf,
                                                   const r_ipv6_traffic_class_t* trafficClass,
                                                   const uint32_t flowLabel,
                                                   uint8_t output[],
                                                   uint16_t* sumWrittenBytes,
                                                   const uint16_t compressedHdrMaxSize);
    \brief This function packs the traffic class and flow label headers
    \param[in] tf TF field
    \param[in] trafficClass Pointer to a structure describing the traffic class header
    \param[in] flowLabel Flow label field
    \param[in] output Address where the packed header will be written to
    \param[in] sumWrittenBytes Pointer to a location where the total number of written bytes will be written to
    \param[in] compressedHdrMaxSize Maximum size of the compressed header
    \return If successfull R_HC_RESULT_SUCCESS, else R_HC_RESULT_ILLEGAL_NULL_POINTER, R_HC_RESULT_BUFFER_ERROR or R_HC_RESULT_FAILED
 */
r_hc_result_t R_HC_PackTrafficClassFlowLabelHeader(const r_hc_traffic_class_field_t tf,
                                                   const r_ipv6_traffic_class_t*    trafficClass,
                                                   const uint32_t                   flowLabel,
                                                   uint8_t                          output[],
                                                   uint16_t*                        sumWrittenBytes,
                                                   const uint16_t                   compressedHdrMaxSize);

/*!
    \fn r_hc_result_t R_HC_UnpackTrafficClassFlowLabelHeader(const uint8_t input[],
                                                             const r_hc_traffic_class_field_t tfField,
                                                             r_ipv6_traffic_class_t* trafficClass,
                                                             uint32_t* flowLabel,
                                                             uint16_t* sumReadBytes);
    \brief This function extracts the traffic class and flow label from the compressed header field.
    \param[in] input Input vector containing in-line information provided for flow label uncompression.
    \param[in] tfField Indicates the used procedure for flow label compression.
    \param[out] trafficClass Structure containing the uncompressed traffic class.
    \param[out] flowLabel Pointer where the uncompressed flow label will be written.
    \param[out] sumReadBytes Counter that will be incremented by the number of bytes read from input.
    \return R_HC_RESULT_SUCCESS, R_HC_RESULT_BAD_INPUT_ARGUMENTS or HC_RESULT_ILLEGAL_NULL_POINTER
 */
r_hc_result_t R_HC_UnpackTrafficClassFlowLabelHeader(const uint8_t                    input[],
                                                     const r_hc_traffic_class_field_t tfField,
                                                     r_ipv6_traffic_class_t*          trafficClass,
                                                     uint32_t*                        flowLabel,
                                                     uint16_t*                        sumReadBytes);

/*!
    \fn r_hc_result_t R_HC_PackHlimField(const r_hc_hop_limit_field_t hlimFlag,
                                 const uint8_t hlimValue,
                                 uint8_t output[],
                                 uint16_t* sumWrittenBytes,
                                 const uint16_t compressedHdrMaxSize);
    \brief This function packs the hlim field
    \param[in] hlimFlag Flag indicating if HLIM is in line or not
    \param[in] hlimValue The HLIM value
    \param[in] output Address where the packed header will be written to
    \param[in] sumWrittenBytes Pointer to a location where the total number of written bytes will be written to
    \param[in] compressedHdrMaxSize Maximum size of the compressed header
    \return If successfull R_HC_RESULT_SUCCESS, else R_HC_RESULT_ILLEGAL_NULL_POINTER or R_HC_RESULT_BUFFER_ERROR
 */
r_hc_result_t R_HC_PackHlimField(const r_hc_hop_limit_field_t hlimFlag,
                                 const uint8_t                hlimValue,
                                 uint8_t                      output[],
                                 uint16_t*                    sumWrittenBytes,
                                 const uint16_t               compressedHdrMaxSize);

/*!
    \fn void R_HC_UnpackHlimField(const uint8_t input,
                                   uint16_t* sumReadBytes,
                                   uint8_t* hopLimit);
    \brief This function extracts the hop limit field.
    \param[in] input Single byte containing hop limit field.
    \param[out] sumReadBytes Counter that will be incremented by the number of bytes read from input.
    \param[out] hopLimit Extracted hop limit.
    \return None
 */
r_hc_result_t R_HC_UnpackHlimField(const uint8_t input,
                                   uint16_t*     sumReadBytes,
                                   uint8_t*      hopLimit);

/*!
    \fn r_hc_result_t R_HC_PackSamDamField(const r_boolean_t m,
                                   const r_hc_address_compression_t sacDac,
                                   const r_hc_address_mode_t samDam,
                                   const uint8_t addr[],
                                   uint8_t output[],
                                   uint16_t* sumWrittenBytes,
                                   const uint16_t compressedHdrMaxSize)
    \brief This function packs the SAM or DAM fields
    \param[in] m Flag indicating if the address is a multicast address or not
    \param[in] sacDac The SAC or DAC field
    \param[in] samDam The SAM or DAM field
    \param[in] addr The source or destination IPv6 address
    \param[in] output Address where the packed header will be written to
    \param[in] sumWrittenBytes Pointer to a location where the total number of written bytes will be written to
    \param[in] compressedHdrMaxSize Maximum size of the compressed header
    \return If successfull R_HC_RESULT_SUCCESS, else R_HC_RESULT_ILLEGAL_NULL_POINTER, R_HC_RESULT_BUFFER_ERROR or R_HC_RESULT_FAILED
 */
r_hc_result_t R_HC_PackSamDamField(const r_boolean_t                m,
                                   const r_hc_address_compression_t sacDac,
                                   const r_hc_address_mode_t        samDam,
                                   const uint8_t                    addr[],
                                   uint8_t                          output[],
                                   uint16_t*                        sumWrittenBytes,
                                   const uint16_t                   compressedHdrMaxSize);

/*!
    \fn r_hc_result_t R_HC_PackInLineNextHeader(r_hc_next_header_field_t nhField,
                                        uint8_t nextHeader,
                                        uint8_t output[],
                                        uint16_t* sumWrittenBytes,
                                        const uint16_t compressedHdrMaxSize);
    \brief This function packs in line next header type field
    \param[in] nhField Flag indicating if the next header is carried in line or not
    \param[in] nextHeader The next header value
    \param[in] output Address where the packed header will be written to
    \param[in] sumWrittenBytes Pointer to a location where the total number of written bytes will be written to
    \param[in] compressedHdrMaxSize Maximum size of the compressed header
    \return If successfull R_HC_RESULT_SUCCESS, else R_HC_RESULT_ILLEGAL_NULL_POINTER or R_HC_RESULT_BUFFER_ERROR
 */
r_hc_result_t R_HC_PackInLineNextHeader(r_hc_next_header_field_t nhField,
                                        uint8_t                  nextHeader,
                                        uint8_t                  output[],
                                        uint16_t*                sumWrittenBytes,
                                        const uint16_t           compressedHdrMaxSize);

#endif /* R_HC_6282_HEADERS_H */
