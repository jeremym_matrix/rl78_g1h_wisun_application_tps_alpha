/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2020 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/*!
 * @file r_assert.h
 * @version 1.0
 * @brief Header file for the definitions of the various R_ASSERT macros
 */

#ifndef R_ASSERT_H
#define R_ASSERT_H

#include <stdlib.h>

/*
 * The following values may be used for R_ASSERT_MODE to configure the behavior of R_ASSERT and related expressions
 * 0: Assertions are disabled -> Conditions are not checked at all
 * 1: Failing assertion results in return of (configurable) error value
 * 2: Failing assertion results in infinite loop (useful for physical device with attached debugger)
 * 3: Special mode for simulation on PC: Failing assertion results in immediate termination with error message
 * 4: Special mode for QAC tool: Failing assertion results in return of error code
 */

#if (R_ASSERT_MODE == 1)

/** Return ret if expression is not true */
    #define R_ASSERT_RET(expr, ret) do { if (!(expr)) { return ret; } } while (0)

#elif (R_ASSERT_MODE == 2)

/** Infinite loop if expression is not true */
#define R_ASSERT_RET(expr, ret) do { if (!(expr)) { while (1) {;} } } while (0)

#elif (R_ASSERT_MODE == 3)

/** Special mode for simulation: Terminate with proper error message if expression is not true */
void sim_assert_failed(const char* filename, int line, const char* functionName);
#define R_ASSERT_RET(expr, ret) do { if (!(expr)) {sim_assert_failed(__FILE__, __LINE__,  __func__); exit(1);} } while (0)

#elif (R_ASSERT_MODE == 4)

/** Special mode for QAC: Return ret if expression is not true */
#define R_ASSERT_RET(expr, ret) if (!(expr))return ret;

#else  /* if (R_ASSERT_MODE == 1) */

/** Do nothing */
#define R_ASSERT_RET(expr, ret) do { if (!(expr)) {  /* Do nothing */ } } while (0)

#endif /* if (R_ASSERT_MODE == 1) */

/* Set default return value of failed assertions (may be unset and redefined by other modules that use asserts) */
#ifndef R_ASSERT_FAIL_RETURN_VALUE
#define R_ASSERT_FAIL_RETURN_VALUE R_RESULT_ILLEGAL_NULL_POINTER  //!< Default return value for failed assertions
#endif

#define R_ASSERT(expr)           R_ASSERT_RET(expr, R_ASSERT_FAIL_RETURN_VALUE)
#define R_ASSERT_RET_VOID(expr)  R_ASSERT_RET(expr, )
#define R_ASSERT_RET_NULL(expr)  R_ASSERT_RET(expr, NULL)
#define R_ASSERT_RET_FALSE(expr) R_ASSERT_RET(expr, R_FALSE)
#define R_ASSERT_RET_RES(expr)   R_ASSERT_RET(expr, R_RESULT_ILLEGAL_NULL_POINTER)

#endif /* R_ASSERT_H */
