/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name    : r_nwk_inter_config.h
 * Version      : 1.00
 * Description  : Configuration parameters for the network layer
 ******************************************************************************/
#ifndef R_NWK_INTER_CONFIG_H
#define R_NWK_INTER_CONFIG_H

#include "r_stdint.h"
#include "r_table_size.h"

/******************************************************************************
   Macro definitions
******************************************************************************/
#if R_MINIMAL_ROUTER_NODE
#define R_SHARED_NWK_MEM          1
#undef R_BORDER_ROUTER_ENABLED
#define R_BORDER_ROUTER_ENABLED   0
#undef R_EDFE_INITIATOR_DISABLED
#define R_EDFE_INITIATOR_DISABLED 1
#undef R_EDFE_MINIMAL_RECEIVER
#define R_EDFE_MINIMAL_RECEIVER   1
#undef R_DEV_TBU_ENABLED
#define R_DEV_TBU_ENABLED         0
#define R_MPL_MAX_SUBSCRIPTIONS   1
#define R_MPL_MAX_SEEDS           1
#define R_MPL_MAX_MESSAGES        1
#endif

#if R_LEAF_NODE_ENABLED && !(R_WISUN_FAN_VERSION >= 110)
#error LFN functionality is only available for R_WISUN_FAN_VERSION >= 110
#endif

#if R_LFN_PARENTING_ENABLED && !(R_WISUN_FAN_VERSION >= 110)
#error LFN parenting functionality is only available for R_WISUN_FAN_VERSION >= 110
#endif

/** The length of the IPv6 address list (in ND) */
#define R_ND_IPV6_ADDRESS_LIST_LENGTH          (4u)

/** Max randomized delay value (in seconds) used by authentication, PAN and NS timers */
#define R_RAND_MAX_VAL                         (16u)

/** Maximum number of supported contexts (start from 0 to R_HC_MAX_CONTEXT_INDEX) */
#define R_HC_MAX_CONTEXT_INDEX                 (0x0Fu)

/* Stateful compression not allowed in FAN 1.0 (but is likely to be allowed in future versions) */
#define R_HC_IPHC_STATEFUL_COMPRESSION_ENABLED 0

/* IPv6 fragmentation disabled by default */
#if !defined(R_IP_FRAG_ENABLED)
#define R_IP_FRAG_ENABLED 0
#endif

/** The UDP port used to relay EAPOL messages */
#define R_AUTH_EAPOL_RELAY_PORT  10253

/** Threshold (referenced to DEVICE_MIN_SENS) above which a neighbor FFN may be considered for inclusion into RPL candidate parent set. */
#define R_CAND_PARENT_THRESHOLD  10

/** Hysteresis factor to be applied to CAND_PARENT_THRESHOLD when admitting or dropping FFNs nodes from the candidate parent set. */
#define R_CAND_PARENT_HYSTERESIS 3

extern uint8_t g_device_min_sens;  //!< The minimum receiver sensitivity level (0 -> -174 dBm; 254 -> +80 dBm).

/**
 * FANTPS-1.1v02: For an FFN to be removed from the candidate parent set, both its node-to-neighbor and neighbor-to-node
 * RSL EWMA values should fall below (DEVICE_MIN_SENS + CAND_PARENT_THRESHOLD - CAND_PARENT_HYSTERESIS).
 */
#define R_CAND_PARENT_THRESHOLD_REMOVAL (g_device_min_sens + R_CAND_PARENT_THRESHOLD - R_CAND_PARENT_HYSTERESIS)

/**
 * FANTPS-1.1v02: For an FFN to be admitted to the candidate parent set, both its node-to-neighbor and neighbor-to-node
 * RSL EWMA values should exceed (DEVICE_MIN_SENS + CAND_PARENT_THRESHOLD + CAND_PARENT_HYSTERESIS).
 */
#define R_CAND_PARENT_THRESHOLD_ADD     (g_device_min_sens + R_CAND_PARENT_THRESHOLD + R_CAND_PARENT_HYSTERESIS)


/* For fixed GTKs the following specifies GTK0 and the first byte is incremented for each GTK1-3 */
#if R_DEV_FIXED_GTKS || R_DEV_DISABLE_AUTH
#define R_AUTH_FIXED_GTK0 0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08, 0x09, 0x0A, 0x0B, 0x0C, 0x0D, 0x0E, 0x0F, 0x10
#endif

/* For 6LowPAN fragmentation */
#ifndef R_FRAG_MAX_CONCURRENT_RX_PACKETS
#define R_FRAG_MAX_CONCURRENT_RX_PACKETS (4u)              //!< Maximum number of datagrams that can be reassembled at the same time
#endif
#define R_FRAG_REASSEMBLY_TIMEOUT        (30000u)          //!< Reassembly timeout in milliseconds (RFC4944 5.3, a maximum of 60 seconds)

#define R_MAX_MTU_SIZE                   (1576u)           //!< Maximum MTU size

#define R_PAN_ID_BLACKLIST_SIZE          8                 //!< Number of elements in the PAN ID black list

#define R_RPL_MAX_PATH_LIFETIME_MINUTES  ((uint8_t)(0xFE)) //!< Maximum Path Lifetime that may advertised in a RPL DAO message

#endif  /* R_NWK_INTER_CONFIG_H */
