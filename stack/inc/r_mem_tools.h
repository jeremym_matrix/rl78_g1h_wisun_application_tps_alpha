/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2020 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/*******************************************************************************
 * File Name   : r_mem_tools.h
 * Version     : 1.0
 * Description : Utility functions to manipulate and compare memory chunks
 ******************************************************************************/

/**
 * @file r_mem_tools.h
 * @version 1.0
 * @brief Utility functions to manipulate and compare memory chunks
 */

#include "r_stdint.h"

#ifndef R_MEM_TOOLS_H
#define R_MEM_TOOLS_H

/******************************************************************************
Macro definitions
******************************************************************************/

/******************************************************************************
Typedef definitions
******************************************************************************/

/******************************************************************************
Exported global functions (to be accessed by other files)
******************************************************************************/

/**
 * Check if a memory area is all-zero
 * @param dst Pointer to the memory area to check
 * @param length The number of bytes to check
 * @return 0 if memory location is all-zero
 */
uint8_t R_MemCmpZero(const void* dst, uint32_t length);

/**
 * Copy memory chunk from src to dst
 * @param dst Pointer to destination memory area
 * @param src Pointer to source memory area
 * @param size The number of bytes that should be copied
 */
void R_memcpy(void* dst, const void* src, uint32_t size);

/**
 * Set each byte of a memory chunk to a specific value
 * @param dst Pointer to the memory chunk
 * @param value The value that should be written to each byte of the memory chunk
 * @param size The size of the memory chunk in bytes
 */
void R_memset(void* dst, uint8_t value, uint32_t size);

/**
 * Compare two memory chunks
 * @param ptr1 Pointer to the first memory chunk
 * @param ptr2 Pointer to the second memory chunk
 * @param num The number of bytes to compare
 * @return 0 if the first num bytes of both memory chunks are identical
 */
int32_t R_memcmp(const void* ptr1, const void* ptr2, uint32_t num);

/**
 * Write a well-known check pattern to the end of a buffer to check for buffer overflows at a later point
 * @param[in, out] buf The buffer location where the check pattern should be written
 * @param checkPatternSize The size of the check pattern in bytes
 */
void R_memWriteCheckPattern(uint8_t* buf, uint32_t checkPatternSize);

/**
 * Verify that the check pattern is still present in the specified buffer
 * @param buf The buffer location where the check pattern should be verified
 * @param checkPatternSize The size of the check pattern in bytes
 * @return 1 if the check pattern is still present so the buffer was NOT overwritten. 0 if the buffer was overwritten.
 */
int R_memVerifyCheckPattern(const uint8_t* buf, uint32_t checkPatternSize);

/**
 * Hook function to capture failures due to buffer overruns
 * @warning This function disables interrupts, logs an error message and loops infinitely
 * @param errorMsg The error message that should be logged before entering the infinite loop
 */
void R_memOverflowHook(const char* errorMsg);

#if R_HC_IPHC_STATEFUL_COMPRESSION_ENABLED
/***********************************************************************
* Function Name     : R_memcmpzerobit
* Description       : Comparison of a bit string with zero
* Argument          : ptr1 : Pointer to the first byte where the string to be compared is contained
*                   : bitOffset : Offset in the first byte to the bit where the string to be compared begins
*                   : numBits : Number of bits to be compared with zero from ptr1 + bitOffset
* Return Value      : Zero if memory location is all-zero, sum of memory bits otherwise
***********************************************************************/
/*!
   \fn          uint8_t R_memcmpzerobit(const uint8_t ptr1[],
                        const uint8_t bitOffset,
                        uint32_t numBits);
   \brief       Comparison of a bit string with zero
   \param[in]   ptr1 Pointer to the first byte where the string to be compared is contained
   \param[in]   bitOffset Offset in the first byte to the bit where the string to be compared begins
   \param[in]   numBits Number of bits to be compared with zero from ptr1 + bitOffset
   \return      Zero if memory location is all-zero, sum of memory bits otherwise
 */
int8_t R_memcmpzerobit(const uint8_t ptr1[],
                       const uint8_t bitOffset,
                       uint32_t      numBits);

/***********************************************************************
* Function Name     : R_memcmpbit
* Description       : Compare memory chunks bit-wise
* Argument          : ptr1 : Pointer to memory location one
*                   : ptr2 : Pointer to memory location two
*                   : bitOffset : Offset in bits to where the bit string starts
* Return Value      : Zero if memory locations are identical
***********************************************************************/
/*!
   \fn          uint8_t R_memcmpbit(const uint8_t ptr1[],
                                    const uint8_t ptr2[],
                                    const uint8_t bitOffset,
                                    const uint32_t numBits);
   \brief       Compare memory chunks bit-wise
   \param[in]   ptr1 Pointer to memory location one
   \param[in]   ptr2 Pointer to memory location two
   \param[in]   bitOffset Offset in bits to where the bit string starts
   \param[in]   numBits Number of bits to be compared
   \return      Zero if memory locations are identical
 */
uint8_t R_memcmpbit(const uint8_t  ptr1[],
                    const uint8_t  ptr2[],
                    const uint8_t  bitOffset,
                    const uint32_t numBits);

/***********************************************************************
* Function Name     : R_memcpybit
* Description       : Copy memory chunk from dst to src bit-wise
* Argument          : dst : Pointer to destination memory location
*                   : src : Pointer to source memory location
*                   : numBits : Size in bites to be copied from source to destination
* Return Value      : None
***********************************************************************/
/*!
   \fn          void R_memcpybit(uint8_t dst[],
                                 const uint8_t src[],
                                 const uint32_t numBits);
   \brief       Copy memory chunk from dst to src bit-wise
   \param[in]   dst Pointer to destination memory location
   \param[in]   src Pointer to source memory location
   \param[in]   numBits Size in bites to be copied from source to destination
 */
void R_memcpybit(uint8_t        dst[],
                 const uint8_t  src[],
                 const uint32_t numBits);

#endif  /* R_HC_IPHC_STATEFUL_COMPRESSION_ENABLED */

#endif  /* R_MEM_TOOLS_H */
