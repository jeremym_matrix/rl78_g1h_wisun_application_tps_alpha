/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2011 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_frag.h
 * Version     : 1.0
 * Description : Header file for the top level fragmentation module
 ******************************************************************************/

/*!
   \file      r_frag.h
   \version   1.0
   \brief     Header file for the top level fragmentation module
 */

#ifndef R_FRAG_H
#define R_FRAG_H

#include "r_6lowpan_headers.h"
#include "r_nwk_api.h"

/******************************************************************************
   Macro definitions
******************************************************************************/
#define R_FRAG_MAX_DATAGRAM_SIZE (R_MAX_MTU_SIZE)  //!< Maximum size in bytes of an entire datagram(= 1280)(RFC4944)

/******************************************************************************
   Typedef definitions
******************************************************************************/

/*!
    \enum r_frag_result_t
    \brief Fragmentation result values
 */
typedef enum
{
    R_FRAG_RESULT_SUCCESS              = 0x00, //!< Success
    R_FRAG_RESULT_FAILED               = 0x01, //!< Process failed
    R_FRAG_RESULT_BAD_INPUT_ARGUMENTS  = 0x02, //!< Bad arguments/attributes
    R_FRAG_RESULT_ILLEGAL_NULL_POINTER = 0x03, //!< Input to a function was null pointer
    R_FRAG_RESULT_BUFFER_ERROR         = 0x04, //!< Write access out of array boundary or iovec error
    R_FRAG_RESULT_NO_MORE_FRAGMENTS    = 0x05, //!< No more fragments available for transmission
    R_FRAG_RESULT_NO_FRAG_REQUIRED     = 0x06, //!< No fragmentation required for datagram
    R_FRAG_RESULT_REASSEMBLY_FINISHED  = 0x07  //!< Reassembly process finished successfully
} r_frag_result_t;

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

/*!
    \fn void R_FRAG_Init(void)
    \brief This function initializes the fragmentation module
    \return None
 */
void R_FRAG_Init(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/*!
    \fn void R_FRAG_Reset(void);
    \brief This function resets the fragmentation module
    \return None
 */
void R_FRAG_Reset(void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

/*!
    \fn r_frag_result_t R_FRAG_GetFragment(const r_iovec_class_t* inIovec,
                                   r_iovec_class_t* outIovec,
                                   uint16_t uncompressedHdrSize,
                                   r_frag_header_data_t* fragHdr,
                                   const uint16_t availPayloadSize);
    \brief This function does the fragmentation tx processing (<a href="../../eap_Fragmentation/index.htm">Procedure described here</a>)
    \param[in] inIovec iovec describing the input data. Must have 1 or 2 valid elements. The first element must contain the header and the second the payload if there is any payload present
    \param[out] outIovec iovec describing the fragment.
    \param[in] uncompressedHdrSize the size in bytes of the uncompressed header(s)
    \param[out] fragHdr structure where the fragmentation header variables will be written to
    \param[in] availPayloadSize size in bytes of payload available in mac frame (before frag header is added)
    \return R_FRAG_RESULT_NO_FRAG_REQUIRED if no fragmentation is required for this frame,
            R_FRAG_RESULT_FAILED if any processing in the funtion fails, R_FRAG_RESULT_SUCCESS if the processing of a fragment has finished and R_FRAG_RESULT_NO_MORE_FRAGMENTS if
            all the fragments for this datagram have been processed
 */
r_frag_result_t R_FRAG_GetFragment(const r_iovec_class_t* inIovec,
                                   r_iovec_class_t*       outIovec,
                                   uint16_t               uncompressedHdrSize,
                                   r_frag_header_data_t*  fragHdr,
                                   const uint16_t         availPayloadSize,
                                   void*                  vp_nwkGlobal); // void* -> r_nwk_cb_t*

/*!
    \brief This function does the reassembly rx processing (for this implementation it is mandatory that packets arrive in order) (<a href="../../eap_Fragmentation/index.htm">Procedure described here</a>)
    \param[in] inIoVec iovec describing the input data.
    \param[in] fragHdr structure where the fragmentation header variables are stored in.
    \param[in] srcAddress short/extended address of fragment origin
    \param[in] secured is the fragment secured
    \param[in] udpHdrInf Structure containing information to be used for UDP header reconstruction after reassembly
    \param[out] datagram pointer to reassembled datagram (memory is allocated inside the fragmentation module)
    \return R_FRAG_RESULT_SUCCESS
 */
r_frag_result_t R_FRAG_ReassembleDatagram(const r_iovec_class_t*      inIoVec,
                                          const r_frag_header_data_t* fragHdr,
                                          const uint8_t               srcAddress[],
                                          r_boolean_t                 secured,
                                          r_hc_upd_uncomp_t*          udpHdrInf,
                                          uint8_t**                   datagram,
                                          void*                       vp_nwkGlobal); // void* -> r_nwk_cb_t*

/*!
    \brief This function packs the fragmentation header
    \param[in]  output          Pointer to a memory location where the output will be written to
    \param[in]  outputMaxSize   Maximum number of bytes that can be written at output
    \param[in]  fragHdr         Pointer to a structure describing the fragment
    \param[in]  fragHdrSize     Size in bytes of the fragmentation header
    \return None
 */
void R_FRAG_PackHeader(uint8_t                     output[],
                       const uint8_t               outputMaxSize,
                       const r_frag_header_data_t* fragHdr,
                       uint8_t*                    fragHdrSize);


void R_FRAG_ProcessTimer(const uint32_t timetickMS, void* vp_nwkGlobal);  // void* -> r_nwk_cb_t*

#endif /* R_FRAG_H */
