/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* (c) 2020 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/*!
 * @file mac_neighbors.h
 * @brief Header file for the neighbor management of the MAC layer
 */

/*
 * Prevent nested inclusions
 */
#ifndef R_MAC_NEIGHBORS_H
#define R_MAC_NEIGHBORS_H

#include <stddef.h>

#include "r_mac_ie.h"
#include "mac_types.h"
#include "mac_frame.h"
#include "r_auth_types.h"
#include "r_schedule_utils.h"

/**********************************************************************************************************************
* Typedef definitions
**********************************************************************************************************************/

typedef struct
{
#ifdef R_HYBRID_PLC_RF
    uint32_t frame_counters[1];  // One frame counter for all keys in G3 hybrid, kept array syntax for FAN consistency
#else
    uint32_t frame_counters[R_KEY_NUM];
#endif /* R_HYBRID_PLC_RF */
    uint8_t  counters_valid;
} r_mac_frame_counters_t;

typedef struct
{
    RmDeviceAddressT               mac_address; //!< The EUI-64 of the neighbor in little-endian representation

    /* information for duplicate frame detection */
    uint8_t                        seq_nbr;             //!< The sequence number of the most recent frame from this nbr
    uint8_t                        seq_nbr_isValid : 1; //!< True, if seq_nbr is valid and may be used for duplicate detection

    /* flags */
    uint8_t                        shortEvictThreshold : 1; //!< True, if this entry has a reduced eviction threshold
    uint8_t                        isLfn : 1;               //!< True, if this neighbor is an LFN

    /* The following block of fields MUST NOT be changed directly! - Use the functions provided by R_MAC_NEIGHBORS */
    uint8_t                        num_locks : 2; //!< The number of locks for this entry to protect it from eviction
    uint32_t                       last_heard;    //!< Last time we received a unicast frame from this neighbor

    /* PAN information */
    uint16_t                       pan_size;     //!< Number of nodes communicating through the Border Router
    uint16_t                       routing_cost; //!< Transmitting node's routing path ETX to the Border Router

    /* information for frequency hopping module */
    r_computed_unicast_schedule_t* schedule;
    volatile uint16_t              slot;
    volatile uint8_t               offset_ms;

    /* information for MAC security module */
    r_mac_frame_counters_t         frame_counters;

#ifdef R_HYBRID_PLC_RF
    uint8_t                        forwardLQI;           //!< Link quality indicator computed as the exponentially weighted moving average (EWMA) of the LQI derived from the received packets from this neighbour using a smoothing factor of 1/8.
    uint8_t                        reverseLQI;           //!< Link quality indicator computed as the exponentially weighted moving average (EWMA) of the LQI derived from RLQ-IEs received from this neighbour using a smoothing factor of 1/8. If no reverse LQI measurements are available, this value shall be set to 0xFF indicating “not measured"
    uint8_t                        dutyCycle;            //!< Duty cycle usage of the neighbour in percent as reported via the last received LI-IE.
    uint8_t                        forwardTxPowerOffset; //!< TX output power offset used for transmissions from this device to the neighbour node. Defined as the power reduction in dB compared to a reference TX output power of 30 dBm. The initial value is the difference in dB of the implementation’s TX output power to 30 dBm. The value is propagated to the neighbour via the LI-IE.
    uint8_t                        reverseTxPowerOffset; //!< TX output power offset used for transmissions from the neighbour node to this device. Defined as the power reduction in dB compared to a reference TX output power of 30 dBm. The value is received from the neighbour via the LI-IE.
#endif /* R_HYBRID_PLC_RF */
} r_mac_neighbor_t;

#ifdef R_HYBRID_PLC_RF
R_HEADER_UTILS_PRAGMA_PACK_1
typedef struct
{
    uint16_t shortAddress;         //!< The MAC short address of the neighbour which this entry refers to.
    uint8_t  forwardLQI;           //!< Link quality indicator computed as the exponentially weighted moving average (EWMA) of the LQI derived from the received packets from this neighbour using a smoothing factor of 1/8.
    uint8_t  reverseLQI;           //!< Link quality indicator computed as the exponentially weighted moving average (EWMA) of the LQI derived from RLQ-IEs received from this neighbour using a smoothing factor of 1/8. If no reverse LQI measurements are available, this value shall be set to 0xFF indicating “not measured"
    uint8_t  dutyCycle;            //!< Duty cycle usage of the neighbour in percent as reported via the last received LI-IE.
    uint8_t  forwardTxPowerOffset; //!< TX output power offset used for transmissions from this device to the neighbour node. Defined as the power reduction in dB compared to a reference TX output power of 30 dBm. The initial value is the difference in dB of the implementation’s TX output power to 30 dBm. The value is propagated to the neighbour via the LI-IE.
    uint8_t  reverseTxPowerOffset; //!< TX output power offset used for transmissions from the neighbour node to this device. Defined as the power reduction in dB compared to a reference TX output power of 30 dBm. The value is received from the neighbour via the LI-IE.
    uint8_t  POSValidTime;         //!< Remaining time in minutes until when this entry is considered valid.
} r_mac_pos_entry_t;

typedef struct
{
    uint16_t shortAddress;  //!< The MAC short address of the neighbour which this entry refers to.
    uint32_t frameCounter;  //!< Stored frame counter which is included in each incoming secured frame.
} r_mac_device_entry_t;
R_HEADER_UTILS_PRAGMA_PACK_DEFAULT
#endif /* ifdef R_HYBRID_PLC_RF */

/**********************************************************************************************************************
* Function definitions
**********************************************************************************************************************/

/**
 * Initialize the neighbor management module. This function must be called before any other function in this module!
 */
void R_MAC_NEIGHBORS_Init(void);

/**
 * Allocate a memory block on the heap for the neighbor cache module.
 */
void* R_MAC_NEIGHBORS_Malloc(size_t size);

/**
 * Allocate and zero-initialize a memory block on the heap for the neighbor cache module.
 */
void* R_MAC_NEIGHBORS_Zalloc(size_t size);

/**
 * Resize memory block on the heap for the neighbor cache module.
 */
void* R_MAC_NEIGHBORS_Realloc(void* ptr, size_t size);

/**
 * Free a memory block memory that was allocated using the R_MAC_NEIGHBORS_Malloc function.
 * @param ptr A pointer to the memory block that should be freed.
 */
void R_MAC_NEIGHBORS_Free(void* ptr);

/**
 * Delete all entries from the neighbor cache.
 */
void R_MAC_NEIGHBORS_Reset(void);

/**
 * Retrieve the entry with the given MAC address from the neighbor cache.
 * @param mac_address The MAC address whose neighbor entry should be retrieved from the neighbor cache.
 * @return The requested neighbor cache entry if it exists; NULL otherwise.
 */
r_mac_neighbor_t* R_MAC_NEIGHBORS_Lookup(const RmDeviceAddressT* mac_address);

/**
 * Retrieve the entry with the given MAC address from the neighbor cache if its unicast schedule is set.
 * @param mac_address The MAC address whose neighbor entry should be retrieved from the neighbor cache.
 * @return The requested neighbor cache entry if it exists and its unicast schedule is set; NULL otherwise.
 */
r_mac_neighbor_t* R_MAC_NEIGHBORS_LookupWithSchedule(const RmDeviceAddressT* mac_address);

/**
 * Retrieve the entry with the given MAC address from the neighbor cache or try to add it if it does not exist.
 * @details On creation, the last_heard timestamp is set to the current time. For existing entries, the timestamp is
 * not modified.
 * @param mac_address The MAC address of the entry that should be retrieved from or added to the neighbor cache.
 * @param reducedEvictThreshold True, if the neighbor's eviction threshold should be reduced so that it may be evicted
 * earlier than usual. True, if the default eviction threshold should be used.
 * @return The requested neighbor cache entry if it exists or could be added to the neighbor cache; NULL otherwise.
 */
r_mac_neighbor_t* R_MAC_NEIGHBORS_LookupOrAdd(const RmDeviceAddressT* mac_address, r_boolean_t reducedEvictThreshold);

/**
 * Check if the specified neighbor can be added to the neighbor cache
 * @details This function does NOT add the specified neighbor; it only checks if the insertion would be possible.
 * @param mac_address The MAC address of the neighbor.
 * @return R_TRUE if the requested neighbor cache exists or could be added to the neighbor cache. R_FALSE otherwise.
 */
r_boolean_t R_MAC_NEIGHBORS_InsertionPossible(const RmDeviceAddressT* mac_address);

/**
 * Update the lastHeard timestamp of the specified neighbor.
 * @details This function is the ONLY permitted way to update the lastHeard timestamp of a neighbor!
 * @param nbr The neighbor whose lastHeard timestamp should be updated.
 * @param newTimestamp The new timestamp that should be stored for the neighbor.
 */
void R_MAC_NEIGHBORS_UpdateLastHeard(r_mac_neighbor_t* nbr, uint32_t newTimestamp);

/**
 * Update the unicast schedule of the neighbor with the given MAC address in the neighbor cache.
 * @param address The MAC address of the neighbor entry whose schedule should be updated.
 * @param new_sched The new unicast schedule that should be set for the respective neighbor. The pointer must have been
 * allocated using the R_MAC_NEIGHBORS_Malloc function. Upon calling this function, the ownership of this pointer is
 * transferred from the caller to the neighbor management module. This parameter may be NULL to delete the neighbor's
 * existing schedule.
 * @retval R_RESULT_SUCCESS if the schedule was updated successfully.
 * @retval R_RESULT_NBR_NOT_FOUND if the specified address is not present in the neighbor cache.
 */
r_result_t R_MAC_NEIGHBORS_UpdateSchedule(const RmDeviceAddressT* address, r_computed_unicast_schedule_t* new_sched);

/**
 * Retrieve the first used entry from the neighbor cache.
 * @return The first used entry from the neighbor cache if it exists; NULL if there are no used entries.
 */
r_mac_neighbor_t* R_MAC_NEIGHBORS_GetFirstEntry(void);

/**
 * Retrieve the next used entry from the neighbor cache.
 * @param neighbor The neighbor entry whose successor should be retrieved from the neighbor cache.
 * @return The subsequent neighbor cache entry of the given entry if it exists; NULL if there are no further entries
 * in the neighbor cache or the neighbor argument is NULL.
 */
r_mac_neighbor_t* R_MAC_NEIGHBORS_GetNextEntry(const r_mac_neighbor_t* neighbor);

/**
 * Check if the received frame is a duplicate.
 * @param mhr The MAC header of the received frame.
 * @param neighbor The neighbor cache entry corresponding to the sender of the received frame.
 * @retval R_RESULT_SUCCESS Received frame is not a duplicate.
 * @retval R_RESULT_FAILED Received frame is a duplicate.
 * @retval R_RESULT_NBR_NOT_FOUND if the specified neighbor is invalid (NULL).
 */
r_result_t R_MAC_DuplicateFrameDetection(const RmFrmMhrT* mhr, r_mac_neighbor_t* neighbor);

/**
 * Lock the neighbor with the specified MAC address so that it is not considered for eviction anymore.
 * @param mac_address The MAC address of the neighbor that should be locked.
 * @return R_RESULT_SUCCESS if the corresponding neighbor was locked. R_RESULT_FAILED if the neighbor does not exist.
 */
r_result_t R_MAC_NEIGHBORS_Lock(const RmDeviceAddressT* mac_address);

/**
 * Unlock the neighbor with the specified MAC address so that it is considered for eviction again.
 * @param mac_address The MAC address of the neighbor that should be unlocked.
 * @return R_RESULT_SUCCESS if the corresponding neighbor was unlocked. R_RESULT_FAILED if the neighbor does not exist.
 */
r_result_t R_MAC_NEIGHBORS_Unlock(const RmDeviceAddressT* mac_address);

/**
 * Determine if the neighbor cache is full and eviction must be performed before a new neighbor can be added.
 * @return True, if all slots in the neighbor cache are in use. False otherwise.
 */
r_boolean_t R_MAC_NEIGHBORS_IsFull(void);

/**
 * Dump all entries from the neighbor cache.
 */
void R_MAC_NEIGHBORS_Dump(void);

/**
 * Flush all receive frame counters for a given key index.
 */
void R_MAC_NEIGHBORS_FlushRxFrameCounters(uint8_t keyIndex);

/**
 * Update PAN Metrics of corresponding neighbor cache entry.
 */
void R_MAC_NEIGHBORS_UpdatePANMetrics(RmDeviceAddressT* mac_address, uint16_t pan_size, uint16_t routing_cost);

#endif /* R_MAC_NEIGHBORS_H */
