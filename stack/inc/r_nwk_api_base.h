/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2020 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/**
 * @file r_nwk_api_base.h
 * @brief Definition of common types and constants used by the network layer
 */

#ifndef R_NWK_API_BASE_H
#define R_NWK_API_BASE_H

#include "r_nwk_inter_config.h"
#include "r_stdint.h"
#include "r_header_utils.h"

/******************************************************************************
   Macro definitions
******************************************************************************/
#define R_FALSE                   (0u)                     //!< Boolean false
#define R_TRUE                    (1u)                     //!< Boolean true

#define R_IPV6_HEADER_SIZE        (40u)                    //!< IPV6 header length
#define R_IPV6_UDP_HEADER_SIZE    (8u)                     //!< UDP header size in bytes
#define R_UDP_HEADER_SIZE         (R_IPV6_UDP_HEADER_SIZE) //!< UDP header size in bytes

/*! FANTPS-1v29: A Border Router's WAN interface MUST support an IPv6 MTU of 1280 bytes. */
#define R_WAN_IP_MTU              (1280u)

/*! Maximum number in bytes of UDP datagram payload */
#define R_UDP_MAX_UDP_DATA_LENGTH (R_MAX_MTU_SIZE - R_IPV6_HEADER_SIZE - R_IPV6_UDP_HEADER_SIZE)

/*! Maximum number in bytes of ICMP Echo request/response message payload */

/* Overhead includes MHR (addressing, aux sec, header IEs), MPX-IE header, MIC, and FCS */
#define R_MAX_FRAME_SIZE              (R_MAX_MTU_SIZE + 24) //!< Maximum frame size including MAC layer

#define R_IPV6_ADDRESS_LENGTH         (16u)                 //!< IPV6 address length in bytes
#define R_IPV6_PREFIX_LEN             (8u)                  //!< IPv6 prefix length in bytes
#define R_MAC_EXTENDED_ADDRESS_LENGTH (8u)                  //!< Length of extended addresses in bytes


/**
 * Maximum length of the network name as transmitted in a Network Name Information Element (NETNAME-IE)
 * FANTPS-1v32: The string MUST NOT contain a terminating 0, and the length of the string MUST NOT exceed 32 octets.
 */
#define R_NETWORK_NAME_MAX_LENGTH       (32U)

/** Maximum length of the network name represented as a C-string (including the null termination byte) */
#define R_NETWORK_NAME_STRING_MAX_BYTES (R_NETWORK_NAME_MAX_LENGTH + 1U)


/* Protocol Id */
#define R_IPV6_PROTOCOL_ID_TCP                    (0x06) //!< TCP
#define R_IPV6_PROTOCOL_ID_UDP                    (0x11) //!< UDP
#define R_IPV6_PROTOCOL_ID_ICMPV6                 (0x3A) //!< ICMPv6

/* ICMP Type */
#define R_ICMP_TYPE_ERROR_DESTINATION_UNREACHABLE (1)   //!< Destination unreachable error type
#define R_ICMP_TYPE_ERROR_PACKET_TOO_BIG          (2)   //!< Packet too big error type
#define R_ICMP_TYPE_ERROR_TIME_EXCEEDED           (3)   //!< Time exceeded error type
#define R_ICMP_TYPE_ERROR_PARAMETER_PROBLEM       (4)   //!< Parameter problem error type
#define R_ICMP_TYPE_ECHO_REQUEST                  (128) //!< Echo Request type
#define R_ICMP_TYPE_ECHO_REPLY                    (129) //!< Echo Reply type
#define R_ICMP_TYPE_NEIGHBOR_SOLICITATION         (135) //!< Neighbor Solicitation type
#define R_ICMP_TYPE_NEIGHBOR_ADVERTISEMENT        (136) //!< Neighbor Advertisement type

/* Tx Options for Application */
#define R_OPTIONS_NON_BLOCKING                    0x0001
#if R_PHY_TYPE_CWX_M
#define R_OPTIONS_MODE_SWITCH_FRAME               0x0002
#endif
#define R_OPTIONS_SEC_ENABLED                     0x0080
#define R_OPTIONS_ND_SUPPRESSED                   0x0040
#define R_OPTIONS_START_FORCE                     0x0100
#if !R_EDFE_INITIATOR_DISABLED
#define R_OPTIONS_FRAME_EXCHANGE_EDFE             0x1000
#endif

/* Tx Options for internal use */
#define R_OPTIONS_FRAME_ADD_US_IE 0x2000
#define R_OPTIONS_FORWARDING      0x4000

/* Rx status in indication message */
#define R_STATUS_SEC_ENABLED      0x0080

#define R_IPV6_DEFAULT_HOP_LIMT   64    //!< Default hop limit for IPv6 frames

/******************************************************************************
   Typedef definitions
******************************************************************************/
typedef uint8_t r_boolean_t;  //!< Boolean type as cast of unsigned 8 bit integer

/**
 * Result codes for NWK and MAC operations
 */
typedef enum R_HEADER_UTILS_ATTR_PACKED
{
    /* Shared result codes */
    R_RESULT_SUCCESS                    = 0x00,  //!< Operation succeeded
    R_RESULT_MAC_FAILURE                = 0x01,
    R_RESULT_ILLEGAL_NULL_POINTER       = 0x03,  //!< Input to a function was a NULL pointer
    R_RESULT_INVALID_REQUEST            = 0x04,  //!< Not accepted request
    R_RESULT_TIMEOUT                    = 0x05,
    R_RESULT_SUCCESS_ADDITIONAL_DATA    = 0x06,  //!< Operation succeeded but additional data is available
    R_RESULT_NOTHING_TO_DO              = 0x07,  //!< Operation was already done / nothing more to do
    R_RESULT_ILLEGAL_STATE              = 0x09,  //!< Function must not be called in current state
    R_RESULT_FAILED                     = 0x10,  //!< Operation failed
    R_RESULT_INVALID_PARAMETER          = 0x11,  //!< Invalid parameter
    R_RESULT_UNSUPPORTED_FEATURE        = 0x12,  //!< Feature is not supported by this device
    R_RESULT_UNAVAILABLE                = 0x13,  //!< Attribute is currently unavailable
    R_RESULT_INSUFFICIENT_BUFFER        = 0x14,  //!< Insufficient memory left on this device
    R_RESULT_INSUFFICIENT_OUTPUT_BUFFER = 0x15,  //!< The specified output buffer is too small to hold the result

    R_RESULT_PAN_ID_CONFLICT            = 0x20,  //!< There was already a PAN running with the same ID
    R_RESULT_NO_NETWORK                 = 0x21,  //!< The network specified by PAN ID in the associate process was not available
    R_RESULT_NBR_NOT_FOUND              = 0x30,  //!< No neighbor cache entry found
    R_RESULT_NWK_IP_REG_CONFLICT        = 0x31,  //!< The IP address was already registered by another EUI-64
    R_RESULT_RPL_FAILED                 = 0x80,  //!< The corresponding RPL operation failed
    R_RESULT_RPL_NO_ROUTE               = 0x81,  //!< No source route found

    R_RESULT_MAC_TRANSMIT_TIME_OVERFLOW = 0xC4,
    R_RESULT_MAC_ACK_RCVD_NODSN_NOSA    = 0xC5,
    R_RESULT_MAC_COUNTER_ERROR          = 0xdb,
    R_RESULT_MAC_UNSUPPORTED_SECURITY   = 0xdf,
    R_RESULT_MAC_CHANNEL_ACCESS_FAILURE = 0xe1,
    R_RESULT_MAC_SECURITY_ERROR         = 0xe4,
    R_RESULT_MAC_FRAME_TOO_LONG         = 0xe5,
    R_RESULT_MAC_INVALID_PARAMETER      = 0xe8,
    R_RESULT_MAC_NO_ACK                 = 0xe9,
    R_RESULT_MAC_UNAVAILABLE_KEY        = 0xf3,
    R_RESULT_MAC_UNSUPPORTED_ATTRIBUTE  = 0xf4
} r_result_t;

/* Disable padding for the following structs so that they can be transmitted via the modem serial interface */
R_HEADER_UTILS_PRAGMA_PACK_1

/**
 * Representation of an EUI-64 (MAC) address
 */
typedef struct
{
    uint8_t bytes[R_MAC_EXTENDED_ADDRESS_LENGTH];
} r_eui64_t;

/**
 * Representation of an IPv6 address
 */
typedef struct
{
    uint8_t bytes[R_IPV6_ADDRESS_LENGTH];
} r_ipv6addr_t;

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

#endif /* R_NWK_API_BASE_H */
