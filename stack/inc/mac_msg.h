/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
#ifndef __RM_mac_msg_H
#define __RM_mac_msg_H
/*******************************************************************************
 * file name    : mac_msg.h
 * description  : The definition for message Interface
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/* --- --- */
#include "mac_types.h"
#include "roa.h"


/******************************************************************************
    Macro Definitions
******************************************************************************/

/* Generic message definition */

/*
 * Message Primitive id
 */
typedef enum
{
    /*
     * PrimitiveId for PHY layer (MLME-SAP/MCPS-SAP Internal Use)
     */
#ifndef R_HYBRID_PLC_RF
    RM_Pid_PD_DATA_request            = 0x00,
    RM_Pid_PLME_CCA_request           = 0x01,
    RM_Pid_PLME_ED_request            = 0x02,
    RM_Pid_PLME_GET_request           = 0x03,
    RM_Pid_PLME_SET_TRX_STATE_request = 0x04,
    RM_Pid_PLME_SET_request           = 0x05,
#endif

    /* The Primitive (request)  definition (MLME-SAP/MCPS-SAP) */
#ifdef R_HYBRID_PLC_RF
    RM_Pid_MCPS_DATA_request             = 0x00,
    RM_Pid_MLME_GET_request              = 0x02,
    RM_Pid_MLME_RESET_request            = 0x01,
    RM_Pid_MLME_SCAN_request             = 0x04,
    RM_Pid_MLME_SET_request              = 0x03,
    RM_Pid_MLME_START_request            = 0x05,
    RM_Pid_MLME_BEACON_request           = 0x06,
#else
    RM_Pid_MCPS_DATA_request             = 0x20,
#if !R_EDFE_INITIATOR_DISABLED
    RM_Pid_MCPS_EDFE_DATA_request        = 0x23,
#endif
    RM_Pid_MLME_GET_request              = 0x25,
    RM_Pid_MLME_RESET_request            = 0x29,
    RM_Pid_MLME_SET_request              = 0x2c,
#endif /* R_HYBRID_PLC_RF */

    /* The PHY Indication/Confirm Primitive Id definition
       (MLME-SAP/MCPS-SAP Internal Use) */
    RM_Pid_PD_DATA_confirm               = 0x2f,
    RM_Pid_PD_DATA_indication            = 0x30,
    RM_Pid_PLME_CCA_confirm              = 0x31,
    RM_Pid_PLME_ED_confirm               = 0x32,
    RM_Pid_PLME_GET_confirm              = 0x33,
    RM_Pid_PLME_SET_TRX_STATE_confirm    = 0x34,
    RM_Pid_PLME_SET_confirm              = 0x35,

    /* The Primitive Id (confirm)  definition (MLME-SAP/MCPS-SAP) */
#ifdef R_HYBRID_PLC_RF
    RM_Pid_MLME_BEACON_NOTIFY_indication = 0x26,
    RM_Pid_MLME_COMM_STATUS_indication   = 0x27,
    RM_Pid_MLME_FRAMECOUNT_indication    = 0x28,
    RM_Pid_MCPS_DATA_indication          = 0x20,

    RM_Pid_MCPS_DATA_confirm             = 0x10,
    RM_Pid_MLME_GET_confirm              = 0x12,
    RM_Pid_MLME_RESET_confirm            = 0x11,
    RM_Pid_MLME_SCAN_confirm             = 0x14,
    RM_Pid_MLME_SET_confirm              = 0x13,
    RM_Pid_MLME_START_confirm            = 0x15,

    /*
     * Fatal Error Indication PrimitiveId
     */
    RM_Pid_Warning_indication            = 0x2A,
    RM_Pid_FatalError_indication         = 0x2B,
#else
    RM_Pid_MCPS_DATA_confirm             = 0x50,
    RM_Pid_MCPS_DATA_indication          = 0x51,
    RM_Pid_MCPS_PURGE_confirm            = 0x52,
    RM_Pid_MCPS_EDFE_DATA_confirm        = 0x53,
    RM_Pid_MCPS_SUBSCRIPTION_indication  = 0x54,
    RM_Pid_MLME_COMM_STATUS_indication   = 0x56,
    RM_Pid_MLME_GET_confirm              = 0x59,
    RM_Pid_MLME_POLL_confirm             = 0x5d,
    RM_Pid_MLME_RESET_confirm            = 0x5e,
    RM_Pid_MLME_SCAN_confirm             = 0x60,
    RM_Pid_MLME_SET_confirm              = 0x61,
    RM_Pid_MLME_START_confirm            = 0x62,
    RM_Pid_MLME_WSASYNCFRAME_request     = 0x63,
    RM_Pid_MLME_WSASYNCFRAME_confirm     = 0x64,
    RM_Pid_MLME_WSASYNCFRAME_indication  = 0x65,
    RM_Pid_MCPS_EAPOL_indication         = 0x66,
    RM_Pid_MCPS_ACKRSL_indication        = 0x67,

    /*
     * Fatal Error Indication PrimitiveId
     */
    RM_Pid_Warning_indication            = 0x6e,
    RM_Pid_FatalError_indication         = 0x6f,
#endif /* R_HYBRID_PLC_RF */

    /*
     * reserved PrimitiveId
     */
    RM_Pid_Reserved                      = 0xa0
} RmPrimitiveIdEnumT;
typedef unsigned char RmPrimitiveIdT;

#ifdef R_HYBRID_PLC_RF
#define RM_MAC_REQ_MASK 0x00
#define RM_MAC_CFM_MASK 0x10
#define RM_MAC_IND_MASK 0x20
#define RM_MAC_CMD_MASK 0x0F
#endif

/*
 * Message basic format
 */
typedef RoaMsgHdrT RmMsgHdrT;

#define mm_mplId mm_Hdr.osMsgHeader.mem_id
#define mm_tskId mm_Hdr.osMsgHeader.task_id
#define mm_prmId mm_Hdr.osMsgHeader.param_id
#define mm_len   mm_Hdr.rm_len

#define RmMsgInitialize(pMsg, prmid, len) do { \
        ((RmMsgHdrT*)(pMsg))->osMsgHeader.param_id = (unsigned char)(prmid); \
        ((RmMsgHdrT*)(pMsg))->rm_len = (unsigned short)(len); \
} while (0)


/******************************************************************************
 *  Copyright (C) 2014-2016 Renesas Electronics Corporation.
 *****************************************************************************/
#endif // #ifndef __RM_mac_msg_H
