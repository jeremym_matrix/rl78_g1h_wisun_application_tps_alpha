/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2020 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

/**
 * @file r_nd_cache.h
 * @version 1.0
 * @brief The public API of the IP neighbor cache based on received address registrations (via NS with ARO)
 */

#ifndef R_ND_CACHE_H
#define R_ND_CACHE_H

#include "r_nwk_api_base.h"
#include "sys/clock.h"

/******************************************************************************
   Macro definitions
******************************************************************************/


/******************************************************************************
   Typedef definitions
******************************************************************************/


typedef enum
{
    R_ND_DEVICE_TYPE_FFN = 0,
#if R_LFN_PARENTING_ENABLED
    R_ND_DEVICE_TYPE_LFN = 1,
#endif
} r_nd_addr_reg_device_type_t;

/**
 * IPv6 address registration entry
 */
typedef struct
{
    r_ipv6addr_t                ipAddress;        //!< The registered IP address
    r_eui64_t                   llAddress;        //!< The link-layer address that registered the IP address
    clock_time_t                expirationTime;   //!< Expiration timestamp of this entry
    uint16_t                    lifetimeMinutes;  //!< The lifetime for this address registration in minutes
    r_nd_addr_reg_device_type_t deviceType;       //!< The device type of this entry
#if (R_WISUN_FAN_VERSION >= 110)
    clock_time_t                nextRouteRefresh; //!< Timestamp of the next RPL route refresh on behalf of this LFN
#endif
} r_nd_addr_reg_entry_t;

/******************************************************************************
   Exported global variables
******************************************************************************/

/******************************************************************************
   Exported global functions (to be accessed by other files)
******************************************************************************/

void R_ND_Cache_Init(void* vp_nwkGlobal);
void R_ND_Cache_Reset(void* vp_nwkGlobal);

const r_eui64_t* R_ND_Cache_Lookup(const r_ipv6addr_t* ip_addr, void* vp_nwkGlobal);

#if R_LFN_PARENTING_ENABLED
/**
 * Return true, if the device with the specified EUI-64 is known to be an LFN. Otherwise, return false.
 */
r_boolean_t R_ND_Cache_IsLfn(const r_eui64_t* eui64, void* vp_nwkGlobal);
#endif
/**
 * Create or update a neighbor cache entry for the Full Function Node (FFN) using the specified address
 * @param ipAddr The IPv6 address that was registered
 * @param macAddr The MAC address corresponding to the address registration
 * @param lifetimeMinutes The lifetime of the address registration in minutes. The corresponding neighbor cache entry
 * cannot be evicted until this lifetime has expired
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the ND cache was successfully updated. Appropriate error code otherwise
 */
r_result_t R_ND_Cache_RegisterFFN(const r_ipv6addr_t* ipAddr, const r_eui64_t* macAddr, uint16_t lifetimeMinutes, void* vp_nwkGlobal);

#if (R_WISUN_FAN_VERSION >= 110)
/**
 * Create or update a neighbor cache entry for the Limited Function Node (LFN) using the specified address
 * @param ipAddr The IPv6 address that was registered
 * @param macAddr The MAC address corresponding to the address registration
 * @param lifetimeMinutes The lifetime of the address registration in minutes. The corresponding neighbor cache entry
 * cannot be evicted until this lifetime has expired
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the ND cache was successfully updated. Appropriate error code otherwise
 */
r_result_t R_ND_Cache_RegisterLFN(const r_ipv6addr_t* ipAddr, const r_eui64_t* macAddr, uint16_t lifetimeMinutes, void* vp_nwkGlobal);
#endif

/**
 * Refresh the lifetime of an existing address registration to the maximum lifetime
 * @details This function should be called whenever IP unicast traffic is received by the registered address.
 * @param ipAddr The IPv6 address that was registered
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the registration exists and the lifetime was successfully updated. Appropriate error
 * code otherwise
 */
r_result_t R_ND_Cache_Refresh(const r_ipv6addr_t* ipAddr, void* vp_nwkGlobal);

#if (R_WISUN_FAN_VERSION >= 110) && !R_LEAF_NODE_ENABLED
/**
 * Reset the DAO interval of an existing address registration according to LFN_DAO_PROXY_THRESHOLD
 * @details This function should be called after sending a DAO on behalf of the respective LFN child or when the
 * corresponding DAO-ACK is received.
 * @param ipAddr The IPv6 address if the LFN child
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the registration exists and the DAO interval was reset. Appropriate error code otherwise.
 */
r_result_t R_ND_Cache_ResetNextRouteRefreshTime(const r_ipv6addr_t* ipAddr, void* vp_nwkGlobal);

/**
 * Refresh the source route of an LFN child. If we are the BR, the route lifetime is refreshed. If we are a RN, a DAO
 * to the BR is scheduled to refresh the route lifetime on behalf of the LFN child.
 * @param ipAddr The IPv6 address of the LFN child whose route should be refreshed
 * @param vp_nwkGlobal The global NWK information structure
 * @return R_RESULT_SUCCESS if the route of the LFN was successfully refreshed or a DAO was scheduled. Appropriate error code otherwise.
 */
r_result_t R_ND_Cache_RefreshLfnRoute(const r_ipv6addr_t* ipAddr, void* vp_nwkGlobal);

/**
 * Refresh the source routes of all LFN children whose LFN_DAO_PROXY_THRESHOLD is reached.
 * @details This function may be called periodically to ensure that the routes of LFN children do not expire on the BR
 * @param vp_nwkGlobal The global NWK information structure
 */
void R_ND_Cache_RefreshLfnRoutes(void* vp_nwkGlobal);
#endif /* (R_WISUN_FAN_VERSION >= 110) && !R_LEAF_NODE_ENABLED */

/**
 * Remove the specified IP address registration from the neighbor cache
 * @details This function does not honor the remaining lifetime of the address registration or other eviction criteria
 * @param ipAddr The IPv6 address that was registered
 * @param vp_nwkGlobal The global NWK information structure
 */
void R_ND_Cache_Deregister(const r_ipv6addr_t* ipAddr, void* vp_nwkGlobal);

/**
 * Retrieve the first entry from the neighbor cache
 * @param vp_nwkGlobal The global NWK information structure
 * @return
 */
r_nd_addr_reg_entry_t* R_ND_Cache_FirstEntry(void* vp_nwkGlobal);

/**
 * Retrieve the next entry from the neighbor cache.
 * @param current The current neighbor whose successor should be retrieved from the cache.
 * @param vp_nwkGlobal The global NWK information structure
 * @return The subsequent entry of the given entry if it exists; NULL if there are no further entries
 * in the cache or the input arguments were invalid.
 */
r_nd_addr_reg_entry_t* R_ND_Cache_NextEntry(r_nd_addr_reg_entry_t* current, void* vp_nwkGlobal);

#endif /* R_ND_CACHE_H */
