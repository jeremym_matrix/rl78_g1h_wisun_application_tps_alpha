/******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corp. and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corp. and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORP. NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *****************************************************************************/
#ifndef R_SCHEDULE_UTILS_H
#define R_SCHEDULE_UTILS_H
/******************************************************************************
 *  File Name       : r_schedule_utils.h
 *  Description     : Functionality related to computed schedules and schedule Information Elements (IEs)
 ******************************************************************************
 *  Copyright (C) 2019 Renesas Electronics
 ******************************************************************************/

#include "r_mac_ie.h"
#include "r_header_utils.h"

/******************************************************************************
    Macro definitions
******************************************************************************/


/******************************************************************************
    Type definitions
******************************************************************************/

R_HEADER_UTILS_PRAGMA_PACK_1

/**
 * Unicast schedule that is computed from an r_ie_wp_unicast_schedule_t object by applying exclusion information and
 * converting regulatory domain information to an explicit channel configuration.
 */
typedef struct
{
    uint8_t  dwell_interval;                                  //!< Time in milliseconds during which the node is active on each channel in the hopping schedule
    uint8_t  clock_drift;                                     //!< Worst case drift of the clock used to measure its frequency hopping dwell interval
    uint8_t  timing_accuracy;                                 //!< Accuracy of time values generated by the node in 10ms
    r_ie_wp_schedule_channel_function_t channel_function : 3; //!< Channel Function
    uint32_t ch0;                                             //!< Channel 0 frequency in kHz
    r_ie_wp_schedule_channel_spacing_t channel_spacing : 4;   //!< Channel Spacing
    uint16_t no_of_channels;                                  //!< Number of channels in the channel plan (including any excluded channels)
    uint16_t fixed_channel;                                   //!< The single channel used by the transmitting node
    uint8_t  channel_hop_count;                               //!< Number of channels in the computed channel hop list
    uint8_t  channel_hop_list[];                              //!< The computed channel hop sequence (either defined explicitly by vendor or implicitly by channel function and exclusions)
} r_computed_unicast_schedule_t;

/**
 * Broadcast schedule that is computed from an r_ie_wp_broadcast_schedule_t object (analogous to r_computed_unicast_schedule_t).
 */
typedef struct
{
    uint32_t broadcast_interval;            //!< Time in milliseconds during which the node is active on each channel in the hopping schedule
    uint16_t broadcast_schedule_identifier; //!< Seed value for the FAN channel functions
    r_computed_unicast_schedule_t* us;      //!< Shared fields with computed unicast schedule
} r_computed_broadcast_schedule_t;

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

/******************************************************************************
    Function definitions
******************************************************************************/

/******************************************************************************************************************//**
 * Convert a broadcast schedule information element to an r_computed_broadcast_schedule_t, which is dynamically allocated.
 * @param bs_ie A pointer to the broadcast schedule IE that the computed schedule is created from.
 * @param[out] out A double pointer to the computed schedule.
 * @param little_endian True, if the schedule IE is the unmodified result of the R_IE_WP_Parse function (so that its
 * fields are represented in little-endian). False, if the schedule fields have been converted to the local byte order.
 * @return Status code to indicate success or failure.
 *********************************************************************************************************************/
r_result_t R_ScheduleUtils_ComputeBroadcastSchedule(const r_ie_wp_broadcast_schedule_t* bs_ie, r_computed_broadcast_schedule_t** out);

/******************************************************************************************************************//**
 * Convert a unicast schedule information element to an r_computed_unicast_schedule_t, which is dynamically allocated
 * @param us_ie The parsed unicast schedule IE that the computed schedule is created from.
 * @param[out] out A double pointer to the computed schedule. The pointer is NOT modified if the function does not
 * succeed.
 * @retval R_RESULT_SUCCESS if the computed schedule was created successfully.
 * @retval R_RESULT_INVALID_PARAMETER if the passed schedule IE is invalid.
 * @retval R_RESULT_INSUFFICIENT_BUFFER if there was not enough heap memory left to create the computed schedule.
 *********************************************************************************************************************/
r_result_t R_ScheduleUtils_ComputeUnicastSchedule(const r_ie_wp_unicast_schedule_t* us_ie, r_computed_unicast_schedule_t** out);

/******************************************************************************************************************//**
 * Free the memory of the given broadcast schedule as well as the contained unicast schedule if they exist and set its
 * pointer to NULL afterwards.
 * @param scheduleAddress Double pointer to the broadcast schedule that should be freed.
 *********************************************************************************************************************/
void R_ScheduleUtils_ClearBroadcastSchedule(r_computed_broadcast_schedule_t** scheduleAddress);

/******************************************************************************************************************//**
 * Free the memory of the given unicast schedule if it exists and set its pointer to NULL afterwards.
 * @param scheduleAddress Double pointer to the unicast schedule that should be freed.
 *********************************************************************************************************************/
void R_ScheduleUtils_ClearUnicastSchedule(r_computed_unicast_schedule_t** scheduleAddress);

/******************************************************************************************************************//**
 * Dynamically allocate memory for the external data fields of the specified schedule and copy the existing values to
 * the new memory block.
 * @param schedule The schedule IE for which the external data should be set.
 *********************************************************************************************************************/
r_result_t R_ScheduleUtils_IE_CreateExternalData(r_ie_wp_unicast_schedule_t* schedule);

/******************************************************************************************************************//**
 * Clear all external data fields of the specified schedule and free any allocated memory for the external data.
 * @param schedule The schedule IE whose external data should be cleared.
 *********************************************************************************************************************/
void R_ScheduleUtils_IE_ClearExternalData(r_ie_wp_unicast_schedule_t* schedule);

/******************************************************************************************************************//**
 * Determine the overall size of all external data fields within a schedule. This function is intended to compute the
 * amount of memory that must be dynamically allocated for a schedule after parsing.
 * @param schedule The schedule whose external data size should be determined.
 * @return The overall size of all external data fields within the specified schedule in bytes.
 *********************************************************************************************************************/
uint16_t R_ScheduleUtils_IE_ExternalDataSize(r_ie_wp_unicast_schedule_t* schedule);

/******************************************************************************************************************//**
 * Convert excluded channel ranges represented as raw bytes in little-endian as specified in FANSPEC to an array of
 * r_ie_wp_excluded_channel_range_t, which must be appropriately sized.
 * @param in The input buffer that contains the raw bytes of the excluded channel ranges in little-endian.
 * @param num_ranges The number of excluded channel ranges.
 * @param[out] out The output buffer where the excluded channel ranges are written to.
 *********************************************************************************************************************/
void R_ScheduleUtils_IE_ExcludedRangesFromBytes(const uint8_t* in, uint8_t num_ranges, r_ie_wp_excluded_channel_range_t* out);

#endif /* R_SCHEDULE_UTILS_H */
