/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2019 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

#include "r_auth_config.h"

#if R_BR_AUTHENTICATOR_ENABLED && R_EAP_TLS_SERVER_ENABLED
#include "r_auth_types.h"
#include "r_auth_br_eap_tls.h"
#include "r_auth_br.h"
#include "sys/clock.h"

#include "r_impl_utils.h"
#undef ARRAY_SIZE  /* redefined in WPA includes */

// R_LOG_PREFIX is processed by RLog code generator
#define R_LOG_PREFIX AUTH
#include "r_log_internal.h"
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
#include "r_loggen_r_auth_br_eap_tls.h"
#endif

// WPA includes
#include "includes.h"
#include "common.h"
#include "crypto/tls.h"
#include "eap_server/eap.h"

#define CURRENT_SUP (&ctx->supplicants[ctx->current_sup])

/** WPA context structure for EAP handshakes */
typedef struct r_eap_tls_srv_wpa_ctx_s
{
    struct eap_eapol_interface* eap_if;
    struct eap_sm*              eap;
    struct eap_config           eap_conf;
} r_eap_tls_srv_wpa_ctx_t;

/**
 * WPA callback function to get the user name (irrelevant for EAP-TLS)
 * @param ctx the WPA context
 * @param identity unused
 * @param identity_len unused
 * @param phase2 unused
 * @param user the eap_user structure
 * @return 0 on success
 */
static int eapcb_get_eap_user(void* ctx, const u8* identity, size_t identity_len, int phase2, struct eap_user* user)
{
    if ((ctx == NULL) || (identity == NULL) || (user == NULL))
    {
        return -1;
    }
#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_74((uint8_t)identity[0], (uint32_t)identity_len, (int32_t)phase2);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif
    memset(user, 0, sizeof(*user));
    user->methods[0].vendor = EAP_VENDOR_IETF;
    user->methods[0].method = EAP_TYPE_TLS;

    // Here, the identity could be checked but nothing is specified in the Wi-SUN standard

    return 0;
}

/**
 * WPA callback to return the request ID text
 * @param ctx the WPA context
 * @param len set to 0
 * @return NULL
 */
static const char* eapcb_get_eap_req_id_text(void* ctx, size_t* len)
{
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_93();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    if (len)
    {
        *len = 0;
    }

    return NULL;
}

static struct eapol_callbacks eap_eapol_callbacks = {
    .get_eap_user        = eapcb_get_eap_user,
    .get_eap_req_id_text = eapcb_get_eap_req_id_text,
    // others are unused
};

/**
 * Create the EAP context
 * @param ctx The EAP-TLS context structure
 * @return NULL on error, the EAP context on success
 */
static r_eap_tls_srv_wpa_ctx_t* eap_create(r_eap_tls_srv_ctx_t* ctx)
{
    if (ctx == NULL || eap_server_identity_register() == -1 || eap_server_tls_register() == -1)
    {
        return NULL;
    }

    struct tls_config tconf = { 0 };
    struct tls_connection_params tparams = { 0 };
    void* tls_ctx = tls_init(&tconf);
    if (tls_ctx == NULL)
    {
        return NULL;
    }
    tparams.private_key = NULL;
    tparams.client_cert = (void*)ctx->encoded_certs;
    if (tls_global_set_params(tls_ctx, &tparams) || tls_global_set_verify(tls_ctx, 0))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_132();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        tls_deinit(tls_ctx);
        return NULL;
    }

    r_eap_tls_srv_wpa_ctx_t* wpa = os_zalloc(sizeof(r_eap_tls_srv_wpa_ctx_t));
    if (!wpa)
    {
        tls_deinit(tls_ctx);
        return NULL;
    }

    wpa->eap_conf.eap_server = 1;
    wpa->eap_conf.ssl_ctx = tls_ctx;
    wpa->eap_conf.fragment_size = R_AUTH_EAP_FRAGMENT_SIZE;

    wpa->eap = eap_server_sm_init(wpa, &eap_eapol_callbacks, &wpa->eap_conf);
    if (wpa->eap == NULL)
    {
        tls_deinit(tls_ctx);
        os_free(wpa);
        return NULL;
    }

    wpa->eap_if = eap_get_interface(wpa->eap);

    /* Enable "port" and request EAP to start authentication. */
    wpa->eap_if->portEnabled = TRUE;
    wpa->eap_if->eapRestart = TRUE;

    return wpa;
}

/** Free all memory allocated by the specified WPA/EAP context. Pointer must not be used afterwards! */
static void eap_free(r_eap_tls_srv_wpa_ctx_t* wpa)
{
    if (wpa)
    {
        eap_server_sm_deinit(wpa->eap);
        tls_deinit(wpa->eap_conf.ssl_ctx);
        os_free(wpa);
    }
}

/**
 * Free/Clear the whole supplicant context; This includes freeing the EAP context
 * @param sup_ctx The context structure of the EAP-TLS supplicant that should be cleared
 */
static void sup_ctx_free(r_eap_tls_sup_t* sup_ctx)
{
    if (sup_ctx == NULL || MEMISZERO_S(sup_ctx))
    {
        return;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_187(sup_ctx->eui64);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    eap_free(sup_ctx->wpa); // Free WPA context (if existing)
    MEMZERO_S(sup_ctx);     // Clear every field in supplicant context struct
}

/**
 * Try to switch to the context of the specified supplicant
 * @param eui64 The EUI-64 of the
 * @param ctx The EAP-TLS server context
 * @return Non-zero on success; Zero on failure
 */
static int sup_ctx_switch(const r_eui64_t* eui64, r_eap_tls_srv_ctx_t* ctx)
{
    uint16_t firstUnusedIndex = UINT16_MAX;
    uint16_t oldestRxIndex = UINT16_MAX;
    uint32_t oldestRxTime = UINT32_MAX;
    for (uint16_t i = 0; i < ARRAY_SIZE(ctx->supplicants); i++)
    {
        r_eap_tls_sup_t* sup = &ctx->supplicants[i];
        if (MEMEQUAL_A(sup->eui64, eui64->bytes))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_208(eui64->bytes);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            ctx->current_sup = i;
            return 1;
        }
        if (sup->wpa == NULL)
        {
            if (firstUnusedIndex == UINT16_MAX)
            {
                firstUnusedIndex = i;  // Remember first unused context slot (no WPA context)
            }
        }
        else if (sup->last_rx < oldestRxTime)
        {
            oldestRxIndex = i;
            oldestRxTime = sup->last_rx;
        }
    }

    /* Select unused context slot (if found) */
    if (firstUnusedIndex != UINT16_MAX)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_229(firstUnusedIndex, eui64->bytes);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        ctx->current_sup = firstUnusedIndex;
        return 2;
    }

    /* Determine if one of the active EAP-TLS handshakes has timed out and select this context slot */
    if (oldestRxTime != UINT32_MAX && (clock_seconds() - oldestRxTime) > R_EAP_TLS_SRV_SUP_TIMEOUT)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_237(ctx->supplicants[oldestRxIndex].eui64, eui64->bytes);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        sup_ctx_free(&ctx->supplicants[oldestRxIndex]);
        ctx->current_sup = oldestRxIndex;
        return 3;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
    r_loggen_243(eui64->bytes);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
    return 0;
}

static r_eap_tls_srv_result_t sup_ctx_create(const uint8_t* sup_eui64, r_eap_tls_srv_ctx_t* ctx, void* auth_cb_ctx)
{
    if (!sup_ctx_switch((const r_eui64_t*)sup_eui64, ctx))
    {
        return R_EAP_TLS_SRV_RESULT_NO_FREE_SUP_SLOTS;  // No SUP context slot available (all slots already allocated for ongoing handshakes)
    }

    /* Destroy/Free supplicant context (may be restored from previous attempt) and create new one */
    sup_ctx_free(CURRENT_SUP);

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_257(sup_eui64, ctx->current_sup);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    CURRENT_SUP->wpa = eap_create(ctx);
    if (CURRENT_SUP->wpa == NULL)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_261(sup_eui64);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return R_EAP_TLS_SRV_RESULT_INTERNAL_ERROR;
    }

    ctx->auth_ctx = auth_cb_ctx;  // Store auth context pointer for callbacks
    MEMCPY_A(CURRENT_SUP->eui64, sup_eui64);
    CURRENT_SUP->last_rx = clock_seconds();
    return R_EAP_TLS_SRV_RESULT_SUCCESS;
}

/**
 * Advance the EAP state machine
 * @param ctx The EAP-TLS context structure
 * @return 1 if the state machine advanced successfully
 */
static int eap_step(r_eap_tls_srv_ctx_t* ctx)
{
    if ((ctx == NULL) || (CURRENT_SUP->wpa == NULL))
    {
        return -1;
    }

    r_eap_tls_srv_wpa_ctx_t* wpa = CURRENT_SUP->wpa;
    int process = 0;
    int eapHandshakeDone = 0;
    int eapHandshakeFailed = 0;
    int res = eap_server_sm_step(wpa->eap);

    if (wpa->eap_if->eapReq)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_291();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        process = 1;
        wpa->eap_if->eapReq = FALSE;
    }

    if (wpa->eap_if->eapSuccess)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_298();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        process = 1;
        wpa->eap_if->eapSuccess = FALSE;
        if (wpa->eap_if->eapKeyAvailable && wpa->eap_if->eapKeyDataLen >= R_AUTH_PMK_SIZE)
        {
            eapHandshakeDone = 1;  // Consider handshake as succeeded
        }
        else
        {
            eapHandshakeFailed = 1;  // Consider handshake as failed since no PMK is available
        }
    }

    if (wpa->eap_if->eapFail)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_313();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        process = 1;
        wpa->eap_if->eapFail = FALSE;
        eapHandshakeFailed = 1;
    }

    if (process && wpa->eap_if->eapReqData)
    {
        res = R_AUTH_BR_SendEapol(ctx->auth_ctx, R_EAPOL_TYPE_CONVERT_FROM_EAP, wpabuf_head(wpa->eap_if->eapReqData),
                                  (uint16_t)wpabuf_len(wpa->eap_if->eapReqData));

        /* Translate result code from R_AUTH_BR_SendEapol */
        if (res != 0)
        {
            res = 0;  // error
        }
        else
        {
            res = 1;  // success
        }
    }

    if (eapHandshakeFailed)
    {
        uint8_t eui64[8];
        MEMCPY_A(eui64, CURRENT_SUP->eui64);
        sup_ctx_free(CURRENT_SUP);
        R_AUTH_BR_EapTlsFailureCallback(eui64, ctx->auth_ctx);
    }

    if (eapHandshakeDone)
    {
        uint8_t eui64[8];
        MEMCPY_A(eui64, CURRENT_SUP->eui64);
        uint8_t pmk[R_AUTH_PMK_SIZE];
        MEMCPY_A(pmk, wpa->eap_if->eapKeyData);
        sup_ctx_free(CURRENT_SUP);
        R_AUTH_BR_EapTlsDoneCallback(eui64, pmk, ctx->auth_ctx);
    }

    return res;
}

void R_EAP_TLS_SRV_Init(const uint8_t* encoded_certs, r_eap_tls_srv_ctx_t* ctx)
{
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_358();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    ctx->encoded_certs = encoded_certs;

}

void R_EAP_TLS_SRV_Reset(r_eap_tls_srv_ctx_t* ctx)
{
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_365();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    for (size_t i = 0; i < ARRAY_SIZE(ctx->supplicants); i++)
    {
        sup_ctx_free(&ctx->supplicants[i]);
    }
    eap_server_unregister_methods();
    MEMZERO_S(ctx);
}

r_eap_tls_srv_result_t R_EAP_TLS_SRV_Start(const uint8_t* sup_eui64, r_eap_tls_srv_ctx_t* ctx, void* auth_cb_ctx)
{
    r_eap_tls_srv_result_t srv_result = sup_ctx_create(sup_eui64, ctx, auth_cb_ctx);

    if (srv_result != R_EAP_TLS_SRV_RESULT_SUCCESS)
    {
        return srv_result;
    }

    if (eap_step(ctx) != 1)
    {
        return R_EAP_TLS_SRV_RESULT_HANDSHAKE_FAILED;
    }
    return R_EAP_TLS_SRV_RESULT_SUCCESS;
}

r_eap_tls_srv_result_t R_EAP_TLS_SRV_ProcessMessage(const uint8_t* src, const void* data, uint16_t size, r_eap_tls_srv_ctx_t* ctx)
{
    if (!sup_ctx_switch((const r_eui64_t*)src, ctx))
    {
        return R_EAP_TLS_SRV_RESULT_INVALID_REQUEST;  // No SUP context slot found for this address
    }
    if (!CURRENT_SUP->wpa)
    {
        return R_EAP_TLS_SRV_RESULT_INTERNAL_ERROR;
    }

    CURRENT_SUP->last_rx = clock_seconds();
    wpabuf_free(CURRENT_SUP->wpa->eap_if->eapRespData);
    CURRENT_SUP->wpa->eap_if->eapRespData = wpabuf_alloc_ext_data_without_ownership((uint8_t*)data + 4, size - 4U);
    if (CURRENT_SUP->wpa->eap_if->eapRespData)
    {
        CURRENT_SUP->wpa->eap_if->eapResp = TRUE;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_409(src);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    if (eap_step(ctx) != 1)
    {
        return R_EAP_TLS_SRV_RESULT_HANDSHAKE_FAILED;
    }
    return R_EAP_TLS_SRV_RESULT_SUCCESS;
}

#endif /* R_BR_AUTHENTICATOR_ENABLED && R_EAP_TLS_SERVER_ENABLED */
