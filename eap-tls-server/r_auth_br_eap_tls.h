/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2019 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

#ifndef R_AUTH_BR_EAP_TLS_SRV_H
#define R_AUTH_BR_EAP_TLS_SRV_H

#if R_BR_AUTHENTICATOR_ENABLED && R_EAP_TLS_SERVER_ENABLED

struct r_eap_tls_srv_wpa_ctx_s;  // Undefined struct to hide implementation details

/**
 * The context of an individual EAP-TLS supplicant (client)
 */
typedef struct r_eap_tls_sup_s
{
    uint8_t  eui64[8];                   // The EUI-64 of the supplicant
    uint32_t last_rx;                    // Timestamp of latest message from the supplicant
    struct r_eap_tls_srv_wpa_ctx_s* wpa; // The WPA context structures for the EAP-TLS handshake
} r_eap_tls_sup_t;

/**
 * The context of the whole EAP-TLS server module
 */
typedef struct r_eap_tls_ctx_s
{
    const uint8_t*  encoded_certs; // The server certificate for the TLS handshake as encoded by the encode-certs tool
    void*           auth_ctx;      // The context structure of the authentication module (required for callbacks)
    uint16_t        current_sup;
    r_eap_tls_sup_t supplicants[R_EAP_TLS_SRV_MAX_CONCURRENT_SUPPLICANTS];
} r_eap_tls_srv_ctx_t;

/**
 * Result codes for EAP-TLS server operations
 */
typedef enum
{
    R_EAP_TLS_SRV_RESULT_SUCCESS           = 0x00, //!< Operation succeeded
    R_EAP_TLS_SRV_RESULT_HANDSHAKE_FAILED  = 0x01, //!< Error during handshake with supplicant
    R_EAP_TLS_SRV_RESULT_INTERNAL_ERROR    = 0x02, //!< Internal error occurred
    R_EAP_TLS_SRV_RESULT_NO_FREE_SUP_SLOTS = 0x03, //!< Could not start handshake since all SUP slots are already allocated (temporary error!)
    R_EAP_TLS_SRV_RESULT_INVALID_REQUEST   = 0x04, //!< Invalid request -> Check arguments and state
} r_eap_tls_srv_result_t;

/**
 * Initialize the EAP-TLS server module
 * @warning This function MUST be called before any other function of this module (except for reset) may be called
 * @param encoded_certs The certificates as encoded by the encode-certs tool
 * @param ctx The EAP-TLS server context
 */
void R_EAP_TLS_SRV_Init(const uint8_t* encoded_certs, r_eap_tls_srv_ctx_t* ctx);

/**
 * Reset the EAP-TLS server module by freeing all memory and deleting all internal state
 * @details Any ongoing EAP-TLS handshakes are terminated
 * @param ctx The EAP-TLS server context
 * @details This function does not require a subsequent call of R_EAP_TLS_SRV_Init if it has been called before
 */
void R_EAP_TLS_SRV_Reset(r_eap_tls_srv_ctx_t* ctx);

/**
 * Start a new EAP-TLS handshake with the specified supplicant
 * @param sup_eui64 The EUI-64 address of the supplicant
 * @param ctx The EAP-TLS server context
 * @param auth_cb_ctx The context structure expected by authentication module callbacks
 * @return 0 on success; Appropriate error code otherwise
 */
r_eap_tls_srv_result_t R_EAP_TLS_SRV_Start(const uint8_t* sup_eui64, r_eap_tls_srv_ctx_t* ctx, void* auth_cb_ctx);

/**
 * Process an incoming EAPOL packet assuming that it contains the next EAP-TLS message from the current supplicant
 * @param data The EAPOL payload
 * @param size The size of the EAPOL payload in bytes
 * @param ctx The EAP-TLS server context
 * @return 0 on success; Appropriate error code otherwise
 */
r_eap_tls_srv_result_t R_EAP_TLS_SRV_ProcessMessage(const uint8_t* src, const void* data, uint16_t size, r_eap_tls_srv_ctx_t* ctx);

#endif /* R_BR_AUTHENTICATOR_ENABLED && R_EAP_TLS_SERVER_ENABLED */

#endif /* R_AUTH_BR_EAP_TLS_SRV_H */
