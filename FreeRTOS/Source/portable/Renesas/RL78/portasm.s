;/*
; * FreeRTOS Kernel V10.0.0
; * Copyright (C) 2017 Amazon.com, Inc. or its affiliates.  All Rights Reserved.
; *
; * Permission is hereby granted, free of charge, to any person obtaining a copy of
; * this software and associated documentation files (the "Software"), to deal in
; * the Software without restriction, including without limitation the rights to
; * use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of
; * the Software, and to permit persons to whom the Software is furnished to do so,
; * subject to the following conditions:
; *
; * The above copyright notice and this permission notice shall be included in all
; * copies or substantial portions of the Software. If you wish to use our Amazon
; * FreeRTOS name, please do so in a fair use way that does not cause confusion.
; *
; * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
; * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS
; * FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR
; * COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER
; * IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
; * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
; *
; * http://www.FreeRTOS.org
; * http://aws.amazon.com/freertos
; *
; * 1 tab == 4 spaces!
; */

#include "ISR_Support.h"

#define CS                    0xFFFFC
#define ES                    0xFFFFD

    PUBLIC    _vPortYield
    PUBLIC    _vPortStartFirstTask
    PUBLIC    _vPortTickISR

    EXTERN    _vTaskSwitchContext
    EXTERN    _xTaskIncrementTick

    PUBLIC ___interrupt_0x32
    PUBLIC ___interrupt_0x7E

; FreeRTOS yield handler.  This is installed as the BRK software interrupt
; handler.
        SECTION `.text`:CODE:NOROOT(0)
        CALL_GRAPH_ROOT _vPortYield, "interrupt"
        CODE
_vPortYield:
___interrupt_0x7E:
	portSAVE_CONTEXT		        ; Save the context of the current task.
	call      f:_vTaskSwitchContext         ; Call the scheduler to select the next task.
	portRESTORE_CONTEXT		        ; Restore the context of the next task to run.
	retb
        REQUIRE ___interrupt_0x7E


; Starts the scheduler by restoring the context of the task that will execute
; first.
        SECTION `.text`:CODE:NOROOT(0)
_vPortStartFirstTask:
	portRESTORE_CONTEXT	            ; Restore the context of whichever task the ...
	reti				    ; An interrupt stack frame is used so the task
                                            ; is started using a RETI instruction.

; FreeRTOS tick handler.  This is installed as the interval timer interrupt
; handler.
        SECTION `.text`:CODE:NOROOT(0)
        CALL_GRAPH_ROOT _vPortTickISR, "interrupt"
        CODE
_vPortTickISR:
___interrupt_0x32:
	portSAVE_CONTEXT		       ; Save the context of the current task.
	call	f:_xTaskIncrementTick          ; Call the timer tick function.
	cmpw	ax, #0x00
	skz
	call	f:_vTaskSwitchContext          ; Call the scheduler to select the next task.
	portRESTORE_CONTEXT		       ; Restore the context of the next task to run.
	reti
        REQUIRE ___interrupt_0x32

        END

