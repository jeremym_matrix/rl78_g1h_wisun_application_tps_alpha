/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized.
* This software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
* PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
* DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this
* software and to discontinue the availability of this software.
* By using this software, you agree to the additional terms and
* conditions found by accessing the following link:
* http://www.renesas.com/disclaimer
******************************************************************************/

/* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.  */

/******************************************************************************
* File Name    : r_app_ip.c
* Version      : 1.00
* Device(s)    :
* Tool-Chain   : CubeSuite+
* H/W Platform :
* Description  : Sample application program
******************************************************************************
* History : DD.MM.YYYY Version Description
*         : 25.08.2014 1.00    First Release
******************************************************************************/

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
 ******************************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "r_app_main.h"
#include "r_apl_global.h"
#include "r_nwk_api.h"
#include "r_icmpv6.h"
#include "sys/clock.h"
#include "mac_intr.h"

#include "r_modem.h"
#include "r_modem_demo_msg.h"

#include "r_byte_swap.h"
#include "r_impl_utils.h"
#include "r_mac_ie.h"
#include "r_mem_tools.h"

#ifdef R_NETWORK_RACK
#include "lib/random.h"
#endif

#if R_DEV_TBU_ENABLED && __RX
#include "iodefine.h"
#endif

#if !R_DEV_DISABLE_AUTH
#include "r_auth_config.h"
#include "r_auth_sup.h"
#include "r_auth_common.h"
#include "r_auth_certs_config.h"
#if R_BR_AUTHENTICATOR_ENABLED
#include "r_auth_br.h"
#endif
#include "r_heap.h"
#endif

#if R_BORDER_ROUTER_ENABLED
#if R_DHCPV6_SERVER_ENABLED
#include "r_dhcpv6.h"
#endif
#endif

#ifdef UMM_INFO
#include "umm_malloc.h"
#endif // UMM_INFO

// R_LOG_PREFIX is processed by RLog code generator
#define R_LOG_PREFIX APP
#include "r_log_internal.h"
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
#include "r_loggen_r_app_ip.h"
#endif
#include "r_log.h"
#include "r_log_flags.h"

#if RTOS_USE == 1
#include "FreeRTOS.h"
#include "task.h"
#endif

#define STACK_NAME "Wi-SUN-FAN V1.4.1"

/******************************************************************************
   Version definitions
******************************************************************************/
#if defined(VIZMO_MODULE_28dBm)
const uint8_t FAN_STACK_VERSION[] = STACK_NAME "-VIZMO+28dBm";
#elif defined(FEM_SE2435L)
const uint8_t FAN_STACK_VERSION[] = STACK_NAME "-FEM_SE2435L";
#elif (R_WISUN_FAN_VERSION >= 110)
const uint8_t FAN_STACK_VERSION[] = STACK_NAME "-TPS1.1-Alpha";
#else
const uint8_t FAN_STACK_VERSION[] = STACK_NAME;
#endif

/******************************************************************************
   Macro definitions
******************************************************************************/
#define APP_CMD_MAC_Channel_Default (0x00)
#define APP_CMD_MAC_PanId_Default   (0xffff)

/** Example value for the UDP port of special "Kick" messages (must not interfere with Wi-SUN FAN UDP ports) */
#define KICK_DEVICE_MSG_UDP_PORT    3611

/** Example value for the payload of a "Kick" message */
#define KICK_DEVICE_MSG_PAYLOAD     { 0xAB, 0xCD, 0xEF }

#ifndef R_LOG_BUFFER_SIZE

/** The size of the static buffer for the logging framework to store log records internally */
#define R_LOG_BUFFER_SIZE 256
#endif

/** The size of the temporary buffer that is used to poll log records from the logging framework */
#define R_LOG_POLL_BUFFER_SIZE 256

/******************************************************************************
   Typedef definitions
******************************************************************************/
typedef struct
{
    uint8_t      ntfHandle;
    uint8_t      deviceType;
    char         networkName[R_NETWORK_NAME_STRING_MAX_BYTES];
    uint16_t     panId;
    uint16_t     panSize;
    uint8_t      useParent_BS_IE;

    /* The following parameters are only configurable for border routers */
    r_ipv6addr_t globalIpAddress;  //!< The GUA/LUA to be set on the BR and the local DHCP server (contains network prefix)
} r_app_config_t;

typedef struct
{
    const uint8_t __far* pCmd;
    void (* pFunc)(uint8_t* pCmd);
} r_app_cmd_func_t;

typedef union
{
    r_ie_wp_serialized_sched_t schedule;
    uint8_t placeholder[sizeof(r_ie_wp_serialized_sched_t) + R_SERIALIZED_SCHEDULE_IE_BYTES_MAX];
} r_app_schedule_buffer_t;  // Buffer for a serialized schedule that is passed between application and NWK layer

typedef union
{
    r_nwk_mac_whitelist_t whitelist;
    uint8_t               placeholder[sizeof(r_nwk_mac_whitelist_t) + R_WHITELIST_TABLE_LENGTH * sizeof(r_eui64_t)];
} r_app_whitelist_buffer_t;  // Buffer for a MAC whitelist that is passed between application and NWK layer

typedef union
{
    r_nwk_nd_cache_t nd_cache;
    uint8_t          placeholder[sizeof(r_nwk_nd_cache_t) + R_NWK_MAX_NEIGHBORS_PER_CONFIRM * sizeof(r_nwk_nd_cache_entry_t)];
} r_app_nd_cache_buffer_t;  // Buffer for the Neighbor Discovery cache that is passed between application and NWK layer

#if R_BORDER_ROUTER_ENABLED
typedef union
{
    r_nwk_routing_entries_t routingTable;
    uint8_t                 placeholder[sizeof(r_nwk_routing_entries_t) + R_NWK_MAX_ROUTES_PER_CONFIRM * sizeof(r_nwk_route_t)];
} r_app_routing_table_buffer_t;  // Buffer for the source routes that are passed between application and NWK layer
#endif

/******************************************************************************
   Imported global variables and functions (from other files)
******************************************************************************/
extern unsigned char AppHexStrToNum(unsigned char** ppBuf, void* pData, short dataSize, unsigned char isOctetStr);

extern const uint8_t* pAppGetMacAddr(void);

#if R_DEV_TBU_ENABLED && __RX
extern uint8_t rstr_handle;
#endif

/******************************************************************************
   Exported global variables and functions (to be accessed by other files)
******************************************************************************/
void AppCmd_Init();

/******************************************************************************
   Private global variables
******************************************************************************/
static r_apl_global_t* p_aplGlobal;
static r_app_config_t AppCmdConfig;

static uint16_t featureMask = 0
#if R_BORDER_ROUTER_ENABLED
                              | (1 << 0)
#endif
#if R_BR_AUTHENTICATOR_ENABLED
                              | (1 << 1)
#endif
#if R_DHCPV6_SERVER_ENABLED
                              | (1 << 2)
#endif
#if R_MPL_SEED_ENABLED
                              | (1 << 3)
#endif
#if R_LEAF_NODE_ENABLED
                              | (1 << 4)
#endif
#if (!R_BORDER_ROUTER_ENABLED && !R_LEAF_NODE_ENABLED)
                              | (1 << 5)
#endif
#if R_DEV_TBU_ENABLED && __RX
                              | (1 << 8)
#endif
#if R_DEV_FAST_JOIN
                              | (1 << 9)
#endif
#if R_DEV_DISABLE_SECURITY
                              | (1 << 11)
#endif
#if R_DEV_DETERMINISTIC_RANDOM
                              | (1 << 12)
#endif
;

/* Command */
static const uint8_t AppCmdStr_RSTR[] = "RSTR";
static const uint8_t AppCmdStr_STARTR[] = "STARTR";
static const uint8_t AppCmdStr_IPSR[] = "IPSR";
static const uint8_t AppCmdStr_UDPSR[] = "UDPSR";
static const uint8_t AppCmdStr_ICMPSR[] = "ICMPSR";
static const uint8_t AppCmdStr_SPDR[] = "SPDR";
static const uint8_t AppCmdStr_RSMR[] = "RSMR";
static const uint8_t AppCmdStr_REVOKE_KEYS[] = "REVOKEKEYSR";
static const uint8_t AppCmdStr_REVOKE_SUPP[] = "REVOKESUPPR";
static const uint8_t AppCmdStr_DEVICEKICKR[] = "DEVICEKICKR";
static const uint8_t AppCmdStr_LEAVE_NETWORKR[] = "LEAVENETWORKR";
static const uint8_t AppCmdStr_PIB_GETR[] = "PIB_GETR";
static const uint8_t AppCmdStr_PIB_SETR[] = "PIB_SETR";
static const uint8_t AppCmdStr_IPV6PREFIX_SETR[] = "IPV6PREFIX_SETR";
static const uint8_t AppCmdStr_DEVCONF_GETR[] = "DEVCONF_GETR";
static const uint8_t AppCmdStr_DEVCONF_SETR[] = "DEVCONF_SETR";
static const uint8_t AppCmdStr_PHYFAN_GETR[] = "PHYFAN_GETR";
static const uint8_t AppCmdStr_PHYFAN_SETR[] = "PHYFAN_SETR";
static const uint8_t AppCmdStr_PHYNET_GETR[] = "PHYNET_GETR";
static const uint8_t AppCmdStr_PHYNET_SETR[] = "PHYNET_SETR";
static const uint8_t AppCmdStr_UCSCH_GETR[] = "UCSCH_GETR";
static const uint8_t AppCmdStr_UCSCH_SETR[] = "UCSCH_SETR";
static const uint8_t AppCmdStr_BCSCH_GETR[] = "BCSCH_GETR";
static const uint8_t AppCmdStr_BCSCH_SETR[] = "BCSCH_SETR";
static const uint8_t AppCmdStr_WHTLST_GETR[] = "WHTLST_GETR";
static const uint8_t AppCmdStr_WHTLST_SETR[] = "WHTLST_SETR";
static const uint8_t AppCmdStr_GTKS_GETR[] = "GTKS_GETR";
static const uint8_t AppCmdStr_GTKS_SETR[] = "GTKS_SETR";
static const uint8_t AppCmdStr_LGTKS_GETR[] = "LGTKS_GETR";
static const uint8_t AppCmdStr_LGTKS_SETR[] = "LGTKS_SETR";
static const uint8_t AppCmdStr_KEYLIFETIMES_SETR[] = "KEYLIFETIMES_SETR";
static const uint8_t AppCmdStr_CERT_SWITCHR[] = "CERT_SWITCHR";
static const uint8_t AppCmdStr_VERBOSE_SETR[] = "VERBOSE_SETR";
static const uint8_t AppCmdStr_LOGVERSION_GETR[] = "LOGVERSION_GETR";
static const uint8_t AppCmdStr_LOGENTRIES_GETR[] = "LOGENTRIES_GETR";
static const uint8_t AppCmdStr_SUBSCP_SETR[] = "SUBSCP_SETR";
static const uint8_t AppCmdStr_PAN_VERSION_INCR[] = "PAN_VERSION_INCR";
static const uint8_t AppCmdStr_RPL_ROUTES_REFRESHR[] = "RPL_ROUTES_REFRESHR";
static const uint8_t AppCmdStr_RPL_GLOBALREPAIRR[] = "RPL_GLOBALREPAIRR";
static const uint8_t AppCmdStr_ROUTELST_GETR[] = "ROUTELST_GETR";
static const uint8_t AppCmdStr_RPLINFO_GETR[] = "RPLINFO_GETR";
static const uint8_t AppCmdStr_NDCACHE_GETR[] = "NDCACHE_GETR";
static const uint8_t AppCmdStr_VERSION_GETR[] = "VERSION_GETR";
static const uint8_t AppCmdStr_STATUS_GETR[] = "STATUS_GETR";
static const uint8_t AppCmdStr_TASKSTATUS_GETR[] = "TASKSTATUS_GETR";
#ifdef UMM_INFO
static const uint8_t AppCmdStr_MEMMARK_GETR[] = "MEMMARK_GETR";
static const uint8_t AppCmdStr_MEMINFO_GETR[] = "MEMINFO_GETR";
static const uint8_t AppCmdStr_MEMMAP_GETR[] = "MEMMAP_GETR";
#endif // UMM_INFO
#if R_PHY_TYPE_CWX_M && R_MDR_ENABLED
static const uint8_t AppCmdStr_MDRSR[] = "MDRSR";
#endif // R_PHY_TYPE_CWX_M && R_MDR_ENABLED

/******************************************************************************
   Private function prototypes
******************************************************************************/
// static
static void       AppCmd_Reset(void);
static r_result_t AppCmd_LeaveNetwork();

/* Command */
static void AppCmd_ProcessCmd_RSTR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_STARTR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_IPSR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_UDPSR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_ICMPSR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_SPDR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_RSMR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_REVOKEKEYSR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_REVOKESUPPR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_DEVICEKICKR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_LEAVENETWORKR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_PIB_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_PIB_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_IPV6PREFIX_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_DEVCONF_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_DEVCONF_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_PHYFAN_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_PHYFAN_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_PHYNET_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_PHYNET_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_UCSCH_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_BCSCH_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_UCSCH_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_BCSCH_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_WHTLST_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_WHTLST_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_GTKS_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_GTKS_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_LGTKS_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_LGTKS_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_KEYLIFETIMES_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_CERT_SWITCHR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_VERBOSE_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_LOGVERSION_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_LOGENTRIES_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_SUBSCP_SETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_PAN_VERSION_INCR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_RPL_GLOBALREPAIRR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_ROUTELST_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_RPLINFO_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_NDCACHE_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_VERSION_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_STATUS_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_TASKSTATUS_GETR(uint8_t* pCmd);
#ifdef UMM_INFO
static void AppCmd_ProcessCmd_MEMMARK_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_MEMINFO_GETR(uint8_t* pCmd);
static void AppCmd_ProcessCmd_MEMMAP_GETR(uint8_t* pCmd);
#endif // UMM_INFO
#if R_PHY_TYPE_CWX_M && R_MDR_ENABLED
static void AppCmd_ProcessCmd_MDRSR(uint8_t* pCmd);
#endif // R_PHY_TYPE_CWX_M && R_MDR_ENABLED

/* Internal : sub function */
static void AppCmd_ProcessCmd_SCHEDULE_GETR(uint8_t* pCmd, uint8_t paramType);
static void AppCmd_ProcessCmd_SCHEDULE_SETR(uint8_t* pCmd, r_app_schedule_type_t paramType);

// static
static r_result_t AppCmd_HexStrToNum(uint8_t** ppBuf, void* pData, int16_t dataSize, r_boolean_t isOctetStr);

/* Internal : for output message */
static void AppPrintUdpDataInd(r_udp_data_ind_t* p_udp_ind);
static void AppPrintIcmpEchoInd(r_icmp_echo_reply_ind_t* p_icmp_echo_ind);
#if R_DEV_TBU_ENABLED && __RX
static void AppPrintSubScriptFrameInd(const r_subscription_frame_ind_t* p_subscript_frame_ind);
#endif
static void AppPrintIpDataIndication(const r_ip_data_ind_t* p_indication);
static void AppPrintBytesAsHexString(const uint8_t* bytes, size_t numBytes);
static void AppPrintIPv6Addr(const uint8_t* pIPAddr);
static void AppPrintMACAddr(const uint8_t* pMACAddr);
static void AppPrintNetworkName(const char* pNetworkName);

/* *FORMATTING-OFF* (Uncrustify has issues with struct initializers and indents them repeatedly) */
/* Command List */
static const r_app_cmd_func_t AppCmdFunc[] = {
    { AppCmdStr_RSTR,                &AppCmd_ProcessCmd_RSTR                },
    { AppCmdStr_STARTR,              &AppCmd_ProcessCmd_STARTR              },
    { AppCmdStr_IPSR,                &AppCmd_ProcessCmd_IPSR                },
    { AppCmdStr_UDPSR,               &AppCmd_ProcessCmd_UDPSR               },
    { AppCmdStr_ICMPSR,              &AppCmd_ProcessCmd_ICMPSR              },
    { AppCmdStr_SPDR,                &AppCmd_ProcessCmd_SPDR                },
    { AppCmdStr_RSMR,                &AppCmd_ProcessCmd_RSMR                },
    { AppCmdStr_REVOKE_KEYS,         &AppCmd_ProcessCmd_REVOKEKEYSR         },
    { AppCmdStr_REVOKE_SUPP,         &AppCmd_ProcessCmd_REVOKESUPPR         },
    { AppCmdStr_DEVICEKICKR,         &AppCmd_ProcessCmd_DEVICEKICKR         },
    { AppCmdStr_LEAVE_NETWORKR,      &AppCmd_ProcessCmd_LEAVENETWORKR       },
    { AppCmdStr_PIB_GETR,            &AppCmd_ProcessCmd_PIB_GETR            },
    { AppCmdStr_PIB_SETR,            &AppCmd_ProcessCmd_PIB_SETR            },
    { AppCmdStr_IPV6PREFIX_SETR,     &AppCmd_ProcessCmd_IPV6PREFIX_SETR     },
    { AppCmdStr_DEVCONF_GETR,        &AppCmd_ProcessCmd_DEVCONF_GETR        },
    { AppCmdStr_DEVCONF_SETR,        &AppCmd_ProcessCmd_DEVCONF_SETR        },
    { AppCmdStr_PHYFAN_GETR,         &AppCmd_ProcessCmd_PHYFAN_GETR         },
    { AppCmdStr_PHYFAN_SETR,         &AppCmd_ProcessCmd_PHYFAN_SETR         },
    { AppCmdStr_PHYNET_GETR,         &AppCmd_ProcessCmd_PHYNET_GETR         },
    { AppCmdStr_PHYNET_SETR,         &AppCmd_ProcessCmd_PHYNET_SETR         },
    { AppCmdStr_UCSCH_GETR,          &AppCmd_ProcessCmd_UCSCH_GETR          },
    { AppCmdStr_UCSCH_SETR,          &AppCmd_ProcessCmd_UCSCH_SETR          },
    { AppCmdStr_BCSCH_GETR,          &AppCmd_ProcessCmd_BCSCH_GETR          },
    { AppCmdStr_BCSCH_SETR,          &AppCmd_ProcessCmd_BCSCH_SETR          },
    { AppCmdStr_WHTLST_GETR,         &AppCmd_ProcessCmd_WHTLST_GETR         },
    { AppCmdStr_WHTLST_SETR,         &AppCmd_ProcessCmd_WHTLST_SETR         },
    { AppCmdStr_GTKS_GETR,           &AppCmd_ProcessCmd_GTKS_GETR           },
    { AppCmdStr_GTKS_SETR,           &AppCmd_ProcessCmd_GTKS_SETR           },
    { AppCmdStr_LGTKS_GETR,          &AppCmd_ProcessCmd_LGTKS_GETR          },
    { AppCmdStr_LGTKS_SETR,          &AppCmd_ProcessCmd_LGTKS_SETR          },
    { AppCmdStr_KEYLIFETIMES_SETR,   &AppCmd_ProcessCmd_KEYLIFETIMES_SETR   },
    { AppCmdStr_CERT_SWITCHR,        &AppCmd_ProcessCmd_CERT_SWITCHR        },
    { AppCmdStr_VERBOSE_SETR,        &AppCmd_ProcessCmd_VERBOSE_SETR        },
    { AppCmdStr_LOGVERSION_GETR,     &AppCmd_ProcessCmd_LOGVERSION_GETR     },
    { AppCmdStr_LOGENTRIES_GETR,     &AppCmd_ProcessCmd_LOGENTRIES_GETR     },
    { AppCmdStr_SUBSCP_SETR,         &AppCmd_ProcessCmd_SUBSCP_SETR         },
    { AppCmdStr_PAN_VERSION_INCR,    &AppCmd_ProcessCmd_PAN_VERSION_INCR    },
    { AppCmdStr_RPL_ROUTES_REFRESHR, &AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR },
    { AppCmdStr_RPL_GLOBALREPAIRR,   &AppCmd_ProcessCmd_RPL_GLOBALREPAIRR   },
    { AppCmdStr_ROUTELST_GETR,       &AppCmd_ProcessCmd_ROUTELST_GETR       },
    { AppCmdStr_RPLINFO_GETR,        &AppCmd_ProcessCmd_RPLINFO_GETR        },
    { AppCmdStr_NDCACHE_GETR,        &AppCmd_ProcessCmd_NDCACHE_GETR        },
    { AppCmdStr_VERSION_GETR,        &AppCmd_ProcessCmd_VERSION_GETR        },
    { AppCmdStr_STATUS_GETR,         &AppCmd_ProcessCmd_STATUS_GETR         },
    { AppCmdStr_TASKSTATUS_GETR,     &AppCmd_ProcessCmd_TASKSTATUS_GETR     },
#ifdef UMM_INFO
    { AppCmdStr_MEMMARK_GETR,      &AppCmd_ProcessCmd_MEMMARK_GETR      },
    { AppCmdStr_MEMINFO_GETR,      &AppCmd_ProcessCmd_MEMINFO_GETR      },
    { AppCmdStr_MEMMAP_GETR,       &AppCmd_ProcessCmd_MEMMAP_GETR       },
#endif // UMM_INFO
# if R_PHY_TYPE_CWX_M && R_MDR_ENABLED
	{ AppCmdStr_MDRSR,             &AppCmd_ProcessCmd_MDRSR             },
#endif // R_PHY_TYPE_CWX_M && R_MDR_ENABLED
};
/* *FORMATTING-ON* */

/******************************************************************************
   Public function bodies
******************************************************************************/
/********************************************************************************
* Function Name     : AppCmd_Init
* Description       : Initialization
* Arguments         : None
* Return Value      : None
********************************************************************************/
void AppCmd_Init(void)
{
    R_NWK_Init();  // Initialize network layer

    /* Initialize global information structure of application layer */
    R_APL_ClearAplGlobal();
    p_aplGlobal = R_APL_GetAplGlobal();

    AppCmd_Reset();
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd
* Description       : Process a test command
* Arguments         : pCmd ... Pointer to command
* Return Value      : Success(1), Failed(0)
********************************************************************************/
unsigned char AppIpProcessCmd(unsigned char* pCmd)
{
    unsigned char ret = 0;
    unsigned char loop, size;

    size = sizeof(AppCmdFunc) / sizeof(AppCmdFunc[0]);
    for (loop = 0; loop < size; loop++)
    {
        if (!AppStricmp((void __far*)AppCmdFunc[loop].pCmd, (void*)&pCmd))
        {
            AppCmdFunc[loop].pFunc(pCmd);
            ret = 1;
            break;
        }
    }
    return ret;
}

/********************************************************************************
* Function Name     : AppReceiveMessage
* Description       : Receive message from NWK task
* Arguments         : pErase ... (output) enable/disable to release message
*                   : p_msg ... message
* Return Value      : Success(1), Failed(0)
********************************************************************************/
unsigned char AppIpReceiveMessage(unsigned char* pErase, r_os_msg_t p_msg)
{
    *pErase = R_TRUE;  // Free all messages by default

    /* Process message */
    r_nwk_msg_t* p_nwk_msg = p_msg;
    switch (p_nwk_msg->hdr.param_id)
    {
#if R_DEV_TBU_ENABLED && __RX
        case R_NWK_API_MSG_SUBSCRIPT_FRAME_IND:
            R_Modem_print("SUBMSGI %02X %02X ", AppCmdConfig.ntfHandle++, R_RESULT_SUCCESS);
            AppPrintSubScriptFrameInd(&p_nwk_msg->message.subscriptionFrameIndication);
            break;

#endif
        case R_NWK_API_MSG_IP_DATA_IND:
            R_Modem_print("IPFI %02X %02X ", AppCmdConfig.ntfHandle++, R_RESULT_SUCCESS);
            AppPrintIpDataIndication(&p_nwk_msg->message.ipInd);
            break;

        case R_NWK_API_MSG_DHCP_VENDOR_OPT_DATA:
        {
            const r_nwk_dhcp_vend_opt_data_ind_t* ind = &p_nwk_msg->message.dhcpVendorOptionInd;
            R_DHCPV6_VendorOptionRecvCallback(&ind->src, ind->enterprise_number, ind->option_data, ind->option_data_len);
            break;
        }

#if R_BORDER_ROUTER_ENABLED
        case R_NWK_API_MSG_DHCP_IND:
        {
#if R_DHCPV6_SERVER_ENABLED
            r_dhcp_data_ind_t* dhcpInd = &p_nwk_msg->message.dhcpInd;
            R_DHCPV6_ServerProcessMsg(dhcpInd->srcAddress, dhcpInd->data, dhcpInd->dataLength);
#endif
            break;
        }

        case R_NWK_API_MSG_DEVICE_STATUS_UPDATE_IND:
        {
            r_nwk_device_status_ind_t* ind = &p_nwk_msg->message.deviceStatusUpdateInd;
            LOG_ONLY_VAR(ind);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_529(ind->deviceAddress.bytes, ind->updateType);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            break;
        }
#endif /* R_BORDER_ROUTER_ENABLED */

#if R_BR_AUTHENTICATOR_ENABLED
        case R_NWK_API_MSG_AUTH_BR_START_IND:
        {
            R_AUTH_BR_Start(p_aplGlobal);
            break;
        }

        case R_NWK_API_MSG_AUTH_BR_PER_UPDATE:
        {
            if (R_AUTH_BR_PeriodicUpdateGtks(p_aplGlobal))
            {
                R_NWK_IncreasePanVersionRequest();
            }
            if (R_AUTH_BR_PeriodicUpdateLgtks(p_aplGlobal))
            {
                R_NWK_IncreaseLfnVersionRequest();
            }
            break;
        }
#endif /* R_BR_AUTHENTICATOR_ENABLED */

        case R_NWK_API_MSG_AUTH_RN_START:
        {
            r_result_t res = R_AUTH_SUP_StartJoin(p_aplGlobal, p_nwk_msg->message.authRnStartInd.target.bytes);
            R_NWK_EapolDataProcessed(res);  // Signal result to network layer
            break;
        }

        case R_NWK_API_MSG_AUTH_RN_RESET:
            R_AUTH_SUP_Reset(p_aplGlobal);
            break;

        case R_NWK_API_MSG_AUTH_RN_CLEAR_GTK_VALIDITY:
            R_AUTH_SUP_ClearGtkValidity(p_aplGlobal);
            break;

        case R_NWK_API_MSG_AUTH_RN_DELETE_GTK:
        {
            R_AUTH_DeleteGTKs(p_aplGlobal, p_nwk_msg->message.authRnGtkDeleteInd.mask);
            break;
        }

        case R_NWK_API_MSG_EAPOL_DATA_IND:
        {
            r_eapol_data_ind_t* ind = &p_nwk_msg->message.eapolDataInd;

#if R_BR_AUTHENTICATOR_ENABLED
            if (AppCmdConfig.deviceType == R_BORDERROUTER)
            {
                R_AUTH_BR_ProcessDirectMessage(p_aplGlobal, ind->type, ind->src.bytes, ind->data, ind->dataLength);
            }
            else
#endif
            {
                uint16_t eapolDataLength = ind->dataLength;
                r_mac_eapol_type_t type = ind->type;
                r_eui64_t src = ind->src;
                r_eui64_t authenticator = ind->authenticator;

                /* Processing function of supplicant authentication expects data in allocated auth heap buffer */
                void* eapolDataHeapBuf;
#if R_SHARED_NWK_MEM && (R_HEAP_ID_AUTH == R_HEAP_ID_NWK)
                // FreeRTOS msg is already on correct heap -> Move EAPOL data to front of buffer and resize it */
                eapolDataHeapBuf = p_msg;
                memmove(eapolDataHeapBuf, ind->data, eapolDataLength);
                void* reallocedBuf = r_realloc(R_HEAP_ID_AUTH, eapolDataHeapBuf, eapolDataLength);
                // If realloc fails, the passed pointer is still valid and untouched -> only assign value on success
                if (reallocedBuf != NULL)
                {
                    /* This branch is unreachable since realloc never fails if the new size is smaller than the old */
                    eapolDataHeapBuf = reallocedBuf;
                }
                *pErase = R_FALSE;  // Ownership will be transferred to AUTH module
#else
                // Create auth heap buffer to hold EAPOL data
                eapolDataHeapBuf = r_alloc(R_HEAP_ID_AUTH, eapolDataLength);
                if (!eapolDataHeapBuf)
                {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
                    r_loggen_612(eapolDataLength);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
                    R_NWK_EapolDataProcessed(R_RESULT_INSUFFICIENT_BUFFER);  // Signal failure to network layer
                    break;
                }
                memcpy(eapolDataHeapBuf, ind->data, eapolDataLength);
#endif /* if R_SHARED_NWK_MEM && (R_HEAP_ID_AUTH == R_HEAP_ID_NWK) */

                r_result_t res = R_AUTH_SUP_ProcessEapolMessage(p_aplGlobal, type, src.bytes, authenticator.bytes,
                                                                eapolDataHeapBuf, eapolDataLength);
                eapolDataHeapBuf = NULL;       // Ownership transferred
                R_NWK_EapolDataProcessed(res); // Signal message processing result to network layer
            }
            break;
        }

        case R_NWK_API_MSG_UDP_DATA_IND:
        {
            r_udp_data_ind_t* ind = &p_nwk_msg->message.udpInd;
            if (ind->dstPort == R_AUTH_EAPOL_RELAY_PORT)
            {
#if R_BR_AUTHENTICATOR_ENABLED
                if (AppCmdConfig.deviceType == R_BORDERROUTER)
                {
                    R_AUTH_BR_ProcessRelayMessage(p_aplGlobal, ind->srcAddress, ind->dstAddress, ind->data, ind->dataLength);
                    break;
                }
                else
#endif
                {
                    R_AUTH_SUP_ProcessRelayMessage(p_aplGlobal, ind->srcAddress, ind->dstAddress, ind->data, ind->dataLength);
                }
            }
            else if (ind->dstPort == KICK_DEVICE_MSG_UDP_PORT)
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                r_loggen_646(ind->srcAddress);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
                uint8_t expectedPayload[] = KICK_DEVICE_MSG_PAYLOAD;
                if (sizeof(expectedPayload) == ind->dataLength && MEMEQUAL_A(expectedPayload, ind->data))
                {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                    r_loggen_650();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
                    AppCmd_LeaveNetwork();
                }
                else
                {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
                    r_loggen_655();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
                }
            }
            else
            {
                R_Modem_print("RCVI %02X %02X ", AppCmdConfig.ntfHandle++, R_RESULT_SUCCESS);
                AppPrintUdpDataInd(ind);
            }
            break;
        }

        case R_NWK_API_MSG_ICMP_ECHO_IND:
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_667(clock_seconds(), p_nwk_msg->message.icmpEchoInd.srcAddress);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            R_Modem_print("RCVI %02X %02X ", AppCmdConfig.ntfHandle++, R_RESULT_SUCCESS);
            AppPrintIcmpEchoInd(&p_nwk_msg->message.icmpEchoInd);
            break;

        case R_NWK_API_MSG_JOIN_STATE_IND:
        {
            r_nwk_join_state_ind_t* ind = &p_nwk_msg->message.joinStateInd;
            p_aplGlobal->nwk.joinState = ind->currentJoinState;  // Cache join state to avoid repeated requests to NWK
            R_Modem_print("JSI %02X %02X\n", AppCmdConfig.ntfHandle++, ind->currentJoinState);
            break;
        }

        /*-----------------------------------------------------------------------*/
        /* The following messages are not supported by this sample application   */
        /*-----------------------------------------------------------------------*/
        /* Warning indication */
        case R_NWK_API_MSG_NWK_WARNING_IND:
            /*
               Warning indication - This message can be ignored
                  An indication messages could not be notified to the application layer
                  due to lack of memory resources. The stack can continue normal operations.
             */
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
            r_loggen_690(p_nwk_msg->message.warningInd.status);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
            break;

        /* Fatal error indication */
        case R_NWK_API_MSG_NWK_FATAL_ERROR_IND:
            /*
               Fatal error indication - This message should not be ignored
                  Fatal error occurred due to possibly hardware errors or unexpected behaviors
                  in RFIC. The stack could not continue normal operations.
             */
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_700(p_nwk_msg->message.fatalErrorInd.status);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            // PRQA S 2870 2
            for ( ; ; )
            {
                /* Stop execution at this point using an infinite loop */
            }

        /* ICMP error message */
        case R_NWK_API_MSG_ICMP_ERROR_IND:
            break;

        /* Confirm message
             These messages will be notified if an API function is called with non-blocking
             NOTE: As for this sample application, all API functions are called with blocking.
                   So these messages will not be notified.
         */
        case R_NWK_API_MSG_NWK_JOIN_CFM:  //!< Network join confirm
#if R_BORDER_ROUTER_ENABLED
        case R_NWK_API_MSG_NWK_START_CFM: //!< Network start confirm
#endif
        case R_NWK_API_MSG_ICMP_ECHO_CFM: //!< ICMP echo confirm
        case R_NWK_API_MSG_UDP_DATA_CFM:  //!< UDP data confirm
            break;

        default:
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
            r_loggen_725(p_nwk_msg->hdr.param_id);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
            break;
    }

    return 1;
}

/******************************************************************************
   Private function bodies
******************************************************************************/
/********************************************************************************
* Function Name     : AppCmd_Reset
* Description       : Reset application state
* Arguments         : None
* Return Value      : None
********************************************************************************/
static void AppCmd_Reset(void)
{
    /* Set MAC address */
    const uint8_t* p_macAddr = pAppGetMacAddr();
    uint8_t eui64[8];
    R_Swap64(p_macAddr, eui64);
    R_NWK_SetRequest(R_NWK_macExtendedAddress, eui64, 8);
    r_app_config_t* p_config = &AppCmdConfig;
    memset(p_config, 0, sizeof(*p_config));

    /* Set current settings for macPANId and phyCurrentChannel  */
    p_config->panId = APP_CMD_MAC_PanId_Default;
    uint8_t channel = APP_CMD_MAC_Channel_Default;
    R_NWK_SetRequest(R_NWK_phyCurrentChannel, &channel, sizeof(channel));

#if !R_DEV_DISABLE_AUTH
#if R_MODEM_CLIENT
    R_AUTH_SUP_Init(p_aplGlobal, r_auth_certs, 2, 0);
#else
    R_AUTH_SUP_Init(p_aplGlobal, r_auth_certs, 2, MAX(sizeof(RmMcpsDataRequestT), sizeof(RmMcpsDataConfirmT)));
#endif
#if R_BR_AUTHENTICATOR_ENABLED
    R_AUTH_BR_Reset(p_aplGlobal);
#endif
#endif

    /* Set default IPv6 prefix */
    const uint8_t prefix[R_IPV6_PREFIX_LEN] = {0x20, 0x17, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00};
    R_memcpy(p_config->globalIpAddress.bytes, prefix, sizeof(prefix));

    /* Set default IPv6 interface Identifier (generated from EUI-64) */
    R_memcpy(&p_config->globalIpAddress.bytes[R_IPV6_PREFIX_LEN], pAppGetMacAddr(), R_IPV6_ADDRESS_LENGTH - R_IPV6_PREFIX_LEN);
    p_config->globalIpAddress.bytes[R_IPV6_PREFIX_LEN] ^= 0x02u;  // Toggle U/L bit

#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
    static uint8_t r_app_log_buffer[R_LOG_BUFFER_SIZE];
    R_LOG_Init(r_app_log_buffer, sizeof(r_app_log_buffer), R_LOG_SEVERITY_OFF);  // Initialize and disable log framework
#endif
}

static r_result_t AppCmd_LeaveNetwork()
{
    /* Send NWK request to leave the network (NWK and lower layers will be reset) */
    r_result_t res = R_NWK_LeaveNetworkRequest();

    /* Reset authentication (required to avoid inconsistent state between auth module and NWK after restart) */
#if !R_DEV_DISABLE_AUTH
#if R_MODEM_CLIENT
    R_AUTH_SUP_Init(p_aplGlobal, r_auth_certs, 2, 0);
#else
    R_AUTH_SUP_Init(p_aplGlobal, r_auth_certs, 2, MAX(sizeof(RmMcpsDataRequestT), sizeof(RmMcpsDataConfirmT)));
#endif
#if R_BR_AUTHENTICATOR_ENABLED
    R_AUTH_BR_Reset(p_aplGlobal);
#endif
#endif

    return res;
}

/* Command */
/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_RSTR
* Description       : Process "RSTR" command
* Arguments         : pCmd ... Pointer to command
*                   :   RSTR (Handle) [(Options)]
* Return Value      : None
* Output            : Confirm command
*                   :   RSTC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_RSTR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    if (res == R_RESULT_SUCCESS)
#if R_DEV_TBU_ENABLED && __RX
    {
        /* Store the original handle number for the confirm */
        rstr_handle = handle;

        /* Perform Software Reset */
        SYSTEM.PRCR.WORD = 0xA503; // Unlock register protection
        SYSTEM.SWRR = 0xA501;      // Software Reset
        SYSTEM.PRCR.WORD = 0xA500; // Lock register protection
        while (1)
            ;
    }
#else /* R_DEV_TBU_ENABLED && __RX */
    {
        res = R_NWK_ResetRequest();
        if (res == R_RESULT_SUCCESS)
        {
            AppCmd_Reset();  // Reset configuration and state of application
        }
    }

#endif /* R_DEV_TBU_ENABLED && __RX */
    R_Modem_print("RSTC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_STARTR
* Description       : Process "STARTR" command
* Arguments         : pCmd ... Pointer to command
*                   :   STARTR (Handle) [(Options)]
* Return Value      : None
* Output            : Confirm command
*                   :   STARTC (Handle) (Status) (ChannelNumber) (PANId)
*                   :          (IPv6Addr) (CoordIPv6Addr) (CoordMACAddr)
********************************************************************************/
static void AppCmd_ProcessCmd_STARTR(uint8_t* pCmd)
{
    r_app_config_t* p_config = &AppCmdConfig;

    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    /* Force to set blocking. */
    options &= (~R_OPTIONS_NON_BLOCKING);

    if (res == R_RESULT_SUCCESS)
    {
        if (p_config->deviceType == R_ROUTERNODE)
        {
            res = R_NWK_JoinRequest(p_config->networkName, options);
        }
#if R_BORDER_ROUTER_ENABLED
        else if (p_config->deviceType == R_BORDERROUTER)
        {
#if R_DHCPV6_SERVER_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_878(AppCmdConfig.globalIpAddress.bytes);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            R_DHCPV6_ServerInit(pAppGetMacAddr(), &R_DHCPV6_ServerLookupAssignPrefix, p_config->globalIpAddress.bytes);
#endif
            res = R_NWK_StartRequest(p_config->panId,
                                     p_config->panSize,
                                     p_config->useParent_BS_IE,
                                     p_config->networkName,
                                     &p_config->globalIpAddress,
                                     options);
        }
#endif /* R_BORDER_ROUTER_ENABLED */
        else
        {
            res = R_RESULT_INVALID_PARAMETER;  // Invalid device type
        }
    }
    // get current Device Type
    uint8_t deviceType;
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_GetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
    }
    // get current MAC address
    uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH] = { 0 };
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));
    }
    // get current Pan ID
    uint16_t panId;
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_GetRequest(R_NWK_macPANId, &panId, sizeof(panId));
    }

    /* Confirm(STARTC) */
    R_Modem_print("STARTC %02X %02X ", handle, res);
    if (res == R_RESULT_SUCCESS)
    {
        /* DeviceType */
        R_Modem_print("%02X ", deviceType);

        /* PANId */
        R_Modem_print("%04X ", panId);

        /* NetworkName */
        AppPrintNetworkName(p_config->networkName);
        R_Modem_print(" ");

        /* MACAddr */
        R_Swap64((uint8_t*)(&macAddr[0]), (uint8_t*)(&macAddr[0]));
        AppPrintMACAddr(macAddr);
    }
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_PIB_GETR
* Description       : Process "PIB_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   PIB_GETR (Handle) (AttributeId) (IsNumeric)
* Return Value      : None
* Output            : Confirm command
*                   :   PIB_GETC (Handle) (Status) (AttributeLength) (AttributeValueBytes)
********************************************************************************/
static void AppCmd_ProcessCmd_PIB_GETR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    uint8_t attributeId;
    res |= AppCmd_HexStrToNum(&pCmd, &attributeId, sizeof(attributeId), R_FALSE);

    r_boolean_t isNumeric;
    res |= AppCmd_HexStrToNum(&pCmd, &isNumeric, sizeof(isNumeric), R_FALSE);

    uint16_t attributeLength = 0;
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_GetRequestVarSize(attributeId, AppCmdBuf, sizeof(AppCmdBuf), &attributeLength);
    }

    R_Modem_print("PIB_GETC %02X %02X", handle, res);

    if (res == R_RESULT_SUCCESS)
    {
        R_Modem_print(" %04X ", attributeLength);
        if (isNumeric)
        {
            /* Device uses little-endian -> Print numeric values in reverse order for big-endian output */
            for (int i = attributeLength - 1; i >= 0; i--)
            {
                R_Modem_print("%02X", AppCmdBuf[i]);
            }
        }
        else
        {
            for (uint16_t i = 0; i < attributeLength; i++)
            {
                R_Modem_print("%02X", AppCmdBuf[i]);
            }
        }
    }

    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_PIB_SETR
* Description       : Process "PIB_SETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   PIB_SETR (Handle) (AttributeId) (IsNumeric) (AttributeLength) (AttributeValueBytes)
* Return Value      : None
* Output            : Confirm command
*                   :   PIB_SETC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_PIB_SETR(uint8_t* pCmd)
{
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    uint8_t attributeId;
    res |= AppCmd_HexStrToNum(&pCmd, &attributeId, sizeof(attributeId), R_FALSE);
    r_boolean_t isNumeric;
    res |= AppCmd_HexStrToNum(&pCmd, &isNumeric, sizeof(isNumeric), R_FALSE);
    uint16_t attributeLength;
    res |= AppCmd_HexStrToNum(&pCmd, &attributeLength, sizeof(attributeLength), R_FALSE);

    uint8_t attributeValue[33];
    uint16_t expectedAttributeLength = R_NWK_GetAttrLength(attributeId);

    if (res == R_RESULT_SUCCESS)
    {
        if (attributeLength > expectedAttributeLength || attributeLength == 0)
        {
            res = R_RESULT_INVALID_PARAMETER;
        }
        else if (attributeLength > sizeof(attributeValue))
        {
            res = R_RESULT_INSUFFICIENT_BUFFER;
        }
    }

    if (res == R_RESULT_SUCCESS)
    {
        res = AppCmd_HexStrToNum(&pCmd, attributeValue, attributeLength, !isNumeric);
    }

    if (res == R_RESULT_SUCCESS)
    {
        // Expand attribute value with zeros to match attribute length expected by NWK API
        R_memset(&attributeValue[attributeLength], 0, expectedAttributeLength - attributeLength);
        attributeLength = expectedAttributeLength;
    }

    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_SetRequest(attributeId, attributeValue, attributeLength);
    }

    R_Modem_print("PIB_SETC %02X %02X", handle, res);
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_WHTLST_GETR
* Description       : Process "WHTLST_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   WHTLST_GETR (Handle) [(Options)]
* Return Value      : None
* Output            : Confirm command
*                   :   WHTLST_GETC (Handle) (Status) (MacAddress1) (MacAddress2) ... (MacAddress16)
********************************************************************************/
static void AppCmd_ProcessCmd_WHTLST_GETR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    r_app_whitelist_buffer_t confirm_buffer = {{ 0 }};
    res |= R_NWK_GetRequest(R_NWK_macWhitelist, &confirm_buffer, sizeof(confirm_buffer));
    /* Always return R_WHITELIST_TABLE_LENGTH entries (required by developer UI). Since confirm_buffer is initialized
     * with zero, the remaining whitelist entries are all-zero addresses, which is fine. */
    confirm_buffer.whitelist.count = R_WHITELIST_TABLE_LENGTH;

    R_Modem_print("WHTLST_GETC %02X %02X", handle, res);
    if (res == R_RESULT_SUCCESS)
    {
        for (uint16_t i = 0; i < confirm_buffer.whitelist.count; i++)
        {
            R_Modem_print(" ");
            AppPrintMACAddr(confirm_buffer.whitelist.entries[i].bytes);
        }
    }
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_WHTLST_SETR
* Description       : Process "WHTLST_SETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   WHTLST_SETR (Handle) [(Options)] [(MACAddress1) (MACAddress2) ...]
* Return Value      : None
* Output            : Confirm command
*                   :   WHTLST_SETR (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_WHTLST_SETR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t options;
    uint8_t i;
    r_app_whitelist_buffer_t request = {{ 0}};

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* [(Options)] */
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    /* Add MAC addresses to the whitelist set request */
    for (i = 0; i < R_WHITELIST_TABLE_LENGTH; i++)
    {
        AppCmd_SkipWhiteSpace(&pCmd);
        if (res != R_RESULT_SUCCESS || *pCmd == '\n')
        {
            break;  // Stop if previous step failed or there are no more arguments in the command string
        }

        res |= AppCmd_HexStrToNum(&pCmd, request.whitelist.entries[request.whitelist.count].bytes,
                                  sizeof(request.whitelist.entries[request.whitelist.count]), R_TRUE);

        /* Add MAC address to whitelist set request if it is not all-zero */
        if (MEMISNOTZERO_A(request.whitelist.entries[request.whitelist.count].bytes))
        {
            request.whitelist.count++;
        }
    }

    if (res == R_RESULT_SUCCESS)
    {
        res |= R_NWK_SetRequest(R_NWK_macWhitelist, &request,
                                sizeof(request.whitelist) + (request.whitelist.count * sizeof(request.whitelist.entries[0])));
    }

    R_Modem_print("WHTLST_SETC %02X %02X", handle, res);
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_GTKS_GETR
* Description       : Process "GTKS_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   GTKS_GETR (Handle)
* Return Value      : None
* Output            : Confirm command
*                   :   GTKS_GETC (Handle) (Status) (Mask) ([GTKs])
********************************************************************************/
static void AppCmd_ProcessCmd_GTKS_GETR(uint8_t* pCmd)
{
    r_result_t res;

    /* Get command parameters */
    uint8_t handle;
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    R_Modem_print("GTKS_GETC %02X %02X", handle, res);

    if (res == R_RESULT_SUCCESS)
    {
#if R_AUTH_NUM_GTKS
        R_Modem_print(" %02x", p_aplGlobal->auth.gtks_valid);
        for (size_t i = 0; i < R_AUTH_NUM_GTKS; i++)
        {
            if (p_aplGlobal->auth.gtks_valid & (1 << i))
            {
                R_Modem_print(" ");
                AppPrintBytesAsHexString(p_aplGlobal->auth.gtks[i], R_AUTH_GTK_SIZE);
            }
        }
#else  /* if R_AUTH_NUM_GTKS */
        R_Modem_print(" %02x", 0);
#endif /* if R_AUTH_NUM_GTKS */
    }
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_GTKS_SETR
* Description       : Process "GTKS_SETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   GTKS_SETR (Handle) (Options) (Mask) ([GTKS])
* Return Value      : None
* Output            : Confirm command
*                   :   GTKS_SETC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_GTKS_SETR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
    uint8_t mask;
    res |= AppCmd_HexStrToNum(&pCmd, &mask, sizeof(mask), R_FALSE);

#if R_BR_AUTHENTICATOR_ENABLED
    if (res == R_RESULT_SUCCESS)
    {
        uint8_t gtks[R_KEY_NUM][R_AUTH_GTK_SIZE];
        for (int i = 0; i < R_KEY_NUM; i++)
        {
            if (mask & (1 << i))
            {
                AppCmd_HexStrToNum(&pCmd, gtks[i], sizeof(gtks[i]), R_TRUE);
                R_AUTH_SetGTK(p_aplGlobal, i, gtks[i]);
            }
        }

        if (AppCmdConfig.deviceType == R_BORDERROUTER && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
        {
            res = R_NWK_IncreasePanVersionRequest();
        }
    }
#else  /* if R_BR_AUTHENTICATOR_ENABLED */
    res = R_RESULT_UNSUPPORTED_FEATURE;
#endif /* if R_BR_AUTHENTICATOR_ENABLED */

    R_Modem_print("GTKS_SETC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_LGTKS_GETR
* Description       : Process "LGTKS_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   LGTKS_GETR (Handle)
* Return Value      : None
* Output            : Confirm command
*                   :   LGTKS_GETC (Handle) (Status) (Mask) ([LGTKs])
********************************************************************************/
static void AppCmd_ProcessCmd_LGTKS_GETR(uint8_t* pCmd)
{
    r_result_t res;

    /* Get command parameters */
    uint8_t handle;
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    R_Modem_print("LGTKS_GETC %02X %02X", handle, res);

    if (res == R_RESULT_SUCCESS)
    {
#if R_AUTH_NUM_LGTKS
        uint8_t lgtkLiveness = (p_aplGlobal->auth.gtks_valid & R_AUTH_LGTK_LIVENESS_MASK) >> R_AUTH_NUM_GTKS;
        R_Modem_print(" %02x", lgtkLiveness);
        for (size_t i = 0; i < R_AUTH_NUM_LGTKS; i++)
        {
            if (lgtkLiveness & (1 << i))
            {
                R_Modem_print(" ");
                AppPrintBytesAsHexString(p_aplGlobal->auth.gtks[i + R_AUTH_NUM_GTKS], R_AUTH_GTK_SIZE);
            }
        }
#else /* R_AUTH_NUM_LGTKS */
        R_Modem_print(" %02x", 0);
#endif /* R_AUTH_NUM_LGTKS */
    }
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_LGTKS_SETR
* Description       : Process "LGTKS_SETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   LGTKS_SETR (Handle) (Options) (Mask) ([LGTKS])
* Return Value      : None
* Output            : Confirm command
*                   :   LGTKS_SETC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_LGTKS_SETR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);
    uint8_t mask;
    res |= AppCmd_HexStrToNum(&pCmd, &mask, sizeof(mask), R_FALSE);

#if R_BR_AUTHENTICATOR_ENABLED
    if (res == R_RESULT_SUCCESS)
    {
        uint8_t gtks[R_AUTH_NUM_LGTKS][R_AUTH_GTK_SIZE];
        for (int i = 0; i < R_AUTH_NUM_LGTKS; i++)
        {
            if (mask & (1 << i))
            {
                AppCmd_HexStrToNum(&pCmd, gtks[i], sizeof(gtks[i]), R_TRUE);

                /* Convert key index since r_auth_br uses continuous key index (LGTK index begins after GTK indices) */
                res = R_AUTH_SetGTK(p_aplGlobal, i + R_AUTH_NUM_GTKS, gtks[i]);
            }
        }

        if (res == R_RESULT_SUCCESS)
        {
            if (AppCmdConfig.deviceType == R_BORDERROUTER && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
            {
                res = R_NWK_IncreaseLfnVersionRequest();
            }
        }
    }
#else  /* if R_BR_AUTHENTICATOR_ENABLED */
    res = R_RESULT_UNSUPPORTED_FEATURE;
#endif /* if R_BR_AUTHENTICATOR_ENABLED */

    R_Modem_print("LGTKS_SETC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_KEYLIFETIMES_SETR
* Description       : Process "KEYLIFETIMES_SETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   KEYLIFETIMES_SETR (Handle) (Options) (PMK_LIFETIME) (PTK_LIFETIME) (GTK_LIFETIME)
*                                         (GTK_NEW_ACTIVATION_TIME_FRACTION) (REVOCATION_LIFETIME_REDUCTION_FRACTION)
* Return Value      : None
* Output            : Confirm command
*                   :   KEYLIFETIMES_SETC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_KEYLIFETIMES_SETR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t options;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (Options) */
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    uint32_t pmk_lifetime_minutes;
    res |= AppCmd_HexStrToNum(&pCmd, &pmk_lifetime_minutes, sizeof(pmk_lifetime_minutes), R_FALSE);

    uint32_t ptk_lifetime_minutes;
    res |= AppCmd_HexStrToNum(&pCmd, &ptk_lifetime_minutes, sizeof(ptk_lifetime_minutes), R_FALSE);

    uint32_t gtk_lifetime_minutes;
    res |= AppCmd_HexStrToNum(&pCmd, &gtk_lifetime_minutes, sizeof(gtk_lifetime_minutes), R_FALSE);

    uint16_t gtk_new_activation_time;
    res |= AppCmd_HexStrToNum(&pCmd, &gtk_new_activation_time, sizeof(gtk_new_activation_time), R_FALSE);

    uint16_t revocation_lifetime_reduction;
    res |= AppCmd_HexStrToNum(&pCmd, &revocation_lifetime_reduction, sizeof(revocation_lifetime_reduction), R_FALSE);

#if R_BR_AUTHENTICATOR_ENABLED
    p_aplGlobal->auth.br.key_lifetimes.pmk_lifetime_minutes = pmk_lifetime_minutes;
    p_aplGlobal->auth.br.key_lifetimes.ptk_lifetime_minutes = ptk_lifetime_minutes;
    p_aplGlobal->auth.br.key_lifetimes.gtk_lifetime_minutes = gtk_lifetime_minutes;
    p_aplGlobal->auth.br.key_lifetimes.gtk_new_activation_time_fraction = gtk_new_activation_time;
    p_aplGlobal->auth.br.key_lifetimes.revocation_lifetime_reduction_fraction = revocation_lifetime_reduction;
#else
    res |= R_RESULT_UNSUPPORTED_FEATURE;  // KEYLIFETIMES_SETR can only be processed by border routers
#endif /* R_BR_AUTHENTICATOR_ENABLED */

    R_Modem_print("KEYLIFETIMES_SETC %02X %02X", handle, res);
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_CERT_SWITCHR
* Description       : Process "CERT_SWITCHR" command
* Arguments         : pCmd ... Pointer to command
*                   :   CERT_SWITCHR (Handle) (Index)
* Return Value      : None
* Output            : Confirm command
*                   :   CERTSWITCHC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_CERT_SWITCHR(uint8_t* pCmd)
{
    uint8_t handle;
    AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    r_result_t res = R_RESULT_UNSUPPORTED_FEATURE;
#if R_AUTH_MULTIPLE_CERTS
    uint8_t cert_index = 0;
    res = AppCmd_HexStrToNum(&pCmd, &cert_index, sizeof(cert_index), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
        switch (cert_index)
        {
            case 0:
                r_auth_certs_use_alternate = 0;
                break;

            case 1:
                r_auth_certs_use_alternate = 1;
                break;

            default:
                res = R_RESULT_INVALID_PARAMETER;
                break;
        }
    }
#endif /* R_AUTH_MULTIPLE_CERTS */

    R_Modem_print("CERT_SWITCHC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_VERBOSE_SETR
* Description       : Process "VERBOSE_SETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   VERBOSE_SETR (Handle) (Options) (VerbosityLevel)
* Return Value      : None
* Output            : Confirm command: VERBOSE_SETC (Handle) (Status) (RLog Version ID)
********************************************************************************/
static void AppCmd_ProcessCmd_VERBOSE_SETR(uint8_t* pCmd)
{
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    uint8_t verbosity;
    res |= AppCmd_HexStrToNum(&pCmd, &verbosity, sizeof(verbosity), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
        if (verbosity <= R_LOG_THRESHOLD)
        {
            R_LOG_SetSeverityThreshold(verbosity);
#if R_MODEM_CLIENT
            res = R_NWK_SetRequest(R_NWK_logVerbosity, &verbosity, sizeof(verbosity));  // Set log level on modem server
#endif
        }
        else
        {
            res = R_RESULT_INVALID_PARAMETER;  // Logging framework was compiled with lower severity level
        }
#else
        if (verbosity > 0)
        {
            res = R_RESULT_UNSUPPORTED_FEATURE;  // Logging framework was disabled at compile time
        }
#endif
    }

    uint8_t versionId[R_LOG_VERSION_ID_SIZE] = { 0 };
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
    size_t versionIdSize = R_LOG_GetVersionID(versionId, sizeof(versionId));
    if (versionIdSize != sizeof(versionId))
    {
        res = R_RESULT_FAILED;
    }
#endif

    R_Modem_print("VERBOSE_SETC %02X %02X ", handle, res);
    if (res == R_RESULT_SUCCESS)
    {
        for (size_t i = 0; i < sizeof(versionId); i++)
        {
            R_Modem_print("%02X", versionId[i]);
        }
    }
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_LOGVERSION_GETR
* Description       : Process "LOGVERSION_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   LOGVERSION_GETR (Handle)
* Return Value      : None
* Output            : Confirm command
*                   :   LOGVERSION_GETC (Handle) (Status) (RLog Version ID)
********************************************************************************/
static void AppCmd_ProcessCmd_LOGVERSION_GETR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
    uint8_t versionId[16] = { 0 };
    size_t versionIdSize = R_LOG_GetVersionID(versionId, sizeof(versionId));
    if (versionIdSize)
    {
        res = R_RESULT_INSUFFICIENT_OUTPUT_BUFFER;
    }
#else
    res = R_RESULT_UNSUPPORTED_FEATURE;
#endif

    R_Modem_print("LOGVERSION_GETC %02X %02X ", handle, res);

#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
    if (res == R_RESULT_SUCCESS)
    {
        for (size_t i = 0; i < versionIdSize; i++)
        {
            R_Modem_print("%02X", versionId[i]);
        }
    }
#endif

    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_LOGENTRIES_GETR
* Description       : Process "LOGVERSION_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   LOGENTRIES_GETR (Handle)
* Return Value      : None
* Output            : Confirm command
*                   :   LOGENTRIES_GETC (Handle) (Status) [Binary RLog records]
********************************************************************************/
static void AppCmd_ProcessCmd_LOGENTRIES_GETR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

#if !(R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF)
    res = R_RESULT_UNSUPPORTED_FEATURE;
#endif
    R_Modem_print("LOGENTRIES_GETC %02X %02X ", handle, res);

#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
    if (res == R_RESULT_SUCCESS)
    {
        uint8_t buf[R_LOG_POLL_BUFFER_SIZE];
        size_t bytesWritten = 0;
        while ((bytesWritten = R_LOG_GetLog(buf, sizeof(buf))) > 0)
        {
            AppPrintBytesAsHexString(buf, bytesWritten);
        }
    }
#endif

    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_SUBSCP_SETR
* Description       : Process "SUBSCP_SETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   SUBSCP_SETR (Handle) (Options) (StartStop)
* Return Value      : None
* Output            : Confirm command
*                   :   SUBSCP_SETC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_SUBSCP_SETR(uint8_t* pCmd)
{
    uint8_t handle;
    AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    r_result_t res = R_RESULT_UNSUPPORTED_FEATURE;
#if R_DEV_TBU_ENABLED && __RX
    uint16_t options;
    res = AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    uint8_t mode;
    res |= AppCmd_HexStrToNum(&pCmd, &mode, sizeof(mode), R_FALSE);

    r_boolean_t subscriptEnabled;
    if (mode != 0)
    {
        subscriptEnabled = R_TRUE;
    }
    else
    {
        subscriptEnabled = R_FALSE;
    }

    if (res == R_RESULT_SUCCESS)
    {
        res |= R_NWK_SetRequest(R_NWK_macFrameSubscriptionEnabled, &subscriptEnabled, sizeof(subscriptEnabled));
    }
#endif /* R_DEV_TBU_ENABLED && __RX */

    R_Modem_print("SUBSCP_SETC %02X %02X\n", handle, res);
}


/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_VERSION_GETR
* Description       : Process "VERSION_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   VERSION_GETR (Handle) (Options)
* Return Value      : None
* Output            : Confirm command
*                   :   VERSION_GETC (Handle) (Status) Wi-SUN-FAN
*                                   (Version) (FeatureMask) (MACAddress)
********************************************************************************/
static void AppCmd_ProcessCmd_VERSION_GETR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t options;

    uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH] = {0};

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* [(Options)] */
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    res |= R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));

    R_Modem_print("VERSION_GETC %02X %02X %s %04X ", handle, res, FAN_STACK_VERSION, featureMask);
    R_Swap64(macAddr, macAddr);
    AppPrintMACAddr(macAddr);
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_STATUS_GETR
* Description       : Process "STATUS_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   STATUS_GETR  (Handle) (Options)
* Return Value      : None
* Output            : Confirm command
*                   :   STATUS_GETC  (Handle) (Status) (JoinState)
*                                    (MacAddress) (IPv6Addr)
********************************************************************************/
static void AppCmd_ProcessCmd_STATUS_GETR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t option;

    uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH];
    uint8_t ipv6Addr[R_IPV6_ADDRESS_LENGTH];

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (option) */
    res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
        /* (MACAddr) */
        res |= R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));

        /* (IPv6Addr) */
        res |= R_NWK_GetRequest(R_NWK_nwkIpv6Address, ipv6Addr, sizeof(ipv6Addr));
    }

    /* Generate confirm STATUS_GETC*/
    R_Modem_print("STATUS_GETC %02X %02X ", handle, res);
    if (res == R_RESULT_SUCCESS)
    {
        /* JoinState */
        R_Modem_print("%02X ", p_aplGlobal->nwk.joinState);

        /* MACAddr */
        R_Swap64(macAddr, macAddr);
        AppPrintMACAddr(macAddr);
        R_Modem_print(" ");

        /* IPv6Addr */
        AppPrintIPv6Addr(ipv6Addr);
    }
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_TASKSTATUS_GETR
* Description       : Process "TASKSTATUS_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   TASKSTATUS_GETR  (Handle) (Options)
* Return Value      : None
* Output            : Confirm command
*                   :   TASKSTATUS_GETC  (Handle) (Status) (TaskHandle0 TaskHighWatermark0...)
********************************************************************************/
static void AppCmd_ProcessCmd_TASKSTATUS_GETR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t option;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (option) */
    res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);

#if RTOS_USE != 1
    if (res == R_RESULT_SUCCESS)
    {
        res = R_RESULT_INVALID_PARAMETER;
    }
#endif

    /* Generate confirm TASKSTATUS_GETC */
    R_Modem_print("TASKSTATUS_GETC %02X %02X ", handle, res);
#if RTOS_USE == 1
    if (res == R_RESULT_SUCCESS)
    {
        r_os_id_t i = 0;
        while (1)
        {
            void* taskHandle = R_OS_GetTaskHandle(i++);
            if (!taskHandle)
            {
                break;
            }
            R_Modem_print("%08x %04x ", (unsigned)(uintptr_t)(taskHandle),
                          (unsigned)(uxTaskGetStackHighWaterMark(taskHandle) * sizeof(StackType_t)));
        }
#if defined(__RX)
        {
            const uint8_t* ptr = __sectop("SI");
            const uint8_t* end = __secend("SI");
            while (*ptr == 0x33 && ptr < end)
            {
                ptr += 1;
            }
            R_Modem_print("%08x %04x ",
                          (unsigned)0xff,
                          (unsigned)(ptr - (const uint8_t*)__sectop("SI")));
        }
#endif /* __RX */
    }
#endif /* RTOS_USE == 1 */
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_RPL_GLOBALREPAIRR
* Description       : Process "RPL_ROUTES_REFRESHR" command
* Arguments         : pCmd ... Pointer to command
*                   :   RPL_GLOBALREPAIRR (Handle)
* Return Value      : None
* Output            : Confirm command
*                   :   RPL_GLOBALREPAIRC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_RPL_GLOBALREPAIRR(uint8_t* pCmd)
{
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
#if R_BORDER_ROUTER_ENABLED
        res = R_NWK_RplGlobalRepairRequest();
#else
        res = R_RESULT_UNSUPPORTED_FEATURE;
#endif
    }
    R_Modem_print("RPL_GLOBALREPAIRC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_PAN_VERSION_INCR
* Description       : Process "PAN_VERSION_INCR" command
* Arguments         : pCmd ... Pointer to command
*                   :   PAN_VERSION_INCR (Handle)
* Return Value      : None
* Output            : Confirm command
*                   :   PAN_VERSION_INCC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_PAN_VERSION_INCR(uint8_t* pCmd)
{
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
#if R_BORDER_ROUTER_ENABLED
        res = R_NWK_IncreasePanVersionRequest();
#else
        res = R_RESULT_UNSUPPORTED_FEATURE;
#endif
    }
    R_Modem_print("PAN_VERSION_INCC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR
* Description       : Process "RPL_ROUTES_REFRESHR" command
* Arguments         : pCmd ... Pointer to command
*                   :   RPL_ROUTES_REFRESHR (Handle)
* Return Value      : None
* Output            : Confirm command
*                   :   RPL_ROUTES_REFRESHC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_RPL_ROUTES_REFRESHR(uint8_t* pCmd)
{
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
#if R_BORDER_ROUTER_ENABLED
        res = R_NWK_RplRouteRefreshRequest();
#else
        res = R_RESULT_UNSUPPORTED_FEATURE;
#endif
    }
    R_Modem_print("RPL_ROUTES_REFRESHC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_ROUTELST_GETR
* Description       : Process "ROUTELST_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   STARTR (Handle) [(Options)]
* Return Value      : None
* Output            : Confirm command
*                   :   STARTC (Handle) (Status) (ChannelNumber) (PANId)
*                   :          (IPv6Addr) (CoordIPv6Addr) (CoordMACAddr)
********************************************************************************/
static void AppCmd_ProcessCmd_ROUTELST_GETR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    r_boolean_t route_Found = R_FALSE;
#if R_BORDER_ROUTER_ENABLED
    if (AppCmdConfig.deviceType == R_BORDERROUTER)
    {
        r_app_routing_table_buffer_t cfm;
        do
        {
            res = R_NWK_GetRequestMultipart(R_NWK_sourceRoutes, &cfm, sizeof(cfm), route_Found);
            if ((res == R_RESULT_SUCCESS || res == R_RESULT_SUCCESS_ADDITIONAL_DATA) && cfm.routingTable.count)
            {
                if (!route_Found)
                {
                    route_Found = R_TRUE;
                    R_Modem_print("ROUTELST_GETC %02X %02X 03 ", handle, R_RESULT_SUCCESS);
                }

                for (size_t i = 0; i < cfm.routingTable.count; i++)
                {
                    AppPrintIPv6Addr(cfm.routingTable.entries[i].destination.bytes);
                    R_Modem_print(" ");
                    AppPrintIPv6Addr(cfm.routingTable.entries[i].preferredParent.bytes);
                    R_Modem_print(" ");
                    AppPrintIPv6Addr(cfm.routingTable.entries[i].alternateParent.bytes);
                    R_Modem_print(" ");
                    R_Modem_print("%08lX ", (unsigned long)cfm.routingTable.entries[i].remainingLifetime);
                }
            }
        }
        while (res == R_RESULT_SUCCESS_ADDITIONAL_DATA);  // Continue as long as more data is available

        if (route_Found)
        {
            R_Modem_print("\n");
        }
    }
    else  // (deviceType == R_ROUTERNODE)
#endif /* R_BORDER_ROUTER_ENABLED */
    {
        /* get default route (uplink)*/
        uint8_t preferredParent[R_IPV6_ADDRESS_LENGTH];
        uint8_t alternateParent[R_IPV6_ADDRESS_LENGTH];
        res = R_NWK_GetRequest(R_NWK_preferredParentAddress, preferredParent, sizeof(preferredParent));
        res |= R_NWK_GetRequest(R_NWK_alternateParentAddress, alternateParent, sizeof(alternateParent));
        if (res == R_RESULT_SUCCESS)
        {
            uint8_t ipv6Addr[R_IPV6_ADDRESS_LENGTH];
            res = R_NWK_GetRequest(R_NWK_nwkIpv6Address, ipv6Addr, sizeof(ipv6Addr));
            if (res == R_RESULT_SUCCESS)
            {
                route_Found = R_TRUE;
                R_Modem_print("ROUTELST_GETC %02X %02X 01 ", handle, res);
                AppPrintIPv6Addr(ipv6Addr);
                R_Modem_print(" ");
                AppPrintIPv6Addr(preferredParent);
                R_Modem_print(" ");
                AppPrintIPv6Addr(alternateParent);
                R_Modem_print(" FFFFFFFF ");  // Lifetime placeholder (RN routes format must be consistent with BR routes)
                R_Modem_print("\n");
            }
        }
    }

    if (!route_Found)
    {
        R_Modem_print("ROUTELST_GETC %02X %02X 00\n", handle, res);
    }
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_RPLINFO_GETR
* Description       : Process "RPLINFO_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   RPLINFO_GETR (Handle) (Options)
* Return Value      : None
* Output            : Confirm command
*                   :   RPLINFO_GETC (Handle) (Status) [(InstanceId) (DodagId)
*                                    (DodagVersion) (DodagPreference) (MOP) (Rank) (DTSN)]
********************************************************************************/
static void AppCmd_ProcessCmd_RPLINFO_GETR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    /* Get RPL info from NWK */
    r_nwk_rpl_info_t rplInfo;
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_GetRequest(R_NWK_rplInfo, &rplInfo, sizeof(rplInfo));
    }

    /* Print confirm */
    R_Modem_print("RPLINFO_GETC %02X %02X", handle, res);
    if (res == R_RESULT_SUCCESS)
    {
        R_Modem_print(" %02X ", rplInfo.instance_id);
        AppPrintIPv6Addr(rplInfo.dodag_id.bytes);
        R_Modem_print(" %02X %02X %02X %02X %02X", rplInfo.dodag_version, rplInfo.dodag_preference, rplInfo.mop, rplInfo.rank, rplInfo.dtsn);
    }
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_NDCACHE_GETR
* Description       : Process "NDCACHE_GETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   NDCACHE_GETR (Handle) [(Options)]
* Return Value      : None
* Output            : Confirm command
*                   :   NDCACHE_GETC (Handle) (Status) (NdCacheEntries)
********************************************************************************/
static void AppCmd_ProcessCmd_NDCACHE_GETR(uint8_t* pCmd)
{
    r_result_t res;

    /* Get command parameters */
    uint8_t handle;
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t options;
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    r_boolean_t isContinuation = R_FALSE;
    do
    {
        r_app_nd_cache_buffer_t cfm;
        res = R_NWK_GetRequestMultipart(R_NWK_ndCache, &cfm, sizeof(cfm), isContinuation);
        if (res == R_RESULT_SUCCESS || res == R_RESULT_SUCCESS_ADDITIONAL_DATA)
        {
            if (!isContinuation)
            {
                isContinuation = R_TRUE;
                R_Modem_print("NDCACHE_GETC %02X %02X ", handle, R_RESULT_SUCCESS);  // Write begin of confirm message
            }

            for (uint16_t i = 0; i < cfm.nd_cache.count; i++)
            {
                /* MACAddr */
                AppPrintMACAddr(cfm.nd_cache.entries[i].llAddress.bytes);
                R_Modem_print(" ");

                /* etx */
                R_Modem_print("%04X ", cfm.nd_cache.entries[i].linkMetric);

                /* rsl */
                R_Modem_print("%04X ", cfm.nd_cache.entries[i].neighToNodeRsl);

                /* rssi */
                R_Modem_print("%04X ", cfm.nd_cache.entries[i].rssi);

                /* panSize */
                R_Modem_print("%04X ", cfm.nd_cache.entries[i].panSize);

                /* RPL rank */
                R_Modem_print("%04X ", cfm.nd_cache.entries[i].rank);

                /* routingCost */
                R_Modem_print("%04X ", cfm.nd_cache.entries[i].routingCost);

                /* IPv6Addr */
                AppPrintIPv6Addr(cfm.nd_cache.entries[i].ipAddress.bytes);
                R_Modem_print(" ");

                /* timeSinceLastRx */
                R_Modem_print("%04X ", (unsigned)(cfm.nd_cache.entries[i].lastReceiveSecondsAgo * 1000));

                /* isParentStatus */
                R_Modem_print("%04X ", cfm.nd_cache.entries[i].status);
            }
        }
        else
        {
            R_Modem_print("NDCACHE_GETC %02X %02X ", handle, res);  // Write confirm message with error code
            break;
        }
    }
    while (res == R_RESULT_SUCCESS_ADDITIONAL_DATA);  // Continue as long as more data is available

    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_IPSR
* Description       : Process "IPSR" command
* Arguments         : pCmd ... Pointer to command
*                   :   IPSR (Handle) (DataLength) (Data) [(Options)]
* Return Value      : None
* Output            : Confirm command
*                   :   UDPSC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_IPSR(uint8_t* pCmd)
{
    /* Get command parameters */
    /* (Handle) */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (PayloadSize) */
    uint16_t dataLength;
    res |= AppCmd_HexStrToNum(&pCmd, &dataLength, sizeof(dataLength), R_FALSE);

    /* (Payload) */
    uint8_t* data = NULL;
    if (res == R_RESULT_SUCCESS)
    {
        /* We assume that the IPv6 tunneling header is already prepended -> Max size is IPv6 MTU + IPv6 header size */
        if (dataLength <= R_WAN_IP_MTU + R_IPV6_HEADER_SIZE)
        {
            data = pCmd;
            if (dataLength != 0)
            {
                res = AppCmd_HexStrToNum(&pCmd, data, dataLength, R_TRUE);
            }
        }
        else
        {
            res = R_RESULT_INVALID_PARAMETER;
        }
    }

    /* [(Options)] */
    uint16_t options = 0;
    AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    /* Force to set blocking. */
    options &= (~R_OPTIONS_NON_BLOCKING);

    /* Allow IP data request only in Join State 5 */
    if (p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
    {
        if (res == R_RESULT_SUCCESS)
        {
            res = R_NWK_IP_DataRequest(data, dataLength, options);
        }
    }
    else
    {
        res = R_RESULT_ILLEGAL_STATE;
    }

    /* Confirm(IPSC) */
    R_Modem_print("IPSC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_UDPSR
* Description       : Process "UDPSR" command
* Arguments         : pCmd ... Pointer to command
*                   :   UDPSR (Handle) (FrameType) (DstIPv6Addr) (DstPort) (SrcPort)
*                   :         (PayloadSize) (Payload) [(Options)]
* Return Value      : None
* Output            : Confirm command
*                   :   UDPSC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_UDPSR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint8_t dstAddress[R_IPV6_ADDRESS_LENGTH];
    uint8_t srcAddress[R_IPV6_ADDRESS_LENGTH];
    uint16_t dstPort, srcPort, payloadSize;
    uint8_t* p_payload;
    uint16_t options;
    uint8_t isEdfe;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (DstIPv6Addr) */
    res |= AppCmd_HexStrToNum(&pCmd, dstAddress, sizeof(dstAddress), R_TRUE);

    /* (SrcIPv6Addr) */
    res |= AppCmd_HexStrToNum(&pCmd, srcAddress, sizeof(srcAddress), R_TRUE);

    /* (DstPort), (SrcPort) */
    res |= AppCmd_HexStrToNum(&pCmd, &dstPort, sizeof(dstPort), R_FALSE);
    res |= AppCmd_HexStrToNum(&pCmd, &srcPort, sizeof(srcPort), R_FALSE);

    /* (FrameExchangeMode) */
    res |= AppCmd_HexStrToNum(&pCmd, &isEdfe, sizeof(handle), R_FALSE);

    /* (PayloadSize), (Payload) */
    res |= AppCmd_HexStrToNum(&pCmd, &payloadSize, sizeof(payloadSize), R_FALSE);
    if (res == R_RESULT_SUCCESS)
    {
        if (payloadSize <= R_UDP_MAX_UDP_DATA_LENGTH)
        {
            p_payload = pCmd;
            if (payloadSize != 0)
            {
                res = AppCmd_HexStrToNum(&pCmd, p_payload, payloadSize, R_TRUE);
            }
        }
        else
        {
            res = R_RESULT_INVALID_PARAMETER;
        }
    }

    /* [(Options)] */
    AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    /* Force to set blocking. */
    options &= (~R_OPTIONS_NON_BLOCKING);

    if (isEdfe)
    {
#if R_EDFE_INITIATOR_DISABLED
        res = R_RESULT_UNSUPPORTED_FEATURE;
#else
        options |= R_OPTIONS_FRAME_EXCHANGE_EDFE;
#endif
    }

    if (res == R_RESULT_SUCCESS)
    {
        res = R_UDP_DataRequest(dstAddress, dstPort, srcPort,
                                p_payload, payloadSize, options);
    }

    /* Confirm(UDPSC) */
    R_Modem_print("UDPSC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_ICMPSR
* Description       : Process "ICMPSR" command
* Arguments         : pCmd ... Pointer to command
*                   :   ICMPSR (Handle) (DstIPv6Addr) (Type=0x80)
*                   :       (Identifier) (Sequence) (PayloadSize) (Payload) [(Options)]
* Return Value      : None
* Output            : Confirm command
*                   :   ICMPSC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_ICMPSR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle, type;
    uint8_t dstAddress[R_IPV6_ADDRESS_LENGTH];
    uint8_t srcAddress[R_IPV6_ADDRESS_LENGTH];
    uint8_t hoplimit;
    uint16_t identifier, sequence, payloadSize;
    uint8_t* p_payload;
    uint16_t options;
    uint8_t isEdfe;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (DstIPv6Addr) */
    res |= AppCmd_HexStrToNum(&pCmd, dstAddress, sizeof(dstAddress), R_TRUE);

    /* (SrcIPv6Addr) */
    res |= AppCmd_HexStrToNum(&pCmd, srcAddress, sizeof(srcAddress), R_TRUE);

    /* (HopLimit) */
    res |= AppCmd_HexStrToNum(&pCmd, &hoplimit, sizeof(hoplimit), R_TRUE);

    /* (Type) */
    res |= AppCmd_HexStrToNum(&pCmd, &type, sizeof(type), R_FALSE);
    if (res == R_RESULT_SUCCESS)
    {
        switch (type)
        {
            /* Type = 0x80: Echo Request */
            case R_ICMP_TYPE_ECHO_REQUEST:

                /* (Identifier) */
                res = AppCmd_HexStrToNum(&pCmd, &identifier, sizeof(identifier), R_FALSE);

                /* (Sequence) */
                res |= AppCmd_HexStrToNum(&pCmd, &sequence, sizeof(sequence), R_FALSE);

                /* (FrameExchangeMode) */
                res |= AppCmd_HexStrToNum(&pCmd, &isEdfe, sizeof(handle), R_FALSE);

                /* (PayloadSize), (Payload) */
                res |= AppCmd_HexStrToNum(&pCmd, &payloadSize, sizeof(payloadSize), R_FALSE);
                if (res == R_RESULT_SUCCESS)
                {
                    if (payloadSize <= R_ICMP_MAX_ECHO_DATA_LENGTH)
                    {
                        p_payload = pCmd;
                        if (payloadSize != 0)
                        {
                            res = AppCmd_HexStrToNum(&pCmd, p_payload, payloadSize, R_TRUE);
                        }
                    }
                    else
                    {
                        res = R_RESULT_INVALID_PARAMETER;
                    }
                }

                /* [(Options)] */
                res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

                /* Force to set blocking. */
                options &= (~R_OPTIONS_NON_BLOCKING);

                if (isEdfe)
                {
#if R_EDFE_INITIATOR_DISABLED
                    res = R_RESULT_UNSUPPORTED_FEATURE;
#else
                    options |= R_OPTIONS_FRAME_EXCHANGE_EDFE;
#endif
                }

                if (res == R_RESULT_SUCCESS)
                {
                    res = R_ICMP_EchoRequest(dstAddress,
                                             identifier,
                                             sequence,
                                             p_payload,
                                             payloadSize,
                                             options,
                                             hoplimit);
                }
                break;

            default:
                res = R_RESULT_INVALID_PARAMETER;
                break;
        }
    }

    /* Confirm(ICMPSC) */
    R_Modem_print("ICMPSC %02X %02X\n", handle, res);
}

#if R_PHY_TYPE_CWX_M && R_MDR_ENABLED
/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_MDRSR
* Description       : Process "MDRSR" command
* Arguments         : pCmd ... Pointer to command
*                   :   MDRSR (Handle) (Modulation)
* Return Value      : None
* Output            : Confirm command
*                   :   MDRSR (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_MDRSR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint8_t phyMDRNewModeSwitchBank;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (Modulation) */
    res |= AppCmd_HexStrToNum(&pCmd, &phyMDRNewModeSwitchBank, sizeof(phyMDRNewModeSwitchBank), R_FALSE);

    // Set new bank
    if (res == R_RESULT_SUCCESS)
    {
        res |= R_NWK_SetRequest(R_NWK_phyMDRNewModeSwitchBank, &phyMDRNewModeSwitchBank, sizeof(phyMDRNewModeSwitchBank));
    }

    /* Confirm(MDRSC) */
    R_Modem_print("MDRSC %02X %02X\n", handle, res);
}
#endif /* R_PHY_TYPE_CWX_M && R_MDR_ENABLED */

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_SPDR
* Description       : Process "SPDR" command
* Arguments         : pCmd ... Pointer to command
*                   :   SPDR (Handle)
* Return Value      : None
* Output            : Confirm command
*                   :   SPDC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_SPDR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_Suspend();
    }

    /* Confirm(SPDC) */
    R_Modem_print("SPDC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_RSMR
* Description       : Process "RSMR" command
* Arguments         : pCmd ... Pointer to command
*                   :   RSMR (Handle) (suspendedTime)
* Return Value      : None
* Output            : Confirm command
*                   :   RSMC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_RSMR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint32_t suspendedTimeMs;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (suspendedTime) */
    res |= AppCmd_HexStrToNum(&pCmd, &suspendedTimeMs, sizeof(suspendedTimeMs), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_Resume(suspendedTimeMs);
    }

    /* Confirm(RSMC) */
    R_Modem_print("RSMC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_REVOKEKEYSR
* Description       : Process "REVOKEKEYSR" command
* Arguments         : pCmd ... Pointer to command
*                   :   REVOKEKEYSR (Handle) (Options) (GTK)
* Return Value      : None
* Output            : Confirm command
*                   :   REVOKEKEYSC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_REVOKEKEYSR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t options;
    uint8_t newGtk[16];

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (Options) */
    res |= AppCmd_HexStrToNum(&pCmd, &options, sizeof(options), R_FALSE);

    /* (GTK) */
    res |= AppCmd_HexStrToNum(&pCmd, newGtk, sizeof(newGtk), R_TRUE);
    if (res == R_RESULT_SUCCESS)
    {
#if R_BR_AUTHENTICATOR_ENABLED
        if (AppCmdConfig.deviceType == R_BORDERROUTER && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
        {
            res = R_AUTH_BR_RevokeGTKs(p_aplGlobal, newGtk);
            if (res == R_RESULT_SUCCESS)
            {
                res = R_NWK_IncreasePanVersionRequest();  // Increment PAN Version to finish revocation
            }
        }
#else
        res = R_RESULT_UNSUPPORTED_FEATURE;  // Key revocation can only be performed on border routers
#endif
    }

    R_Modem_print("REVOKEKEYSC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_REVOKESUPPR
* Description       : Process "REVOKESUPPR" command
* Arguments         : pCmd ... Pointer to command
*                   :   REVOKESUPPR (Handle) (MAC)
* Return Value      : None
* Output            : Confirm command
*                   :   REVOKESUPPC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_REVOKESUPPR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint8_t supplicant[R_MAC_EXTENDED_ADDRESS_LENGTH];

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (Supplicant) */
    res |= AppCmd_HexStrToNum(&pCmd, supplicant, sizeof(supplicant), R_TRUE);
    if (res == R_RESULT_SUCCESS)
    {
#if R_BR_AUTHENTICATOR_ENABLED
        if (AppCmdConfig.deviceType == R_BORDERROUTER && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
        {
            res = R_AUTH_BR_RevokeSupplicant(p_aplGlobal, supplicant);
            if (res == R_RESULT_SUCCESS)
            {
                res = R_NWK_IncreasePanVersionRequest();  // Increment PAN Version to finish revocation
            }
        }
#else
        res = R_RESULT_UNSUPPORTED_FEATURE;  // Supplicant revocation can only be performed on border routers
#endif
    }

    R_Modem_print("REVOKESUPPC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_DEVICEKICKR
* Description       : Process "DEVICEKICKR" command
* Arguments         : pCmd ... Pointer to command
*                   :   DEVICEKICKR (Handle) (DstIPv6Addr)
* Return Value      : None
* Output            : Confirm command
*                   :   DEVICEKICKC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_DEVICEKICKR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    uint8_t dstIpAddr[R_IPV6_ADDRESS_LENGTH];
    res |= AppCmd_HexStrToNum(&pCmd, dstIpAddr, sizeof(dstIpAddr), R_TRUE);

#if R_BORDER_ROUTER_ENABLED
    if (AppCmdConfig.deviceType == R_BORDERROUTER)
    {
        uint8_t srcAddress[R_IPV6_ADDRESS_LENGTH];
        res |= R_NWK_GetRequest(R_NWK_nwkIpv6Address, srcAddress, sizeof(srcAddress));

        uint16_t dstPort = KICK_DEVICE_MSG_UDP_PORT;
        uint16_t srcPort = KICK_DEVICE_MSG_UDP_PORT;

        uint8_t payload[] = KICK_DEVICE_MSG_PAYLOAD;
        uint16_t payloadSize = sizeof(payload);

        uint16_t options = 0;
        if (res == R_RESULT_SUCCESS)
        {
            res = R_UDP_DataRequest(dstIpAddr, dstPort, srcPort, payload, payloadSize, options);
        }
    }
    else
#endif /* R_BORDER_ROUTER_ENABLED */
    {
        res = R_RESULT_UNSUPPORTED_FEATURE;  // Device kick message may only be sent by border routers
    }
    R_Modem_print("DEVICEKICKC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_LEAVENETWORKR
* Description       : Process "LEAVENETWORKR" command
* Arguments         : pCmd ... Pointer to command
*                   :   AppCmd_ProcessCmd_LEAVENETWORKR (Handle)
* Return Value      : None
* Output            : Confirm command
*                   :   LEAVENETWORKC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_LEAVENETWORKR(uint8_t* pCmd)
{
    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
        res = R_RESULT_ILLEGAL_STATE;
        if (AppCmdConfig.deviceType == R_ROUTERNODE && p_aplGlobal->nwk.joinState == R_NWK_JoinState5_Operational)
        {
            res = AppCmd_LeaveNetwork();
        }
    }

    R_Modem_print("LEAVENETWORKC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_IPV6PREFIX_SETR
* Description       : Process "IPV6PREFIX_SETR" command
* Arguments         : pCmd ... Pointer to command
*                   :   IPV6PREFIX_SETR (Handle) (Options) (IPv6 Prefix)
* Return Value      : None
* Output            : Confirm command
*                   :   IPV6PREFIX_SETC (Handle) (Status)
********************************************************************************/
static void AppCmd_ProcessCmd_IPV6PREFIX_SETR(uint8_t* pCmd)
{
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    uint8_t prefix[R_IPV6_PREFIX_LEN];
    res |= AppCmd_HexStrToNum(&pCmd, &prefix, sizeof(prefix), R_TRUE);

#if R_BORDER_ROUTER_ENABLED
    if (res == R_RESULT_SUCCESS)
    {
        if (res == R_RESULT_SUCCESS && p_aplGlobal->nwk.joinState == R_NWK_JoinState0_Reset)
        {
            R_memcpy(AppCmdConfig.globalIpAddress.bytes, prefix, sizeof(prefix));
        }
        else
        {
            res = R_RESULT_ILLEGAL_STATE;  // IPv6 prefix/address may only be set before starting the device
        }
    }
#else  /* R_BORDER_ROUTER_ENABLED */
    res = R_RESULT_UNSUPPORTED_FEATURE;  // IPv6 prefix/address may only be set on border routers
#endif  /* R_BORDER_ROUTER_ENABLED */

    R_Modem_print("IPV6PREFIX_SETC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_DEVCONF_SETR
* Description       : Process "SETR" command - ParamType = ConfigParams
* Arguments         : pCmd ... Pointer to command parameters
*                   :   ConfigParams: (DeviceType) (PANId) (NetworkName) (MTUsize)
* Return Value      : None
********************************************************************************/
static void AppCmd_ProcessCmd_DEVCONF_SETR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t option;

    uint8_t deviceType;
    uint16_t panId;
    uint16_t panSize;
    uint8_t useParent_BS_IE;
    uint8_t networkName[R_NETWORK_NAME_STRING_MAX_BYTES];

    uint8_t demo_mode;
    uint16_t sixLowPanMTU;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (option) */
    res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);

    r_app_config_t* p_config = &AppCmdConfig;

    /* Get command parameters - ConfigParams */
    /* DeviceType */
    res |= AppCmd_HexStrToNum(&pCmd, &deviceType, sizeof(deviceType), R_FALSE);

    /* PANId */
    res |= AppCmd_HexStrToNum(&pCmd, &panId, sizeof(panId), R_FALSE);

    /* PANSize */
    res |= AppCmd_HexStrToNum(&pCmd, &panSize, sizeof(panSize), R_FALSE);

    /* UseParent_BS_IE */
    res |= AppCmd_HexStrToNum(&pCmd, &useParent_BS_IE, sizeof(useParent_BS_IE), R_FALSE);

    /* NetworkName */
    res |= AppCmd_HexStrToNum(&pCmd, &networkName, sizeof(networkName), R_TRUE);

    /* SixLowPanMTU */
    res |= AppCmd_HexStrToNum(&pCmd, &sixLowPanMTU, sizeof(sixLowPanMTU), R_FALSE);

    /* DemoMode */
    res |= AppCmd_HexStrToNum(&pCmd, &demo_mode, sizeof(demo_mode), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
        /* set PANID, PAN size and useParent_BS_IE */
        p_config->panId = panId;
        p_config->panSize = panSize;
        p_config->useParent_BS_IE = useParent_BS_IE;

        /* set global Network Name used for Network Name Information Element (NETNAME-IE) */
        memset(p_config->networkName, 0x00, sizeof(p_config->networkName));
        memcpy(p_config->networkName, networkName, sizeof(p_config->networkName));
        p_config->networkName[sizeof(p_config->networkName) - 1] = 0;  // Ensure null-termination

        /* Set operation mode (demo mode)*/
        res |= R_NWK_SetRequest(R_NWK_rplDemoMode, &demo_mode, sizeof(demo_mode));
    }
    if (res == R_RESULT_SUCCESS)
    {
        res |= R_NWK_SetRequest(R_NWK_sixLowpanMtu, &sixLowPanMTU, sizeof(sixLowPanMTU));
    }
    if (res == R_RESULT_SUCCESS)
    {
        p_config->deviceType = deviceType;
        res |= R_NWK_SetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
    }

    /* Send Confirm -> DEVCONF_SETC */
    R_Modem_print("DEVCONF_SETC %02X %02X\n", handle, res);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_DEVCONF_GETR
* Description       : Process "GETR" command (Output common message)
* Arguments         : r_result_t ... result when get command
*                   : handle ... index
*                   : paramType ... parameter type
* Return Value      : None
*                   : paramType ... parameter type (=PhyConfig)
* Return Value      : None
* Output            : Confirm command
*                   :   ConfigParams: (DeviceType) (PANId) (NetworkName) (MTUsize)
********************************************************************************/
static void AppCmd_ProcessCmd_DEVCONF_GETR(uint8_t* pCmd)
{

    uint8_t deviceType;
    uint16_t panId;

    uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH];
    uint8_t ipv6Addr[R_IPV6_ADDRESS_LENGTH];

    uint8_t demo_mode;
    uint16_t sixLowPanMTU;

    /* Get command parameters */
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    uint16_t option;
    res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
        /* Obtain values from IPv6 stack */
        /* (DeviceType) */
        res |= R_NWK_GetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));

        /* (PANId) */
        res |= R_NWK_GetRequest(R_NWK_macPANId, &panId, sizeof(panId));

        /* (MACAddr) */
        res |= R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));

        /* (IPv6Addr) */
        res |= R_NWK_GetRequest(R_NWK_nwkIpv6Address, ipv6Addr, sizeof(ipv6Addr));

        /* (DemoMode) */
        res |= R_NWK_GetRequest(R_NWK_rplDemoMode, &demo_mode, sizeof(demo_mode));
    }

    /* Get sixLowpanMtu */
    res |= R_NWK_GetRequest(R_NWK_sixLowpanMtu, &sixLowPanMTU, sizeof(sixLowPanMTU));


    /* Generate confirm DEVCONF_GETC*/
    R_Modem_print("DEVCONF_GETC %02X %02X ", handle, res);
    if (res == R_RESULT_SUCCESS)
    {
        /* DeviceType */
        R_Modem_print("%02X ", deviceType);

        /* PANId */
        if (deviceType == R_ROUTERNODE)
        {
            R_Modem_print("%04X ", panId);
        }
        else
        {
            R_Modem_print("%04X ", AppCmdConfig.panId);
        }

        /* PANSize */
        R_Modem_print("%04X ", AppCmdConfig.panSize);

        /* UseParent_BS_IE */
        R_Modem_print("%02X ", AppCmdConfig.useParent_BS_IE);

        /* NetworkName */
        AppPrintNetworkName(&(AppCmdConfig.networkName[0]));
        R_Modem_print(" ");

        /* SixLowPanMTU */
        R_Modem_print("%04X ", sixLowPanMTU);

        /* DemoMode */
        R_Modem_print("%02X ", demo_mode);

        /* MACAddr */
        R_Swap64((uint8_t*)(&macAddr[0]), (uint8_t*)(&macAddr[0]));
        AppPrintMACAddr(macAddr);
        R_Modem_print(" ");

        /* IPv6Addr */
        AppPrintIPv6Addr(ipv6Addr);
    }
    R_Modem_print("\n");
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_PHYFAN_SETR
* Description       : Process "SETR" command - ParamType = ConfigParams
* Arguments         : pCmd ... Pointer to command parameters
*                   :   PhyConfig: (phy_band) (phy_operation_mode)
*                   :                (txPower) (phy_RegulatoryMode)
* Return Value      : None
********************************************************************************/
#if R_PHY_TYPE_CWX_M
static void AppCmd_ProcessCmd_PHYFAN_SETR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t option;

    uint8_t wisun_regulatory_domain;
    uint8_t wisun_operating_class;
    uint8_t wisun_operating_mode;
    uint8_t phy_txPower;
    uint8_t phy_RegulatoryMode;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (option) */
    res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);

    /* Get command parameters - ConfigParams */
    /* wisun_regulatory_domain */
    res |= AppCmd_HexStrToNum(&pCmd, &wisun_regulatory_domain, sizeof(wisun_regulatory_domain), R_FALSE);

    /* wisun_operating_class */
    res |= AppCmd_HexStrToNum(&pCmd, &wisun_operating_class, sizeof(wisun_operating_class), R_FALSE);

    /* wisun_operating_mode */
    res |= AppCmd_HexStrToNum(&pCmd, &wisun_operating_mode, sizeof(wisun_operating_mode), R_FALSE);

    /* phy_TxPower */
    res |= AppCmd_HexStrToNum(&pCmd, &phy_txPower, sizeof(phy_txPower), R_FALSE);

    /* phy_RegulatoryMode */
    res |= AppCmd_HexStrToNum(&pCmd, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode), R_FALSE);

    /* Apply PHY configuration according to the Wi-SUN PHY specification */
    if (res == R_RESULT_SUCCESS)
    {
        r_nwk_wisun_phy_config_t wisun_phy_config;

        wisun_phy_config.regulatory_domain = wisun_regulatory_domain;
        wisun_phy_config.operating_class = wisun_operating_class;
        wisun_phy_config.operating_mode = wisun_operating_mode;
        res |= R_NWK_SetRequest(R_NWK_phyWiSunPhyConfig, &wisun_phy_config, sizeof(wisun_phy_config));
    }

    if (res == R_RESULT_SUCCESS)
    {
        uint16_t phy_CcaVth = 0xFF5A;  // set to -85 dBm as default, depending on the region (regulatory domain) and
                                       // the data rate (operating mode) optimization of the CCA level threshold is recommended.
        res |= R_NWK_SetRequest(R_NWK_phyFskCcaVth, &phy_CcaVth, sizeof(phy_CcaVth));
        res |= R_NWK_SetRequest(R_NWK_phyOfdmCcaVth, &phy_CcaVth, sizeof(phy_CcaVth));
    }

    if (res == R_RESULT_SUCCESS)
    {
        phy_txPower = 0xFB;  // - 5dbm
        res |= R_NWK_SetRequest(R_NWK_phyTransmitPower, &phy_txPower, sizeof(phy_txPower));
        res |= R_NWK_GetRequest(R_NWK_phyTransmitPower, &phy_txPower, sizeof(phy_txPower));
    }

    /* Send Confirm -> PHY_SETC */
    R_Modem_print("PHYFAN_SETC %02X %02X\n", handle, res);
}
#else /* R_PHY_TYPE_CWX_M */
static void AppCmd_ProcessCmd_PHYFAN_SETR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t option;

    uint8_t wisun_regulatory_domain;
    uint8_t wisun_operating_class;
    uint8_t wisun_operating_mode;
    uint8_t phy_txPower;
    uint8_t phy_RegulatoryMode;

    uint8_t phy_AntennaSelectTx;
    uint16_t phy_CcaVth;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (option) */
    res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);

    /* Get command parameters - ConfigParams */
    /* wisun_regulatory_domain */
    res |= AppCmd_HexStrToNum(&pCmd, &wisun_regulatory_domain, sizeof(wisun_regulatory_domain), R_FALSE);

    /* wisun_operating_class */
    res |= AppCmd_HexStrToNum(&pCmd, &wisun_operating_class, sizeof(wisun_operating_class), R_FALSE);

    /* wisun_operating_mode */
    res |= AppCmd_HexStrToNum(&pCmd, &wisun_operating_mode, sizeof(wisun_operating_mode), R_FALSE);

    /* phy_TxPower */
    res |= AppCmd_HexStrToNum(&pCmd, &phy_txPower, sizeof(phy_txPower), R_FALSE);

    /* phy_RegulatoryMode */
    res |= AppCmd_HexStrToNum(&pCmd, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode), R_FALSE);

    /* Apply PHY configuration according to the Wi-SUN PHY specification */
    if (res == R_RESULT_SUCCESS)
    {
        r_nwk_wisun_phy_config_t wisun_phy_config;

        wisun_phy_config.regulatory_domain = wisun_regulatory_domain;
        wisun_phy_config.operating_class = wisun_operating_class;
        wisun_phy_config.operating_mode = wisun_operating_mode;
        res |= R_NWK_SetRequest(R_NWK_phyWiSunPhyConfig, &wisun_phy_config, sizeof(wisun_phy_config));
    }

    if (res == R_RESULT_SUCCESS)
    {
        res |= R_NWK_SetRequest(R_NWK_phyTransmitPower, &phy_txPower, sizeof(phy_txPower));
    }
    if (res == R_RESULT_SUCCESS)
    {
        res |= R_NWK_SetRequest(R_NWK_phyRegulatoryMode, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode));
    }
    if (res == R_RESULT_SUCCESS)
    {
        phy_AntennaSelectTx = 1;  //  0 = TX and RX via connector J2;  1 = TX and RX via connector J3;
        res |= R_NWK_SetRequest(R_NWK_phyAntennaSelectTx, &phy_AntennaSelectTx, sizeof(phy_AntennaSelectTx));
    }
    if (res == R_RESULT_SUCCESS)
    {
        phy_CcaVth = 0x01AA;  // set to -86 dBm as default, depending on the region (regulatory domain) and
                              // the data rate (operating mode) optimization of the CCA level threshold is recommended.
        res |= R_NWK_SetRequest(R_NWK_phyCcaVth, &phy_CcaVth, sizeof(phy_CcaVth));
    }

#if defined(FEM_SE2435L)
    // Initialize Front-end module_SE2435L (ANT1 port) / case2.
    // Adapt device minimum sense to support FEM_SE2435L.

    // The minimum receiver sensitivity level (0 -> -174 dBm; 254 -> +80 dBm).
    uint16_t phy_DataRate;
    res |= R_NWK_GetRequest(R_NWK_phyDataRate, &phy_DataRate, sizeof(phy_DataRate));

    // -------------------------------------------------
    //  DEVICE_MIN_SEN = Max. Sensitivity -4db +6dB
    // -------------------------------------------------
    //                      | 50kbps | 100kbps | 150kbps
    // -------------------------------------------------
    //  Maximum Sensitivity |  -102  |  -99    |  -97
    //  device_min_sens     |  -100  |  -97    |  -95
    // -------------------------------------------------
    uint8_t device_min_sens;
    if (phy_DataRate == 150)      // 150kbps mode
    {
        device_min_sens = 74;     // -100 dBm
    }
    else if (phy_DataRate == 100) // 100kbps mode
    {
        device_min_sens = 77;     // -97 dBm
    }
    else  // 50kbps mode
    {
        device_min_sens = 79;  // -95 dBm
    }
    res |= R_NWK_SetRequest(R_NWK_deviceMinSens, &device_min_sens, sizeof(device_min_sens));

    uint8_t phyAgcWaitGainOffset = 0x0D;
    res |= R_NWK_SetRequest(R_NWK_phyAgcWaitGainOffset, &phyAgcWaitGainOffset, sizeof(phyAgcWaitGainOffset));

    uint16_t phyCcaVth = 0x01AA;
    res |= R_NWK_SetRequest(R_NWK_phyCcaVth, &phyCcaVth, sizeof(phyCcaVth));

    uint8_t phyCcaVthOffset = 0x11;
    res |= R_NWK_SetRequest(R_NWK_phyCcaVthOffset, &phyCcaVthOffset, sizeof(phyCcaVthOffset));
    res |= R_NWK_SetRequest(R_NWK_phyRssiOutputOffset, &phyCcaVthOffset, sizeof(phyCcaVthOffset));

    uint8_t phyAntennaSwitchEna = 0x01;
    res |= R_NWK_SetRequest(R_NWK_phyAntennaSwitchEna, &phyAntennaSwitchEna, sizeof(phyAntennaSwitchEna));

    uint16_t phyAntennaSwitchEnaTiming = 0x012C;
    res |= R_NWK_SetRequest(R_NWK_phyAntennaSwitchEnaTiming, &phyAntennaSwitchEnaTiming, sizeof(phyAntennaSwitchEnaTiming));

#elif defined(VIZMO_MODULE_28dBm)
    // Initialize Front-end module_SE2435L (ANT2 port) / case1.
    // Adapt device minimum sense.

    // The minimum receiver sensitivity level (0 -> -174 dBm; 254 -> +80 dBm).
    uint16_t phy_DataRate;
    res |= R_NWK_GetRequest(R_NWK_phyDataRate, &phy_DataRate, sizeof(phy_DataRate));

    // -------------------------------------------------
    //  DEVICE_MIN_SEN = Max. Sensitivity -4db +6dB
    // -------------------------------------------------
    //                      | 50kbps | 100kbps | 150kbps
    // -------------------------------------------------
    //  Maximum Sensitivity |  -102  |  -99    |  -97
    //  device_min_sens     |  -100  |  -97    |  -95
    // -------------------------------------------------
    uint8_t device_min_sens;
    if (phy_DataRate == 150)      // 150kbps mode
    {
        device_min_sens = 74;     // -100 dBm
    }
    else if (phy_DataRate == 100) // 100kbps mode
    {
        device_min_sens = 77;     // -97 dBm
    }
    else  // 50kbps mode
    {
        device_min_sens = 79;  // -95 dBm
    }
    res |= R_NWK_SetRequest(R_NWK_deviceMinSens, &device_min_sens, sizeof(device_min_sens));

    uint8_t phyAgcWaitGainOffset = 0x0D;
    res |= R_NWK_SetRequest(R_NWK_phyAgcWaitGainOffset, &phyAgcWaitGainOffset, sizeof(phyAgcWaitGainOffset));

    uint16_t phyCcaVth = 0x01AA;
    res |= R_NWK_SetRequest(R_NWK_phyCcaVth, &phyCcaVth, sizeof(phyCcaVth));

    uint8_t phyCcaVthOffset = 0x11;
    res |= R_NWK_SetRequest(R_NWK_phyCcaVthOffset, &phyCcaVthOffset, sizeof(phyCcaVthOffset));
    res |= R_NWK_SetRequest(R_NWK_phyRssiOutputOffset, &phyCcaVthOffset, sizeof(phyCcaVthOffset));

    uint8_t phyAntennaSwitchEna = 0x01;
    res |= R_NWK_SetRequest(R_NWK_phyAntennaSwitchEna, &phyAntennaSwitchEna, sizeof(phyAntennaSwitchEna));

    uint16_t phyAntennaSwitchEnaTiming = 0x012C;
    res |= R_NWK_SetRequest(R_NWK_phyAntennaSwitchEnaTiming, &phyAntennaSwitchEnaTiming, sizeof(phyAntennaSwitchEnaTiming));

    uint8_t phyGpio0Setting = 0x01;
    res |= R_NWK_SetRequest(R_NWK_phyGpio0Setting, &phyGpio0Setting, sizeof(phyGpio0Setting));
#else  /* if defined(FEM_SE2435L) */
    uint16_t phy_DataRate;
    res |= R_NWK_GetRequest(R_NWK_phyDataRate, &phy_DataRate, sizeof(phy_DataRate));

    // -------------------------------------------------
    //  DEVICE_MIN_SEN = Max. Sensitivity +6dB
    // -------------------------------------------------
    //                      | 50kbps | 100kbps | 150kbps
    // -------------------------------------------------
    //  Maximum Sensitivity |  -102  |  -99    |  -97
    //  device_min_sens     |   -96  |  -93    |  -91
    // -------------------------------------------------
    uint8_t device_min_sens;
    if (phy_DataRate == 150)      // 150kbps mode
    {
        device_min_sens = 83;     // -91 dBm
    }
    else if (phy_DataRate == 100) // 100kbps mode
    {
        device_min_sens = 81;     // -93 dBm
    }
    else  // 50kbps mode
    {
        device_min_sens = 78;  // -96 dBm
    }
    res |= R_NWK_SetRequest(R_NWK_deviceMinSens, &device_min_sens, sizeof(device_min_sens));
#endif /* if defined(FEM_SE2435L) */

    /* Send Confirm -> PHY_SETC */
    R_Modem_print("PHYFAN_SETC %02X %02X\n", handle, res);

}
#endif /* R_PHY_TYPE_CWX_M */

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_PHYFAN_GETR
* Description       : Process "GETR" command (Output common message)
* Arguments         : r_result_t ... result when get command
*                   : handle ... index
*                   : paramType ... parameter type
* Return Value      : None
*                   : paramType ... parameter type (=PhyConfig)
* Return Value      : None
* Output            : Confirm command
*                   :   GETC (Handle) (Status) (ParamType)
*                   :     PhyConfig:
*                   :       (FreqBandId) (FSKOpeMode) (TxPower) (RegulatoryMode)
********************************************************************************/
static void AppCmd_ProcessCmd_PHYFAN_GETR(uint8_t* pCmd)
{
    r_result_t res;
    uint8_t handle;
    uint16_t option;

    r_nwk_wisun_phy_config_t wisun_phy_config;

    uint8_t phy_txpower;
    uint8_t phy_RegulatoryMode;

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (option) */
    res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
        /* (TxPower) */
        res |= R_NWK_GetRequest(R_NWK_phyTransmitPower, &phy_txpower, sizeof(phy_txpower));

        /* (phy_RegulatoryMode) */
        res |= R_NWK_GetRequest(R_NWK_phyRegulatoryMode, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode));
    }

    /* Obtain Wi-SUN PHY configuration parameters */
    if (res == R_RESULT_SUCCESS)
    {
        res |= R_NWK_GetRequest(R_NWK_phyWiSunPhyConfig, &wisun_phy_config, sizeof(wisun_phy_config));
    }

    /* Generate confirm PHY_GETC*/
    R_Modem_print("PHYFAN_GETC %02X %02X ", handle, res);
    if (res == R_RESULT_SUCCESS)
    {
        /* wisun_regulatory_domain */
        R_Modem_print("%02X ", wisun_phy_config.regulatory_domain);

        /* wisun_operating_class */
        R_Modem_print("%02X ", wisun_phy_config.operating_class);

        /* wisun_operating_mode */
        R_Modem_print("%02X ", wisun_phy_config.operating_mode);

        /* phy_txPower */
        R_Modem_print("%02X ", phy_txpower);

        /* phy_RegulatoryMode */
        R_Modem_print("%02X ", phy_RegulatoryMode);
    }
    R_Modem_print("\n");
}


/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_PHYNET_SETR
* Description       : Process "SETR" command - ParamType = ConfigParams
* Arguments         : pCmd ... Pointer to command parameters
*                   :   PhyConfig for Netricity -> tbd
* Return Value      : None
********************************************************************************/
static void AppCmd_ProcessCmd_PHYNET_SETR(uint8_t* pCmd)
{
    UNUSED_SYM(pCmd);
}


/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_PHYNET_GETR
* Description       : Process "GETR" command (Output common message)
* Arguments         : r_result_t ... result when get command
*                   : handle ... index
*                   : paramType ... parameter type
* Return Value      : None
*                   : paramType ... parameter type (=PhyConfig)
* Return Value      : None
* Output            : Confirm command
*                   :   GETC (Handle) (Status) (ParamType)
*                   :     PhyConfig for Netricity -> tbd
********************************************************************************/
static void AppCmd_ProcessCmd_PHYNET_GETR(uint8_t* pCmd)
{
    UNUSED_SYM(pCmd);
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd_SCHEDULE_SETR
* Description       : Process "SETR" command - ParamType = ConfigParams
* Arguments         : pCmd ... Pointer to command parameters
*                   :   ConfigParams: (DeviceType) (ChannelMask) (PANId) (TxPower)
*                   :                 (NetworkName) (RCVIEn)
* Return Value      : None
********************************************************************************/
static void AppCmd_ProcessCmd_UCSCH_SETR(uint8_t* pCmd)
{
    AppCmd_ProcessCmd_SCHEDULE_SETR(pCmd, APP_CMD_ParamType_UnicastSchedule);
}

static void AppCmd_ProcessCmd_BCSCH_SETR(uint8_t* pCmd)
{
    AppCmd_ProcessCmd_SCHEDULE_SETR(pCmd, APP_CMD_ParamType_BroadcastSchedule);
}

static void AppCmd_ProcessCmd_SCHEDULE_SETR(uint8_t* pCmd, r_app_schedule_type_t paramType)
{
    r_result_t res;
    uint8_t handle;
    uint16_t option;

    uint8_t phyCurrentChannel;

    uint8_t i;
    r_ie_wp_broadcast_schedule_t schedule;     // structure to hold the parsed values from the command
    r_app_schedule_buffer_t request = {{ 0 }}; // buffer to hold the serialized schedule that is passed to the NWK layer
    uint8_t bitfield_buffer = 0;               // buffer to temporarily store parsed values for bitfields

    /* Get command parameters */
    /* (Handle) */
    res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);

    /* (option) */
    res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);

    /* Initialize schedule */
    MEMZERO_S(&schedule);
    union
    {
        r_ie_wp_excluded_channel_range_t excluded_ranges[R_SCHEDULE_IE_EXCLUDED_CHANNEL_RANGES_MAX];
        uint8_t excluded_mask[R_SCHEDULE_IE_EXCLUDED_CHANNEL_MASK_MAX_SIZE];
    } channel_exclusions;
    MEMZERO_S(&channel_exclusions);

    schedule.us.excluded_ranges.ranges = &channel_exclusions.excluded_ranges[0];
    schedule.us.excluded_channel_mask = &channel_exclusions.excluded_mask[0];

    /* Get command parameters - ConfigParams */
    if (paramType == APP_CMD_ParamType_BroadcastSchedule)
    {
        /* broadcast_interval */
        res |= AppCmd_HexStrToNum(&pCmd, &schedule.broadcast_interval, sizeof(schedule.broadcast_interval), R_FALSE);

        /* broadcast_schedule_identifier */
        res |= AppCmd_HexStrToNum(&pCmd, &schedule.broadcast_schedule_identifier, sizeof(schedule.broadcast_schedule_identifier), R_FALSE);
    }

    /* Dwell_Interval  */
    res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.dwell_interval, sizeof(schedule.us.dwell_interval), R_FALSE);

    /* Clock_Drift  */
    res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.clock_drift, sizeof(schedule.us.clock_drift), R_FALSE);

    /* Timing_Accuracy  */
    res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.timing_accuracy, sizeof(schedule.us.timing_accuracy), R_FALSE);

    /* ChannelPlan */
    res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
    schedule.us.channel_plan = (r_ie_wp_schedule_channel_plan_t)bitfield_buffer;

    /* ChannelFunction */
    res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
    schedule.us.channel_function = (r_ie_wp_schedule_channel_function_t)bitfield_buffer;

    /* ExcludedChannelControl */
    res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
    schedule.us.excluded_channel_control = (r_ie_wp_schedule_excluded_channel_control_t)bitfield_buffer;

    /* RegulatoryDomain */
    res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
    schedule.us.regulatory_domain = (r_ie_wp_schedule_regulatory_domain_t)bitfield_buffer;

    /* OperatingClass */
    res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.operating_class, sizeof(schedule.us.operating_class), R_FALSE);

    /* CH0 */
    res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.ch0, sizeof(schedule.us.ch0), R_FALSE);

    /* ChannelSpacing */
    res |= AppCmd_HexStrToNum(&pCmd, &bitfield_buffer, sizeof(bitfield_buffer), R_FALSE);
    schedule.us.channel_spacing = (r_ie_wp_schedule_channel_spacing_t)bitfield_buffer;

    /* NumberOfChannels */
    res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.no_of_channels, sizeof(schedule.us.no_of_channels), R_FALSE);

    /* FixedChannel  */
    res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.fixed_channel, sizeof(schedule.us.fixed_channel), R_FALSE);

    if (schedule.us.excluded_channel_control == R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_RANGE)
    {
        /* No_of_excluded_ranges */
        res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.no_of_excluded_ranges, sizeof(schedule.us.no_of_excluded_ranges), R_FALSE);

        for (i = 0; i < schedule.us.no_of_excluded_ranges; i++)
        {
            /* Start Channel Number */
            res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.excluded_ranges.ranges[i].start, sizeof(schedule.us.excluded_ranges.ranges[i].start), R_FALSE);

            /* End Channel Number */
            res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.excluded_ranges.ranges[i].end, sizeof(schedule.us.excluded_ranges.ranges[i].end), R_FALSE);
        }
    }
    else if (schedule.us.excluded_channel_control == R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_MASK)
    {
        /* excluded_channel_mask */
        for (i = 0; i < R_IE_ScheduleNumberOfMaskBytes(&schedule.us); i++)
        {
            res |= AppCmd_HexStrToNum(&pCmd, &schedule.us.excluded_channel_mask[i], sizeof(schedule.us.excluded_channel_mask[i]), R_FALSE);
        }
    }

    if (res == R_RESULT_SUCCESS)
    {
        phyCurrentChannel = (uint8_t)schedule.us.fixed_channel;
        res = R_NWK_SetRequest(R_NWK_phyCurrentChannel, &phyCurrentChannel, sizeof(phyCurrentChannel));
    }

    if (res == R_RESULT_SUCCESS)
    {
        if (paramType == APP_CMD_ParamType_BroadcastSchedule)
        {
            request.schedule.size = R_IE_BroadcastScheduleCreate(&schedule, request.schedule.bytes, sizeof(request) - sizeof(request.schedule), 0);
            if (request.schedule.size > 0)
            {
                res = R_NWK_SetRequest(R_NWK_nwkSchedule_Broadcast, &request, sizeof(request));
            }
            else
            {
                res = (r_result_t)(-request.schedule.size);  // create function returns negative r_result_t on error -> negate again
            }
        }
        else
        {
            request.schedule.size = R_IE_UnicastScheduleCreate(&schedule.us, request.schedule.bytes, sizeof(request) - sizeof(request.schedule), 0);
            if (request.schedule.size > 0)
            {
                res = R_NWK_SetRequest(R_NWK_nwkSchedule_Unicast, &request, sizeof(request));
            }
            else
            {
                res = (r_result_t)(-request.schedule.size);  // create function returns negative r_result_t on error -> negate again
            }
        }
    }

    /* Send confirm */
    if (paramType == APP_CMD_ParamType_BroadcastSchedule)
    {
        R_Modem_print("BCSCH_SETC %02X %02X\n", handle, res);
    }
    else
    {
        R_Modem_print("UCSCH_SETC %02X %02X\n", handle, res);
    }
}

/********************************************************************************
* Function Name     : AppSubGetR_ScheduleConfig
* Description       : Process "GETR" command (Output common message)
* Arguments         : r_result_t ... result when get command
*                   : handle ... index
*                   : paramType ... parameter type
* Return Value      : None
*                   : paramType ... parameter type (=PhyConfig)
* Return Value      : None
* Output            : Confirm command
*                   :   GETC (Handle) (Status) (ParamType) (FreqBandId) (FSKOpeMode) (RegulatoryMode)
*                   :     PhyConfig:
*                   :       (FreqBandId) (FSKOpeMode) (RegulatoryMode)
********************************************************************************/
static void AppCmd_ProcessCmd_UCSCH_GETR(uint8_t* pCmd)
{
    AppCmd_ProcessCmd_SCHEDULE_GETR(pCmd, APP_CMD_ParamType_UnicastSchedule);
}

static void AppCmd_ProcessCmd_BCSCH_GETR(uint8_t* pCmd)
{
    AppCmd_ProcessCmd_SCHEDULE_GETR(pCmd, APP_CMD_ParamType_BroadcastSchedule);
}

static void AppCmd_ProcessCmd_SCHEDULE_GETR(uint8_t* pCmd, uint8_t paramType)
{
    r_ie_wp_broadcast_schedule_t schedule = {0};

    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    uint16_t option;
    res |= AppCmd_HexStrToNum(&pCmd, &option, sizeof(option), R_FALSE);

    if (res == R_RESULT_SUCCESS)
    {
        /* Obtain Schedule Information */
        r_ie_wp_serialized_sched_t* serializedSched = (r_ie_wp_serialized_sched_t*)AppCmdBuf;
        int16_t parseResult = R_RESULT_SUCCESS;
        if (paramType == APP_CMD_ParamType_BroadcastSchedule)
        {
            res = R_NWK_GetRequest(R_NWK_nwkSchedule_Broadcast, AppCmdBuf, sizeof(AppCmdBuf));
            if (res == R_RESULT_SUCCESS)
            {
                parseResult = R_IE_BroadcastScheduleParse(&schedule, serializedSched->bytes, serializedSched->size, 0);
            }
        }
        else
        {
            res = R_NWK_GetRequest(R_NWK_nwkSchedule_Unicast, AppCmdBuf, sizeof(AppCmdBuf));
            if (res == R_RESULT_SUCCESS)
            {
                parseResult = R_IE_UnicastScheduleParse(&schedule.us, serializedSched->bytes, serializedSched->size, 0);
            }
        }
        if (parseResult < 0)
        {
            res = (r_result_t)-parseResult;  // parse function returns negative r_result_t on error -> negate again
        }
    }

    /* Generate confirm */
    if (paramType == APP_CMD_ParamType_BroadcastSchedule)
    {
        /* Send Confirm -> BCSCH_SETC */
        R_Modem_print("BCSCH_GETC %02X %02X ", handle, res);
    }
    else
    {
        /* Send Confirm -> UCSCH_SETC */
        R_Modem_print("UCSCH_GETC %02X %02X ", handle, res);
    }

    if (res == R_RESULT_SUCCESS)
    {
        if (paramType == APP_CMD_ParamType_BroadcastSchedule)
        {
            /* Broadcast Interval */
            R_Modem_print("%04X", (uint16_t)(schedule.broadcast_interval >> 16));
            R_Modem_print("%04X ", (uint16_t)(schedule.broadcast_interval));

            /* Broadcast Schedule Identifier */
            R_Modem_print("%04X ", schedule.broadcast_schedule_identifier);
        }

        /* dwell_interval */
        R_Modem_print("%02X ", schedule.us.dwell_interval);

        /* clock_drift */
        R_Modem_print("%02X ", schedule.us.clock_drift);

        /* timing_accuracy */
        R_Modem_print("%02X ", schedule.us.timing_accuracy);

        /* channel_plan */
        R_Modem_print("%02X ", schedule.us.channel_plan);

        /* channel_function */
        R_Modem_print("%02X ", schedule.us.channel_function);

        /* excluded_channel_control */
        R_Modem_print("%02X ", schedule.us.excluded_channel_control);

        /* regulatory_domain */
        R_Modem_print("%02X ", schedule.us.regulatory_domain);

        /* operating_class */
        R_Modem_print("%02X ", schedule.us.operating_class);

        /* ch0 */
        R_Modem_print("%04X", (uint16_t)(schedule.us.ch0 >> 16));
        R_Modem_print("%04X ", (uint16_t)(schedule.us.ch0));

        /* channel_spacing */
        R_Modem_print("%02X ", schedule.us.channel_spacing);

        /* no_of_channels */
        R_Modem_print("%02X ", schedule.us.no_of_channels);

        /* fixed_channel */
        R_Modem_print("%02X ", schedule.us.fixed_channel);

        if (schedule.us.excluded_channel_control == R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_RANGE)
        {
            /* no_of_excluded_ranges */
            R_Modem_print("%02X ", schedule.us.no_of_excluded_ranges);

            for (uint8_t i = 0; i < schedule.us.no_of_excluded_ranges; i++)
            {
                /* Start Channel Number */
                R_Modem_print("%02X ", schedule.us.excluded_ranges.ranges[i].start);

                /* End Channel Number */
                R_Modem_print("%02X ", schedule.us.excluded_ranges.ranges[i].end);
            }
        }
        else if (schedule.us.excluded_channel_control == R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_MASK)
        {
            /* excluded_channel_mask */
            for (uint8_t i = 0; i < R_IE_ScheduleNumberOfMaskBytes(&schedule.us); i++)
            {
                R_Modem_print("%02X ", schedule.us.excluded_channel_mask[i]);
            }
        }
    }
    R_Modem_print("\n");
}

#ifdef UMM_INFO
static void AppCmd_ProcessCmd_MEMMARK_GETR(uint8_t* pCmd)
{
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    R_Modem_print("MEMMARK_GETC %02X %02X ", handle, res);

    for (int heap_id = 0; heap_id < R_HEAP_COUNT; heap_id++)
    {
        R_Modem_print("%02X ", heap_id);
        R_Modem_print("%08X %08X ", umm_high_watermark(UMM_HEAP_ID_A), umm_heap_size(UMM_HEAP_ID_A));
    }
    R_Modem_print("\n");
}

static void AppCmd_ProcessCmd_MEMINFO_GETR(uint8_t* pCmd)
{
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    R_Modem_print("MEMINFO_GETC %02X %02X ", handle, res);

    for (int heap_id = 0; heap_id < R_HEAP_COUNT; heap_id++)
    {
        R_Modem_print("%02X ", heap_id);
        umm_info(UMM_HEAP_ID_A_ NULL, 0);
        R_Modem_print("%04X %04X %04X ", ummHeapInfo.totalEntries, ummHeapInfo.usedEntries, ummHeapInfo.freeEntries);
        R_Modem_print("%04X %04X %04X ", ummHeapInfo.totalBlocks, ummHeapInfo.usedBlocks, ummHeapInfo.freeBlocks);
        R_Modem_print("%04X %04X ", ummHeapInfo.maxFreeContiguousBlocks, ummHeapInfo.maxAllocatedContiguousBlocks);
    }
    R_Modem_print("\n");
}

static void AppCmd_ProcessCmd_MEMMAP_GETR(uint8_t* pCmd)
{
    uint8_t handle;
    r_result_t res = AppCmd_HexStrToNum(&pCmd, &handle, sizeof(handle), R_FALSE);
    R_Modem_print("MEMMAP_GETC %02X %02X ", handle, res);

    for (int heap_id = 0; heap_id < R_HEAP_COUNT; heap_id++)
    {
        R_Modem_print("%02X ", heap_id);

        umm_info(UMM_HEAP_ID_A_ NULL, 0);
        unsigned short int blockNo = 0;
        unsigned short int nextBlockNo = 0;
        int freeFlag = 0;
        size_t blockSize = 0;

        do
        {
            nextBlockNo = umm_map(UMM_HEAP_ID_A_ blockNo, &freeFlag, &blockSize);
            R_Modem_print("%04X %04X %08X %02X ", blockNo, nextBlockNo, blockSize, freeFlag);
            blockNo = nextBlockNo;
        }
        while (blockNo != 0);
    }
    R_Modem_print("\n");
}
#endif // UMM_INFO

/********************************************************************************
* Function Name     : AppCmd_HexStrToNum
* Description       : Convert hex format string to number
* Arguments         : ppBuf ... Double pointer buffer
*                   : pData ... Pointer to data
*                   : size ... Size of string
*                   : isOctetStr ...
*                   :   R_TRUE: Octet string
*                   :   R_TRUE: Big endian format
* Return Value      : r_result_t
********************************************************************************/
static r_result_t AppCmd_HexStrToNum(uint8_t** ppBuf, void* pData, int16_t dataSize, r_boolean_t isOctetStr)
{
    unsigned char ret;
    r_result_t res;

    ret = AppHexStrToNum(ppBuf,  pData, dataSize, isOctetStr);
    if (0 == ret)
    {
        // TRUE
        res = R_RESULT_SUCCESS;
    }
    else
    {
        // FALSE
        res = R_RESULT_INVALID_PARAMETER;
    }

    return res;
}

/********************************************************************************
* Function Name     : AppPrintBytesAsHexString
* Description       : Display bytes as hexadecimal string
* Arguments         : const uint8_t *bytes ... Pointer to bytes
*                   : size_t numBytes ... number of bytes to print
* Return Value      : None
********************************************************************************/
static void AppPrintBytesAsHexString(const uint8_t* bytes, size_t numBytes)
{
    for (size_t i = 0; i < numBytes; i++)
    {
        R_Modem_print("%02X", bytes[i]);
    }
}
/********************************************************************************
* Function Name     : AppPrintIPv6Addr
* Description       : Display IPv6 address
* Arguments         : const uint8_t *pIPAddr ... Pointer to IPv6 address
* Return Value      : None
********************************************************************************/
static void AppPrintIPv6Addr(const uint8_t* pIPAddr)
{
    AppPrintBytesAsHexString(pIPAddr, R_IPV6_ADDRESS_LENGTH);
}

/********************************************************************************
* Function Name     : AppPrintMACAddr
* Description       : Display MAC address
* Arguments         : const uint8_t *pMACAddr ... Pointer to MAC address
* Return Value      : None
********************************************************************************/
static void AppPrintMACAddr(const uint8_t* pMACAddr)
{
    AppPrintBytesAsHexString(pMACAddr, R_MAC_EXTENDED_ADDRESS_LENGTH);
}

/********************************************************************************
* Function Name     : AppPrintNetworkName
* Description       : Display Network Name address
* Arguments         : const uint8_t *pNetworkName ... Pointer to Network Name
* Return Value      : None
********************************************************************************/
static void AppPrintNetworkName(const char* pNetworkName)
{
    while (*pNetworkName)
    {
        R_Modem_print("%02X", *pNetworkName++);
    }
}

/* For Network Rack testing reply original UDP packet
 * back to it's source within random time between 0-15 seconds */

/********************************************************************************
* Function Name     : AppPrintUdpDataInd
* Description       : Display udp_ind message
* Arguments         : r_udp_data_ind_t *p_udp_ind ... Pointer to message
* Return Value      : None
********************************************************************************/
static void AppPrintUdpDataInd(r_udp_data_ind_t* p_udp_ind)
{
    /* (Protocol) */
    R_Modem_print("%04X ", R_IPV6_PROTOCOL_ID_UDP);

    /* (DstIPv6Addr) */
    AppPrintIPv6Addr(p_udp_ind->dstAddress);
    R_Modem_print(" ");

    /* (SrcIPv6Addr) */
    AppPrintIPv6Addr(p_udp_ind->srcAddress);
    R_Modem_print(" ");

    /* (DstPort),(SrcPort) */
    R_Modem_print("%04X %04X ", p_udp_ind->dstPort, p_udp_ind->srcPort);

    /* (PayloadSize),(Payload) */
    R_Modem_print("%04X ", p_udp_ind->dataLength);
    AppPrintBytesAsHexString(p_udp_ind->data, p_udp_ind->dataLength);

    /* (Status), (LQI) */
    R_Modem_print(" %04X %02X\n", p_udp_ind->status, p_udp_ind->lqi);

#ifdef R_NETWORK_RACK
    /* For Network Rack testing reply original UDP packet
     * back to it's source within random time between 0-15 seconds */
    clock_time_t delay = (random_rand() % R_RAND_MAX_VAL) * CLOCK_SECOND;
    R_Modem_print("TEST: Sending UDP reply in %lu ticks\n", delay);
    R_OS_DelayTaskMs(delay);

    R_UDP_DataRequest(p_udp_ind->srcAddress,
                      p_udp_ind->srcPort,
                      p_udp_ind->dstPort,
                      p_udp_ind->data,
                      p_udp_ind->dataLength, R_OPTIONS_NON_BLOCKING);
#endif
}

/********************************************************************************
* Function Name     : AppPrintIcmpEchoInd
* Description       : Display echo_ind message
* Arguments         : r_icmp_echo_reply_ind_t *p_icmp_echo_ind ... Pointer to message
* Return Value      : None
********************************************************************************/
static void AppPrintIcmpEchoInd(r_icmp_echo_reply_ind_t* p_icmp_echo_ind)
{
    /* (Protocol) */
    R_Modem_print("%04X ", R_IPV6_PROTOCOL_ID_ICMPV6);

    /* (DstIPv6Addr) */
    AppPrintIPv6Addr(p_icmp_echo_ind->dstAddress);
    R_Modem_print(" ");

    /* (SrcIPv6Addr) */
    AppPrintIPv6Addr(p_icmp_echo_ind->srcAddress);
    R_Modem_print(" ");

    /* (Type), (Identifier), (Sequence), PayloadSize */
    R_Modem_print("%02X %04X %04X ",
                  R_ICMP_TYPE_ECHO_REPLY,
                  p_icmp_echo_ind->identifier, p_icmp_echo_ind->sequence);

    /* (PayloadSize), (Payload) */
    R_Modem_print("%04X ", p_icmp_echo_ind->dataLength);
    AppPrintBytesAsHexString(p_icmp_echo_ind->data, p_icmp_echo_ind->dataLength);

    /* (Status), (LQI) */
    R_Modem_print(" %04X %02X\n", p_icmp_echo_ind->status, p_icmp_echo_ind->lqi);
}

#if R_DEV_TBU_ENABLED && __RX
/********************************************************************************
* Function Name     : AppPrintSubScriptFrameInd
* Description       : Display subscription frame
* Arguments         : r_subscription_frame_ind_t *p_subscript_frame_ind ... Pointer to frame
* Return Value      : None
********************************************************************************/
static void AppPrintSubScriptFrameInd(const r_subscription_frame_ind_t* p_subscript_frame_ind)
{
    R_Modem_print("%04X ", p_subscript_frame_ind->frameLength);
    AppPrintBytesAsHexString(p_subscript_frame_ind->frame, p_subscript_frame_ind->frameLength);
    R_Modem_print("\n");
}
#endif /* R_DEV_TBU_ENABLED && __RX */

/********************************************************************************
* Function Name     : AppPrintIpDataIndication
* Description       : Display IP packet
* Arguments         : r_ip_data_ind_t *p_ip_ind ... Pointer to IP indication
* Return Value      : None
********************************************************************************/
static void AppPrintIpDataIndication(const r_ip_data_ind_t* p_indication)
{
    R_Modem_print("%04X ", p_indication->packetLength);
    AppPrintBytesAsHexString(p_indication->packet, p_indication->packetLength);
    R_Modem_print("\n");
}

/** This is just a placeholder; A valid Vendor-specific enterprise number (registered with IANA) must be used! */
#define DHCPV6_VENDOR_OPT_EXAMPLE_ENTERPRISE_NUMBER    0xDEADBEEF
#define DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FEATURES 0xACDC
#define DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FILLER   0xCAFE

uint16_t R_DHCPV6_VendorOptionSendCallback(uint32_t* enterprise_number, uint8_t* option_data, uint16_t max_option_data_size)
{
    *enterprise_number = DHCPV6_VENDOR_OPT_EXAMPLE_ENTERPRISE_NUMBER;

    /*
     * The encapsulated vendor-specific options field MUST be encoded as a sequence of code/length/value fields of
     * identical format to the DHCP options field.
     */
    uint16_t pos = 0;

    /* Write 2-byte option containing our feature mask */
    UInt16ToArr(DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FEATURES, &option_data[pos]); // option code
    pos += 2;
    UInt16ToArr(2, &option_data[pos]);                                              // option length
    pos += 2;
    UInt16ToArr(featureMask, &option_data[pos]);                                    // option data
    pos += 2;

    /* Write another (useless) option with all bytes set to 0xFF to demonstrate usage of max supported option size */
    UInt16ToArr(DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FILLER, &option_data[pos]);  // option code
    pos += 2;
    uint16_t remainingSize = max_option_data_size - pos - 2;
    UInt16ToArr(remainingSize, &option_data[pos]);  // option length
    pos += 2;
    while (pos < max_option_data_size)
    {
        option_data[pos++] = 0xFF;  // option data
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_3692(pos);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    return pos;
}

void R_DHCPV6_VendorOptionRecvCallback(const r_eui64_t* src, uint32_t enterprise_number,
                                       const uint8_t* option_data, uint16_t option_len)
{
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_3699(option_len, src->bytes);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    /* Validate enterprise number */
    if (enterprise_number != DHCPV6_VENDOR_OPT_EXAMPLE_ENTERPRISE_NUMBER)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_3704(enterprise_number);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return;
    }

    /* Process option */
    uint16_t pos = 0;
    while ((option_len - pos) >= 4)
    {
        uint16_t code = ArrToUInt16(&option_data[pos]);
        pos += 2;
        uint16_t current_option_len = ArrToUInt16(&option_data[pos]);
        pos += 2;
        if (current_option_len > (option_len - pos))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
            r_loggen_3718(code, option_len - pos, current_option_len);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
            pos = option_len;
            break;
        }
        switch (code)
        {
            case DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FEATURES:
            {
                if (current_option_len == 2)
                {
                    uint16_t remoteFeatureMask = ArrToUInt16(&option_data[pos]);
                    LOG_ONLY_VAR(remoteFeatureMask);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                    r_loggen_3730(remoteFeatureMask);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
                }
                else
                {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
                    r_loggen_3734();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
                }
                pos += current_option_len;
                break;
            }

            case DHCPV6_VENDOR_OPT_EXAMPLE_OPTION_TYPE_FILLER:
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                r_loggen_3742(current_option_len);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
                pos += current_option_len;
                break;
            }

            default:
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
                r_loggen_3749(code);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
                break;
            }
        }
    }
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_3754(pos, src->bytes);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
}

#ifdef R_DEV_AUTO_START
/********************************************************************************
* Function Name     : AppCmd_AutoStart
* Description       : Automatic device configuration and device start
* Arguments         : None
* Return Value      : None
********************************************************************************/
void AppCmd_AutoStart(void)
{
    r_result_t res = R_RESULT_SUCCESS;
    r_app_config_t* p_config = &AppCmdConfig;
    r_ie_wp_broadcast_schedule_t schedule = {0};
    r_app_schedule_buffer_t sched_buf = {{ 0 }};

    uint8_t rf_phyFreqBandId;
    uint8_t rf_phyFSKOpeMode;
    uint8_t rf_phyChanConv;
    uint16_t rf_phyCcaDuration;
    uint8_t phyCurrentChannel;

    uint8_t handle = 0;
    uint8_t macAddr[R_MAC_EXTENDED_ADDRESS_LENGTH];
    uint8_t cnt;


    /***************************************************************************
    *
    * Start: Change desired AutoStart device configuration from here
    *
    ***************************************************************************/

    /***************************************************************************
    * Device Configuration
    ***************************************************************************/
    uint8_t networkName[] = "WiSUN PAN";
    uint8_t deviceType = R_ROUTERNODE;
    uint16_t panId = 0xffff;
    uint16_t panSize = 0;
    uint16_t sixLowPanMTU = 1280u;
    uint8_t useParent_BS_IE = 1;
    uint8_t demo_mode = 0;

    /***************************************************************************
    * PHY Configuration
    ***************************************************************************/
    uint8_t phy_txPower = 47u;
    uint8_t phy_RegulatoryMode = 0;
    uint8_t phy_AntennaSelectTx = 1;  //  0 = TX and RX via connector J2; 1 = TX and RX via connector;

#if R_DEV_FREQUENCY_HOPPING

    uint8_t wisun_regulatory_domain = R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_NORTH_AMERICA;
    uint8_t wisun_operating_class = R_OPERATING_CLASS_2;
    uint8_t wisun_operating_mode = R_OPERATING_MODE_3;

    /***************************************************************************
    * Unicast and Broadcast Schedule Configuration
    ***************************************************************************/
    schedule.broadcast_interval = 1020u;
    schedule.broadcast_schedule_identifier = 1u;

    schedule.us.dwell_interval = 255u;
    schedule.us.clock_drift = 255u;
    schedule.us.timing_accuracy = 0u;
    schedule.us.channel_plan = R_IE_WP_SCHEDULE_CHANNEL_PLAN_INDIRECT;
    schedule.us.channel_function = R_IE_WP_SCHEDULE_CHANNEL_FUNCTION_DIRECT_HASH;
    schedule.us.excluded_channel_control = R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_NONE;
    schedule.us.regulatory_domain = wisun_regulatory_domain;
    schedule.us.operating_class = wisun_operating_class;
    schedule.us.ch0 = 902400u;
    schedule.us.channel_spacing = R_IE_WP_SCHEDULE_CHANNEL_SPACING_400;
    schedule.us.no_of_channels = 64u;
    schedule.us.fixed_channel = 0u;

#else /* R_DEV_FREQUENCY_HOPPING */

    uint8_t wisun_regulatory_domain = R_IE_WP_SCHEDULE_REGULATORY_DOMAIN_NORTH_AMERICA;
    uint8_t wisun_operating_class = R_OPERATING_CLASS_1;
    uint8_t wisun_operating_mode = R_OPERATING_MODE_1b;

    /***************************************************************************
    * Unicast and Broadcast Schedule Configuration
    ***************************************************************************/
    schedule.broadcast_interval = 1020u;
    schedule.broadcast_schedule_identifier = 1u;

    schedule.us.dwell_interval = 255u;
    schedule.us.clock_drift = 255u;
    schedule.us.timing_accuracy = 0u;
    schedule.us.channel_plan = R_IE_WP_SCHEDULE_CHANNEL_PLAN_INDIRECT;
    schedule.us.channel_function = R_IE_WP_SCHEDULE_CHANNEL_FUNCTION_FIXED;
    schedule.us.excluded_channel_control = R_IE_WP_SCHEDULE_EXCLUDED_CHANNEL_CONTROL_NONE;
    schedule.us.regulatory_domain = wisun_regulatory_domain;
    schedule.us.operating_class = wisun_operating_class;
    schedule.us.ch0 = 902200u;
    schedule.us.channel_spacing = R_IE_WP_SCHEDULE_CHANNEL_SPACING_200;
    schedule.us.no_of_channels = 129u;
    schedule.us.fixed_channel = 0u;

#endif /* R_DEV_FREQUENCY_HOPPING */

    /***************************************************************************
    *
    * End: Change desired AutoStart device configuration from here
    *
    ***************************************************************************/

    /***************************************************************************
    * Set Device Configuration
    ***************************************************************************/
    p_config->deviceType = deviceType;
    p_config->panId = panId;
    p_config->panSize = panSize;
    p_config->useParent_BS_IE = useParent_BS_IE;

    /* set global Network Name used for Network Name Information Element (NETNAME-IE) */
    memset(p_config->networkName, 0x00, sizeof(p_config->networkName));
    memcpy(p_config->networkName, networkName, sizeof(p_config->networkName));
    p_config->networkName[sizeof(p_config->networkName) - 1] = 0;  // Ensure null-termination

    /* Set operation mode (demo mode)*/
    res = R_NWK_SetRequest(R_NWK_rplDemoMode, &demo_mode, sizeof(demo_mode));

    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_SetRequest(R_NWK_sixLowpanMtu, &sixLowPanMTU, sizeof(sixLowPanMTU));
    }
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_SetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
    }

    /***************************************************************************
     * Set PHY Configuration
    ***************************************************************************/

    /* Apply PHY configuration according to the Wi-SUN PHY specification */
    if (res == R_RESULT_SUCCESS)
    {
        r_nwk_wisun_phy_config_t wisun_phy_config;

        wisun_phy_config.regulatory_domain = wisun_regulatory_domain;
        wisun_phy_config.operating_class = wisun_operating_class;
        wisun_phy_config.operating_mode = wisun_operating_mode;
        res |= R_NWK_SetRequest(R_NWK_phyWiSunPhyConfig, &wisun_phy_config, sizeof(wisun_phy_config));
    }

    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_SetRequest(R_NWK_phyTransmitPower, &phy_txPower, sizeof(phy_txPower));
    }
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_SetRequest(R_NWK_phyRegulatoryMode, &phy_RegulatoryMode, sizeof(phy_RegulatoryMode));
    }
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_SetRequest(R_NWK_phyAntennaSelectTx, &phy_AntennaSelectTx, sizeof(phy_AntennaSelectTx));
    }
    if (res == R_RESULT_SUCCESS)
    {
        uint16_t phy_CcaVth = 0x01AA;  // set to -86 dBm as default, depending on the region (regulatory domain) and
                                       // the data rate (operating mode) optimization of the CCA level threshold is recommended.
        res = R_NWK_SetRequest(R_NWK_phyCcaVth, &phy_CcaVth, sizeof(phy_CcaVth));
    }

    /***************************************************************************
    * Set Unicast and Broadcast Schedule Configuration
    ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        phyCurrentChannel = (uint8_t)schedule.us.fixed_channel;
        res = R_NWK_SetRequest(R_NWK_phyCurrentChannel, &phyCurrentChannel, sizeof(phyCurrentChannel));
    }

    if (res == R_RESULT_SUCCESS)
    {
        sched_buf.schedule.size = R_IE_UnicastScheduleCreate(&schedule.us, sched_buf.schedule.bytes, sizeof(sched_buf) - sizeof(sched_buf.schedule), 0);
        if (sched_buf.schedule.size > 0)
        {
            res = R_NWK_SetRequest(R_NWK_nwkSchedule_Unicast, &sched_buf, sizeof(sched_buf));
        }
        else
        {
            res = (r_result_t)(-sched_buf.schedule.size);  // create function returns negative r_result_t on error -> negate again
        }
    }

    if (res == R_RESULT_SUCCESS)
    {
        sched_buf.schedule.size = R_IE_BroadcastScheduleCreate(&schedule, sched_buf.schedule.bytes, sizeof(sched_buf) - sizeof(sched_buf.schedule), 0);
        if (sched_buf.schedule.size > 0)
        {
            res = R_NWK_SetRequest(R_NWK_nwkSchedule_Broadcast, &sched_buf, sizeof(sched_buf));
        }
        else
        {
            res = (r_result_t)(-sched_buf.schedule.size);  // create function returns negative r_result_t on error -> negate again
        }
    }

    /***************************************************************************
    * Set WhiteList Configuration
    ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        r_nwk_mac_whitelist_t empty_whitelist = {0};
        res = R_NWK_SetRequest(R_NWK_macWhitelist, &empty_whitelist, sizeof(empty_whitelist));
    }

#if R_DLMS_UA_CTT
    /***************************************************************************
    * Enable IP indications for incoming UDP messages (for external forwarding)
    ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        r_boolean_t enabled = R_TRUE;
        res = R_NWK_SetRequest(R_NWK_udpIpIndication, &enabled, sizeof(enabled));
    }
#endif

    /***************************************************************************
    * Enable Frame Counter Checking
    ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        uint8_t macFrameCounterCheckEnabled = R_TRUE;
        res = R_NWK_SetRequest(R_NWK_macFrameCounterCheckEnabled, &macFrameCounterCheckEnabled, sizeof(macFrameCounterCheckEnabled));
    }

    /***************************************************************************
    * Adjust PAN timeout to 90 minutes: Since DAO/DAO-ACK are sent approximately
    * every 60 min, PAN timeout should never occur even without application data
    * traffic
    ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        uint8_t panTimeout = 90;
        res = R_NWK_SetRequest(R_NWK_panTimeout, &panTimeout, sizeof(panTimeout));
    }

    /***************************************************************************
    * Authentication Join Time Optimization:
    *
    * For "smaller networks" where the overall join time is critical, please
    * enable the define "R_DEV_AUTH_JOIN_TIME_OPT". By enabling this define, the
    * stack makes use of optimized timing settings speeding up authentication.
    *
    * For "larger networks" please do not change the default authentication
    * timing settings.
    ***************************************************************************/
#if R_DEV_AUTH_JOIN_TIME_OPT

    /***************************************************************************
    * Adjust initial retransmission time of unsuccessful authentication
    * to 2 minutes to speed up authentication time.
    ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        uint16_t authJoinTimerBase = 2 * 60;
        res = R_NWK_SetRequest(R_NWK_authJoinTimerBase, &authJoinTimerBase, sizeof(authJoinTimerBase));
    }

    /***************************************************************************
    * Adjust backoff factor of authentication retransmissions
    * to 1 to speed up authentication time.
    ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        uint8_t authJoinTimerBackoffFactor = 1;
        res = R_NWK_SetRequest(R_NWK_authJoinTimerBackoffFactor, &authJoinTimerBackoffFactor, sizeof(authJoinTimerBackoffFactor));
    }

    /***************************************************************************
     * Reduce maximum value for authentication join timer to 10 minutes:
     * The node falls back to join state 1 after two failing authentication
     * attempt, which avoids retrying over an EAPOL target with bad connectivity.
     * This value needs to be increased for larger networks.
     ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        uint16_t authJoinTimerMax = (10 * 60);
        res = R_NWK_SetRequest(R_NWK_authJoinTimerMax, &authJoinTimerMax, sizeof(authJoinTimerMax));
    }

#endif /* R_DEV_AUTH_JOIN_TIME_OPT */

    /***************************************************************************
    * Enable verbose logging, severity level = error messages
    ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        uint8_t verbosity = R_LOG_SEVERITY_ERR;
        R_LOG_SetSeverityThreshold(verbosity);
    }

    /***************************************************************************
    * Start the Device in Router Node Configuration
    ***************************************************************************/
    if (res == R_RESULT_SUCCESS)
    {
        if (deviceType == R_ROUTERNODE)
        {
            res = R_NWK_JoinRequest(p_config->networkName, 0);
        }
#if R_BORDER_ROUTER_ENABLED
        else if (p_config->deviceType == R_BORDERROUTER)
        {
#if R_DHCPV6_SERVER_ENABLED
            R_DHCPV6_ServerInit(pAppGetMacAddr(), &R_DHCPV6_ServerLookupAssignPrefix, p_config->globalIpAddress.bytes);
#endif
            res = R_NWK_StartRequest(p_config->panId,
                                     p_config->panSize,
                                     p_config->useParent_BS_IE,
                                     p_config->networkName,
                                     &p_config->globalIpAddress,
                                     0);
        }
#endif /* R_BORDER_ROUTER_ENABLED */
        else
        {
            res = R_RESULT_INVALID_PARAMETER;  // Invalid device type
        }
    }

    // get current Device Type
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_GetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
    }
    // get current MAC address
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_GetRequest(R_NWK_macExtendedAddress, macAddr, sizeof(macAddr));
    }
    // get current Pan ID
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_GetRequest(R_NWK_macPANId, &panId, sizeof(panId));
    }

    /* Confirm device start */
    R_Modem_print("STARTC %02X %02X ", handle, res);
    if (res == R_RESULT_SUCCESS)
    {
        /* DeviceType */
        R_Modem_print("%02X ", deviceType);

        /* PANId */
        R_Modem_print("%04X ", panId);

        /* NetworkName */
        AppPrintNetworkName(p_config->networkName);
        R_Modem_print(" ");

        /* MACAddr */
        R_Swap64((uint8_t*)(&macAddr[0]), (uint8_t*)(&macAddr[0]));
        AppPrintMACAddr(macAddr);
    }
    R_Modem_print("\n");
}
#endif /* R_DEV_AUTO_START */
