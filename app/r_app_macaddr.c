/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * file name   : r_app_macaddr.c
 * version     : 1.0
 * description : Wi-SUN MAC Protocol stack Sample application program
 ******************************************************************************/

/******************************************************************************
   Private global variables
 ******************************************************************************/

/******************************************************************************
 * MAC Address Details of Renesas Electronics Corporation
 * OUI:    74:90:50 ( 24-bit Organizationally Unique Identifier )
 * Range:  74:90:50:00:00:00 - 74:90:50:FF:FF:FF
 * Type:   IEEE MA-L
 *****************************************************************************/

#include "r_stdint.h"

#if defined(__RX)

    #pragma address g_AppMacAddress = 0xFFF00000 /* Code FLASH Address */
const                                            /* const (and in flash) on RX based nodes, but must not be const on PC */

#elif defined(__ICCRL78__)

    #pragma location=0x7F000 /* Code FLASH Address */
const                        /* const (and in flash) on RL78 based nodes */

#endif /* defined(__RX) */

uint8_t g_AppMacAddress[8] = {0x74, 0x90, 0x50, 0x00, 0x00, 0x00, 0x00, 0x00};

/***************************************************************************
 * name         : pAppGetMacAddr
 * parameters   : none
 * returns      : none
 * description  : returns MacAddress
 **************************************************************************/
const uint8_t* pAppGetMacAddr(void)
{
    return &g_AppMacAddress[0];
}

/*******************************************************************************
 * Copyright (C) 2015 Renesas Electronics Corporation. All rights reserved.
 ******************************************************************************/
