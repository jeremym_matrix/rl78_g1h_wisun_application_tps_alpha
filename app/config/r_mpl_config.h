#ifndef R_MPL_CONFIG_H
#define R_MPL_CONFIG_H

#include "r_nwk_inter_config.h"

/******************************************************************************
* Instance Parameters
******************************************************************************/
/*! \name MPL Instance Parameters
 * Configure the MPL instance with respect to memory requirements.
 * *
 * On 32-Bit platforms, the memory requirements are as follows
 * - 20 bytes per subscription
 * - 32 bytes per seed
 * - 44 bytes per buffered message in addition to the buffered IPv6 packet
 * Note: aside from the subscriptions, all information is stored on the heap
 * However, to allow managing memory in shared heap environments, the MAX
 * limits defined below are still enforced.
 * @{
 */
#ifndef R_MPL_MAX_SUBSCRIPTIONS
#define R_MPL_MAX_SUBSCRIPTIONS 1
#endif

#ifndef R_MPL_MAX_SEEDS
#define R_MPL_MAX_SEEDS (R_MPL_MAX_SUBSCRIPTIONS * 3)
#endif

#ifndef R_MPL_MAX_MESSAGES
#define R_MPL_MAX_MESSAGES (R_MPL_MAX_SEEDS * 3)
#endif

#ifndef R_MPL_MEM_CAPACITY
//! The amount of dynamic memory that must be available for the buffered packets
#define R_MPL_MEM_CAPACITY (R_MPL_MAX_MESSAGES * (1280 + 52) + R_MPL_MAX_SEEDS * 40)
#endif

#if R_MPL_MAX_SUBSCRIPTIONS < 1
#error R_MPL_MAX_SUBSCRIPTIONS must be >= 1 (cf. 20130125-FANWG-FANTPS-1v00S6.2.3.1.2.2)
#endif
//! @}

#endif /* R_MPL_CONFIG_H */
