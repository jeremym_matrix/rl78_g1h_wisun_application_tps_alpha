/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer*
* Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/

/*******************************************************************************
 * File Name   : r_table_config.h
 * Version     : 1.0
 * Description : This module contains configuration defines for FAN functions
 ******************************************************************************/
#ifndef _R_TABLE_CONFIG_H
#define _R_TABLE_CONFIG_H

#if R_BR_AUTHENTICATOR_ENABLED
#include "r_auth_config.h"
#endif
#include "r_auth_types.h"
#include "mac_neighbors.h"
#ifndef R_HYBRID_PLC_RF
#include "r_nd.h"
#include "r_nd_cache.h"
#endif /* R_HYBRID_PLC_RF */

/* MAC Neighbor table */
extern r_mac_neighbor_t g_neighbors[];
extern const uint16_t r_mac_neighbor_table_size_glb;

/* Neighbor cache */
#ifndef R_HYBRID_PLC_RF
extern r_nd_neighbor_cache_entry_t g_nd_rpl_cache[];
extern r_nd_addr_reg_entry_t g_nd_aro_cache[];
extern const uint16_t g_nd_rpl_cache_length;
extern const uint16_t g_nd_aro_cache_length;

#if R_BR_AUTHENTICATOR_ENABLED

/* Authentication supplicant table */
extern r_auth_br_supplicant_t g_supplicants[];
extern const uint16_t r_auth_br_supplicants_size_glb;
#endif /* R_BR_AUTHENTICATOR_ENABLED */

/**
/ extern the pointer of routing table struct g_sourceroutememb
*/
extern struct memb* g_sourceroutememb;
#endif /* R_HYBRID_PLC_RF */

#endif /* _R_TABLE_CONFIG_H */
