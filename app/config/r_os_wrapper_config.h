/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/**
   @file
   @version   1.00
   @brief     OS abstractions configuration file
   @attention The definitions must adhere to the following rules:
              - All ID ranges must increase sequentially
              - All 'list' entries must be in the correct order according to their IDs
              - Except for heaps, ID ranges must start with 0
              - Heap IDs must start with R_OS_HEAP_BASEID
              - R_OS_HEAP_BASEID must be greater than 0
              - The #include GUARD must not be changed
 */

#ifndef R_OS_WRAPPER_CONFIG_H
#define R_OS_WRAPPER_CONFIG_H

#include "r_os_wrapper_def.h"
#include "r_auth_config.h"
#include "r_mpl_config.h"

/******************************************************************************
* Memory Capacities
******************************************************************************/

/* Base heap sizes according to previously used fixed-size memory pools */
#define R_HEAP_SIZE_MAC_IND_BASE 1624
#define R_HEAP_SIZE_NWK_REQ_BASE 1624
#define R_HEAP_SIZE_NWK_IND_BASE 1624
#define R_HEAP_SIZE_APL_REQ_BASE 1624

#if R_DEV_TBU_ENABLED || R_BORDER_ROUTER_ENABLED
    #define R_HEAP_SIZE_NWK_IND  (8 * R_HEAP_SIZE_NWK_IND_BASE)
    #define R_HEAP_SIZE_MAC_IND  (16 * R_HEAP_SIZE_MAC_IND_BASE)
#else
    #define R_HEAP_SIZE_NWK_IND  (4 * R_HEAP_SIZE_NWK_IND_BASE)
    #define R_HEAP_SIZE_MAC_IND  (6 * R_HEAP_SIZE_MAC_IND_BASE)
#endif

#if defined(__RL78__)
    #define R_HEAP_SIZE_NWK_REQ     (2 * R_HEAP_SIZE_NWK_REQ_BASE)
#else
    #define R_HEAP_SIZE_NWK_REQ     (4 * R_HEAP_SIZE_NWK_REQ_BASE)
#endif

#define R_HEAP_SIZE_APL_REQ         (1 * R_HEAP_SIZE_APL_REQ_BASE) //!< Requests/Confirms APL <-> NWK

#if defined(__x86_64)
    #define R_HEAP_SIZE_WARN_IND    64
    #define R_HEAP_SIZE_ERR_IND     64
    #define R_HEAP_SIZE_CYCTICK_NTF 112
#else
    #define R_HEAP_SIZE_WARN_IND    48
    #define R_HEAP_SIZE_ERR_IND     48
    #define R_HEAP_SIZE_CYCTICK_NTF 80
#endif

#if R_MINIMAL_ROUTER_NODE
    #if defined(__RL78__)
// Note: Almost 17kB of shared NWK heap memory are required during authentication with max certificate chain
        #define R_HEAP_SIZE_NWK_SHARED (17 * 1024)
    #else
        #define R_HEAP_SIZE_NWK_SHARED (14 * 1024 + R_AUTH_MEM_CAPACITY + R_MPL_MEM_CAPACITY)
    #endif
#elif R_MODEM_SERVER
    #define R_HEAP_SIZE_NWK_SHARED     (15 * 1024)
#else
    #define R_HEAP_SIZE_NWK_SHARED     (20 * 1024 + R_FRAG_MAX_CONCURRENT_RX_PACKETS * (R_MAX_MTU_SIZE + 16) + R_MPL_MEM_CAPACITY)
#endif

/*
 * Heap size configuration for unicast and broadcast schedules (our own and those of our neighbors) on MAC layer
 *
 * Example values for PHY profile "North America 50 kb/s (902-928 MHz, 200 kHz, 129 channels)":
 * - Fixed channel mode requires ~200 bytes without channel exclusions
 * - Frequency hopping mode requires ~700 bytes if all devices use the same unicast schedule without channel exclusions
 * - In frequency hopping mode, the memory requirements increase if neighbors use different unicast schedules since
 * each schedule must be stored separately. If multiple neighbors use the same unicast schedule, it will only be
 * allocated once and shared among the corresponding neighbors to save memory.
 */
#ifndef R_MAC_NEIGHBORS_MEM_CAPACITY
#if R_MINIMAL_ROUTER_NODE
#define R_MAC_NEIGHBORS_MEM_CAPACITY 800
#else
#define R_MAC_NEIGHBORS_MEM_CAPACITY 3000
#endif
#endif

/******************************************************************************
* Heaps
******************************************************************************/

#define R_OS_HEAP_BASEID            (32)

#if R_SHARED_NWK_MEM
    #define R_HEAP_COUNT            6                      //!< R_HEAP_COUNT *must* be set to the total number of heaps

    #define R_HEAP_ID_CYCTICK_NTF   (R_OS_HEAP_BASEID + 0) //!< Internal NWK messages (for cyglic timer)
    #define R_HEAP_ID_WARN_IND      (R_OS_HEAP_BASEID + 1) //!< Warning indications from MAC and NWK layer
    #define R_HEAP_ID_ERR_IND       (R_OS_HEAP_BASEID + 2) //!< Fatal error indications from MAC and NWK layer
    #define R_HEAP_ID_APL_REQ       (R_OS_HEAP_BASEID + 3) //!< Requests/Confirms APL <-> NWK
    #define R_HEAP_ID_MAC_NEIGHBORS (R_OS_HEAP_BASEID + 4) //!< Unicast schedules of neighbors (shared if equal!)
    #define R_HEAP_ID_NWK           (R_OS_HEAP_BASEID + 5) //!< Shared heap for all remaining modules
// share 1 heap among all NWK modules
    #define R_HEAP_ID_AUTH          R_HEAP_ID_NWK
    #define R_HEAP_ID_MPL           R_HEAP_ID_NWK
    #define R_HEAP_ID_FRAG_RX       R_HEAP_ID_NWK
    #define R_HEAP_ID_MAC_IND       R_HEAP_ID_NWK
    #define R_HEAP_ID_NWK_REQ       R_HEAP_ID_NWK
    #define R_HEAP_ID_NWK_IND       R_HEAP_ID_NWK

R_OS_HEAP_LIST_BEGIN R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_CYCTICK_NTF, R_HEAP_SIZE_CYCTICK_NTF)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_WARN_IND, R_HEAP_SIZE_WARN_IND)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_ERR_IND, R_HEAP_SIZE_ERR_IND)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_APL_REQ, R_HEAP_SIZE_APL_REQ)  //!< Requests/Confirms APL <-> NWK
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_MAC_NEIGHBORS, R_MAC_NEIGHBORS_MEM_CAPACITY)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_NWK, R_HEAP_SIZE_NWK_SHARED)
R_OS_HEAP_LIST_END
#else  /* if R_SHARED_NWK_MEM */
    #define R_HEAP_COUNT            11                      //!< R_HEAP_COUNT *must* be set to the total number of heaps

    #define R_HEAP_ID_CYCTICK_NTF   (R_OS_HEAP_BASEID + 0)  //!< Internal NWK messages (for cyglic timer)
    #define R_HEAP_ID_WARN_IND      (R_OS_HEAP_BASEID + 1)  //!< Warning indications from MAC and NWK layer
    #define R_HEAP_ID_ERR_IND       (R_OS_HEAP_BASEID + 2)  //!< Fatal error indications from MAC and NWK layer
    #define R_HEAP_ID_APL_REQ       (R_OS_HEAP_BASEID + 3)  //!< Requests/Confirms APL <-> NWK
    #define R_HEAP_ID_MAC_NEIGHBORS (R_OS_HEAP_BASEID + 4)  //!< Unicast schedules of neighbors (shared if equal!)
    #define R_HEAP_ID_AUTH          (R_OS_HEAP_BASEID + 5)  //!< Authentication/Security module (EAP-TLS + RSN)
    #define R_HEAP_ID_MPL           (R_OS_HEAP_BASEID + 6)  //!< Heap for multicast traffic module (MPL)
    #define R_HEAP_ID_FRAG_RX       (R_OS_HEAP_BASEID + 7)  //!< 6LoWPAN reassembly of incoming fragmented packets
    #define R_HEAP_ID_MAC_IND       (R_OS_HEAP_BASEID + 8)  //!< Indications MAC -> NWK
    #define R_HEAP_ID_NWK_REQ       (R_OS_HEAP_BASEID + 9)  //!< Requests/Confirms NWK <-> MAC
    #define R_HEAP_ID_NWK_IND       (R_OS_HEAP_BASEID + 10) //!< Indications NWK -> APL

// move heaps to external SRAM for BR configuration with large PAN Size and large neighbor table
#if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
#if defined(__CCRX__)
#pragma section B expRAM
#endif
#endif
R_OS_HEAP_LIST_BEGIN R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_CYCTICK_NTF, R_HEAP_SIZE_CYCTICK_NTF)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_WARN_IND, R_HEAP_SIZE_WARN_IND)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_ERR_IND, R_HEAP_SIZE_ERR_IND)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_APL_REQ, R_HEAP_SIZE_APL_REQ)  //!< Requests/Confirms APL <-> NWK
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_MAC_NEIGHBORS, R_MAC_NEIGHBORS_MEM_CAPACITY)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_AUTH, R_AUTH_MEM_CAPACITY)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_MPL, R_MPL_MEM_CAPACITY)
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_FRAG_RX, R_FRAG_MAX_CONCURRENT_RX_PACKETS * (R_MAX_MTU_SIZE + 16))
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_MAC_IND, R_HEAP_SIZE_MAC_IND)  //!< Indications MAC -> NWK
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_NWK_REQ, R_HEAP_SIZE_NWK_REQ)  //!< Requests/Confirms NWK <-> MAC
R_OS_HEAP_LIST_ENTRY(R_HEAP_ID_NWK_IND, R_HEAP_SIZE_NWK_IND)  //!< Indications NWK -> APL
R_OS_HEAP_LIST_END
#if R_BR_AUTHENTICATOR_ENABLED && (R_BR_PAN_SIZE > 64u)
#if defined(__CCRX__)
#pragma section
#endif
#endif

#endif /* R_SHARED_NWK_MEM */


/******************************************************************************
* Mailboxes (not based on FreeRTOS but an internal MSG implementation)
******************************************************************************/
#define ID_mac_mbx       0
#define ID_nwk_mbx       1
#define ID_nwk_inter_mbx 2
#define ID_apl_mbx       3

R_OS_MAILBOX_LIST_BEGIN R_OS_MAILBOX_LIST_ENTRY(ID_mac_mbx)
R_OS_MAILBOX_LIST_ENTRY(ID_nwk_mbx)
R_OS_MAILBOX_LIST_ENTRY(ID_nwk_inter_mbx)
R_OS_MAILBOX_LIST_ENTRY(ID_apl_mbx)
R_OS_MAILBOX_LIST_END

/******************************************************************************
* Event groups (flags)
******************************************************************************/

#define ID_mac_flg 0

R_OS_FLAG_LIST_BEGIN R_OS_FLAG_LIST_ENTRY(ID_mac_flg)
R_OS_FLAG_LIST_END


/******************************************************************************
* Semaphores
******************************************************************************/

#define ID_rfdrv_sem 0

R_OS_SEMAPHORE_LIST_BEGIN R_OS_SEMAPHORE_LIST_ENTRY(ID_rfdrv_sem, 1, 1)
R_OS_SEMAPHORE_LIST_END


/******************************************************************************
* Timers (cyclic handlers)
******************************************************************************/

#define ID_nwk_cyclic 0

void R_TIMER_Handle();

R_OS_TIMER_LIST_BEGIN R_OS_TIMER_LIST_ENTRY(ID_rfdrv_sem, R_TIMER_Handle, 100, 0)
R_OS_TIMER_LIST_END


/******************************************************************************
* Tasks
******************************************************************************/
#define ID_mac_tsk               0
#define ID_nwk_tsk               1
#define ID_apl_tsk               2

#define MAC_TSK_DEFAULT_PRIORITY 3
#define NWK_TSK_DEFAULT_PRIORITY 1
#define APL_TSK_DEFAULT_PRIORITY 0

#define HIGHEST_TSK_PRIORITY     4

void mac_tsk(void*);
void nwk_tsk(void*);
void apl_tsk(void*);

#if R_MINIMAL_ROUTER_NODE || R_MODEM_SERVER
// 308 bytes seem to be used for some internal state
#define ID_mac_tsk_stack_size (1200 + 308)
#define ID_nwk_tsk_stack_size (1800 + 308)
#define ID_apl_tsk_stack_size (2400 + 308)
#else
#define ID_mac_tsk_stack_size (4 * 1200)
#define ID_nwk_tsk_stack_size (4 * 2400)
#define ID_apl_tsk_stack_size (4 * 2400)
#endif

R_OS_TASK_LIST_BEGIN R_OS_TASK_LIST_ENTRY(ID_mac_tsk, "MAC_TSK", mac_tsk, ID_mac_tsk_stack_size, MAC_TSK_DEFAULT_PRIORITY, 1)
R_OS_TASK_LIST_ENTRY(ID_nwk_tsk, "NWK_TSK", nwk_tsk, ID_nwk_tsk_stack_size, NWK_TSK_DEFAULT_PRIORITY, 1)
R_OS_TASK_LIST_ENTRY(ID_apl_tsk, "APL_TSK", apl_tsk, ID_apl_tsk_stack_size, APL_TSK_DEFAULT_PRIORITY, 0)
R_OS_TASK_LIST_END

#endif /* R_OS_WRAPPER_CONFIG_H */
