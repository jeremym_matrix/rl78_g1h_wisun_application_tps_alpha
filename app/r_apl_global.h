/*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
* © 2020 Renesas Electronics Corporation All rights reserved.
******************************************************************************/

#if !R_DEV_DISABLE_AUTH
#include "r_auth_types.h"
#if R_EAP_TLS_SERVER_ENABLED
#include "r_auth_br_eap_tls.h"
#endif
#endif

#include "r_nwk_api.h"

#ifndef R_APL_CONFIG_H
#define R_APL_CONFIG_H

/******************************************************************************
   Typedef definitions
******************************************************************************/

/** The global information structure of the application layer */
typedef struct
{
#if !R_DEV_DISABLE_AUTH
    r_auth_state_t      auth;
#if R_EAP_TLS_SERVER_ENABLED
    r_eap_tls_srv_ctx_t eap_tls_srv;  //!< The context of the EAP-TLS server module
#endif
#endif

    /** Cached NWK state to avoid repeated requests to network layer */
    struct
    {
        r_nwk_join_state_t joinState;  //!< The current join state of this device (known from NWK indications)
    } nwk;
} r_apl_global_t;


/******************************************************************************
   Functions Prototypes
******************************************************************************/

void R_APL_ClearAplGlobal();

r_apl_global_t* R_APL_GetAplGlobal();

#endif  /* R_APL_CONFIG_H */
