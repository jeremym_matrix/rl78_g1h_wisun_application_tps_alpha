/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2020 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/**
 * @file r_app_main.c
 * @brief Generic application layer functionality (including task definition)
 */

/******************************************************************************
   Includes   <System Includes> , "Project Includes"
******************************************************************************/
#include "r_app_main.h"

#include <stdio.h>
#include <string.h>

#include "hardware.h"
#include "r_os_wrapper.h"
#include "r_nwk_api_base.h"
#include "r_rpl_api.h"
#include "r_modem_demo_msg.h"

#ifdef R_SIMPLE_RFTEST_ENABLED
#include "r_simple_rftest.h"
#endif

#if R_DEV_TBU_ENABLED && __RX
#include "iodefine.h"
#endif

/******************************************************************************
   Macro definitions
******************************************************************************/


/******************************************************************************
   Typedef definitions
******************************************************************************/


/******************************************************************************
   Imported global variables and functions (from other files)
******************************************************************************/
extern void AppCmd_Init();

#ifdef R_DEV_AUTO_START
extern void AppCmd_AutoStart();
#endif

extern unsigned char AppIpProcessCmd(unsigned char* pCmd);
extern unsigned char AppIpReceiveMessage(unsigned char* pErase, r_os_msg_t p_msg);

/******************************************************************************
   Exported global variables and functions (to be accessed by other files)
******************************************************************************/
unsigned char AppStrToLower(unsigned char c);
unsigned char AppAton(unsigned char c);

unsigned char AppCmdBuf[APP_CMD_BUFSIZE];
unsigned short AppCmdSize;

#ifdef R_SIMPLE_RFTEST_ENABLED
static r_boolean_t rfTestModeEnable = R_FALSE;
#endif

/******************************************************************************
   Map dummy variable to external SRAM section to avoid linker warning
******************************************************************************/
#if defined(__CCRX__)
#pragma section B expRAM
uint8_t dummy_u8_var_in_expRAM;
uint32_t dummy_u32_var_in_expRAM;
#pragma section
#endif

/******************************************************************************
   Map rstr_handle variable to none initialized RAM section
******************************************************************************/
#if R_DEV_TBU_ENABLED && __RX
#pragma section B noINIT
uint8_t rstr_handle;
#pragma section
#endif

/******************************************************************************
   Private variables
******************************************************************************/

/******************************************************************************
   Private function prototypes
******************************************************************************/
static void AppReceiveMessage(void);

/******************************************************************************
   Public function bodies
******************************************************************************/

/********************************************************************************
* Function Name     : reset
* Description       : Reset(dummy)
* Arguments         : None
* Return Value      : None
********************************************************************************/
void reset(void)
{
}


/********************************************************************************
* Function Name     : apl_task
* Description       : Application task to process messages from IPv6 stack
* Arguments         : None
* Return Value      : None
********************************************************************************/
void apl_task()
{
    /* Initialization */
    RdrvPeripheralInitialize();

#ifdef R_SIMPLE_RFTEST_ENABLED

    /* Check if RF Test mode is enabled */
    rfTestModeEnable = R_Simple_RFTest_IsValid();

    if (rfTestModeEnable == R_TRUE)
    {
        /* Execute simple RF Test program */
        R_Simple_RFTest_Main();
    }
#endif /* R_SIMPLE_RFTEST_ENABLED */

    R_Modem_Demo_Init(AppCmdBuf, sizeof(AppCmdBuf));  // Initialize HDLC module for UART

    AppCmd_Init();

#ifdef R_DEV_AUTO_START
    AppCmd_AutoStart();
#endif

#if R_DEV_TBU_ENABLED && __RX

    /* Check if a software reset was detected  */
    if (SYSTEM.RSTSR2.BIT.SWRF == R_TRUE)
    {
        /* Send the pending confirm for the RSTR reset request */
        R_Modem_print("RSTC %02X %02X\n", rstr_handle, R_RESULT_SUCCESS);
    }
#endif

    while (1)
    {
        R_Modem_Demo_ReadCommand();
        AppReceiveMessage();
    }
}

/******************************************************************************
   Private function bodies
******************************************************************************/

/********************************************************************************
* Function Name     : AppReceiveMessage
* Description       : Receive Message from NWK task
* Arguments         : None
* Return Value      : None
********************************************************************************/
static void AppReceiveMessage(void)
{
    r_os_msg_header_t* msg = R_OS_ReceiveMsg(ID_apl_mbx, R_OS_TIMEOUT_POLL);
    if (msg != NULL)
    {
        unsigned char erase;
        AppIpReceiveMessage(&erase, msg);
        if (R_TRUE == erase)
        {
            R_OS_MsgFree(msg);
        }
    }
}

/********************************************************************************
* Function Name     : AppCmd_ProcessCmd
* Description       : Process a test command
* Arguments         : pCmd ... Pointer to command
* Return Value      : None
********************************************************************************/
void AppCmd_ProcessCmd(unsigned char* pCmd, uint16_t size)
{
    if (pCmd[size - 1] != '\n')
    {
        return;  // Discard command if it does not end with a newline character
    }

    if (AppIpProcessCmd(pCmd) == 0)
    {
        R_Modem_print("Command not found!!\n");
    }
}

/********************************************************************************
* Function Name     : AppStricmp
* Description       : Compare string
* Arguments         : pStr1 ... Pointer to string #1
*                   : pStr2 ... Double pointer to string #2
* Return Value      : int8_t
********************************************************************************/
char AppStricmp(void __far* pStr1, void** ppStr2)
{
    unsigned char __far* ptr1 = (unsigned char __far*)pStr1;
    unsigned char* ptr2 = (unsigned char*)*ppStr2;
    unsigned char c1 = 0, c2;

    /* first remove leading white spaces */
    c2 = *ptr2;
    while ((c2 == ' ') || (c2 == '\t'))
    {
        c2 = *(++ptr2);
    }

    while (1)
    {
        c1 = *ptr1;
        c2 = *ptr2;

        /* upper case -> lower case */
        c1 = AppStrToLower(c1);
        c2 = AppStrToLower(c2);

        /* null terminator or no match ? */
        if ((!c1 || !c2) || (c1 != c2))
        {
            break;
        }
        else  /* if ( c1 == c2 ) */
        {
            ptr1++;
            ptr2++;
        }
    }

    /* re-position the pointer to skip the parsed part (only if match was a success) */
    if (!c1 && (!c2 || c2 == ' ' || c2 == '\t'))
    {
        *ppStr2 = ptr2;
        return 0;
    }
    else
    {
        return c1 - c2;
    }
}

/********************************************************************************
* Function Name     : AppAton
* Description       : Convert ascii character to number
* Arguments         : c ... Ascii character ('0'...'9','a'...'f','A'...'F')
* Return Value      : Converted number (0 ... 15)
********************************************************************************/
unsigned char AppAton(unsigned char c)
{
    c = AppStrToLower(c);
    if (c >= '0' && c <= '9')
    {
        return c - '0';
    }
    else if (c >= 'a' && c <= 'f')
    {
        return (c - 'a') + 10;
    }
    return 0;
}

/********************************************************************************
* Function Name     : AppStrToLower
* Description       : Compare string
* Arguments         : c ... data
* Return Value      : uint8_t
********************************************************************************/
unsigned char AppStrToLower(unsigned char c)
{
    if ((c >= 'A') && (c <= 'Z'))
    {
        c = (c - 'A') + 'a';
    }
    return c;
}

/********************************************************************************
* Function Name     : AppCmd_HexStrToNum
* Description       : Convert hex format string to number
* Arguments         : ppBuf ... Double pointer buffer
*                   : pData ... Ponter to data
*                   : size ... Size of string
*                   : isOctetStr ...
*                   :   R_TRUE: Octtet string
*                   :   R_TRUE: Big endian format
* Return Value      : r_result_t
********************************************************************************/
unsigned char AppHexStrToNum(unsigned char** ppBuf, void* pData, short dataSize, unsigned char isOctetStr)
{
    unsigned char* ptr1, * ptr2, byte;
    unsigned short count, i;
    unsigned char ret = 0;

    /* initialize with 0. */
    if (0 == isOctetStr)
    {
        memset(pData, 0, dataSize);
    }

    ptr1 = *ppBuf;
    ptr2 = (unsigned char*)pData;

    /* skip the non-numeric characters */
    AppCmd_SkipWhiteSpace(&ptr1);

    /* Count numeric characters */
    count = 0;
    while ((count != (dataSize * 2)) && ((*ptr1 >= '0' && *ptr1 <= '9')
                                         || (*ptr1 >= 'A' && *ptr1 <= 'F')
                                         || (*ptr1 >= 'a' && *ptr1 <= 'f')))
    {
        count++;
        ptr1++;
    }

    if (!count || (count % 2) != 0)
    {
        ret = 1;
    }
    else
    {
        *ppBuf = ptr1;

        if (isOctetStr)
        {
            ptr1 = ptr1 - count;
        }
        else
        {
            ptr1 -= 2;
        }

        i = count;
        do
        {
            /* Convert character to number */
            byte = AppAton(*ptr1++);
            byte <<= 4;
            byte |= AppAton(*ptr1++);

            *ptr2++ = byte;
            if (!isOctetStr)
            {
                ptr1 -= 4;
            }
            i -= 2;
        }
        while (i != 0);

        if (isOctetStr)
        {
            memset(ptr2, 0, dataSize - (count / 2));
        }
    }

    return ret;
}

/********************************************************************************
* Function Name     : AppSkipWhiteSpace
* Description       : Skip white space
* Arguments         : ppBuf ... Double pointer to buffer
* Return Value      : None
********************************************************************************/
void AppCmd_SkipWhiteSpace(unsigned char** ppBuf)
{
    unsigned char* ptr = *ppBuf;

    /* skip the non-numeric characters */
    while ((*ptr == ' ') || (*ptr == '\t'))
    {
        ptr++;
    }
    *ppBuf = ptr;
}
