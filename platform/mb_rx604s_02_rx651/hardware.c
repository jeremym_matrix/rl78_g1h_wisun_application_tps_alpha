/****************************************************************************** 
* DISCLAIMER 

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. 

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws. 

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED. 

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software. 
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link: 
* http://www.renesas.com/disclaimer 
******************************************************************************/ 

/* Copyright (C) 2014. Renesas Electronics Corporation, All Rights Reserved. */

/****************************************************************************** 
* File Name : hardware.c 
* Version : 1.00 
* Device(s) : RX651 
* Tool-Chain :  
* OS : 
* H/W Platform : 
* Description : Hardware dependency routines for RX651
* Operation : 
* Limitations : None 
****************************************************************************** 
* History : DD.MM.YYYY Version Description 
******************************************************************************/ 

/****************************************************************************** 
    Includes
******************************************************************************/ 

#include    "stdio.h"
#include    <machine.h>
#include    "iodefine.h"
#include    "hardware.h"
#include    "MB_RXRAA604S00.h"
#include    "uart_interface.h"
#if R_PHY_TYPE_CWX_M
#include    "r_phy.h"
#else
#include    "phy.h"
#endif
#include    "clocksetting.h"
#include    "r_frequency_hopping.h"

#if R_MODEM_SERVER || R_MODEM_CLIENT
#include "r_modem.h"
#endif

#ifdef R_HYBRID_TYPE2B_TESSERA
#include "r_typedefs.h"
#include "r_config.h"
#include "r_bsp_api.h"
#endif

/****************************************************************************** 
Hardware Revision of Tessera Board
******************************************************************************/ 
/* UART Registers */
#ifdef R_HYBRID_TYPE2B_TESSERA
#define UART_RX             SCI8.RDR
#define UART_TX             SCI8.TDR
#define UART_RX_STATUS      SCI8.SSR.BYTE
#define UART_TX_STATUS      SCI8.SSR.BYTE
#else
#define UART_RX             SCI6.RDR
#define UART_TX             SCI6.TDR
#define UART_RX_STATUS      SCI6.SSR.BYTE
#define UART_TX_STATUS      SCI6.SSR.BYTE
#endif
#define UART_CTRL           SCI6.SCR.BYTE
#define UART_MODE           SCI6.SMR.BYTE
#define UART_SMARTCARDMODE  SCI6.SCMR.BYTE
#define UART_BRGC           SCI6.BRP
#define UART_BITRATE        SCI6.BRR

#define UART_CKE            SCI6.SCR.BIT.CKE
#define UART_TX_ENA         SCI6.SCR.BIT.TE
#define UART_RX_ENA         SCI6.SCR.BIT.RE
#define UART_TX_IENA        SCI6.SCR.BIT.TIE
#define UART_RX_IENA        SCI6.SCR.BIT.RIE

#define UART_TX_INT_PRI     IPR(SCI6, TXI6)     // for SCI6.SCR.BIT.TIE
#define UART_RX_INT_PRI     IPR(SCI6, RXI6)     // for SCI6.SCR.BIT.RIE
#define UART_ERR_INT_PRI    IPR(ICU, GROUPBL0)  //
#define UART_TX_INT_ENA     IEN(SCI6, TXI6)     // for SCI6.SCR.BIT.TIE
#define UART_RX_INT_ENA     IEN(SCI6, RXI6)     // for SCI6.SCR.BIT.RIE
#define UART_ERR_INT_ENA_GP IEN(ICU, GROUPBL0)  //
#define UART_ERR_INT_ENA    EN(SCI6, ERI6)      //

#define UART_TX_INT_REQ     IR(SCI6, TXI6)      // for SCI6.SCR.BIT.TIE
#define UART_RX_INT_REQ     IR(SCI6, RXI6)      // for SCI6.SCR.BIT.RIE
#define UART_ERR_INT_REQ    IR(ICU, GROUPBL0)   //

#define UART_RX_STATUS_PARTYERR     SCI6.SSR.BIT.PER    //
#define UART_RX_STATUS_FRAMEERR     SCI6.SSR.BIT.FER    //
#define UART_RX_STATUS_OVERRUNERR   SCI6.SSR.BIT.ORER   //

/* UART Ports */
#define UART_RX_PM          PORT3.PDR.BIT.B3    // PM3.3
#define UART_TX_PM          PORT3.PDR.BIT.B2    // PM3.2
#define UART_CTS_PM         PORT0.PDR.BIT.B7    // PM0.7
#define UART_RTS_PM         PORTJ.PDR.BIT.B3    // PMJ.3
#define UART_RX_PORT        PORT3.PIDR.BIT.B3   // P3.3
#define UART_TX_PORT        PORT3.PODR.BIT.B2   // P3.2
#define UART_CTS_PORT       PORT0.PIDR.BIT.B7   // PM0.7
#define UART_RTS_PORT       PORTJ.PODR.BIT.B3   // PMJ.3
#define UART_RX_PORT_PUP    PORT3.PCR.BIT.B3    // PU3.3
#define UART_TX_PORT_PUP    PORT3.PCR.BIT.B2    // PU3.2
#define UART_CTS_PORT_PUP   PORT0.PCR.BIT.B7    // PU0.7
#define UART_RTS_PORT_PUP   PORTJ.PCR.BIT.B3    // PUJ.3
#define UART_RX_FUNC        MPC.P33PFS.BYTE     //
#define UART_TX_FUNC        MPC.P32PFS.BYTE     //
#define UART_PORT_SEL       PORT3.PMR.BYTE      //


#define R_UDP_MAX_UDP_DATA_LENGTH (1024)

/****************************************************************************** 
Macro definitions 
******************************************************************************/
#if R_MODEM_SERVER || R_MODEM_CLIENT
#define RDRV_UART_RX_BUFSIZ             (1)
/* Since the modem server makes a system call to pass the UART data, its priority must not exceed the maximum system
 * call interrupt priority of FreeRTOS (configMAX_SYSCALL_INTERRUPT_PRIORITY). Otherwise, the call will fail (trap). */
#define RP_APL_INTLEVEL_UARTRXs         (6)
#else
#define RDRV_UART_RX_BUFSIZ             (R_UDP_MAX_UDP_DATA_LENGTH*2 + 128)
#define RP_APL_INTLEVEL_UARTRXs         (7)
#endif

/* Interrupt Level */
#define RP_MAC_INTLEVEL_FREQHOPPTimer   (3)               // Frequency Hopping Timer

/****************************************************************************** 
 Prototypes 
******************************************************************************/ 
static void         RdrvUART_Initialize(unsigned char brgc);
static void         RdrvUART_RxErrInt(void);
static void         RdrvSwitch_Initialize(void);
void                RdrvLED_Initialize(void);
static void         RdrvExtBus_Initialize(void);
static void         peripheral_modules_enable(void);
void                configure_operating_frequency(unsigned char);
static void         CheckSRAMAccess(void);
void                sci_iic_int_sci_iic12_tei_isr (void);
#ifdef R_HYBRID_PLC_RF
void                r_modem_rx(uint8_t byte);
#endif

/****************************************************************************** 
 Option Memory Setting 
******************************************************************************/ 


/****************************************************************************** 

 Variables
******************************************************************************/ 
unsigned char           RdrvUART_RXBuf[RDRV_UART_RX_BUFSIZ];                /* UART RX Buffer */
unsigned short          RdrvUART_RXBuf_RdPtr;                               /* UART RX Buffer read pointer */
unsigned short          RdrvUART_RXBuf_WrPtr;                               /* UART RX Buffer write pointer */


// ===========================================================================================
//   Hardware settings
// ===========================================================================================
// -------------------------------------------------
//   Initialize
// -------------------------------------------------
void hardware_setup(void)
{
    peripheral_modules_enable();
    configure_operating_frequency(1);
}

/******************************************************************************
* Function name: peripheral_modules_enable
* Description  : Enables and configures peripheral devices on the MCU
* Arguments    : none
* Return value : none
******************************************************************************/
static void peripheral_modules_enable(void)
{

    /* Add code here to enable peripherals used by the application */
    SYSTEM.PRCR.WORD = 0xA503;                                      // Unlock register protection
    MSTP(SCI0) = 0;
    MSTP(SCI6) = 0;
    MSTP(DMAC) = 0;
    MSTP(TPU2) = 0;     // used for RpWupTskHdr of RF driver
    MSTP(TPU3) = 0;     // used for Frequency Hopping
    MSTP(TPU4) = 0;     // used for RF driver
    SYSTEM.PRCR.WORD = 0xA500;                                      // Lock register protection
}

/******************************************************************************
* Outline       : configure_operating_frequency
* Description   : Configures the clock settings for each of the device clocks
* Argument      : none
* Return value  : none
******************************************************************************/
void 
configure_operating_frequency(unsigned char subclk_set)
{
    unsigned long i, sckcr = 0;
    unsigned short sckcr2 = 0;

    /*
    System clock settings are defined in platform.h header file 
    and used in other peripheral settings.  System clocks are setup 
    based on input crystal frequency and internal PLL as follows: 

    Input Clock ..........................  XTAL_FREQUENCY
    PLL ..................................  XTAL_FREQUENCY * 16
    Flash Clock ..........................  XTAL_FREQUENCY * FCLK_MUL  (Max 50  MHz)
    System Clock .........................  XTAL_FREQUENCY * ICLK_MUL  (Max 100 MHz)
    External Bus Clock Output Disabled                                 (Max 50 MHz)
    SDRAM Clock Output Disabled                                        (Max 50 MHz) 
    External Bus Clock ...................  XTAL_FREQUENCY * BCLK_MUL  (Max 100 MHz)
    Peripheral Module Clock A ............  XTAL_FREQUENCY * PCLKA_MUL (Max 100 MHz)
    Peripheral Module Clock B ............  XTAL_FREQUENCY * PCLKB_MUL (Max 50 MHz)
    USB Clock ............................  XTAL_FREQUENCY * UCLK_MUL  (Max 48 MHz)
    Inter Equipment Bus Clock ............  XTAL_FREQUENCY * IECLK_MUL (Max 44 MHz)
    */

    /* FCK - Flash clock */
    sckcr += (FCLK_MUL==8) ? (1ul << 28) : (FCLK_MUL==4) ? (2ul << 28) : (FCLK_MUL==2) ? (3ul << 28) : (4ul << 28);
    /* ICK - System clock */
    sckcr += (ICLK_MUL==8) ? (1ul << 24) : (ICLK_MUL==4) ? (2ul << 24) : (ICLK_MUL==2) ? (3ul << 24) : (4ul << 24);
    /* PSTOP1 - BCLK output disabled */
    sckcr += (1ul << 23);
    /* PSTOP0 - SDCLK output disabled */
    sckcr += (1ul << 22);
    /* BCK - External bus clock */
    sckcr += (BCLK_MUL==8) ? (1ul << 16) : (BCLK_MUL==4) ? (2ul << 16) : (BCLK_MUL==2) ? (3ul << 16) : (4ul << 16);
    /* PCKA - Peripheral clock A */
    sckcr += (PCLKA_MUL==8) ? (1ul << 12) : (PCLKA_MUL==4) ? (2ul << 12) : (PCLKA_MUL==2) ? (3ul << 12) : (4ul <<  8);
    /* PCKB - Peripheral clock B */
    sckcr += (PCLKB_MUL==8) ? (1ul <<  8) : (PCLKB_MUL==4) ? (2ul <<  8) : (PCLKB_MUL==2) ? (3ul <<  8) : (4ul <<  8);
    /* Must be same as PCKB */
    sckcr += (PCLKB_MUL==8) ? (1ul <<  4) : (PCLKB_MUL==4) ? (2ul <<  4) : (PCLKB_MUL==2) ? (3ul <<  4) : (4ul <<  4);
    /* Must be same as PCKB */
    sckcr += (PCLKB_MUL==8) ? (1ul <<  0) : (PCLKB_MUL==4) ? (2ul <<  0) : (PCLKB_MUL==2) ? (3ul <<  0) : (4ul <<  0);

    /* UCK - USB clock.  Not much options here. */
    sckcr2 += (UCLK_MUL == 4) ? (3ul << 4) : (3ul << 4);
    /* IEBCK - Inter Equipment Bus Clock */
    sckcr2 += (IECLK_MUL == 8) ? (1ul << 0) : (IECLK_MUL == 4) ? (2ul << 0) : (IECLK_MUL == 2) ? (3ul << 0) : (4ul << 0);
    /* PCKC */
    sckcr += (1ul <<  4);
    /* PCKD */
    sckcr += (1ul <<  0);

    /* UCK - USB clock.  Not much options here. */
    sckcr2 += (UCLK_MUL==4) ? (3ul << 4) : (3ul << 4);
    /* Not much options here. */
    sckcr2 += (1ul << 0);

    /* Enable writing to proteced registers */
    SYSTEM.PRCR.WORD = 0xA50B;

    if(subclk_set)
    {
        /* Sub-clock Initialize */
        SYSTEM.SOSCCR.BIT.SOSTP = 0;
        for(i = 0; i < 20; i++)            /* Wait a little */
        {
            Rhw_NOP();
        }
        while(SYSTEM.SOSCCR.BIT.SOSTP == 1);
        RTC.RCR4.BIT.RCKSEL = 0;            //RTC.RCR4.BIT.RCKSEL  = 0;
        RTC.RCR3.BYTE = 0x0D;               //RTC.RCR3.BIT.RTCEN = 1;
        while(RTC.RCR3.BIT.RTCEN == 0);
        SYSTEM.SOSCCR.BIT.SOSTP = 1;
        SYSTEM.SOSCWTCR.BIT.SSTS = 0;
        for(i = 0; i < 20; i++)            /* Wait a little */
        {
            Rhw_NOP();
        }
        SYSTEM.SOSCCR.BIT.SOSTP = 0;
        for(i = 0; i < 20; i++)            /* Wait a little */
        {
            Rhw_NOP();
        }
        while(SYSTEM.SOSCCR.BIT.SOSTP == 1);
        RTC.RCR3.BIT.RTCEN = 0;
    }

   
    SYSTEM.MOSCWTCR.BYTE = 0x0D;        /* Main clock oscillator wait is 131,072 cycles at 12 MHz = 10.92ms */
    SYSTEM.ROMWT.BIT.ROMWT = 2;         /* RX651 */
    FLASH.ROMCE.BIT.ROMCEN = 1;         /* RX651 */
    SYSTEM.PLLCR.WORD = 0x1300;         /* PLL is 16MHz * 10 = 160 MHz */
    SYSTEM.MOSCCR.BYTE = 0x00;          /* Turn on main clock oscillator */
    SYSTEM.PLLCR2.BYTE = 0x00;          /* Turn on PLL */
    SYSTEM.BCKCR.BYTE = 0x01;   //tani

    /*
    At this point still running on LOCO at 125 kHz. According to
    HW manual we have to wait until main clock and PLL stabilization
    time has passed.
    */

    for(i = 0; i < 275; i++)            /* Wait about 11ms */
    {
        Rhw_NOP();
    }

    /* Set system clocks */
    SYSTEM.SCKCR.LONG = sckcr;
    SYSTEM.SCKCR2.WORD = sckcr2;
    
    /* Select PLL */
    SYSTEM.SCKCR3.WORD = 0x0400;

    /* Disable writing to protected registers*/
    SYSTEM.PRCR.WORD = 0xA500;
}


/******************************************************************************
Function Name:       RdrvPeripheralInitialize
Parameters:          none.
Return value:        none.
Description:         Initialize peripheral settings.
******************************************************************************/
void
RdrvPeripheralInitialize(void)
{
    Rhw_DI();

    /* UART settings */
#ifdef R_HYBRID_TYPE2B_TESSERA
    R_BSP_SetBoardType(R_BOARD_G_CPX3);

    R_BSP_ConfigureUart(R_UART_CPX_CLOCK,
                        500000uL,
                        R_BSP_MCU_CPX_UART,
                        RP_APL_INTLEVEL_UARTRXs,
                        RP_APL_INTLEVEL_UARTRXs,
                        NULL,
                        &r_modem_rx);
#else
    RdrvUART_Initialize(BPS115200);
#endif

#if !R_PHY_TYPE_CWX_M
    /* SWITCH ports settings */
    RdrvSwitch_Initialize();

    /* LED ports settings */
    RdrvLED_Initialize();
#ifndef R_NETWORK_RACK
    /* ExternalBus settings */
    RdrvExtBus_Initialize();
    CheckSRAMAccess();
#endif /* R_NETWORK_RACK */
#endif /* !R_PHY_TYPE_CWX_M */
    Rhw_EI();
}


/******************************************************************************
Function Name:       CheckSRAMAccess
Parameters:          none.
Return value:        none.
Description:         
******************************************************************************/
static void
CheckSRAMAccess(void)
{
    unsigned char   *ExtPtr;
    unsigned long   *ExtLongPtr;
    unsigned long   i;
#if 0
    static unsigned char    aa[256];
    static unsigned long    bb[256];

    /* clear result mem */
    for(i=0;i<256;i++){
        aa[i] = 0x00;
        bb[i] = 0x00;
    }
#endif

    /* 0x5A */
    /* Write to ext RAM */
    ExtPtr = (unsigned char*)0x5000000;

    for(i=0;i<(1024*1024);i++){
        *ExtPtr = 0x5A;
        ExtPtr++;
    }

    /* Check */
    ExtPtr = (unsigned char*)0x5000000;

    for(i=0;i<(1024*1024);i++){
        if(*ExtPtr != 0x5A){
            while(1);
        }
        ExtPtr++;
    }

    /* Copy to internal RAM */
    ExtPtr = (unsigned char*)0x5000000;

    for(i=0;i<256;i++){
#if 0
        aa[i] = *ExtPtr;
#endif
        ExtPtr++;
    }

    /* 0xA5 */
    /* Write to ext RAM */
    ExtPtr = (unsigned char*)0x5000000;

    for(i=0;i<(1024*1024);i++){
        *ExtPtr = 0xA5;
        ExtPtr++;
    }

    /* Check */
    ExtPtr = (unsigned char*)0x5000000;

    for(i=0;i<(1024*1024);i++){
        if(*ExtPtr != 0xA5){
            while(1);
        }
        ExtPtr++;
    }

    /* Copy to internal RAM */
    ExtPtr = (unsigned char*)0x5000000;

    for(i=0;i<256;i++){
#if 0
        aa[i] = *ExtPtr;
#endif
        ExtPtr++;
    }

    /* incremental data(32bit) */
    /* Write to ext RAM */
    ExtLongPtr = (unsigned long*)0x5000000;

    for(i=0;i<(1024*1024)/4;i++){
        *ExtLongPtr = i;
        ExtLongPtr++;
    }

    /* Check */
    ExtLongPtr = (unsigned long*)0x5000000;

    for(i=0;i<(1024*1024)/4;i++){
        if(*ExtLongPtr != i){
            while(1);
        }
        ExtLongPtr++;
    }

    /* Copy to internal RAM */
    ExtLongPtr = (unsigned long*)0x5000000;
    ExtPtr = (unsigned char*)0x5000000;

    for(i=0;i<256;i++){
#if 0
        bb[i] = *ExtLongPtr;
        ExtLongPtr++;
        aa[i] = *ExtPtr;
        ExtPtr++;
#else
        ExtLongPtr++;
        ExtPtr++;
#endif
    }
}


/******************************************************************************
Function Name:       RdrvExtBus_Initialize
Parameters:          none.
Return value:        none.
Description:         Initialize external bus settings.
******************************************************************************/
static void
RdrvExtBus_Initialize(void)
{
    BSC.CS3CR.WORD = 0x1001;
    BSC.CS3MOD.WORD = 0x0000;
    BSC.CS3REC.WORD = 0x0000;
    BSC.CSRECEN.WORD = 0x0000;
    BSC.CS3WCR1.LONG = 0x07070707;
    BSC.CS3WCR2.LONG = 0x02220013;

    MPC.PFAOE1.BIT.A16E = 1;        /* A16 enable */
    MPC.PFAOE1.BIT.A17E = 1;        /* A17 enable */
    MPC.PFAOE1.BIT.A18E = 1;        /* A18 enable */
    MPC.PFAOE1.BIT.A19E = 1;        /* A19 enable */
    MPC.PFBCR0.BIT.ADRHMS2 = 0;      /* PC2-PC4 => A18-A20 */
    MPC.PFBCR0.BIT.DHE = 1;         /* D8-D15 enable */
    MPC.PFCSE.BIT.CS3E = 1;         /* CS3# enable */
    MPC.PFCSS0.BYTE = 0xA9;         /* PC4 => CS3# */
    MPC.PFBCR0.BIT.WR1BC1E = 1;     /* WR1# enable */
    MPC.PFBCR1.BIT.ALEOE = 1;       /* ALE enable */

#if 0
    PORT5.PDR.BIT.B0 = 0;           /* P50 => Input */
    PORT5.PDR.BIT.B1 = 0;           /* P51 => Input */
    PORT5.PDR.BIT.B2 = 0;           /* P52 => Input */
    PORT5.PDR.BIT.B4 = 0;           /* P54 => Input */
    PORTC.PDR.BIT.B0 = 0;           /* PC0 => Input */
    PORTC.PDR.BIT.B1 = 0;           /* PC1 => Input */
    PORTC.PDR.BIT.B2 = 0;           /* PC2 => Input */
    PORTC.PDR.BIT.B3 = 0;           /* PC3 => Input */
    PORTC.PDR.BIT.B4 = 0;           /* PC4 => Input */
    PORTD.PDR.BIT.B0 = 0;           /* PD0 => Input */
    PORTD.PDR.BIT.B1 = 0;           /* PD1 => Input */
    PORTD.PDR.BIT.B2 = 0;           /* PD2 => Input */
    PORTD.PDR.BIT.B3 = 0;           /* PD3 => Input */
    PORTD.PDR.BIT.B4 = 0;           /* PD4 => Input */
    PORTD.PDR.BIT.B5 = 0;           /* PD5 => Input */
    PORTD.PDR.BIT.B6 = 0;           /* PD6 => Input */
    PORTD.PDR.BIT.B7 = 0;           /* PD7 => Input */
    PORTE.PDR.BIT.B0 = 0;           /* PE0 => Input */
    PORTE.PDR.BIT.B1 = 0;           /* PE1 => Input */
    PORTE.PDR.BIT.B2 = 0;           /* PE2 => Input */
    PORTE.PDR.BIT.B3 = 0;           /* PE3 => Input */
    PORTE.PDR.BIT.B4 = 0;           /* PE4 => Input */
    PORTE.PDR.BIT.B5 = 0;           /* PE5 => Input */
    PORTE.PDR.BIT.B6 = 0;           /* PE6 => Input */
    PORTE.PDR.BIT.B7 = 0;           /* PE7 => Input */
#else
    PORT5.PDR.BIT.B0 = 1;           /* P50 => Input */
    PORT5.PDR.BIT.B1 = 1;           /* P51 => Input */
    PORT5.PDR.BIT.B2 = 1;           /* P52 => Input */
    PORT5.PDR.BIT.B4 = 1;           /* P54 => Input */
    PORTC.PDR.BIT.B0 = 1;           /* PC0 => Input */
    PORTC.PDR.BIT.B1 = 1;           /* PC1 => Input */
    PORTC.PDR.BIT.B2 = 1;           /* PC2 => Input */
    PORTC.PDR.BIT.B3 = 1;           /* PC3 => Input */
    PORTC.PDR.BIT.B4 = 1;           /* PC4 => Input */
    PORTD.PDR.BIT.B0 = 1;           /* PD0 => Input */
    PORTD.PDR.BIT.B1 = 1;           /* PD1 => Input */
    PORTD.PDR.BIT.B2 = 1;           /* PD2 => Input */
    PORTD.PDR.BIT.B3 = 1;           /* PD3 => Input */
    PORTD.PDR.BIT.B4 = 1;           /* PD4 => Input */
    PORTD.PDR.BIT.B5 = 1;           /* PD5 => Input */
    PORTD.PDR.BIT.B6 = 1;           /* PD6 => Input */
    PORTD.PDR.BIT.B7 = 1;           /* PD7 => Input */
    PORTE.PDR.BIT.B0 = 1;           /* PE0 => Input */
    PORTE.PDR.BIT.B1 = 1;           /* PE1 => Input */
    PORTE.PDR.BIT.B2 = 1;           /* PE2 => Input */
    PORTE.PDR.BIT.B3 = 1;           /* PE3 => Input */
    PORTE.PDR.BIT.B4 = 1;           /* PE4 => Input */
    PORTE.PDR.BIT.B5 = 1;           /* PE5 => Input */
    PORTE.PDR.BIT.B6 = 1;           /* PE6 => Input */
    PORTE.PDR.BIT.B7 = 1;           /* PE7 => Input */
#endif

    SYSTEM.PRCR.WORD = 0xA503;
    SYSTEM.SYSCR0.WORD = 0x5A03;    /* ExternalBus enable */
    SYSTEM.PRCR.WORD = 0xA500;

}


/******************************************************************************
Function Name:       RdrvUART_Initialize
Parameters:          bgrc
                        Baudrate
Return value:        none.
Description:         Initialize UART settings.
******************************************************************************/
static void
RdrvUART_Initialize(unsigned char brgc)
{
    Rhw_NOP();
    Rhw_NOP();
    Rhw_NOP();
    Rhw_NOP();

    /* port setting */
    UART_CTRL = 0x00;
//  SCI6.SEMR.BYTE = 0x85;      // Enable ACS0, NFEN, RXDESEL for 115200bps mode
    SCI6.SEMR.BYTE = 0xC5;      // Enable BGDM, ACS0, NFEN, RXDESEL for 500000bps mode 

    MPC.PWPR.BYTE = 0x00;           // Enable PWPR register PFSWE bit access
    MPC.PWPR.BYTE = 0x40;           // PFSWE bit = "1" -> Enable PFSregister access
    UART_TX_FUNC = 0x0A;            // Port -> TXD
    UART_RX_FUNC = 0x0A;            // Port -> RXD
    MPC.PWPR.BYTE = 0x80;           // Unlock PWPR register protection
    UART_PORT_SEL = 0x0C;           // Port => Peripheral
                                    // Port -> TXD
                                    // Port -> RXD

    /* clock setting */
    UART_CKE = 0x00;                // PCLK/1 = 48MHz
    UART_MODE = 0x00;               // Clock source of Baudrate Gen = PCLKB( 48[MHz] )
                                    // UART mode
    UART_SMARTCARDMODE = 0x10;      // CHR1 = 1 8bit snd/rcv
//  UART_BITRATE = 10;              // 115200bps mode
    UART_BITRATE = 4;               // 500000bps mode

    /* RXD/TXD(/CTS/RTS) pin setting */
    UART_RX_PM      = PORT_INPUT;
    UART_TX_PM      = PORT_OUTPUT;
    UART_TX_PORT    = PORT_HI;
    UART_RX_PORT_PUP= PORT_PULLUP_ON;
    UART_TX_PORT_PUP= PORT_PULLUP_ON;

    /* UART enable setting */
    UART_TX_ENA = 1;                                    // enable UART transmit
    UART_RX_ENA = 1;                                    // enable UART receive
    UART_TX_IENA = 1;
    UART_RX_IENA = 1;

    /* interrupt priority setting */
    UART_TX_INT_PRI = RP_APL_INTLEVEL_UARTRXs;
    UART_RX_INT_PRI = RP_APL_INTLEVEL_UARTRXs;
    UART_ERR_INT_PRI = RP_APL_INTLEVEL_UARTRXs;

    UART_TX_INT_REQ = 0;                                // clear UART TX interrupt request
    UART_RX_INT_REQ = 0;                                // clear UART RX interrupt request
    UART_ERR_INT_REQ = 0;                               // clear UART error interrupt request

    UART_RX_INT_ENA = 1;                                // enable UART TX interrupt
    UART_ERR_INT_ENA_GP = 1;                            // enable UART error group interrupt
    UART_ERR_INT_ENA = 1;                               // enable UART error interrupt

    RdrvUART_RXBuf_RdPtr = 0x00;                        // Rx Buffer read pointer
    RdrvUART_RXBuf_WrPtr = 0x00;                        // Rx Buffer write pointer
}


/******************************************************************************
Function Name:       RdrvUART_GetChar
Parameters:          none.
Return value:        Received character.
Description:         Get character from UART.
******************************************************************************/
signed short RdrvUART_GetChar(void)
{
    unsigned char   ch;

    if (RdrvUART_RXBuf_RdPtr == RdrvUART_RXBuf_WrPtr)
    {
        return EOF;

    }
    else
    {
        ch = RdrvUART_RXBuf[RdrvUART_RXBuf_RdPtr];
        RdrvUART_RXBuf_RdPtr++;
        if(RdrvUART_RXBuf_RdPtr >= RDRV_UART_RX_BUFSIZ)
        {
            RdrvUART_RXBuf_RdPtr = 0;
        }

        return (long)ch;
    }
}


/******************************************************************************
Function Name:       RdrvUART_PutChar
Parameters:          ch
                       Character to be sent.
Return value:        none.
Description:         Put character to UART.
******************************************************************************/
void RdrvUART_PutChar(unsigned char ch)
{
    /* Wait while Tx register is not empty */
    while((UART_TX_STATUS & ASIF_TXBF)==0);

    /* Send character immediately */
    UART_TX = ch;
}


/******************************************************************************
Function Name:       RdrvUART_GetLen
Parameters:          none.
Return value:        Length of received strings.
Description:         Return Length of received strings.
******************************************************************************/
short
RdrvUART_GetLen(void)
{
    short   len;

    len = (short)RdrvUART_RXBuf_WrPtr - (short)RdrvUART_RXBuf_RdPtr;

    if (len < 0)
    {
        len += RDRV_UART_RX_BUFSIZ;
    }

    return len;
}


/******************************************************************************
Function Name:       RdrvUART_GetTxBufLen
Parameters:          none.
Return value:        Length of received strings.
Description:         Return Length of received strings.
******************************************************************************/
short
RdrvUART_GetTxBufLen(void)
{

    return 0;
}


/******************************************************************************
Function Name:       RdrvSwitch_Initialize
Parameters:          none.
Return value:        none.
Description:         Initialize Switch ports.
******************************************************************************/
static void
RdrvSwitch_Initialize(void)
{
    PORT_PSW0_PM = 0;
    PORT_PSW1_PM = 0;
    PORT_PSW2_PM = 0;
    PORT_PSW3_PM = 0;
    PORT_PSW0_PUP = 1;
    PORT_PSW1_PUP = 1;
    PORT_PSW2_PUP = 1;
    PORT_PSW3_PUP = 1;
}


/******************************************************************************
Function Name:       RdrvSwitch_GetAllConditions
Parameters:          none.
Return value:        0x00
Description:         Get Switch conditions.
******************************************************************************/
unsigned char
RdrvSwitch_GetAllConditions(void)
{
    unsigned char   sw;

    sw = 0;

    return(sw);
}


/******************************************************************************
Function Name:       RdrvSwitch0_GetCondition
Parameters:          none.
Return value:        0 : not pushed
Description:         Get Switch0 condition.
******************************************************************************/
unsigned char
RdrvSwitch0_GetCondition(void)
{
    /* Read Switch SW3-1 of MB-RX604S-02 board (Device Port P40) */
    return(PORT_PSW0_PORT);
}


/******************************************************************************
Function Name:       RdrvSwitch1_GetCondition
Parameters:          none.
Return value:        0 : not pushed
Description:         Get Switch1 condition.
******************************************************************************/
unsigned char
RdrvSwitch1_GetCondition(void)
{
    return(PORT_PSW1_PORT);
}


/******************************************************************************
Function Name:       RdrvSwitch2_GetCondition
Parameters:          none.
Return value:        0 : not pushed
Description:         Get Switch2 condition.
******************************************************************************/
unsigned char
RdrvSwitch2_GetCondition(void)
{
    return(PORT_PSW2_PORT);
}


/******************************************************************************
Function Name:       RdrvSwitch3_GetCondition
Parameters:          none.
Return value:        0 : not pushed
Description:         Get Switch3 condition.
******************************************************************************/
unsigned char
RdrvSwitch3_GetCondition(void)
{
    return(PORT_PSW3_PORT);
}


/******************************************************************************
Function Name:       RdrvSwitch4_GetCondition
Parameters:          none.
Return value:        0 : not pushed
Description:         Get Switch4 condition.
******************************************************************************/
unsigned char
RdrvSwitch4_GetCondition(void)
{
    return(0x00);
}


/******************************************************************************
Function Name:       RdrvSwitch5_GetCondition
Parameters:          none.
Return value:        0 : not pushed
Description:         Get Switch5 condition.
******************************************************************************/
unsigned char
RdrvSwitch5_GetCondition(void)
{
    return(0x00);
}


/******************************************************************************
Function Name:       RdrvSwitch6_GetCondition
Parameters:          none.
Return value:        0 : not pushed
Description:         Get Switch6 condition.
******************************************************************************/
unsigned char
RdrvSwitch6_GetCondition(void)
{
    return(0x00);
}


/******************************************************************************
Function Name:       RdrvSwitch7_GetCondition
Parameters:          none.
Return value:        0 : not pushed
Description:         Get Switch7 condition.
******************************************************************************/
unsigned char
RdrvSwitch7_GetCondition(void)
{
    return(0x00);
}


/******************************************************************************
Function Name:       RdrvLED_Initialize
Parameters:          none.
Return value:        none.
Description:         Initialize LED ports.
******************************************************************************/
void
RdrvLED_Initialize(void)
{
    /* Turn off all LEDs */
    RdrvLED_ALL_OFF();

    /* Set port mode */
    PORT_LED0_PM = PORT_OUTPUT;
    PORT_LED1_PM = PORT_OUTPUT;
    PORT_LED2_PM = PORT_OUTPUT;
    PORT_LED3_PM = PORT_OUTPUT;
}

/******************************************************************************
Function Name:       RdrvLED_ALL_ON
Parameters:          none.
Return value:        none.
Description:         Turn on all LEDs.
******************************************************************************/
void
RdrvLED_ALL_ON(void)
{
    PORT_LED0_PORT = PORT_LO;   /* LED0 on */
    PORT_LED1_PORT = PORT_LO;   /* LED1 on */
    PORT_LED2_PORT = PORT_LO;   /* LED2 on */
    PORT_LED3_PORT = PORT_LO;   /* LED3 on */
}

/******************************************************************************
Function Name:       RdrvLED_ALL_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off all LEDs.
******************************************************************************/
void
RdrvLED_ALL_OFF(void)
{
    PORT_LED0_PORT = PORT_HI;   /* LED0 off */
    PORT_LED1_PORT = PORT_HI;   /* LED1 off */
    PORT_LED2_PORT = PORT_HI;   /* LED2 off */
    PORT_LED3_PORT = PORT_HI;   /* LED3 off */
}

/******************************************************************************
Function Name:       RdrvLED0_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED0.
******************************************************************************/
void
RdrvLED0_ON(void)
{
    PORT_LED0_PORT = PORT_LO;   /* LED0 on */
}


/******************************************************************************
Function Name:       RdrvLED0_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED0.
******************************************************************************/
void
RdrvLED0_OFF(void)
{
    PORT_LED0_PORT = PORT_HI;   /* LED0 off */
}


/******************************************************************************
Function Name:       RdrvLED1_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED1.
******************************************************************************/
void
RdrvLED1_ON(void)
{
    PORT_LED1_PORT = PORT_LO;   /* LED1 on */
}


/******************************************************************************
Function Name:       RdrvLED1_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED1.
******************************************************************************/
void
RdrvLED1_OFF(void)
{
    PORT_LED1_PORT = PORT_HI;   /* LED1 off */
}


/******************************************************************************
Function Name:       RdrvLED2_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED2.
******************************************************************************/
void
RdrvLED2_ON(void)
{
    PORT_LED2_PORT = PORT_LO;   /* LED2 on */
}


/******************************************************************************
Function Name:       RdrvLED2_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED2.
******************************************************************************/
void
RdrvLED2_OFF(void)
{
    PORT_LED2_PORT = PORT_HI;   /* LED2 off */
}


/******************************************************************************
Function Name:       RdrvLED3_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED3.
******************************************************************************/
void
RdrvLED3_ON(void)
{
    PORT_LED3_PORT = PORT_LO;   /* LED3 on */
}


/******************************************************************************
Function Name:       RdrvLED3_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED3.
******************************************************************************/
void
RdrvLED3_OFF(void)
{
    PORT_LED3_PORT = PORT_HI;   /* LED3 off */
}


/******************************************************************************
Function Name:       RdrvLED4_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED4.
******************************************************************************/
void
RdrvLED4_ON(void)
{
    /* LED0 ~ 3 only */
}


/******************************************************************************
Function Name:       RdrvLED4_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED4.
******************************************************************************/
void
RdrvLED4_OFF(void)
{
    /* LED0 ~ 3 only */
}


/******************************************************************************
Function Name:       RdrvLED5_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED5.
******************************************************************************/
void
RdrvLED5_ON(void)
{
    /* LED0 ~ 3 only */
}


/******************************************************************************
Function Name:       RdrvLED5_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED5.
******************************************************************************/
void
RdrvLED5_OFF(void)
{
    /* LED0 ~ 3 only */
}


/******************************************************************************
Function Name:       RdrvLED6_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED6.
******************************************************************************/
void
RdrvLED6_ON(void)
{
    /* LED0 ~ 3 only */
}


/******************************************************************************
Function Name:       RdrvLED6_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED6.
******************************************************************************/
void
RdrvLED6_OFF(void)
{
    /* LED0 ~ 3 only */
}


/******************************************************************************
Function Name:       RdrvLED7_ON
Parameters:          none.
Return value:        none.
Description:         Turn on LED7.
******************************************************************************/
void
RdrvLED7_ON(void)
{
    /* LED0 ~ 3 only */
}


/******************************************************************************
Function Name:       RdrvLED7_OFF
Parameters:          none.
Return value:        none.
Description:         Turn off LED7.
******************************************************************************/
void
RdrvLED7_OFF(void)
{
    /* LED0 ~ 3 only */
}

/******************************************************************************
Function Name:       RdrvLED0_GetCondition
Parameters:          none.
Return value:        1 : On
                     0 : Off
Description:         Get LED0 condition.
******************************************************************************/
unsigned char
RdrvLED0_GetCondition(void)
{
    unsigned char   ret;

    ret = 0;

    if(PORT_LED0_PORT == PORT_LO)
    {
        ret = 0x01;
    }

    return(ret);
}


/******************************************************************************
Function Name:       RdrvLED1_GetCondition
Parameters:          none.
Return value:        1 : On
                     0 : Off
Description:         Get LED1 condition.
******************************************************************************/
unsigned char
RdrvLED1_GetCondition(void)
{
    unsigned char   ret;

    ret = 0;

    if(PORT_LED1_PORT == PORT_LO)
    {
        ret = 0x01;
    }

    return(ret);
}


/******************************************************************************
Function Name:       RdrvLED2_GetCondition
Parameters:          none.
Return value:        1 : On
                     0 : Off
Description:         Get LED2 condition.
******************************************************************************/
unsigned char
RdrvLED2_GetCondition(void)
{
    unsigned char   ret;

    ret = 0;

    if(PORT_LED2_PORT == PORT_LO)
    {
        ret = 0x01;
    }

    return(ret);
}


/******************************************************************************
Function Name:       RdrvLED3_GetCondition
Parameters:          none.
Return value:        0 : Off
Description:         Get LED3 condition.
******************************************************************************/
unsigned char
RdrvLED3_GetCondition(void)
{
    unsigned char   ret;

    ret = 0;

    if(PORT_LED3_PORT == PORT_LO)
    {
        ret = 0x01;
    }

    return(ret);
}


/******************************************************************************
Function Name:       RdrvLED4_GetCondition
Parameters:          none.
Return value:        0 : Off
Description:         Get LED4 condition.
******************************************************************************/
unsigned char
RdrvLED4_GetCondition(void)
{
    /* LED0 ~ 3 only */
    return(0x00);
}


/******************************************************************************
Function Name:       RdrvLED5_GetCondition
Parameters:          none.
Return value:        0 : Off
Description:         Get LED5 condition.
******************************************************************************/
unsigned char
RdrvLED5_GetCondition(void)
{
    /* LED0 ~ 3 only */
    return(0x00);
}


/******************************************************************************
Function Name:       RdrvLED6_GetCondition
Parameters:          none.
Return value:        0 : Off
Description:         Get LED6 condition.
******************************************************************************/
unsigned char
RdrvLED6_GetCondition(void)
{
    /* LED0 ~ 3 only */
    return(0x00);
}


/******************************************************************************
Function Name:       RdrvLED7_GetCondition
Parameters:          none.
Return value:        0 : Off
Description:         Get LED7 condition.
******************************************************************************/
unsigned char
RdrvLED7_GetCondition(void)
{
    /* LED0 ~ 3 only */
    return(0x00);
}

/******************************************************************************
Function Name:       RdrvFREQHOPPTIMER_Initialize
Parameters:          none
Return value:        none
Description:         Initialize TPU3 to generate 1ms timer interval
******************************************************************************/
void RdrvFREQHOPPTIMER_Initialize(void)
{
    TPU3.TCR.BIT.TPSC = 0x00;      /* Set TPU3 as 16 bit counter clocked by 40MHz/1 = 25[nsec] */
    TPU3.TCNT = 0x0000;            /* TPU3 TGI3A (TGRAI:Input capture/Compare match) */
    TPU3.TGRA = 40000;             /* 1ms */
    TPU3.TCR.BIT.CCLR = 0x1;       /* TRGA compare clear */
    TPU3.TMDR.BYTE = 0x00;         /* normal */

    ICU.SLIBXR142.BYTE = 28;       
    IR(PERIB,INTB142) = 0;
    IPR(PERIB,INTB142) = RP_MAC_INTLEVEL_FREQHOPPTimer;
    IEN(PERIB,INTB142) = 1;
    
    TPU3.TIER.BIT.TGIEA = 1;
    TPUA.TSTR.BIT.CST3 = 1;
}

/******************************************************************************
Function Name:       RdrvFREQHOPPTIMER_Stop
Parameters:          none
Return value:        none
Description:         Stop TPU10 timer operation
******************************************************************************/
void RdrvFREQHOPPTIMER_Stop(void)
{
    TPUA.TSTR.BIT.CST3 = 0;
}

/******************************************************************************
Function Name:       RdrvUART_RxInt
Parameters:          none.
Return value:        none.
Description:         UART Rx interrupt handler.
******************************************************************************/
#pragma interrupt RdrvUART_RxInt(save, vect=VECT(SCI6,RXI6))

void RdrvUART_RxInt(void)
{
#if R_MODEM_SERVER || R_MODEM_CLIENT
#ifdef R_HYBRID_PLC_RF
    r_modem_rx(UART_RX);
#else
    extern r_modem_ctx g_modem_nwk_ctx;
    r_modem_rx(UART_RX, &g_modem_nwk_ctx);
#endif
#else
    volatile unsigned char      ch;
    short                       len;

    /* Read character from UART */
    ch = UART_RX;

    /* Get number of characters in buffer */
    len = (short)RdrvUART_RXBuf_WrPtr - (short)RdrvUART_RXBuf_RdPtr;
    if(len < 0)
    {
        len += RDRV_UART_RX_BUFSIZ;
    }

    /* Buffer overflow */
    if(len > RDRV_UART_RX_BUFSIZ)
    {
        return;
    }

    /* Update pointer and Store character to buffer */
    RdrvUART_RXBuf[RdrvUART_RXBuf_WrPtr] = ch;

    RdrvUART_RXBuf_WrPtr++;

    if(RdrvUART_RXBuf_WrPtr >= RDRV_UART_RX_BUFSIZ)
    {
        RdrvUART_RXBuf_WrPtr = 0;
    }
#endif /* R_MODEM_SERVER || R_MODEM_CLIENT */
}

#if !R_MODEM_CLIENT
/******************************************************************************
Function Name:       RdrvTPU3_FreqHoppInt
Parameters:          none.
Return value:        none.
Description:         TPU3 interrupt handler.
******************************************************************************/
#pragma interrupt RdrvTPU3_FreqHoppInt(save, vect=VECT(PERIB,INTB142))

void RdrvTPU3_FreqHoppInt(void)
{
    setpsw_i();     // allow responsive of serial API 

    RmFreqHoppTimer_Handler(R_FREQ_HOPP_TIMER_INTERVAL_MS);

}
#endif

/******************************************************************************
Function Name:       Rdrv_ICU_GROUPBL0_INT
Parameters:          none.
Return value:        none.
Description:         GROUPBL0 interrupt handler
******************************************************************************/
#pragma interrupt Rdrv_ICU_GROUPBL0_INT(save, vect=VECT(ICU,GROUPBL0))
void Rdrv_ICU_GROUPBL0_INT(void)
{
    /* BL0 IS13 */
    if (1 == ICU.GRPBL0.BIT.IS13)
    {
        /* UART RxErr interrupt handler */
        RdrvUART_RxErrInt();
    }
    /* BL0 IS16 */
    if (1 == ICU.GRPBL0.BIT.IS16)
    {
#if R_FLASH_ENABLED == 2
        /* SCI12 TEI12 interrupt handler */
        sci_iic_int_sci_iic12_tei_isr();
#endif
    }
}

/******************************************************************************
Function Name:       RdrvUART_RxErrInt
Parameters:          none.
Return value:        none.
Description:         UART RxErr interrupt handler.
******************************************************************************/
void RdrvUART_RxErrInt(void)
{
    volatile unsigned char      ch;

    /* Read status */
    ch = (unsigned char)UART_RX_STATUS;
    if (ch & (ASIS_PE | ASIS_FE | ASIS_OVE))
    {
        /* Error -> read dummy data then Exit */
        ch = UART_RX;
        ch = UART_RX;

        if(UART_RX_STATUS_PARTYERR)
        {
            /* clear parity error */
            UART_RX_STATUS_PARTYERR = 0;
        }
        if(UART_RX_STATUS_FRAMEERR)
        {
            /* clear framing error */
            UART_RX_STATUS_FRAMEERR = 0;
        }
        if(UART_RX_STATUS_OVERRUNERR)
        {
            /* clear overrun error */
            UART_RX_STATUS_OVERRUNERR = 0;
        }
    }
}

#if 1
unsigned char charget(void)
{
    return RdrvUART_GetChar();
}

void charput(unsigned char c)
{
    RdrvUART_PutChar(c);
}
#endif



/*****************************************************************
    Copyright (C) 2014 Renesas Electronics Corporation 
    and Renesas Solutions Corp. All rights reserved.
******************************************************************/

