/***********************************************************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No 
* other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all 
* applicable laws, including copyright laws. 
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, 
* FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM 
* EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES 
* SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO THIS 
* SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of 
* this software. By using this software, you agree to the additional terms and conditions found by accessing the 
* following link:
* http://www.renesas.com/disclaimer 
*
* Copyright (C) 2016 Renesas Electronics Corporation. All rights reserved.    
***********************************************************************************************************************/
/***********************************************************************************************************************
* File Name    : vecttbl.c
* Device(s)    : RX65N
* Description  : Definition of the exception vector table, reset vector, and user boot options.
***********************************************************************************************************************/

/***********************************************************************************************************************
Includes   <System Includes> , "Project Includes"
***********************************************************************************************************************/
/* platform configuration */
#include "../../../platform/mb_rx604s_02_rx651/platform.h"
#include "../../../platform/mb_rx604s_02_rx651/hardware.h"
#include "../../../platform/mb_rx604s_02_rx651/MB_RXRAA604S00.h"

/***********************************************************************************************************************
Macro definitions
***********************************************************************************************************************/
/* Defines CV, CO, CZ, CU, CX, and CE bits. */
#define FPU_CAUSE_FLAGS     (0x000000FC)

/***********************************************************************************************************************
* Function name: PowerON_Reset_PC
* Description  : The reset vector points to this function.  Code execution starts in this function after reset.
* Arguments    : none
* Return value : none
***********************************************************************************************************************/
extern void PowerON_Reset_PC(void);                           

extern void Software_Reset(void);

/***********************************************************************************************************************
* Function name: excep_supervisor_inst_isr
* Description  : Supervisor Instruction Violation ISR
* Arguments    : none
* Return Value : none
***********************************************************************************************************************/
#pragma interrupt (excep_supervisor_inst_isr)
void excep_supervisor_inst_isr(void)
{
    /* insert user code here. */

}

/***********************************************************************************************************************
* Function name: excep_access_isr
* Description  : Access exception ISR
* Arguments    : none
* Return Value : none
***********************************************************************************************************************/
#pragma interrupt (excep_access_isr)
void excep_access_isr(void)
{
    /* insert user code here. */

}

/***********************************************************************************************************************
* Function name: excep_undefined_inst_isr
* Description  : Undefined instruction exception ISR
* Arguments    : none
* Return Value : none
***********************************************************************************************************************/
#pragma interrupt (excep_undefined_inst_isr)
void excep_undefined_inst_isr(void)
{
    /* insert user code here. */
	static uint16_t i;
	static uint8_t test = 0;

#ifdef R_DEV_AUTO_START
	Software_Reset();
#endif

	while(1)
	{
	    PORT_LED0_PORT = test;
	    PORT_LED1_PORT = test;
	    PORT_LED2_PORT = test;
	    PORT_LED3_PORT = test;
	    /* wait a little */
		for (i = 0; i < 65535; i++)
		{
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
		}
		test = ~test;
	}
}

/***********************************************************************************************************************
* Function name: excep_floating_point_isr
* Description  : Floating point exception ISR
* Arguments    : none
* Return Value : none
***********************************************************************************************************************/
#pragma interrupt (excep_floating_point_isr)
void excep_floating_point_isr(void)
{
    /* Used for reading FPSW register. */
    uint32_t temp_fpsw;

    /* insert user code here. */


    /* Get current FPSW. */
    temp_fpsw = (uint32_t)get_fpsw();
    /* Clear only the FPU exception flags. */
    set_fpsw(temp_fpsw & ((uint32_t)~FPU_CAUSE_FLAGS));
}

/***********************************************************************************************************************
* Function name: non_maskable_isr
* Description  : Non-maskable interrupt ISR
* Arguments    : none
* Return Value : none
***********************************************************************************************************************/
#pragma interrupt (non_maskable_isr)
void non_maskable_isr(void)
{
    /* Determine what is the cause of this interrupt. */
    if (1 == ICU.NMISR.BIT.NMIST)
    {
        /* NMI pin interrupt is requested. */
        /* insert user code here. */


        /* Clear NMI pin interrupt flag. */
        ICU.NMICLR.BIT.NMICLR = 1;
    }

    if (1 == ICU.NMISR.BIT.OSTST)
    {
        /* Oscillation stop detection interrupt is requested. */
        /* insert user code here. */


        /* Clear oscillation stop detect flag. */
        ICU.NMICLR.BIT.OSTCLR = 1;
    }

    if (1 == ICU.NMISR.BIT.WDTST)
    {
        /* WDT underflow/refresh error interrupt is requested. */
        /* insert user code here. */


        /* Clear WDT flag. */
        ICU.NMICLR.BIT.WDTCLR = 1;
    }

    if (1 == ICU.NMISR.BIT.IWDTST)
    {
        /* IWDT underflow/refresh error interrupt is requested. */
        /* insert user code here. */


        /* Clear IWDT flag. */
        ICU.NMICLR.BIT.IWDTCLR = 1;
    }

    if (1 == ICU.NMISR.BIT.LVD1ST)
    {
        /* Voltage monitoring 1 interrupt is requested. */
        /* insert user code here. */


        /* Clear LVD1 flag. */
        ICU.NMICLR.BIT.LVD1CLR = 1;
    }

    if (1 == ICU.NMISR.BIT.LVD2ST)
    {
        /* Voltage monitoring 1 interrupt is requested. */
        /* insert user code here. */


        /* Clear LVD2 flag. */
        ICU.NMICLR.BIT.LVD2CLR = 1;
    }

    if (1 == ICU.NMISR.BIT.RAMST)
    {
        if(1 == RAM.RAMSTS.BIT.RAMERR)
        {
            /* RAM Error interrupt is requested. */
            /* insert user code here. */


            /* Clear RAM flags. */
            RAM.RAMSTS.BIT.RAMERR = 0;
        }

        if(1 == RAM.EXRAMSTS.BIT.EXRAMERR)
        {
            /* Expansion RAM Error interrupt is requested. */
            /* insert user code here. */


            /* Clear Expansion RAM flags. */
            RAM.EXRAMSTS.BIT.EXRAMERR = 0;
        }
    }
}

/***********************************************************************************************************************
* Function name: undefined_interrupt_source_isr
* Description  : All undefined interrupt vectors point to this function.
*                Set a breakpoint in this function to determine which source is creating unwanted interrupts.
* Arguments    : none
* Return Value : none
***********************************************************************************************************************/
#pragma interrupt (undefined_interrupt_source_isr)
void undefined_interrupt_source_isr(void)
{
    /* insert user code here. */

}

/***********************************************************************************************************************
* Function name: bus_error_isr
* Description  : By default, this demo code enables the Bus Error Interrupt. This interrupt will fire if the user tries 
*                to access code or data from one of the reserved areas in the memory map, including the areas covered 
*                by disabled chip selects. A nop() statement is included here as a convenient place to set a breakpoint 
*                during debugging and development, and further handling should be added by the user for their 
*                application.
* Arguments    : none
* Return value : none
***********************************************************************************************************************/
#pragma interrupt (bus_error_isr(vect=VECT(BSC,BUSERR)))
void bus_error_isr (void)
{
    /* Clear the bus error */
    BSC.BERCLR.BIT.STSCLR = 1;

    /* 
        To find the address that was accessed when the bus error occurred, read the register BSC.BERSR2.WORD.  The upper
        13 bits of this register contain the upper 13-bits of the offending address (in 512K byte units)
    */

    /* insert user code here. */

}

/***********************************************************************************************************************
* The following array fills in the UB codes to get into User Boot Mode, the MDEB register, and the User Boot reset
* vector.
***********************************************************************************************************************/
#pragma address __SPCCreg=0xFE7F5D40           /* SPCC register */
const unsigned long __SPCCreg = 0xffffffff;

#pragma address __TMEFreg=0xFE7F5D48           /* TMEF register */
const unsigned long __TMEFreg = 0xffffffff;

#pragma address __OSISreg=0xFE7F5D50           /* OSIS register (ID codes) */
const unsigned long __OSISreg[4] = {
        0xffffffff,
        0xffffffff,
        0xffffffff,
        0xffffffff,
};

#pragma address __TMINFreg=0xFE7F5D10          /* TMINF register */
const unsigned long __TMINFreg = 0xffffffff;

#ifdef __BIG
    #define MDE_VALUE (0xfffffff8)    /* big */
#else
    #define MDE_VALUE (0xffffffff)    /* little */
#endif

#pragma address __MDEreg=0xFE7F5D00             /* MDE register (Single Chip Mode) */
const unsigned long __MDEreg = MDE_VALUE;

#pragma address __OFS0reg=0xFE7F5D04            /* OFS0 register */
const unsigned long __OFS0reg = 0xFFFFFFFF;

#pragma address __OFS1reg=0xFE7F5D08            /* OFS1 register */
const unsigned long __OFS1reg = 0xFFFFFFFF;

#pragma address __FAWreg=0xFE7F5D64             /* FAW register */
const unsigned long __FAWreg = 0xFFFFFFFF;

#pragma address __ROMCODEreg=0xFE7F5D70         /* ROMCODE register */
const unsigned long __ROMCODEreg = 0xFFFFFFFF;


/***********************************************************************************************************************
* Function name: Break_Trap
* Description  : Break instruction ISR
* Arguments    : none
* Return Value : none
***********************************************************************************************************************/
#pragma interrupt Break_Trap(save, vect=0)
void Break_Trap(void)
{
	/* insert user code here. */
	static uint16_t i;
	static uint8_t test = 0;

#ifdef R_DEV_AUTO_START
	Software_Reset();
#endif

	while(1)
	{
		PORT_LED0_PORT = test;
		PORT_LED1_PORT = test;
		PORT_LED2_PORT = test;
		PORT_LED3_PORT = test;
		/* wait a little */
		for (i = 0; i < 65535; i++)
		{
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
			nop(); nop(); nop(); nop();
		}
		test++;
	}
}


/***********************************************************************************************************************
* Common unused interrupt / exception handler
***********************************************************************************************************************/
static unsigned int unused_isr_count = 0;
void unused_isr(void)
{
	unused_isr_count++;
}

/***********************************************************************************************************************
* Definition of unused interrupts / exceptions
***********************************************************************************************************************/
//#pragma interrupt unused_isr_0(save, vect=0)			/*-> Unconditional Trap  0: void Break_Trap(void)	*/
#pragma interrupt unused_isr_1(save, vect=1)
#pragma interrupt unused_isr_2(save, vect=2)
#pragma interrupt unused_isr_3(save, vect=3)
#pragma interrupt unused_isr_4(save, vect=4)
#pragma interrupt unused_isr_5(save, vect=5)
#pragma interrupt unused_isr_6(save, vect=6)
#pragma interrupt unused_isr_7(save, vect=7)
#pragma interrupt unused_isr_8(save, vect=8)
//#pragma interrupt unused_isr_9(save, vect=9)			/*-> Unconditional Trap  9: void _DI_Int(void)		*/
//#pragma interrupt unused_isr_10(save, vect=10)		/*-> Unconditional Trap 10: void _IPL_Int(void)		*/
//#pragma interrupt unused_isr_11(save, vect=11)		/*-> Unconditional Trap 10: void _UM_Int(void)		*/
//#pragma interrupt unused_isr_12(save, vect=12)		/*-> Unconditional Trap 11: void _IPL_0_Int(void)	*/
#pragma interrupt unused_isr_13(save, vect=13)
#pragma interrupt unused_isr_14(save, vect=14)
#pragma interrupt unused_isr_15(save, vect=15)
//#pragma interrupt unused_isr_16(save, vect=16)		/*-> BSC/BUSERR: void bus_error_isr(void) 			*/
#pragma interrupt unused_isr_17(save, vect=17)
#pragma interrupt unused_isr_18(save, vect=18)
#pragma interrupt unused_isr_19(save, vect=19)
#pragma interrupt unused_isr_20(save, vect=20)                  
//#pragma interrupt unused_isr_21(save, vect=21)		/*-> FCU/FIFERR (for FLASH programming): void Excep_FCU_FIFERR(void)		*/
#pragma interrupt unused_isr_22(save, vect=22)                  
//#pragma interrupt unused_isr_23(save, vect=23)        /*-> FCU/FRDYI (for FLASH programming): void Excep_FCU_FRDYI(void)			*/
#pragma interrupt unused_isr_24(save, vect=24)                  
#pragma interrupt unused_isr_25(save, vect=25)                  
#pragma interrupt unused_isr_26(save, vect=26)                  
//#pragma interrupt unused_isr_27(save, vect=27)		/*-> ICU/SWINT (for FreeRTOS): void vSoftwareInterruptISR(void) 	*/
//#pragma interrupt unused_isr_28(save, vect=28)		/*-> CMT0/CMI0  (for FreeRTOS): void vTickISR(void) 					*/
#pragma interrupt unused_isr_29(save, vect=29)                  
#if !R_PHY_TYPE_CWX_M
#pragma interrupt unused_isr_30(save, vect=30)          /* for CWX-M -> r_drv_cmwi0_interrupt(vect=30) */
#pragma interrupt unused_isr_31(save, vect=31)          /* for CWX-M -> r_drv_cmwi1_interrupt(vect=31) */
#endif
#pragma interrupt unused_isr_32(save, vect=32)                  
#pragma interrupt unused_isr_33(save, vect=33)                  
#pragma interrupt unused_isr_34(save, vect=34)                  
#pragma interrupt unused_isr_35(save, vect=35)                  
#pragma interrupt unused_isr_36(save, vect=36)                  
#pragma interrupt unused_isr_37(save, vect=37)                  
#pragma interrupt unused_isr_38(save, vect=38)                  
#pragma interrupt unused_isr_39(save, vect=39)            
#pragma interrupt unused_isr_40(save, vect=40)                  
#pragma interrupt unused_isr_41(save, vect=41)                  
#pragma interrupt unused_isr_42(save, vect=42)                  
#pragma interrupt unused_isr_43(save, vect=43)                  
#pragma interrupt unused_isr_44(save, vect=44)                  
#pragma interrupt unused_isr_45(save, vect=45)                  
#pragma interrupt unused_isr_46(save, vect=46)                  
#pragma interrupt unused_isr_47(save, vect=47)                  
#pragma interrupt unused_isr_48(save, vect=48)                  
#pragma interrupt unused_isr_49(save, vect=49)            
#pragma interrupt unused_isr_50(save, vect=50)                  
#pragma interrupt unused_isr_51(save, vect=51)                  
#pragma interrupt unused_isr_52(save, vect=52)                  
#pragma interrupt unused_isr_53(save, vect=53)                  
#pragma interrupt unused_isr_54(save, vect=54)                  
#pragma interrupt unused_isr_55(save, vect=55)                  
#pragma interrupt unused_isr_56(save, vect=56)                  
#pragma interrupt unused_isr_57(save, vect=57)                  
#pragma interrupt unused_isr_58(save, vect=58)                  
#pragma interrupt unused_isr_59(save, vect=59)            
#pragma interrupt unused_isr_60(save, vect=60)                  
#pragma interrupt unused_isr_61(save, vect=61)                  
#pragma interrupt unused_isr_62(save, vect=62)                  
#pragma interrupt unused_isr_63(save, vect=63)                  
#pragma interrupt unused_isr_64(save, vect=64)                  
#pragma interrupt unused_isr_65(save, vect=65)                  
#pragma interrupt unused_isr_66(save, vect=66)                  
#pragma interrupt unused_isr_67(save, vect=67)                  
#pragma interrupt unused_isr_68(save, vect=68)                  
#if !R_PHY_TYPE_CWX_M
#pragma interrupt unused_isr_69(save, vect=69)          /* for CWX-M -> r_cwx_intout0_interrupt(vect=69) */
#pragma interrupt unused_isr_70(save, vect=70)          /* for CWX-M -> r_cwx_intout1_interrupt(vect=70) */
#pragma interrupt unused_isr_71(save, vect=71)          /* for CWX-M -> r_cwx_intout2_interrupt(vect=71) */
#endif /* !R_PHY_TYPE_CWX_M */
#pragma interrupt unused_isr_72(save, vect=72)
#pragma interrupt unused_isr_73(save, vect=73)
//#pragma interrupt unused_isr_74(save, vect=74)		/*-> ICU/IRQ10 : void INTPx_Interrupt(void)				*/
#pragma interrupt unused_isr_75(save, vect=75)                  
#pragma interrupt unused_isr_76(save, vect=76)                  
#pragma interrupt unused_isr_77(save, vect=77)                  
#pragma interrupt unused_isr_78(save, vect=78)                  
#pragma interrupt unused_isr_79(save, vect=79)     
#pragma interrupt unused_isr_80(save, vect=80)                  
#pragma interrupt unused_isr_81(save, vect=81)                  
#pragma interrupt unused_isr_82(save, vect=82)                  
#pragma interrupt unused_isr_83(save, vect=83)                  
#pragma interrupt unused_isr_84(save, vect=84)                  
#pragma interrupt unused_isr_85(save, vect=85)                  
//#pragma interrupt unused_isr_86(save, vect=86)		/*-> SCI6/RXI6 : void RdrvUART_RxInt(void)				*/
#pragma interrupt unused_isr_87(save, vect=87)                  
#pragma interrupt unused_isr_88(save, vect=88)                  
#pragma interrupt unused_isr_89(save, vect=89)     
#pragma interrupt unused_isr_90(save, vect=90)                  
#pragma interrupt unused_isr_91(save, vect=91)                  
#pragma interrupt unused_isr_92(save, vect=92)                  
#pragma interrupt unused_isr_93(save, vect=93)                  
#pragma interrupt unused_isr_94(save, vect=94)                  
#pragma interrupt unused_isr_95(save, vect=95)                  
#pragma interrupt unused_isr_96(save, vect=96)                  
#pragma interrupt unused_isr_97(save, vect=97)                  
#pragma interrupt unused_isr_98(save, vect=98)                  
#pragma interrupt unused_isr_99(save, vect=99)     
#ifndef R_HYBRID_PLC_RF
#pragma interrupt unused_isr_100(save, vect=100)
#pragma interrupt unused_isr_101(save, vect=101)
#endif
#pragma interrupt unused_isr_102(save, vect=102)
#pragma interrupt unused_isr_103(save, vect=103)
#pragma interrupt unused_isr_104(save, vect=104)
#pragma interrupt unused_isr_105(save, vect=105)
#pragma interrupt unused_isr_106(save, vect=106)
#pragma interrupt unused_isr_107(save, vect=107)
#pragma interrupt unused_isr_108(save, vect=108)
#pragma interrupt unused_isr_109(save, vect=109)
//#pragma interrupt unused_isr_110(save, vect=110)      /*-> ICU/GROUPBL0: void Rdrv_ICU_GROUPBL0_INT(void)		*/
#ifndef R_HYBRID_PLC_RF
#pragma interrupt unused_isr_111(save, vect=111)        /*-> ICU/GROUPBL1: void R_EXCEP_IcuGroup12(void) */
#endif
#pragma interrupt unused_isr_112(save, vect=112)
#pragma interrupt unused_isr_113(save, vect=113)
#pragma interrupt unused_isr_114(save, vect=114)
#pragma interrupt unused_isr_115(save, vect=115)
#pragma interrupt unused_isr_116(save, vect=116)
#if R_FLASH_ENABLED != 2
#pragma interrupt unused_isr_117(save, vect=117)        /*-> SCI12/TXI12: void sci_iic_int_sci_iic12_txi_isr(void)        */
#endif
#pragma interrupt unused_isr_118(save, vect=118)
#pragma interrupt unused_isr_119(save, vect=119)
#pragma interrupt unused_isr_120(save, vect=120)                  
#pragma interrupt unused_isr_121(save, vect=121)                  
#pragma interrupt unused_isr_122(save, vect=122)                  
#pragma interrupt unused_isr_123(save, vect=123)                  
#pragma interrupt unused_isr_124(save, vect=124)                  
#pragma interrupt unused_isr_125(save, vect=125)                  
#pragma interrupt unused_isr_126(save, vect=126)                  
#pragma interrupt unused_isr_127(save, vect=127)
#pragma interrupt unused_isr_128(save, vect=128)
#pragma interrupt unused_isr_129(save, vect=129)                  
#pragma interrupt unused_isr_130(save, vect=130)                  
#pragma interrupt unused_isr_131(save, vect=131)                  
#pragma interrupt unused_isr_132(save, vect=132)                  
#pragma interrupt unused_isr_133(save, vect=133)                  
#pragma interrupt unused_isr_134(save, vect=134)                  
#pragma interrupt unused_isr_135(save, vect=135)                  
#pragma interrupt unused_isr_136(save, vect=136)                  
#pragma interrupt unused_isr_137(save, vect=137)                  
//#pragma interrupt unused_isr_138(save, vect=138)		/*-> TPU2/TGI2A: void RpWupTskHdr(void)				*/
#pragma interrupt unused_isr_139(save, vect=139)            
#pragma interrupt unused_isr_140(save, vect=140)                  
#pragma interrupt unused_isr_141(save, vect=141)                  
//#pragma interrupt unused_isr_142(save, vect=142)		/*-> TPU3/TGI3A: void RdrvTPU3_FreqHoppInt(void)	*/
#pragma interrupt unused_isr_143(save, vect=143)                  
#pragma interrupt unused_isr_144(save, vect=144)                  
#pragma interrupt unused_isr_145(save, vect=145)                  
#ifndef R_HYBRID_PLC_RF
#pragma interrupt unused_isr_146(save, vect=146)                  
#endif                
#pragma interrupt unused_isr_147(save, vect=147)                  
#pragma interrupt unused_isr_148(save, vect=148)                  
#pragma interrupt unused_isr_149(save, vect=149)            
#pragma interrupt unused_isr_150(save, vect=150)                  
#pragma interrupt unused_isr_151(save, vect=151)                  
#pragma interrupt unused_isr_152(save, vect=152)                  
#pragma interrupt unused_isr_153(save, vect=153)                  
#pragma interrupt unused_isr_154(save, vect=154)                  
#pragma interrupt unused_isr_155(save, vect=155)                  
#pragma interrupt unused_isr_156(save, vect=156)                  
#pragma interrupt unused_isr_157(save, vect=157)                  
#pragma interrupt unused_isr_158(save, vect=158)                  
#pragma interrupt unused_isr_159(save, vect=159)            
//#pragma interrupt unused_isr_160(save, vect=160)		/-> TPU4/TGI4A:  void INTTM04_Interrupt(void);		*/
#pragma interrupt unused_isr_161(save, vect=161)                  
#pragma interrupt unused_isr_162(save, vect=162)                  
#pragma interrupt unused_isr_163(save, vect=163)                  
#pragma interrupt unused_isr_164(save, vect=164)                  
#pragma interrupt unused_isr_165(save, vect=165)                  
#pragma interrupt unused_isr_166(save, vect=166)                  
#pragma interrupt unused_isr_167(save, vect=167)                  
#pragma interrupt unused_isr_168(save, vect=168)                  
#pragma interrupt unused_isr_169(save, vect=169)
#pragma interrupt unused_isr_170(save, vect=170)                  
#pragma interrupt unused_isr_171(save, vect=171)                  
#pragma interrupt unused_isr_172(save, vect=172)                  
#pragma interrupt unused_isr_173(save, vect=173)                  
#pragma interrupt unused_isr_174(save, vect=174)
#pragma interrupt unused_isr_175(save, vect=175)                  
#pragma interrupt unused_isr_176(save, vect=176)                  
#pragma interrupt unused_isr_177(save, vect=177)                  
#pragma interrupt unused_isr_178(save, vect=178)                  
#pragma interrupt unused_isr_179(save, vect=179)     
#pragma interrupt unused_isr_180(save, vect=180)                  
#pragma interrupt unused_isr_181(save, vect=181)                  
#pragma interrupt unused_isr_182(save, vect=182)                  
#pragma interrupt unused_isr_183(save, vect=183)                  
#pragma interrupt unused_isr_184(save, vect=184)                  
#pragma interrupt unused_isr_185(save, vect=185)                  
#pragma interrupt unused_isr_186(save, vect=186)
#pragma interrupt unused_isr_187(save, vect=187)                  
#pragma interrupt unused_isr_188(save, vect=188)                  
#pragma interrupt unused_isr_189(save, vect=189)     
#pragma interrupt unused_isr_190(save, vect=190)                  
#pragma interrupt unused_isr_191(save, vect=191)                  
#pragma interrupt unused_isr_192(save, vect=192)                  
#pragma interrupt unused_isr_193(save, vect=193)                  
#pragma interrupt unused_isr_194(save, vect=194)                  
#pragma interrupt unused_isr_195(save, vect=195)                  
#pragma interrupt unused_isr_196(save, vect=196)                  
#pragma interrupt unused_isr_197(save, vect=197)                  
//#pragma interrupt unused_isr_198(save, vect=198)		/*-> DMAC/DMAC0I: void INTDMA0_Interrupt(void)		*/
#pragma interrupt unused_isr_199(save, vect=199)     
#pragma interrupt unused_isr_200(save, vect=200)
#pragma interrupt unused_isr_201(save, vect=201)
#pragma interrupt unused_isr_202(save, vect=202)
#pragma interrupt unused_isr_203(save, vect=203)
#pragma interrupt unused_isr_204(save, vect=204)
#pragma interrupt unused_isr_205(save, vect=205)
#pragma interrupt unused_isr_206(save, vect=206)
#pragma interrupt unused_isr_207(save, vect=207)
#pragma interrupt unused_isr_208(save, vect=208)
#pragma interrupt unused_isr_209(save, vect=209)
#pragma interrupt unused_isr_210(save, vect=210)
#pragma interrupt unused_isr_211(save, vect=211)
#pragma interrupt unused_isr_212(save, vect=212)
#pragma interrupt unused_isr_213(save, vect=213)
#pragma interrupt unused_isr_214(save, vect=214)
#pragma interrupt unused_isr_215(save, vect=215)
#pragma interrupt unused_isr_216(save, vect=216)
#pragma interrupt unused_isr_217(save, vect=217)
#pragma interrupt unused_isr_218(save, vect=218)
#pragma interrupt unused_isr_219(save, vect=219)
#pragma interrupt unused_isr_220(save, vect=220)
#pragma interrupt unused_isr_221(save, vect=221)
#pragma interrupt unused_isr_222(save, vect=222)
#pragma interrupt unused_isr_223(save, vect=223)
#pragma interrupt unused_isr_224(save, vect=224)
#pragma interrupt unused_isr_225(save, vect=225)
#pragma interrupt unused_isr_226(save, vect=226)
#pragma interrupt unused_isr_227(save, vect=227)
#pragma interrupt unused_isr_228(save, vect=228)
#pragma interrupt unused_isr_229(save, vect=229)
#pragma interrupt unused_isr_230(save, vect=230)
#pragma interrupt unused_isr_231(save, vect=231)
#pragma interrupt unused_isr_232(save, vect=232)
#pragma interrupt unused_isr_233(save, vect=233)
#pragma interrupt unused_isr_234(save, vect=234)
#pragma interrupt unused_isr_235(save, vect=235)
#pragma interrupt unused_isr_236(save, vect=236)
#pragma interrupt unused_isr_237(save, vect=237)
#pragma interrupt unused_isr_238(save, vect=238)
#pragma interrupt unused_isr_239(save, vect=239)
#pragma interrupt unused_isr_240(save, vect=240)
#pragma interrupt unused_isr_241(save, vect=241)
#pragma interrupt unused_isr_242(save, vect=242)
#pragma interrupt unused_isr_243(save, vect=243)
#pragma interrupt unused_isr_244(save, vect=244)
#pragma interrupt unused_isr_245(save, vect=245)
#pragma interrupt unused_isr_246(save, vect=246)
#pragma interrupt unused_isr_247(save, vect=247)
#pragma interrupt unused_isr_248(save, vect=248)
#pragma interrupt unused_isr_249(save, vect=249)
#pragma interrupt unused_isr_250(save, vect=250)
#pragma interrupt unused_isr_251(save, vect=251)
#pragma interrupt unused_isr_252(save, vect=252)
#pragma interrupt unused_isr_253(save, vect=253)
#pragma interrupt unused_isr_254(save, vect=254)
#pragma interrupt unused_isr_255(save, vect=255)


void unused_isr_0(void)   { unused_isr(); };
void unused_isr_1(void)   { unused_isr(); };
void unused_isr_2(void)   { unused_isr(); };
void unused_isr_3(void)   { unused_isr(); };
void unused_isr_4(void)   { unused_isr(); };
void unused_isr_5(void)   { unused_isr(); };
void unused_isr_6(void)   { unused_isr(); };
void unused_isr_7(void)   { unused_isr(); };
void unused_isr_8(void)   { unused_isr(); };
void unused_isr_9(void)   { unused_isr(); };
void unused_isr_10(void)  { unused_isr(); };
void unused_isr_11(void)  { unused_isr(); };
void unused_isr_12(void)  { unused_isr(); };
void unused_isr_13(void)  { unused_isr(); };
void unused_isr_14(void)  { unused_isr(); };
void unused_isr_15(void)  { unused_isr(); };
void unused_isr_16(void)  { unused_isr(); };
void unused_isr_17(void)  { unused_isr(); };
void unused_isr_18(void)  { unused_isr(); };
void unused_isr_19(void)  { unused_isr(); };
void unused_isr_20(void)  { unused_isr(); };                  
void unused_isr_21(void)  { unused_isr(); };                  
void unused_isr_22(void)  { unused_isr(); };                  
void unused_isr_23(void)  { unused_isr(); };                  
void unused_isr_24(void)  { unused_isr(); };                  
void unused_isr_25(void)  { unused_isr(); };                  
void unused_isr_26(void)  { unused_isr(); };                  
void unused_isr_27(void)  { unused_isr(); };                  
void unused_isr_28(void)  { unused_isr(); };                  
void unused_isr_29(void)  { unused_isr(); };                  
void unused_isr_30(void)  { unused_isr(); };                  
void unused_isr_31(void)  { unused_isr(); };                  
void unused_isr_32(void)  { unused_isr(); };                  
void unused_isr_33(void)  { unused_isr(); };                  
void unused_isr_34(void)  { unused_isr(); };                  
void unused_isr_35(void)  { unused_isr(); };                  
void unused_isr_36(void)  { unused_isr(); };                  
void unused_isr_37(void)  { unused_isr(); };                  
void unused_isr_38(void)  { unused_isr(); };                  
void unused_isr_39(void)  { unused_isr(); };            
void unused_isr_40(void)  { unused_isr(); };                  
void unused_isr_41(void)  { unused_isr(); };                  
void unused_isr_42(void)  { unused_isr(); };                  
void unused_isr_43(void)  { unused_isr(); };                  
void unused_isr_44(void)  { unused_isr(); };                  
void unused_isr_45(void)  { unused_isr(); };                  
void unused_isr_46(void)  { unused_isr(); };                  
void unused_isr_47(void)  { unused_isr(); };                  
void unused_isr_48(void)  { unused_isr(); };                  
void unused_isr_49(void)  { unused_isr(); };            
void unused_isr_50(void)  { unused_isr(); };                  
void unused_isr_51(void)  { unused_isr(); };                  
void unused_isr_52(void)  { unused_isr(); };                  
void unused_isr_53(void)  { unused_isr(); };                  
void unused_isr_54(void)  { unused_isr(); };                  
void unused_isr_55(void)  { unused_isr(); };                  
void unused_isr_56(void)  { unused_isr(); };                  
void unused_isr_57(void)  { unused_isr(); };                  
void unused_isr_58(void)  { unused_isr(); };                  
void unused_isr_59(void)  { unused_isr(); };            
void unused_isr_60(void)  { unused_isr(); };                  
void unused_isr_61(void)  { unused_isr(); };                  
void unused_isr_62(void)  { unused_isr(); };                  
void unused_isr_63(void)  { unused_isr(); };                  
void unused_isr_64(void)  { unused_isr(); };                  
void unused_isr_65(void)  { unused_isr(); };                  
void unused_isr_66(void)  { unused_isr(); };                  
void unused_isr_67(void)  { unused_isr(); };                  
void unused_isr_68(void)  { unused_isr(); };                  
void unused_isr_69(void)  { unused_isr(); };
void unused_isr_70(void)  { unused_isr(); };                  
void unused_isr_71(void)  { unused_isr(); };                  
void unused_isr_72(void)  { unused_isr(); };                  
void unused_isr_73(void)  { unused_isr(); };                  
void unused_isr_74(void)  { unused_isr(); };                  
void unused_isr_75(void)  { unused_isr(); };                  
void unused_isr_76(void)  { unused_isr(); };                  
void unused_isr_77(void)  { unused_isr(); };                  
void unused_isr_78(void)  { unused_isr(); };                  
void unused_isr_79(void)  { unused_isr(); };     
void unused_isr_80(void)  { unused_isr(); };                  
void unused_isr_81(void)  { unused_isr(); };                  
void unused_isr_82(void)  { unused_isr(); };                  
void unused_isr_83(void)  { unused_isr(); };                  
void unused_isr_84(void)  { unused_isr(); };                  
void unused_isr_85(void)  { unused_isr(); };                  
void unused_isr_86(void)  { unused_isr(); };                  
void unused_isr_87(void)  { unused_isr(); };                  
void unused_isr_88(void)  { unused_isr(); };                  
void unused_isr_89(void)  { unused_isr(); };     
void unused_isr_90(void)  { unused_isr(); };                  
void unused_isr_91(void)  { unused_isr(); };                  
void unused_isr_92(void)  { unused_isr(); };                  
void unused_isr_93(void)  { unused_isr(); };                  
void unused_isr_94(void)  { unused_isr(); };                  
void unused_isr_95(void)  { unused_isr(); };                  
void unused_isr_96(void)  { unused_isr(); };                  
void unused_isr_97(void)  { unused_isr(); };                  
void unused_isr_98(void)  { unused_isr(); };                  
void unused_isr_99(void)  { unused_isr(); };     
void unused_isr_100(void) { unused_isr(); };
void unused_isr_101(void) { unused_isr(); };
void unused_isr_102(void) { unused_isr(); };
void unused_isr_103(void) { unused_isr(); };
void unused_isr_104(void) { unused_isr(); };
void unused_isr_105(void) { unused_isr(); };
void unused_isr_106(void) { unused_isr(); };
void unused_isr_107(void) { unused_isr(); };
void unused_isr_108(void) { unused_isr(); };
void unused_isr_109(void) { unused_isr(); };
void unused_isr_110(void) { unused_isr(); };
void unused_isr_111(void) { unused_isr(); };
void unused_isr_112(void) { unused_isr(); };
void unused_isr_113(void) { unused_isr(); };
void unused_isr_114(void) { unused_isr(); };
void unused_isr_115(void) { unused_isr(); };
void unused_isr_116(void) { unused_isr(); };
void unused_isr_117(void) { unused_isr(); };
void unused_isr_118(void) { unused_isr(); };
void unused_isr_119(void) { unused_isr(); };
void unused_isr_120(void) { unused_isr(); };                  
void unused_isr_121(void) { unused_isr(); };                  
void unused_isr_122(void) { unused_isr(); };                  
void unused_isr_123(void) { unused_isr(); };                  
void unused_isr_124(void) { unused_isr(); };                  
void unused_isr_125(void) { unused_isr(); };                  
void unused_isr_126(void) { unused_isr(); };                  
void unused_isr_127(void) { unused_isr(); };                  
void unused_isr_128(void) { unused_isr(); };                  
void unused_isr_129(void) { unused_isr(); };                  
void unused_isr_130(void) { unused_isr(); };                  
void unused_isr_131(void) { unused_isr(); };                  
void unused_isr_132(void) { unused_isr(); };                  
void unused_isr_133(void) { unused_isr(); };                  
void unused_isr_134(void) { unused_isr(); };                  
void unused_isr_135(void) { unused_isr(); };                  
void unused_isr_136(void) { unused_isr(); };                  
void unused_isr_137(void) { unused_isr(); };                  
void unused_isr_138(void) { unused_isr(); };                  
void unused_isr_139(void) { unused_isr(); };            
void unused_isr_140(void) { unused_isr(); };                  
void unused_isr_141(void) { unused_isr(); };                  
void unused_isr_142(void) { unused_isr(); };                  
void unused_isr_143(void) { unused_isr(); };                  
void unused_isr_144(void) { unused_isr(); };                  
void unused_isr_145(void) { unused_isr(); };                  
void unused_isr_146(void) { unused_isr(); };                  
void unused_isr_147(void) { unused_isr(); };                  
void unused_isr_148(void) { unused_isr(); };                  
void unused_isr_149(void) { unused_isr(); };            
void unused_isr_150(void) { unused_isr(); };                  
void unused_isr_151(void) { unused_isr(); };                  
void unused_isr_152(void) { unused_isr(); };                  
void unused_isr_153(void) { unused_isr(); };                  
void unused_isr_154(void) { unused_isr(); };                  
void unused_isr_155(void) { unused_isr(); };                  
void unused_isr_156(void) { unused_isr(); };                  
void unused_isr_157(void) { unused_isr(); };                  
void unused_isr_158(void) { unused_isr(); };                  
void unused_isr_159(void) { unused_isr(); };            
void unused_isr_160(void) { unused_isr(); };                  
void unused_isr_161(void) { unused_isr(); };                  
void unused_isr_162(void) { unused_isr(); };                  
void unused_isr_163(void) { unused_isr(); };                  
void unused_isr_164(void) { unused_isr(); };                  
void unused_isr_165(void) { unused_isr(); };                  
void unused_isr_166(void) { unused_isr(); };                  
void unused_isr_167(void) { unused_isr(); };                  
void unused_isr_168(void) { unused_isr(); };                  
void unused_isr_169(void) { unused_isr(); };
void unused_isr_170(void) { unused_isr(); };                  
void unused_isr_171(void) { unused_isr(); };                  
void unused_isr_172(void) { unused_isr(); };                  
void unused_isr_173(void) { unused_isr(); };                  
void unused_isr_174(void) { unused_isr(); };                  
void unused_isr_175(void) { unused_isr(); };                  
void unused_isr_176(void) { unused_isr(); };                  
void unused_isr_177(void) { unused_isr(); };                  
void unused_isr_178(void) { unused_isr(); };                  
void unused_isr_179(void) { unused_isr(); };     
void unused_isr_180(void) { unused_isr(); };                  
void unused_isr_181(void) { unused_isr(); };                  
void unused_isr_182(void) { unused_isr(); };                  
void unused_isr_183(void) { unused_isr(); };                  
void unused_isr_184(void) { unused_isr(); };                  
void unused_isr_185(void) { unused_isr(); };                  
void unused_isr_186(void) { unused_isr(); };                  
void unused_isr_187(void) { unused_isr(); };                  
void unused_isr_188(void) { unused_isr(); };                  
void unused_isr_189(void) { unused_isr(); };     
void unused_isr_190(void) { unused_isr(); };                  
void unused_isr_191(void) { unused_isr(); };                  
void unused_isr_192(void) { unused_isr(); };                  
void unused_isr_193(void) { unused_isr(); };                  
void unused_isr_194(void) { unused_isr(); };                  
void unused_isr_195(void) { unused_isr(); };                  
void unused_isr_196(void) { unused_isr(); };                  
void unused_isr_197(void) { unused_isr(); };                  
void unused_isr_198(void) { unused_isr(); };                  
void unused_isr_199(void) { unused_isr(); };     
void unused_isr_200(void) { unused_isr(); };
void unused_isr_201(void) { unused_isr(); };
void unused_isr_202(void) { unused_isr(); };
void unused_isr_203(void) { unused_isr(); };
void unused_isr_204(void) { unused_isr(); };
void unused_isr_205(void) { unused_isr(); };
void unused_isr_206(void) { unused_isr(); };
void unused_isr_207(void) { unused_isr(); };
void unused_isr_208(void) { unused_isr(); };
void unused_isr_209(void) { unused_isr(); };
void unused_isr_210(void) { unused_isr(); };
void unused_isr_211(void) { unused_isr(); };
void unused_isr_212(void) { unused_isr(); };
void unused_isr_213(void) { unused_isr(); };
void unused_isr_214(void) { unused_isr(); };
void unused_isr_215(void) { unused_isr(); };
void unused_isr_216(void) { unused_isr(); };
void unused_isr_217(void) { unused_isr(); };
void unused_isr_218(void) { unused_isr(); };
void unused_isr_219(void) { unused_isr(); };
void unused_isr_220(void) { unused_isr(); };
void unused_isr_221(void) { unused_isr(); };
void unused_isr_222(void) { unused_isr(); };
void unused_isr_223(void) { unused_isr(); };
void unused_isr_224(void) { unused_isr(); };
void unused_isr_225(void) { unused_isr(); };
void unused_isr_226(void) { unused_isr(); };
void unused_isr_227(void) { unused_isr(); };
void unused_isr_228(void) { unused_isr(); };
void unused_isr_229(void) { unused_isr(); };
void unused_isr_230(void) { unused_isr(); };
void unused_isr_231(void) { unused_isr(); };
void unused_isr_232(void) { unused_isr(); };
void unused_isr_233(void) { unused_isr(); };
void unused_isr_234(void) { unused_isr(); };
void unused_isr_235(void) { unused_isr(); };
void unused_isr_236(void) { unused_isr(); };
void unused_isr_237(void) { unused_isr(); };
void unused_isr_238(void) { unused_isr(); };
void unused_isr_239(void) { unused_isr(); };
void unused_isr_240(void) { unused_isr(); };
void unused_isr_241(void) { unused_isr(); };
void unused_isr_242(void) { unused_isr(); };
void unused_isr_243(void) { unused_isr(); };
void unused_isr_244(void) { unused_isr(); };
void unused_isr_245(void) { unused_isr(); };
void unused_isr_246(void) { unused_isr(); };
void unused_isr_247(void) { unused_isr(); };
void unused_isr_248(void) { unused_isr(); };
void unused_isr_249(void) { unused_isr(); };
void unused_isr_250(void) { unused_isr(); };
void unused_isr_251(void) { unused_isr(); };
void unused_isr_252(void) { unused_isr(); };
void unused_isr_253(void) { unused_isr(); };
void unused_isr_254(void) { unused_isr(); };
void unused_isr_255(void) { unused_isr(); };


/***********************************************************************************************************************
* The following array fills in the exception vector table.
***********************************************************************************************************************/
#pragma section C EXCEPTVECT

void (* const Except_Vectors[])(void) =
{ 
    /* Offset from EXTB: Reserved area - must be all 0xFF */
    (void (*)(void))0xFFFFFFFF,  /* 0x00 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x04 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x08 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x0c - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x10 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x14 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x18 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x1c - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x20 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x24 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x28 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x2c - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x30 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x34 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x38 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x3c - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x40 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x44 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x48 - Reserved */
    (void (*)(void))0xFFFFFFFF,  /* 0x4c - Reserved */

    /* Exception vector table */
    excep_supervisor_inst_isr,         /* 0x50  Exception(Supervisor Instruction) */
    excep_access_isr,                  /* 0x54  Exception(Access exception) */
    undefined_interrupt_source_isr,    /* 0x58  Reserved */
    excep_undefined_inst_isr,          /* 0x5c  Exception(Undefined Instruction) */
    undefined_interrupt_source_isr,    /* 0x60  Reserved */
    excep_floating_point_isr,          /* 0x64  Exception(Floating Point) */
    undefined_interrupt_source_isr,    /* 0x68  Reserved */
    undefined_interrupt_source_isr,    /* 0x6c  Reserved */
    undefined_interrupt_source_isr,    /* 0x70  Reserved */
    undefined_interrupt_source_isr,    /* 0x74  Reserved */
    non_maskable_isr,                  /* 0x78  NMI */
};

/***********************************************************************************************************************
* The following array fills in the reset vector.
***********************************************************************************************************************/
#pragma section C RESETVECT

void (* const Reset_Vector[])(void) =
{
    PowerON_Reset_PC                   /* 0xfffffffc  RESET */
};


