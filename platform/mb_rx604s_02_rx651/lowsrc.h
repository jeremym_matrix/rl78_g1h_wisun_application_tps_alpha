/***********************************************************************/
/*                                                                     */
/* Device     : RX                                                     */
/*                                                                     */
/*  FILE        :lowsrc.h                                              */
/*  DATE        :Tue, Oct 31, 2006                                     */
/*  DESCRIPTION :Header file of I/O Stream file                        */
/*  CPU TYPE    :                                                      */
/*                                                                     */
/*  NOTE:THIS IS A TYPICAL EXAMPLE.                                    */
/* Copyright (C) 2009 Renesas Electronics Corporation.                 */
/* and Renesas Solutions Corp.                                         */
/*                                                                     */
/***********************************************************************/
/*Number of I/O Stream*/
#define IOSTREAM 3
