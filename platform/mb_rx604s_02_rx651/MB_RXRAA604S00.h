/****************************************************************************** 
* DISCLAIMER 

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. 

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws. 

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED. 

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software. 
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link: 
* http://www.renesas.com/disclaimer 
******************************************************************************/ 

/* Copyright (C) 2014. Renesas Electronics Corporation, All Rights Reserved. */

/****************************************************************************** 
* File Name : MB_RXRAA604S00.h 
* Version : 1.00 
* Device(s) : RX651
* Tool-Chain : 
* OS : 
* H/W Platform : 
* Description : Evaluation board dependency macro header 
* Operation : 
* Limitations : None 
****************************************************************************** 
* History : DD.MM.YYYY Version Description 
* 
******************************************************************************/ 

#ifndef	__MB_RXRAA604S00_H__
#define	__MB_RXRAA604S00_H__


/***************************************************************************
 * Macros
 **************************************************************************/
#if	defined(CPURX651)
/*	LEDs */
#define PORT_LED0_PM			PORT1.PDR.BIT.B5	// PM1.5
#define PORT_LED0_PORT			PORT1.PODR.BIT.B5	// P1.5
#define PORT_LED1_PM			PORTA.PDR.BIT.B5	// PMA.5
#define PORT_LED1_PORT			PORTA.PODR.BIT.B5	// PA.5
#define PORT_LED2_PM			PORT2.PDR.BIT.B5	// PM2.5
#define PORT_LED2_PORT			PORT2.PODR.BIT.B5	// P2.5
#define PORT_LED3_PM			PORT4.PDR.BIT.B3	// PM4.3
#define PORT_LED3_PORT			PORT4.PODR.BIT.B3	// P4.3

/*	SWITCHs */
#define PORT_PSW0_PUP			PORT4.PCR.BIT.B0	// PU4.0
#define PORT_PSW0_PM			PORT4.PDR.BIT.B0	// PM4.0
#define PORT_PSW0_PORT			PORT4.PIDR.BIT.B0	// P4.0 / IRQ8-DS
#define PORT_PSW1_PUP			PORT4.PCR.BIT.B1	// PU4.1
#define PORT_PSW1_PM			PORT4.PDR.BIT.B1	// PM4.1
#define PORT_PSW1_PORT			PORT4.PIDR.BIT.B1	// P4.1 / IRQ9-DS
#define PORT_PSW2_PUP			PORT4.PCR.BIT.B2	// PU4.2
#define PORT_PSW2_PM			PORT4.PDR.BIT.B2	// PM4.2
#define PORT_PSW2_PORT			PORT4.PIDR.BIT.B2	// P4.2 / IRQ10-DS
#define PORT_PSW3_PUP			PORT0.PCR.BIT.B5	// PU0.5
#define PORT_PSW3_PM			PORT0.PDR.BIT.B5	// PM0.5
#define PORT_PSW3_PORT			PORT0.PIDR.BIT.B5	// P0.5 / IRQ13

#define	PORT_INPUT				0
#define	PORT_OUTPUT				1
#define	PORT_HI					1
#define	PORT_LO					0
#define	PORT_PULLUP_ON			1
#define	PORT_PULLUP_OFF			0

/* serial interface status register */
#define	ASIS_PE				0x08			// parity error
#define	ASIS_FE				0x10			// framing error
#define	ASIS_OVE			0x20			// overrun error

/* serial interface tx status register */
#define	ASIF_TXBF			0x04			// tx buffer data flag

/* baudrate setting */
#define BPS2400				10
#define	BPS4800				9
#define	BPS9600				8
#define	BPS19200			7
#define	BPS38400			6
#define	BPS115200			0


#endif	/* defined(CPURX651) */

#endif	/* __MB_RXRAA604S00_H__ */

