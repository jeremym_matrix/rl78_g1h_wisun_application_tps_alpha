//-------------------------------------------------------------------------
//      ILINK command file template for RL78 microcontroller R5F11FLL.
//
//      This file can be used to link object files from the RL78
//      Assembler, IASMRL78, and the C/C++ compiler ICCRL78.
//
//          This file is generated from the device file:
//          DR5F11FLL.DVF
//          Copyright(C) 2015 Renesas
//
//          Core type: s3
//
//          Format version 3.00, File version 1.00 
//-------------------------------------------------------------------------


define exported symbol __link_file_version_2 = 1;

initialize by copy with simple ranges, packing = auto { rw, R_DATA, R_BSS, R_DATAF, R_BSSF, R_SDATA, R_SBSS };
do not initialize   { section *.noinit };

define memory mem with size = 1M;


// Set the symbol __RESERVE_OCD_ROM to 1 to reserve the OCD area for debugging.
// IDE: Symbol can be defined within the project settings here:
//      "Project"-->"Options..."->"Linker"-->"Config"-->"Configuration file symbol definitions"
//      Symbol definition: __RESERVE_OCD_ROM=1
// Command line: --config_def __RESERVE_OCD_ROM=1

if (isdefinedsymbol(__RESERVE_OCD_ROM))
{
  if (__RESERVE_OCD_ROM == 1)
  {
    reserve region "OCD ROM area" = mem:[from 0x7FE00 size 0x0200];
  }
}

// Set the symbol __RESERVE_OCD_TRACE_RAM to 1 to reserve the OCD trace ram area for debugging.
// IDE: Symbol can be defined within the project settings here:
//      "Project"-->"Options..."->"Linker"-->"Config"-->"Configuration file symbol definitions"
//      Symbol definition: __RESERVE_OCD_TRACE_RAM=1
// Command line: --config_def __RESERVE_OCD_TRACE_RAM=1

if (isdefinedsymbol(__RESERVE_OCD_TRACE_RAM))
{
  if (__RESERVE_OCD_TRACE_RAM == 1)
  {
    reserve region "OCD Trace RAM" = mem:[from 0xF4300 size 0x0400];
  }
}
define region ROM_near      = mem:[from 0x000D8 to 0x02FFF];
define region ROM_far       = mem:[from 0x000D8 to 0x7FFFF];
define region ROM_huge      = mem:[from 0x000D8 to 0x7FFFF];
define region SADDR         = mem:[from 0xFFE20 to 0xFFEDF];

// Workaround for IAR Compiler Operating Precaution: Switch/case statement over 64KB page.
// Force linker to map related code into 64KB pages.
define region ROM_switch_1  = mem:[from 0x50000 to 0x5FFFF];
define region ROM_switch_2  = mem:[from 0x60000 to 0x6FFFF];

// Reserve internal RAM areas for FLASH self-programming (area F3F00H to F4309H)
// and DTC transfer (area FFD00H to FFD4FH).
define region RAM_near      = mem:[from 0xF3F00 to 0xFFE1F] - mem:[from 0xF3F00 to 0xF4309] - mem:[from 0xFFD00 to 0xFFD4F];
define region RAM_far       = mem:[from 0xF3F00 to 0xFFE1F] - mem:[from 0xF3F00 to 0xF4309] - mem:[from 0xFFD00 to 0xFFD4F];
define region RAM_huge      = mem:[from 0xF3F00 to 0xFFE1F] - mem:[from 0xF3F00 to 0xF4309] - mem:[from 0xFFD00 to 0xFFD4F];

define region VECTOR        = mem:[from 0x00000 to 0x0007F];
define region CALLT         = mem:[from 0x00080 to 0x000BF];
define region EEPROM        = mem:[from 0xF1000 to 0xF2FFF];

define block NEAR_HEAP  with alignment = 2, size = _NEAR_HEAP_SIZE {  };
define block FAR_HEAP   with alignment = 2, size = _FAR_HEAP_SIZE {  };
define block HUGE_HEAP  with alignment = 2, size = _HUGE_HEAP_SIZE {  };
define block CSTACK     with alignment = 2, size = _STACK_SIZE { rw section CSTACK };
define block INIT_ARRAY with alignment = 2, fixed order { ro section .preinit_array,
                                                          ro section .init_array };
define block INIT_ARRAY_TLS with alignment = 2, fixed order { ro section .preinit_array_tls,
                                                              ro section .init_array_tls };



define block OPT_BYTE with size = 4  { R_OPT_BYTE,
                                       ro section .option_byte,
                                       ro section OPTBYTE };
define block SECUR_ID with size = 10 { R_SECUR_ID,
                                       ro section .security_id,
                                       ro section SECUID };
define block EEL_BLOCK with alignment = 2, fixed order { ro section FDL_CODE,
                                                         ro section EEL_CODE,
                                                         ro section FDL_CNST,
                                                         ro section EEL_CNST};

place at address mem:0x00000       { ro section .reset };
place at address mem:0x00004       { ro section .intvec };
place at address mem:0x000C0       { block OPT_BYTE };
place at address mem:0x000C4       { block SECUR_ID };
place at address mem:0xF430A       { block CSTACK };

"CALLT":place in CALLT             { R_CALLT0, ro section .callt0 };

if ( _NEAR_CONST_LOCATION_SIZE > 0 )
{
  "MIRROR": place in [from _NEAR_CONST_LOCATION_START size _NEAR_CONST_LOCATION_SIZE] with mirroring to (_NEAR_CONST_LOCATION_START | 0xF0000) { ro R_CONST,
                                                                                                                                                    ro section .const,
                                                                                                                                                    ro section .switch };
}

"ROMNEAR":place in ROM_near        { R_TEXT, ro section .text};

"ROMFAR":place in ROM_far          { block INIT_ARRAY,
                                     block INIT_ARRAY_TLS,
                                     R_TEXTF_UNIT64KP,
                                     block EEL_BLOCK,
                                     ro section .textf_unit64kp,
                                     ro section .constf,
                                     ro };

"ROMHUGE":place in ROM_huge        { ro section .consth,
                                     R_TEXTF,
                                     ro section .textf } except { object mac_mlme.o,
                                                                  object r_mac_ie.o,
                                                                  object r_nwk_task.o,
                                                                  object ssl_cli.o,
                                                                  object x509_crt.o,
                                                                  object x509.o,
                                                                  object phy_attr.o,
                                                                  object wpa_common.o };

// Workaround for IAR Compiler Operating Precaution: Switch/case statement over 64KB page.
// Force linker to map related code into 64KB page.
"ROMSWITCH1":place in ROM_switch_1 { ro section .switchf,
                                     ro section .textf object r_nwk_task.o,
                                     ro section .textf object r_mac_ie.o,
                                     ro section .textf object phy_attr.o,
                                     ro section .textf_unit64kp object phy_attr.o } except { object ssl_cli.o,
                                                                                             object x509_crt.o,
                                                                                             object x509.o,
                                                                                             object mac_mlme.o,
                                                                                             object wpa_common.o };

// Workaround for IAR Compiler Operating Precaution: Switch/case statement over 64KB page.
// Force linker to map related code into 64KB page.
"ROMSWITCH2":place in ROM_switch_2 { ro section .switchf object ssl_cli.o,
                                     ro section .switchf object x509_crt.o,
                                     ro section .switchf object x509.o,
                                     ro section .switchf object wpa_common.o,
                                     ro section .textf object ssl_cli.o,
                                     ro section .textf object x509_crt.o,
                                     ro section .textf object x509.o,
                                     ro section .textf object wpa_common.o,
                                     ro section .textf object mac_mlme.o
                                   };

"RAMNEAR":place in RAM_near        { block NEAR_HEAP,
                                     zi section .iar.dynexit,
                                     R_DATA,
                                     rw section .data,
                                     R_BSS,
                                     rw section .bss*,
                                     rw };

"RAMFAR":place in RAM_far          { block FAR_HEAP,
                                     R_DATAF,
                                     rw section .dataf,
                                     rw section .data_unit64kp,
                                     rw section .bss_unit64kp,
                                     R_BSSF,
                                     rw section .bssf* };

"RAMHUGE":place in RAM_huge        { block HUGE_HEAP,
                                     rw section .hdata,
                                     rw section .hbss* };

"SADDRMEM":place in SADDR          { rw section .sdata,
                                     rw section FDL_SDAT,
                                     rw section EEL_SDAT,
                                     R_SDATA,
                                     rw section .sbss*,
                                     R_SBSS,
                                     rw section .wrkseg };

