/*******************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only
 * intended for use with Renesas products. No other uses are authorized.
 * This software is owned by Renesas Electronics Corporation and is protected under
 * all applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES
 * REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY,
 * INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A
 * PARTICULAR PURPOSE AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY
 * DISCLAIMED.
 * TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
 * ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
 * FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES
 * FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS
 * AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this
 * software and to discontinue the availability of this software.
 * By using this software, you agree to the additional terms and
 * conditions found by accessing the following link:
 * http://www.renesas.com/disclaimer
 *******************************************************************************/
#ifndef __hardware_sfr_H
#define __hardware_sfr_H

/*******************************************************************************
 * file name	: hardware_sfr.h
 * description	: sample application program
 *******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/

/***********************************************************************
 *	Serial
 **********************************************************************/
/***********************************************************
 *	Serial Clock Select Register m (SPSm)
 **********************************************************/
/* Operating mode and clear mode selection(PRS013~PRS010) */
#define SAU_CK01_FCLK_0				0x0000		/* ck01-fclk */
#define SAU_CK01_FCLK_1				0x0010		/* ck01-fclk/2^1 */

/***********************************************************
 * Serial Mode Register mn (SMRmn)
 **********************************************************/
/* Initial Value */
#define	SAU_SMRMN_INITIALVALUE		0x0020

/* Selection of macro clock (MCK) of channel n(CKSmn) */
#define SAU_CLOCK_SELECT_CLR		0x8000		/* for clear the bits */
#define SAU_CLOCK_SELECT_CK00		0x0000		/* operation clock CK0 set by PRS register */
#define SAU_CLOCK_SELECT_CK01		0x8000		/* 0peration clock CK1 set by PRS register */

/* Selection of transfer clock (TCLK) of channel n(CCSmn) */
#define SAU_CLOCK_MODE_CLR			0x4000		/* for clear the bits */
#define SAU_CLOCK_MODE_CKS			0x0000		/* divided operation clock MCK specified by CKSmn bit */
#define SAU_CLOCK_MODE_TI0N			0x4000		/* clock input from SCK pin (slave transfer in CSI mode) */

/* Selection of start trigger source(STSmn) */
#define SAU_TRIGGER_CLR				0x0100		/* for clear the bits */
#define SAU_TRIGGER_SOFTWARE		0x0000		/* only software trigger is valid */
#define SAU_TRIGGER_RXD				0x0100		/* valid edge of RXD pin */

/* Setting of operation mode of channel n(MDmn2,MDmn1) */
#define SAU_CSI						0x0000		/* CSI mode */
#define SAU_UART					0x0002		/* UART mode */
#define SAU_IIC						0x0004		/* simplified IIC mode */

/* Selection of interrupt source of channel n(MDmn0) */
#define SAU_TRANSFER_END			0x0000		/* transfer end interrupt */
#define SAU_BUFFER_EMPTY			0x0001		/* buffer empty interrupt */

/***********************************************************
 * Serial Communication Operation Setting Register mn (SCRmn)
 **********************************************************/
/* Setting of operation mode of channel n(TXEmn,RXEmn) */
#define SAU_NOT_COMMUNICATION		0x0000		/* does not start communication */
#define SAU_RECEPTION				0x4000		/* reception only */
#define SAU_TRANSMISSION			0x8000		/* transmission only */
#define SAU_RECEPTION_TRANSMISSION	0xc000		/* reception and transmission */

/* Selection of data and clock phase in CSI mode(DAPmn,CKPmn) */
#define SAU_TIMING_1				0x0000		/* type 1 */
#define SAU_TIMING_2				0x1000		/* type 2 */
#define SAU_TIMING_3				0x2000		/* type 3 */
#define SAU_TIMING_4				0x3000		/* type 4 */

/* Selection of masking of error interrupt signal(EOCmn) */
#define SAU_INTSRE_MASK				0x0000		/* masks error interrupt INTSREx */
#define SAU_INTSRE_ENABLE			0x0400		/* enables generation of error interrupt INTSREx */

/* Setting of parity bit in UART mode(PTCmn1,PTCmn0) */
#define SAU_PARITY_NONE				0x0000		/* none parity */
#define SAU_PARITY_ZERO				0x0100		/* zero parity */
#define SAU_PARITY_EVEN				0x0200		/* even parity */
#define SAU_PARITY_ODD				0x0300		/* odd parity */

/* Selection of data transfer sequence in CSI and UART modes(DIRmn) */
#define SAU_MSB						0x0000		/* MSB */
#define SAU_LSB						0x0080		/* LSB */

/* Setting of stop bit in UART mode(SLCmn1,SLCmn0) */
#define SAU_STOP_NONE				0x0000		/* none stop bit */
#define SAU_STOP_1					0x0010		/* 1 stop bit */
#define SAU_STOP_2					0x0020		/* 2 stop bits */

/* Setting of data length in CSI and UART modes(DLSmn2~DLSmn0) */
#define SAU_LENGTH_5				0x0004		/* 5-bit data length */
#define SAU_LENGTH_7				0x0006		/* 7-bit data length */
#define SAU_LENGTH_8				0x0007		/* 8-bit data length */

/***********************************************************
 * Serial Output Level Register m (SOLm)
 **********************************************************/
/* Selects inversion of the level of the transmit data of channel n in UART mode */
#define SAU_CHANNEL0_NORMAL			0x0000		/* normal bit level */
#define SAU_CHANNEL0_INVERTED		0x0001		/* inverted bit level */

#define SAU_CHANNEL2_NORMAL			0x0000		/* normal bit level */
#define SAU_CHANNEL2_INVERTED		0x0004		/* inverted bit level */

#define SAU_CHANNEL3_NORMAL			0x0000		/* normal bit level */
#define SAU_CHANNEL3_INVERTED		0x0008		/* inverted bit level */

/***********************************************************
 * Format of Serial Status Register mn (SSRmn)
 **********************************************************/
/* Communication status indication flag of channel n(TSFmn) */
#define SAU_UNDER_EXECUTE			0x0040		/* communication is under execution */
#define SAU_DATA_STORED				0x0020		/* valid data is stored SDRmn register */

/* Parity error detection flag of channel n(PEFmn) */
#define	SAU_FRAMING_ERROR			0x0004		/* a framing error occurs during UART reception */
#define SAU_PARITY_ERROR			0x0002		/* a parity error occurs during UART reception or ACK is not detected during I2C transmission */
#define	SAU_OVERRUN_ERROR			0x0001		/* a overrun error during UART reception */

/***********************************************************
 * Serial Channel Start Register m (SSm)
 **********************************************************/
/* Operation start trigger of channel 0(SSm0) */
#define SAU_CH0_START_TRG_OFF		0x0000		/* no trigger operation */
#define SAU_CH0_START_TRG_ON		0x0001		/* sets SEm0 to 1 and enters the communication wait status */

/* Operation start trigger of channel 1(SSm1) */
#define SAU_CH1_START_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH1_START_TRG_ON		0x0002	/* sets SEm1 to 1 and enters the communication wait status */

/* Operation start trigger of channel 2(SSm2) */
#define SAU_CH2_START_TRG_OFF		0x0000		/* no trigger operation */
#define SAU_CH2_START_TRG_ON		0x0004		/* sets SEm2 to 1 and enters the communication wait status */

/* Operation start trigger of channel 3(SSm3) */
#define SAU_CH3_START_TRG_OFF		0x0000		/* no trigger operation */
#define SAU_CH3_START_TRG_ON		0x0008		/* sets SEm3 to 1 and enters the communication wait status */

/***********************************************************
 * Serial Channel Stop Register m (STm)
 **********************************************************/
/* Operation stop trigger of channel 0(STm0) */
#define SAU_CH0_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define SAU_CH0_STOP_TRG_ON			0x0001		/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 1(STm1) */
#define SAU_CH1_STOP_TRG_OFF		0x0000	/* no trigger operation */
#define SAU_CH1_STOP_TRG_ON			0x0002	/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 2(STm2) */
#define SAU_CH2_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define SAU_CH2_STOP_TRG_ON			0x0004		/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 3(STm3) */
#define SAU_CH3_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define SAU_CH3_STOP_TRG_ON			0x0008		/* operation is stopped (stop trigger is generated) */

/***********************************************************
 * Format of Serial Flag Clear Trigger Register mn (SIRmn)
 **********************************************************/
/* Clear trigger of overrun error flag of channel n(OVCTmn) */
#define	SAU_SIRMN_OVCTMN			0x0001

/* Clear trigger of parity error flag of channel n(PECTmn) */
#define	SAU_SIRMN_PECTMN			0x0002

/* Clear trigger of framing error of channel n(FECTMN) */
#define	SAU_SIRMN_FECTMN			0x0004

/***********************************************************
 * Serial Output Enable Register m (SOEm)
 **********************************************************/
/* Serial output enable/disable of channel 0(SOEm0) */
#define SAU_CH0_OUTPUT_ENABLE		0x0001		/* stops output by serial communication operation */
#define SAU_CH0_OUTPUT_DISABLE		0x0000		/* enables output by serial communication operation */

/* Serial output enable/disable of channel 2(SOEm2) */
#define SAU_CH2_OUTPUT_ENABLE		0x0004		/* stops output by serial communication operation */
#define SAU_CH2_OUTPUT_DISABLE		0x0000		/* enables output by serial communication operation */

/* Serial output enable/disable of channel 3(SOEm3) */
#define SAU_CH3_OUTPUT_ENABLE		0x0008		/* stops output by serial communication operation */
#define SAU_CH3_OUTPUT_DISABLE		0x0000		/* enables output by serial communication operation */

/***********************************************************
 * Serial Output Register m (SOm)
 **********************************************************/
/* Serial data output of channel 0(SOm0) */
#define SAU_CH0_DATA_OUTPUT_0		0x0000		/* Serial data output value is 0*/
#define SAU_CH0_DATA_OUTPUT_1		0x0001		/* Serial data output value is 1*/

/* Serial data output of channel 2(SOm2) */
#define SAU_CH2_DATA_OUTPUT_0		0x0000		/* Serial data output value is 0*/
#define SAU_CH2_DATA_OUTPUT_1		0x0004		/* Serial data output value is 1*/

/* Serial data output of channel 3(SOm3) */
#define SAU_CH3_DATA_OUTPUT_0		0x0000		/* Serial data output value is 0*/
#define SAU_CH3_DATA_OUTPUT_1		0x0008		/* Serial data output value is 1*/

/* Serial clock output of channel 0(CKOm0) */
#define SAU_CH0_CLOCK_OUTPUT_0		0x0000		/* Serial clock output value is 0*/
#define SAU_CH0_CLOCK_OUTPUT_1		0x0100		/* Serial clock output value is 1*/

/* Serial clock output of channel 2(CKOm2) */
#define SAU_CH2_CLOCK_OUTPUT_0		0x0000		/* Serial clock output value is 0*/
#define SAU_CH2_CLOCK_OUTPUT_1		0x0400		/* Serial clock output value is 1*/

/* Serial clock output of channel 3(CKOm3) */
#define SAU_CH3_CLOCK_OUTPUT_0		0x0000		/* Serial clock output value is 0*/
#define SAU_CH3_CLOCK_OUTPUT_1		0x0800		/* Serial clock output value is 1*/

/***********************************************************************
 *	Timer
 **********************************************************************/
/***********************************************************
 *	Timer Clock Select Register 0 (TPS0)
 **********************************************************/
/* Initial Value */
#define TAU_TPS0_INITIALVALUE		0x0000

/* Operating mode and clear mode selection(PRS003 & PRS002 & PRS001 & PRS000) */
#define TAU_CK00_FCLK_0				0x0000		/* ck00-fclk */
#define TAU_CK00_FCLK_1				0x0001		/* ck00-fclk/2^1 */
#define TAU_CK00_FCLK_2				0x0002		/* ck00-fclk/2^2 */
#define TAU_CK00_FCLK_3				0x0003		/* ck00-fclk/2^3 */

/* Operating mode and clear mode selection(PRS013 & PRS012 & PRS011 & PRS010) */
#define TAU_CK01_FCLK_8				0x0080		/* ck01-fclk/2^8 */
#define TAU_CK01_FCLK_10			0x00A0		/* ck01-fclk/2^10 */

/***********************************************************
 *	Timer Mode Register 0n (TMR0n)
 **********************************************************/
/* Initial Value */
#define TAU_TMR0_INITIALVALUE		0x0000

/* Selection of macro clock (MCK) of channel n(CKS0n) */
#define TAU_CLOCK_SELECT_CLR		0x8000		/* for clear the bits */
#define TAU_CLOCK_SELECT_CK00		0x0000		/* operation clock CK0 set by PRS register */
#define TAU_CLOCK_SELECT_CK01		0x8000		/* operation clock CK1 set by PRS register */

/* Selection of count clock (CCK) of channel n(CCS0n) */
#define TAU_CLOCK_MODE_CLR			0x1000		/* for clear the bits */
#define TAU_CLOCK_MODE_CKS			0x0000		/* macro clock MCK specified by CKS0n bit */
#define TAU_CLOCK_MODE_TI0N			0x1000		/* valid edge of input signal input from TI0n pin */

/* Operation mode of channel n(MD0n3 & MD0n2 & MD0n1 & MD0n0) */
#define TAU_MODE_CLR				0x000F		/* for clear the bits */
#define TAU_MODE_INTERVAL_TIMER		0x0000		/* interval timer mode */
#define TAU_MODE_EVENT_COUNT 		0x0006		/* event counter mode */
#define TAU_MODE_CAPTURE			0x0004		/* capture mode */
#define TAU_MODE_HIGHLOW_MEASURE	0x000C		/* high-/low-level width measurement mode */
#define TAU_MODE_PWM_MASTER			0x0001		/* PWM Function (Master Channel) mode */
#define TAU_MODE_PWM_SLAVE			0x0009		/* PWM Function (Slave Channel) mode */
#define TAU_MODE_ONESHOT			0x0008		/* one-shot pulse output mode */

/* Setting of starting counting and interrupt(MD0n0) */
#define TAU_START_INT_CLR			0x0001		/* for clear the bits */
#define TAU_START_INT_UNUSED		0x0000		/* timer interrupt is not generated when counting is started (timer output does not change, either) */
#define TAU_START_INT_USED			0x0001		/* timer interrupt is generated when counting is started (timer output also changes) */

/***********************************************************
 *	Timer Channel Start Register 0 (TS0)
 **********************************************************/
/* Initial Value */
#define TAU_TS0_INITIALVALUE		0x0000

/* Operation enable (start) trigger of channel 0(TS00) */
#define TAU_CH0_START_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH0_START_TRG_ON		0x0001		/* operation is enabled (start software trigger is generated) */

/* Operation enable (start) trigger of channel 1(TS01) */
#define TAU_CH1_START_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH1_START_TRG_ON		0x0002		/* operation is enabled (start software trigger is generated) */

/* Operation enable (start) trigger of channel 2(TS02) */
#define TAU_CH2_START_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH2_START_TRG_ON		0x0004		/* operation is enabled (start software trigger is generated) */

/* Operation enable (start) trigger of channel 3(TS03) */
#define TAU_CH3_START_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH3_START_TRG_ON		0x0008		/* operation is enabled (start software trigger is generated) */

/* Operation enable (start) trigger of channel 4(TS04) */
#define TAU_CH4_START_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH4_START_TRG_ON		0x0010		/* operation is enabled (start software trigger is generated) */

/* Operation enable (start) trigger of channel 5(TS05) */
#define TAU_CH5_START_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH5_START_TRG_ON		0x0020		/* operation is enabled (start software trigger is generated) */

/* Operation enable (start) trigger of channel 6(TS06) */
#define TAU_CH6_START_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH6_START_TRG_ON		0x0040		/* operation is enabled (start software trigger is generated) */

/* Operation enable (start) trigger of channel 7(TS07) */
#define TAU_CH7_START_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH7_START_TRG_ON		0x0080		/* operation is enabled (start software trigger is generated) */

/***********************************************************
 *	Timer Channel Stop Register 0 (TT0)
 **********************************************************/
/* Initial Value */
#define TAU_TT0_INITIALVALUE		0x0000

/* Operation stop trigger of channel 0(TT00) */
#define TAU_CH0_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH0_STOP_TRG_ON			0x0001		/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 1(TT01) */
#define TAU_CH1_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH1_STOP_TRG_ON			0x0002		/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 2(TT02) */
#define TAU_CH2_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH2_STOP_TRG_ON			0x0004		/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 3(TT03) */
#define TAU_CH3_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH3_STOP_TRG_ON			0x0008		/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 4(TT04) */
#define TAU_CH4_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH4_STOP_TRG_ON			0x0010		/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 5(TT05) */
#define TAU_CH5_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH5_STOP_TRG_ON			0x0020		/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 6(TT06) */
#define TAU_CH6_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH6_STOP_TRG_ON			0x0040		/* operation is stopped (stop trigger is generated) */

/* Operation stop trigger of channel 7(TT07) */
#define TAU_CH7_STOP_TRG_OFF		0x0000		/* no trigger operation */
#define TAU_CH7_STOP_TRG_ON			0x0080		/* operation is stopped (stop trigger is generated) */

/***********************************************************
 *	Timer Output Enable Register 0 (TOE0)
 **********************************************************/
/* Initial Value */
#define TAU_TOE0_INITIALVALUE		0x0000

/* Timer output enable/disable of channel 0(TOE00) */
#define TAU_CH0_OUTPUT_ENABLE		0x0001		/* the TO00 operation enabled by count operation (timer channel output bit) */
#define TAU_CH0_OUTPUT_DISABLE		0x0000		/* the TO00 operation stopped by count operation (timer channel output bit) */

/* Timer output enable/disable of channel 1(TOE01) */
#define TAU_CH1_OUTPUT_ENABLE		0x0002		/* the TO01 operation enabled by count operation (timer channel output bit) */
#define TAU_CH1_OUTPUT_DISABLE		0x0000		/* the TO01 operation stopped by count operation (timer channel output bit) */

/* Timer output enable/disable of channel 2(TOE02) */
#define TAU_CH2_OUTPUT_ENABLE		0x0004		/* the TO02 operation enabled by count operation (timer channel output bit) */
#define TAU_CH2_OUTPUT_DISABLE		0x0000		/* the TO02 operation stopped by count operation (timer channel output bit) */

/* Timer output enable/disable of channel 3(TOE03) */
#define TAU_CH3_OUTPUT_ENABLE		0x0008		/* the TO03 operation enabled by count operation (timer channel output bit) */
#define TAU_CH3_OUTPUT_DISABLE		0x0000		/* the TO03 operation stopped by count operation (timer channel output bit) */

/***********************************************************
 *	Timer Output Level Register 0 (TOL0)
 **********************************************************/
/* Initial Value */
#define TAU_TOL0_INITIALVALUE		0x0000

/* Control of timer output level of channel 0(TOL00) */
#define TAU_CH0_OUTPUT_LEVEL_H		0x0000		/* positive logic output (active-high) */
#define TAU_CH0_OUTPUT_LEVEL_L		0x0001		/* inverted output (active-low) */

/* Control of timer output level of channel 1(TOL01) */
#define TAU_CH1_OUTPUT_LEVEL_H		0x0000		/* positive logic output (active-high) */
#define TAU_CH1_OUTPUT_LEVEL_L		0x0002		/* inverted output (active-low) */

/* Control of timer output level of channel 2(TOL02) */
#define TAU_CH2_OUTPUT_LEVEL_H		0x0000		/* positive logic output (active-high) */
#define TAU_CH2_OUTPUT_LEVEL_L		0x0004		/* inverted output (active-low) */

/* Control of timer output level of channel 3(TOL03) */
#define TAU_CH3_OUTPUT_LEVEL_H		0x0000		/* positive logic output (active-high) */
#define TAU_CH3_OUTPUT_LEVEL_L		0x0008		/* inverted output (active-low) */

/***********************************************************
 *	Timer Output Mode Register 0 (TOM0)
 **********************************************************/
/* Initial Value */
#define TAU_TOM0_INITIALVALUE		0x0000

/* Control of timer output mode of channel 0(TOM00) */
#define TAU_CH0_OUTPUT_TOGGLE		0x0000		/* toggle operation mode */
#define TAU_CH0_OUTPUT_COMBIN		0x0001		/* combination operation mode */

/* Control of timer output mode of channel 1(TOM01) */
#define TAU_CH1_OUTPUT_TOGGLE		0x0000		/* toggle operation mode */
#define TAU_CH1_OUTPUT_COMBIN		0x0002		/* combination operation mode */

/* Control of timer output mode of channel 2(TOM02) */
#define TAU_CH2_OUTPUT_TOGGLE		0x0000		/* toggle operation mode */
#define TAU_CH2_OUTPUT_COMBIN		0x0004		/* combination operation mode */

/* Control of timer output mode of channel 3(TOM03) */
#define TAU_CH3_OUTPUT_TOGGLE		0x0000		/* toggle operation mode */
#define TAU_CH3_OUTPUT_COMBIN		0x0008		/* combination operation mode */

/***********************************************************
 * 16-bit timer data register 00 (TDR00)
 **********************************************************/
#define TAU_TDR00_VALUE				0xF9Fu		/* 1ms */


/***********************************************************
 * Timer RJ Control Register 0 (TRJCR0) 
 **********************************************************/
/* Timer RJ underflow flag (TUNDF) */
#define _00_TMRJ_UNDERFLOW_NOT_OCCUR            (0x00U) /* no underflow */
#define _20_TMRJ_UNDERFLOW_OCCUR                (0x20U) /* underflow */
/* Timer RJ count forcible stop bit (TSTOP) */
#define _00_TMRJ_FORCIBLE_STOP_DISABLE          (0x00U) /* the count is not forcibly stopped */
#define _04_TMRJ_FORCIBLE_STOP_ENABLE           (0x04U) /* the count is forcibly stopped */
/* Timer RJ count status flag (TCSTF) */
#define _00_TMRJ_STATUS_STOP                    (0x00U) /* count stops */
#define _02_TMRJ_STATUS_COUNT                   (0x02U) /* during count */
/* Timer RJ count start bit (TSTART) */
#define _00_TMRJ_COUNT_STOP                     (0x00U) /* count stops */
#define _01_TMRJ_COUNT_START                    (0x01U) /* count starts */

/***********************************************************
 *  Timer RJ Mode Register 0 (TRJMR0) 
 **********************************************************/
/* Timer RJ count source select bit (TCK2,TCK1,TCK0) */
#define _00_TMRJ_COUNT_SOURCE_FCLK              (0x00U) /* fCLK */
#define _10_TMRJ_COUNT_SOURCE_FCLK8             (0x10U) /* fCLK/8 */
#define _30_TMRJ_COUNT_SOURCE_FCLK2             (0x30U) /* fCLK/2 */
#define _40_TMRJ_COUNT_SOURCE_FIL               (0x40U) /* fIL */
#define _50_TMRJ_COUNT_SOURCE_FELC              (0x50U) /* event input from event link controller (ELC) */
#define _60_TMRJ_COUNT_SOURCE_FSUB              (0x60U) /* fSUB */
/* Timer RJ operating mode select bit (TMOD2,TMOD1,TMOD0) */
#define _00_TMRJ_MODE_TIMER                     (0x00U) /* timer mode */

/***********************************************************
 *  Real-time clock control register 0 (RTCC0) 
 **********************************************************/
/* High-accuracy real-time clock operation control (RTCE) */
#define _00_RTC_COUNTER_STOP                   (0x00U) /* stops counter operation */
#define _80_RTC_COUNTER_START                  (0x80U) /* starts counter operation. */
/* 12-/24-hour system select (AMPM) */
#define _00_RTC_12HOUR_SYSTEM                  (0x00U) /* 12-hour system */
#define _08_RTC_24HOUR_SYSTEM                  (0x08U) /* 24-hour system */
#define _08_RTC_RTCC0_AMPM                     (0x08U) /* AMPM bit status detect */
#define _08_RTC_HOURSYSTEM_CLEAR               (0x08U) /* hour system select  clear */
/* Constant-period interrupt (INTRTC) selection (CT2,CT1,CT0) */
#define _00_RTC_INTRTC_NOT_GENERATE            (0x00U) /* does not use fixed-cycle interrupt function */
#define _01_RTC_INTRTC_CLOCK_0                 (0x01U) /* once per 0.5 s (synchronized with second count up) */
#define _02_RTC_INTRTC_CLOCK_1                 (0x02U) /* once per 1 s (same time as second count up) */
#define _03_RTC_INTRTC_CLOCK_2                 (0x03U) /* once per 1 m (second 00 of every minute) */
#define _04_RTC_INTRTC_CLOCK_3                 (0x04U) /* once per 1 hour (minute 00 and second 00 of every hour) */
#define _05_RTC_INTRTC_CLOCK_4                 (0x05U) /* once per 1 day (hour 00, minute 00, and second 00 of every day) */
#define _06_RTC_INTRTC_CLOCK_5                 (0x06U) /* once per 1 month(Day 1, hour 00 a.m., minute 00, and second 00 of every month) */
#define _07_RTC_INTRTC_CLEAR                   (0x07U) /* INTRTC clear */

/***********************************************************
 *  Real-time clock control register 1 (RTCC1) 
 **********************************************************/
/* Alarm operation control (WALE) */
#define _00_RTC_ALARM_DISABLE                  (0x00U) /* match operation is invalid */
#define _80_RTC_ALARM_ENABLE                   (0x80U) /* match operation is valid */
/* Control of alarm interrupt function operation (WALIE) */
#define _00_RTC_ALARM_INT_DISABLE              (0x00U) /* does not generate interrupt on matching of alarm */
#define _40_RTC_ALARM_INT_ENABLE               (0x40U) /* generates interrupt on matching of alarm */
/* Alarm detection status flag (WAFG) */
#define _00_RTC_ALARM_MISMATCH                 (0x00U) /* alarm mismatch */
#define _10_RTC_ALARM_MATCH                    (0x10U) /* detection of matching of alarm */
/* Constant-period interrupt status flag (RIFG) */
#define _00_RTC_INTC_NOTGENERATE_FALG          (0x00U) /* constant-period interrupt is not generated */
#define _08_RTC_INTC_GENERATE_FLAG             (0x08U) /* constant-period interrupt is generated */
/* Wait status flag of high-accuracy real-time clock (RWST) */
#define _00_RTC_COUNTER_OPERATING              (0x00U) /* counter is operating */
#define _02_RTC_COUNTER_STOP                   (0x02U) /* mode to read or write counter value */
/* Wait control of high-accuracy real-time clock (RWAIT) */
#define _00_RTC_COUNTER_SET                    (0x00U) /* sets counter operation */
#define _01_RTC_COUNTER_PAUSE                  (0x01U) /* stops SEC to YEAR counters. Mode to read or write counter value */

/***********************************************************
 *  Event output destination select register n (ELSELRn) 
 **********************************************************/
/* Event output destination select register n (ELSELn2 - ELSELn0) */
#define _00_ELC_EVENT_LINK_OFF            (0x00U) /* event link disabled */
#define _01_ELC_EVENT_LINK_AD             (0x01U) /* select operation of peripheral function 1 to A/D converter */
#define _02_ELC_EVENT_LINK_TAU0           (0x02U) /* select operation of peripheral function 2 to TAU0 channel 0 */
#define _03_ELC_EVENT_LINK_TAU1           (0x03U) /* select operation of peripheral function 3 to TAU0 channel 1 */
#define _04_ELC_EVENT_LINK_TMRJ0          (0x04U) /* select operation of peripheral function 4 to Timer RJ0 */

#define _0004_TMRJ_TRJ0_VALUE             (0x0004U) /* Timer RJ counter register 0 */

/*******************************************************************************
 * Copyright (C) 2014-2016 Renesas Electronics Corporation.
 ******************************************************************************/
#endif // #ifndef __hardware_sfr_H

