/****************************************************************************** 
* DISCLAIMER 

* This software is supplied by Renesas Electronics Corporation and is only 
* intended for use with Renesas products. No other uses are authorized. 

* This software is owned by Renesas Electronics Corporation and is protected under 
* all applicable laws, including copyright laws. 

* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES 
* REGARDING THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, 
* INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
* PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY 
* DISCLAIMED. 

* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS 
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE 
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES 
* FOR ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS 
* AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES. 

* Renesas reserves the right, without notice, to make changes to this 
* software and to discontinue the availability of this software. 
* By using this software, you agree to the additional terms and 
* conditions found by accessing the following link: 
* http://www.renesas.com/disclaimer 
******************************************************************************/ 

/* Copyright (C) 2014. Renesas Electronics Corporation, All Rights Reserved. */

/****************************************************************************** 
* File Name : hardware.h 
* Version : 1.00 
* Device(s) : RL78/G13 
* Tool-Chain :  CubeSuite+ 
* OS : RI78V4 
* H/W Platform : 
* Description : 
* Operation : 
* Limitations : None 
****************************************************************************** 
* History : DD.MM.YYYY Version Description 
*
******************************************************************************/ 

#ifndef __hardware_h__
#define __hardware_h__

/***************************************************************************
 * Includes
 **************************************************************************/
#if defined(__RL78__)
#include	"RL78G1H.h"
#endif

/***************************************************************************
 * Inlines
 **************************************************************************/
#define Hardware_DisableInterrupt()			{ DI(); }
#define Hardware_EnableInterrupt()			{ EI(); }
#define Hardware_No_Operation()				{ NOP(); }

/***************************************************************************
 * Prototypes
 **************************************************************************/
void			RdrvPeripheralInitialize(void);
signed short 	RdrvUART_GetChar(void);
void			RdrvUART_PutChar(unsigned char);
signed short  	RdrvUART_GetLen(void);
void            RdrvFREQHOPPTIMER_Initialize(void);
void            RdrvFREQHOPPTIMER_Stop(void);
unsigned char 	CSI_ReadWrite(unsigned char);

unsigned char	RdrvSwitch_GetAllConditions(void);
unsigned char	RdrvSwitch0_GetCondition(void);
unsigned char	RdrvSwitch1_GetCondition(void);
unsigned char	RdrvSwitch2_GetCondition(void);
unsigned char	RdrvSwitch3_GetCondition(void);
unsigned char	RdrvSwitch4_GetCondition(void);
unsigned char	RdrvSwitch5_GetCondition(void);
unsigned char	RdrvSwitch6_GetCondition(void);
unsigned char	RdrvSwitch7_GetCondition(void);
void			RdrvLED_ALL_ON(void);
void			RdrvLED_ALL_OFF(void);
void			RdrvLED0_ON(void);
void			RdrvLED0_OFF(void);
void			RdrvLED1_ON(void);
void			RdrvLED1_OFF(void);
void			RdrvLED2_ON(void);
void			RdrvLED2_OFF(void);
void			RdrvLED3_ON(void);
void			RdrvLED3_OFF(void);
void			RdrvLED4_ON(void);
void			RdrvLED4_OFF(void);
void			RdrvLED5_ON(void);
void			RdrvLED5_OFF(void);
void			RdrvLED6_ON(void);
void			RdrvLED6_OFF(void);
void			RdrvLED7_ON(void);
void			RdrvLED7_OFF(void);
unsigned char	RdrvLED0_GetCondition(void);
unsigned char	RdrvLED1_GetCondition(void);
unsigned char	RdrvLED2_GetCondition(void);
unsigned char	RdrvLED3_GetCondition(void);
unsigned char	RdrvLED4_GetCondition(void);
unsigned char	RdrvLED5_GetCondition(void);
unsigned char	RdrvLED6_GetCondition(void);
unsigned char	RdrvLED7_GetCondition(void);


#endif

