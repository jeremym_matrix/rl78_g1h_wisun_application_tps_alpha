/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2016 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      r_flash_wrapper_rl78.c
   \brief     Handling of flash storage for RX651 devices
 */

/******************************************************************************
* Includes
******************************************************************************/
#include "fdl.h"            /* library API                    */
#include "fdl_types.h"      /* library type definition        */
#include "fdl_descriptor.h" /* library descriptor definitions */
#include "eel.h"            /* library API                    */
#include "eel_types.h"      /* library type definition        */
#include "eel_descriptor.h" /* library descriptor definitions */
#include "intrinsics.h"     /* IAR system                     */

#include "r_mac_ie.h"
#include "r_impl_utils.h"
#include "r_nwk_api.h"
#include "r_nwk_api_base.h"
#include "r_nwk_global.h"
#include "r_framesec.h"
#include "r_flash_wrapper.h"
#include "r_mem_tools.h"
#include "sys/ctimer.h"

// R_LOG_PREFIX is processed by RLog code generator
#define R_LOG_PREFIX FLASH
#include "r_log_internal.h"
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
#include "r_loggen_r_flash_wrapper_rl78.h"
#endif

/* compiler settings */
#pragma language = default

/* defines */
typedef enum eel_descriptor_ids_e
{
    HASH_1_ID          =  1,
    HASH_2_ID          =  2,
    HASH_3_ID          =  3,
    HASH_4_ID          =  4,
    HASH_5_ID          =  5,
    HASH_6_ID          =  6,
    HASH_7_ID          =  7,
    FRAME_COUNTER_1_ID =  8,
    FRAME_COUNTER_2_ID =  9,
    FRAME_COUNTER_3_ID = 10,
    FRAME_COUNTER_4_ID = 11,
    FRAME_COUNTER_5_ID = 12,
    FRAME_COUNTER_6_ID = 13,
    FRAME_COUNTER_7_ID = 14
} eel_descriptor_ids_t;

/* import list */
extern __far const fdl_descriptor_t fdl_descriptor_str;
extern __far const eel_u08 eel_descriptor[EEL_VAR_NO + 2];

/* prototype list */
eel_status_t eel_execute(void);

eel_status_t R_EEL_Access(eel_u08* address, eel_u08 id, eel_command_t command);

/* vaiables */
static struct ctimer storeFrameCounterTimer;

static uint16_t frame_counter_offset = 200;

r_nwk_cb_t* p_df_nwkGlobal;  // Global NWK instance used by data flash functions

__near eel_request_t r_eel_request;

/* initialization the flash */
/* ------------------------ */
void R_Flash_Init(void* vp_nwkGlobal)
{
    uint8_t eel_dummy_buffer[4];
    fdl_status_t fdl_status;
    eel_status_t eel_status;

    /* Global NWK reference used by data flash functions */
    p_df_nwkGlobal = (r_nwk_cb_t*)vp_nwkGlobal;
    R_ASSERT_RET_VOID(p_df_nwkGlobal != NULL);

    /* initialization of the EEL request */
    r_eel_request.address_pu08 =   &eel_dummy_buffer[0];
    r_eel_request.identifier_u08 =   0x00;
    r_eel_request.command_enu =   EEL_CMD_UNDEFINED;
    r_eel_request.status_enu =   EEL_OK;

    /* initialize and activate data flash "physically" */
    fdl_status =  FDL_Init(&fdl_descriptor_str);
    if (fdl_status != FDL_OK)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_120(fdl_status);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return;
    }
    FDL_Open();

    /* initialize and activate EEPROM "logically" */
    eel_status =  EEL_Init();
    if (eel_status != EEL_OK)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_129(eel_status);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return;
    }
    EEL_Open();

    /* plausibility check and startup of the EEL data */
    r_eel_request.command_enu =   EEL_CMD_STARTUP;
    r_eel_request.status_enu =   EEL_OK;
    eel_status = eel_execute();

    if (eel_status == EEL_OK)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_141();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
    else if (eel_status == EEL_ERR_POOL_INCONSISTENT)
    {
        R_Flash_Format();
    }
    return;
}


/* Format the flash */
/* ---------------- */
r_result_t R_Flash_Format(void)
{
    uint8_t eel_hash[8];
    eel_status_t eel_status = EEL_OK;
    eel_u32 frame_counter = 0;

    r_result_t res = R_RESULT_FAILED;

    r_eel_request.command_enu = EEL_CMD_SHUTDOWN;
    r_eel_request.status_enu = EEL_OK;
    eel_status |= eel_execute();

    r_eel_request.command_enu = EEL_CMD_FORMAT;
    r_eel_request.status_enu = EEL_OK;
    eel_status |= eel_execute();

    r_eel_request.command_enu = EEL_CMD_STARTUP;
    r_eel_request.status_enu = EEL_OK;
    eel_status |= eel_execute();

    MEMZERO_A(eel_hash);

    for (uint8_t i = 0; i < R_KEY_NUM; i++)
    {
        // pre-init hashes amd frame counters
        eel_status =  R_EEL_Access(&eel_hash[0], HASH_1_ID + i, EEL_CMD_WRITE);
        eel_status =  R_EEL_Access((eel_u08*)&frame_counter, FRAME_COUNTER_1_ID + i, EEL_CMD_WRITE);
    }

    if (eel_status == EEL_OK)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_184();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        res = R_RESULT_SUCCESS;
    }
    else
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_189(eel_status);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
    }

    return res;
}

/* close the flash  */
/* ---------------- */
void R_Flash_Close(void)
{
    /* stop timer for frame counter storage */
    ctimer_stop(&storeFrameCounterTimer);

    /* deactivate EEL "logically" */
    EEL_Close();

    /* deactivate data flash "physically" */
    FDL_Close();
}

/* execution of global EEL request      */
/* ------------------------------------ */
eel_status_t eel_execute(void)
{
    EEL_Execute(&r_eel_request);                                         /* initiate the command      */

    while (r_eel_request.status_enu == EEL_BUSY)
    {
        EEL_Handler();                                                   /* proceed execution         */
    }
    return r_eel_request.status_enu;
}

/* access EEL pool                      */
/* ------------------------------------ */
eel_status_t R_EEL_Access(eel_u08* address, eel_u08 id, eel_command_t command)
{
    eel_status_t eel_status = EEL_OK;

    r_eel_request.address_pu08 =   (eel_u08 __near*)address;
    r_eel_request.command_enu =   command;
    r_eel_request.status_enu =   EEL_OK;
    r_eel_request.identifier_u08 =   id;

    eel_status = eel_execute();

    return eel_status;
}

/* Read data from EEL pool              */
/* ------------------------------------ */
eel_status_t R_EEL_Read(eel_u08* address, eel_u08 id)
{
    eel_status_t eel_status;

    eel_status = R_EEL_Access(address, id, EEL_CMD_READ);

    return eel_status;
}


/* Write Data to EEL pool               */
/* ------------------------------------ */
eel_status_t R_EEL_Write(eel_u08* address, eel_u08 id)
{
    eel_status_t eel_status;

    eel_status = R_EEL_Access(address, id, EEL_CMD_WRITE);

    if (eel_status == EEL_ERR_POOL_FULL)
    {
        eel_status = R_EEL_Access(address, id, EEL_CMD_REFRESH);

        if (eel_status == EEL_OK)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_264();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

            /* write latest data again */
            eel_status = R_EEL_Access(address, id, EEL_CMD_WRITE);

            eel_status = eel_execute();
        }
        else
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_273(eel_status);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        }
    }
    return eel_status;
}


/* get previous stored frame counters from the flash */
/* ------------------------------------------------- */
void R_Flash_SyncFrameCounters(void)
{
    uint8_t eel_hash[8];
    eel_u32 frame_counter;

    eel_status_t eel_status;

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_289();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    for (uint8_t i = 0; i < R_KEY_NUM; i++)
    {
        frame_counter = 0;
        eel_status = EEL_OK;
        // Read stored hashes from EEL pool
        eel_status |=  R_EEL_Read(&eel_hash[0], HASH_1_ID + i);

        // Check if received hashes and stored ones are identical
        if ((MEMEQUAL_A(p_df_nwkGlobal->common.gtkHashesFromBr[i], &eel_hash[0])) && (eel_status == EEL_OK))
        {
            // restore frame counter in case hash is valid
            if (MEMISNOTZERO(&eel_hash[0], 8))
            {
                // Read frame counter from EEL pool
                eel_status |=  R_EEL_Read((eel_u08*)&frame_counter, FRAME_COUNTER_1_ID + i);
                // Add offset
                frame_counter += frame_counter_offset;
            }
        }
        else
        {
            // Store actual hashes to the EEL pool
            eel_status |=  R_EEL_Write(p_df_nwkGlobal->common.gtkHashesFromBr[i], HASH_1_ID + i);
        }
        // Store actual frame counter to the EEL pool
        eel_status |=  R_EEL_Write((eel_u08*)&frame_counter, FRAME_COUNTER_1_ID + i);
        R_FRAMESEC_SetFrameCounter(frame_counter, i);

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_319(i, p_df_nwkGlobal->common.gtkHashesFromBr[i], i, frame_counter);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

        if (eel_status != EEL_OK)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_323(eel_status);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        }
    }
    // start frame counter timer */
    clock_time_t ticks = 600;  // default value = 1 min -> 600 *100ms
    ctimer_set(&storeFrameCounterTimer, ticks, handle_StoreFrameCounter_timer, NULL);
}


/******************************************************************************
   Function Name:       handle_StoreFrameCounter_timer
   Parameters:          void *ptr
   Return value:        none
   Description:         Timer for cyclic frame counter storage to the flash
******************************************************************************/
void handle_StoreFrameCounter_timer(void* unused)
{
    uint8_t eel_hash[8];
    eel_u32 eel_frame_counter;
    uint32_t frame_counter;

    eel_status_t eel_status;

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_346();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    for (uint8_t i = 0; i < R_KEY_NUM; i++)
    {
        eel_status = EEL_OK;
        // Read stored hashes from EEL pool
        eel_status |=  R_EEL_Read(&eel_hash[0], HASH_1_ID + i);
        // Check if actual hashes and EEL pool hashes are identical
        if (MEMDIFFER_A(p_df_nwkGlobal->common.gtkHashesFromBr[i], &eel_hash[0]))
        {
            // update stored hashes
            eel_status |=  R_EEL_Write(p_df_nwkGlobal->common.gtkHashesFromBr[i], HASH_1_ID + i);
        }

        // Read stored frame counter from EEL pool
        eel_status |=  R_EEL_Read((eel_u08*)&eel_frame_counter, FRAME_COUNTER_1_ID + i);
        frame_counter = R_FRAMESEC_GetFrameCounter(i);
        // Check if actual frame counters and EEL pool frame counters are identical
        if (frame_counter != eel_frame_counter)
        {
            // update stored frame counter
            eel_status |=  R_EEL_Write((eel_u08*)&frame_counter, FRAME_COUNTER_1_ID + i);
        }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_370(i, p_df_nwkGlobal->common.gtkHashesFromBr[i], i, frame_counter);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

        if (eel_status != EEL_OK)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_374(eel_status);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        }

    }

    ctimer_reset(&storeFrameCounterTimer);
}


void R_Flash_SetFrameCounterOffset(uint16_t offset)
{
    frame_counter_offset = offset;
}

uint16_t R_Flash_GetFrameCounterOffset()
{
    return frame_counter_offset;
}
