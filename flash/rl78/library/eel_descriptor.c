/*******************************************************************************
* Library       : EEPROM Emulation Library (T02)
*
* File Name     : eel_descriptor.c
* Device(s)     : RL78
* Lib. Version  : V1.00 (for IAR V2)
* Description   : contains the user defined EEL-variable descriptor
*******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2015, 2016 Renesas Electronics Corporation. All rights reserved.
*******************************************************************************/



/*==============================================================================================*/
/* compiler settings                                                                            */
/*==============================================================================================*/
#pragma language = extended


/*==============================================================================================*/
/* include files list                                                                           */
/*==============================================================================================*/
#include  "eel_types.h"
#include  "eel_descriptor.h"
#include  "fdl_types.h"


/* definition of variable types registered at EEL */
#include  "EEL_user_types.h"


/*==============================================================================================*/
/* import list                                                                                  */
/*==============================================================================================*/
/* empty */


/*==============================================================================================*/
/* data definition                                                                              */
/*==============================================================================================*/
/* empty */
  
/*********************************************************************************************************/
/*******                                                                                           *******/
/*******      B E G I N    O F    C U S T O M I Z A B L E    D E C L A R A T I O N    A R E A      *******/
/*******                                                                                           *******/
/*********************************************************************************************************/

#pragma location="EEL_CNST"
#pragma data_alignment=2

__far const eel_u08 eel_descriptor[EEL_VAR_NO+2] =
{
  (eel_u08)(EEL_VAR_NO),                   /* variable count            */  \
  (eel_u08)(sizeof(eel_u08[8])),           /* id=1 -> hash_1            */  \
  (eel_u08)(sizeof(eel_u08[8])),           /* id=2 -> hash_2            */  \
  (eel_u08)(sizeof(eel_u08[8])),           /* id=3 -> hash_3            */  \
  (eel_u08)(sizeof(eel_u08[8])),           /* id=4 -> hash_4            */  \
  (eel_u08)(sizeof(eel_u08[8])),           /* id=5 -> hash_5            */  \
  (eel_u08)(sizeof(eel_u08[8])),           /* id=6 -> hash_6            */  \
  (eel_u08)(sizeof(eel_u08[8])),           /* id=7 -> hash_7            */  \
  (eel_u08)(sizeof(eel_u32)),              /* id=8 -> frame_counter_1   */  \
  (eel_u08)(sizeof(eel_u32)),              /* id=9 -> frame_counter_2   */  \
  (eel_u08)(sizeof(eel_u32)),              /* id=10 -> frame_counter_3   */  \
  (eel_u08)(sizeof(eel_u32)),              /* id=11 -> frame_counter_4   */  \
  (eel_u08)(sizeof(eel_u32)),              /* id=12 -> frame_counter_5   */  \
  (eel_u08)(sizeof(eel_u32)),              /* id=13 -> frame_counter_6   */  \
  (eel_u08)(sizeof(eel_u32)),              /* id=14 -> frame_counter_7   */  \
  (eel_u08)(0x00),                         /* zero terminator  */  \
};

/*********************************************************************************************************/
/*******                                                                                           *******/
/*******      E N D    O F    C U S T O M I Z A B L E    D E C L A R A T I O N    A R E A          *******/
/*******                                                                                           *******/
/*********************************************************************************************************/




/*********************************************************************************************************/
/*******                                                                                           *******/
/*******      B E G I N    O F    U N T O U C H A B L E     D E C L A R A T I O N    A R E A       *******/
/*******                                                                                           *******/
/*********************************************************************************************************/

#pragma location="EEL_CNST"
#pragma data_alignment=2

__far const eel_u08   eel_internal_cfg_cau08[]     = {0x40};

/*********************************************************************************************************/
/*******                                                                                           *******/
/*******      E N D    O F    U N T O U C H A B L E     D E C L A R A T I O N    A R E A           *******/
/*******                                                                                           *******/
/*********************************************************************************************************/
