/***********************************************************************/
/*                                                                     */
/*  FILE        : r_eeprom_wrapper.h                                   */
/*  DATE        : Sep 28, 2020                                         */
/*  DESCRIPTION :                                                      */
/*  CPU TYPE    :                                                      */
/*                                                                     */
/*  NOTE:THIS IS A TYPICAL EXAMPLE.                                    */
/*                                                                     */
/***********************************************************************/
#ifndef R_EEPROM_WRAPPER_H
#define R_EEPROM_WRAPPER_H

sci_iic_return_t r_eeprom_open(void);
sci_iic_return_t r_eeprom_write(uint8_t *addr, uint8_t *data, uint32_t size);
sci_iic_return_t r_eeprom_read(uint8_t *addr, uint8_t *data, uint32_t size);
sci_iic_return_t r_eeprom_close(void);
void r_eeprom_force_stop(void);

#endif /* R_EEPROM_WRAPPER_H */
