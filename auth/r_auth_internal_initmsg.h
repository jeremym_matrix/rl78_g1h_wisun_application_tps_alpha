/**********************************************************************************************************************
 * DISCLAIMER
 * This software is supplied by Renesas Electronics Corporation and is only intended for use with Renesas products. No
 * other uses are authorized. This software is owned by Renesas Electronics Corporation and is protected under all
 * applicable laws, including copyright laws.
 * THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
 * THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT LIMITED TO WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NON-INFRINGEMENT. ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED. TO THE MAXIMUM
 * EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES
 * SHALL BE LIABLE FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR ANY REASON RELATED TO
 * THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
 * Renesas reserves the right, without notice, to make changes to this software and to discontinue the availability of
 * this software. By using this software, you agree to the additional terms and conditions found by accessing the
 * following link:
 * http://www.renesas.com/disclaimer
 *
 * Copyright (C) 2022 Renesas Electronics Corporation. All rights reserved.
 *********************************************************************************************************************/

/**
 * @file r_auth_internal_initmsg.h
 * @brief Internal Wi-SUN Authentication structures for the initial EAP-KEY message (aka Key Request)
 */

#ifndef R_AUTH_INTERNAL_INITMSG_H
#define R_AUTH_INTERNAL_INITMSG_H

#include "includes.h"
#include "common.h"
#include "common/wpa_common.h"
#include "eap_common/eap_defs.h"

/******************************************************************************
* Declarations for the initial EAPOL message defined by Wi-SUN
******************************************************************************/

// Some notes on the standard (20130125-FANWG-FANTPS-1v10):
// The keys form a hierarchy where getting one depends on the higher up key, i.e.,
// (Certificates ->) PMK -> PTK -> GTK
// However, this hierarchy is not reflected in the initial message (6.5.2.2):
// "The Key Data field MAY contain a PMKID KDE, MAY contain a PTKID KDE, and MUST contain a GTKL KDE (in that order)."
// We implement this as follows:
// Supplicant: we send either all KDEs or just the PMKID and the GTKL or (if we have nothing) just the GTKL
// Authenticator: We accept all allowed messages but perform shortcuts only as far as the key hierarchy chain is unbroken

#define OUI_IEEE_802_11 0x000fac
enum
{
    KDE_TYPE            = 0xdd,
    KDE_DATA_TYPE_PMKID = 0x04,
    KDE_ID_SIZE         = 16,
};

#define OUI_WISUN 0x0c5a9e

/** The Wi-SUN KDE header */
struct wisun_kde_s
{
    u8 type; //!< KDE_TYPE = 0xdd
    u8 len;  //!< including oui and data_type
    u8 oui[3];
    u8 data_type;
} STRUCT_PACKED;

/** The Wi-SUN PTK/PMK ID KDE */
struct wisun_pxkid_s
{
    struct wisun_kde_s header;
    u8                 id[KDE_ID_SIZE];
} STRUCT_PACKED;

/** The Wi-SUN GTK Liveness KDE (GTKL) */
struct wisun_gtkl_s
{
    struct wisun_kde_s header;       // oui 0c-5a-9e (Wi-SUN), type 0x02
    u8                 gtk_liveness; // bitmask: 0: GTK-0 is live, ...
} STRUCT_PACKED;

/** The Wi-SUN LGTK Liveness KDE (LGTKL) */
struct wisun_lgtkl_s
{
    struct wisun_kde_s header;        // oui 0c-5a-9e (Wi-SUN), type 0x04
    u8                 lgtk_liveness; // bitmask: 0: LGTK-0 is live, ...
} STRUCT_PACKED;

/** The Wi-SUN Node Role KDE (NR) */
struct wisun_nr_kde_s
{
    struct wisun_kde_s header;    // oui 0c-5a-9e (Wi-SUN), type 0x03
    uint8_t            node_role; // Node Role ID as defined for the Node Role Information Element (NR-IE)
} STRUCT_PACKED;

/** The EAPOL header */
struct eapol_header_s
{
    u8 version;
    u8 type;
    u8 length[2];  // big-endian
} STRUCT_PACKED;

/** Fields from wpa_eapol_key that are variable length in WPA 2.7 */
struct wpa_eapol_key_extra
{
    u8 key_mic[16];
    u8 key_data_length[2];  /* big endian */
} STRUCT_PACKED;

/** The Wi-SUN initial authentication message */
struct wisun_first_message_s
{
    struct eapol_header_s      eapol_header;
    struct wpa_eapol_key       header;
    struct wpa_eapol_key_extra header_extra;  // these have been removed from header in WPA 2.7 (considered variable length)
    union
    {
        struct
        {
            struct wisun_pxkid_s pmkid;
            struct wisun_pxkid_s ptkid;
            struct wisun_gtkl_s  gtkl;
        } all;
        struct
        {
            struct wisun_pxkid_s pxkid;
            struct wisun_gtkl_s  gtkl;
        } with_pxkid;
        struct
        {
            /* 22 bytes pmkid + 22 bytes ptkid + 7 bytes gtkl + 7 bytes nr + 7 bytes lgtkl = 65 bytes */
            uint8_t bytes[65];
        } raw;
        struct wisun_gtkl_s gtkl;
    } data;
} STRUCT_PACKED;

/** The size of the EAPOL Key Request message (first authentication message) without any KDEs (i.e. the minimal size) */
#define WISUN_KEY_REQUEST_SIZE_WITHOUT_KDES (sizeof(struct eapol_header_s) + sizeof(struct wpa_eapol_key) + sizeof(struct wpa_eapol_key_extra))

/** The first parts of the EAPOL message with the EAP header or the EAPOL key header */
struct eapol_message_s
{
    struct eapol_header_s eapol;
    union
    {
        struct wpa_eapol_key key;
        struct
        {
            struct eap_hdr hdr;
            uint8_t        type;
        } eap;
    } d;
} STRUCT_PACKED;

// allows accessing of eap code and eapol-key key-info
#define EAPOL_MESSAGE_MINSIZE (8)

void R_AUTH_InternalCalcKdeId(uint8_t* id, const uint8_t* key, uint8_t isPtk, const uint8_t* authenticator, const uint8_t* supplicant);
int  R_AUTH_InternalVerifyKdeId(const uint8_t* id, const uint8_t* key, uint8_t isPtk, const uint8_t* authenticator, const uint8_t* supplicant);

#endif /* R_AUTH_INTERNAL_INITMSG_H */
