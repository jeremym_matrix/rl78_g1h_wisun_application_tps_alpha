/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      r_auth_internal.c
   \version   1.00
   \brief     Wi-SUN FAN Authentication internal and common helper functions
 */

#include <string.h>
#include "r_auth_common.h"
#include "r_auth_internal.h"
#include "r_auth_internal_initmsg.h"
#include "r_nwk_api.h"

#include "mbedtls/ctr_drbg.h"
#include "mbedtls/md.h"
#include "mbedtls/md_internal.h"

#include "common/eapol_common.h"

#undef ARRAY_SIZE
#undef BIT
#include "r_impl_utils.h"

#if !R_DEV_DETERMINISTIC_RANDOM
#include "lib/random.h"
#endif

#include "r_table_config.h"

// R_LOG_PREFIX is processed by RLog code generator
#define R_LOG_PREFIX AUTH
#include "r_log_internal.h"
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
#include "r_loggen_r_auth_internal.h"
#endif

static mbedtls_ctr_drbg_context ctr_drbg_context;
static uint8_t is_initialized;

/**
 * Fill the provided buffer with entropy
 * @param data (unused) user pointer
 * @param output the entropy buffer
 * @param len size of the entropy buffer
 * @return 0 on success
 */
static int entropy_func(void* data, unsigned char* output, size_t len)
{
#if R_DEV_DETERMINISTIC_RANDOM
    UNUSED_SYM(data);
    memset(output, 1, len);
#else
    for (size_t i = 0; i < len; i++)
    {
        *output++ = random_rand();
    }
#endif
    return 0;
}

/**
 * Fill the provided buffer with random data
 * @param p_rng (unused) pointer to the random generator
 * @param output the buffer
 * @param output_len the size of the buffer
 * @return 0 on success
 */
int R_AUTH_InternalRandom(void* UNUSEDP(p_rng), unsigned char* output, size_t output_len)
{
    if (output == NULL)
    {
        return -1;
    }

    return mbedtls_ctr_drbg_random(&ctr_drbg_context, output, output_len);
}


/**
 * Reset state used by both the Supplicant and the BR
 */
void R_AUTH_InternalReset()
{
    if (is_initialized)
    {
        mbedtls_ctr_drbg_free(&ctr_drbg_context);
    }
    mbedtls_ctr_drbg_init(&ctr_drbg_context);
    mbedtls_ctr_drbg_seed(&ctr_drbg_context, entropy_func, NULL, NULL, 0);
    is_initialized = 1;
}

r_result_t R_AUTH_SetAuthStateCache(r_apl_global_t* nwk)
{
    if (!nwk)
    {
        return R_RESULT_INVALID_PARAMETER;
    }
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_122(nwk->auth.gtks_valid, nwk->auth.sup.gtks_live, nwk->auth.sup.authenticator);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    r_nwk_auth_state_cache_t nwk_cache;
    nwk_cache.sup.gtks_live = nwk->auth.sup.gtks_live;
    MEMCPY_A(nwk_cache.sup.authenticator, nwk->auth.sup.authenticator);
    nwk_cache.gtks_valid = nwk->auth.gtks_valid;
    return R_NWK_SetRequest(R_NWK_authStateCache, &nwk_cache, sizeof(nwk_cache));
}

/**
 * Calculate or check a KDE ID
 *
 * Spec: Truncate-128(HMAC-SHA1(PMK, "PMK Name" || AA || SPA))
 * or Truncate-128(HMAC-SHA1(PTK, "PTK Name" || AA || SPA))
 * where
 * - AA is the EUI-64 of the Border Router
 * - SPA is the EUI-64 of the FAN node
 * @param verify verify or calculate
 * @param id the received KDE ID (verify) or the KDE ID buffer
 * @param key PMK or PTK
 * @param isPtk key is a PTK (instead of a PMK)
 * @param authenticator the authenticator EUI-64
 * @param supplicant the supplicant EUI-64
 * @return 0 on success
 */
static int calcOrCheckKdeId(int verify, uint8_t* id, const uint8_t* key, uint8_t isPtk, const uint8_t* authenticator, const uint8_t* supplicant)
{
    static const char* PMK_NAME = "PMK Name";
#define PMK_NAME_LEN 8
    uint8_t hash[20];
    uint8_t input[PMK_NAME_LEN + 8 + 8];  // "PMK Name" || AA || SPA
    memcpy(input, PMK_NAME, PMK_NAME_LEN);
    if (isPtk)
    {
        input[1] = 'T';
    }
    memcpy(input + PMK_NAME_LEN, authenticator, 8);
    memcpy(input + PMK_NAME_LEN + 8, supplicant, 8);
    int res = mbedtls_md_hmac(&mbedtls_sha1_info, key, isPtk ? R_AUTH_PTK_SIZE : R_AUTH_PMK_SIZE, input, sizeof(input), hash);
    LOG_ONLY_VAR(res);
    if (res)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_164((int32_t)res);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
    }
    if (verify)
    {
        return memcmp(hash, id, 128 / 8) == 0;
    }
    memcpy(id, hash, 128 / 8);
    return 0;
#undef PMK_NAME_LEN
}

/**
 * Calculate the KDE ID (@see calcOrCheckKdeId)
 * @param id the output buffer
 * @param key the PMK or PTK
 * @param isPtk key is a PTK (instead of a PMK)
 * @param authenticator the authenticator EUI-64
 * @param supplicant the supplicant EUI-64
 */
void R_AUTH_InternalCalcKdeId(uint8_t* id, const uint8_t* key, uint8_t isPtk, const uint8_t* authenticator, const uint8_t* supplicant)
{
    if (id && key && authenticator && supplicant)
    {
        calcOrCheckKdeId(0, id, key, isPtk, authenticator, supplicant);
    }
}

/**
 * Verify the KDE ID (@see calcOrCheckKdeId)
 * @param id the received KDE ID
 * @param key the PMK or PTK
 * @param isPtk key is a PTK (instead of a PMK)
 * @param authenticator the authenticator EUI-64
 * @param supplicant the supplicant EUI-64
 * @return 1 if the received KDE ID is correct
 */
int R_AUTH_InternalVerifyKdeId(const uint8_t* id, const uint8_t* key, uint8_t isPtk, const uint8_t* authenticator, const uint8_t* supplicant)
{
    if (id && key && authenticator && supplicant)
    {
        return calcOrCheckKdeId(1, (uint8_t*)id, key, isPtk, authenticator, supplicant);
    }
    return 0;
}

r_result_t R_AUTH_SetGTK(r_apl_global_t* nwk, uint8_t igtk, const uint8_t gtk[16])
{
    if (!(nwk && igtk < R_KEY_NUM))
    {
        return R_RESULT_INVALID_PARAMETER;
    }
    // ignore identical
    if (gtk != NULL && GTK_IS_VALID(igtk) && MEMEQUAL_A(GTK(igtk), gtk))
    {
        return R_RESULT_SUCCESS;
    }
    if (gtk == NULL)
    {
#if R_DEV_FIXED_GTKS
        static const uint8_t fixed_gtk[] = { R_AUTH_FIXED_GTK0 };
        MEMCPY_A(GTK(igtk), fixed_gtk);
        // we want easy Wireshark support but the GTKs must change or otherwise the supplicant does not care
        // since we loop around the entries and there is some time between deletion and re-adding of the same index,
        // this should be enough
        GTK(igtk)[0] = 1 + igtk;
#else
        R_AUTH_InternalRandom(NULL, GTK(igtk), sizeof(GTK(igtk)));
#endif
    }
    else
    {
        memcpy(GTK(igtk), gtk, sizeof(GTK(igtk)));
    }
    nwk->auth.gtks_valid |= 1 << igtk;
    R_AUTH_SetAuthStateCache(nwk);

#if R_BR_AUTHENTICATOR_ENABLED
    for (size_t i = 0; i < r_auth_br_supplicants_size_glb; i++)
    {
        nwk->auth.br.supplicants[i].gtks_valid &= ~(1 << igtk);
    }

    // The expiration time of a GTK is calculated as the expiration time of the GTK most recently installed at the Border Router plus GTK_EXPIRE_OFFSET.
    clock_time_t currentClockSecs = clock_seconds();
    uint32_t maxExpiration = currentClockSecs;  // at least start from now

    uint8_t startIndex = 0;
    uint8_t endIndex = R_AUTH_NUM_GTKS;  // Consider only GTK lifetimes, not LGTKs -> end loop at last GTK index
    if (igtk >= R_AUTH_NUM_GTKS)
    {
        /* If the new GTK is an LGTK, we need to adapt the loop parameters */
        startIndex = R_AUTH_NUM_GTKS; // Consider only LGTK lifetimes, not GTKs -> Start loop at first LGTK index
        endIndex = R_KEY_NUM;         // Consider only LGTK lifetimes, not GTKs -> End loop at last key index
    }
    for (uint8_t i = startIndex; i < endIndex; i++)
    {
        if (GTK_IS_VALID(i) && nwk->auth.br.gtk_expiration_seconds[i] > maxExpiration)
        {
            maxExpiration = nwk->auth.br.gtk_expiration_seconds[i];
        }
    }
    nwk->auth.br.gtk_expiration_seconds[igtk] = maxExpiration + 60 * nwk->auth.br.key_lifetimes.gtk_lifetime_minutes;

    uint8_t deviceType;
    r_result_t requestResult = R_NWK_GetRequest(R_NWK_deviceType, &deviceType, sizeof(deviceType));
    if (requestResult == R_RESULT_SUCCESS && deviceType == R_BORDERROUTER)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_271(igtk, GTK(igtk), nwk->auth.br.gtk_expiration_seconds[igtk], nwk->auth.gtks_valid, currentClockSecs, maxExpiration);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
#endif /* if R_BR_AUTHENTICATOR_ENABLED */

    r_nwk_framesec_gtk_t nwk_framesec_gtk;
    nwk_framesec_gtk.index = igtk;
    nwk_framesec_gtk.remove = R_FALSE;
    MEMCPY_A(nwk_framesec_gtk.gtk, GTK(igtk));
    r_result_t res = R_NWK_SetRequest(R_NWK_framesecGtk, &nwk_framesec_gtk, sizeof(nwk_framesec_gtk));
    if (res != R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_282(igtk, (uint8_t)res);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return res;
    }
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_285(igtk);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    return R_RESULT_SUCCESS;
}

r_result_t R_AUTH_DeleteGTKs(r_apl_global_t* nwk, uint8_t mask)
{
    for (uint8_t i = 0; i < R_KEY_NUM; i++)
    {
        if (mask & 1u << i)
        {
            r_result_t res = R_AUTH_DeleteGTK(nwk, i);
            if (res != R_RESULT_SUCCESS)
            {
                return res;
            }
        }
    }
    return R_RESULT_SUCCESS;
}

r_result_t R_AUTH_DeleteGTK(r_apl_global_t* nwk, uint8_t igtk)
{
    if (!(nwk && igtk < R_KEY_NUM))
    {
        return R_RESULT_INVALID_PARAMETER;
    }

    MEMZERO_A(GTK(igtk));
    r_nwk_framesec_gtk_t nwk_framesec_gtk;
    nwk_framesec_gtk.index = igtk;
    nwk_framesec_gtk.remove = R_TRUE;
    r_result_t res = R_NWK_SetRequest(R_NWK_framesecGtk, &nwk_framesec_gtk, sizeof(nwk_framesec_gtk));
    if (res != R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_319(igtk, (uint8_t)res);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return res;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_323(igtk);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    nwk->auth.gtks_valid &= ~(1U << igtk);
    R_AUTH_SetAuthStateCache(nwk);

#if R_BR_AUTHENTICATOR_ENABLED
    nwk->auth.br.gtk_expiration_seconds[igtk] = 0;
#endif

    return R_RESULT_SUCCESS;
}

r_mac_eapol_type_t R_AUTH_DetectEapolType(const void* eapol_message)
{
    if (eapol_message == NULL)
    {
        return R_EAPOL_TYPE_8021X;
    }

    struct eapol_message_s* msg = (struct eapol_message_s*)eapol_message;
    uint16_t keyInfo = WPA_GET_BE16(msg->d.key.key_info);
    r_mac_eapol_type_t type = R_EAPOL_TYPE_8021X;
    if (msg->eapol.type == IEEE802_1X_TYPE_EAPOL_KEY && !(keyInfo & WPA_KEY_INFO_REQUEST))
    {
        type = keyInfo & WPA_KEY_INFO_KEY_TYPE ? R_EAPOL_TYPE_4WH : R_EAPOL_TYPE_GKH;  // 'pairwise' bit
    }
    return type;
}

r_result_t R_AUTH_CreateMessage(r_mac_eapol_type_t intype, const uint8_t* data, uint16_t datasize, uint8_t* buf, uint16_t* bufsize, r_mac_eapol_type_t* outtype)
{
    if (!(data && buf && bufsize && outtype && *bufsize >= 4))
    {
        return R_RESULT_INVALID_PARAMETER;
    }
    uint8_t* d = buf;
    uint16_t len = 0;
    *outtype = intype;
    switch (intype)
    {
        case R_EAPOL_TYPE_CONVERT_FROM_EAP:
            d[0] = 0x03;  // version (spec v17: 3)
            d[1] = 0x00;  // type: EAP
            d[2] = (uint8_t)(datasize >> 8u);
            d[3] = (uint8_t)(datasize);
            d += 4;
            len += 4;
            *outtype = R_EAPOL_TYPE_8021X;
            break;

        case R_EAPOL_TYPE_CONVERT_FROM_4WH_OR_GKH:
            *outtype = R_EAPOL_TYPE_GKH;
            if (data[6] & (1 << 3))  // 'pairwise' bit of 'key info' byte
            {
                *outtype = R_EAPOL_TYPE_4WH;
            }
            break;

        default:
            break;
    }

    if (*bufsize < len + datasize)
    {
        return R_RESULT_INSUFFICIENT_BUFFER;
    }
    memcpy(d, data, datasize);
    *bufsize = len + datasize;
    return R_RESULT_SUCCESS;
}
