/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      r_auth_br.c
   \version   1.00
   \brief     Wi-SUN FAN Authenticator (for Border Router) implementation
 */

#include "r_auth_config.h"

#if R_BR_AUTHENTICATOR_ENABLED

#include <string.h>
#include "r_impl_utils.h"
#include "r_byte_swap.h"
#undef ARRAY_SIZE  /* redefined in WPA includes */
#include "r_auth_br.h"
#include "r_auth_common.h"
#include "r_auth_internal.h"
#include "r_auth_internal_initmsg.h"
#include "r_auth_certs_config.h"

#if R_EAP_TLS_SERVER_ENABLED
#include "r_auth_br_eap_tls.h"
#endif

// WPA includes
#include "ap/wpa_auth.h"
#include "ap/wpa_auth_i.h"

#include "r_table_config.h"

// R_LOG_PREFIX is processed by RLog code generator
#define R_LOG_PREFIX AUTH
#include "r_log_internal.h"
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
#include "r_loggen_r_auth_br.h"
#endif

/** The state of the border router */
#define BR  (&nwk->auth.br)

/** The state of the currently active supplicant */
#define SUP (&BR->supplicants[BR->current_supplicant])

/******************************************************************************
* Debugging & Development
******************************************************************************/

// allow easy changing for debugging
#define MSG_UNUSED_CB MSG_DEBUG
#define unused_marker(x) do {} while (0)

/******************************************************************************
* Wi-SUN Initial Message Functionality
******************************************************************************/

static void rsn_save_state(r_apl_global_t* nwk);
static void rsn_restore_state(r_apl_global_t* nwk);
static void update_border_router_state(r_apl_global_t* nwk);
static void retry_blocked_eap_tls_handshakes(r_apl_global_t* nwk);

/******************************************************************************
* General EAPOL and initial "Key" message Functionality
******************************************************************************/

/** Return true, if the key with the specified lifetime timestamp (in minutes!) is expired. False otherwise. */
static r_boolean_t is_expired(uint32_t expirationMinutes)
{
    return expirationMinutes <= (clock_seconds() / 60);  // use <= to handle clock_seconds==0
}

static r_result_t process_eapol_key_message(r_apl_global_t* nwk, uint16_t size, struct wisun_first_message_s* msg)
{
    // validation
    if (msg->eapol_header.version != 3 || size < WISUN_KEY_REQUEST_SIZE_WITHOUT_KDES ||
        WPA_GET_BE16(msg->eapol_header.length) != size - sizeof(msg->eapol_header) ||
        msg->header.type != 2 ||
        WPA_GET_BE16(msg->header.key_info) != (2 | WPA_KEY_INFO_REQUEST)
        )
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_104();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return R_RESULT_INVALID_PARAMETER;
    }

    struct wisun_pxkid_s* pmkid = NULL;
    struct wisun_pxkid_s* ptkid = NULL;
    struct wisun_gtkl_s* gtkl = NULL;
    struct wisun_nr_kde_s* nodeRoleKde = NULL;
    struct wisun_lgtkl_s* lgtklKde = NULL;
    uint8_t dataIndex = 0;
    while (dataIndex < (size - WISUN_KEY_REQUEST_SIZE_WITHOUT_KDES))
    {
        struct wisun_kde_s* kde = (struct wisun_kde_s*)&msg->data.raw.bytes[dataIndex];
        const uint32_t oui = WPA_GET_BE24(kde->oui);
        uint16_t fullKdeLen = 1 + 1 + kde->len;  // 1 byte Type + 1 byte Length + Value of "Length" field
        dataIndex += fullKdeLen;

        if (kde->type != KDE_TYPE)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_123(kde->type);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            continue;
        }

        if (oui == OUI_IEEE_802_11 && kde->data_type == KDE_DATA_TYPE_PMKID && fullKdeLen == sizeof(struct wisun_pxkid_s))
        {
            pmkid = (struct wisun_pxkid_s*)kde;
        }
        else if (oui == OUI_WISUN && kde->data_type == KDT_PTKID && fullKdeLen == sizeof(struct wisun_pxkid_s))
        {
            ptkid = (struct wisun_pxkid_s*)kde;
        }
        else if (oui == OUI_WISUN && kde->data_type == KDT_GTKL && fullKdeLen == sizeof(struct wisun_gtkl_s))
        {
            gtkl = (struct wisun_gtkl_s*)kde;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_138(gtkl->gtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        }
        else if (oui == OUI_WISUN && kde->data_type == KDT_NR && fullKdeLen == sizeof(struct wisun_nr_kde_s))
        {
            nodeRoleKde = (struct wisun_nr_kde_s*)kde;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_143(nodeRoleKde->node_role);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        }
        else if (oui == OUI_WISUN && kde->data_type == KDT_LGTKL && fullKdeLen == sizeof(struct wisun_lgtkl_s))
        {
            lgtklKde = (struct wisun_lgtkl_s*)kde;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_148(lgtklKde->lgtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        }
        else
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_152(oui, kde->data_type, kde->len);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            continue;
        }
    }

    /* FAN 1.0 requires the GTKL KDE to be present; FAN 1.1 requires the NR KDE to be present */
    if (!gtkl && !lgtklKde)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_160();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return R_RESULT_INVALID_PARAMETER;
    }

    uint8_t eui64[R_MAC_EXTENDED_ADDRESS_LENGTH];
    r_result_t requestResult = R_NWK_GetRequest(R_NWK_macExtendedAddress, &eui64, sizeof(eui64));
    if (requestResult != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    R_Swap64(eui64, eui64);

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_172(SUP->address, SUP->pmk_expiration_minutes ? 1 : 0, SUP->ptk_expiration_minutes ? 1 : 0, SUP->gtks_valid, pmkid ? 1 : 0, ptkid ? 1 : 0);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    /* Set SUP device type; If the SUP does not send a Node Role KDE, we consider it as FAN 1.0 node */
    SUP->node_role = nodeRoleKde ? nodeRoleKde->node_role : R_AUTH_NODE_ROLE_ID_FAN10_RN;

    if (is_expired(SUP->pmk_expiration_minutes) || !pmkid || !R_AUTH_InternalVerifyKdeId(pmkid->id, SUP->pmk, 0, eui64, SUP->address))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_179(SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        SUP->pmk_expiration_minutes = 0;
        SUP->ptk_expiration_minutes = 0;
    }
    if (is_expired(SUP->ptk_expiration_minutes) || !ptkid || !R_AUTH_InternalVerifyKdeId(ptkid->id, SUP->ptk, 1, eui64, SUP->address))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_185(SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        SUP->ptk_expiration_minutes = 0;
    }

    if (gtkl)
    {
        /* Set only GTK liveness bits (in SUP state) and leave the LGTK liveness bits untouched */
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_192(gtkl->gtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        SUP->gtks_valid &= ~R_AUTH_GTK_LIVENESS_MASK;                       // Clear all GTK liveness bits
        SUP->gtks_valid |= (gtkl->gtk_liveness & R_AUTH_GTK_LIVENESS_MASK); // Set GTK liveness bits from GTKL
    }
    if (lgtklKde)
    {
        /* Set only LGTK liveness bits (in SUP state) and leave the GTK liveness bits untouched */
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_199(lgtklKde->lgtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        SUP->gtks_valid &= ~R_AUTH_LGTK_LIVENESS_MASK;                                                 // Clear all LGTK liveness bits
        SUP->gtks_valid |= ((lgtklKde->lgtk_liveness << R_AUTH_NUM_GTKS) & R_AUTH_LGTK_LIVENESS_MASK); // Set LGTK liveness bits from LGTKL
    }

    // even if everything matches, the SUP might need an updated GTKL (if a GTK is deleted instead of replaced)
    if (SUP->node_role == R_AUTH_NODE_ROLE_ID_FAN10_RN)
    {
        uint8_t brGtkLiveness = nwk->auth.gtks_valid & R_AUTH_GTK_LIVENESS_MASK;
        if (SUP->pmk_expiration_minutes && SUP->ptk_expiration_minutes && SUP->gtks_valid == brGtkLiveness)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_210(brGtkLiveness, SUP->gtks_valid);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            return R_AUTH_BR_StartGKH(nwk, R_KEY_NUM);  // index to indicate 'send GTKL' only
        }
    }

    r_result_t res = R_AUTH_BR_ContinueAuthentication(nwk);
    if (res == R_RESULT_NOTHING_TO_DO)
    {
        res = R_RESULT_SUCCESS;  // Indicate success to callers if the supplicant already has all keys
    }
    return res;
}

/**
 * Process an incoming EAP message
 * @param nwk the central network structure
 * @param src the src EUI-64 of the message
 * @param data the message
 * @param size the size of the message
 * @return R_RESULT_SUCCESS if the message has been processed
 */
static r_result_t process_eap_message(r_apl_global_t* nwk, const uint8_t* src, const void* data, uint16_t size)
{
    if ((nwk == NULL) || (data == NULL))
    {
        return R_RESULT_INVALID_PARAMETER;
    }

    struct wisun_first_message_s* msg = (struct wisun_first_message_s*)data;
    // Wi-SUN specific initial EAPOL-KEY message?
    if (msg->eapol_header.type == IEEE802_1X_TYPE_EAPOL_KEY)
    {
        return process_eapol_key_message(nwk, size, msg);
    }

#if R_EAP_TLS_SERVER_ENABLED

    /* EAP-TLS handshake is performed by Renesas EAP-TLS server library */
    return R_EAP_TLS_SRV_ProcessMessage(src, data, size, &nwk->eap_tls_srv) == R_EAP_TLS_SRV_RESULT_SUCCESS ? R_RESULT_SUCCESS : R_RESULT_FAILED;
#else
    return R_RESULT_UNSUPPORTED_FEATURE;  // Alternative implementation to Renesas EAP-TLS library may be provided here
#endif
}

/******************************************************************************
* Multi-supplicant management Functions
******************************************************************************/

static void update_border_router_state(r_apl_global_t* nwk)
{
    uint32_t totalSupplicants = 0;
    uint32_t retryPendingSupplicants = 0;
    uint32_t pmkSupplicants = 0;
    uint32_t ptkSupplicants = 0;
    for (uint16_t i = 0; i < r_auth_br_supplicants_size_glb; i++)
    {
        if (!MEMISZERO_A(BR->supplicants[i].address))
        {
            totalSupplicants++;
#if 0
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_270(i, BR->supplicants[i].address, BR->supplicants[i].pmk_expiration_minutes, BR->supplicants[i].ptk_expiration_minutes, BR->supplicants[i].gtks_valid, BR->supplicants[i].key_replay.counter);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif
            if (BR->supplicants[i].retryPending)
            {
                retryPendingSupplicants++;
            }
            if (MEMISNOTZERO_A(BR->supplicants[i].pmk))
            {
                pmkSupplicants++;
            }
            if (MEMISNOTZERO_A(BR->supplicants[i].ptk))
            {
                ptkSupplicants++;
            }
        }
    }
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_286(totalSupplicants, retryPendingSupplicants, pmkSupplicants, ptkSupplicants);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    /* If gap between PMKs and PTKs is sufficiently small, start new EAP TLS handshake. */
    if (pmkSupplicants <= ptkSupplicants + R_AUTH_EAP_TLS_PTK_PMK_GAP_SIZE)
    {
        retry_blocked_eap_tls_handshakes(nwk);
    }
}

/**
 * Set the index of the current supplicant to the corresponding supplicant table entry for the specified EUI-64
 * @param nwk the central network structure
 * @param address the EUI-64 of the supplicant
 * @return R_RESULT_SUCCESS if the new supplicant is selected
 */
static r_result_t switch_supplicant(r_apl_global_t* nwk, const uint8_t* address)
{
    if ((nwk == NULL) || (address == NULL))
    {
        return R_RESULT_INVALID_PARAMETER;
    }

    if (MEMDIFFER_A(SUP->address, address))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_310(SUP->address, SUP->pmk_expiration_minutes, SUP->ptk_expiration_minutes, SUP->gtks_valid, nwk->auth.gtks_valid);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

        uint32_t oldestTime = ~(uint32_t)0;
        size_t found = r_auth_br_supplicants_size_glb;
        size_t oldest = r_auth_br_supplicants_size_glb;
        size_t unusedSlot = r_auth_br_supplicants_size_glb;
        for (uint16_t i = 0; i < r_auth_br_supplicants_size_glb; i++)
        {
            if (MEMEQUAL_A(BR->supplicants[i].address, address))
            {
                found = i;
                break;
            }

            if (MEMISZERO_A(BR->supplicants[i].address))
            {
                unusedSlot = i;
                break;  // Use first free slot for new supplicant to avoid gaps in SUP array
            }
            else if (BR->supplicants[i].last_rx < oldestTime)
            {
                /* Evict oldest entry (always succeeds); last_rx is 32 bits of clock seconds so the value range is
                 * about 136 years -> no worries about wraparound */
                oldest = i;
                oldestTime = BR->supplicants[i].last_rx;
            }
        }

        if (found < r_auth_br_supplicants_size_glb)
        {
            BR->current_supplicant = (uint16_t)found;
        }
        else
        {
            /* We don't know the SUP yet -> Create new entry */
            if (unusedSlot < r_auth_br_supplicants_size_glb)
            {
                BR->current_supplicant = (uint16_t)unusedSlot;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                r_loggen_348(address, BR->current_supplicant);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            }
            else
            {
                BR->current_supplicant = (uint16_t)oldest;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                r_loggen_353(SUP->address, address, BR->current_supplicant);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            }

            MEMZERO_S(SUP);
            MEMCPY_A(SUP->address, address);
            SUP->relay[0] = 0xff;
        }
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_362(SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    return R_RESULT_SUCCESS;
}

/******************************************************************************
* RSN (4-Way-Handshake and Group Key Handshake) Functionality
******************************************************************************/

typedef struct r_auth_br_rsn_ctx_s
{
    struct wpa_authenticator* wpa_auth;
    struct wpa_state_machine* wpa_sm;

    struct wpa_auth_callbacks cb;
} r_auth_br_rsn_ctx_t;

static struct wpa_auth_config rsn_wpa_auth_config = {
    .wpa                           = 2,                      // test: 2
    .wpa_key_mgmt                  = WPA_KEY_MGMT_IEEE8021X, // test: 1
    .wpa_pairwise                  = WPA_CIPHER_TKIP,        // test: 8
    .wpa_group                     = WPA_CIPHER_CCMP,        // test: 16
    .wpa_group_rekey               = 600,                    // test: 600
    .wpa_strict_rekey              = 0,                      // test: 0
    .wpa_gmk_rekey                 = 86400,                  // test: 86400
    .wpa_ptk_rekey                 = 0,                      // test: 0
    .wpa_group_update_count        = 1,
    .wpa_pairwise_update_count     = 1,
    .wpa_disable_eapol_key_retries = 0,
    .rsn_pairwise                  = WPA_CIPHER_CCMP, // test: 16
    .rsn_preauth                   = 0,               // test: 0
    .eapol_version                 = 3,               // test: 2, Spec v17: 3
    .wmm_enabled                   = 1,               // test: 1
    .wmm_uapsd                     = 0,               // test: 0
    .disable_pmksa_caching         = 1,               // test: 0
    .okc                           = 0,               // test: 0
    .tx_status                     = 1,               // test: 1
    .disable_gtk                   = 0,
    .ap_mlme                       = 0,
};

#if 0  /* For debugging only */
static const char* eap_set_strings[] = {
    "WPA_EAPOL_portEnabled",
    "WPA_EAPOL_portValid",
    "WPA_EAPOL_authorized",
    "WPA_EAPOL_portControl_Auto",
    "WPA_EAPOL_keyRun",
    "WPA_EAPOL_keyAvailable",
    "WPA_EAPOL_keyDone",
    "WPA_EAPOL_inc_EapolFramesTx"
};

static void rsncb_set_eapol(void* ctx, const u8* addr, wpa_eapol_variable var, int value)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_set_eapol: addr: %02x%02x..., var: %s, value: %d", addr[0], addr[1], eap_set_strings[var], value);
}
#endif

// unused callbacks
static void rsncb_disconnect(void* ctx, const u8* addr, u16 reason)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_disconnect: addr: %02x%02x..., reason: %d", addr[0], addr[1], reason);
    unused_marker("rsncb_disconnect");
}
static int rsncb_mic_failure_report(void* ctx, const u8* addr)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_mic_failure_report: addr: %02x%02x...", addr[0], addr[1]);
    unused_marker("rsncb_mic_failure_report");
    return 0;
}
static void rsncb_psk_failure_report(void* ctx, const u8* addr)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_psk_failure_report: addr: %02x%02x...", addr[0], addr[1]);
    unused_marker("rsncb_psk_failure_report");
}
static void rsncb_set_eapol(void* ctx, const u8* addr, wpa_eapol_variable var, int value)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_set_eapol: addr: %02x%02x..., var: %d, value: %d", addr[0], addr[1], var, value);
    unused_marker("rsncb_set_eapol");
}
static const u8* rsncb_get_psk(void* ctx, const u8* addr, const u8* p2p_dev_addr, const u8* prev_psk, size_t* psk_len)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_get_psk: addr: %02x%02x..., p2p_dev_addr: %02x%02x..., prev_psk: %02x%02x...", addr[0], addr[1], p2p_dev_addr[0], p2p_dev_addr[1], prev_psk[0], prev_psk[1]);
    unused_marker("rsncb_get_psk");
    return NULL;
}
static int rsncb_get_seqnum(void* ctx, const u8* addr, int idx, u8* seq)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_get_seqnum");
    unused_marker("rsncb_get_seqnum");
    return 0;
}
static int rsncb_send_ether(void* ctx, const u8* dst, u16 proto, const u8* data, size_t data_len)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_send_ether: dst: %02x%02x..., proto: %d, data: %02x%02x..., data_len: %zu", dst[0], dst[1], proto, data[0], data[1], data_len);
    unused_marker("my_send_ether");
    return 0;
}
static int rsncb_for_each_sta(void* ctx, int (* cb)(struct wpa_state_machine* sm, void* ctx), void* cb_ctx)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_for_each_sta");
    unused_marker("rsncb_for_each_sta");
    return 0;
}
static int rsncb_for_each_auth(void* ctx, int (* cb)(struct wpa_authenticator* a, void* ctx), void* cb_ctx)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_for_each_auth");
    unused_marker("rsncb_for_each_auth");
    return 0;
}

// unused on nodes
static void rsncb_logger(void* ctx, const u8* addr, logger_level level, const char* txt)
{
    wpa_printf(MSG_DEBUG + level, "%s: %02x:%02x:%02x:%02x:%02x:%02x:%02x:%02x", txt, addr[0], addr[1], addr[2], addr[3], addr[4], addr[5], addr[6], addr[7]);
}

/**
 * WPA callback to get the current value if relevant EAPOL variables
 * @param ctx the WPA context
 * @param addr the supplicant address
 * @param var the variable
 * @return the value of the variable or -1 if not supported
 */
static int rsncb_get_eapol(void* ctx, const u8* addr, wpa_eapol_variable var)
{
    wpa_printf(MSG_DEBUG, "rsncb_get_eapol: addr: %02x%02x..., var: %d", addr[0], addr[1], var);
    switch (var)
    {
        case WPA_EAPOL_keyRun:  // fall-through
        case WPA_EAPOL_keyAvailable:
            return TRUE;

        default:
            return -1;
    }
}

/**
 * WPA callback to retrieve the shared secret (PMK for our use case)
 * @param ctx the WPA context
 * @param addr the address of the supplicant
 * @param msk buffer for the PMK
 * @param[out] len the length of the PMK
 * @return 0 on success
 */
static int rsncb_get_msk(void* ctx, const u8* addr, u8* msk, size_t* len)
{
#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_511(addr[0], addr[1]);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif
    r_apl_global_t* nwk = ctx;

    if (len == NULL)
    {
        return -1;
    }

    if (!(ctx && msk && SUP->pmk_expiration_minutes))
    {
        *len = 0;
        return -1;
    }
    *len = sizeof(SUP->pmk);
    memcpy(msk, SUP->pmk, sizeof(SUP->pmk));
    return 0;

}

/**
 * WPA callback to set the keys
 * @param ctx the WPA context
 * @param vlan_id unused
 * @param alg unused
 * @param addr the address of the supplicant of ffff... for group keys
 * @param key_idx the key index
 * @param key the key
 * @param key_len the length of the key
 * @return 0 to indicate success
 */
static int rsncb_set_key(void* ctx, int vlan_id, enum wpa_alg alg, const u8* addr, int key_idx, u8* key, size_t key_len)
{
#if R_DEV_AUTH_DEBUG_ENABLED
    if (key)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_547(vlan_id, alg, addr[0], addr[1], key_idx, key[0], key[1], key_len);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
    else
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_551(vlan_id, alg, addr[0], addr[1], key_idx, key_len);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
#endif

    if ((ctx == NULL) || (addr == NULL) || (key == NULL))
    {
        return -1;
    }

    r_apl_global_t* nwk = ctx;
#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_562((uint32_t)clock_seconds(), SUP->address, (uint8_t)key_idx, key);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif

    // get the right key
//    rsncb_set_key: vlan_id: 0, alg: 3, addr: ffff..., key_idx: 1, key: db33..., key_len: 16
//    rsncb_set_key: vlan_id: 0, alg: 0, addr: 4444..., key_idx: 0, key: NULL, key_len: 0
//    rsncb_set_key: vlan_id: 0, alg: 3, addr: ffff..., key_idx: 1, key: 1111..., key_len: 16   <--- GTK0
//    rsncb_set_key: vlan_id: 0, alg: 3, addr: 4444..., key_idx: 0, key: 38e8..., key_len: 16   <--- TK (PTK is ready)
//    rsncb_set_key: vlan_id: 0, alg: 3, addr: ffff..., key_idx: 2, key: 2222..., key_len: 16   <--- GTK1
//    rsncb_set_key: vlan_id: 0, alg: 3, addr: ffff..., key_idx: 3, key: 3333..., key_len: 16   <--- GTK2
//    rsncb_set_key: vlan_id: 0, alg: 3, addr: ffff..., key_idx: 4, key: 4444..., key_len: 16   <--- GTK3

    // multicast EUI -> least-significant bit of first byte == 1
    // key_idx == 0 -> Key is PTK
    if (!(addr[0] & 1) && key_idx == 0)
    {
        if (!(BR->rsn_ctx && BR->rsn_ctx->wpa_sm))
        {
            return -1;
        }

        // key is just the TK -> get the full PTK from the state machine
        const struct wpa_ptk* ptk = &BR->rsn_ctx->wpa_sm->PTK;
        memcpy(SUP->ptk, ptk->kck, 16);
        memcpy(SUP->ptk + 16, ptk->kek, 16);
        memcpy(SUP->ptk + 32, ptk->tk, 16);
        SUP->ptk_expiration_minutes = clock_seconds() / 60 + BR->key_lifetimes.ptk_lifetime_minutes;
#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_590(SUP->ptk);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#else
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_592();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif
    }
    else if (key_idx >= 1 && key_idx < R_KEY_NUM + 1 && key_len == R_AUTH_GTK_SIZE && addr[0] & 0x01)
    {
        key_idx -= 1;  // WPA uses 1-based key index but our index is 0-based
        if (!GTK_IS_VALID(key_idx))
        {
            // need to copy the key as it points to the same memory location
            uint8_t key_copy[R_AUTH_GTK_SIZE];
            MEMCPY_A(key_copy, key);
            R_AUTH_SetGTK(nwk, (uint8_t)key_idx, key_copy);
        }
        SUP->gtks_valid |= 1U << key_idx;
#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_607((uint8_t)key_idx, key);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#else
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_609((uint8_t)key_idx);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif
    }

    return 0;
}

/**
 * WPA callback to send an EAPOL message
 * @param ctx the WPA context
 * @param addr the destination address
 * @param data the payload
 * @param data_len the payload length
 * @param encrypt unused
 * @return 0 on success
 */
static int rsncb_send_eapol(void* ctx, const u8* addr, const u8* data, size_t data_len, int encrypt)
{
#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_628(addr[0], addr[1], data[0], data[1], data_len, encrypt);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif
    return R_AUTH_BR_SendEapol(ctx, R_EAPOL_TYPE_CONVERT_FROM_4WH_OR_GKH, data, (uint16_t)data_len);
}

/**
 * Free the RSN context
 * @param nwk the central network structure
 */
static void rsn_free(r_apl_global_t* nwk)
{
    if (nwk == NULL)
    {
        return;
    }

    if (BR->rsn_ctx)
    {
        if (BR->rsn_ctx->wpa_sm)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_648(BR->rsn_ctx->wpa_sm->addr);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            MEMZERO_A(BR->rsn_ctx->wpa_sm->addr);
            wpa_auth_sta_deinit(BR->rsn_ctx->wpa_sm);
        }
        if (BR->rsn_ctx->wpa_auth)
        {
            wpa_deinit(BR->rsn_ctx->wpa_auth);
        }
        os_free(BR->rsn_ctx);
        BR->rsn_ctx = NULL;
    }
    BR->reservedUntil = 0;  // Remove RSN reservation
}

/**
 * Free the RSN context and log the supplied error message
 * @param nwk the central network structure
 * @param message the error message
 * @return NULL
 */
static void* rsn_free_fatal(r_apl_global_t* nwk, const char* message)
{
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
    r_loggen_670(message);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
    rsn_free(nwk);
    if (nwk)
    {
        SUP->ptk_expiration_minutes = 0;
    }
    return NULL;
}

/**
 * Create the RSN context
 * @param nwk the central network structure
 * @return NULL on error, the context on success
 */
static r_auth_br_rsn_ctx_t* rsn_create(r_apl_global_t* nwk)
{
    if (nwk == NULL)
    {
        return NULL;
    }

    r_auth_br_rsn_ctx_t* ctx = os_zalloc(sizeof(r_auth_br_rsn_ctx_t));
    if (!ctx)
    {
        return rsn_free_fatal(nwk, "br.rsn_create: out-of-memory");
    }

    BR->rsn_ctx = ctx;                                     // freed in rsn_free_fatal if following steps fail
    ctx->cb.logger = rsncb_logger;                         // can be NULL
    ctx->cb.disconnect = rsncb_disconnect;                 // can be NULL
    ctx->cb.mic_failure_report = rsncb_mic_failure_report; // can be NULL
    ctx->cb.psk_failure_report = rsncb_psk_failure_report; // can be NULL
    ctx->cb.set_eapol = rsncb_set_eapol;
    ctx->cb.get_eapol = rsncb_get_eapol;
    ctx->cb.get_psk = rsncb_get_psk;  // can be NULL
    ctx->cb.get_msk = rsncb_get_msk;
    ctx->cb.set_key = rsncb_set_key;
    ctx->cb.get_seqnum = rsncb_get_seqnum;  // can be NULL
    ctx->cb.send_eapol = rsncb_send_eapol;
    ctx->cb.for_each_sta = rsncb_for_each_sta;
    ctx->cb.for_each_auth = rsncb_for_each_auth;
    ctx->cb.send_ether = rsncb_send_ether;  // can be NULL

    uint8_t eui64[R_MAC_EXTENDED_ADDRESS_LENGTH];
    r_result_t requestResult = R_NWK_GetRequest(R_NWK_macExtendedAddress, &eui64, sizeof(eui64));
    if (requestResult != R_RESULT_SUCCESS)
    {
        return rsn_free_fatal(nwk, "br.rsn_create: Could not determine local EUI-64");
    }
    R_Swap64(eui64, eui64);
    ctx->wpa_auth = wpa_init(eui64, &rsn_wpa_auth_config, &ctx->cb, nwk);
    if (ctx->wpa_auth == NULL)
    {
        return rsn_free_fatal(nwk, "br.rsn_create: wpa_init failed");
    }
#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_726((uint32_t)(clock_seconds()), SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif
    // Link GTK information in WPA state machine to our settings
    for (unsigned i = 0; i < R_KEY_NUM; i++)
    {
        ctx->wpa_auth->group->GTK[i] = GTK(i);
    }
    // we use R_KEY_NUM as the index to indicate to just send the GTKL, but it must still point to valid memory for earlier memcpy
    ctx->wpa_auth->group->GTK[R_KEY_NUM] = GTK(0);
    ctx->wpa_auth->group->gtks_set = &nwk->auth.gtks_valid;
    ctx->wpa_auth->group->gtk_lifetimes = &nwk->auth.br.gtk_expiration_seconds[0];
    if (SUP->node_role == R_AUTH_NODE_ROLE_ID_LFN)
    {
        /* LFNs do not support regular GTKs -> Set key index to current LGTK (this key will be sent to the SUP by WPA) */
        ctx->wpa_auth->group->GN = BR->current_lgtk + 1;  // GN is 1-based but our GTK index is 0-based
    }
    else
    {
        ctx->wpa_auth->group->GN = BR->current_gtk + 1;  // GN is 1-based but our GTK index is 0-based
    }

    wpa_init_keys(ctx->wpa_auth);

    ctx->wpa_sm = wpa_auth_sta_init(ctx->wpa_auth, SUP->address, NULL);
    if (ctx->wpa_sm == NULL)
    {
        return rsn_free_fatal(nwk, "br.rsn_create: wpa_auth_sta_init failed");
    }

    // In IEEE 802.11, information excluded in Wi-SUN is transmitted in the 2nd message of the 4WH. On the supplicant,
    // this is handled via the call to wpa_sm_set_assoc_wpa_ie_default. The authenticator expects to receive that output.
    // However, because Wi-SUN changes message 2, we have to inject the information into the WPA SM manually.
    static const uint8_t wpa_ie[] = {0x30, 0x14, 0x01, 0x00, 0x00, 0x0f, 0xac, 0x04, 0x01, 0x00, 0x00, 0x0f, 0xac, 0x04, 0x01, 0x00, 0x00, 0x0f, 0xac, 0x01, 0x00, 0x00};
    wpa_validate_wpa_ie(ctx->wpa_auth, ctx->wpa_sm, wpa_ie, sizeof(wpa_ie), NULL, 0, NULL, 0);

    /*
     * Use the replay counter as a check to determine if we have already had a context that needs to be restored. This
     * is necessary since we no longer keep the RSN context around after we are done with the 4WH or each GKH.
     */
    if (!MEMISZERO(SUP->key_replay.counter, R_AUTH_REPLAY_COUNTER_LEN))
    {
        rsn_restore_state(nwk);

        /*
         * Set the Pair variable in the state machine for the case where
         * there is no need to call the wpa_auth_sta_associated function
         * below. If this is not set, the GKH packets differ in the install
         * bit in the KeyInfo field. Although the tests seem to work well
         * with and without the Pair variable set, the standard specifies
         * the value of the install bit.
         */
        ctx->wpa_sm->Pair = 1;
    }

    /*
     * Only call this function if we are creating the context to perform
     * a 4WH. If we are creating the context to perform a GKH, calling
     * this function sends an extra packet that starts the 4WH.
     */
    if (is_expired(SUP->ptk_expiration_minutes))
    {
#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_788((uint32_t)(clock_seconds()), SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif
        if (wpa_auth_sta_associated(ctx->wpa_auth, ctx->wpa_sm) < 0)
        {
            return rsn_free_fatal(nwk, "br.rsn_create: wpa_auth_sta_associated failed");
        }
    }

    BR->reservedUntil = clock_seconds() + R_AUTH_BR_RSN_SUPPLICANT_TIMEOUT;
    return ctx;
}

/** Return true if the RSN context slot may be used by the current supplicant. False otherwise. */
static r_boolean_t acquire_rsn_context_slot(r_apl_global_t* nwk)
{
    if (!BR->rsn_ctx)
    {
        return R_TRUE;
    }

    if (!BR->rsn_ctx->wpa_sm)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_810();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        rsn_free(nwk);
        return R_TRUE;  // Existing RSN ctx has no state machine (should not happen as we create/destroy them together)
    }

    if (MEMEQUAL_A(BR->rsn_ctx->wpa_sm->addr, SUP->address))
    {
        return R_TRUE;  // Existing RSN context belongs to the current supplicant
    }
    else if (BR->reservedUntil < clock_seconds())
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_821(BR->rsn_ctx->wpa_sm->addr, SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        rsn_free(nwk);
        return R_TRUE;  // Existing RSN context belongs to another supplicant but its reservation is expired
    }
    else
    {
        return R_FALSE;  // Existing RSN context belongs to another supplicant and its reservation is still active
    }
}

/******************************************************************************
* Multi-supplicant utility Functions
******************************************************************************/

/**
 * Save the current state of the RSN context in the supplicant state
 * @param nwk the central network structure
 * @return nothing
 */
static void rsn_save_state(r_apl_global_t* nwk)
{
    if (BR->rsn_ctx && BR->rsn_ctx->wpa_sm && MEMEQUAL_A(BR->rsn_ctx->wpa_sm->addr, SUP->address))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_844(BR->current_supplicant, BR->rsn_ctx->wpa_sm->key_replay[0].counter[R_AUTH_REPLAY_COUNTER_LEN - 1]);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        memcpy(&SUP->key_replay, &BR->rsn_ctx->wpa_sm->key_replay[0], R_AUTH_REPLAY_COUNTER_LEN + 1);
    }
}

/**
 * Restore the current supplicant state into the RSN context. Do nothing if it does not exist.
 * @param nwk the central network structure
 * @return nothing
 */
static void rsn_restore_state(r_apl_global_t* nwk)
{
    if (!(nwk && BR->rsn_ctx))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_858();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_862(BR->current_supplicant, SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    memcpy(&BR->rsn_ctx->wpa_sm->addr, SUP->address, sizeof(SUP->address));

    memcpy(&BR->rsn_ctx->wpa_sm->PMK, SUP->pmk, R_AUTH_PMK_SIZE);
    BR->rsn_ctx->wpa_sm->pmk_len = R_AUTH_PMK_SIZE;

    memcpy(&BR->rsn_ctx->wpa_sm->PTK.kck, &SUP->ptk[0], R_AUTH_KCK_SIZE);
    BR->rsn_ctx->wpa_sm->PTK.kck_len = R_AUTH_KCK_SIZE;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_870(BR->rsn_ctx->wpa_sm->PTK.kck);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    /* The KEK starts right after the KCK in the stored PTK */
    memcpy(&BR->rsn_ctx->wpa_sm->PTK.kek, &SUP->ptk[R_AUTH_KCK_SIZE], R_AUTH_KEK_SIZE);
    BR->rsn_ctx->wpa_sm->PTK.kek_len = R_AUTH_KEK_SIZE;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_875(BR->rsn_ctx->wpa_sm->PTK.kek);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    if (SUP->ptk_expiration_minutes)
    {
        BR->rsn_ctx->wpa_sm->PTK_valid = 1;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_882(BR->rsn_ctx->wpa_sm->PTK.tk);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_884(BR->current_supplicant, SUP->address, SUP->key_replay.counter[R_AUTH_REPLAY_COUNTER_LEN - 1]);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    memcpy(&BR->rsn_ctx->wpa_sm->key_replay[0], &SUP->key_replay, R_AUTH_REPLAY_COUNTER_LEN + 1);
}

/**
 * Process an incoming RSN message
 * @param nwk the central network structure
 * @param data the incoming message
 * @param size the size of the message
 * @return R_RESULT_SUCCESS if the message has been successfully processed
 */
static r_result_t rsn_process_message(r_apl_global_t* nwk, const void* data, uint16_t size)
{
    if ((nwk == NULL) || (data == NULL))
    {
        return R_RESULT_INVALID_PARAMETER;
    }

    if (!acquire_rsn_context_slot(nwk))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_904(SUP->address, BR->rsn_ctx->wpa_sm->addr);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return R_RESULT_ILLEGAL_STATE;
    }

    if (!BR->rsn_ctx)
    {
        if (is_expired(SUP->pmk_expiration_minutes))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_912();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            return R_RESULT_FAILED;
        }
        BR->rsn_ctx = rsn_create(nwk);
        if (!BR->rsn_ctx)
        {
            return R_RESULT_FAILED;
        }
    }
    else
    {
        // extend reservation time since this exchange is still alive
        BR->reservedUntil = clock_seconds() + R_AUTH_BR_RSN_SUPPLICANT_TIMEOUT;
    }

    // if we have 4WH message #4 or GKH message #2, we can continue with additional GTKs
    struct eapol_message_s* msg = (struct eapol_message_s*)data;
    int isFinalRsnMessage = msg->eapol.type == IEEE802_1X_TYPE_EAPOL_KEY && WPA_GET_BE16(msg->d.key.key_info) & WPA_KEY_INFO_SECURE;
    wpa_receive(BR->rsn_ctx->wpa_auth, BR->rsn_ctx->wpa_sm, (uint8_t*)data, size);
    if (isFinalRsnMessage)
    {
        if (R_AUTH_BR_ContinueAuthentication(nwk) == R_RESULT_NOTHING_TO_DO)
        {
            rsn_save_state(nwk);
            rsn_free(nwk);
            update_border_router_state(nwk);

            /* Continue previously blocked RSN handshake(s) */
            for (size_t i = 0; i < r_auth_br_supplicants_size_glb; i++)
            {
                if (BR->supplicants[i].pmk_expiration_minutes && !BR->supplicants[i].ptk_expiration_minutes  &&
                    (((BR->supplicants[i].relay[0] == 0xff) && ((clock_seconds() - BR->supplicants[i].last_rx) < R_AUTH_EAP_TLS_RETRY_THRESHOLD)) ||
                     ((BR->supplicants[i].relay[0] != 0xff) && ((clock_seconds() - BR->supplicants[i].last_rx) < R_AUTH_EAP_TLS_RETRY_THRESHOLD_RELAY))))
                {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                    r_loggen_946(BR->supplicants[i].address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
                    switch_supplicant(nwk, BR->supplicants[i].address);  // cannot fail
                    R_AUTH_BR_ContinueAuthentication(nwk);
                    break;
                }
            }
        }
    }
    return R_RESULT_SUCCESS;
}

/******************************************************************************
* Utilities
******************************************************************************/

static void add_gtk(r_apl_global_t* nwk, const uint8_t* gtk)
{
    for (unsigned i = 0; i < R_AUTH_NUM_GTKS; ++i)
    {
        unsigned igtk = (BR->current_gtk + i) % R_AUTH_NUM_GTKS;
        if (!GTK_IS_VALID(igtk))
        {
            R_AUTH_SetGTK(nwk, igtk, gtk);
            break;
        }
    }
}

static void add_lgtk(r_apl_global_t* nwk, const uint8_t* gtk)
{
    for (unsigned i = 0; i < R_AUTH_NUM_LGTKS; ++i)
    {
        unsigned igtk = ((BR->current_lgtk - R_AUTH_NUM_GTKS + i) % R_AUTH_NUM_LGTKS) + R_AUTH_NUM_GTKS;
        if (!GTK_IS_VALID(igtk))
        {
            R_AUTH_SetGTK(nwk, igtk, gtk);
            break;
        }
    }
}

/// @return the 'next' available GTK: minimum lifetime, but bigger than minExpirationSeconds
static unsigned next_gtk_index(r_apl_global_t* nwk, uint32_t min_expiration_seconds)
{
    // search the 'next' available GTK: minimum lifetime, but bigger than minExpirationSeconds
    uint32_t min = ~(uint32_t)0;
    unsigned r = ~0u;

    for (unsigned i = 0; i < R_AUTH_NUM_GTKS; ++i)
    {
        if (GTK_IS_VALID(i) && BR->gtk_expiration_seconds[i] > min_expiration_seconds && BR->gtk_expiration_seconds[i] < min)
        {
            min = BR->gtk_expiration_seconds[i];
            r = i;
        }
    }

    return r;
}

/// @return the 'next' available LGTK: minimum lifetime, but bigger than minExpirationSeconds
static unsigned next_lgtk_index(r_apl_global_t* nwk, uint32_t min_expiration_seconds)
{
    // search the 'next' available GTK: minimum lifetime, but bigger than minExpirationSeconds
    uint32_t min = ~(uint32_t)0;
    unsigned r = ~0u;

    for (unsigned i = R_AUTH_NUM_GTKS; i < R_KEY_NUM; ++i)
    {
        if (GTK_IS_VALID(i) && BR->gtk_expiration_seconds[i] > min_expiration_seconds && BR->gtk_expiration_seconds[i] < min)
        {
            min = BR->gtk_expiration_seconds[i];
            r = i;
        }
    }

    return r;
}

/** Activate the GTK or LGTK with the specified index to be used for frame encryption and decryption */
static r_result_t activate_gtk(uint8_t index)
{
    r_nwk_gtk_index_t new_active_gtk = { .index = index };
    r_result_t res = R_NWK_SetRequest(R_NWK_framesecActiveGtk, &new_active_gtk, sizeof(new_active_gtk));
    if (res == R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_1032(index);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
    else
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1036(index, res);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
    }
    return res;
}

/******************************************************************************
* Public Functions
******************************************************************************/

void R_AUTH_BR_Reset(r_apl_global_t* nwk)
{
    if (nwk)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_1049();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#if R_EAP_TLS_SERVER_ENABLED
        R_EAP_TLS_SRV_Reset(&nwk->eap_tls_srv);
#endif
        rsn_free(nwk);
        MEMZERO_S(BR);

        /* Set supplicant table pointer to global variable and clear whole table */
        BR->supplicants = g_supplicants;
        R_memset(g_supplicants, 0, (sizeof(r_auth_br_supplicant_t)) * r_auth_br_supplicants_size_glb);

        BR->key_lifetimes.pmk_lifetime_minutes = R_AUTH_BR_PMK_LIFETIME_MINUTES_DEFAULT;
        BR->key_lifetimes.ptk_lifetime_minutes = R_AUTH_BR_PTK_LIFETIME_MINUTES_DEFAULT;
        BR->key_lifetimes.gtk_lifetime_minutes = R_AUTH_BR_GTK_EXPIRE_OFFSET_MINUTES_DEFAULT;
        BR->key_lifetimes.gtk_new_activation_time_fraction = R_AUTH_BR_GTK_NEW_ACTIVATION_TIME_DEFAULT;
        BR->key_lifetimes.revocation_lifetime_reduction_fraction = R_AUTH_BR_REVOCATION_LIFETIME_REDUCTION_DEFAULT;
        BR->key_lifetimes.gtk_new_install_required_percentage = R_AUTH_BR_GTK_NEW_INSTALL_REQUIRED_DEFAULT;
        nwk->auth.gtks_valid = 0;
        BR->current_gtk = 0;
        BR->current_lgtk = R_AUTH_NUM_GTKS;
        R_AUTH_SetAuthStateCache(nwk);

        R_AUTH_InternalReset();
    }
}

void R_AUTH_BR_Start(r_apl_global_t* nwk)
{
    if (nwk)
    {
        /* Set all existing MAC Keys again for key expansion (after network name was set) */
        for (unsigned i = 0; i < R_KEY_NUM; i++)
        {
            if (GTK_IS_VALID(i))
            {
                r_nwk_framesec_gtk_t nwk_framesec_gtk;
                nwk_framesec_gtk.index = i;
                nwk_framesec_gtk.remove = R_FALSE;
                MEMCPY_A(nwk_framesec_gtk.gtk, GTK(i));
                R_NWK_SetRequest(R_NWK_framesecGtk, &nwk_framesec_gtk, sizeof(nwk_framesec_gtk));
            }
        }

        /* Make sure the current GTK is valid (if not, add a new one) */
        if (!GTK_IS_VALID(BR->current_gtk))
        {
            R_AUTH_SetGTK(nwk, BR->current_gtk, NULL);
        }
        activate_gtk(BR->current_gtk);

        /* Make sure the current LGTK is valid (if not, add a new one) */
        if (!GTK_IS_VALID(BR->current_lgtk))
        {
            R_AUTH_SetGTK(nwk, BR->current_lgtk, NULL);
        }
        activate_gtk(BR->current_lgtk);

        // install additional GTKs is required
        R_AUTH_BR_PeriodicUpdate(nwk);
    }
}

r_boolean_t R_AUTH_BR_PeriodicUpdateGtks(r_apl_global_t* nwk)
{
    r_boolean_t gtksChanged = R_FALSE;

    uint32_t now = clock_seconds();
    uint32_t newInstallRequiredSeconds = 60 * BR->key_lifetimes.gtk_lifetime_minutes * (100 - BR->key_lifetimes.gtk_new_install_required_percentage) / 100;
    uint32_t newActivationRequiredSeconds = (60 * BR->key_lifetimes.gtk_lifetime_minutes / BR->key_lifetimes.gtk_new_activation_time_fraction);
    uint32_t gtkExpirationSecs = 60 * BR->key_lifetimes.gtk_lifetime_minutes;

    uint8_t firstKeyIndex = 0;
    uint8_t lastKeyIndex = firstKeyIndex + R_AUTH_NUM_GTKS;

    // Remove expired (unless current)
    for (unsigned i = firstKeyIndex; i < lastKeyIndex; i++)
    {
        if (GTK_IS_VALID(i) && i != BR->current_gtk && BR->gtk_expiration_seconds[i] <= now)
        {
            R_AUTH_DeleteGTK(nwk, i);
            gtksChanged = R_TRUE;
        }
    }

    uint32_t maxExpire = 0;
    unsigned iMaxExpire = ~0u;
    for (unsigned i = firstKeyIndex; i < lastKeyIndex; i++)
    {
        if (GTK_IS_VALID(i) && BR->gtk_expiration_seconds[i] > maxExpire)
        {
            maxExpire = BR->gtk_expiration_seconds[i];
            iMaxExpire = i;
        }
    }

#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1145(nwk->auth.gtks_valid, clock_seconds(), BR->current_gtk, (uint32_t)BR->gtk_expiration_seconds[BR->current_gtk], (uint32_t)(BR->gtk_expiration_seconds[BR->current_gtk] - newInstallRequiredSeconds), (uint32_t)(BR->gtk_expiration_seconds[BR->current_gtk] - newActivationRequiredSeconds), iMaxExpire, (uint32_t)maxExpire);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif

    // new install required?
    if (BR->gtk_expiration_seconds[BR->current_gtk] <= now + newInstallRequiredSeconds && maxExpire < now + gtkExpirationSecs)
    {
        // remove all that are not current and not the one with maxExpire
        for (unsigned i = firstKeyIndex; i < lastKeyIndex; i++)
        {
            if (GTK_IS_VALID(i) && i != BR->current_gtk && i != iMaxExpire)
            {
                R_AUTH_DeleteGTK(nwk, i);
            }
        }
        add_gtk(nwk, NULL);
        gtksChanged = R_TRUE;
    }

    // activate new GTK?
    if (BR->gtk_expiration_seconds[BR->current_gtk] <= now + newActivationRequiredSeconds)
    {
        BR->current_gtk = next_gtk_index(nwk, BR->gtk_expiration_seconds[BR->current_gtk]);
        activate_gtk(BR->current_gtk);
        gtksChanged = R_TRUE;
    }
    return gtksChanged;
}

r_boolean_t R_AUTH_BR_PeriodicUpdateLgtks(r_apl_global_t* nwk)
{
    r_boolean_t gtksChanged = R_FALSE;

    uint32_t now = clock_seconds();
    uint32_t newInstallRequiredSeconds = 60 * BR->key_lifetimes.gtk_lifetime_minutes * (100 - BR->key_lifetimes.gtk_new_install_required_percentage) / 100;
    uint32_t newActivationRequiredSeconds = (60 * BR->key_lifetimes.gtk_lifetime_minutes / BR->key_lifetimes.gtk_new_activation_time_fraction);
    uint32_t gtkExpirationSecs = 60 * BR->key_lifetimes.gtk_lifetime_minutes;

    uint8_t firstKeyIndex = R_AUTH_NUM_GTKS;
    uint8_t lastKeyIndex = firstKeyIndex + R_AUTH_NUM_LGTKS;

    // Remove expired (unless current)
    for (unsigned i = firstKeyIndex; i < lastKeyIndex; i++)
    {
        if (GTK_IS_VALID(i) && i != BR->current_lgtk && BR->gtk_expiration_seconds[i] <= now)
        {
            R_AUTH_DeleteGTK(nwk, i);
            gtksChanged = R_TRUE;
        }
    }

    uint32_t maxExpire = 0;
    unsigned iMaxExpire = ~0u;
    for (unsigned i = firstKeyIndex; i < lastKeyIndex; i++)
    {
        if (GTK_IS_VALID(i) && BR->gtk_expiration_seconds[i] > maxExpire)
        {
            maxExpire = BR->gtk_expiration_seconds[i];
            iMaxExpire = i;
        }
    }

#if R_DEV_AUTH_DEBUG_ENABLED
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1207(nwk->auth.gtks_valid, clock_seconds(), BR->current_lgtk, (uint32_t)BR->gtk_expiration_seconds[BR->current_lgtk], (uint32_t)(BR->gtk_expiration_seconds[BR->current_lgtk] - newInstallRequiredSeconds), (uint32_t)(BR->gtk_expiration_seconds[BR->current_lgtk] - newActivationRequiredSeconds), iMaxExpire, (uint32_t)maxExpire);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif

    // new install required?
    if (BR->gtk_expiration_seconds[BR->current_lgtk] <= now + newInstallRequiredSeconds && maxExpire < now + gtkExpirationSecs)
    {
        // remove all that are not current and not the one with maxExpire
        for (unsigned i = firstKeyIndex; i < lastKeyIndex; i++)
        {
            if (GTK_IS_VALID(i) && i != BR->current_lgtk && i != iMaxExpire)
            {
                R_AUTH_DeleteGTK(nwk, i);
            }
        }
        add_lgtk(nwk, NULL);
        gtksChanged = R_TRUE;
    }

    // activate new LGTK?
    if (BR->gtk_expiration_seconds[BR->current_lgtk] <= now + newActivationRequiredSeconds)
    {
        BR->current_lgtk = next_lgtk_index(nwk, BR->gtk_expiration_seconds[BR->current_lgtk]);
        activate_gtk(BR->current_lgtk);
        gtksChanged = R_TRUE;
    }

    return gtksChanged;
}

r_boolean_t R_AUTH_BR_PeriodicUpdate(r_apl_global_t* nwk)
{
    r_boolean_t panVersionUpdateRequired = R_AUTH_BR_PeriodicUpdateGtks(nwk);
    panVersionUpdateRequired |= R_AUTH_BR_PeriodicUpdateLgtks(nwk);
    return panVersionUpdateRequired;
}

r_result_t R_AUTH_BR_RevokeGTKs(r_apl_global_t* nwk, const uint8_t* new_gtk)
{
    R_AUTH_BR_PeriodicUpdate(nwk);  // make sure we are in a sane state

    unsigned modGtk;
    uint32_t minExpirationSeconds = clock_seconds() + (60 * BR->key_lifetimes.gtk_lifetime_minutes / BR->key_lifetimes.revocation_lifetime_reduction_fraction);
    if (BR->gtk_expiration_seconds[BR->current_gtk] <= minExpirationSeconds)
    {
        modGtk = next_gtk_index(nwk, minExpirationSeconds);
    }
    else
    {
        modGtk = BR->current_gtk;
    }

    // "modify the lifetime of the next available GTK to be (lifetime /  REVOCATION_LIFETIME_REDUCTION)"
    if (modGtk < R_AUTH_NUM_GTKS)
    {
        BR->gtk_expiration_seconds[modGtk] = minExpirationSeconds;
    }

    // "destroy all GTKs"
    for (unsigned i = 0; i < R_AUTH_NUM_GTKS; ++i)
    {
        if (GTK_IS_VALID(i) && !(i == BR->current_gtk || i == modGtk))
        {
            r_result_t res = R_AUTH_DeleteGTK(nwk, i);
            if (res != R_RESULT_SUCCESS)
            {
                return res;
            }
        }
    }

    add_gtk(nwk, new_gtk);  // add the "new GTK (with normal lifetime)"

    return R_RESULT_SUCCESS;
}

r_result_t R_AUTH_BR_RevokeSupplicant(r_apl_global_t* nwk, const uint8_t* supplicant)
{
    R_AUTH_BR_PeriodicUpdate(nwk);  // make sure we are in a sane state

    for (size_t i = 0; i < r_auth_br_supplicants_size_glb; i++)
    {
        if (MEMEQUAL_A(BR->supplicants[i].address, supplicant))
        {
            /* Delete the whole supplicant entry (including PMK, PTK and GTKs) */
            MEMZERO_S(&BR->supplicants[i]);
            return R_RESULT_SUCCESS;
        }
    }

    return R_RESULT_FAILED;  // Supplicant not found
}

/**
 * Continue the currently running RSN handshake or start a new one
 */
static r_result_t continueRsnHandshake(r_apl_global_t* nwk)
{
    if (is_expired(SUP->ptk_expiration_minutes))
    {
        return R_AUTH_BR_Start4WH(nwk);
    }

    uint8_t myValidGtks = nwk->auth.gtks_valid;
    uint8_t supsValidGtks = SUP->gtks_valid;
    if (SUP->node_role == R_AUTH_NODE_ROLE_ID_LFN)
    {
        /* LFNs do not support GTKs -> Only consider LGTK liveness by masking out GTK liveness bits */
        myValidGtks &= R_AUTH_LGTK_LIVENESS_MASK;
        supsValidGtks &= R_AUTH_LGTK_LIVENESS_MASK;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_1316(SUP->address, nwk->auth.gtks_valid, myValidGtks);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
    if (SUP->node_role == R_AUTH_NODE_ROLE_ID_FAN10_RN)
    {
        /* FAN 1.0 nodes do not support LGTKs -> Only consider GTK liveness by masking out LGTK liveness bits */
        myValidGtks &= R_AUTH_GTK_LIVENESS_MASK;
        supsValidGtks &= R_AUTH_GTK_LIVENESS_MASK;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_1323(SUP->address, nwk->auth.gtks_valid, myValidGtks);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }

    if (supsValidGtks != myValidGtks)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_1328(SUP->address, supsValidGtks, myValidGtks);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        for (uint8_t i = 0; i < R_KEY_NUM; i++)
        {
            if ((myValidGtks & 1u << i) && !(supsValidGtks & 1u << i))
            {
                return R_AUTH_BR_StartGKH(nwk, i);
            }
        }
    }
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1337(SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    return R_RESULT_NOTHING_TO_DO;  // nothing to do
}

r_result_t R_AUTH_BR_ContinueAuthentication(r_apl_global_t* nwk)
{
    if (!nwk)
    {
        return R_RESULT_INVALID_PARAMETER;
    }
    if (is_expired(SUP->pmk_expiration_minutes))
    {
#if R_EAP_TLS_SERVER_ENABLED

        /* Initialize Renesas EAP-TLS server library and start handshake */
#if R_AUTH_MULTIPLE_CERTS
        if (r_auth_certs_use_alternate)
        {
            R_EAP_TLS_SRV_Init(r_auth_certs_alternate, &nwk->eap_tls_srv);
        }
        else
#endif /* R_AUTH_MULTIPLE_CERTS */
        {
            R_EAP_TLS_SRV_Init(r_auth_certs, &nwk->eap_tls_srv);
        }
        SUP->retryPending = R_FALSE;  // Clear retry flag since we start an EAP-TLS handshake now
        r_eap_tls_srv_result_t eapTlsResult = R_EAP_TLS_SRV_Start(SUP->address, &nwk->eap_tls_srv, nwk);
        if (eapTlsResult == R_EAP_TLS_SRV_RESULT_NO_FREE_SUP_SLOTS)
        {
            SUP->retryPending = R_TRUE;  // Set retry flag so that we may retry once a slot is free
        }
        return (eapTlsResult == R_EAP_TLS_SRV_RESULT_SUCCESS) ? R_RESULT_SUCCESS : R_RESULT_FAILED;
#else /* R_EAP_TLS_SERVER_ENABLED */
        return R_RESULT_UNSUPPORTED_FEATURE;  // Alternative implementation to Renesas EAP-TLS server library may be provided here
#endif /* if R_EAP_TLS_SERVER_ENABLED */
    }
    return continueRsnHandshake(nwk);
}

/** Retry pending EAP-TLS handshakes that were previously blocked because of insufficient SUP slots */
static void retry_blocked_eap_tls_handshakes(r_apl_global_t* nwk)
{
    for (size_t i = 0; i < r_auth_br_supplicants_size_glb; i++)
    {
        if (BR->supplicants[i].retryPending)
        {
            if (((BR->supplicants[i].relay[0] == 0xff) && ((clock_seconds() - BR->supplicants[i].last_rx) < R_AUTH_EAP_TLS_RETRY_THRESHOLD)) ||
                ((BR->supplicants[i].relay[0] != 0xff) && ((clock_seconds() - BR->supplicants[i].last_rx) < R_AUTH_EAP_TLS_RETRY_THRESHOLD_RELAY)))
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                r_loggen_1386(BR->supplicants[i].address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
                switch_supplicant(nwk, BR->supplicants[i].address);  // cannot fail here
                r_result_t res = R_AUTH_BR_ContinueAuthentication(nwk);
                if (res != R_RESULT_SUCCESS)
                {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
                    r_loggen_1391(SUP->address, res);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
                    return;  // If it fails here, there is no need to try further ones
                }
            }
            else
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
                r_loggen_1397(BR->supplicants[i].address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
                BR->supplicants[i].retryPending = R_FALSE;
            }
        }
    }
}

void R_AUTH_BR_EapTlsDoneCallback(const uint8_t* eui64, const uint8_t* pmk, r_apl_global_t* nwk)
{
    if (pmk == NULL || switch_supplicant(nwk, eui64) != R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1408(eui64);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1412(eui64);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    memcpy(SUP->pmk, pmk, R_AUTH_PMK_SIZE);
    SUP->pmk_expiration_minutes = clock_seconds() / 60 + BR->key_lifetimes.pmk_lifetime_minutes;

    continueRsnHandshake(nwk);  // EAP-TLS handshake for this SUP is done -> Start RSN handshake

    update_border_router_state(nwk);
}

void R_AUTH_BR_EapTlsFailureCallback(const uint8_t* eui64, r_apl_global_t* nwk)
{
    LOG_ONLY_VAR(eui64);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
    r_loggen_1424(eui64);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
    if (nwk == NULL)
    {
        return;
    }
    update_border_router_state(nwk);
}

r_result_t R_AUTH_BR_Start4WH(r_apl_global_t* nwk)
{
    if (!nwk || is_expired(SUP->pmk_expiration_minutes))
    {
        return R_RESULT_INVALID_PARAMETER;
    }

    if (!acquire_rsn_context_slot(nwk))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1441(SUP->address, BR->rsn_ctx->wpa_sm->addr);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return R_RESULT_ILLEGAL_STATE;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1445(SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    rsn_free(nwk);
    SUP->ptk_expiration_minutes = 0;
    BR->rsn_ctx = rsn_create(nwk);
    if (!BR->rsn_ctx)
    {
        return R_RESULT_FAILED;
    }

    rsn_save_state(nwk);

    return R_RESULT_SUCCESS;
}

r_result_t R_AUTH_BR_StartGKH(r_apl_global_t* nwk, uint8_t igtk)
{
    if (nwk == NULL || igtk > R_KEY_NUM)
    {
        return R_RESULT_INVALID_PARAMETER;
    }

    if (!acquire_rsn_context_slot(nwk))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1468(SUP->address, BR->rsn_ctx->wpa_sm->addr);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return R_RESULT_ILLEGAL_STATE;
    }

    if (!BR->rsn_ctx)
    {
        BR->rsn_ctx = rsn_create(nwk);
        if (!BR->rsn_ctx)
        {
            return R_RESULT_FAILED;
        }
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1481(igtk, SUP->address);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    SUP->gtks_valid &= ~(1U << igtk);
    BR->rsn_ctx->wpa_sm->GUpdateStationKeys = TRUE;
    struct wpa_group* group = BR->rsn_ctx->wpa_sm->group;
    group->GN = igtk + 1;                                            // WPA starts at 1
    wpa_gtk_rekey(BR->rsn_ctx->wpa_auth);                            // if GTKs are already set, they will not be changed
    MEMZERO_A(group->GNonce);                                        // Wi-SUN: nonce must be 0
    wpa_auth_sm_event(BR->rsn_ctx->wpa_sm, WPA_GROUP_KEY_HANDSHAKE); // Sends the first message of the handshake
    rsn_save_state(nwk);
    return R_RESULT_SUCCESS;
}

r_result_t R_AUTH_BR_ProcessEapolMessage(r_apl_global_t* nwk, r_mac_eapol_type_t type, const uint8_t* source,
                                         const void* data, uint16_t size)
{
    r_result_t res = R_RESULT_FAILED;

    if (!(nwk && source && data && size >= EAPOL_MESSAGE_MINSIZE))
    {
        return R_RESULT_INVALID_PARAMETER;
    }

    SUP->last_rx = clock_seconds();  // Set reception timestamp

    if (type == R_EAPOL_TYPE_8021X)
    {
        res = process_eap_message(nwk, source, data, size);
    }
    else if (type == R_EAPOL_TYPE_4WH || type == R_EAPOL_TYPE_GKH)
    {
        res = rsn_process_message(nwk, data, size);
    }

    if (res != R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1516(res);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
    }

    return res;
}

r_result_t R_AUTH_BR_ProcessDirectMessage(r_apl_global_t* nwk, r_mac_eapol_type_t type, const uint8_t* source,
                                          const void* data, uint16_t size)
{
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1525(MSGTYPE(type), size, source);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    if (switch_supplicant(nwk, source) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }

    SUP->relay[0] = 0xff;  // Clear stored relay for this supplicant
    return R_AUTH_BR_ProcessEapolMessage(nwk, type, source, data, size);
}


r_result_t R_AUTH_BR_ProcessRelayMessage(r_apl_global_t* nwk, const uint8_t* source, const uint8_t* dest,
                                         const uint8_t* data, uint16_t size)
{
    if (!(nwk && source && dest && data && size >= sizeof(eapol_relay_t) + EAPOL_MESSAGE_MINSIZE))
    {
        return R_RESULT_INVALID_PARAMETER;
    }
    const eapol_relay_t* relay = (const eapol_relay_t*)data;
    r_mac_eapol_type_t type = R_AUTH_DetectEapolType(relay->msg);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1545(MSGTYPE(type), size, relay->sup, source);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    if (switch_supplicant(nwk, relay->sup) != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    memcpy(SUP->relay, source, sizeof(SUP->relay));  // Store relay address in supplicant entry
    return R_AUTH_BR_ProcessEapolMessage(nwk, type, relay->sup, relay->msg, size - sizeof(eapol_relay_t));
}

int R_AUTH_BR_SendEapol(r_apl_global_t* nwk, r_mac_eapol_type_t type, const uint8_t* data, uint16_t size)
{
    r_result_t res;

    if ((nwk == NULL) || (data == NULL))
    {
        return -1;
    }

    uint16_t msgSize = size + R_AUTH_CREATEMESSAGE_MAX_OVERHEAD;
    if (SUP->relay[0] == 0xff)
    {
        const uint8_t* authenticator = NULL;
        uint8_t eui64[R_MAC_EXTENDED_ADDRESS_LENGTH];
        // insert authenticator EA-IE in "Request, Identity" message
        if ((type == R_EAPOL_TYPE_CONVERT_FROM_EAP && size == R_AUTH_REQUEST_IDENTITY_LENGTH - 4) || (type == R_EAPOL_TYPE_8021X && size == R_AUTH_REQUEST_IDENTITY_LENGTH))
        {
            r_result_t requestResult = R_NWK_GetRequest(R_NWK_macExtendedAddress, &eui64, sizeof(eui64));
            if (requestResult == R_RESULT_SUCCESS)
            {
                R_Swap64(eui64, eui64);
                authenticator = eui64;
            }
            else
            {
                return -1;
            }
        }

        /* Prepare message for transmission by NWK layer */
        uint8_t* msgBuf = os_malloc(msgSize);
        if (!msgBuf)
        {
            return -1;
        }
        res = R_AUTH_CreateMessage(type, data, size, msgBuf, &msgSize, &type);
        if (res == R_RESULT_SUCCESS)
        {
            res = R_NWK_EapolDataRequest(type, SUP->address, authenticator, msgBuf, msgSize);
            if (res != R_RESULT_SUCCESS)
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
                r_loggen_1596(SUP->address, res, size, MSGTYPE(type));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            }
            else
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                r_loggen_1600(SUP->address, size, MSGTYPE(type));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            }
        }
        os_free(msgBuf);
    }
    else
    {
        eapol_relay_t* relay = os_malloc(sizeof(eapol_relay_t) + msgSize);
        if (!relay)
        {
            return -1;
        }
        memcpy(relay->sup, SUP->address, sizeof(relay->sup));
        res = R_AUTH_CreateMessage(type, data, size, relay->msg, &msgSize, &type);
        if (res == R_RESULT_SUCCESS)
        {
            relay->kmp_id = (uint8_t)type;
            res = R_UDP_DataRequest(SUP->relay, R_AUTH_EAPOL_RELAY_PORT, R_AUTH_EAPOL_RELAY_PORT, (uint8_t*)relay, sizeof(eapol_relay_t) + msgSize, 0);
            if (res != R_RESULT_SUCCESS)
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
                r_loggen_1620(SUP->address, SUP->relay, res, size, MSGTYPE(type));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            }
            else
            {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
                r_loggen_1624(SUP->address, SUP->relay, size, MSGTYPE(type));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            }
        }
        os_free(relay);
    }
    return res == R_RESULT_SUCCESS ? 0 : -1;
}
#endif /* R_BR_AUTHENTICATOR_ENABLED */
