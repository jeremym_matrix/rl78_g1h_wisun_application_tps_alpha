/*
 * WPA Supplicant - WPA state machine and EAPOL-Key processing
 * Copyright (c) 2003-2018, Jouni Malinen <j@w1.fi>
 * Copyright(c) 2015 Intel Deutschland GmbH
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */

#include "includes.h"

#include "common.h"
#include "crypto/aes.h"
#include "crypto/aes_wrap.h"
#include "crypto/crypto.h"
#include "crypto/random.h"
#include "crypto/aes_siv.h"
#include "crypto/sha256.h"
#include "crypto/sha384.h"
#include "crypto/sha512.h"
#include "common/ieee802_11_defs.h"
#include "common/ieee802_11_common.h"
#include "eap_common/eap_defs.h"
#include "eapol_supp/eapol_supp_sm.h"
#include "wpa.h"
#include "preauth.h"
#include "pmksa_cache.h"
#include "wpa_i.h"
#include "wpa_ie.h"

#if R_WISUN_SUPPORT
#include "r_auth_types.h"
// R_LOG_PREFIX is processed by RLog code generator
#define R_LOG_PREFIX WPA
#include "r_log_internal.h"
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
#include "r_loggen_wpa.h"
#endif
#endif /* R_WISUN_SUPPORT */

static const u8 null_rsc[8] = { 0, 0, 0, 0, 0, 0, 0, 0 };

// LCOV_EXCL_START

/**
 * wpa_eapol_key_send - Send WPA/RSN EAPOL-Key message
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @ptk: PTK for Key Confirmation/Encryption Key
 * @ver: Version field from Key Info
 * @dest: Destination address for the frame
 * @proto: Ethertype (usually ETH_P_EAPOL)
 * @msg: EAPOL-Key message
 * @msg_len: Length of message
 * @key_mic: Pointer to the buffer to which the EAPOL-Key MIC is written
 * Returns: >= 0 on success, < 0 on failure
 */
int wpa_eapol_key_send(struct wpa_sm* sm, struct wpa_ptk* ptk,
                       int ver, const u8* dest, u16 proto,
                       u8* msg, size_t msg_len, u8* key_mic)
{
    int ret = -1;
    size_t mic_len = wpa_mic_len(sm->key_mgmt, sm->pmk_len);

    wpa_printf(MSG_DEBUG, "WPA: Send EAPOL-Key frame to " MACSTR
               " ver=%d mic_len=%d key_mgmt=0x%x",
               MAC2STR(dest), ver, (int)mic_len, sm->key_mgmt);
    if (mic_len)
    {
        if (key_mic && (!ptk || !ptk->kck_len))
        {
            goto out;
        }

        if (key_mic &&
            wpa_eapol_key_mic(ptk->kck, ptk->kck_len, sm->key_mgmt, ver,
                              msg, msg_len, key_mic))
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_ERROR,
                    "WPA: Failed to generate EAPOL-Key version %d key_mgmt 0x%x MIC",
                    ver, sm->key_mgmt);
            goto out;
        }
        if (ptk)
        {
            wpa_hexdump_key(MSG_DEBUG, "WPA: KCK",
                            ptk->kck, ptk->kck_len);
        }
        wpa_hexdump(MSG_DEBUG, "WPA: Derived Key MIC",
                    key_mic, mic_len);
    }
    else
    {
        goto out;
    }

    wpa_hexdump(MSG_MSGDUMP, "WPA: TX EAPOL-Key", msg, msg_len);
    ret = wpa_sm_ether_send(sm, dest, proto, msg, msg_len);
    eapol_sm_notify_tx_eapol_key(sm->eapol);
out:
    os_free(msg);
    return ret;
}


static void wpa_supplicant_key_mgmt_set_pmk(struct wpa_sm* sm)
{
    if (wpa_sm_key_mgmt_set_pmk(sm, sm->pmk, sm->pmk_len))
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "RSN: Cannot set PMK for key management offload");
    }
}


static int wpa_supplicant_get_pmk(struct wpa_sm*       sm,
                                  const unsigned char* src_addr,
                                  const u8*            pmkid)
{
    int abort_cached = 0;
    if (wpa_key_mgmt_wpa_ieee8021x(sm->key_mgmt) && sm->eapol)
    {
        int res, pmk_len;

        pmk_len = PMK_LEN;
        res = eapol_sm_get_key(sm->eapol, sm->pmk, pmk_len);
        if (res == 0)
        {
            struct rsn_pmksa_cache_entry* sa = NULL;
            const u8* fils_cache_id = NULL;

            wpa_hexdump_key(MSG_DEBUG, "WPA: PMK from EAPOL state "
                            "machines", sm->pmk, pmk_len);
            sm->pmk_len = pmk_len;
            wpa_supplicant_key_mgmt_set_pmk(sm);
            if (sm->proto == WPA_PROTO_RSN)
            {
                sa = pmksa_cache_add(sm->pmksa,
                                     sm->pmk, pmk_len, NULL,
                                     NULL, 0,
                                     src_addr, sm->own_addr,
                                     sm->network_ctx,
                                     sm->key_mgmt,
                                     fils_cache_id);
            }
            if (!sm->cur_pmksa && pmkid &&
                pmksa_cache_get(sm->pmksa, src_addr, pmkid, NULL,
                                0))
            {
                wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                        "RSN: the new PMK matches with the "
                        "PMKID");
                abort_cached = 0;
            }
            else if (sa && !sm->cur_pmksa && pmkid)
            {
                /*
                 * It looks like the authentication server
                 * derived mismatching MSK. This should not
                 * really happen, but bugs happen.. There is not
                 * much we can do here without knowing what
                 * exactly caused the server to misbehave.
                 */
                wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                        "RSN: PMKID mismatch - authentication server may have derived different MSK?!");
                return -1;
            }

            if (!sm->cur_pmksa)
            {
                sm->cur_pmksa = sa;
            }
        }
        else
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: Failed to get master session key from "
                    "EAPOL state machines - key handshake "
                    "aborted");
            if (sm->cur_pmksa)
            {
                wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                        "RSN: Cancelled PMKSA caching "
                        "attempt");
                sm->cur_pmksa = NULL;
                abort_cached = 1;
            }
            else if (!abort_cached)
            {
                return -1;
            }
        }
    }

    return 0;
}
// LCOV_EXCL_STOP

/**
 * wpa_supplicant_send_2_of_4 - Send message 2 of WPA/RSN 4-Way Handshake
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @dst: Destination address for the frame
 * @key: Pointer to the EAPOL-Key frame header
 * @ver: Version bits from EAPOL-Key Key Info
 * @nonce: Nonce value for the EAPOL-Key frame
 * @wpa_ie: WPA/RSN IE
 * @wpa_ie_len: Length of the WPA/RSN IE
 * @ptk: PTK to use for keyed hash and encryption
 * Returns: >= 0 on success, < 0 on failure
 */
int wpa_supplicant_send_2_of_4(struct wpa_sm* sm, const unsigned char* dst,
                               const struct wpa_eapol_key* key,
                               int ver, const u8* nonce,
                               const u8* wpa_ie, size_t wpa_ie_len,
                               struct wpa_ptk* ptk)
{
    size_t mic_len, hdrlen, rlen;
    struct wpa_eapol_key* reply;
    u8* rbuf, * key_mic;
    u8* rsn_ie_buf = NULL;
    u16 key_info;

    if (wpa_ie == NULL)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING, "WPA: No wpa_ie set - "
                "cannot generate msg 2/4");
        return -1;
    }

    wpa_hexdump(MSG_DEBUG, "WPA: WPA IE for msg 2/4", wpa_ie, wpa_ie_len);

    mic_len = wpa_mic_len(sm->key_mgmt, sm->pmk_len);
    hdrlen = sizeof(*reply) + mic_len + 2;
#if R_WISUN_SUPPORT
    /*
     * The second message is not supposed to include any key data
     * information according to the WiSun standard. Do not include
     * the extra data after the header (wpa_id_len)
     */
    rbuf = wpa_sm_alloc_eapol(sm, IEEE802_1X_TYPE_EAPOL_KEY,
                              NULL, hdrlen,
                              &rlen, (void*)&reply);
#else
    rbuf = wpa_sm_alloc_eapol(sm, IEEE802_1X_TYPE_EAPOL_KEY,
                              NULL, hdrlen + wpa_ie_len,
                              &rlen, (void*)&reply);
#endif /* R_WISUN_SUPPORT */
    if (rbuf == NULL)
    {
        os_free(rsn_ie_buf);
        return -1;
    }

    reply->type = EAPOL_KEY_TYPE_RSN;
    key_info = ver | WPA_KEY_INFO_KEY_TYPE;
    if (mic_len)
    {
        key_info |= WPA_KEY_INFO_MIC;
    }
    else
    {
        key_info |= WPA_KEY_INFO_ENCR_KEY_DATA;
    }
    WPA_PUT_BE16(reply->key_info, key_info);
    WPA_PUT_BE16(reply->key_length, 0);
    os_memcpy(reply->replay_counter, key->replay_counter,
              WPA_REPLAY_COUNTER_LEN);
    wpa_hexdump(MSG_DEBUG, "WPA: Replay Counter", reply->replay_counter,
                WPA_REPLAY_COUNTER_LEN);

    key_mic = (u8*)(reply + 1);
#if !R_WISUN_SUPPORT
    WPA_PUT_BE16(key_mic + mic_len, wpa_ie_len);          /* Key Data Length */
    os_memcpy(key_mic + mic_len + 2, wpa_ie, wpa_ie_len); /* Key Data */
#endif
    os_free(rsn_ie_buf);

    os_memcpy(reply->key_nonce, nonce, WPA_NONCE_LEN);

    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "WPA: Sending EAPOL-Key 2/4");
    return wpa_eapol_key_send(sm, ptk, ver, dst, ETH_P_EAPOL, rbuf, rlen,
                              key_mic);
}

// LCOV_EXCL_START
static int wpa_derive_ptk(struct wpa_sm* sm, const unsigned char* src_addr,
                          const struct wpa_eapol_key* key, struct wpa_ptk* ptk)
{
    return wpa_pmk_to_ptk(sm->pmk, sm->pmk_len, "Pairwise key expansion",
                          sm->own_addr, sm->bssid, sm->snonce,
                          key->key_nonce, ptk, sm->key_mgmt,
                          sm->pairwise_cipher);
}


static void wpa_supplicant_process_1_of_4(struct wpa_sm* sm,
                                          const unsigned char* src_addr,
                                          const struct wpa_eapol_key* key,
                                          u16 ver, const u8* key_data,
                                          size_t key_data_len)
{
    struct wpa_eapol_ie_parse ie;
    struct wpa_ptk* ptk;
    int res;
    u8* kde, * kde_buf = NULL;
    size_t kde_len;

    if (wpa_sm_get_network_ctx(sm) == NULL)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING, "WPA: No SSID info "
                "found (msg 1 of 4)");
        return;
    }

    wpa_sm_set_state(sm, WPA_4WAY_HANDSHAKE);
    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "WPA: RX message 1 of 4-Way "
            "Handshake from " MACSTR " (ver=%d)", MAC2STR(src_addr), ver);

    os_memset(&ie, 0, sizeof(ie));

    if (sm->proto == WPA_PROTO_RSN)
    {
        /* RSN: msg 1/4 should contain PMKID for the selected PMK */
        wpa_hexdump(MSG_DEBUG, "RSN: msg 1/4 key data",
                    key_data, key_data_len);
        if (wpa_supplicant_parse_ies(key_data, key_data_len, &ie) < 0)
        {
            goto failed;
        }
        if (ie.pmkid)
        {
            wpa_hexdump(MSG_DEBUG, "RSN: PMKID from "
                        "Authenticator", ie.pmkid, PMKID_LEN);
        }
    }

    res = wpa_supplicant_get_pmk(sm, src_addr, ie.pmkid);
    if (res == -2)
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "RSN: Do not reply to "
                "msg 1/4 - requesting full EAP authentication");
        return;
    }
    if (res)
    {
        goto failed;
    }

    if (sm->renew_snonce)
    {
        if (random_get_bytes(sm->snonce, WPA_NONCE_LEN))
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: Failed to get random data for SNonce");
            goto failed;
        }
        sm->renew_snonce = 0;
        wpa_hexdump(MSG_DEBUG, "WPA: Renewed SNonce",
                    sm->snonce, WPA_NONCE_LEN);
    }

    /* Calculate PTK which will be stored as a temporary PTK until it has
     * been verified when processing message 3/4. */
    ptk = &sm->tptk;
    if (wpa_derive_ptk(sm, src_addr, key, ptk) < 0)
    {
        goto failed;
    }
    sm->tptk_set = 1;

    kde = sm->assoc_wpa_ie;
    kde_len = sm->assoc_wpa_ie_len;

    if (wpa_supplicant_send_2_of_4(sm, sm->bssid, key, ver, sm->snonce,
                                   kde, kde_len, ptk) < 0)
    {
        goto failed;
    }

    os_free(kde_buf);
    os_memcpy(sm->anonce, key->key_nonce, WPA_NONCE_LEN);
    return;

failed:
    os_free(kde_buf);
    wpa_sm_deauthenticate(sm, WLAN_REASON_UNSPECIFIED);
}


static void wpa_supplicant_key_neg_complete(struct wpa_sm* sm,
                                            const u8* addr, int secure)
{
    wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
            "WPA: Key negotiation completed with "
            MACSTR " [PTK=%s GTK=%s]", MAC2STR(addr),
            wpa_cipher_txt(sm->pairwise_cipher),
            wpa_cipher_txt(sm->group_cipher));
    wpa_sm_cancel_auth_timeout(sm);
    wpa_sm_set_state(sm, WPA_COMPLETED);

    if (secure)
    {
        wpa_sm_mlme_setprotection(
            sm, addr, MLME_SETPROTECTION_PROTECT_TYPE_RX_TX,
            MLME_SETPROTECTION_KEY_TYPE_PAIRWISE);
        eapol_sm_notify_portValid(sm->eapol, TRUE);
    }
}


static int wpa_supplicant_install_ptk(struct wpa_sm*              sm,
                                      const struct wpa_eapol_key* key)
{
    int keylen, rsclen;
    enum wpa_alg alg;
    const u8* key_rsc;

    if (sm->ptk.installed)
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "WPA: Do not re-install same PTK to the driver");
        return 0;
    }

    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
            "WPA: Installing PTK to the driver");

    if (sm->pairwise_cipher == WPA_CIPHER_NONE)
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "WPA: Pairwise Cipher "
                "Suite: NONE - do not use pairwise keys");
        return 0;
    }

    if (!wpa_cipher_valid_pairwise(sm->pairwise_cipher))
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: Unsupported pairwise cipher %d",
                sm->pairwise_cipher);
        return -1;
    }

    alg = wpa_cipher_to_alg(sm->pairwise_cipher);
    keylen = wpa_cipher_key_len(sm->pairwise_cipher);
    if (keylen <= 0 || (unsigned int)keylen != sm->ptk.tk_len)
    {
        wpa_printf(MSG_DEBUG, "WPA: TK length mismatch: %d != %lu",
                   keylen, (long unsigned int)sm->ptk.tk_len);
        return -1;
    }
    rsclen = wpa_cipher_rsc_len(sm->pairwise_cipher);
    key_rsc = null_rsc;

    if (wpa_sm_set_key(sm, alg, sm->bssid, 0, 1, key_rsc, rsclen,
                       sm->ptk.tk, keylen) < 0)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: Failed to set PTK to the "
                "driver (alg=%d keylen=%d bssid=" MACSTR ")",
                alg, keylen, MAC2STR(sm->bssid));
        return -1;
    }

    /* TK is not needed anymore in supplicant */
    os_memset(sm->ptk.tk, 0, WPA_TK_MAX_LEN);
    sm->ptk.tk_len = 0;
    sm->ptk.installed = 1;

    return 0;
}


static int wpa_supplicant_check_group_cipher(struct wpa_sm* sm,
                                             int group_cipher,
                                             int keylen, int maxkeylen,
                                             int* key_rsc_len,
                                             enum wpa_alg* alg)
{
    int klen;

    *alg = wpa_cipher_to_alg(group_cipher);
    if (*alg == WPA_ALG_NONE)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: Unsupported Group Cipher %d",
                group_cipher);
        return -1;
    }
    *key_rsc_len = wpa_cipher_rsc_len(group_cipher);

    klen = wpa_cipher_key_len(group_cipher);
    if (keylen != klen || maxkeylen < klen)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: Unsupported %s Group Cipher key length %d (%d)",
                wpa_cipher_txt(group_cipher), keylen, maxkeylen);
        return -1;
    }
    return 0;
}


struct wpa_gtk_data
{
    enum wpa_alg alg;
    int          tx, key_rsc_len, keyidx;
    u8           gtk[32];
    int          gtk_len;
};


static int wpa_supplicant_install_gtk(struct wpa_sm* sm,
                                      const struct wpa_gtk_data* gd,
                                      const u8* key_rsc, int wnm_sleep)
{
    const u8* _gtk = gd->gtk;
    u8 gtk_buf[32];

    /* Detect possible key reinstallation */
    if ((sm->gtk.gtk_len == (size_t)gd->gtk_len &&
         os_memcmp(sm->gtk.gtk, gd->gtk, sm->gtk.gtk_len) == 0) ||
        (sm->gtk_wnm_sleep.gtk_len == (size_t)gd->gtk_len &&
         os_memcmp(sm->gtk_wnm_sleep.gtk, gd->gtk,
                   sm->gtk_wnm_sleep.gtk_len) == 0))
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "WPA: Not reinstalling already in-use GTK to the driver (keyidx=%d tx=%d len=%d)",
                gd->keyidx, gd->tx, gd->gtk_len);
        return 0;
    }

    wpa_hexdump_key(MSG_DEBUG, "WPA: Group Key", gd->gtk, gd->gtk_len);
    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
            "WPA: Installing GTK to the driver (keyidx=%d tx=%d len=%d)",
            gd->keyidx, gd->tx, gd->gtk_len);
    wpa_hexdump(MSG_DEBUG, "WPA: RSC", key_rsc, gd->key_rsc_len);
    if (sm->group_cipher == WPA_CIPHER_TKIP)
    {
        /* Swap Tx/Rx keys for Michael MIC */
        os_memcpy(gtk_buf, gd->gtk, 16);
        os_memcpy(gtk_buf + 16, gd->gtk + 24, 8);
        os_memcpy(gtk_buf + 24, gd->gtk + 16, 8);
        _gtk = gtk_buf;
    }
    if (sm->pairwise_cipher == WPA_CIPHER_NONE)
    {
        if (wpa_sm_set_key(sm, gd->alg, NULL,
                           gd->keyidx, 1, key_rsc, gd->key_rsc_len,
                           _gtk, gd->gtk_len) < 0)
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: Failed to set GTK to the driver "
                    "(Group only)");
            os_memset(gtk_buf, 0, sizeof(gtk_buf));
            return -1;
        }
    }
    else if (wpa_sm_set_key(sm, gd->alg, broadcast_ether_addr,
                            gd->keyidx, gd->tx, key_rsc, gd->key_rsc_len,
                            _gtk, gd->gtk_len) < 0)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: Failed to set GTK to "
                "the driver (alg=%d keylen=%d keyidx=%d)",
                gd->alg, gd->gtk_len, gd->keyidx);
        os_memset(gtk_buf, 0, sizeof(gtk_buf));
        return -1;
    }
    os_memset(gtk_buf, 0, sizeof(gtk_buf));

    if (wnm_sleep)
    {
        sm->gtk_wnm_sleep.gtk_len = gd->gtk_len;
        os_memcpy(sm->gtk_wnm_sleep.gtk, gd->gtk,
                  sm->gtk_wnm_sleep.gtk_len);
    }
    else
    {
        sm->gtk.gtk_len = gd->gtk_len;
        os_memcpy(sm->gtk.gtk, gd->gtk, sm->gtk.gtk_len);
    }

    return 0;
}


static int wpa_supplicant_gtk_tx_bit_workaround(const struct wpa_sm* sm,
                                                int                  tx)
{
    if (tx && sm->pairwise_cipher != WPA_CIPHER_NONE)
    {
        /* Ignore Tx bit for GTK if a pairwise key is used. One AP
         * seemed to set this bit (incorrectly, since Tx is only when
         * doing Group Key only APs) and without this workaround, the
         * data connection does not work because wpa_supplicant
         * configured non-zero keyidx to be used for unicast. */
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "WPA: Tx bit set for GTK, but pairwise "
                "keys are used - ignore Tx bit");
        return 0;
    }
    return tx;
}

static int wpa_supplicant_pairwise_gtk(struct wpa_sm* sm,
                                       const struct wpa_eapol_key* key,
                                       const u8* gtk, size_t gtk_len,
                                       int key_info)
{
    struct wpa_gtk_data gd;
    const u8* key_rsc;

    /*
     * IEEE Std 802.11i-2004 - 8.5.2 EAPOL-Key frames - Figure 43x
     * GTK KDE format:
     * KeyID[bits 0-1], Tx [bit 2], Reserved [bits 3-7]
     * Reserved [bits 0-7]
     * GTK
     */

    os_memset(&gd, 0, sizeof(gd));
    wpa_hexdump_key(MSG_DEBUG, "RSN: received GTK in pairwise handshake",
                    gtk, gtk_len);

    if (gtk_len < 2 || gtk_len - 2 > sizeof(gd.gtk))
    {
        return -1;
    }

    gd.keyidx = gtk[0] & 0x3;
#if R_WISUN_SUPPORT
    // Access OUI and Data Type field of KDE to determine whether this is a GTK KDE or an LGTK KDE
    if (WPA_GET_BE32(&gtk[-4]) == RSN_KEY_DATA_WISUN_LGTK)
    {
        /* Adapt key index so that auth_br callbacks can distinguish GTKs and LGTKs */
        gd.keyidx += R_AUTH_NUM_GTKS;  // LGTKs use key indices above the GTK index values
    }
#endif
    gd.tx = wpa_supplicant_gtk_tx_bit_workaround(sm,
                                                 !!(gtk[0] & BIT(2)));
    gtk += 2;
    gtk_len -= 2;

    os_memcpy(gd.gtk, gtk, gtk_len);
    gd.gtk_len = gtk_len;

    key_rsc = key->key_rsc;

    if (sm->group_cipher != WPA_CIPHER_GTK_NOT_USED &&
        (wpa_supplicant_check_group_cipher(sm, sm->group_cipher,
                                           gtk_len, gtk_len,
                                           &gd.key_rsc_len, &gd.alg) ||
         wpa_supplicant_install_gtk(sm, &gd, key_rsc, 0)))
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "RSN: Failed to install GTK");
        os_memset(&gd, 0, sizeof(gd));
        return -1;
    }
    os_memset(&gd, 0, sizeof(gd));

    wpa_supplicant_key_neg_complete(sm, sm->bssid,
                                    key_info & WPA_KEY_INFO_SECURE);
    return 0;
}

static void wpa_report_ie_mismatch(struct wpa_sm* sm,
                                   const char* reason, const u8* src_addr,
                                   const u8* wpa_ie, size_t wpa_ie_len,
                                   const u8* rsn_ie, size_t rsn_ie_len)
{
    wpa_msg(sm->ctx->msg_ctx, MSG_WARNING, "WPA: %s (src=" MACSTR ")",
            reason, MAC2STR(src_addr));

    if (sm->ap_wpa_ie)
    {
        wpa_hexdump(MSG_INFO, "WPA: WPA IE in Beacon/ProbeResp",
                    sm->ap_wpa_ie, sm->ap_wpa_ie_len);
    }
    if (wpa_ie)
    {
        if (!sm->ap_wpa_ie)
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                    "WPA: No WPA IE in Beacon/ProbeResp");
        }
        wpa_hexdump(MSG_INFO, "WPA: WPA IE in 3/4 msg",
                    wpa_ie, wpa_ie_len);
    }

    if (sm->ap_rsn_ie)
    {
        wpa_hexdump(MSG_INFO, "WPA: RSN IE in Beacon/ProbeResp",
                    sm->ap_rsn_ie, sm->ap_rsn_ie_len);
    }
    if (rsn_ie)
    {
        if (!sm->ap_rsn_ie)
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                    "WPA: No RSN IE in Beacon/ProbeResp");
        }
        wpa_hexdump(MSG_INFO, "WPA: RSN IE in 3/4 msg",
                    rsn_ie, rsn_ie_len);
    }

    wpa_sm_deauthenticate(sm, WLAN_REASON_IE_IN_4WAY_DIFFERS);
}
// LCOV_EXCL_STOP

static int wpa_supplicant_validate_ie(struct wpa_sm*             sm,
                                      const unsigned char*       src_addr,
                                      struct wpa_eapol_ie_parse* ie)
{
    // LCOV_EXCL_START
    if (sm->ap_wpa_ie == NULL && sm->ap_rsn_ie == NULL)
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "WPA: No WPA/RSN IE for this AP known. "
                "Trying to get from scan results");
        if (wpa_sm_get_beacon_ie(sm) < 0)
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: Could not find AP from "
                    "the scan results");
        }
        else
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_DEBUG,
                    "WPA: Found the current AP from "
                    "updated scan results");
        }
    }
    // LCOV_EXCL_STOP

    if (ie->wpa_ie == NULL && ie->rsn_ie == NULL &&
        (sm->ap_wpa_ie || sm->ap_rsn_ie))
    {
        wpa_report_ie_mismatch(sm, "IE in 3/4 msg does not match "
                               "with IE in Beacon/ProbeResp (no IE?)",
                               src_addr, ie->wpa_ie, ie->wpa_ie_len,
                               ie->rsn_ie, ie->rsn_ie_len);
        return -1;
    }

    if ((ie->wpa_ie && sm->ap_wpa_ie &&
         (ie->wpa_ie_len != sm->ap_wpa_ie_len ||
          os_memcmp(ie->wpa_ie, sm->ap_wpa_ie, ie->wpa_ie_len) != 0)) ||
        (ie->rsn_ie && sm->ap_rsn_ie &&
         wpa_compare_rsn_ie(wpa_key_mgmt_ft(sm->key_mgmt),
                            sm->ap_rsn_ie, sm->ap_rsn_ie_len,
                            ie->rsn_ie, ie->rsn_ie_len)))
    {
        wpa_report_ie_mismatch(sm, "IE in 3/4 msg does not match "
                               "with IE in Beacon/ProbeResp",
                               src_addr, ie->wpa_ie, ie->wpa_ie_len,
                               ie->rsn_ie, ie->rsn_ie_len);
        return -1;
    }

#if R_WISUN_SUPPORT
    if (ie->gtk_liveness > 0x0f)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_763(src_addr, ie->gtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return -1;
    }

    if (ie->lgtk_liveness > 0x07)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_769(src_addr, ie->lgtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return -1;
    }

    if (!ie->gtk_liveness && !ie->lgtk_liveness)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_775(src_addr);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return -1;
    }
#endif /* if R_WISUN_SUPPORT */
    return 0;
}

// LCOV_EXCL_START
/**
 * wpa_supplicant_send_4_of_4 - Send message 4 of WPA/RSN 4-Way Handshake
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @dst: Destination address for the frame
 * @key: Pointer to the EAPOL-Key frame header
 * @ver: Version bits from EAPOL-Key Key Info
 * @key_info: Key Info
 * @ptk: PTK to use for keyed hash and encryption
 * Returns: >= 0 on success, < 0 on failure
 */
int wpa_supplicant_send_4_of_4(struct wpa_sm* sm, const unsigned char* dst,
                               const struct wpa_eapol_key* key,
                               u16 ver, u16 key_info,
                               struct wpa_ptk* ptk)
{
    size_t mic_len, hdrlen, rlen;
    struct wpa_eapol_key* reply;
    u8* rbuf, * key_mic;

    mic_len = wpa_mic_len(sm->key_mgmt, sm->pmk_len);
    hdrlen = sizeof(*reply) + mic_len + 2;
    rbuf = wpa_sm_alloc_eapol(sm, IEEE802_1X_TYPE_EAPOL_KEY, NULL,
                              hdrlen, &rlen, (void*)&reply);
    if (rbuf == NULL)
    {
        return -1;
    }

    reply->type = EAPOL_KEY_TYPE_RSN;
    key_info &= WPA_KEY_INFO_SECURE;
    key_info |= ver | WPA_KEY_INFO_KEY_TYPE;
    if (mic_len)
    {
        key_info |= WPA_KEY_INFO_MIC;
    }
    else
    {
        key_info |= WPA_KEY_INFO_ENCR_KEY_DATA;
    }
    WPA_PUT_BE16(reply->key_info, key_info);
    WPA_PUT_BE16(reply->key_length, 0);
    os_memcpy(reply->replay_counter, key->replay_counter,
              WPA_REPLAY_COUNTER_LEN);

    key_mic = (u8*)(reply + 1);
    WPA_PUT_BE16(key_mic + mic_len, 0);

    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "WPA: Sending EAPOL-Key 4/4");
    return wpa_eapol_key_send(sm, ptk, ver, dst, ETH_P_EAPOL, rbuf, rlen,
                              key_mic);
}

static void wpa_supplicant_process_3_of_4(struct wpa_sm* sm,
                                          const struct wpa_eapol_key* key,
                                          u16 ver, const u8* key_data,
                                          size_t key_data_len)
{
    u16 key_info, keylen;
    struct wpa_eapol_ie_parse ie;

    wpa_sm_set_state(sm, WPA_4WAY_HANDSHAKE);
    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "WPA: RX message 3 of 4-Way "
            "Handshake from " MACSTR " (ver=%d)", MAC2STR(sm->bssid), ver);

    key_info = WPA_GET_BE16(key->key_info);

    wpa_hexdump(MSG_DEBUG, "WPA: IE KeyData", key_data, key_data_len);
    if (wpa_supplicant_parse_ies(key_data, key_data_len, &ie) < 0)
    {
        goto failed;
    }
    if ((ie.gtk || ie.lgtk) && !(key_info & WPA_KEY_INFO_ENCR_KEY_DATA))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_856();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        goto failed;
    }

    if (wpa_supplicant_validate_ie(sm, sm->bssid, &ie) < 0)
    {
        goto failed;
    }

    if (os_memcmp(sm->anonce, key->key_nonce, WPA_NONCE_LEN) != 0)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: ANonce from message 1 of 4-Way Handshake "
                "differs from 3 of 4-Way Handshake - drop packet (src="
                MACSTR ")", MAC2STR(sm->bssid));
        goto failed;
    }

    keylen = WPA_GET_BE16(key->key_length);
    if (keylen != wpa_cipher_key_len(sm->pairwise_cipher))
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: Invalid %s key length %d (src=" MACSTR
                ")", wpa_cipher_txt(sm->pairwise_cipher), keylen,
                MAC2STR(sm->bssid));
        goto failed;
    }

#if R_WISUN_SUPPORT
    if (ie.gtk_liveness)
    {
        /* Set GTK liveness based on GTKL (no bit shift needed since the lower 4 bits are used for GTK liveness) */
        sm->ctx->set_gtk_liveness(sm->ctx->ctx, ie.gtk_liveness);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_889(ie.gtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
    else if (ie.lgtk_liveness)
    {
        /* Set LGTK liveness based on LGTKL (shift by n bits since the lower n bits are used for GTK liveness) */
        sm->ctx->set_lgtk_liveness(sm->ctx->ctx, ie.lgtk_liveness << R_AUTH_NUM_GTKS);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_895(ie.lgtk_liveness, ie.lgtk_liveness << R_AUTH_NUM_GTKS);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
    else
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_899();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        goto failed;
    }
#else  /* if R_WISUN_SUPPORT */
    sm->ctx->set_gtk_liveness(sm->ctx->ctx, ie.gtk_liveness);
#endif /* if R_WISUN_SUPPORT */

    if (wpa_supplicant_send_4_of_4(sm, sm->bssid, key, ver, key_info,
                                   &sm->ptk) < 0)
    {
        goto failed;
    }

    /* SNonce was successfully used in msg 3/4, so mark it to be renewed
     * for the next 4-Way Handshake. If msg 3 is received again, the old
     * SNonce will still be used to avoid changing PTK. */
    sm->renew_snonce = 1;

    if (key_info & WPA_KEY_INFO_INSTALL)
    {
        if (wpa_supplicant_install_ptk(sm, key))
        {
            goto failed;
        }
    }

    if (key_info & WPA_KEY_INFO_SECURE)
    {
        wpa_sm_mlme_setprotection(
            sm, sm->bssid, MLME_SETPROTECTION_PROTECT_TYPE_RX,
            MLME_SETPROTECTION_KEY_TYPE_PAIRWISE);
        eapol_sm_notify_portValid(sm->eapol, TRUE);
    }
    wpa_sm_set_state(sm, WPA_GROUP_HANDSHAKE);

    if (sm->group_cipher == WPA_CIPHER_GTK_NOT_USED)
    {
        wpa_supplicant_key_neg_complete(sm, sm->bssid,
                                        key_info & WPA_KEY_INFO_SECURE);
    }
    else if (ie.gtk &&
             wpa_supplicant_pairwise_gtk(sm, key,
                                         ie.gtk, ie.gtk_len, key_info) < 0)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "RSN: Failed to configure GTK");
        goto failed;
    }

    if (ie.gtk)
    {
        wpa_sm_set_rekey_offload(sm);
    }

    sm->msg_3_of_4_ok = 1;
    return;

failed:
    wpa_sm_deauthenticate(sm, WLAN_REASON_UNSPECIFIED);
}
// LCOV_EXCL_STOP

static int wpa_supplicant_process_1_of_2_rsn(struct wpa_sm*       sm,
                                             const u8*            keydata,
                                             size_t               keydatalen,
                                             u16                  key_info,
                                             struct wpa_gtk_data* gd)
{
    int maxkeylen;
    struct wpa_eapol_ie_parse ie;

    wpa_hexdump_key(MSG_DEBUG, "RSN: msg 1/2 key data",
                    keydata, keydatalen);
    if (wpa_supplicant_parse_ies(keydata, keydatalen, &ie) < 0)
    {
        return -1;
    }
    if ((ie.gtk || ie.lgtk) && !(key_info & WPA_KEY_INFO_ENCR_KEY_DATA))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_978();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return -1;
    }
#if R_WISUN_SUPPORT
    if (ie.gtk_liveness)
    {
        /* Set GTK liveness based on GTKL */
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_985(ie.gtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        sm->ctx->set_gtk_liveness(sm->ctx->ctx, ie.gtk_liveness);
    }
    else if (ie.lgtk_liveness)
    {
        /* Set LGTK liveness based on LGTKL (conversion/shift is done in auth_sup callback) */
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_991(ie.lgtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        sm->ctx->set_lgtk_liveness(sm->ctx->ctx, ie.lgtk_liveness);
    }
    else
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_996();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return -1;
    }

    /* If an LGTK KDE was present in the current msg, relocate the GTK KDE pointer since they are handled equally (they
     * will be distinguished based on the OUI and Data Type field of the KDE) */
    if (ie.lgtk)
    {
        ie.gtk = ie.lgtk;
        ie.gtk_len = ie.lgtk_len;
    }

    if (ie.gtk)
    {
        gd->gtk_len = ie.gtk_len - 2;
    }
    else
    {
        gd->gtk_len = 123; // mark as GTK missing
        return 0;          // allow missing GTK for key revocation (only GTKL/LGTKL)
    }
#else /* R_WISUN_SUPPORT */
    if (ie.gtk == NULL)
    {
        // This code is unreachable
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "WPA: No GTK IE in Group Key msg 1/2");
        return -1;
    }
    gd->gtk_len = ie.gtk_len - 2;
#endif /* R_WISUN_SUPPORT */
    maxkeylen = gd->gtk_len;

    if (wpa_supplicant_check_group_cipher(sm, sm->group_cipher,
                                          gd->gtk_len, maxkeylen,
                                          &gd->key_rsc_len, &gd->alg))
    {
        return -1;
    }

    wpa_hexdump_key(MSG_DEBUG, "RSN: received GTK in group key handshake",
                    ie.gtk, ie.gtk_len);
    gd->keyidx = ie.gtk[0] & 0x3;
#if R_WISUN_SUPPORT
    // Access OUI and Data Type field of KDE to determine whether this is a GTK KDE or an LGTK KDE
    if (WPA_GET_BE32(&ie.gtk[-4]) == RSN_KEY_DATA_WISUN_LGTK)
    {
        /* Adapt key index so that auth_br callbacks can distinguish GTKs and LGTKs */
        gd->keyidx += R_AUTH_NUM_GTKS;  // LGTKs use key indices above the GTK index values
    }
#endif
    gd->tx = wpa_supplicant_gtk_tx_bit_workaround(sm,
                                                  !!(ie.gtk[0] & BIT(2)));
    if (ie.gtk_len - 2 > sizeof(gd->gtk))
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "RSN: Too long GTK in GTK IE (len=%lu)",
                (unsigned long)ie.gtk_len - 2);
        return -1;
    }
    os_memcpy(gd->gtk, ie.gtk + 2, ie.gtk_len - 2);

    return 0;
}

// LCOV_EXCL_START
static int wpa_supplicant_send_2_of_2(struct wpa_sm* sm,
                                      const struct wpa_eapol_key* key,
                                      int ver, u16 key_info)
{
    size_t mic_len, hdrlen, rlen;
    struct wpa_eapol_key* reply;
    u8* rbuf, * key_mic;

    mic_len = wpa_mic_len(sm->key_mgmt, sm->pmk_len);
    hdrlen = sizeof(*reply) + mic_len + 2;
    rbuf = wpa_sm_alloc_eapol(sm, IEEE802_1X_TYPE_EAPOL_KEY, NULL,
                              hdrlen, &rlen, (void*)&reply);
    if (rbuf == NULL)
    {
        return -1;
    }

    reply->type = EAPOL_KEY_TYPE_RSN;
    key_info &= WPA_KEY_INFO_KEY_INDEX_MASK;
    key_info |= ver | WPA_KEY_INFO_SECURE;
    if (mic_len)
    {
        key_info |= WPA_KEY_INFO_MIC;
    }
    else
    {
        key_info |= WPA_KEY_INFO_ENCR_KEY_DATA;
    }
    WPA_PUT_BE16(reply->key_info, key_info);
    WPA_PUT_BE16(reply->key_length, 0);
    os_memcpy(reply->replay_counter, key->replay_counter,
              WPA_REPLAY_COUNTER_LEN);

    key_mic = (u8*)(reply + 1);
    WPA_PUT_BE16(key_mic + mic_len, 0);

    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "WPA: Sending EAPOL-Key 2/2");
    return wpa_eapol_key_send(sm, &sm->ptk, ver, sm->bssid, ETH_P_EAPOL,
                              rbuf, rlen, key_mic);
}
// LCOV_EXCL_STOP

static void wpa_supplicant_process_1_of_2(struct wpa_sm* sm,
                                          const unsigned char* src_addr,
                                          const struct wpa_eapol_key* key,
                                          const u8* key_data,
                                          size_t key_data_len, u16 ver)
{
    u16 key_info;
    int rekey, ret = -1;
    struct wpa_gtk_data gd;
    const u8* key_rsc;

    if (!sm->msg_3_of_4_ok)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "WPA: Group Key Handshake started prior to completion of 4-way handshake");
        goto failed;
    }

    os_memset(&gd, 0, sizeof(gd));

    rekey = wpa_sm_get_state(sm) == WPA_COMPLETED;
    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "WPA: RX message 1 of Group Key "
            "Handshake from " MACSTR " (ver=%d)", MAC2STR(src_addr), ver);

    key_info = WPA_GET_BE16(key->key_info);

    if (sm->proto == WPA_PROTO_RSN)
    {
        ret = wpa_supplicant_process_1_of_2_rsn(sm, key_data,
                                                key_data_len, key_info,
                                                &gd);
    }

    wpa_sm_set_state(sm, WPA_GROUP_HANDSHAKE);

    if (ret)
    {
        goto failed;
    }

    key_rsc = key->key_rsc;

#if R_WISUN_SUPPORT
// allow missing GTK (indicated by wrong gtk_len)
    if (gd.gtk_len == 123)
    {
        if (wpa_supplicant_send_2_of_2(sm, key, ver, key_info) < 0)
        {
            goto failed;
        }
    }
    else
    {
#endif
    if (wpa_supplicant_install_gtk(sm, &gd, key_rsc, 0) ||
        wpa_supplicant_send_2_of_2(sm, key, ver, key_info) < 0)
    {
        goto failed;
    }
#if R_WISUN_SUPPORT
}
#endif
    os_memset(&gd, 0, sizeof(gd));

    if (rekey)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO, "WPA: Group rekeying "
                "completed with " MACSTR " [GTK=%s]",
                MAC2STR(sm->bssid), wpa_cipher_txt(sm->group_cipher));
        wpa_sm_cancel_auth_timeout(sm);
        wpa_sm_set_state(sm, WPA_COMPLETED);
    }
    else
    {
        wpa_supplicant_key_neg_complete(sm, sm->bssid,
                                        key_info &
                                        WPA_KEY_INFO_SECURE);
    }

    wpa_sm_set_rekey_offload(sm);

    return;

failed:
    os_memset(&gd, 0, sizeof(gd));
    wpa_sm_deauthenticate(sm, WLAN_REASON_UNSPECIFIED);
}

// LCOV_EXCL_START
static int wpa_supplicant_verify_eapol_key_mic(struct wpa_sm* sm,
                                               struct wpa_eapol_key* key,
                                               u16 ver,
                                               const u8* buf, size_t len)
{
    u8 mic[WPA_EAPOL_KEY_MIC_MAX_LEN];
    int ok = 0;
    size_t mic_len = wpa_mic_len(sm->key_mgmt, sm->pmk_len);

    os_memcpy(mic, key + 1, mic_len);
    if (sm->tptk_set)
    {
        os_memset(key + 1, 0, mic_len);
        if (wpa_eapol_key_mic(sm->tptk.kck, sm->tptk.kck_len,
                              sm->key_mgmt,
                              ver, buf, len, (u8*)(key + 1)) < 0 ||
            os_memcmp_const(mic, key + 1, mic_len) != 0)
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: Invalid EAPOL-Key MIC "
                    "when using TPTK - ignoring TPTK");
        }
        else
        {
            ok = 1;
            sm->tptk_set = 0;
            sm->ptk_set = 1;
            os_memcpy(&sm->ptk, &sm->tptk, sizeof(sm->ptk));
            os_memset(&sm->tptk, 0, sizeof(sm->tptk));
            /*
             * This assures the same TPTK in sm->tptk can never be
             * copied twice to sm->ptk as the new PTK. In
             * combination with the installed flag in the wpa_ptk
             * struct, this assures the same PTK is only installed
             * once.
             */
            sm->renew_snonce = 1;
        }
    }

    if (!ok && sm->ptk_set)
    {
        os_memset(key + 1, 0, mic_len);
        if (wpa_eapol_key_mic(sm->ptk.kck, sm->ptk.kck_len,
                              sm->key_mgmt,
                              ver, buf, len, (u8*)(key + 1)) < 0 ||
            os_memcmp_const(mic, key + 1, mic_len) != 0)
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: Invalid EAPOL-Key MIC - "
                    "dropping packet");
            return -1;
        }
        ok = 1;
    }

    if (!ok)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: Could not verify EAPOL-Key MIC - "
                "dropping packet");
        return -1;
    }

    os_memcpy(sm->rx_replay_counter, key->replay_counter,
              WPA_REPLAY_COUNTER_LEN);
    sm->rx_replay_counter_set = 1;
    return 0;
}


/* Decrypt RSN EAPOL-Key key data (RC4 or AES-WRAP) */
static int wpa_supplicant_decrypt_key_data(struct wpa_sm* sm,
                                           struct wpa_eapol_key* key,
                                           size_t mic_len, u16 ver,
                                           u8* key_data, size_t* key_data_len)
{
    wpa_hexdump(MSG_DEBUG, "RSN: encrypted key data",
                key_data, *key_data_len);
    if (!sm->ptk_set)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: PTK not available, cannot decrypt EAPOL-Key Key "
                "Data");
        return -1;
    }

    /* Decrypt key data here so that this operation does not need
     * to be implemented separately for each message type. */
    if (ver == WPA_KEY_INFO_TYPE_HMAC_MD5_RC4 && sm->ptk.kek_len == 16)
    {
#ifdef CONFIG_NO_RC4
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: RC4 not supported in the build");
        return -1;
#else /* CONFIG_NO_RC4 */
        u8 ek[32];

        wpa_printf(MSG_DEBUG, "WPA: Decrypt Key Data using RC4");
        os_memcpy(ek, key->key_iv, 16);
        os_memcpy(ek + 16, sm->ptk.kek, sm->ptk.kek_len);
        if (rc4_skip(ek, 32, 256, key_data, *key_data_len))
        {
            os_memset(ek, 0, sizeof(ek));
            wpa_msg(sm->ctx->msg_ctx, MSG_ERROR,
                    "WPA: RC4 failed");
            return -1;
        }
        os_memset(ek, 0, sizeof(ek));
#endif /* CONFIG_NO_RC4 */
    }
    else if (ver == WPA_KEY_INFO_TYPE_HMAC_SHA1_AES ||
             ver == WPA_KEY_INFO_TYPE_AES_128_CMAC ||
             wpa_use_aes_key_wrap(sm->key_mgmt))
    {
        u8* buf;

        wpa_printf(MSG_DEBUG,
                   "WPA: Decrypt Key Data using AES-UNWRAP (KEK length %u)",
                   (unsigned int)sm->ptk.kek_len);
        if (*key_data_len < 8 || *key_data_len % 8)
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: Unsupported AES-WRAP len %u",
                    (unsigned int)*key_data_len);
            return -1;
        }
        *key_data_len -= 8;  /* AES-WRAP adds 8 bytes */
        buf = os_malloc(*key_data_len);
        if (buf == NULL)
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: No memory for AES-UNWRAP buffer");
            return -1;
        }
        if (aes_unwrap(sm->ptk.kek, sm->ptk.kek_len, *key_data_len / 8,
                       key_data, buf))
        {
            bin_clear_free(buf, *key_data_len);
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: AES unwrap failed - "
                    "could not decrypt EAPOL-Key key data");
            return -1;
        }
        os_memcpy(key_data, buf, *key_data_len);
        bin_clear_free(buf, *key_data_len);
        WPA_PUT_BE16(((u8*)(key + 1)) + mic_len, *key_data_len);
    }
    else
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: Unsupported key_info type %d", ver);
        return -1;
    }
    wpa_hexdump_key(MSG_DEBUG, "WPA: decrypted EAPOL-Key key data",
                    key_data, *key_data_len);
    return 0;
}


static void wpa_eapol_key_dump(struct wpa_sm* sm,
                               const struct wpa_eapol_key* key,
                               unsigned int key_data_len,
                               const u8* mic, unsigned int mic_len)
{
#ifndef CONFIG_NO_STDOUT_DEBUG
    u16 key_info = WPA_GET_BE16(key->key_info);

    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "  EAPOL-Key type=%d", key->type);
    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
            "  key_info 0x%x (ver=%d keyidx=%d rsvd=%d %s%s%s%s%s%s%s%s)",
            key_info, key_info & WPA_KEY_INFO_TYPE_MASK,
            (key_info & WPA_KEY_INFO_KEY_INDEX_MASK) >>
            WPA_KEY_INFO_KEY_INDEX_SHIFT,
            (key_info & (BIT(13) | BIT(14) | BIT(15))) >> 13,
            key_info & WPA_KEY_INFO_KEY_TYPE ? "Pairwise" : "Group",
            key_info & WPA_KEY_INFO_INSTALL ? " Install" : "",
            key_info & WPA_KEY_INFO_ACK ? " Ack" : "",
            key_info & WPA_KEY_INFO_MIC ? " MIC" : "",
            key_info & WPA_KEY_INFO_SECURE ? " Secure" : "",
            key_info & WPA_KEY_INFO_ERROR ? " Error" : "",
            key_info & WPA_KEY_INFO_REQUEST ? " Request" : "",
            key_info & WPA_KEY_INFO_ENCR_KEY_DATA ? " Encr" : "");
    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
            "  key_length=%u key_data_length=%u",
            WPA_GET_BE16(key->key_length), key_data_len);
    wpa_hexdump(MSG_DEBUG, "  replay_counter",
                key->replay_counter, WPA_REPLAY_COUNTER_LEN);
    wpa_hexdump(MSG_DEBUG, "  key_nonce", key->key_nonce, WPA_NONCE_LEN);
    wpa_hexdump(MSG_DEBUG, "  key_iv", key->key_iv, 16);
    wpa_hexdump(MSG_DEBUG, "  key_rsc", key->key_rsc, 8);
    wpa_hexdump(MSG_DEBUG, "  key_id (reserved)", key->key_id, 8);
    wpa_hexdump(MSG_DEBUG, "  key_mic", mic, mic_len);
#endif /* CONFIG_NO_STDOUT_DEBUG */
}

/**
 * wpa_sm_rx_eapol - Process received WPA EAPOL frames
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @src_addr: Source MAC address of the EAPOL packet
 * @buf: Pointer to the beginning of the EAPOL data (EAPOL header)
 * @len: Length of the EAPOL frame
 * Returns: 1 = WPA EAPOL-Key processed, 0 = not a WPA EAPOL-Key, -1 failure
 *
 * This function is called for each received EAPOL frame. Other than EAPOL-Key
 * frames can be skipped if filtering is done elsewhere. wpa_sm_rx_eapol() is
 * only processing WPA and WPA2 EAPOL-Key frames.
 *
 * The received EAPOL-Key packets are validated and valid packets are replied
 * to. In addition, key material (PTK, GTK) is configured at the end of a
 * successful key handshake.
 */
int wpa_sm_rx_eapol(struct wpa_sm* sm, const u8* src_addr,
                    const u8* buf, size_t len)
{
    size_t plen, data_len, key_data_len;
    const struct ieee802_1x_hdr* hdr;
    struct wpa_eapol_key* key;
    u16 key_info, ver;
    u8* tmp = NULL;
    int ret = -1;
    u8* mic, * key_data;
    size_t mic_len, keyhdrlen;

    mic_len = wpa_mic_len(sm->key_mgmt, sm->pmk_len);
    keyhdrlen = sizeof(*key) + mic_len + 2;

    if (len < sizeof(*hdr) + keyhdrlen)
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "WPA: EAPOL frame too short to be a WPA "
                "EAPOL-Key (len %lu, expecting at least %lu)",
                (unsigned long)len,
                (unsigned long)sizeof(*hdr) + keyhdrlen);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1427((uint32_t)len, (uint32_t)(sizeof(*hdr) + keyhdrlen));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return 0;
    }

    hdr = (const struct ieee802_1x_hdr*)buf;
    plen = be_to_host16(hdr->length);
    data_len = plen + sizeof(*hdr);
    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
            "IEEE 802.1X RX: version=%d type=%d length=%lu",
            hdr->version, hdr->type, (unsigned long)plen);

    if (hdr->version < EAPOL_VERSION)
    {
        /* TODO: backwards compatibility */
    }
    if (hdr->type != IEEE802_1X_TYPE_EAPOL_KEY)
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "WPA: EAPOL frame (type %u) discarded, "
                "not a Key frame", hdr->type);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1447((uint8_t)hdr->type);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        ret = 0;
        goto out;
    }
    wpa_hexdump(MSG_MSGDUMP, "WPA: RX EAPOL-Key", buf, len);
    if (plen > len - sizeof(*hdr) || plen < keyhdrlen)
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "WPA: EAPOL frame payload size %lu "
                "invalid (frame size %lu)",
                (unsigned long)plen, (unsigned long)len);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_1458((uint32_t)plen, (uint32_t)len);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        ret = 0;
        goto out;
    }
    if (data_len < len)
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "WPA: ignoring %lu bytes after the IEEE 802.1X data",
                (unsigned long)len - data_len);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1467((uint32_t)(len - data_len));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
    }

    /*
     * Make a copy of the frame since we need to modify the buffer during
     * MAC validation and Key Data decryption.
     */
    tmp = os_memdup(buf, data_len);
    if (tmp == NULL)
    {
        goto out;
    }
    key = (struct wpa_eapol_key*)(tmp + sizeof(struct ieee802_1x_hdr));
    mic = (u8*)(key + 1);
    key_data = mic + mic_len + 2;

    if (key->type != EAPOL_KEY_TYPE_WPA && key->type != EAPOL_KEY_TYPE_RSN)
    {
        wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG,
                "WPA: EAPOL-Key type (%d) unknown, discarded",
                key->type);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1488((uint8_t)key->type);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        ret = 0;
        goto out;
    }

    key_data_len = WPA_GET_BE16(mic + mic_len);
    wpa_eapol_key_dump(sm, key, key_data_len, mic, mic_len);

    if (key_data_len > plen - keyhdrlen)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO, "WPA: Invalid EAPOL-Key "
                "frame - key_data overflow (%u > %u)",
                (unsigned int)key_data_len,
                (unsigned int)(plen - keyhdrlen));
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1502((uint16_t)key_data_len, (uint16_t)(plen - keyhdrlen));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        goto out;
    }

    eapol_sm_notify_lower_layer_success(sm->eapol, 0);
    key_info = WPA_GET_BE16(key->key_info);
    ver = key_info & WPA_KEY_INFO_TYPE_MASK;
    if (ver != WPA_KEY_INFO_TYPE_HMAC_MD5_RC4 &&
        ver != WPA_KEY_INFO_TYPE_HMAC_SHA1_AES &&
        !wpa_use_akm_defined(sm->key_mgmt))
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "WPA: Unsupported EAPOL-Key descriptor version %d",
                ver);
        goto out;
    }

    if (wpa_use_akm_defined(sm->key_mgmt) &&
        ver != WPA_KEY_INFO_TYPE_AKM_DEFINED)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "RSN: Unsupported EAPOL-Key descriptor version %d (expected AKM defined = 0)",
                ver);
        goto out;
    }

    if (sm->pairwise_cipher == WPA_CIPHER_CCMP &&
        !wpa_use_akm_defined(sm->key_mgmt) &&
        ver != WPA_KEY_INFO_TYPE_HMAC_SHA1_AES)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "WPA: CCMP is used, but EAPOL-Key "
                "descriptor version (%d) is not 2", ver);
        if (sm->group_cipher != WPA_CIPHER_CCMP &&
            !(key_info & WPA_KEY_INFO_KEY_TYPE))
        {
            /* Earlier versions of IEEE 802.11i did not explicitly
             * require version 2 descriptor for all EAPOL-Key
             * packets, so allow group keys to use version 1 if
             * CCMP is not used for them. */
            wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                    "WPA: Backwards compatibility: allow invalid "
                    "version for non-CCMP group keys");
        }
        else if (ver == WPA_KEY_INFO_TYPE_AES_128_CMAC)
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                    "WPA: Interoperability workaround: allow incorrect (should have been HMAC-SHA1), but stronger (is AES-128-CMAC), descriptor version to be used");
        }
        else
        {
            goto out;
        }
    }
    else if (sm->pairwise_cipher == WPA_CIPHER_GCMP &&
             !wpa_use_akm_defined(sm->key_mgmt) &&
             ver != WPA_KEY_INFO_TYPE_HMAC_SHA1_AES)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "WPA: GCMP is used, but EAPOL-Key "
                "descriptor version (%d) is not 2", ver);
        goto out;
    }

    if (sm->rx_replay_counter_set &&
        os_memcmp(key->replay_counter, sm->rx_replay_counter,
                  WPA_REPLAY_COUNTER_LEN) <= 0)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                "WPA: EAPOL-Key Replay Counter did not increase - "
                "dropping packet");
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1573();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        goto out;
    }

    if (key_info & WPA_KEY_INFO_SMK_MESSAGE)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "WPA: Unsupported SMK bit in key_info");
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1581();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        goto out;
    }

    if (!(key_info & WPA_KEY_INFO_ACK))
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "WPA: No Ack bit in key_info");
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1589();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        goto out;
    }

    if (key_info & WPA_KEY_INFO_REQUEST)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_INFO,
                "WPA: EAPOL-Key with Request bit - dropped");
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1597();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        goto out;
    }

    if ((key_info & WPA_KEY_INFO_MIC) &&
        wpa_supplicant_verify_eapol_key_mic(sm, key, ver, tmp, data_len))
    {
        goto out;
    }

    if (sm->proto == WPA_PROTO_RSN && (key_info & WPA_KEY_INFO_ENCR_KEY_DATA) && mic_len)
    {
        /*
         * Only decrypt the Key Data field if the frame's authenticity
         * was verified. When using AES-SIV (FILS), the MIC flag is not
         * set, so this check should only be performed if mic_len != 0
         * which is the case in this code branch.
         */
        if (!(key_info & WPA_KEY_INFO_MIC))
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: Ignore EAPOL-Key with encrypted but unauthenticated data");
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
            r_loggen_1619();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
            goto out;
        }
        if (wpa_supplicant_decrypt_key_data(sm, key, mic_len,
                                            ver, key_data,
                                            &key_data_len))
        {
            goto out;
        }
    }

    if (key_info & WPA_KEY_INFO_KEY_TYPE)
    {
        if (key_info & WPA_KEY_INFO_KEY_INDEX_MASK)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
            r_loggen_1634();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
            goto out;
        }
        if (key_info & (WPA_KEY_INFO_MIC |
                        WPA_KEY_INFO_ENCR_KEY_DATA))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_1640();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

            /* 3/4 4-Way Handshake */
            wpa_supplicant_process_3_of_4(sm, key, ver, key_data,
                                          key_data_len);
        }
        else
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_1648();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

            /* 1/4 4-Way Handshake */
            wpa_supplicant_process_1_of_4(sm, src_addr, key,
                                          ver, key_data,
                                          key_data_len);
        }
    }
    else
    {
        if ((mic_len && (key_info & WPA_KEY_INFO_MIC)) ||
            (!mic_len && (key_info & WPA_KEY_INFO_ENCR_KEY_DATA)))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_1661();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

            /* 1/2 Group Key Handshake */
            wpa_supplicant_process_1_of_2(sm, src_addr, key,
                                          key_data, key_data_len,
                                          ver);
        }
        else
        {
            wpa_msg(sm->ctx->msg_ctx, MSG_WARNING,
                    "WPA: EAPOL-Key (Group) without Mic/Encr bit - "
                    "dropped");
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
            r_loggen_1673();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        }
    }

    ret = 1;

out:
    bin_clear_free(tmp, data_len);
    return ret;
}

static void wpa_sm_pmksa_free_cb(struct rsn_pmksa_cache_entry* entry,
                                 void* ctx, enum pmksa_free_reason reason)
{
    struct wpa_sm* sm = ctx;
    int deauth = 0;

    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "RSN: PMKSA cache entry free_cb: "
            MACSTR " reason=%d", MAC2STR(entry->aa), reason);

    if (deauth)
    {
        sm->pmk_len = 0;
        os_memset(sm->pmk, 0, sizeof(sm->pmk));
        wpa_sm_deauthenticate(sm, WLAN_REASON_UNSPECIFIED);
    }
}


/**
 * wpa_sm_init - Initialize WPA state machine
 * @ctx: Context pointer for callbacks; this needs to be an allocated buffer
 * Returns: Pointer to the allocated WPA state machine data
 *
 * This function is used to allocate a new WPA state machine and the returned
 * value is passed to all WPA state machine calls.
 */
struct wpa_sm* wpa_sm_init(struct wpa_sm_ctx* ctx)
{
    struct wpa_sm* sm;

    sm = os_zalloc(sizeof(*sm));
    if (sm == NULL)
    {
        return NULL;
    }
    dl_list_init(&sm->pmksa_candidates);
    sm->renew_snonce = 1;
    sm->ctx = ctx;

    sm->dot11RSNAConfigPMKLifetime = 43200;
    sm->dot11RSNAConfigPMKReauthThreshold = 70;
    sm->dot11RSNAConfigSATimeout = 60;

    sm->pmksa = pmksa_cache_init(wpa_sm_pmksa_free_cb, sm, sm);
    if (sm->pmksa == NULL)
    {
        wpa_msg(sm->ctx->msg_ctx, MSG_ERROR,
                "RSN: PMKSA cache initialization failed");
        os_free(sm);
        return NULL;
    }

    return sm;
}


/**
 * wpa_sm_deinit - Deinitialize WPA state machine
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 */
void wpa_sm_deinit(struct wpa_sm* sm)
{
    if (sm == NULL)
    {
        return;
    }
    pmksa_cache_deinit(sm->pmksa);
    os_free(sm->assoc_wpa_ie);
    os_free(sm->ap_wpa_ie);
    os_free(sm->ap_rsn_ie);
    wpa_sm_drop_sa(sm);
    os_free(sm->ctx);
#ifdef CONFIG_TESTING_OPTIONS
    wpabuf_free(sm->test_assoc_ie);
#endif /* CONFIG_TESTING_OPTIONS */
    os_free(sm);
}


/**
 * wpa_sm_set_pmk - Set PMK
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @pmk: The new PMK
 * @pmk_len: The length of the new PMK in bytes
 * @pmkid: Calculated PMKID
 * @bssid: AA to add into PMKSA cache or %NULL to not cache the PMK
 *
 * Configure the PMK for WPA state machine.
 */
void wpa_sm_set_pmk(struct wpa_sm* sm, const u8* pmk, size_t pmk_len,
                    const u8* pmkid, const u8* bssid)
{
    if (sm == NULL)
    {
        return;
    }

    wpa_hexdump_key(MSG_DEBUG, "WPA: Set PMK based on external data",
                    pmk, pmk_len);
    sm->pmk_len = pmk_len;
    os_memcpy(sm->pmk, pmk, pmk_len);
}

/**
 * wpa_sm_set_own_addr - Set own MAC address
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @addr: Own MAC address
 */
void wpa_sm_set_own_addr(struct wpa_sm* sm, const u8* addr)
{
    if (sm)
    {
        os_memcpy(sm->own_addr, addr, ETH_ALEN);
    }
}


/**
 * wpa_sm_set_eapol - Set EAPOL state machine pointer
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @eapol: Pointer to EAPOL state machine allocated with eapol_sm_init()
 */
void wpa_sm_set_eapol(struct wpa_sm* sm, struct eapol_sm* eapol)
{
    if (sm)
    {
        sm->eapol = eapol;
    }
}


/**
 * wpa_sm_set_param - Set WPA state machine parameters
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @param: Parameter field
 * @value: Parameter value
 * Returns: 0 on success, -1 on failure
 */
int wpa_sm_set_param(struct wpa_sm* sm, enum wpa_sm_conf_params param,
                     unsigned int value)
{
    int ret = 0;

    if (sm == NULL)
    {
        return -1;
    }

    switch (param)
    {
        case WPA_PARAM_PROTO:
            sm->proto = value;
            break;

        case WPA_PARAM_PAIRWISE:
            sm->pairwise_cipher = value;
            break;

        case WPA_PARAM_GROUP:
            sm->group_cipher = value;
            break;

        case WPA_PARAM_KEY_MGMT:
            sm->key_mgmt = value;
            break;

        default:
            break;
    }

    return ret;
}


/**
 * wpa_sm_set_assoc_wpa_ie_default - Generate own WPA/RSN IE from configuration
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @wpa_ie: Pointer to buffer for WPA/RSN IE
 * @wpa_ie_len: Pointer to the length of the wpa_ie buffer
 * Returns: 0 on success, -1 on failure
 */
int wpa_sm_set_assoc_wpa_ie_default(struct wpa_sm* sm, u8* wpa_ie,
                                    size_t* wpa_ie_len)
{
    int res;

    if (sm == NULL)
    {
        return -1;
    }

#ifdef CONFIG_TESTING_OPTIONS
    if (sm->test_assoc_ie)
    {
        wpa_printf(MSG_DEBUG,
                   "TESTING: Replace association WPA/RSN IE");
        if (*wpa_ie_len < wpabuf_len(sm->test_assoc_ie))
        {
            return -1;
        }
        os_memcpy(wpa_ie, wpabuf_head(sm->test_assoc_ie),
                  wpabuf_len(sm->test_assoc_ie));
        res = wpabuf_len(sm->test_assoc_ie);
    }
    else
#endif /* CONFIG_TESTING_OPTIONS */
    res = wpa_gen_wpa_ie(sm, wpa_ie, *wpa_ie_len);
    if (res < 0)
    {
        return -1;
    }
    *wpa_ie_len = res;

    wpa_hexdump(MSG_DEBUG, "WPA: Set own WPA IE default",
                wpa_ie, *wpa_ie_len);

    if (sm->assoc_wpa_ie == NULL)
    {
        /*
         * Make a copy of the WPA/RSN IE so that 4-Way Handshake gets
         * the correct version of the IE even if PMKSA caching is
         * aborted (which would remove PMKID from IE generation).
         */
        sm->assoc_wpa_ie = os_memdup(wpa_ie, *wpa_ie_len);
        if (sm->assoc_wpa_ie == NULL)
        {
            return -1;
        }

        sm->assoc_wpa_ie_len = *wpa_ie_len;
    }
    else
    {
        wpa_hexdump(MSG_DEBUG,
                    "WPA: Leave previously set WPA IE default",
                    sm->assoc_wpa_ie, sm->assoc_wpa_ie_len);
    }

    return 0;
}


void wpa_sm_drop_sa(struct wpa_sm* sm)
{
    wpa_dbg(sm->ctx->msg_ctx, MSG_DEBUG, "WPA: Clear old PMK and PTK");
    sm->ptk_set = 0;
    sm->tptk_set = 0;
    sm->pmk_len = 0;
    os_memset(sm->pmk, 0, sizeof(sm->pmk));
    os_memset(&sm->ptk, 0, sizeof(sm->ptk));
    os_memset(&sm->tptk, 0, sizeof(sm->tptk));
    os_memset(&sm->gtk, 0, sizeof(sm->gtk));
    os_memset(&sm->gtk_wnm_sleep, 0, sizeof(sm->gtk_wnm_sleep));
}

#ifdef CONFIG_TESTING_OPTIONS

void wpa_sm_set_test_assoc_ie(struct wpa_sm* sm, struct wpabuf* buf)
{
    wpabuf_free(sm->test_assoc_ie);
    sm->test_assoc_ie = buf;
}


const u8* wpa_sm_get_anonce(struct wpa_sm* sm)
{
    return sm->anonce;
}

// LCOV_EXCL_STOP
#endif /* CONFIG_TESTING_OPTIONS */
