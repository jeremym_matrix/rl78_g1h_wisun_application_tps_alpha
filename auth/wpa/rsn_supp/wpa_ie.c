/*
 * wpa_supplicant - WPA/RSN IE and KDE processing
 * Copyright (c) 2003-2018, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */

#include "includes.h"

#include "common.h"
#include "wpa.h"
#include "pmksa_cache.h"
#include "common/ieee802_11_defs.h"
#include "wpa_i.h"
#include "wpa_ie.h"

#if R_WISUN_SUPPORT
#include "r_auth_internal_initmsg.h"
#endif

// LCOV_EXCL_START

static int wpa_gen_wpa_ie_rsn(u8* rsn_ie, size_t rsn_ie_len,
                              int pairwise_cipher, int group_cipher,
                              int key_mgmt, int mgmt_group_cipher,
                              struct wpa_sm* sm)
{
    u8* pos;
    struct rsn_ie_hdr* hdr;
    u16 capab;
    u32 suite;

    if (rsn_ie_len < sizeof(*hdr) + RSN_SELECTOR_LEN +
        2 + RSN_SELECTOR_LEN + 2 + RSN_SELECTOR_LEN + 2 +
        (sm->cur_pmksa ? 2 + PMKID_LEN : 0))
    {
        wpa_printf(MSG_DEBUG, "RSN: Too short IE buffer (%lu bytes)",
                   (unsigned long)rsn_ie_len);
        return -1;
    }

    hdr = (struct rsn_ie_hdr*)rsn_ie;
    hdr->elem_id = WLAN_EID_RSN;
    WPA_PUT_LE16(hdr->version, RSN_VERSION);
    pos = (u8*)(hdr + 1);

    suite = wpa_cipher_to_suite(WPA_PROTO_RSN, group_cipher);
    if (suite == 0)
    {
        wpa_printf(MSG_WARNING, "Invalid group cipher (%d).",
                   group_cipher);
        return -1;
    }
    RSN_SELECTOR_PUT(pos, suite);
    pos += RSN_SELECTOR_LEN;

    *pos++ = 1;
    *pos++ = 0;
    suite = wpa_cipher_to_suite(WPA_PROTO_RSN, pairwise_cipher);
    if (suite == 0 ||
        (!wpa_cipher_valid_pairwise(pairwise_cipher) &&
         pairwise_cipher != WPA_CIPHER_NONE))
    {
        wpa_printf(MSG_WARNING, "Invalid pairwise cipher (%d).",
                   pairwise_cipher);
        return -1;
    }
    RSN_SELECTOR_PUT(pos, suite);
    pos += RSN_SELECTOR_LEN;

    *pos++ = 1;
    *pos++ = 0;
    if (key_mgmt == WPA_KEY_MGMT_IEEE8021X)
    {
        RSN_SELECTOR_PUT(pos, RSN_AUTH_KEY_MGMT_UNSPEC_802_1X);
    }
    else
    {
        wpa_printf(MSG_WARNING, "Invalid key management type (%d).",
                   key_mgmt);
        return -1;
    }
    pos += RSN_SELECTOR_LEN;

    /* RSN Capabilities */
    capab = 0;
    WPA_PUT_LE16(pos, capab);
    pos += 2;

    if (sm->cur_pmksa)
    {
        /* PMKID Count (2 octets, little endian) */
        *pos++ = 1;
        *pos++ = 0;

        /* PMKID */
        os_memcpy(pos, sm->cur_pmksa->pmkid, PMKID_LEN);
        pos += PMKID_LEN;
    }

    hdr->len = (pos - rsn_ie) - 2;

    WPA_ASSERT((size_t)(pos - rsn_ie) <= rsn_ie_len);

    return pos - rsn_ie;
}

/**
 * wpa_gen_wpa_ie - Generate WPA/RSN IE based on current security policy
 * @sm: Pointer to WPA state machine data from wpa_sm_init()
 * @wpa_ie: Pointer to memory area for the generated WPA/RSN IE
 * @wpa_ie_len: Maximum length of the generated WPA/RSN IE
 * Returns: Length of the generated WPA/RSN IE or -1 on failure
 */
int wpa_gen_wpa_ie(struct wpa_sm* sm, u8* wpa_ie, size_t wpa_ie_len)
{
    if (sm->proto == WPA_PROTO_RSN)
    {
        return wpa_gen_wpa_ie_rsn(wpa_ie, wpa_ie_len,
                                  sm->pairwise_cipher,
                                  sm->group_cipher,
                                  sm->key_mgmt, sm->mgmt_group_cipher,
                                  sm);
    }
    else
    {
        return -1;
    }
}
// LCOV_EXCL_STOP

/**
 * wpa_parse_vendor_specific - Parse Vendor Specific IEs
 * @pos: Pointer to the IE header
 * @end: Pointer to the end of the Key Data buffer
 * @ie: Pointer to parsed IE data
 * Returns: 0 on success, 1 if end mark is found, -1 on failure
 */
static int wpa_parse_vendor_specific(const u8* pos, const u8* end,
                                     struct wpa_eapol_ie_parse* ie)
{
    uint32_t oui;

    if (pos[1] < 4)
    {
        wpa_printf(MSG_MSGDUMP, "Too short vendor specific IE ignored (len=%u)",
                   pos[1]);
        return 1;
    }

    oui = WPA_GET_BE24(&pos[2]);
#if R_WISUN_SUPPORT
    // LGTK KDE
    if (oui == OUI_WISUN && pos[5] == KDT_LGTK)
    {
        ie->lgtk = &pos[6];
        ie->lgtk_len = pos[1] - RSN_SELECTOR_LEN;
    }

    /* If an LGTK KDE is present in the current msg, relocate the GTK KDE pointer since they are handled equally.
     * They will be distinguished based on the OUI and Data Type field of the KDE. */
    if (ie->lgtk)
    {
        ie->gtk = ie->lgtk;
        ie->gtk_len = ie->lgtk_len;
    }

    // GTKL KDE
    if (oui == OUI_WISUN && pos[5] == KDT_GTKL)
    {
        ie->gtk_liveness = pos[6];
    }

    // GTKL KDE
    if (oui == OUI_WISUN && pos[5] == KDT_LGTKL)
    {
        ie->lgtk_liveness = pos[6];
    }
#endif /* if R_WISUN_SUPPORT */
    return 0;
}

// LCOV_EXCL_START
/**
 * wpa_parse_generic - Parse EAPOL-Key Key Data Generic IEs
 * @pos: Pointer to the IE header
 * @end: Pointer to the end of the Key Data buffer
 * @ie: Pointer to parsed IE data
 * Returns: 0 on success, 1 if end mark is found, -1 on failure
 */
static int wpa_parse_generic(const u8* pos, const u8* end,
                             struct wpa_eapol_ie_parse* ie)
{
    if (pos[1] == 0)
    {
        return 1;
    }

    if (pos[1] >= 6 &&
        RSN_SELECTOR_GET(pos + 2) == WPA_OUI_TYPE &&
        pos[2 + WPA_SELECTOR_LEN] == 1 &&
        pos[2 + WPA_SELECTOR_LEN + 1] == 0)
    {
        ie->wpa_ie = pos;
        ie->wpa_ie_len = pos[1] + 2;
        wpa_hexdump(MSG_DEBUG, "WPA: WPA IE in EAPOL-Key",
                    ie->wpa_ie, ie->wpa_ie_len);
        return 0;
    }

    if (1 + RSN_SELECTOR_LEN < end - pos &&
        pos[1] >= RSN_SELECTOR_LEN + PMKID_LEN &&
        RSN_SELECTOR_GET(pos + 2) == RSN_KEY_DATA_PMKID)
    {
        ie->pmkid = pos + 2 + RSN_SELECTOR_LEN;
        wpa_hexdump(MSG_DEBUG, "WPA: PMKID in EAPOL-Key",
                    pos, pos[1] + 2);
        return 0;
    }

    if (pos[1] > RSN_SELECTOR_LEN + 2 &&
        RSN_SELECTOR_GET(pos + 2) == RSN_KEY_DATA_GROUPKEY)
    {
        ie->gtk = pos + 2 + RSN_SELECTOR_LEN;
        ie->gtk_len = pos[1] - RSN_SELECTOR_LEN;
        wpa_hexdump_key(MSG_DEBUG, "WPA: GTK in EAPOL-Key",
                        pos, pos[1] + 2);
        return 0;
    }

    if (pos[1] > RSN_SELECTOR_LEN + 2 &&
        RSN_SELECTOR_GET(pos + 2) == RSN_KEY_DATA_MAC_ADDR)
    {
        ie->mac_addr = pos + 2 + RSN_SELECTOR_LEN;
        ie->mac_addr_len = pos[1] - RSN_SELECTOR_LEN;
        wpa_hexdump(MSG_DEBUG, "WPA: MAC Address in EAPOL-Key",
                    pos, pos[1] + 2);
        return 0;
    }

    return 0;
}


/**
 * wpa_supplicant_parse_ies - Parse EAPOL-Key Key Data IEs
 * @buf: Pointer to the Key Data buffer
 * @len: Key Data Length
 * @ie: Pointer to parsed IE data
 * Returns: 0 on success, -1 on failure
 */
int wpa_supplicant_parse_ies(const u8* buf, size_t len,
                             struct wpa_eapol_ie_parse* ie)
{
    const u8* pos, * end;
    int ret = 0;

    os_memset(ie, 0, sizeof(*ie));
    for (pos = buf, end = pos + len; end - pos > 1; pos += 2 + pos[1])
    {
        if (pos[0] == 0xdd &&
            ((pos == buf + len - 1) || pos[1] == 0))
        {
            /* Ignore padding */
            break;
        }
        if (2 + pos[1] > end - pos)
        {
            wpa_printf(MSG_DEBUG, "WPA: EAPOL-Key Key Data "
                       "underflow (ie=%d len=%d pos=%d)",
                       pos[0], pos[1], (int)(pos - buf));
            wpa_hexdump_key(MSG_DEBUG, "WPA: Key Data",
                            buf, len);
            ret = -1;
            break;
        }
        if (*pos == WLAN_EID_VENDOR_SPECIFIC)
        {
            ret = wpa_parse_generic(pos, end, ie);
            if (ret < 0)
            {
                break;
            }
            if (ret > 0)
            {
                ret = 0;
                break;
            }

            ret = wpa_parse_vendor_specific(pos, end, ie);
            if (ret < 0)
            {
                break;
            }
            if (ret > 0)
            {
                ret = 0;
                break;
            }
        }
        else
        {
            wpa_hexdump(MSG_DEBUG, "WPA: Unrecognized EAPOL-Key "
                        "Key Data IE", pos, 2 + pos[1]);
        }
    }

    return ret;
}
// LCOV_EXCL_STOP
