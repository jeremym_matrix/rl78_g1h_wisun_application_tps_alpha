/*
 * wpa_supplicant/hostapd - Build time configuration defines
 * Copyright (c) 2005-2006, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 *
 * This header file can be used to define configuration defines that were
 * originally defined in Makefile. This is mainly meant for IDE use or for
 * systems that do not have suitable 'make' tool. In these cases, it may be
 * easier to have a single place for defining all the needed C pre-processor
 * defines.
 */

#ifndef BUILD_CONFIG_H
#define BUILD_CONFIG_H

/* Insert configuration defines, e.g., #define EAP_MD5, here, if needed. */

#include "r_stdint.h"

// BEGIN RENESAS #defines
// Reduced list from hostap/eap_example/Makefile
#define IEEE8021X_EAPOL
#define EAP_TLS
#define EAP_SERVER
#define EAP_SERVER_IDENTITY
#define EAP_SERVER_TLS

// WPA support for 4-way handshake
#define CONFIG_NO_VLAN
#define CONFIG_NO_RADIUS

// Marker flag for changes in WPA sources
#define R_WISUN_SUPPORT 1

// Disable features to reduce footprint
#define CONFIG_NO_RANDOM_POOL
#define CONFIG_NO_RC4

// We need wrappers for the alloc functions
#define OS_NO_C_LIB_DEFINES
// #define WPA_TRACE 1
#if !__unix__
#define CONFIG_NO_STDOUT_DEBUG
#define CONFIG_NO_WPA_MSG
#define CONFIG_NO_HOSTAPD_LOGGER

#ifndef __LITTLE_ENDIAN
#define __LITTLE_ENDIAN 1
#endif
#define __BYTE_ORDER    __LITTLE_ENDIAN

static inline uint16_t bswap_16(uint16_t v)
{
    return ((v & 0xff) << 8) | (v >> 8);
}
static inline uint32_t bswap_32(uint32_t v)
{
    return ((v & 0xff) << 24) | ((v & 0xff00) << 8) |
           ((v & 0xff0000) >> 8) | (v >> 24);
}

#endif  /* !__unix__ */

// END RENESAS #defines


#ifdef CONFIG_WIN32_DEFAULTS
#define CONFIG_NATIVE_WINDOWS
#define CONFIG_ANSI_C_EXTRA
#define CONFIG_WINPCAP
#define IEEE8021X_EAPOL
#define PKCS12_FUNCS
#define PCSC_FUNCS
#define CONFIG_CTRL_IFACE
#define CONFIG_CTRL_IFACE_NAMED_PIPE
#define CONFIG_DRIVER_NDIS
#define CONFIG_NDIS_EVENTS_INTEGRATED
#define CONFIG_DEBUG_FILE
#define EAP_MD5
#define EAP_TLS
#define EAP_MSCHAPv2
#define EAP_PEAP
#define EAP_TTLS
#define EAP_GTC
#define EAP_OTP
#define EAP_LEAP
#define EAP_TNC
#define _CRT_SECURE_NO_DEPRECATE

#ifdef USE_INTERNAL_CRYPTO
#define CONFIG_TLS_INTERNAL_CLIENT
#define CONFIG_INTERNAL_LIBTOMMATH
#define CONFIG_CRYPTO_INTERNAL
#endif /* USE_INTERNAL_CRYPTO */
#endif /* CONFIG_WIN32_DEFAULTS */

#endif /* BUILD_CONFIG_H */
