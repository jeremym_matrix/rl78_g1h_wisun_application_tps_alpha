/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   @file      r_auth_tls.h
   @version   1.00
   @brief     Header file for the TLS implementation
 */

#ifndef R_AUTH_TLS_H
#define R_AUTH_TLS_H

#include "r_tls_mbedtls.h"
#include "r_stdint.h"

#define TLS_RECORD_HEADER_LENGTH 5

int  tls_client_init(const uint8_t* client_cert, const uint8_t* cert_index, r_tls_state_t* tls);
void tls_client_reset(r_tls_state_t* tls);
int  tls_client_connection_established(struct tls_connection* connection);
int  tls_client_export_key(r_tls_state_t* tls, uint8_t* out, size_t out_len);
int  tls_client_handshake(r_tls_state_t* tls, struct wpabuf* in_data);

#endif /* R_AUTH_TLS_H */
