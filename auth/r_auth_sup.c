/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   @file      r_auth_sup.c
   @version   1.00
   @brief     Wi-SUN FAN Supplicant implementation
 */

#include "r_auth_sup.h"

#include <string.h>

#include "r_auth_common.h"
#include "r_auth_internal.h"
#include "r_auth_internal_initmsg.h"
#include "r_auth_eap_tls.h"
#include "r_auth_tls.h"
#include "r_os_wrapper.h"

// Include for memory optimization (only supported in single-chip architecture)
#if !(R_MODEM_SERVER || R_MODEM_CLIENT)
#include "r_nwk_os_wrapper.h"
#endif

#include "r_auth_types.h"
#include "r_os_wrapper_config.h"

// name clash with WPA
#undef ARRAY_SIZE
#undef BIT
#include "r_impl_utils.h"
#include "r_byte_swap.h"

// WPA includes
#include "rsn_supp/wpa.h"
#include "rsn_supp/wpa_i.h"

// R_LOG_PREFIX is processed by RLog code generator
#define R_LOG_PREFIX AUTH
#include "r_log_internal.h"
#if R_LOG_THRESHOLD > R_LOG_SEVERITY_OFF
#include "r_loggen_r_auth_sup.h"
#endif

/******************************************************************************
* Debugging & Development
******************************************************************************/

// allow easy changing for debugging
#define MSG_UNUSED_CB MSG_DEBUG
#define unused_marker(x) do {} while (0)

/******************************************************************************
* Wi-SUN Initial Message Functionality
******************************************************************************/

static void rsn_save_state(r_apl_global_t* nwk);
static void rsn_restore_state(r_apl_global_t* nwk);
static void rsn_free(r_apl_global_t* nwk);

/**
 * Create a KDE ID header
 * @param kde The KDE structure
 * @param oui The OUI
 * @param data_type The data type
 * @param data_len The size of the payload
 * @return The size of the KDE ID (incl. data_len)
 */
static uint8_t create_kde_header(struct wisun_kde_s* kde, uint32_t oui, uint8_t data_type, uint8_t data_len)
{
    if (kde == NULL)
    {
        return 0;
    }

    kde->type = KDE_TYPE;
    kde->len = (uint8_t)(3 + 1 + data_len);
    WPA_PUT_BE24(kde->oui, oui);
    kde->data_type = data_type;
    return sizeof(*kde) + data_len;
}

/******************************************************************************
* Utility Functions
******************************************************************************/

/**
 * Send an EAPOL message
 * @param nwk The global NWK information structure
 * @param dst the destination address
 * @param type the eapol message type
 * @param data the payload
 * @param size the payload size
 * @return R_RESULT_SUCCESS on successful transmission. Appropriate error code otherwise
 */
static r_result_t eapol_send(r_apl_global_t* nwk, const uint8_t dst[8], r_mac_eapol_type_t type, const uint8_t* data, uint16_t size)
{
    r_result_t res;

    if (nwk == NULL)
    {
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

    /* Prepare message for transmission by NWK layer */
    uint16_t msgSize = size + R_AUTH_CREATEMESSAGE_MAX_OVERHEAD;
    uint8_t* msgBuf = os_malloc(msgSize);
    if (!msgBuf)
    {
        return R_RESULT_INSUFFICIENT_BUFFER;
    }
    res = R_AUTH_CreateMessage(type, data, size, msgBuf, &msgSize, &type);
    if (res == R_RESULT_SUCCESS)
    {
        res = R_NWK_EapolDataRequest(type, dst, NULL, msgBuf, msgSize);
        if (res != R_RESULT_SUCCESS)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_140((uint8_t)res, MSGTYPE(type), size, dst);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        }
        else
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_144(MSGTYPE(type), size, dst);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        }
    }
    os_free(msgBuf);
    return res;
}

/**
 * Send an EAPOL message embedded in a fully prepared OS message buffer including MAC data for max memory optimization
 * @param nwk The global NWK information structure
 * @param dst The destination address
 * @param data The fully prepared message buffer for the MAC layer. Ownership is transferred so this pointer MUST NOT
 * be used after calling this function.
 * @param size The size of the data buffer
 * @return R_RESULT_SUCCESS on successful transmission. Appropriate error code otherwise
 */
static r_result_t eapol_send_opt(r_apl_global_t* nwk, const uint8_t* dst, uint8_t* data, uint16_t size)
{
    if (nwk == NULL || dst == NULL)
    {
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

#if !(R_MODEM_SERVER || R_MODEM_CLIENT)
    r_result_t res = R_NWK_EapolDataRequestOptimized(dst, data, size);
    data = NULL;  // Ownership transferred
#else
    /* Optimized EAPOL data request not supported in modem architecture -> Discard MAC part of data buffer and use
     * non-optimized request */
    size_t off = nwk->auth.sup.mac_offset;
    r_result_t res = R_NWK_EapolDataRequest(data[off], dst, NULL, &data[off + 1], size - (off + 1));
    os_free(data);
#endif
    if (res != R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_179((uint8_t)res, size, dst);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
    }
    else
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_183(size, dst);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }

    return res;
}

/**
 * Determine if an an EAPOL message is directed to the BR
 * @param data The EAPOL message
 * @return 1 if directed to the BR
 */
static int isToBR(const void* data)
{
    const struct eapol_message_s* msg = (const struct eapol_message_s*)data;
    if (msg->eapol.type == IEEE802_1X_TYPE_EAP_PACKET)
    {
        return msg->d.eap.hdr.code == EAP_RESPONSE;
    }
    return (WPA_GET_BE16(msg->d.key.key_info) & WPA_KEY_INFO_ACK) == 0;
}

/******************************************************************************
* EAP-TLS Functionality
******************************************************************************/

typedef struct r_auth_eap_config_s
{
    uint8_t* encoded_certs;
    uint8_t* cert_index;
} r_auth_sup_eap_config_t;

/** EAP-TLS context of the supplicant */
typedef struct r_auth_sup_eap_ctx_s
{
    r_auth_sup_eap_config_t config;    //!< EAP-TLS configuration
    r_auth_eap_state        state;     //!< Current state of the EAP-TLS handshake
    uint8_t                 eap_id;    //!< The identifier from in the last EAP request received
    r_tls_state_t           tls_state; //!< Current state of the TLS handshake
} r_auth_sup_eap_ctx_t;

/**
 * Create the EAP context
 * @param nwk The global NWK information structure
 * @return 0 on success, -1 on error
 */
static int eap_create(r_apl_global_t* nwk)
{
    r_auth_sup_eap_ctx_t* ctx = NULL;

    if (nwk == NULL)
    {
        return -1;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_237();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    r_result_t indResult = R_NWK_EapTlsIsStarting();
    if (indResult != R_RESULT_SUCCESS)
    {
        return -1;
    }

    ctx = os_malloc(sizeof(r_auth_sup_eap_ctx_t));
    if (!ctx)
    {
        R_NWK_EapTlsIsDone();
        return -1;
    }
    MEMZERO_S(ctx);

    ctx->config.encoded_certs = (u8*)nwk->auth.sup.encoded_certs;
    ctx->config.cert_index = &nwk->auth.sup.cert_index;

    nwk->auth.sup.eap_ctx = ctx;

    // Make sure we wipe out an existing RSN context if we are restarting the EAP-TLS exchange
    rsn_free(nwk);
    return 0;
}

/**
 * Free the EAP context
 * @param nwk The global NWK information structure
 */
static void eap_free(r_apl_global_t* nwk)
{
    if (nwk == NULL)
    {
        return;
    }

    struct r_auth_sup_eap_ctx_s* ctx = nwk->auth.sup.eap_ctx;
    if (ctx)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_276();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        tls_client_reset(&ctx->tls_state);
        os_free(ctx);
        nwk->auth.sup.eap_ctx = NULL;
        R_NWK_EapTlsIsDone();
    }
}

/**
 * Send an EAP-TLS response packet
 * @param mac_buffer The fully prepared message buffer for the MAC layer. Ownership is transferred so this pointer
 * MUST NOT be used after calling this function
 * @param tls_payload_size The size of the TLS payload in bytes
 * @param nwk The global NWK information structure
 * @return R_RESULT_SUCCESS on successful transmission. Appropriate error code otherwise
 */
static r_result_t eap_tls_send_response(uint8_t* mac_buffer, uint16_t tls_payload_size, r_apl_global_t* nwk)
{
    r_auth_eap_tls_mpx_fragment_t* mpx = (r_auth_eap_tls_mpx_fragment_t*)(&mac_buffer[nwk->auth.sup.mac_offset]);

    /* Set EAP header fields */
    mpx->payload.eap.code = EAP_RESPONSE;
    mpx->payload.eap.id = nwk->auth.sup.eap_ctx->eap_id;
    uint16_t eap_length = sizeof(r_auth_eap_header_t) + sizeof(r_auth_eap_tls_header_t) + tls_payload_size;
    UInt16ToArr(eap_length, mpx->payload.eap.length);
    mpx->payload.eap.type = R_EAP_TYPE_EAP_TLS;
    mpx->payload.eap_tls.eap_tls_flags = 0;

    /* Set EAPOL header fields */
    mpx->payload.eapol.version = 0x03; // version (spec v17: 3)
    mpx->payload.eapol.type = 0x00;    // type: EAP
    UInt16ToArr(eap_length, (uint8_t*)(&mpx->payload.eapol.length));

    /* Set KMP ID */
    mpx->kmp_id = R_EAPOL_TYPE_8021X;

    size_t mpx_ie_size = eap_length + sizeof(r_auth_eapol_header_t) + 1;
    return eapol_send_opt(nwk, nwk->auth.sup.target, mac_buffer, nwk->auth.sup.mac_offset + mpx_ie_size);
}

/**
 * Export the PMK from the ongoing EAP-TLS handshake, copy it to nwkGlobal and terminate the handshake
 * @param nwk The global NWK information structure
 * @return R_RESULT_SUCCESS if the PMK was exported and copied to nwkGlobal. Appropriate error code otherwise
 * @return R_RESULT_ILLEGAL_STATE if no EAP-TLS handshake is ongoing
 * @return R_RESULT_FAILED if the PMK could not be exported from the ongoing EAP-TLS handshake
 * @details The EAP-TLS handshake will be terminated (context is destroyed) when this function returns
 */
static r_result_t tls_finish_handshake(r_apl_global_t* nwk)
{
    /* Verify that an EAP-TLS handshake is currently ongoing */
    if (nwk->auth.sup.eap_ctx == NULL || !tls_client_connection_established(nwk->auth.sup.eap_ctx->tls_state.conn))
    {
        return R_RESULT_ILLEGAL_STATE;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_332();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    uint8_t pmk_buf[R_AUTH_PMK_SIZE];
    if (tls_client_export_key(&nwk->auth.sup.eap_ctx->tls_state, pmk_buf, sizeof(pmk_buf)))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_336();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        eap_free(nwk);
        return R_RESULT_FAILED;
    }
    memcpy(nwk->auth.sup.pmk, pmk_buf, R_AUTH_PMK_SIZE);
    nwk->auth.sup.pmk_valid = 1;

    eap_free(nwk);

    return R_RESULT_SUCCESS;
}

/**
 * Pass TLS data to mbed TLS to continue the TLS handshake and (optionally) send appropriate EAPOL response afterwards
 * @param data Incoming TLS data. Ownership is transferred to this function so the data MUST NOT be used afterwards
 * @param size The size of the incoming TLS data buffer
 * @param more_fragments Flag to indicate whether additional fragments will follow or not
 * @param nwk The global NWK information structure
 * @return R_RESULT_SUCCESS if the TLS handshake could be continued. Appropriate error code otherwise
 */
static r_result_t tls_continue_handshake(uint8_t* data, uint16_t size, int more_fragments, r_apl_global_t* nwk)
{
    r_auth_sup_eap_ctx_t* eap_ctx = nwk->auth.sup.eap_ctx;

    if (eap_ctx->tls_state.conn->push_buf)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_362((uint32_t)wpabuf_len(eap_ctx->tls_state.conn->push_buf));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
    }

    size_t msg_initial_size = nwk->auth.sup.mac_offset + sizeof(r_auth_eap_tls_mpx_fragment_t);

    /* Create OS message that can be passed to the MAC layer later for transmission. mbed TLS will incrementally resize
     * this message and append its data. This avoids any data duplication before transmission (memory optimization) */
#if R_MODEM_CLIENT
    eap_ctx->tls_state.conn->push_buf = wpabuf_alloc_ext_data_with_ownership(os_malloc(msg_initial_size), msg_initial_size);
#else
    eap_ctx->tls_state.conn->push_buf = wpabuf_alloc_ext_data_with_ownership(R_NWK_OS_AllocMsg(R_HEAP_ID_AUTH, msg_initial_size), msg_initial_size);
#endif
    if (!eap_ctx->tls_state.conn->push_buf || !eap_ctx->tls_state.conn->push_buf->buf)
    {
        return R_RESULT_INSUFFICIENT_BUFFER;
    }

    /* Mark initial bytes as used so that they are not overwritten by upcoming wpabuf operations */
    eap_ctx->tls_state.conn->push_buf->used = wpabuf_size(eap_ctx->tls_state.conn->push_buf);

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_382(size);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    int tls_status = tls_client_handshake(&eap_ctx->tls_state,
                                          size ? wpabuf_alloc_ext_data_with_ownership(data, size) : NULL);

    r_result_t res = R_RESULT_FAILED;
    if (tls_status == 0 || tls_status == MBEDTLS_ERR_SSL_WANT_READ)
    {
        size_t msg_size = wpabuf_len(eap_ctx->tls_state.conn->push_buf);
        if (msg_size > msg_initial_size
            || (msg_size == msg_initial_size && tls_client_connection_established(eap_ctx->tls_state.conn)))
        {
            size_t tls_payload_size = msg_size - msg_initial_size;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_394((uint32_t)tls_payload_size);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            res = eap_tls_send_response(wpabuf_mhead_u8(eap_ctx->tls_state.conn->push_buf), tls_payload_size, nwk);
            eap_ctx->tls_state.conn->push_buf->buf = NULL;  // Ownership transferred -> prevent further usage
        }
        else if (more_fragments)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_400();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            size_t ack_size = nwk->auth.sup.mac_offset + sizeof(r_auth_eap_tls_mpx_fragment_t);
#if R_MODEM_CLIENT
            uint8_t* ack_msg = os_malloc(ack_size);
#else
            uint8_t* ack_msg = R_NWK_OS_AllocMsg(R_HEAP_ID_AUTH, ack_size);
#endif
            if (ack_msg == NULL)
            {
                eap_free(nwk);
                return R_RESULT_INSUFFICIENT_BUFFER;
            }
            res = eap_tls_send_response(ack_msg, 0, nwk);
        }
        else
        {
            /* mbed TLS has nothing to send and requires more data (but there are no more fragments) */
            eap_free(nwk);
            return R_RESULT_FAILED;
        }
    }

    /* Always free the allocated wpabuf; The external data might be already freed by MAC layer but the wpabuf is not */
    wpabuf_free(eap_ctx->tls_state.conn->push_buf);
    eap_ctx->tls_state.conn->push_buf = NULL;

    return res;
}

/**
 * Process an incoming TLS message encapsulated in an EAPOL packet
 * @param eapol_data The EAPOL packet that contains the TLS data to process
 * @param eapol_size The size of the EAPOL packet
 * @param nwk The global NWK information structure
 * @return R_RESULT_SUCCESS if the TLS packet was successfully processed. Appropriate error code otherwise
 */
static r_result_t tls_process_message(uint8_t* eapol_data, uint16_t eapol_size, r_apl_global_t* nwk)
{
    if (eapol_size <= sizeof(r_auth_eap_tls_packet_t))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_440((uint32_t)sizeof(r_auth_eap_tls_packet_t));
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        eap_free(nwk);
        os_free(eapol_data);
        return R_RESULT_FAILED;
    }
    uint16_t first_tls_record_offset = sizeof(r_auth_eap_tls_packet_t);
    uint8_t eap_tls_flags = ((r_auth_eap_tls_packet_t*)(eapol_data))->eap_tls.eap_tls_flags;
    if (eap_tls_flags & EAP_TLS_FLAG_LENGTH_INCLUDED)
    {
        first_tls_record_offset += 4;  // Skip four-octet TLS Message Length field
        if (eapol_size <= first_tls_record_offset)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_452(first_tls_record_offset);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            eap_free(nwk);
            os_free(eapol_data);
            return R_RESULT_FAILED;
        }
    }
    uint16_t tls_payload_size = eapol_size - first_tls_record_offset;  // Is guaranteed to be > 0

    /* Move TLS payload to the front of the message buffer */
    memmove(eapol_data, &eapol_data[first_tls_record_offset], tls_payload_size);
    uint8_t* tls_data = os_realloc(eapol_data, tls_payload_size);
    if (tls_data == NULL) // LCOV_EXCL_BR_LINE
    {                     /* This code is unreachable since umm_realloc never fails if the new size is smaller than the old size */
        // LCOV_EXCL_START
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_466();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        eap_free(nwk);
        os_free(eapol_data);
        return R_RESULT_INSUFFICIENT_BUFFER;
        // LCOV_EXCL_STOP
    }
    return tls_continue_handshake(tls_data, tls_payload_size, eap_tls_flags & EAP_TLS_FLAG_MORE_FRAGMENTS, nwk);
}

/**
 * Process an incoming EAP message
 * @param nwk The global NWK information structure
 * @param source The EUI-64 source address of the message
 * @param authenticator The EUI-64 of the authenticator if the EAPOL-Authenticator-EUI-64 IE is present (may be NULL)
 * @param data The message. This MUST be a pointer to dynamically allocated memory on the AUTH heap. The data buffer is
 * reduced iteratively during processing and finally freed so the pointer MUST NOT be used after calling this function
 * @param data_len The size of the message
 * @return R_RESULT_SUCCESS if the EAP message was processed successfully. Appropriate error code otherwise
 */
static r_result_t eap_process_message(r_apl_global_t* nwk, const uint8_t* source, const uint8_t* authenticator,
                                      uint8_t* data, uint16_t data_len)
{
    if ((nwk == NULL) || (source == NULL) || (data == NULL))
    {
        os_free(data);
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

    if (data_len < EAPOL_MIN_LENGTH)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_496((uint16_t)EAPOL_MIN_LENGTH);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        os_free(data);
        return R_RESULT_INVALID_PARAMETER;
    }

    r_auth_eap_tls_packet_t* msg = (r_auth_eap_tls_packet_t*)data;
    if (msg->eap.code == EAP_FAILURE)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_504();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        eap_free(nwk);
        os_free(data);
        return R_RESULT_SUCCESS;
    }

    if (msg->eap.code >= EAP_INVALID_CODE)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_512(msg->eap.code);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        os_free(data);
        return R_RESULT_SUCCESS;
    }

    r_auth_sup_state_t* sup = &nwk->auth.sup;
    if ((sup->eap_ctx == NULL || sup->eap_ctx->state != EAP_STATE_WAIT_FOR_SUCCESS) && msg->eap.code == EAP_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_520();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        os_free(data);
        return R_RESULT_SUCCESS;
    }

    if (sup->eap_ctx == NULL)
    {
        if (eap_create(nwk))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_529();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            os_free(data);
            return R_RESULT_FAILED;
        }
        memcpy(sup->target, source, 8);
        if (!sup->pmk_valid)
        {
            memcpy(sup->authenticator, source, 8);
        }
    }

    if (authenticator && MEMISNOTZERO(authenticator, 8))
    {
        memcpy(sup->authenticator, authenticator, 8);
    }

    r_auth_sup_eap_ctx_t* ctx = sup->eap_ctx;
    if (msg->eap.code == EAP_REQUEST)
    {
        if ((msg->eap.id == ctx->eap_id) && (ctx->state != EAP_STATE_WAIT_FOR_IDENTITY_REQUEST))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
            r_loggen_550(msg->eap.id);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
            os_free(data);
            return R_RESULT_SUCCESS;
        }
        ctx->eap_id = msg->eap.id;  // Update EAP id on incoming request
    }

    /* Process the incoming EAP message (according to the current state of the EAP-TLS handshake) */
    r_result_t res = R_RESULT_FAILED;
    if (ctx->state == EAP_STATE_WAIT_FOR_IDENTITY_REQUEST)
    {
        if (msg->eap.type != R_EAP_TYPE_IDENTITY)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_563(R_EAP_TYPE_IDENTITY, msg->eap.type);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            eap_free(nwk);
            os_free(data);
            return R_RESULT_FAILED;
        }

        /* Send Identity Response message with current EAP ID */
        r_auth_eap_response_identity_t* response = os_malloc(sizeof(r_auth_eap_response_identity_t));
        response->identity[0] = 'r';  // identity is not be specified in Wi-SUN spec (certificates matter)
        response->eap_header.code = EAP_RESPONSE;
        response->eap_header.type = R_EAP_TYPE_IDENTITY;
        response->eap_header.id = ctx->eap_id;
        UInt16ToArr(sizeof(r_auth_eap_response_identity_t), response->eap_header.length);

        res = eapol_send(nwk, sup->target, R_EAPOL_TYPE_CONVERT_FROM_EAP, (const uint8_t*)response,
                         sizeof(r_auth_eap_response_identity_t));
        os_free(response);
        ctx->state = EAP_STATE_WAIT_FOR_EAP_TLS_START;
    }
    else if (ctx->state == EAP_STATE_WAIT_FOR_EAP_TLS_START)
    {
        if (data_len < sizeof(r_auth_eap_tls_packet_t) || msg->eap.type != R_EAP_TYPE_EAP_TLS
            || !(msg->eap_tls.eap_tls_flags & EAP_TLS_FLAG_START))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
            r_loggen_587();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
            os_free(data);
            return R_RESULT_FAILED;
        }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_592();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        if (tls_client_init(ctx->config.encoded_certs, ctx->config.cert_index, &ctx->tls_state))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_595();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            eap_free(nwk);
            os_free(data);
            return R_RESULT_FAILED;
        }

        res = tls_continue_handshake(NULL, 0, 0, nwk);
        if (res != R_RESULT_SUCCESS)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_604();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            eap_free(nwk);
            os_free(data);
            return R_RESULT_FAILED;
        }
        ctx->state = EAP_STATE_TLS_HANDSHAKE_ONGOING;
    }
    else if (ctx->state == EAP_STATE_TLS_HANDSHAKE_ONGOING)
    {
        res = tls_process_message(data, data_len, nwk);
        data = NULL;  // ownership transferred (prevent further usage)
        if (res != R_RESULT_SUCCESS)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_617();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            eap_free(nwk);
            return R_RESULT_FAILED;
        }

        if (tls_client_connection_established(ctx->tls_state.conn))
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_624();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
            ctx->state = EAP_STATE_WAIT_FOR_SUCCESS;
        }
    }
    else if (ctx->state == EAP_STATE_WAIT_FOR_SUCCESS)
    {
        if (msg->eap.code == EAP_SUCCESS && msg->eap.id == ctx->eap_id)
        {
            res = tls_finish_handshake(nwk);
        }
        else
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
            r_loggen_636();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        }
    }
    else
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_641();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        eap_free(nwk);
    }

    os_free(data);
    return res;
}


/******************************************************************************
* RSN (4-Way-Handshake and Group Key Handshake) Functionality
******************************************************************************/

/* Unused Callbacks */
static enum wpa_states rsncb_get_state(void* ctx)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_get_state");
    unused_marker("rsncb_get_state");
    return WPA_DISCONNECTED;
}
static void rsncb_deauthenticate(void* ctx, int reason_code)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_deauthenticate: reason_code: %d", reason_code);
    unused_marker("rsncb_deauthenticate");
}
static int rsncb_get_bssid(void* ctx, u8* bssid)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_get_bssid: bssid: %02x%02x...", bssid[0], bssid[1]);
    unused_marker("rsncb_get_bssid");
    return 0;
}
static int rsncb_get_beacon_ie(void* ctx)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_get_beacon_ie");
    unused_marker("rsncb_get_beacon_ie");
    return 0;
}
static void rsncb_cancel_auth_timeout(void* ctx)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_cancel_auth_timeout");
    unused_marker("rsncb_cancel_auth_timeout");
}
static int rsncb_add_pmkid(void* ctx, void* network_ctx, const u8* bssid, const u8* pmkid, const u8* fils_cache_id, const u8* pmk, size_t pmk_len)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_add_pmkid: bssid: %02x%02x..., pmkid: %02x%02x...", bssid[0], bssid[1], pmkid[0], pmkid[1]);
    unused_marker("rsncb_add_pmkid");
    return 0;
}
static int rsncb_remove_pmkid(void* ctx, void* network_ctx, const u8* bssid, const u8* pmkid, const u8* fils_cache_id)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_remove_pmkid: bssid: %02x%02x..., pmkid: %02x%02x...", bssid[0], bssid[1], pmkid[0], pmkid[1]);
    unused_marker("rsncb_remove_pmkid");
    return 0;
}
static void rsncb_set_config_blob(void* ctx, struct wpa_config_blob* blob)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_set_config_blob");
    unused_marker("rsncb_set_config_blob");
}
static const struct wpa_config_blob* rsncb_get_config_blob(void* ctx, const char* name)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_get_config_blob: name: %s", name);
    unused_marker("rsncb_get_config_blob");
    return NULL;
}
static int rsncb_mlme_setprotection(void* ctx, const u8* addr, int protection_type, int key_type)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_mlme_setprotection: addr: %02x%02x..., protection_type: %d, key_type: %d", addr[0], addr[1], protection_type, key_type);
    unused_marker("rsncb_mlme_setprotection");
    return 0;
}
static int rsncb_update_ft_ies(void* ctx, const u8* md, const u8* ies, size_t ies_len)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_update_ft_ies: md: %02x%02x..., ies: %02x%02x..., ies_len: %zu", md[0], md[1], ies[0], ies[1], ies_len);
    unused_marker("rsncb_update_ft_ies");
    return 0;
}
static int rsncb_send_ft_action(void* ctx, u8 action, const u8* target_ap, const u8* ies, size_t ies_len)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_send_ft_action: action: %d, target_ap: %02x%02x..., ies: %02x%02x..., ies_len: %zu", action, target_ap[0], target_ap[1], ies[0], ies[1], ies_len);
    unused_marker("rsncb_send_ft_action");
    return 0;
}
static int rsncb_mark_authenticated(void* ctx, const u8* target_ap)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_mark_authenticated: target_ap: %02x%02x...", target_ap[0], target_ap[1]);
    unused_marker("rsncb_mark_authenticated");
    return 0;
}
static void rsncb_set_rekey_offload(void* ctx, const u8* kek, size_t kek_len, const u8* kck, size_t kck_len, const u8* replay_ctr)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_set_rekey_offload: kek: %02x%02x..., kek_len: %zu, kck: %02x%02x..., kck_len: %zu, replay_ctr: %02x%02x...", kek[0], kek[1], kek_len, kck[0], kck[1], kck_len, replay_ctr[0], replay_ctr[1]);
    unused_marker("rsncb_set_rekey_offload");
}
static int rsncb_key_mgmt_set_pmk(void* ctx, const u8* pmk, size_t pmk_len)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_key_mgmt_set_pmk: pmk: %02x%02x..., pmk_len: %zu", pmk[0], pmk[1], pmk_len);
    unused_marker("rsncb_key_mgmt_set_pmk");
    return 0;
}
static void rsncb_set_state(void* ctx, enum wpa_states state)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_set_state: state: %d", state);
    unused_marker("rsncb_set_state");
}

/**
 * WPA callback to the the network context
 *
 * @details This is only called from wpa_supplicant_process_1_of_4 and just compared with NULL.
 * The original WPA implementation calls wpa_supplicant_get_ssid
 * @param ctx the WPA context
 * @return not-null
 */
static void* rsncb_get_network_ctx(void* ctx)
{
    wpa_printf(MSG_UNUSED_CB, "rsncb_get_network_ctx: ctx: %p", ctx);
    return ctx;
}

/**
 * WPA callback function to allocate an EAPOL message
 *
 * @details Copied and adapted from wpa_supplicant/wpas_glue.c
 * @param ctx the WPA context
 * @param type EAPOL type
 * @param data the payload
 * @param data_len the payload length
 * @param[out] msg_len the message length (header plus data_len)
 * @param[out] data_pos the start of data in the allocated buffer
 * @return the buffer or NULL in case of error
 */
static u8* rsncb_alloc_eapol(void* ctx, u8 type, const void* data, u16 data_len, size_t* msg_len, void** data_pos)
{
    if (msg_len == NULL)
    {
        return NULL;
    }

    wpa_printf(MSG_UNUSED_CB, "rsncb_alloc_eapol");
    struct ieee802_1x_hdr* hdr;

    *msg_len = sizeof(*hdr) + data_len;
    hdr = os_malloc(*msg_len);
    if (hdr == NULL)
    {
        return NULL;
    }

    hdr->version = 3;  // changed from original, spec v17
    hdr->type = type;
    hdr->length = host_to_be16(data_len);

    if (data)
    {
        os_memcpy(hdr + 1, data, data_len);
    }
    else
    {
        os_memset(hdr + 1, 0, data_len);
    }

    if (data_pos)
    {
        *data_pos = hdr + 1;
    }

    return (u8*)hdr;
}

/**
 * WPA callback to send an EAPOL message
 * @param ctx the WPA context
 * @param dest unused
 * @param proto unused
 * @param buf the message
 * @param len the size of the message
 * @return 0 on success, -1 on error
 */
static int rsncb_ether_send(void* ctx, const u8* dest, u16 proto, const u8* buf, size_t len)
{
    if (ctx == NULL)
    {
        return -1;
    }

    r_apl_global_t* nwk = (r_apl_global_t*)ctx;
    r_result_t res = eapol_send(nwk, nwk->auth.sup.target, R_EAPOL_TYPE_CONVERT_FROM_4WH_OR_GKH, buf, (uint16_t)len);
    if (res != R_RESULT_SUCCESS)
    {
        return -1;
    }
    return 0;
}

/**
 * WPA callback to set the keys
 * @param ctx the WPA context
 * @param alg unused
 * @param addr unused
 * @param key_idx the key index
 * @param set_tx key is used for sending
 * @param seq unused
 * @param seq_len unused
 * @param key the key
 * @param key_len the length of the key
 * @return 0 to indicate success
  */
static int rsncb_set_key(void* ctx, enum wpa_alg alg, const u8* addr, int key_idx, int set_tx, const u8* seq, size_t seq_len, const u8* key, size_t key_len)
{
    if (ctx == NULL)
    {
        return -1;
    }

    r_auth_sup_state_t* sup = &((r_apl_global_t*)ctx)->auth.sup;
    wpa_printf(MSG_UNUSED_CB, "rsncb_set_key: alg: %d, addr: %02x%02x..., key_idx: %d, set_tx: %d, seq: %02x%02x..., seq_len: %zu, key: %02x%02x..., key_len: %zu", alg, addr[0], addr[1], key_idx, set_tx, seq[0], seq[1], seq_len, key[0], key[1], key_len);
    if (set_tx && key_idx == 0)
    {
        // The call just includes the TK -> get the full PTK from the state machine
        const struct wpa_ptk* ptk = &((struct wpa_sm*)sup->wpa_sm)->ptk;
        memcpy(sup->ptk, ptk->kck, 16);
        memcpy(sup->ptk + 16, ptk->kek, 16);
        memcpy(sup->ptk + 32, ptk->tk, 16);
        sup->ptk_valid = 1;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_866(sup->ptk);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
    else if (key && set_tx == 0 && key_idx >= 0 && key_idx < R_KEY_NUM && key_len == R_AUTH_GTK_SIZE)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_870((uint16_t)key_idx, key, ((r_apl_global_t*)ctx)->auth.gtks_valid);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        R_AUTH_SetGTK((r_apl_global_t*)ctx, (uint8_t)key_idx, key);
    }
    return 0;
}

/**
 * New WPA callback function to set the GTK liveness from the GTKL
 * @param ctx the WPA context
 * @param liveness the GTKL liveness bitmask
 */
static void rsncb_set_gtk_liveness(void* ctx, u8 liveness)
{
#if R_AUTH_NUM_GTKS
    r_apl_global_t* nwk = (r_apl_global_t*)ctx;
    uint8_t oldGtksLive = nwk->auth.sup.gtks_live;
    LOG_ONLY_VAR(oldGtksLive);

    /* Set only GTK liveness bits and leave the LGTK liveness bits untouched */
    nwk->auth.sup.gtks_live &= ~R_AUTH_GTK_LIVENESS_MASK;
    nwk->auth.sup.gtks_live |= (liveness & R_AUTH_GTK_LIVENESS_MASK);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_891((uint8_t)liveness, oldGtksLive, nwk->auth.sup.gtks_live, nwk->auth.gtks_valid);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    for (uint8_t i = 0; i < R_KEY_NUM; i++)
    {
        if (GTK_IS_VALID(i) && !(nwk->auth.sup.gtks_live & (1 << i)))
        {
            R_AUTH_DeleteGTK(nwk, i);
        }
    }
#else  /* if R_AUTH_NUM_GTKS */
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
    r_loggen_901(liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
#endif /* R_AUTH_NUM_GTKS */
}

/**
 * New WPA callback function to set the LGTK liveness from the LGTKL
 * @param ctx the WPA context
 * @param liveness the LGTKL liveness bitmask
 */
static void rsncb_set_lgtk_liveness(void* ctx, u8 liveness)
{
#if R_AUTH_NUM_LGTKS
    r_apl_global_t* nwk = (r_apl_global_t*)ctx;
    uint8_t oldGtksLive = nwk->auth.sup.gtks_live;
    LOG_ONLY_VAR(oldGtksLive);

    /* Set only LGTK liveness bits and leave the GTK liveness bits untouched */
    nwk->auth.sup.gtks_live &= ~R_AUTH_LGTK_LIVENESS_MASK;
    nwk->auth.sup.gtks_live |= ((liveness << R_AUTH_NUM_GTKS) & R_AUTH_LGTK_LIVENESS_MASK);
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_920((uint8_t)liveness, oldGtksLive, nwk->auth.sup.gtks_live, nwk->auth.gtks_valid);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    for (uint8_t i = 0; i < R_KEY_NUM; i++)
    {
        if (GTK_IS_VALID(i) && !(nwk->auth.sup.gtks_live & (1 << i)))
        {
            R_AUTH_DeleteGTK(nwk, i);
        }
    }
#else  /* if R_AUTH_NUM_LGTKS */
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
    r_loggen_930(liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
#endif /* R_AUTH_NUM_LGTKS */
}

/**
 * Free the RSN context
 * @param nwk The global NWK information structure
 */
static void rsn_free(r_apl_global_t* nwk)
{
    if (nwk && nwk->auth.sup.wpa_sm)
    {
        // wpa_sm_deinit also calls os_free(sm->ctx)
        wpa_sm_deinit(nwk->auth.sup.wpa_sm);
        nwk->auth.sup.wpa_sm = NULL;
    }
}

/**
 * Create the RSN context
 * @param nwk The global NWK information structure
 * @param dest The EAPOL target
 * @return NULL on error, the context on success
 */

static struct wpa_sm* rsn_create(r_apl_global_t* nwk, const uint8_t* dest)
{
    // wpa_sm_deinit also calls os_free(sm->ctx) -> sm_ctx must be os_alloc'ed
    struct wpa_sm_ctx* sm_ctx;
    struct wpa_sm* sm;

    if ((nwk == NULL) || (dest == NULL))
    {
        return NULL;
    }

    sm_ctx = os_malloc(sizeof(struct wpa_sm_ctx));
    if (!sm_ctx)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_969();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return NULL;
    }

    sm_ctx->ctx = nwk;
    sm_ctx->msg_ctx = nwk;
    sm_ctx->set_state = rsncb_set_state;
    sm_ctx->get_state = rsncb_get_state;
    sm_ctx->deauthenticate = rsncb_deauthenticate;
    sm_ctx->set_key = rsncb_set_key;
    sm_ctx->get_network_ctx = rsncb_get_network_ctx;
    sm_ctx->get_bssid = rsncb_get_bssid;
    sm_ctx->ether_send = rsncb_ether_send;
    sm_ctx->get_beacon_ie = rsncb_get_beacon_ie;
    sm_ctx->cancel_auth_timeout = rsncb_cancel_auth_timeout;
    sm_ctx->alloc_eapol = rsncb_alloc_eapol;
    sm_ctx->add_pmkid = rsncb_add_pmkid;
    sm_ctx->remove_pmkid = rsncb_remove_pmkid;
    sm_ctx->set_config_blob = rsncb_set_config_blob;
    sm_ctx->get_config_blob = rsncb_get_config_blob;
    sm_ctx->mlme_setprotection = rsncb_mlme_setprotection;
    sm_ctx->update_ft_ies = rsncb_update_ft_ies;
    sm_ctx->send_ft_action = rsncb_send_ft_action;
    sm_ctx->mark_authenticated = rsncb_mark_authenticated;
    sm_ctx->set_rekey_offload = rsncb_set_rekey_offload;
    sm_ctx->key_mgmt_set_pmk = rsncb_key_mgmt_set_pmk;
    sm_ctx->set_gtk_liveness = rsncb_set_gtk_liveness;
    sm_ctx->set_lgtk_liveness = rsncb_set_lgtk_liveness;

    sm = wpa_sm_init(sm_ctx);
    if (!sm)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1001();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        os_free(sm_ctx);
        return NULL;
    }

    uint8_t eui64[R_MAC_EXTENDED_ADDRESS_LENGTH];
    r_result_t requestResult = R_NWK_GetRequest(R_NWK_macExtendedAddress, &eui64, sizeof(eui64));
    if (requestResult != R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1010();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        wpa_sm_deinit(sm);
        return NULL;
    }
    R_Swap64(eui64, eui64);
    wpa_sm_set_own_addr(sm, eui64);
    memcpy(sm->bssid, dest, ETH_ALEN);

    {
        u8 wpa_ie[32];
        size_t wpa_ie_len = sizeof(wpa_ie);

        // from test (in wpa_parse_wpa_ie_rsn)
        wpa_sm_set_param(sm, WPA_PARAM_PROTO, WPA_PROTO_RSN);
        wpa_sm_set_param(sm, WPA_PARAM_PAIRWISE, WPA_CIPHER_CCMP);
        wpa_sm_set_param(sm, WPA_PARAM_GROUP, WPA_CIPHER_CCMP);
        wpa_sm_set_param(sm, WPA_PARAM_KEY_MGMT, WPA_KEY_MGMT_IEEE8021X);

        if (wpa_sm_set_assoc_wpa_ie_default(sm, wpa_ie, &wpa_ie_len) < 0)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_1030();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
            wpa_sm_deinit(sm);
            return NULL;
        }
    }

    wpa_sm_set_eapol(sm, (struct eapol_sm*)nwk);  // used as a context for some functions
    wpa_sm_set_pmk(sm, nwk->auth.sup.pmk, R_AUTH_PMK_SIZE, 0, 0);

    nwk->auth.sup.wpa_sm = sm;

    /*
     * Use the replay counter as a check to determine if we have already
     * had a context that needs to be restored. This is necessary since
     * we no longer keep the RSN context around after we are done with
     * the 4WH or each GKH.
     */
    if (!MEMISZERO(nwk->auth.sup.rsn_replay_counter, R_AUTH_REPLAY_COUNTER_LEN))
    {
        rsn_restore_state(nwk);
    }

    return sm;
}

/**
 * Process an incoming RSN message
 * @param nwk The global NWK information structure
 * @param data the incoming message
 * @param size the size of the message
 * @return R_RESULT_SUCCESS if the message has been successfully processed
 */
static r_result_t rsn_process_message(r_apl_global_t* nwk, const void* data, uint16_t size)
{
    int res;
    if (!nwk->auth.sup.wpa_sm)
    {
        if (!nwk->auth.sup.pmk_valid)
        {
            /* Finish ongoing EAP-TLS handshake (assuming that the final SUCCESS message from authenticator was lost) */
            if (tls_finish_handshake(nwk) != R_RESULT_SUCCESS)
            {
                return R_RESULT_FAILED;
            }
        }
        // rsn_create takes care of restoring the previous state of the rsn context if needed
        nwk->auth.sup.wpa_sm = rsn_create(nwk, nwk->auth.sup.authenticator);
        if (!nwk->auth.sup.wpa_sm)
        {
            return R_RESULT_FAILED;
        }
    }

    // * Returns: 1 = WPA EAPOL-Key processed, 0 = not a WPA EAPOL-Key, -1 failure
    res = wpa_sm_rx_eapol(nwk->auth.sup.wpa_sm, nwk->auth.sup.authenticator, data, size);
    if (res <= 0)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1087();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return R_RESULT_FAILED;
    }

    // If we are in the process of performing a 4WH exchange, ptk_valid is 0 and we do not need
    // to save the state or free the RSN context. In other words, save the state and free the
    // context only after finishing the 4WH or the GKH
    if (nwk->auth.sup.ptk_valid)
    {
        rsn_save_state(nwk);
        rsn_free(nwk);
    }

    return R_RESULT_SUCCESS;
}

/**
 * Save the current state of the RSN context in the supplicant state
 * @param nwk the central network structure
 * @return nothing
 */
static void rsn_save_state(r_apl_global_t* nwk)
{
    struct wpa_sm* rsn_wpa_sm = nwk->auth.sup.wpa_sm;

    if (rsn_wpa_sm)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_1114(rsn_wpa_sm->rx_replay_counter[R_AUTH_REPLAY_COUNTER_LEN - 1]);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        memcpy(&nwk->auth.sup.rsn_replay_counter, rsn_wpa_sm->rx_replay_counter, R_AUTH_REPLAY_COUNTER_LEN);

        memcpy(&nwk->auth.sup.ptk[0], rsn_wpa_sm->ptk.kck, R_AUTH_KCK_SIZE);
        memcpy(&nwk->auth.sup.ptk[R_AUTH_KCK_SIZE], rsn_wpa_sm->ptk.kek, R_AUTH_KEK_SIZE);
        nwk->auth.sup.ptk_valid = 1;
    }
}

/**
 * Restore the current supplicant state into the RSN context. Do nothing if it does not exist.
 * @param nwk the central network structure
 * @return nothing
 */
static void rsn_restore_state(r_apl_global_t* nwk)
{
    if (!(nwk && nwk->auth.sup.wpa_sm))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN
        r_loggen_1132();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_WARN */
        return;
    }

    struct wpa_sm* rsn_wpa_sm = nwk->auth.sup.wpa_sm;

    uint8_t eui64[R_MAC_EXTENDED_ADDRESS_LENGTH];
    r_result_t requestResult = R_NWK_GetRequest(R_NWK_macExtendedAddress, &eui64, sizeof(eui64));
    if (requestResult != R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1142();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return;
    }
    R_Swap64(eui64, eui64);
    wpa_sm_set_own_addr(rsn_wpa_sm, eui64);
    memcpy(rsn_wpa_sm->bssid, nwk->auth.sup.authenticator, ETH_ALEN);

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1149(eui64);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    memcpy(rsn_wpa_sm->pmk, nwk->auth.sup.pmk, R_AUTH_PMK_SIZE);
    rsn_wpa_sm->pmk_len = R_AUTH_PMK_SIZE;

    memcpy(rsn_wpa_sm->ptk.kck, &nwk->auth.sup.ptk[0], R_AUTH_KCK_SIZE);
    rsn_wpa_sm->ptk.kck_len = R_AUTH_KCK_SIZE;

    /* The KEK starts right after the KCK in the stored PTK */
    memcpy(rsn_wpa_sm->ptk.kek, &nwk->auth.sup.ptk[R_AUTH_KCK_SIZE], R_AUTH_KEK_SIZE);
    rsn_wpa_sm->ptk.kek_len = R_AUTH_KEK_SIZE;
    rsn_wpa_sm->ptk_set = 1;

    // Make sure we restore the values of the two variables if we are restoring the state
    // to run a GKH. Otherwise, WPA thinks the 4WH did not run properly and will not send
    // the appropriate message
    if (nwk->auth.sup.ptk_valid)
    {
        rsn_wpa_sm->msg_3_of_4_ok = 1;
        rsn_wpa_sm->ptk.installed = 1;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1170(eui64, nwk->auth.sup.rsn_replay_counter[R_AUTH_REPLAY_COUNTER_LEN - 1]);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    memcpy(rsn_wpa_sm->rx_replay_counter, nwk->auth.sup.rsn_replay_counter, R_AUTH_REPLAY_COUNTER_LEN);
    rsn_wpa_sm->rx_replay_counter_set = 1;
}

/******************************************************************************
* Public Functions
******************************************************************************/

r_result_t R_AUTH_SUP_Init(r_apl_global_t* nwk, const uint8_t* encoded_certs, uint8_t cert_index, size_t mac_offset)
{
    if (nwk == NULL)
    {
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1186();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    eap_free(nwk);
    rsn_free(nwk);

    MEMZERO_S(&nwk->auth.sup);
    nwk->auth.gtks_valid = 0;
    r_result_t res = R_AUTH_SetAuthStateCache(nwk);

    R_AUTH_InternalReset();

    nwk->auth.sup.encoded_certs = encoded_certs;
    nwk->auth.sup.cert_index = cert_index;
    nwk->auth.sup.mac_offset = mac_offset;

    return res;
}

r_result_t R_AUTH_SUP_Reset(r_apl_global_t* nwk)
{
    if (nwk == NULL)
    {
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1210();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    const uint8_t* encoded_certs = nwk->auth.sup.encoded_certs;
    const uint8_t cert_index = nwk->auth.sup.cert_index;
    const size_t mac_eapol_offset = nwk->auth.sup.mac_offset;

    return R_AUTH_SUP_Init(nwk, encoded_certs, cert_index, mac_eapol_offset);
}

void R_AUTH_SUP_ClearGtkValidity(r_apl_global_t* nwk)
{
    if (nwk == NULL)
    {
        return;
    }

    nwk->auth.gtks_valid = 0;
}

r_result_t R_AUTH_SUP_StartJoin(r_apl_global_t* nwk, const uint8_t* target)
{
    if (nwk == NULL)
    {
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

    uint8_t newTarget[8] = { 0 };
    if (target != NULL && MEMISNOTZERO(target, R_MAC_EXTENDED_ADDRESS_LENGTH))
    {
        MEMCPY_A(newTarget, target);
    }
    else
    {
        MEMCPY_A(newTarget, nwk->auth.sup.target);
    }

    if (MEMISZERO_A(newTarget))
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1247();
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

    // stop potentially running attempts
    eap_free(nwk);  // stop potentially running EAP-TLS attempt
    MEMCPY_A(nwk->auth.sup.target, newTarget);

    // if we have a pmk already, assume authenticator is already set
    if (!nwk->auth.sup.pmk_valid)
    {
        MEMCPY_A(nwk->auth.sup.authenticator, nwk->auth.sup.target);
    }

    struct wisun_first_message_s msg = { 0 };  // Set all fields to 0
    msg.header.type = 2;
    uint16_t key_info = 2 | WPA_KEY_INFO_REQUEST;
    WPA_PUT_BE16(msg.header.key_info, key_info);
    WPA_PUT_BE64(msg.header.replay_counter, nwk->auth.sup.replay_counter++);
    // Key Nonce = 0
    // Key IV = 0
    // Key RSC = 0
    // Key ID (Reserved in IEEE 802.11i/RSN) = 0
    // Key MIC = 0

    uint8_t eui64[R_MAC_EXTENDED_ADDRESS_LENGTH];
    r_result_t requestResult = R_NWK_GetRequest(R_NWK_macExtendedAddress, &eui64, sizeof(eui64));
    if (requestResult != R_RESULT_SUCCESS)
    {
        return R_RESULT_FAILED;
    }
    R_Swap64(eui64, eui64);

    r_boolean_t havePmk = nwk->auth.sup.pmk_valid;
    r_boolean_t havePtk = nwk->auth.sup.ptk_valid;
    if (!havePmk || !havePtk)
    {
        rsn_free(nwk);  // stop potentially running 4WH attempt
    }

    uint16_t totalKdeLength = 0;
    if (havePmk)
    {
        struct wisun_pxkid_s* pmkid = (struct wisun_pxkid_s*)&msg.data.raw.bytes[totalKdeLength];
        totalKdeLength += create_kde_header(&pmkid->header, OUI_IEEE_802_11, KDE_DATA_TYPE_PMKID, sizeof(pmkid->id));
        R_AUTH_InternalCalcKdeId(pmkid->id, nwk->auth.sup.pmk, 0, nwk->auth.sup.authenticator, eui64);
    }

    if (havePtk)
    {
        struct wisun_pxkid_s* ptkid = (struct wisun_pxkid_s*)&msg.data.raw.bytes[totalKdeLength];
        totalKdeLength += create_kde_header(&ptkid->header, OUI_WISUN, KDT_PTKID, sizeof(ptkid->id));
        R_AUTH_InternalCalcKdeId(ptkid->id, nwk->auth.sup.ptk, 1, nwk->auth.sup.authenticator, eui64);
    }

#if R_AUTH_NUM_GTKS
    struct wisun_gtkl_s* gtkl = (struct wisun_gtkl_s*)&msg.data.raw.bytes[totalKdeLength];
    totalKdeLength += create_kde_header(&gtkl->header, OUI_WISUN, KDT_GTKL, sizeof(gtkl->gtk_liveness));
    gtkl->gtk_liveness = nwk->auth.gtks_valid & R_AUTH_GTK_LIVENESS_MASK;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1306(gtkl->gtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif

#if (R_LEAF_NODE_ENABLED || R_LFN_PARENTING_ENABLED)
    struct wisun_nr_kde_s* nr = (struct wisun_nr_kde_s*)&msg.data.raw.bytes[totalKdeLength];
    totalKdeLength += create_kde_header(&nr->header, OUI_WISUN, KDT_NR, sizeof(nr->node_role));
#if R_LEAF_NODE_ENABLED
    nr->node_role = R_AUTH_NODE_ROLE_ID_LFN;
#else
    nr->node_role = R_AUTH_NODE_ROLE_ID_FFN_ROUTER;
#endif
#endif

#if (R_LEAF_NODE_ENABLED || R_LFN_PARENTING_ENABLED)
    struct wisun_lgtkl_s* lgtkl = (struct wisun_lgtkl_s*)&msg.data.raw.bytes[totalKdeLength];
    totalKdeLength += create_kde_header(&lgtkl->header, OUI_WISUN, KDT_LGTKL, sizeof(lgtkl->lgtk_liveness));
    lgtkl->lgtk_liveness = (nwk->auth.gtks_valid & R_AUTH_LGTK_LIVENESS_MASK) >> R_AUTH_NUM_GTKS;
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1323(lgtkl->lgtk_liveness);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
#endif

    UInt16ToArr(totalKdeLength, msg.header_extra.key_data_length);
    msg.eapol_header.version = 3;  // spec v17
    msg.eapol_header.type = IEEE802_1X_TYPE_EAPOL_KEY;
    uint16_t eapol_len = sizeof(msg.header) + sizeof(msg.header_extra) + totalKdeLength;
    WPA_PUT_BE16(msg.eapol_header.length, eapol_len);

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1332(havePmk, havePtk, nwk->auth.gtks_valid, nwk->auth.sup.gtks_live);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    return eapol_send(nwk, nwk->auth.sup.target, R_EAPOL_TYPE_8021X, (uint8_t*)&msg, sizeof(struct eapol_header_s) + eapol_len);
}

r_result_t R_AUTH_SUP_ProcessEapolMessage(r_apl_global_t* nwk, r_mac_eapol_type_t type, const uint8_t* source,
                                          const uint8_t* authenticator, uint8_t* data, uint16_t size)
{
    if (!(nwk && source && data))
    {
        os_free(data);
        return R_RESULT_ILLEGAL_NULL_POINTER;
    }

    if (size < EAPOL_MESSAGE_MINSIZE)
    {
        os_free(data);
        return R_RESULT_INVALID_PARAMETER;
    }

    r_result_t res = R_RESULT_FAILED;
    if (isToBR(data))
    {
        r_ipv6addr_t brAddress;
        r_result_t requestResult = R_NWK_GetRequest(R_NWK_borderRouterAddress, &brAddress, sizeof(brAddress));
        if (requestResult != R_RESULT_SUCCESS)
        {
            os_free(data);
            return R_RESULT_FAILED;
        }
        eapol_relay_t* relay = os_malloc(sizeof(eapol_relay_t) + size);
        if (!relay)
        {
            os_free(data);
            return R_RESULT_INSUFFICIENT_BUFFER;
        }
        memcpy(relay->sup, source, sizeof(relay->sup));
        relay->kmp_id = (uint8_t)type;
        // QAC cannot deal well with flexible arrays
        // PRQA S 0432 3
        // PRQA S 0541 2
        memcpy(relay->msg, data, size);
        res = R_UDP_DataRequest(brAddress.bytes, R_AUTH_EAPOL_RELAY_PORT, R_AUTH_EAPOL_RELAY_PORT,
                                (const uint8_t*)relay, sizeof(eapol_relay_t) + size, 0);
        if (res != R_RESULT_SUCCESS)
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
            r_loggen_1378((uint8_t)res, size, source);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
        }
        else
        {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
            r_loggen_1382(size, source);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
        }

        os_free(relay);
        os_free(data);
        return res;
    }

    /* It must be an Authentication message from the BR -> reset PAN TimeOut timer */
    R_NWK_ResetPanTimeoutRequest();

#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
    r_loggen_1393(MSGTYPE(type), size, source);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */

    /* Increase own task priority, give maximum priority to EAP processing */
    R_OS_ChangeOwnTaskPriority(HIGHEST_TSK_PRIORITY);
    if (type == R_EAPOL_TYPE_8021X)
    {
        res = eap_process_message(nwk, source, authenticator, data, size);  // data is freed inside
        data = NULL;
    }
    if (type == R_EAPOL_TYPE_4WH || type == R_EAPOL_TYPE_GKH)
    {
        if (type == R_EAPOL_TYPE_4WH)
        {
            nwk->auth.sup.ptk_valid = 0;
        }
        res = rsn_process_message(nwk, data, size);
    }

    /* Restore own task priority */
    R_OS_ChangeOwnTaskPriority(APL_TSK_DEFAULT_PRIORITY);

    os_free(data);
    return res;
}

r_result_t R_AUTH_SUP_ProcessRelayMessage(r_apl_global_t* nwk, const uint8_t* source, const uint8_t* dest, const uint8_t* data, uint16_t size)
{
    if (!(nwk && source && dest && data && size >= sizeof(eapol_relay_t) + EAPOL_MESSAGE_MINSIZE))
    {
        return R_RESULT_INVALID_PARAMETER;
    }
    const eapol_relay_t* relay = (const eapol_relay_t*)data;
    // PRQA S 0481 2
    struct eapol_message_s* msg = (struct eapol_message_s*)relay->msg;
    const uint8_t* authenticator = NULL;
    if (msg->eapol.type == IEEE802_1X_TYPE_EAP_PACKET
        && msg->d.eap.hdr.code == EAP_REQUEST
        && size - sizeof(eapol_relay_t) >= R_AUTH_REQUEST_IDENTITY_LENGTH
        && msg->d.eap.type == R_EAP_TYPE_IDENTITY
        )
    {
        // insert authenticator EA-IE in Request Identity
        authenticator = nwk->auth.sup.authenticator;
    }

    // QAC cannot deal well with flexible arrays
    // PRQA S 0432 4
    // PRQA S 0541 3
    r_result_t res = R_NWK_EapolDataRequest(R_AUTH_DetectEapolType(relay->msg), relay->sup, authenticator, relay->msg,
                                            size - sizeof(eapol_relay_t));

    if (res != R_RESULT_SUCCESS)
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR
        r_loggen_1446((uint8_t)res, size, relay->sup);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_ERR */
    }
    else
    {
#if R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG
        r_loggen_1450(size, relay->sup);
#endif /* R_LOG_THRESHOLD >= R_LOG_SEVERITY_DBG */
    }
    return res;
}


/******************************************************************************
* Replaced global functions
******************************************************************************/

// unused
void rsn_preauth_candidate_process(struct wpa_sm* sm)
{
    wpa_printf(MSG_UNUSED_CB, "rsn_preauth_candidate_process");
    unused_marker("rsn_preauth_candidate_process");
}
void rsn_preauth_deinit(struct wpa_sm* sm)
{
    wpa_printf(MSG_UNUSED_CB, "rsn_preauth_deinit");
    unused_marker("rsn_preauth_deinit");
}

/**
 * eapol_sm_get_key - Get master session key (MSK) from EAP
 * @param sm Pointer to EAPOL state machine allocated with eapol_sm_init()
 * @param key Pointer for key buffer
 * @param len Number of bytes to copy to key
 * @return 0 on success (len of key available), maximum available key len
 * (>0) if key is available but it is shorter than len, or -1 on failure.
 *
 * @details Fetch EAP keying material (MSK, eapKeyData) from EAP state machine.
 * The key is available only after a successful authentication.
 */
int eapol_sm_get_key(struct eapol_sm* sm, u8* key, size_t len)
{
    wpa_printf(MSG_UNUSED_CB, "eapol_sm_get_key: key: %02x%02x..., len: %zu", key[0], key[1], len);
    if (sm == NULL || key == NULL || !((r_apl_global_t*)sm)->auth.sup.pmk_valid)
    {
        return -1;
    }
    if (len > 64)
    {
        return 64;
    }
    memcpy(key, ((r_apl_global_t*)sm)->auth.sup.pmk, (uint32_t)len);
    return 0;
}

// unused
void eapol_sm_notify_cached(struct eapol_sm* sm)
{
    wpa_printf(MSG_UNUSED_CB, "eapol_sm_notify_cached");
    unused_marker("eapol_sm_notify_cached");
}
void eapol_sm_notify_eap_success(struct eapol_sm* sm, Boolean success)
{
    wpa_printf(MSG_UNUSED_CB, "eapol_sm_notify_eap_success: success: %d", success);
    unused_marker("eapol_sm_notify_eap_success");
}
void eapol_sm_notify_lower_layer_success(struct eapol_sm* sm, int in_eapol_sm)
{
    wpa_printf(MSG_UNUSED_CB, "eapol_sm_notify_lower_layer_success: in_eapol_sm: %d", in_eapol_sm);
    unused_marker("eapol_sm_notify_lower_layer_success");
}
void eapol_sm_notify_portValid(struct eapol_sm* sm, Boolean valid)
{
    wpa_printf(MSG_UNUSED_CB, "eapol_sm_notify_portValid: valid: %d", valid);
    unused_marker("eapol_sm_notify_portValid");
}
void eapol_sm_notify_tx_eapol_key(struct eapol_sm* sm)
{
    wpa_printf(MSG_UNUSED_CB, "eapol_sm_notify_tx_eapol_key");
    unused_marker("eapol_sm_notify_tx_eapol_key");
}
void eapol_sm_register_scard_ctx(struct eapol_sm* sm, void* ctx)
{
    wpa_printf(MSG_UNUSED_CB, "eapol_sm_register_scard_ctx");
    unused_marker("eapol_sm_register_scard_ctx");
}
void eapol_sm_request_reauth(struct eapol_sm* sm)
{
    wpa_printf(MSG_UNUSED_CB, "eapol_sm_request_reauth");
    unused_marker("eapol_sm_request_reauth");
}
