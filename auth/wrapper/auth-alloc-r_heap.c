/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file
   \version   1.00
   \brief     Dynamic memory wrapper for WPA/mbedTLS using r_heap
 */

#include <string.h>
#include "r_os_wrapper_config.h"
#define MY_HEAP_ID R_HEAP_ID_AUTH
#include "r_heap.h"

// memory for wpa
void* os_malloc(size_t size)
{
    return my_alloc(size);
}
void* os_realloc(void* ptr, size_t size)
{
    return my_realloc(ptr, size);
}
void os_free(void* ptr)
{
    my_free(ptr);
}
void* os_zalloc(size_t size)
{
    return my_zalloc(size);
}

// memory for mbedTLS
void authwrap_free(void* ptr)
{
    my_free(ptr);
}
void* authwrap_calloc(size_t nmemb, size_t size)
{
    return my_zalloc(nmemb * size);
}

void mbedtls_platform_zeroize(void* buf, size_t len)
{
    memset(buf, 0, len);
}
