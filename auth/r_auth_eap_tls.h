/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   @file      r_auth_eap_tls.h
   @version   1.00
   @brief     Header file for the EAP-TLS implementation
 */

#ifndef R_AUTH_EAP_TLS_H
#define R_AUTH_EAP_TLS_H

#include "r_header_utils.h"

#define EAP_TLS_FLAG_START           0x20
#define EAP_TLS_FLAG_MORE_FRAGMENTS  0x40
#define EAP_TLS_FLAG_LENGTH_INCLUDED 0x80

#define EAPOL_MIN_LENGTH             8 //!< The minimal length of an EAPOL packet in bytes (EAP SUCCESS or FAILURE)

R_HEADER_UTILS_PRAGMA_PACK_1

/** EAP types used in Request/Response exchanges */
typedef enum r_auth_eap_type_e
{
    R_EAP_TYPE_IDENTITY = 1,
    R_EAP_TYPE_EAP_TLS  = 13,
} r_auth_eap_type_t;

/** EAP codes to identify the type of an EAP packet */
typedef enum r_auth_eap_code_e
{
    EAP_REQUEST      = 1,
    EAP_RESPONSE     = 2,
    EAP_SUCCESS      = 3,
    EAP_FAILURE      = 4,
    EAP_INVALID_CODE = 5
} r_auth_eap_code_t;

/** Possible states during the EAP-TLS handshake */
typedef enum r_auth_eap_state_e
{
    EAP_STATE_WAIT_FOR_IDENTITY_REQUEST = 0,
    EAP_STATE_WAIT_FOR_EAP_TLS_START,
    EAP_STATE_TLS_HANDSHAKE_ONGOING,
    EAP_STATE_WAIT_FOR_SUCCESS,
} r_auth_eap_state;

typedef struct r_auth_eapol_header_s
{
    uint8_t version;
    uint8_t type;
    uint8_t length[2];  // big-endian
} r_auth_eapol_header_t;

typedef struct r_auth_eap_header_s
{
    uint8_t code;
    uint8_t id;
    uint8_t length[2];  // big-endian
    uint8_t type;
} r_auth_eap_header_t;

typedef struct r_auth_eap_response_identity_s
{
    r_auth_eap_header_t eap_header;
    char                identity[1];
} r_auth_eap_response_identity_t;

typedef struct r_auth_eap_tls_header_s
{
    uint8_t eap_tls_flags;
    // We do not support outgoing fragmentation so we omit the (optional) 4 byte length field here
} r_auth_eap_tls_header_t;

typedef struct r_auth_eap_tls_packet_s
{
    r_auth_eapol_header_t   eapol;
    r_auth_eap_header_t     eap;
    r_auth_eap_tls_header_t eap_tls;
    uint8_t                 tls[]; //!< The TLS payload
} r_auth_eap_tls_packet_t;

/**
 * An MPX fragment containing an EAP-TLS message encapsulated in EAPOL. This struct can be processed by the MAC layer
 * without further duplication and is used for memory-optimized transmission
 */
typedef struct r_auth_eap_tls_mpx_fragment_s
{
    uint8_t                 kmp_id;
    r_auth_eap_tls_packet_t payload;
} r_auth_eap_tls_mpx_fragment_t;

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

#endif /* R_AUTH_EAP_TLS_H */
