/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2019 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   @file      r_auth_sup.h
   @version   1.00
   @brief     Wi-SUN FAN Supplicant API
 */

#ifndef R_AUTH_SUP_H
#define R_AUTH_SUP_H

#include "r_apl_global.h"
#include "r_nwk_api_base.h"
#include "r_nwk_api.h"

/**
 * Initialize the supplicant management structures
 * @details Must be called before any other R_AUTH_SUP_* function
 * @param nwk The global network information structure
 * @param encoded_certs The certificates as encoded by the encode-certs tool
 * @param cert_index The index of the private key and certificate chain to use
 * @param mac_offset The number of bytes that must be prepended to any buffer that is passed to the MAC layer
 * @return R_RESULT_SUCCESS on success. Appropriate error code otherwise.
 */
r_result_t R_AUTH_SUP_Init(r_apl_global_t* nwk, const uint8_t* encoded_certs, uint8_t cert_index, size_t mac_offset);

/**
 * Reset the supplicant management structures
 * @details Persistent attributes that were set by R_AUTH_SUP_Init are retained
 * @param nwk The global network information structure
 */
r_result_t R_AUTH_SUP_Reset(r_apl_global_t* nwk);

/**
 * Start/continue the authentication process by sending the Wi-SUN initial EAPOL-Key packet
 * @param nwk The global network information structure
 * @param target The EUI-64 address of the target chosen for the authentication or NULL to restart authentication with
 * the previous target.
 * @return R_RESULT_SUCCESS on success. Appropriate error code otherwise.
 */
r_result_t R_AUTH_SUP_StartJoin(r_apl_global_t* nwk, const uint8_t* target);

/**
 * Consume and process an EAPOL Message.
 * @param nwk The global network information structure
 * @param type The KMP ID of the message
 * @param source The EUI-64 source address of the message
 * @param authenticator The EUI-64 of the authenticator if the EAPOL-Authenticator-EUI-64 IE is present (may be NULL)
 * @param data The message. This MUST be a pointer to dynamically allocated memory on the AUTH heap. The buffer is
 * reduced iteratively during processing and finally freed so the pointer MUST NOT be used after calling this function
 * @param size The size of the message
 * @return R_RESULT_SUCCESS on success. Appropriate error code otherwise.
 */
r_result_t R_AUTH_SUP_ProcessEapolMessage(r_apl_global_t* nwk, r_mac_eapol_type_t type, const uint8_t* source,
                                          const uint8_t* authenticator, uint8_t* data, uint16_t size);

/**
 * Process an EAPOL Relay Message
 * @param nwk The global network information structure
 * @param source The IPv6 source address of the received message
 * @param dest The IPv6 destination address of the received message
 * @param data The message
 * @param size The size of the message
 * @return R_RESULT_SUCCESS on success. Appropriate error code otherwise.
 */
r_result_t R_AUTH_SUP_ProcessRelayMessage(r_apl_global_t* nwk, const uint8_t* source, const uint8_t* dest,
                                          const uint8_t* data, uint16_t size);


/**
 * Callback to indicate that an EAP-TLS exchange is starting
 * @details this can be used to free non-critical memory before EAP-TLS begins
 * @param nwk The global network information structure
 */
void R_AUTH_SUP_EapTlsStartIndication();

/**
 * Callback to indicate that an EAP-TLS exchange has been finished
 * @details this can be used to re-allocate non-critical memory after EAP-TLS ended
 * @param nwk The global network information structure
 */
void R_AUTH_SUP_EapTlsFinishedIndication();

/**
 * Clear the supplicant GTK validity flags
 * @details The GTK validity flag indicates which GTKs can still be used. This function clears them all
 * @param nwk The global network information structure
 */
void R_AUTH_SUP_ClearGtkValidity(r_apl_global_t* nwk);

// memory for auth
void* R_AUTH_malloc(size_t size);
void* R_AUTH_realloc(void* ptr, size_t size);
void  R_AUTH_free(void* ptr);
void* R_AUTH_zalloc(size_t size);

#endif /* R_AUTH_SUP_H */
