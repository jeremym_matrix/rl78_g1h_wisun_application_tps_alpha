/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      r_auth_common.h
   \version   1.00
   \brief     Wi-SUN FAN Authentication common API
 */

#ifndef R_AUTH_COMMON_H
#define R_AUTH_COMMON_H

#include "r_apl_global.h"
#include "r_nwk_api_base.h"
#include "r_nwk_api.h"

/**
 * Set a GTK to the specified value (for BR, the GTK is otherwise randomized)
 * @details The specified GTK is also used to configure the MAC layer.
 * @param nwk The global NWK information structure
 * @param igtk The GTK index (from 0 to R_AUTH_NUM_GTKS-1) or LGTK index (from R_AUTH_NUM_GTKS to R_KEY_NUM-1)
 * @param gtk The GTK, if NULL, generate a new random GTK
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_SetGTK(r_apl_global_t* nwk, uint8_t igtk, const uint8_t gtk[16]);

/**
 * Delete the specified GTKs
 * @details The specified GTK is also removed from the MAC layer. The internal GTK-HASH IE is zeroed.
 * For the BR, this changes also the GTK-HASH IE that is sent in the PAN Configs.
 * @param nwk The global NWK information structure
 * @param mask A bit mask specifying the GTKs that should be deleted (bit 0 represents index 0; bit 3 is index 3)
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_DeleteGTKs(r_apl_global_t* nwk, uint8_t mask);

/**
 * Delete the specified GTK
 * @details The specified GTK is also removed from the MAC layer. The internal GTK-HASH IE is zeroed.
 * For the BR, this changes also the GTK-HASH IE that is sent in the PAN Configs.
 * @param nwk The global NWK information structure
 * @param igtk The GTK index (0-3)
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_DeleteGTK(r_apl_global_t* nwk, uint8_t igtk);

/**
 * Derive the KMP ID for the provided eapol_message
 * @param eapol_message The EAPOL message
 * @return The KMP ID
 */
r_mac_eapol_type_t R_AUTH_DetectEapolType(const void* eapol_message);

/**
 * Add an EAPOL header if necessary to create a complete EAPOL message and derive the KMP ID
 * @details The maximum overhead for the out message is R_AUTH_CREATEMESSAGE_MAX_OVERHEAD
 * @param intype The input type
 * @param data The existing message
 * @param datasize The size of the existing message
 * @param buf The output buffer
 * @param[in,out] bufsize The capacity (in) and used size (out) of the output buffer
 * @param[out] outtype The derived KMP ID
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_CreateMessage(r_mac_eapol_type_t intype, const uint8_t* data, uint16_t datasize, uint8_t* buf, uint16_t* bufsize, r_mac_eapol_type_t* outtype);

// Size of the EAPOL header, which might need to be prepended
#define R_AUTH_CREATEMESSAGE_MAX_OVERHEAD ((uint16_t)4)

#define R_AUTH_REQUEST_IDENTITY_LENGTH    9

#endif /* R_AUTH_COMMON_H */
