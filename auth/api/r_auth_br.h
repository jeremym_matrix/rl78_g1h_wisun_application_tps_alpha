/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      r_auth_br.h
   \version   1.00
   \brief     Wi-SUN FAN Authenticator API
 */

#ifndef R_AUTH_BR_H
#define R_AUTH_BR_H

#include "r_apl_global.h"
#include "r_nwk_api_base.h"
#include "r_nwk_api.h"

#if R_BR_AUTHENTICATOR_ENABLED

/**
 * Reset or initialize the border router/authenticator management structures
 * @details Must be called before R_AUTH_BR_Start
 * @param nwk The global network information structure
 */
void R_AUTH_BR_Reset(r_apl_global_t* nwk);

/**
 * Start the authenticator
 * @details Must be called before any other R_AUTH_BR_* function except R_AUTH_BR_Start
 * @param nwk The global network information structure
 */
void R_AUTH_BR_Start(r_apl_global_t* nwk);

/**
 * Perform periodic updates of GTKs
 * @details Should be called at least every minute
 * @param nwk The global network information structure
 * @return TRUE if the PAN Version must be increased
 */
r_boolean_t R_AUTH_BR_PeriodicUpdate(r_apl_global_t* nwk);

r_boolean_t R_AUTH_BR_PeriodicUpdateGtks(r_apl_global_t* nwk);

r_boolean_t R_AUTH_BR_PeriodicUpdateLgtks(r_apl_global_t* nwk);

/**
 * Revoke exiting GTKs and install the provided new GTK as described in [FANTPS-1v32-6.5.2.5 Revocation of Node Access]
 * @details The PAN version must be increased by the caller afterwards to finish the GTK revocation
 * @param nwk The global network information structure
 * @param new_gtk The new GTK that should be installed
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_BR_RevokeGTKs(r_apl_global_t* nwk, const uint8_t* new_gtk);

/**
 * Revoke existing supplicant by clearing the PMK, PTK and GTKs and resetting their lifetimes
 * @details The PAN version must be increased by the caller afterwards to finish the supplicant revocation
 * @param nwk The global network information structure
 * @param supplicant The MAC Address of the supplicant to be revoked
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_BR_RevokeSupplicant(r_apl_global_t* nwk, const uint8_t* supplicant);

/**
 * Continue the currently running authentication process
 * @param nwk The global network information structure
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_BR_ContinueAuthentication(r_apl_global_t* nwk);

/**
 * Process the PMK of a successful EAP-TLS handshake and start the RSN handshake
 * @details This function should be called by the EAP-TLS server when an EAP-TLS handshake succeeded
 * @param pmk The PMK (size is R_AUTH_PMK_SIZE)
 * @param nwk The global network information structure
 * @return R_RESULT_SUCCESS on success
 */
void R_AUTH_BR_EapTlsDoneCallback(const uint8_t* eui64, const uint8_t* pmk, r_apl_global_t* nwk);

/**
 * Indicate a failure in the EAP-TLS exchange in order to clean up the BR
 * @details This function should be called by the EAP-TLS server when an EAP-TLS handshake failed
 * @param nwk The global network information structure
 * @return R_RESULT_SUCCESS on success
 */
void R_AUTH_BR_EapTlsFailureCallback(const uint8_t* eui64, r_apl_global_t* nwk);

/**
 * Start a 4-Way-Handshake for the currently selected supplicant
 * @param nwk The global network information structure
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_BR_Start4WH(r_apl_global_t* nwk);

/**
 * Start a Group Key Handshake for the currently selected supplicant
 * @param nwk The global network information structure
 * @param igtk The GTK index (0-3)
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_BR_StartGKH(r_apl_global_t* nwk, uint8_t igtk);

/**
 * Process an EAPOL Message
 * @param nwk The global network information structure
 * @param type The KMP ID
 * @param source The EUI-64 source address of the received message
 * @param data The message
 * @param size The size of the message
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_BR_ProcessEapolMessage(r_apl_global_t* nwk, r_mac_eapol_type_t type, const uint8_t* source,
                                         const void* data, uint16_t size);

/**
 * Process an EAPOL Direct Message
 * @param nwk The global network information structure
 * @param type The KMP ID
 * @param source The EUI-64 source address of the received message
 * @param data The message
 * @param size The size of the message
 * @return R_RESULT_SUCCESS on success
 */
r_result_t R_AUTH_BR_ProcessDirectMessage(r_apl_global_t* nwk, r_mac_eapol_type_t type, const uint8_t* source,
                                          const void* data, uint16_t size);

/**
 * Process an EAPOL Relay Message
 * @param nwk The global network information structure
 * @param source The IPv6 source address of the received message
 * @param dest The IPv6 destination address of the received message
 * @param data The message
 * @param size The size of the message
 * @return R_RESULT_SUCCESS on success
*/
r_result_t R_AUTH_BR_ProcessRelayMessage(r_apl_global_t* nwk, const uint8_t* source, const uint8_t* dest,
                                         const uint8_t* data, uint16_t size);

/**
 * Send an EAPOL message (if necessary via relay)
 * @param nwk The global network information structure
 * @param type The EAPOL message type
 * @param data The payload (depending on the EAPOL message type)
 * @param size The payload size in bytes
 * @return 0 on success, -1 on failure
 */
int R_AUTH_BR_SendEapol(r_apl_global_t* nwk, r_mac_eapol_type_t type, const uint8_t* data, uint16_t size);

#endif /* R_BR_AUTHENTICATOR_ENABLED */

#endif /* R_AUTH_BR_H */
