/******************************************************************************
* DISCLAIMER
* This software is supplied by Renesas Electronics Corporation and is only
* intended for use with Renesas products. No other uses are authorized. This
* software is owned by Renesas Electronics Corporation and is protected under
* all applicable laws, including copyright laws.
* THIS SOFTWARE IS PROVIDED "AS IS" AND RENESAS MAKES NO WARRANTIES REGARDING
* THIS SOFTWARE, WHETHER EXPRESS, IMPLIED OR STATUTORY, INCLUDING BUT NOT
* LIMITED TO WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE
* AND NON-INFRINGEMENT.  ALL SUCH WARRANTIES ARE EXPRESSLY DISCLAIMED.
* TO THE MAXIMUM EXTENT PERMITTED NOT PROHIBITED BY LAW, NEITHER RENESAS
* ELECTRONICS CORPORATION NOR ANY OF ITS AFFILIATED COMPANIES SHALL BE LIABLE
* FOR ANY DIRECT, INDIRECT, SPECIAL, INCIDENTAL OR CONSEQUENTIAL DAMAGES FOR
* ANY REASON RELATED TO THIS SOFTWARE, EVEN IF RENESAS OR ITS AFFILIATES HAVE
* BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGES.
* Renesas reserves the right, without notice, to make changes to this software
* and to discontinue the availability of this software. By using this software,
* you agree to the additional terms and conditions found by accessing the
* following link:
* http://www.renesas.com/disclaimer
*
* Copyright (C) 2017 Renesas Electronics Corporation. All rights reserved.
******************************************************************************/

/*!
   \file      r_auth_types.h
   \version   1.00
   \brief     Wi-SUN FAN Authentication types
 */

#ifndef R_AUTH_TYPES_H
#define R_AUTH_TYPES_H

#include <stddef.h>
#include <stdint.h>

#ifndef R_HYBRID_PLC_RF
#include "r_auth_config.h"
#endif /* R_HYBRID_PLC_RF */
#include "r_header_utils.h"

#ifdef R_HYBRID_PLC_RF
#ifndef R_KEY_NUM
#define R_KEY_NUM 2
#endif /* R_KEY_NUM */
#else

/* Number of GTKs */
#if R_LEAF_NODE_ENABLED
#define R_AUTH_NUM_GTKS          0 //!< LFNs do not support GTKs
#define R_AUTH_GTK_LIVENESS_MASK 0
#else
#define R_AUTH_NUM_GTKS          4    //!< Wi-SUN FAN 1.0 nodes and FAN 1.1 FFNs use 4 GTKs
#define R_AUTH_GTK_LIVENESS_MASK 0x0F //!< GTK liveness is indicated by the lower 4 bits of the liveness byte
#endif

/* Number of LGTKs */
#if R_BORDER_ROUTER_ENABLED || R_LFN_PARENTING_ENABLED || R_LEAF_NODE_ENABLED
#define R_AUTH_NUM_LGTKS          3                         //!< Wi-SUN FAN 1.1 LFNs use 3 LGTKs
#define R_AUTH_LGTK_LIVENESS_MASK (0x07 << R_AUTH_NUM_GTKS) //!< LGTK liveness is indicated by the 3 bits above the GTK liveness bits of the liveness byte
#else
#define R_AUTH_NUM_LGTKS          0                         //!< Wi-SUN FAN 1.0 nodes do not support LGTKs
#define R_AUTH_LGTK_LIVENESS_MASK 0
#endif

#define R_KEY_NUM                 (R_AUTH_NUM_GTKS + R_AUTH_NUM_LGTKS) //!< Total number of keys (GTKs + LGTKs)

#endif /* R_HYBRID_PLC_RF */

#define R_AUTH_PMK_SIZE           32 //!< Size of the Pairwise Master Key in bytes
#define R_AUTH_PTK_SIZE           48 //!< Size of the Pairwise Transient Key in bytes
#define R_AUTH_KCK_SIZE           16 //!< Size of the Key Confirmation Key (first part of PTK) in bytes
#define R_AUTH_KEK_SIZE           16 //!< Size of the Key Encryption Key (second part of PTK) in bytes
#define R_AUTH_TK_SIZE            16 //!< Size of the Temporal Key (third part of PTK) in bytes
#define R_AUTH_GTK_SIZE           16 //!< Size of the Group Transient Key in bytes

#define R_AUTH_REPLAY_COUNTER_LEN 8  //!< Size of the replay counter for the RSN 4-way handshake

struct r_auth_sup_eap_ctx_s;         // Undefined struct to hide implementation details

/**
 * Node role IDs as defined for Node Role Information Element (NR-IE)
 * @details Padding is disabled since values in NR-IE are defined as 1-byte values
 */
typedef enum R_HEADER_UTILS_ATTR_PACKED r_auth_node_role_id_e
{
    R_AUTH_NODE_ROLE_ID_FFN_BR = 0,     //!< FFN (Full Function Node) Border Router
    R_AUTH_NODE_ROLE_ID_FFN_ROUTER = 1, //!< FFN (Full Function Node) Router Node
    R_AUTH_NODE_ROLE_ID_LFN = 2,        //!< LFN Limited Functional Node
    R_AUTH_NODE_ROLE_ID_FAN10_RN = 7,   //!< FAN 1.0 Router Node (for auth-internal usage; value not defined in Wi-SUN FAN spec)
} r_auth_node_role_id_t;


/** The state necessary for the supplicant */
typedef struct
{
    struct r_auth_sup_eap_ctx_s* eap_ctx;
    void*          wpa_sm;
    uint8_t        rsn_replay_counter[R_AUTH_REPLAY_COUNTER_LEN];
    uint32_t       replay_counter;   // <! the replay counter used for the Wi-SUN Initial Key message
    uint8_t        target[8];        //!< the destination for EAPOL messages
    uint8_t        authenticator[8]; //!< the authenticator EUI-64
    uint8_t        pmk[R_AUTH_PMK_SIZE];
    uint8_t        pmk_valid;
    uint8_t        ptk[R_AUTH_PTK_SIZE];
    uint8_t        ptk_valid;
    uint8_t        gtks_live; // bit mask that indicates liveness of GTKs and LGTKs (disseminated by border router)
    const uint8_t* encoded_certs;
    uint8_t        cert_index;
    uint8_t        cert_index_eos; // this is needed as a 0 byte because we use cert_index in place of a string for which strcmp is used
    size_t         mac_offset;     //!< The number of bytes that must be prepended to any buffer that is passed to the MAC layer for transmission
} r_auth_sup_state_t;


#if R_BR_AUTHENTICATOR_ENABLED

struct r_auth_br_rsn_ctx_s;  // Undefined struct to hide implementation details

/** The state kept on BR for each supplicant */
typedef struct
{
    uint32_t              last_rx;                //!< Timestamp (in clock seconds) of the last message reception from this SUP
    uint8_t               address[8];             //!< EUI-64 of the SUP; all-zero address indicates unused entry
    uint8_t               relay[16];              //!< IPv6 address of the next hop relay node for this SUP; [0]==0xff -> no relay
    uint8_t               pmk[R_AUTH_PMK_SIZE];   //!< The PMK negotiated with this SUP
    uint32_t              pmk_expiration_minutes; //!< PMK expiration timestamp (in clock minutes)
    uint8_t               ptk[R_AUTH_PTK_SIZE];   //!< The PTK negotiated with this SUP
    uint32_t              ptk_expiration_minutes; //!< PTK expiration timestamp (in clock minutes)
    uint8_t               gtks_valid;             //!< Bit mask indicating GTK and LGTK validity reported by this SUP (via GTKL and LGTKL KDE)
    uint8_t               retryPending : 1;       //!< Flag to indicate that the previous request failed due to insufficient resources on the BR
    r_auth_node_role_id_t node_role : 3;          //!< Node Role IDs of the SUP defined by the Node Role IE (NR-IE)
    struct key_replay_counter
    {
        uint8_t counter[R_AUTH_REPLAY_COUNTER_LEN];
        uint8_t valid;
    } key_replay;
} r_auth_br_supplicant_t;

/* Disable padding for the following structs so that they can be transmitted via the modem serial interface */
R_HEADER_UTILS_PRAGMA_PACK_1

typedef struct
{
    uint32_t pmk_lifetime_minutes;
    uint32_t ptk_lifetime_minutes;
    uint32_t gtk_lifetime_minutes;
    uint16_t gtk_new_activation_time_fraction;
    uint16_t revocation_lifetime_reduction_fraction;
    uint8_t  gtk_new_install_required_percentage;
} r_auth_key_lifetimes_t;

R_HEADER_UTILS_PRAGMA_PACK_DEFAULT

/** The state necessary for the BR */
typedef struct
{
    struct r_auth_br_rsn_ctx_s* rsn_ctx;
    uint16_t                current_supplicant;
    uint8_t                 current_gtk;   //!< The index within the GTK array, where the currently active GTK is located
    uint8_t                 current_lgtk;  //!< The index within the GTK array, where the currently active LGTK is located
    uint32_t                reservedUntil; //!< The time until the BR is "reserved" for the current supplicant (0 means "no reservation")
    r_auth_br_supplicant_t* supplicants;   //!< The supplicant table
    r_auth_key_lifetimes_t  key_lifetimes;
    uint32_t                gtk_expiration_seconds[R_KEY_NUM];
} r_auth_br_state_t;
#endif /* R_BR_AUTHENTICATOR_ENABLED */


/** The state necessary for the complete authentication module */
typedef struct
{
    r_auth_sup_state_t sup;
#if R_BR_AUTHENTICATOR_ENABLED
    r_auth_br_state_t  br;
#endif
    uint8_t            gtks_valid; // bit mask indicating the [L]GTKs available on this device; on BR, this also indicates liveness
    uint8_t            gtks[R_KEY_NUM][R_AUTH_GTK_SIZE];
} r_auth_state_t;

#endif /* R_AUTH_TYPES_H */
